/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#define USE_CATCH

#ifdef USE_CATCH
#define CATCH_CONFIG_MAIN
#endif
#define STRINGIFY(s) (#s)

#include <cstdio>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <chrono>

#ifdef USE_CATCH
#include "catch.hpp"
#endif
#include "json11.hpp"

#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_geowkt.h"
#include "gs_projection.h"
#include "gs_series.h"
#include "gs_string.h"
#include "gs_ascii.h"
#include "gs_flt.h"
#include "gs_gsr.h"
#include "gs_epsg.h"
#include "gs_operation.h"

#include "gs_network_flow.h"
#include "gs_level_set.h"
#include "gs_ode.h"
#include "gs_particle.h"
#include "gs_multigrid.h"
#include "gs_shallow_water.h"

using namespace Geostack;
using namespace std::chrono;

#ifndef USE_CATCH

// Test application
int main() {

    steady_clock::time_point opTime;

    try {    

        Solver &solver = Solver::getSolver();
        solver.setVerbose(true);
        solver.getContext();
        solver.getQueue();

    } catch (std::runtime_error &e) {

        std::cout << "ERROR: " << e.what() << std::endl;
        return 1;

    } catch (cl::Error e) {

        std::stringstream err;
        err << "OpenCL exception '" << e.what() << "': " << e.err();
        std::cout << err.str() << std::endl;
        return 1;
    }
    
    auto totalTime = duration<double, std::micro>(steady_clock::now()-opTime).count()*1.0E-6;
    std::cout << "Total: " << std::to_string(totalTime) << "s" << std::endl;
    
}

#else

steady_clock::time_point opTime;

TEST_CASE( "OpenCL initialisation", "[core]" ) {

    // Get solver singleton
    Solver &solver = Solver::getSolver();
    solver.setVerbose(true);
    solver.getContext();

    // Test initialisation
    REQUIRE( solver.openCLInitialised() == true );
}

TEST_CASE( "EPSG Projection Parameters", "[core]" ) {
    // get proj params from EPSG code
    auto projParamsStr = Geostack::projParamsFromEPSG("4326");
    
    REQUIRE(projParamsStr == "+proj=longlat +datum=WGS84 +no_defs");
    
    auto projParams = Projection::fromEPSG("4326");
    
    REQUIRE(projParams == Projection::parsePROJ4(projParamsStr));
}

TEST_CASE( "Property map", "[core]" ) {

    auto p = PropertyMap();
    p.setProperty("A", "10");
    p.setProperty("B", 20);
    p.setProperty("C", 30.0F);
    p.setProperty("D", 40.0);
    p.setProperty("E", (cl_uint)50);

    p.setProperty("Av", std::vector<std::string>( { "11", "12", "13", "14" } ));
    p.setProperty("Bv", std::vector<int>( { 21, 22, 23, 24 } ));
    p.setProperty("Cv", std::vector<float>( { 31.0F, 32.0F, 33.0F, 34.0F } ));
    p.setProperty("Dv", std::vector<double>( { 41.0, 42.0, 43.0, 44.0 } ));
    p.setProperty("Ev", std::vector<cl_uint>( { 51, 52, 53, 54 } ));
    p.setProperty("Fv", std::vector<std::vector<float> >( {
        { 101.0F, 102.0F, 103.0F, 104.0F },
        { 111.0F, 112.0F, 113.0F, 114.0F },
        { 121.0F, 122.0F, 123.0F, 124.0F },
        { 131.0F, 132.0F, 133.0F, 134.0F } } ));
    p.setProperty("Gv", std::vector<std::vector<double> >( {
        { 101.0, 102.0, 103.0, 104.0 },
        { 111.0, 112.0, 113.0, 114.0 },
        { 121.0, 122.0, 123.0, 124.0 },
        { 131.0, 132.0, 133.0, 134.0 } } ));

    std::string expected = 
        R"({"A": "10", "Av": ["11", "12", "13", "14"], "B": 20, "Bv": [21, 22, 23, 24], "C": 30, "Cv": [31, 32, 33, 34],)"
        R"( "D": 40, "Dv": [41, 42, 43, 44], "E": 50, "Ev": [51, 52, 53, 54],)"
        R"( "Fv": [[101, 102, 103, 104], [111, 112, 113, 114], [121, 122, 123, 124], [131, 132, 133, 134]],)"
        R"( "Gv": [[101, 102, 103, 104], [111, 112, 113, 114], [121, 122, 123, 124], [131, 132, 133, 134]]})";
    REQUIRE( p.toJsonString() == expected );

    auto A_type = p.getPropertyType("A");
    auto B_type = p.getPropertyType("B");
    auto C_type = p.getPropertyType("C");
    auto D_type = p.getPropertyType("D");
    auto E_type = p.getPropertyType("E");

    auto Av_type = p.getPropertyType("Av");
    auto Bv_type = p.getPropertyType("Bv");
    auto Cv_type = p.getPropertyType("Cv");
    auto Dv_type = p.getPropertyType("Dv");
    auto Ev_type = p.getPropertyType("Ev");
    auto Fv_type = p.getPropertyType("Fv");
    auto Gv_type = p.getPropertyType("Gv");

    auto A_structure = p.getPropertyStructure("A");
    auto B_structure = p.getPropertyStructure("B");
    auto C_structure = p.getPropertyStructure("C");
    auto D_structure = p.getPropertyStructure("D");
    auto E_structure = p.getPropertyStructure("E");

    auto Av_structure = p.getPropertyStructure("Av");
    auto Bv_structure = p.getPropertyStructure("Bv");
    auto Cv_structure = p.getPropertyStructure("Cv");
    auto Dv_structure = p.getPropertyStructure("Dv");
    auto Ev_structure = p.getPropertyStructure("Ev");
    auto Fv_structure = p.getPropertyStructure("Fv");
    auto Gv_structure = p.getPropertyStructure("Gv");

    REQUIRE( A_type == PropertyType::String );
    REQUIRE( B_type == PropertyType::Integer );
    REQUIRE( C_type == PropertyType::Float );
    REQUIRE( D_type == PropertyType::Double );
    REQUIRE( E_type == PropertyType::Index );

    REQUIRE( Av_type == PropertyType::String );
    REQUIRE( Bv_type == PropertyType::Integer );
    REQUIRE( Cv_type == PropertyType::Float );
    REQUIRE( Dv_type == PropertyType::Double );
    REQUIRE( Ev_type == PropertyType::Index );
    REQUIRE( Fv_type == PropertyType::FloatVector );
    REQUIRE( Gv_type == PropertyType::DoubleVector );

    REQUIRE( A_structure == PropertyStructure::Scalar );
    REQUIRE( B_structure == PropertyStructure::Scalar );
    REQUIRE( C_structure == PropertyStructure::Scalar );
    REQUIRE( D_structure == PropertyStructure::Scalar );
    REQUIRE( E_structure == PropertyStructure::Scalar );

    REQUIRE( Av_structure == PropertyStructure::Vector );
    REQUIRE( Bv_structure == PropertyStructure::Vector );
    REQUIRE( Cv_structure == PropertyStructure::Vector );
    REQUIRE( Dv_structure == PropertyStructure::Vector );
    REQUIRE( Ev_structure == PropertyStructure::Vector );
    REQUIRE( Fv_structure == PropertyStructure::Vector );
    REQUIRE( Gv_structure == PropertyStructure::Vector );

    REQUIRE( p.template getProperty<std::string>("A") == "10" );
    REQUIRE( p.template getProperty<std::string>("B") == std::to_string(20) );
    REQUIRE( p.template getProperty<std::string>("C") == std::to_string(30.0F) );
    REQUIRE( p.template getProperty<std::string>("D") == std::to_string(40.0) );
    REQUIRE( p.template getProperty<std::string>("E") == std::to_string((cl_uint)50) );

    REQUIRE( p.template getProperty<int>("A") == 10 );
    REQUIRE( p.template getProperty<int>("B") == 20 );
    REQUIRE( p.template getProperty<int>("C") == 30 );
    REQUIRE( p.template getProperty<int>("D") == 40 );
    REQUIRE( p.template getProperty<int>("E") == 50 );

    REQUIRE( p.template getProperty<float>("A") == 10.0F );
    REQUIRE( p.template getProperty<float>("B") == 20.0F );
    REQUIRE( p.template getProperty<float>("C") == 30.0F );
    REQUIRE( p.template getProperty<float>("D") == 40.0F );
    REQUIRE( p.template getProperty<float>("E") == 50.0F );

    REQUIRE( p.template getProperty<double>("A") == 10.0 );
    REQUIRE( p.template getProperty<double>("B") == 20.0 );
    REQUIRE( p.template getProperty<double>("C") == 30.0 );
    REQUIRE( p.template getProperty<double>("D") == 40.0 );
    REQUIRE( p.template getProperty<double>("E") == 50.0 );

    REQUIRE( p.template getPropertyFromVector<cl_uint>("Av", 1) == (cl_uint)12 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Bv", 1) == (cl_uint)22 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Cv", 1) == (cl_uint)32 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Dv", 1) == (cl_uint)42 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Ev", 1) == (cl_uint)52 );

    REQUIRE( p.template getPropertyFromVector<int>("Av", 1) == 12 );
    REQUIRE( p.template getPropertyFromVector<int>("Bv", 1) == 22 );
    REQUIRE( p.template getPropertyFromVector<int>("Cv", 1) == 32 );
    REQUIRE( p.template getPropertyFromVector<int>("Dv", 1) == 42 );
    REQUIRE( p.template getPropertyFromVector<int>("Ev", 1) == 52 );

    REQUIRE( p.template getPropertyFromVector<float>("Av", 1) == 12.0F );
    REQUIRE( p.template getPropertyFromVector<float>("Bv", 1) == 22.0F );
    REQUIRE( p.template getPropertyFromVector<float>("Cv", 1) == 32.0F );
    REQUIRE( p.template getPropertyFromVector<float>("Dv", 1) == 42.0F );
    REQUIRE( p.template getPropertyFromVector<float>("Ev", 1) == 52.0F );

    REQUIRE( p.template getPropertyFromVector<double>("Av", 1) == 12.0 );
    REQUIRE( p.template getPropertyFromVector<double>("Bv", 1) == 22.0 );
    REQUIRE( p.template getPropertyFromVector<double>("Cv", 1) == 32.0 );
    REQUIRE( p.template getPropertyFromVector<double>("Dv", 1) == 42.0 );
    REQUIRE( p.template getPropertyFromVector<double>("Ev", 1) == 52.0 );

    REQUIRE( p.template getPropertyFromVector<cl_uint>("Av", 1) == (cl_uint)12 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Bv", 1) == (cl_uint)22 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Cv", 1) == (cl_uint)32 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Dv", 1) == (cl_uint)42 );
    REQUIRE( p.template getPropertyFromVector<cl_uint>("Ev", 1) == (cl_uint)52 );

    REQUIRE( p.template getPropertyFromVector<std::vector<float> >("Fv", 1)[2] == 113.0F );
    REQUIRE( p.template getPropertyFromVector<std::vector<double> >("Gv", 1)[2] == 113.0 );

    std::string &Ar = p.template getPropertyRef<std::string>("A");
    REQUIRE( Ar == "10" );

    int &Br = p.template getPropertyRef<int>("B");
    REQUIRE( Br == 20 );

    float &Cr = p.template getPropertyRef<float>("C");
    REQUIRE( Cr == 30.0F );

    double &Dr = p.template getPropertyRef<double>("D");
    REQUIRE( Dr == 40.0 );

    cl_uint &Er = p.template getPropertyRef<cl_uint>("E");
    REQUIRE( Er == (cl_uint)50 );

    {
    std::vector<std::string> &Avr = p.template getPropertyRef<std::vector<std::string> >("Av");
    REQUIRE( Avr[1] == "12" );

    std::vector<int> &Bvr = p.template getPropertyRef<std::vector<int> >("Bv");
    REQUIRE( Bvr[1] == 22 );

    std::vector<float> &Cvr = p.template getPropertyRef<std::vector<float> >("Cv");
    REQUIRE( Cvr[1] == 32.0F );

    std::vector<double> &Dvr = p.template getPropertyRef<std::vector<double> >("Dv");
    REQUIRE( Dvr[1] == 42.0 );

    std::vector<cl_uint> &Evr = p.template getPropertyRef<std::vector<cl_uint> >("Ev");
    REQUIRE( Evr[1] == (cl_uint)52 );

    std::vector<std::vector<float> > &Fvr = p.template getPropertyRef<std::vector<std::vector<float> > >("Fv");
    REQUIRE( Fvr[1][2] == 113.0F );

    std::vector<std::vector<double> > &Gvr = p.template getPropertyRef<std::vector<std::vector<double> > >("Gv");
    REQUIRE( Gvr[1][2] == 113.0 );

    Avr[1] = "120";
    std::vector<std::string> &Avr2 = p.template getPropertyRef<std::vector<std::string> >("Av");
    REQUIRE( Avr2[1] == "120" );

    Bvr[1] = 220;
    std::vector<int> &Bvr2 = p.template getPropertyRef<std::vector<int> >("Bv");
    REQUIRE( Bvr2[1] == 220 );

    Cvr[1] = 320.0F;
    std::vector<float> &Cvr2 = p.template getPropertyRef<std::vector<float> >("Cv");
    REQUIRE( Cvr2[1] == 320.0F );

    Dvr[1] = 420.0;
    std::vector<double> &Dvr2 = p.template getPropertyRef<std::vector<double> >("Dv");
    REQUIRE( Dvr2[1] == 420.0 );

    Evr[1] = (cl_uint)520;
    std::vector<cl_uint> &Evr2 = p.template getPropertyRef<std::vector<cl_uint> >("Ev");
    REQUIRE( Evr2[1] == (cl_uint)520 );

    Fvr[1][2] = 620.0F;
    std::vector<std::vector<float> > &Fvr2 = p.template getPropertyRef<std::vector<std::vector<float> > >("Fv");
    REQUIRE( Fvr2[1][2] == 620.0F );

    Gvr[1][2] = (cl_uint)720.0;
    std::vector<std::vector<double> > &Gvr2 = p.template getPropertyRef<std::vector<std::vector<double> > >("Gv");
    REQUIRE( Gvr2[1][2] == 720.0 );
    }

    auto q = PropertyMap();
    q.setProperty("Av", std::vector<std::string>( { "21", "22", "23", "24" } ));
    q.setProperty("Bv", std::vector<int>( { 31, 32, 33, 34 } ));
    q.setProperty("Cv", std::vector<float>( { 41.0F, 42.0F, 43.0F, 44.0F } ));
    q.setProperty("Dv", std::vector<double>( { 51.0, 52.0, 53.0, 54.0 } ));
    q.setProperty("Ev", std::vector<cl_uint>( { 61, 62, 63, 64 } ));
    q.setProperty("Fv", std::vector<std::vector<float> >( {
        { 201.0F, 202.0F, 203.0F, 204.0F },
        { 211.0F, 212.0F, 213.0F, 214.0F },
        { 221.0F, 222.0F, 223.0F, 224.0F },
        { 231.0F, 232.0F, 233.0F, 234.0F } } ));
    q.setProperty("Gv", std::vector<std::vector<double> >( {
        { 201.0, 202.0, 203.0, 204.0 },
        { 211.0, 212.0, 213.0, 214.0 },
        { 221.0, 222.0, 223.0, 224.0 },
        { 231.0, 232.0, 233.0, 234.0 } } ));

    p+=q;

    // Test copy
    q.copy("Av", 1, 3);
    q.copy("Bv", 1, 3);
    q.copy("Cv", 1, 3);
    q.copy("Dv", 1, 3);
    q.copy("Ev", 1, 3);

    {
    std::vector<std::string> &Avr = q.template getPropertyRef<std::vector<std::string> >("Av");
    REQUIRE( Avr[3] == "22" );

    std::vector<int> &Bvr = q.template getPropertyRef<std::vector<int> >("Bv");
    REQUIRE( Bvr[3] == 32 );

    std::vector<float> &Cvr = q.template getPropertyRef<std::vector<float> >("Cv");
    REQUIRE( Cvr[3] == 42.0F );

    std::vector<double> &Dvr = q.template getPropertyRef<std::vector<double> >("Dv");
    REQUIRE( Dvr[1] == 52.0 );

    std::vector<cl_uint> &Evr = q.template getPropertyRef<std::vector<cl_uint> >("Ev");
    REQUIRE( Evr[1] == (cl_uint)62 );
    }

    {
    std::vector<std::string> &Avr = p.template getPropertyRef<std::vector<std::string> >("Av");
    REQUIRE( Avr.size() == 8 );
    REQUIRE( Avr[7] == "24" );

    std::vector<int> &Bvr = p.template getPropertyRef<std::vector<int> >("Bv");
    REQUIRE( Bvr.size() == 8 );
    REQUIRE( Bvr[7] == 34 );

    std::vector<float> &Cvr = p.template getPropertyRef<std::vector<float> >("Cv");
    REQUIRE( Cvr.size() == 8 );
    REQUIRE( Cvr[7] == 44.0F );

    std::vector<double> &Dvr = p.template getPropertyRef<std::vector<double> >("Dv");
    REQUIRE( Dvr.size() == 8 );
    REQUIRE( Dvr[7] == 54.0 );

    std::vector<cl_uint> &Evr = p.template getPropertyRef<std::vector<cl_uint> >("Ev");
    REQUIRE( Evr.size() == 8 );
    REQUIRE( Evr[7] == (cl_uint)64 );

    std::vector<std::vector<float> > &Fvr = p.template getPropertyRef<std::vector<std::vector<float> > >("Fv");
    REQUIRE( Fvr.size() == 8 );
    REQUIRE( Fvr[7][2] == 233.0F );

    std::vector<std::vector<double> > &Gvr = p.template getPropertyRef<std::vector<std::vector<double> > >("Gv");
    REQUIRE( Gvr.size() == 8 );
    REQUIRE( Gvr[7][2] == 233.0 );
    }
}


TEST_CASE( "Property parsing", "[core]" ) {

    // Create GeoJSON string
    std::string json = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"A": 1, "B": null, "C": null, "D": null}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 2, "B": 1, "C": null, "D": null}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 3, "B": 2, "C": null, "D": null}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 4, "B": 3, "C": "test", "D": null}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 5, "B": 4, "C": null, "D": null}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json);

    REQUIRE( v.getProperty<int>(0, "A") == 1 );
    REQUIRE( v.getProperty<int>(1, "A") == 2 );
    REQUIRE( v.getProperty<int>(2, "A") == 3 );
    REQUIRE( v.getProperty<int>(3, "A") == 4 );
    REQUIRE( v.getProperty<int>(4, "A") == 5 );

    REQUIRE( isInvalid<int>(v.getProperty<int>(0, "B")) );
    REQUIRE( v.getProperty<int>(1, "B") == 1 );
    REQUIRE( v.getProperty<int>(2, "B") == 2 );
    REQUIRE( v.getProperty<int>(3, "B") == 3 );
    REQUIRE( v.getProperty<int>(4, "B") == 4 );

    REQUIRE( v.getProperty<std::string>(0, "C") == getNullValue<std::string>() );
    REQUIRE( v.getProperty<std::string>(1, "C") == getNullValue<std::string>() );
    REQUIRE( v.getProperty<std::string>(2, "C") == getNullValue<std::string>() );
    REQUIRE( v.getProperty<std::string>(3, "C") == "test" );
    REQUIRE( v.getProperty<std::string>(4, "C") == getNullValue<std::string>() );

    REQUIRE( isInvalid<int>(v.getProperty<int>(0, "D")) );
    REQUIRE( isInvalid<int>(v.getProperty<int>(1, "D")) );
    REQUIRE( isInvalid<int>(v.getProperty<int>(2, "D")) );
    REQUIRE( isInvalid<int>(v.getProperty<int>(3, "D")) );
    REQUIRE( isInvalid<int>(v.getProperty<int>(4, "D")) );

    REQUIRE( isInvalid<REAL>(v.getProperty<REAL>(0, "D")) );
    REQUIRE( isInvalid<REAL>(v.getProperty<REAL>(1, "D")) );
    REQUIRE( isInvalid<REAL>(v.getProperty<REAL>(2, "D")) );
    REQUIRE( isInvalid<REAL>(v.getProperty<REAL>(3, "D")) );
    REQUIRE( isInvalid<REAL>(v.getProperty<REAL>(4, "D")) );
}

TEST_CASE( "Raster initialisation", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init(5, 5, 5, 1.0, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2, 2);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.max() == (REAL)99.9 );
    REQUIRE( testRasterA.min() == (REAL)1.0 );
}

TEST_CASE( "Raster equality", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init(5, 5, 5, 1.0, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init(5, 5, 5, 1.0, 1.0, 1.0);
    testRasterB.setAllCellValues((REAL)1.0);
    testRasterB.setCellValue((REAL)99.9, 2, 2, 2);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterA == testRasterB );
}

TEST_CASE( "Raster resize 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    // Resize
    testRasterA.resize2D(3, 3, 1, 1);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(2, 2) == (REAL)99.9 );
    REQUIRE( testRasterA.max() == (REAL)99.9 );
    REQUIRE( testRasterA.min() == (REAL)1.0 );
}

TEST_CASE( "Raster variable script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setVariableData<REAL>("varA", (REAL)99.9);
    testRasterA.setVariableData<REAL>("varB", (REAL)88.8);
    testRasterA.setVariableData<REAL>("varC", (REAL)77.7);
    testRasterA.setVariableData<REAL>("arrD", (REAL)111.0, 0);
    testRasterA.setVariableData<REAL>("arrD", (REAL)111.1, 1);
    testRasterA.setVariableData<REAL>("arrD", (REAL)111.2, 2);
    testRasterA.setVariableData<REAL>("arrD", (REAL)111.3, 3);

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varA;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)99.9 );

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varB;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)88.8 );

    // Run script
    runScript<REAL>("testRasterA = testRasterA::varC;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)77.7 );

    // Run script
    runScript<REAL>(
        "testRasterA = testRasterA::varA+testRasterA::varB+testRasterA::varC+testRasterA::arrD;",
        {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)99.9+(REAL)88.8+(REAL)77.7+(REAL)111.0 );

    // Run script
    runScript<REAL>(
        "testRasterA = testRasterA::arrD[2]; testRasterA::arrD[3] = 999.9",
        {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(1, 1) == (REAL)111.2 );
    REQUIRE( testRasterA.getVariableData<REAL>("arrD", 3) == (REAL)999.9 );
}

TEST_CASE( "Raster float script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues((REAL)2.0);

    makeRaster(testRasterC, REAL, REAL)
    testRasterC.init2D(15, 15, 1.0, 1.0, -5, -5);

    // Run script
    std::string script = "testRasterC = 100.0 + testRasterA * testRasterB;";
    runScript<REAL>(script, {testRasterC, testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (REAL)299.8 );
    REQUIRE( testRasterC.min() == (REAL)102.0 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (REAL)299.8 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (REAL)299.8 );
    REQUIRE( testRasterC.getBilinearValue(2.5, 2.5) == (REAL)299.8 );
}

TEST_CASE( "Raster float output script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues((REAL)2.0);

    // Run script
    std::string script = "output = 100.0 + testRasterA * testRasterB;";
    auto testRasterC = runScript<REAL, REAL>(script, {testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (REAL)299.8 );
    REQUIRE( testRasterC.min() == (REAL)102.0 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (REAL)299.8 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (REAL)299.8 );
    REQUIRE( testRasterC.getBilinearValue(2.5, 2.5) == (REAL)299.8 );
}

TEST_CASE( "Raster integer output script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, uint32_t, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues(1);
    testRasterA.setCellValue(99, 2, 2);

    makeRaster(testRasterB, uint32_t, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues(2);

    // Run script
    std::string script = "output = 100.0 + testRasterA * testRasterB;";
    auto testRasterC = runScript<uint32_t, REAL>(script, {testRasterA, testRasterB}, RasterNullValue::Zero);

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (uint32_t)298 );
    REQUIRE( testRasterC.min() == (uint32_t)100 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (uint32_t)298 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (uint32_t)298 );
}

TEST_CASE( "Raster byte output script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, uint8_t, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues(1);
    testRasterA.setCellValue(99, 2, 2);

    makeRaster(testRasterB, uint8_t, REAL)
    testRasterB.init2D(15, 15, 1.0, 1.0, -5, -5);
    testRasterB.setAllCellValues(2);

    // Run script
    std::string script = "output = 10 + testRasterA * testRasterB;";
    auto testRasterC = runScript<uint8_t, REAL>(script, {testRasterA, testRasterB}, RasterNullValue::Zero);

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.max() == (uint8_t)208 );
    REQUIRE( testRasterC.min() == (uint8_t)10 );
    REQUIRE( testRasterC.getCellValue(7, 7) == (uint8_t)208 );
    REQUIRE( testRasterC.getNearestValue(2.5, 2.5) == (uint8_t)208 );
}

TEST_CASE( "Raster integer float output script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(50, 50, (REAL)0.1, (REAL)0.1);
    testRasterA.setAllCellValues(0.0);

    makeRaster(testRasterB, uint32_t, REAL)
    testRasterB.init2D(50, 50, (REAL)0.1, (REAL)0.1);
    testRasterB.setAllCellValues(0);

    // Create gradient
    testRasterA = runScript<REAL, REAL>("output = x*y;", {testRasterA} );

    // Run script
    std::string script = "output = testRasterA;";
    auto testRasterC = runScript<uint32_t, REAL>(script, {testRasterB, testRasterA});

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( fabs(testRasterA.getCellValue(26, 14)-(REAL)2.65*(REAL)1.45) < 1.0E-6 );
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.getCellValue(26, 14) == (uint32_t)3 );
}

TEST_CASE( "Raster float area script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(21, 21, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)0.0);
    testRasterA.setCellValue((REAL)99.9, 10, 10);

    // Run script
    std::string script = R"(

    // Average
    if (isValid_REAL(testRasterA)) {
        if isValid_REAL(output) {
            output += testRasterA;
        } else {
            output = testRasterA;
        }
        sum+=1.0;
    }

    )";
    auto output = runAreaScript<REAL, REAL>(script, testRasterA, 3);

    // Test values
    REQUIRE( output.hasData() == true );
    REQUIRE( output.getCellValue(10, 10) == ((REAL)99.9/(REAL)49.0) );
}

TEST_CASE( "Raster reduction script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(300, 300, 1.0, 1.0);

    // Set values
    testRasterA.setAllCellValues((REAL)-99.9);
    testRasterA.setCellValue((REAL)99.9, 250, 250);

    // Test maximum reduction
    testRasterA.setReductionType(Reduction::Maximum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)99.9);

    // Test minimum reduction
    testRasterA.setReductionType(Reduction::Minimum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)-99.9);

    // Test maximum reduction
    testRasterA.setReductionType(Reduction::Maximum);
    runScript<REAL>("testRasterA = (ipos&0x03)*(jpos&0x03);", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)9.0);

    // Test count reduction
    testRasterA.setReductionType(Reduction::Count);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)90000);

    // Test sum reduction
    testRasterA.setReductionType(Reduction::Sum);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)202500);

    // Test mean reduction
    testRasterA.setReductionType(Reduction::Mean);
    runScript<REAL>("", { testRasterA } );
    REQUIRE(testRasterA.reduce() == (REAL)2.25);

    // Test copy and maximum reduction
    auto testRasterB = runScript<REAL, REAL>("output = testRasterA;", { testRasterA }, Reduction::Maximum );
    REQUIRE(testRasterB.reduce() == (REAL)9.0);
}

TEST_CASE( "Raster random script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(100, 100, 1.0, 1.0);
    testRasterA.setProperty<cl_uint>("random_seed", 12345);

    testRasterA.setReductionType(Reduction::Mean);
    runScriptNoOut<REAL>("testRasterA = random;", { testRasterA });

    REQUIRE( fabs(testRasterA.reduce()-(REAL)0.5) < 0.1 );

    // Create raster
    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(100, 100, 1.0, 1.0);

    testRasterB.setReductionType(Reduction::Mean);
    runScriptNoOut<REAL>("testRasterB = randomNormal(5.0, 1.0);", { testRasterB });

    REQUIRE( fabs(testRasterB.reduce()-(REAL)5.0) < 0.1 );
}

TEST_CASE( "Raster boundary script 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(1000, 1000, 1.0, 1.0);

    runScriptNoOut<REAL>("testRasterA = isBoundary_N || isBoundary_S || isBoundary_W || isBoundary_E;", { testRasterA });

    REQUIRE( fabs(testRasterA.getCellValue(0, 0)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(999, 999)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(999, 0)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(0, 999)) == 1 );
    REQUIRE( fabs(testRasterA.getCellValue(1, 1)) == 0 );
    REQUIRE( fabs(testRasterA.getCellValue(499, 499)) == 0 );
}

TEST_CASE( "Raster debug script", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    makeRaster(rA, REAL, REAL)
    makeRaster(rB, REAL, REAL)

    rA.init2D(100, 100, 1, 1);
    rB.init2D(100, 100, 1, 1);
    rA.setVariableData("A", (REAL)11.1);
    rB.setVariableData("B", (REAL)22.2);

    runScriptNoOut<REAL>("rA = x+rB::B; rB = x+rA::A; rA::A = 44.4; rB::B = 55.5", {rA, rB}, RasterDebug::Enable);

    REQUIRE(rA.getCellValue(0, 0) == (REAL)22.7);
    REQUIRE(rB.getCellValue(0, 0) == (REAL)11.6);
    REQUIRE(rA.getCellValue(1, 1) != rA.getCellValue(1, 1));
    REQUIRE(rB.getCellValue(1, 1) != rA.getCellValue(1, 1));
    
    REQUIRE(rA.getVariableData<REAL>("A") == (REAL)44.4);
    REQUIRE(rB.getVariableData<REAL>("B") == (REAL)55.5);
}

TEST_CASE( "Raster neighbours 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(5, 5, 1.0, 1.0);
    runScript<REAL>("testRasterA = ipos+jpos*5;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(3, 3) == (REAL)18 );

    // Map north neighbours
    runScript<REAL>("testRasterB = testRasterA_N;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)23 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)24 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_E;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)19 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)14 );
    REQUIRE( testRasterB.getCellValue(4, 4) != testRasterB.getCellValue(4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_S;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)13 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)12 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_W;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)17 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3) == (REAL)22 );
    REQUIRE( testRasterB.getCellValue(0, 0) != testRasterB.getCellValue(0, 0) );
}

TEST_CASE( "Raster sampling 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(20, 20, 0.25, 0.25);

    // Run script
    //   This will default to use the minimum resolution of the two rasters
    std::string script = "output = testRasterA;";
    testRasterB = runScript<REAL, REAL>(script, {testRasterA, testRasterB} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getNearestValue(2.5, 2.5) == testRasterB.getNearestValue(2.5, 2.5) );
}

TEST_CASE( "Raster intersection 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);
    testRasterA.setAllCellValues((REAL)1.0);
    testRasterA.setCellValue((REAL)99.9, 2, 2);

    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init2D(20, 20, 0.25, 0.25, -4, -4);
    testRasterB.setAllCellValues((REAL)-1.0);

    // Run script
    //   This will use zero as the null value and default
    //   to use the minimum resolution of the two rasters
    std::string script = "output = testRasterA + testRasterB;";
    auto testRasterC = runScript<REAL, REAL>(script, {testRasterA, testRasterB}, RasterNullValue::Zero);

    // Test values
    REQUIRE( testRasterC.hasData() == true );
    REQUIRE( testRasterC.getCellValue(17, 17) == (REAL)0.0 );
}

TEST_CASE( "Raster properties 2D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init2D(5, 5, 1.0, 1.0);

    // Set properties
    testRasterA.setProperty("property0", 99);
    testRasterA.setProperty("property1", "rstr");

    // Test values
    REQUIRE( testRasterA.template getProperty<int>("property0") == 99 );
    REQUIRE( testRasterA.template getProperty<std::string>("property1") == "rstr" );
}

TEST_CASE( "Raster float script 3D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRaster2D, REAL, REAL)
    testRaster2D.init2D(5, 5, 1.0, 1.0);
    makeRaster(testRaster3D_A, REAL, REAL)
    testRaster3D_A.init(5, 5, 5, 1.0, 1.0, 1.0);
    makeRaster(testRaster3D_B, REAL, REAL)
    testRaster3D_B.init(5, 5, 5, 1.0, 1.0, 1.0);
    makeRaster(testRaster3D_C, REAL, REAL)
    testRaster3D_C.init(10, 10, 10, 0.5, 0.5, 0.5);

    // Create 3D raster
    runScript<REAL>("testRaster3D_A = x*y*z;", { testRaster3D_A } );
    REQUIRE( testRaster3D_A.hasData() == true );
    REQUIRE( testRaster3D_A.getCellValue(2, 2, 2) == (REAL)15.625 );

    // Sum 3D raster into 3D raster
    std::string scriptA = "output = 0; for (uint k = 0; k < testRaster3D_A_layers; k++) output += testRaster3D_A[k];";
    auto output = runScript<REAL, REAL>(scriptA, { testRaster3D_A } );
    REQUIRE( output.hasData() == true );
    REQUIRE( output.getCellValue(1, 1, 0) == (REAL)28.125 );
    REQUIRE( output.getCellValue(1, 1, 2) == (REAL)28.125 );
    REQUIRE( output.getCellValue(1, 1, 4) == (REAL)28.125 );

    // Sum 3D raster into 2D raster
    std::string scriptB = "testRaster2D = 0; for (uint k = 0; k < testRaster3D_A_layers; k++) testRaster2D += testRaster3D_A[k];";
    runScript<REAL>(scriptB, { testRaster2D, testRaster3D_A } );
    REQUIRE( testRaster2D.hasData() == true );
    REQUIRE( testRaster2D.getCellValue(1, 1) == (REAL)28.125 );

    // Map 2D raster into 3D raster
    std::string scriptC = "for (uint k = 0; k < testRaster3D_B_layers; k++) testRaster3D_B[k] = testRaster2D+k;";
    runScript<REAL>(scriptC, { testRaster2D, testRaster3D_B } );
    REQUIRE( testRaster3D_B.hasData() == true );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 1) == (REAL)29.125 );

    // Map 3D raster into 3D raster
    std::string scriptD = "testRaster3D_B = testRaster3D_A+1.0;";
    runScript<REAL>(scriptD, { testRaster3D_B, testRaster3D_A } );
    REQUIRE( testRaster3D_B.getCellValue(2, 2, 2) == (REAL)16.625 );

    // Run script
    std::string scriptE = "testRaster3D_B = testRaster2D+1.0;";
    runScript<REAL>(scriptE, { testRaster3D_B, testRaster2D } );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 0) == (REAL)29.125 );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 2) == (REAL)29.125 );
    REQUIRE( testRaster3D_B.getCellValue(1, 1, 4) == (REAL)29.125 );

    // Map non-aligned 3D raster into 2D raster
    runScript<REAL>("testRaster3D_C = 2.0*z;", { testRaster3D_C } );
    REQUIRE( testRaster3D_C.hasData() == true );
    REQUIRE( testRaster3D_C.getCellValue(1, 1, 1) == (REAL)1.5 );
    std::string scriptF = "testRaster2D = 0.0; for (uint k = 0; k < testRaster3D_C_layers; k++) testRaster2D += testRaster3D_C[k];";
    runScript<REAL>(scriptF, { testRaster2D, testRaster3D_C } );
    REQUIRE( testRaster2D.hasData() == true );
    REQUIRE( testRaster2D.getCellValue(1, 1) == (REAL)50.0 );

    // Map 2D layer from 3D raster
    testRaster2D.setOrigin_z(3.125);
    runScript<REAL>("testRaster2D = testRaster3D_C;", { testRaster2D, testRaster3D_C } );
    REQUIRE( testRaster2D.hasData() == true );
    REQUIRE( testRaster2D.getCellValue(1, 1) == (REAL)6.5 );

    // Sum multiple indices of 3D raster
    std::string scriptG = "testRaster2D = 0; testRaster2D += testRaster3D_A[0] + testRaster3D_A[1];";
    runScript<REAL, REAL>(scriptG, { testRaster2D, testRaster3D_A } );
    REQUIRE( testRaster2D.hasData() == true );
    REQUIRE( testRaster2D.getCellValue(1, 1) == (REAL)4.5 );
}

TEST_CASE( "Raster float sort 3D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create raster
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init(5, 5, 5, 1.0, 1.0, 1.0);

    REAL kVals[] = { 9.0, 7.5, 22.2, getNullValue<REAL>(), 3.7 };
    for (int k = 0; k < 5; k++) {
        for (int j = 0; j < 5; j++) {
            for (int i = 0; i < 5; i++) {
                testRasterA.setCellValue(kVals[k], i, j, k);
            }
        }
    }

    // Run sort
    sortColumns<REAL>(testRasterA);

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(2, 2, 0) == (REAL)3.7 );
    REQUIRE( testRasterA.getCellValue(2, 2, 3) == (REAL)22.2 );
    REQUIRE( testRasterA.getCellValue(2, 2, 4) != testRasterA.getCellValue(2, 2, 4) );
}

TEST_CASE( "Raster neighbours 3D", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create rasters
    makeRaster(testRasterA, REAL, REAL)
    testRasterA.init(5, 5, 5, 1.0, 1.0, 1.0);
    makeRaster(testRasterB, REAL, REAL)
    testRasterB.init(5, 5, 5, 1.0, 1.0, 1.0);
    runScript<REAL>("testRasterA = ipos+(jpos+kpos*5)*5;", {testRasterA} );

    // Test values
    REQUIRE( testRasterA.hasData() == true );
    REQUIRE( testRasterA.getCellValue(3, 3, 3) == (REAL)93 );

    // Map north neighbours
    runScript<REAL>("testRasterB = testRasterA_N;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)98 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)99 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_E;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)94 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SE;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)89 );
    REQUIRE( testRasterB.getCellValue(4, 4, 4) != testRasterB.getCellValue(4, 4, 4) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_S;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)88 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_SW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)87 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_W;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)92 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );

    // Map neighbours
    runScript<REAL>("testRasterB = testRasterA_NW;", {testRasterB, testRasterA} );

    // Test values
    REQUIRE( testRasterB.hasData() == true );
    REQUIRE( testRasterB.getCellValue(3, 3, 3) == (REAL)97 );
    REQUIRE( testRasterB.getCellValue(0, 0, 0) != testRasterB.getCellValue(0, 0, 0) );
}

TEST_CASE( "Raster layer indexing", "[raster]") {
    makeRaster(prec, REAL, REAL);
    prec.init(72, 72, 240, 0.04, 0.04, 1.0, 110.0, 1.5, 1);
    prec.setAllCellValues(2.0);
    
    // prec.read("subset.tif");

    std::string scriptA =  R"(
        uint season_idx[] = {10,11,12,13,14};
        uint season_count = 19;
        prec_NDJFM = 0.0;

        for (uint i = 0; i < season_count; i++) {
            REAL value = 0.0;
            for (uint j = 0; j < 5; j++) {
                uint idx = season_idx[j];
                value += prec[idx + i * 12];
            }
            prec_NDJFM += value / season_count;
        }
    )";

    auto rasterdim = prec.getRasterDimensions();

    makeRaster(prec_NDJFM, REAL, REAL);
    prec_NDJFM.init2D(rasterdim.d.nx, rasterdim.d.ny, rasterdim.d.hx,
                      rasterdim.d.hy, rasterdim.d.ox, rasterdim.d.oy);

    runScript<REAL, REAL>(scriptA, {prec_NDJFM, prec});
    
    // exception catch
    std::string scriptB =  R"(
        uint season_idx[] = {10,11,12,13,14};
        uint season_count = 19;
        prec_NDJFM = 0.0;

        for (uint i = 0; i < season_count; i++) {
            REAL value = 0.0;
            for (uint j = 0; j < 5; j++) {
                value += prec[season_idx[j] + i * 12];
            }
            prec_NDJFM += value / season_count;
        }
    )";
    
    REQUIRE_THROWS(runScript<REAL, REAL>(scriptB, {prec_NDJFM, prec}));
}

TEST_CASE( "Raster distance", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    Vector<REAL> v1;
    v1.addProperty("level");
    auto v1p1 = v1.addPoint( {  1,  1} );
    auto v1p2 = v1.addPoint( { -1,  1} );
    auto v1p3 = v1.addPoint( { -1, -1} );
    v1.setProperty(v1p1, "level", (uint32_t)1);
    v1.setProperty(v1p2, "level", (uint32_t)1);
    v1.setProperty(v1p3, "level", (uint32_t)1);
    v1.setProperty(v1p1, "radius", (REAL)0.5);
    v1.setProperty(v1p2, "radius", (REAL)0.5);
    v1.setProperty(v1p3, "radius", (REAL)0.5);

    Vector<REAL> v2;
    auto v2p1 = v2.addPoint( {  1,  1} );
    auto v2y1 = v2.addPolygon( { 
            { { -1+1.5, -1+1.5 }, { -1+1.5, 1+1.5 }, { 1+1.5, 1+1.5 }, { 1+1.5, -1+1.5 } },
            { { -0.5+1.5, -0.5+1.5 }, { -0.5+1.5, 0.5+1.5 }, { 0.5+1.5, 0.5+1.5 }, { 0.5+1.5, -0.5+1.5 } },
        } );
    auto v2y2 = v2.addPolygon( { 
            { { -1-1.5, -1-1.5 }, { -1-1.5, 1-1.5 }, { 1-1.5, 1-1.5 }, { 1-1.5, -1-1.5 } },
            { { -0.5-1.5, -0.5-1.5 }, { -0.5-1.5, 0.5-1.5 }, { 0.5-1.5, 0.5-1.5 }, { 0.5-1.5, -0.5-1.5 } },
        } );
    auto v2p2 = v2.addPoint( {  1, -1} );
    auto v2l1 = v2.addLineString( { { -1, -1 }, { 0, 0 }, { 1, 1 } } );
    auto v2p3 = v2.addPoint( { -1, -1} );
    v2.addProperty("level");
    v2.setProperty(v2p1, "level", (uint32_t)0);
    v2.setProperty(v2p2, "level", (uint32_t)0);
    v2.setProperty(v2p3, "level", (uint32_t)0);
    v2.setProperty(v2l1, "level", (uint32_t)2);
    v2.setProperty(v2y1, "level", (uint32_t)3);
    v2.setProperty(v2y2, "level", (uint32_t)3);
    v2.setProperty(v2p1, "radius", (REAL)0.0);
    v2.setProperty(v2p2, "radius", (REAL)0.25);
    v2.setProperty(v2p3, "radius", (REAL)0.5);
    v2.setProperty(v2l1, "radius", (REAL)0.5);

    REAL delta = 0.01;
    auto bounds = BoundingBox<REAL>( { {-3, -3}, {3, 3} });

    // Test distance map
    auto distance = v1.mapDistance<REAL>(delta, GeometryType::Point, bounds);
    REQUIRE( fabs(distance.getNearestValue( 0,  0)-sqrt(2.0)) < delta );
    REQUIRE( fabs(distance.getNearestValue( 1,  0)-1.0) < delta );
    REQUIRE( fabs(distance.getNearestValue( 0, -1)-1.0) < delta );
    REQUIRE( fabs(distance.getNearestValue( 1,  1)) < delta );

    // Test radius
    auto dim = distance.getRasterDimensions();
    makeRaster (distance2, REAL, REAL)
    distance2.init2D(dim.d.nx, dim.d.ny, dim.d.hx, dim.d.hy, dim.d.ox, dim.d.oy);
    distance2.mapVector(v1, GeometryType::Point, "radius");
    REQUIRE( fabs(distance2.getNearestValue( 0,  0)-(sqrt(2.0)-0.5)) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 1,  0)-0.5) < delta );
    REQUIRE( fabs(distance2.getNearestValue( 0, -1)-0.5) < delta );
    REQUIRE( fabs(distance2.getNearestValue(-1,  1)+0.5) < delta );

    // Test distance map on second layer
    makeRaster (distance3, REAL, REAL)
    distance3.init(dim.d.nx, dim.d.ny, 4, dim.d.hx, dim.d.hy, 1.0, dim.d.ox, dim.d.oy, 0.0);
    distance3.mapVector(v1, GeometryType::Point, "radius", "level");
    distance3.mapVector(v2, GeometryType::Point | GeometryType::LineString | GeometryType::Polygon, "radius", "level");
    REQUIRE( fabs(distance3.getNearestValue( 0,  0, 0)-(sqrt(2.0)-0.5)) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 1,  0, 0)-0.75) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 0, -1, 0)-0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 1, -1, 0)+0.25) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 0,  0, 1)-(sqrt(2.0)-0.5)) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 1,  0, 1)-0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 0, -1, 1)-0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue(-1,  1, 1)+0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 1, -1, 2)-(sqrt(2.0)-0.5)) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 1,  1, 2)+0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue( -1,  -1, 2)+0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 1.5,  1.5, 3)-0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue( -1.5,  -1.5, 3)-0.5) < delta );
    REQUIRE( fabs(distance3.getNearestValue( 2.25, 1.5, 3)+0.25) < delta );
    REQUIRE( fabs(distance3.getNearestValue( -2.25, -1.5, 3)+0.25) < delta );

    // Test radial basis map
    auto rb = v1.mapDistance<REAL>(delta,
        "REAL rb = exp(-2.0*dot(d, d)); output = isValid_REAL(output) ? output+rb : rb;", GeometryType::Point, bounds);
    REQUIRE( fabs(rb.getNearestValue( 0,  0)-3.0*exp(-4.0)) < delta );
    REQUIRE( fabs(rb.getNearestValue( 1,  0)-exp(-2.0)) < delta );
    REQUIRE( fabs(rb.getNearestValue( 0, -1)-exp(-2.0)) < delta );
    REQUIRE( fabs(rb.getNearestValue( 1,  1)-1.0) < delta );

}

TEST_CASE( "Raster vectorisation", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    Solver &solver = Solver::getSolver();
    solver.setVerbose(true);
    solver.getContext();
    solver.getQueue();
        
    // Create input vector
    Vector<REAL> vector_in;
    vector_in.addPoint( {  0,  0} );
    REAL delta = 0.01;
    auto bounds = BoundingBox<REAL>( { {-2, -2}, {2, 2} });
    auto distance = vector_in.mapDistance<REAL>(delta, GeometryType::Point, bounds);

    // Vectorise
    auto vector_out = distance.vectorise( {1} , 2);

    // Check bounds
    auto bMin = vector_out.getBounds().min;
    auto bMax = vector_out.getBounds().max;

    REQUIRE( fabs(bMin.p+1.0) < 1.0E-3 );
    REQUIRE( fabs(bMin.q+1.0) < 1.0E-3 );
    REQUIRE( bMin.r == 0.0 );
    REQUIRE( bMin.s == 0.0 );

    REQUIRE( fabs(bMax.p-1.0) < 1.0E-3 );
    REQUIRE( fabs(bMax.q-1.0) < 1.0E-3 );
    REQUIRE( bMax.r == 0.0 );
    REQUIRE( bMax.s == 0.0 );
}

#if ! __APPLE__
TEST_CASE( "Raster indexing", "[raster]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif
         
    // Create raster
    makeRaster(r, REAL, REAL)
    r.init2D(300, 300, 1, 1);
    
    // Create points
    auto v = Vector<REAL>();
    auto id0 = v.addPoint( { (REAL)100, (REAL)100 } );
    auto id1 = v.addPoint( { (REAL)200, (REAL)200 } );
    auto id2 = v.addPoint( { (REAL)100, (REAL)200 } );

    // Map distance
    r.mapVector(v, "");

    // Index connected components
    auto index0 = r.indexComponents("r < 10.0");
    REQUIRE( index0.hasData() == true );
    REQUIRE( index0.getProperty<uint32_t>("componentCount") == 3 );

    auto index1 = r.indexComponents("r < 50.0");
    REQUIRE( index1.hasData() == true );
    REQUIRE( index1.getProperty<uint32_t>("componentCount") == 1 );
    
    auto v1 = r.indexComponentsVector("r < 10.0");
    REQUIRE( v1.hasData() == true );
    REQUIRE( v1.getPointCount() == 3 );
    
    auto v2 = r.indexComponentsVector("r < 50.0");
    REQUIRE( v2.hasData() == true );
    REQUIRE( v2.getPointCount() == 1 );
}
#endif

TEST_CASE( "Vector initialisation", "[vector]" ) {

    // Create vector data
    Vector<REAL> vector;
    Coordinate<REAL> c = { (REAL)144.9631, (REAL)-37.8136 };
    auto newPointIndex = vector.addPoint(c);
    auto newLineStringIndex = vector.addLineString( { { (REAL)144.9631, (REAL)-37.8136 }, { (REAL)144.9731, (REAL)-37.8236 } } );
    auto newPolygonIndex = vector.addPolygon( { { { (REAL)144.9631, (REAL)-37.8136 }, { (REAL)144.9731, (REAL)-37.8236 }, { (REAL)144.9531, (REAL)-37.7936 } } } );
    
    vector.setProperty(newPointIndex, "new_string", std::string("newstr"));
    vector.setProperty(newPointIndex, "new_int", (int)11);
    vector.setProperty(newPointIndex, "new_REAL", (REAL)22.2);
    vector.setProperty(newPointIndex, "new_index", (cl_uint)33);
    vector.setProperty(newPointIndex, "new_byte", (cl_uchar)44);
    
    vector.setProperty(newLineStringIndex, "new_string", std::string("newstr"));
    vector.setProperty(newLineStringIndex, "new_int", (int)11);
    vector.setProperty(newLineStringIndex, "new_REAL", (REAL)22.2);
    vector.setProperty(newLineStringIndex, "new_index", (cl_uint)33);
    vector.setProperty(newLineStringIndex, "new_byte", (cl_uchar)44);
    
    vector.setProperty(newPolygonIndex, "new_string", std::string("newstr"));
    vector.setProperty(newPolygonIndex, "new_int", (int)11);
    vector.setProperty(newPolygonIndex, "new_REAL", (REAL)22.2);
    vector.setProperty(newPolygonIndex, "new_index", (cl_uint)33);
    vector.setProperty(newPolygonIndex, "new_byte", (cl_uchar)44);

    // Test values
    REQUIRE( vector.getPointCoordinate(newPointIndex) == c );
    REQUIRE( vector.template getProperty<std::string>(newPointIndex, "new_string") == "newstr" );
    REQUIRE( vector.template getProperty<int>(newPointIndex, "new_int") == 11 );
    REQUIRE( vector.template getProperty<REAL>(newPointIndex, "new_REAL") == (REAL)22.2 );
    REQUIRE( vector.template getProperty<cl_uint>(newPointIndex, "new_index") == (cl_uint)33 );
    REQUIRE( vector.template getProperty<cl_uchar>(newPointIndex, "new_byte") == (cl_uchar)44 );

    REQUIRE( vector.template getProperty<std::string>(newLineStringIndex, "new_string") == "newstr" );
    REQUIRE( vector.template getProperty<int>(newLineStringIndex, "new_int") == 11 );
    REQUIRE( vector.template getProperty<REAL>(newLineStringIndex, "new_REAL") == (REAL)22.2 );
    REQUIRE( vector.template getProperty<cl_uint>(newLineStringIndex, "new_index") == (cl_uint)33 );
    REQUIRE( vector.template getProperty<cl_uchar>(newLineStringIndex, "new_byte") == (cl_uchar)44 );

    REQUIRE( vector.template getProperty<std::string>(newPolygonIndex, "new_string") == "newstr" );
    REQUIRE( vector.template getProperty<int>(newPolygonIndex, "new_int") == 11 );
    REQUIRE( vector.template getProperty<REAL>(newPolygonIndex, "new_REAL") == (REAL)22.2 );
    REQUIRE( vector.template getProperty<cl_uint>(newPolygonIndex, "new_index") == (cl_uint)33 );
    REQUIRE( vector.template getProperty<cl_uchar>(newPolygonIndex, "new_byte") == (cl_uchar)44 );

    // Clone geometry
    auto clonePointIndex = vector.clone(newPointIndex);
    auto cloneLineStringIndex = vector.clone(newLineStringIndex);
    auto clonePolygonIndex = vector.clone(newPolygonIndex);

    // Test values
    REQUIRE( vector.getPointCoordinate(clonePointIndex) == c );
    REQUIRE( vector.template getProperty<std::string>(clonePointIndex, "new_string") == "newstr" );
    REQUIRE( vector.template getProperty<int>(clonePointIndex, "new_int") == 11 );
    REQUIRE( vector.template getProperty<REAL>(clonePointIndex, "new_REAL") == (REAL)22.2 );
    REQUIRE( vector.template getProperty<cl_uint>(clonePointIndex, "new_index") == (cl_uint)33 );
    REQUIRE( vector.template getProperty<cl_uchar>(clonePointIndex, "new_byte") == (cl_uchar)44 );
    
    REQUIRE( vector.template getProperty<std::string>(newLineStringIndex, "new_string") == "newstr" );
    REQUIRE( vector.template getProperty<int>(newLineStringIndex, "new_int") == 11 );
    REQUIRE( vector.template getProperty<REAL>(newLineStringIndex, "new_REAL") == (REAL)22.2 );
    REQUIRE( vector.template getProperty<cl_uint>(newLineStringIndex, "new_index") == (cl_uint)33 );
    REQUIRE( vector.template getProperty<cl_uchar>(newLineStringIndex, "new_byte") == (cl_uchar)44 );
    
    REQUIRE( vector.template getProperty<std::string>(newPolygonIndex, "new_string") == "newstr" );
    REQUIRE( vector.template getProperty<int>(newPolygonIndex, "new_int") == 11 );
    REQUIRE( vector.template getProperty<REAL>(newPolygonIndex, "new_REAL") == (REAL)22.2 );
    REQUIRE( vector.template getProperty<cl_uint>(newPolygonIndex, "new_index") == (cl_uint)33 );
    REQUIRE( vector.template getProperty<cl_uchar>(newPolygonIndex, "new_byte") == (cl_uchar)44 );
}

TEST_CASE( "Vector subregion point", "[vector]" ) {

    // Create GeoJSON string
    std::string inputGeoJSON = R"({"features": [
        {"geometry": {"coordinates": [143, -36], "type": "Point"}, "properties": {}, "type": "Feature"},
        {"geometry": {"coordinates": [144, -37, 1], "type": "Point"}, "properties": {}, "type": "Feature"},
        {"geometry": {"coordinates": [145, -38, 1], "type": "Point"}, "properties": {"time": 10}, "type": "Feature"},
        {"geometry": {"coordinates": [146, -39, 2], "type": "Point"}, "properties": {"time": 10}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto vector = GeoJson<REAL>::geoJsonToVector(inputGeoJSON);

    // Test p, q slice
    auto r0 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0}, {(REAL)143, (REAL)-36, (REAL)0}});
    auto &p0 = r0.getPointIndexes();
    REQUIRE( p0.size() == 1 );
    REQUIRE( r0.getPointCoordinate(p0[0]) == Coordinate<REAL>((REAL)143, (REAL)-36, (REAL)0) );

    // Test r slice
    auto r1 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0.5}, {(REAL)146, (REAL)-39, (REAL)1.5}});
    auto &p1 = r1.getPointIndexes();
    REQUIRE( p1.size() == 1 );
    REQUIRE( r1.getPointCoordinate(p1[0]) == Coordinate<REAL>((REAL)144, (REAL)-37, (REAL)1, (REAL)0) );

    // Test s slice
    auto r2 = vector.region({ {(REAL)143, (REAL)-36, (REAL)0, (REAL)10}, {(REAL)146, (REAL)-39, (REAL)2, (REAL)10}});
    auto &p2 = r2.getPointIndexes();
    REQUIRE( p2.size() == 2 );
    REQUIRE( r2.getPointCoordinate(p2[0]) == Coordinate<REAL>((REAL)145, (REAL)-38, (REAL)1, (REAL)10) );
    REQUIRE( r2.getPointCoordinate(p2[1]) == Coordinate<REAL>((REAL)146, (REAL)-39, (REAL)2, (REAL)10) );
}

TEST_CASE( "Vector subregion polygon", "[vector]" ) {

    int id;

    // Create GeoJSON string
    std::string inputGeoJSON = R"({
        "type": "FeatureCollection",
        "bbox": [ -0.427340686321259, -0.452744781970978, 0.358994424343109, 0.196392238140106 ],                                          
        "features": [
        { "type": "Feature", "properties": { "id": 1 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.103895604610443, -0.106009542942047 ], [ 0.024032771587372, -0.416883587837219 ], [ -0.427340686321259, -0.081559956073761 ], [ 0.103895604610443, -0.106009542942047 ] ] ] } },
        { "type": "Feature", "properties": { "id": 2 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -0.248120009899139, 0.084039270877838 ], [ 0.103895604610443, -0.106009542942047 ], [ -0.427340686321259, -0.081559956073761 ], [ -0.248120009899139, 0.084039270877838 ] ] ] } },
        { "type": "Feature", "properties": { "id": 3 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.103895604610443, -0.106009542942047 ], [ 0.047567307949066, 0.196392238140106 ], [ 0.297955214977264, 0.172094762325287 ], [ 0.103895604610443, -0.106009542942047 ] ] ] } },
        { "type": "Feature", "properties": { "id": 4 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.134359300136566, -0.118756055831909 ], [ 0.103895604610443, -0.106009542942047 ], [ 0.297955214977264, 0.172094762325287 ], [ 0.134359300136566, -0.118756055831909 ] ] ] } },
        { "type": "Feature", "properties": { "id": 5 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.358994424343109, -0.114117622375488 ], [ 0.134359300136566, -0.118756055831909 ], [ 0.297955214977264, 0.172094762325287 ], [ 0.358994424343109, -0.114117622375488 ] ] ] } },
        { "type": "Feature", "properties": { "id": 6 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.157473385334015, -0.186499178409576 ], [ 0.091192901134491, -0.362354099750519 ], [ 0.103895604610443, -0.106009542942047 ], [ 0.157473385334015, -0.186499178409576 ] ] ] } },
        { "type": "Feature", "properties": { "id": 7 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.134359300136566, -0.118756055831909 ], [ 0.157473385334015, -0.186499178409576 ], [ 0.103895604610443, -0.106009542942047 ], [ 0.134359300136566, -0.118756055831909 ] ] ] } },
        { "type": "Feature", "properties": { "id": 8 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ -0.248120009899139, 0.084039270877838 ], [ 0.047567307949066, 0.196392238140106 ], [ 0.103895604610443, -0.106009542942047 ], [ -0.248120009899139, 0.084039270877838 ] ] ] } },
        { "type": "Feature", "properties": { "id": 9 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.091192901134491, -0.362354099750519 ], [ 0.024032771587372, -0.416883587837219 ], [ 0.103895604610443, -0.106009542942047 ], [ 0.091192901134491, -0.362354099750519 ] ] ] } },
        { "type": "Feature", "properties": { "id": 10 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.134359300136566, -0.118756055831909 ], [ 0.358994424343109, -0.114117622375488 ], [ 0.344905078411102, -0.127646267414093 ], [ 0.134359300136566, -0.118756055831909 ] ] ] } },
        { "type": "Feature", "properties": { "id": 11 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.157473385334015, -0.186499178409576 ], [ 0.134359300136566, -0.118756055831909 ], [ 0.344905078411102, -0.127646267414093 ], [ 0.157473385334015, -0.186499178409576 ] ] ] } },
        { "type": "Feature", "properties": { "id": 12 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.301843702793121, -0.23103803396225 ], [ 0.157473385334015, -0.186499178409576 ], [ 0.344905078411102, -0.127646267414093 ], [ 0.301843702793121, -0.23103803396225 ] ] ] } },
        { "type": "Feature", "properties": { "id": 13 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.138354003429413, -0.38995236158371 ], [ 0.091192901134491, -0.362354099750519 ], [ 0.156435906887054, -0.296441614627838 ], [ 0.138354003429413, -0.38995236158371 ] ] ] } },
        { "type": "Feature", "properties": { "id": 14 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.192281067371368, -0.361442387104034 ], [ 0.138354003429413, -0.38995236158371 ], [ 0.156435906887054, -0.296441614627838 ], [ 0.192281067371368, -0.361442387104034 ] ] ] } },
        { "type": "Feature", "properties": { "id": 15 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.301843702793121, -0.23103803396225 ], [ 0.192281067371368, -0.361442387104034 ], [ 0.156435906887054, -0.296441614627838 ], [ 0.301843702793121, -0.23103803396225 ] ] ] } },
        { "type": "Feature", "properties": { "id": 16 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.157473385334015, -0.186499178409576 ], [ 0.301843702793121, -0.23103803396225 ], [ 0.156435906887054, -0.296441614627838 ], [ 0.157473385334015, -0.186499178409576 ] ] ] } },
        { "type": "Feature", "properties": { "id": 17 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.091192901134491, -0.362354099750519 ], [ 0.157473385334015, -0.186499178409576 ], [ 0.156435906887054, -0.296441614627838 ], [ 0.091192901134491, -0.362354099750519 ] ] ] } },
        { "type": "Feature", "properties": { "id": 18 }, "geometry": { "type": "Polygon", "coordinates": [ [ [ 0.357247769832611, -0.452744781970978 ], [ 0.192281067371368, -0.361442387104034 ], [ 0.301843702793121, -0.23103803396225 ], [ 0.357247769832611, -0.452744781970978 ] ] ] } }
        ]})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(inputGeoJSON);
    
    auto vr1 = v.region(BoundingBox<REAL>( { 0.17958, -0.08676 }, { 0.23961,-0.13215 } ));
    auto pv1 = vr1.getPolygonIndexes();
    REQUIRE(pv1.size() == 3);
        
    id = vr1.getProperty<int>(pv1[0], "id");
    REQUIRE(id == 5);
    id = vr1.getProperty<int>(pv1[1], "id");
    REQUIRE(id == 10);
    id = vr1.getProperty<int>(pv1[2], "id");
    REQUIRE(id == 11);

    auto vr2 = v.region(BoundingBox<REAL>( { 0.09703, -0.35874 }, { 0.15121, -0.38674 } ));
    auto pv2 = vr2.getPolygonIndexes();
    REQUIRE(pv2.size() == 2);
        
    id = vr2.getProperty<int>(pv2[0], "id");
    REQUIRE(id == 13);
    id = vr2.getProperty<int>(pv2[1], "id");
    REQUIRE(id == 14);
        
    auto vr3 = v.region(BoundingBox<REAL>( { -0.11985, -0.14789 }, { -0.10814, -0.16253 } ));
    auto pv3 = vr3.getPolygonIndexes();
    REQUIRE(pv3.size() == 1);

    id = vr3.getProperty<int>(pv3[0], "id");
    REQUIRE(id == 1);

    auto vr4 = v.region(BoundingBox<REAL>( { -0.76191, 0.54761 }, { 0.87215, -0.89317 } ));
    auto pv4 = vr4.getPolygonIndexes();
    REQUIRE(pv4.size() == 18);
}

TEST_CASE( "Vector rasterisation", "[vector]" ) {
        
    // Create GeoJSON string
    std::string json = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"A": 1, "level": 0}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 2, "level": 1}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 3, "level": 2}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 4, "level": 3}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 5, "level": 4}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json);

    // No script, default to mask of 1
    auto testRasterA = v.rasterise<REAL>(0.02);
    REQUIRE( testRasterA.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(25, 25) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(12, 12) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(45, 46) == (REAL)1.0 );
    REQUIRE( testRasterA.getCellValue(70, 51) == (REAL)1.0 );

    // No processing specified, A is order-specific
    auto testRasterB = v.rasterise<REAL>(0.02, "output = A;");
    REQUIRE( testRasterB.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterB.getCellValue(25, 25) == (REAL)5.0 );
    REQUIRE( testRasterB.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterB.getCellValue(45, 45) == (REAL)5.0 );
    REQUIRE( testRasterB.getCellValue(70, 51) == (REAL)5.0 );

    // Minimum specified
    auto testRasterC = v.rasterise<REAL>(0.02, "output = min(A, output);");
    REQUIRE( testRasterC.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterC.getCellValue(25, 25) == (REAL)4.0 );
    REQUIRE( testRasterC.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterC.getCellValue(45, 45) == (REAL)3.0 );
    REQUIRE( testRasterC.getCellValue(70, 51) == (REAL)4.0 );

    // Maximum specified
    auto testRasterD = v.rasterise<REAL>(0.02, "output = max(A, output);");
    REQUIRE( testRasterD.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterD.getCellValue(25, 25) == (REAL)5.0 );
    REQUIRE( testRasterD.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterD.getCellValue(45, 45) == (REAL)5.0 );
    REQUIRE( testRasterD.getCellValue(70, 51) == (REAL)5.0 );
    
    // Edge specified
    auto testRasterE = v.rasterise<REAL>(0.02, "output = A;", GeometryType::All | VectorIndexingOptions::Edges);
    REQUIRE( testRasterE.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterE.getCellValue(25, 25) == (REAL)5.0 );
    REQUIRE( testRasterE.getCellValue(12, 12) == (REAL)2.0 );
    REQUIRE( testRasterE.getCellValue(45, 45) == (REAL)2.0 );
    REQUIRE( isInvalid<REAL>(testRasterE.getCellValue(70, 51)) );

    // Interior specified
    auto testRasterF = v.rasterise<REAL>(0.02, "output = A;", GeometryType::All | VectorIndexingOptions::Interior);
    REQUIRE( testRasterF.getCellValue(1, 76) == (REAL)1.0 );
    REQUIRE( testRasterF.getCellValue(25, 25) == (REAL)2.0 );
    REQUIRE( testRasterF.getCellValue(12, 12) == (REAL)3.0 );
    REQUIRE( testRasterF.getCellValue(45, 45) == (REAL)5.0 );
    REQUIRE( testRasterD.getCellValue(70, 51) == (REAL)5.0 );
        
    // Level test
    auto dims = testRasterA.getRasterDimensions().d;
    dims.nz = 5;

    makeRaster(testRasterG, REAL, REAL)
    testRasterG.setProjectionParameters(testRasterA.getProjectionParameters());
    testRasterG.init(dims);
    testRasterG.rasterise(v, "output = A;");
    REQUIRE( testRasterG.getCellValue(1, 76, 0) == (REAL)1.0 );
    REQUIRE( testRasterG.getCellValue(25, 25, 1) == (REAL)5.0 );
    REQUIRE( testRasterG.getCellValue(12, 12, 2) == (REAL)3.0 );
    REQUIRE( testRasterG.getCellValue(45, 45, 3) == (REAL)5.0 );
    REQUIRE( testRasterG.getCellValue(70, 51, 4) == (REAL)5.0 );

    makeRaster(testRasterH, REAL, REAL)
    testRasterH.setProjectionParameters(testRasterA.getProjectionParameters());
    testRasterH.init(dims);
    testRasterH.rasterise(v, "output = A;", GeometryType::All, "level");
    REQUIRE( testRasterH.getCellValue(1, 76, 0) == (REAL)1.0 );
    REQUIRE( testRasterH.getCellValue(25, 25, 1) == (REAL)2.0 );
    REQUIRE( testRasterH.getCellValue(12, 12, 2) == (REAL)3.0 );
    REQUIRE( testRasterH.getCellValue(45, 46, 3) == (REAL)4.0 );
    REQUIRE( testRasterH.getCellValue(45, 46, 4) == (REAL)5.0 );
}

TEST_CASE( "Vector pointSample", "[vector]") {
    // Create GeoJSON string
    std::string json = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"A": 1, "level": 0}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 2, "level": 1}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 3, "level": 2}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 4, "level": 3}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 5, "level": 4}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json);

    // No script, default to mask of 1
    auto testRasterA = v.rasterise<REAL>(0.02, "output = A;", GeometryType::All);
    auto rasterName = testRasterA.getProperty<std::string>("name");    
    
    auto testVectorA = testRasterA.vectorise({ 2.5, 3.5}, 2);   
    auto pointVectorA = testVectorA.convert(GeometryType::Point);
    
    REQUIRE ( pointVectorA.getPointCount() > 0 );
    
    pointVectorA.pointSample(testRasterA);
    
    REQUIRE( pointVectorA.hasData() );
    REQUIRE( pointVectorA.hasProperty(rasterName) );
}

TEST_CASE( "Vector script", "[vector]") {

    // Create GeoJSON string
    std::string json = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"C": 10}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"C": 20}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"C": 30}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"C": 40}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"C": 50}, "type": "Feature"},
        {"geometry": {"coordinates": [2, 0.75], "type": "Point"},
            "properties": {"C": 60}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json);
    v.setProjectionParameters(ProjectionParameters<double>());
    v.template setProperty<REAL>(0, "A", (REAL)1.0);
    v.template setProperty<REAL>(1, "A", (REAL)1.0);
    v.template setPropertyVector<std::vector<REAL> >("B", { 99.0, 99.0, 0.0, 0.0, 0.0, 0.0 } );

    runVectorScript<REAL, REAL>("A += B-C;", v);
    REQUIRE(v.template getProperty<REAL>(0, "A") == (REAL)90.0);
    REQUIRE(v.template getProperty<REAL>(1, "A") == (REAL)80.0);
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(2, "A")));
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(3, "A")));
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(4, "A")));
    REQUIRE(isInvalid<REAL>(v.template getProperty<REAL>(5, "A")));

    REAL delta = 0.02;
    REAL delta2 = delta/2.0;

    makeRaster(r, REAL, REAL)
    r.init2D(150, 150, delta, delta);
    runScriptNoOut<REAL>("r = x;", { r } );
    r.setVariableData<REAL>("var", 11);

    makeRaster(s, REAL, REAL)
    s.init2D(150, 150, delta, delta);
    runScriptNoOut<REAL>("s = 2;", { s } );
    s.setVariableData<REAL>("var", 22);

    runVectorScript<REAL, REAL>("A = r::var;", v, { r }, Reduction::Maximum);
    REQUIRE(v.template getProperty<REAL>(0, "A") == (11));
    runVectorScript<REAL, REAL>("A = r::var+s::var;", v, { r, s }, Reduction::Maximum);
    REQUIRE(v.template getProperty<REAL>(0, "A") == (11+22));


    runVectorScript<REAL, REAL>("A = r; B += 1.0;", v, { r }, Reduction::Maximum);
    REAL A2_max = v.template getProperty<REAL>(2, "A");
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - (3.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - (1.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - (1.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - (1.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 100.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r; B += 1.0;", v, { r }, Reduction::Minimum);
    REAL A2_min = v.template getProperty<REAL>(2, "A");
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - (0.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - (0.5-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 101.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r;", v, { r }, Reduction::Count);
    REQUIRE(v.template getProperty<int>(0, "A") == 1);
    REQUIRE(v.template getProperty<int>(1, "A") == 150);
    //REQUIRE(v.template getProperty<int>(2, "A") == 1875);
    REQUIRE(v.template getProperty<int>(3, "A") == 2704);
    REQUIRE(v.template getProperty<int>(4, "A") == 2704);
    REQUIRE(v.template getProperty<int>(5, "A") == 1);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r; B += 1.0;", v, { r }, Reduction::Mean);
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - (1.5)) < 1.0E-3);
    //REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - (0.5)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - (1.0)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - (1.0)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 102.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = s; B += 1.0;", v, { s }, Reduction::Sum);
    REQUIRE(v.template getProperty<int>(0, "A") == 2);
    REQUIRE(v.template getProperty<int>(1, "A") == 300);
    //REQUIRE(v.template getProperty<int>(2, "A") == 3750);
    REQUIRE(v.template getProperty<int>(3, "A") == 5408);
    REQUIRE(v.template getProperty<int>(4, "A") == 5408);
    REQUIRE(v.template getProperty<int>(5, "A") == 2);
    REQUIRE(v.template getProperty<REAL>(0, "B") == 103.0);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 10.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 60.0);

    runVectorScript<REAL, REAL>("A = r*s; B = r; C += 10.0;", v, { r, s }, Reduction::Maximum);
    REQUIRE(fabs(v.template getProperty<REAL>(0, "A") - 2.0*(0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "A") - 2.0*(3.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "A") - 2.0*(1.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "A") - 2.0*(1.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "A") - 2.0*(1.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "A") - 2.0*(2.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(0, "B") - (0.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(1, "B") - (3.0-delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(2, "B") - (1.0+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(3, "B") - (1.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(4, "B") - (1.5+delta2)) < 1.0E-3);
    REQUIRE(fabs(v.template getProperty<REAL>(5, "B") - (2.0+delta2)) < 1.0E-3);
    REQUIRE(v.template getProperty<REAL>(0, "C") == 20.0);
    REQUIRE(v.template getProperty<REAL>(1, "C") == 30.0);
    REQUIRE(v.template getProperty<REAL>(2, "C") == 40.0);
    REQUIRE(v.template getProperty<REAL>(3, "C") == 50.0);
    REQUIRE(v.template getProperty<REAL>(4, "C") == 60.0);
    REQUIRE(v.template getProperty<REAL>(5, "C") == 70.0);

    v.addProperty("D");
    v.addProperty("E");
    runVectorScript<REAL, REAL>("D = r; E = s;", v, { r, s }, Reduction::None);
    {
    auto D2_vec = v.template getPropertyVectorRef<std::vector<REAL> >(2, "D");
    REQUIRE(D2_vec[0] == A2_min);
    REQUIRE(D2_vec[D2_vec.size()-1] == A2_max);

    auto E2_vec = v.template getPropertyVectorRef<std::vector<REAL> >(2, "E");
    REQUIRE(E2_vec[0] == (REAL)2);
    }
    v.removeProperty("D");
    REQUIRE( v.hasProperty("D") == false );
    v.removeProperty("E");
    REQUIRE( v.hasProperty("E") == false );

    v.removeProperty("A");
    v.removeProperty("B");
    v.addProperty("A");
    v.addProperty("B");
    runVectorScript<REAL, REAL>("A = r; B = s;", v, { r, s }, Reduction::None);
    {
    auto A2_vec = v.template getPropertyVectorRef<std::vector<REAL> >(2, "A");
    REQUIRE(A2_vec[0] == A2_min);
    REQUIRE(A2_vec[A2_vec.size()-1] == A2_max);

    auto B2_vec = v.template getPropertyVectorRef<std::vector<REAL> >(2, "B");
    REQUIRE(B2_vec[0] == (REAL)2);
    }

    runVectorScript<REAL, REAL>("A = s; B = r;", v, { r, s }, Reduction::None);
    {
    auto B2_vec = v.template getPropertyVectorRef<std::vector<REAL> >(2, "B");
    REQUIRE(B2_vec[0] == A2_min);
    REQUIRE(B2_vec[B2_vec.size()-1] == A2_max);

    auto A2_vec = v.template getPropertyVectorRef<std::vector<REAL> >(2, "A");
    REQUIRE(A2_vec[0] == (REAL)2);
    }
}

TEST_CASE( "Vector script 3D", "[vector]") {

        // Create 3D Raster
        makeRaster(r, REAL, REAL);
        r.init(5, 5, 5, 1, 1, 1);

        runScriptNoOut<REAL>("r = z;", { r });

        // Get cell centres
        auto v = r.cellCentres();
        v.addProperty("r_val_vec");
        v.addProperty("r_val_min");
        v.addProperty("r_val_max");
        v.addProperty("r_val_mean");
        v.addProperty("r_val_sum");
        v.addProperty("r_val_sum_squares");
        v.addProperty("r_count");

        // Run vector script over 3D Raster
        runVectorScript<REAL, REAL>("r_val_vec = r", v, { r });
        auto &pv = v.getPropertyVectorRef<std::vector<REAL> >(0, "r_val_vec");
        
        REQUIRE(pv.size() == 5);
        REQUIRE(pv[0] == (REAL)0.5);
        REQUIRE(pv[4] == (REAL)4.5);

        // Minimum reduction
        runVectorScript<REAL, REAL>("r_val_min = r", v, { r }, Reduction::Minimum);
        auto rv_min = v.getProperty<REAL>(0, "r_val_min");

        REQUIRE(rv_min == (REAL)0.5);

        // Maximum reduction
        runVectorScript<REAL, REAL>("r_val_max = r", v, { r }, Reduction::Maximum);
        auto rv_max = v.getProperty<REAL>(0, "r_val_max");

        REQUIRE(rv_max == (REAL)4.5);

        // Mean reduction
        runVectorScript<REAL, REAL>("r_val_mean = r", v, { r }, Reduction::Mean);
        auto rv_mean = v.getProperty<REAL>(0, "r_val_mean");

        REQUIRE(rv_mean == (REAL)2.5);

        // Sum reduction
        runVectorScript<REAL, REAL>("r_val_sum = r", v, { r }, Reduction::Sum);
        auto rv_sum = v.getProperty<REAL>(0, "r_val_sum");

        REQUIRE(rv_sum == (REAL)12.5);

        // Sum of squares reduction
        runVectorScript<REAL, REAL>("r_val_sum_squares = r", v, { r }, Reduction::SumSquares);
        auto rv_sum_squares = v.getProperty<REAL>(0, "r_val_sum_squares");

        REQUIRE(rv_sum_squares == (REAL)41.25);

        // Count reduction
        runVectorScript<REAL, REAL>("r_count = r", v, { r }, Reduction::Count);
        auto rv_count = v.getProperty<REAL>(0, "r_count");

        REQUIRE(rv_count == (REAL)5);

        // Create polygon
        auto v2 = Vector<REAL>();
        v2.addPolygon( { { { 0.0, 0.0 }, { 0.0, 5.0 }, { 5.0, 5.0 }, { 5.0, 0.0 } } } );
        v2.addProperty("r_val_vec");
        v2.addProperty("r_val_min");
        v2.addProperty("r_val_max");
        v2.addProperty("r_val_mean");
        v2.addProperty("r_val_sum");
        v2.addProperty("r_val_sum_squares");
        v2.addProperty("r_count");

        // Run vector script over 3D Raster
        runVectorScript<REAL, REAL>("r_val_vec = r", v2, { r });
        auto &pv2 = v2.getPropertyVectorRef<std::vector<REAL> >(0, "r_val_vec");

        REQUIRE(pv2.size() == 125);
        REQUIRE(pv2[0] == (REAL)0.5);
        REQUIRE(pv2[100] == (REAL)4.5);

        // Minimum reduction
        runVectorScript<REAL, REAL>("r_val_min = r", v2, { r }, Reduction::Minimum);
        auto rv2_min = v2.getProperty<REAL>(0, "r_val_min");

        REQUIRE(rv2_min == (REAL)0.5);

        // Maximum reduction
        runVectorScript<REAL, REAL>("r_val_max = r", v2, { r }, Reduction::Maximum);
        auto rv2_max = v2.getProperty<REAL>(0, "r_val_max");

        REQUIRE(rv2_max == (REAL)4.5);

        // Mean reduction
        runVectorScript<REAL, REAL>("r_val_mean = r", v2, { r }, Reduction::Mean);
        auto rv2_mean = v2.getProperty<REAL>(0, "r_val_mean");

        REQUIRE(rv2_mean == (REAL)2.5);

        // Sum reduction
        runVectorScript<REAL, REAL>("r_val_sum = r", v2, { r }, Reduction::Sum);
        auto rv2_sum = v2.getProperty<REAL>(0, "r_val_sum");

        REQUIRE(rv2_sum == (REAL)312.5);

        // Sum of squares reduction
        runVectorScript<REAL, REAL>("r_val_sum_squares = r", v2, { r }, Reduction::SumSquares);
        auto rv2_sum_squares = v2.getProperty<REAL>(0, "r_val_sum_squares");

        REQUIRE(rv2_sum_squares == (REAL)1031.25);

        // Count reduction
        runVectorScript<REAL, REAL>("r_count = r", v2, { r }, Reduction::Count);
        auto rv2_count = v2.getProperty<REAL>(0, "r_count");

        REQUIRE(rv2_count == (REAL)125);
}

TEST_CASE( "Vector script filter", "[vector]") {

    auto v1 = Vector<REAL>();
    for (int i = 0; i < 10; i++) {
        v1.setProperty(v1.addPoint( { (REAL)i, (REAL)i } ), "test", i);
    }
    auto v2 = v1;
    runVectorScript<REAL, REAL>("if (test < 5) keep = false;", v2);
    
    REQUIRE(v1.getPointIndexes().size() == 10);
    REQUIRE(v2.getPointIndexes().size() == 5);
    
    REQUIRE(v1.template getProperty<int>(v1.getPointIndexes()[0], "test") == 0);
    REQUIRE(v1.template getProperty<int>(v1.getPointIndexes()[9], "test") == 9);
    REQUIRE(v2.template getProperty<int>(v2.getPointIndexes()[0], "test") == 5);
    REQUIRE(v2.template getProperty<int>(v2.getPointIndexes()[4], "test") == 9);
}

TEST_CASE( "Vector script bounds", "[vector]") {

    auto v1 = Vector<REAL>();
    v1.addPoint( { 1.0, 2.0, 3.0, 4.0 } );
    for (int i = 0; i < 10; i++) {
        v1.addPoint( { (REAL)i, (REAL)i } );
    }
    v1.addPolygon( { { { 3.0, 1.0 }, { 3.0, 2.5 }, { 5.0, 2.5 }, { 5.0, 1.0 } } } );
    v1.addProperty("bx");
    v1.addProperty("by");
    v1.addProperty("bz");
    v1.addProperty("bt");
    
    // Min bound
    runVectorScript<REAL, REAL>("bx = boundsMin.p; by = boundsMin.q; bz = boundsMin.r; bt = boundsMin.s;", v1);
    
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "bx") == 1.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "by") == 2.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "bz") == 3.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "bt") == 4.0);

    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[1], "bx") == 0.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[1], "by") == 0.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[5], "bx") == 4.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[5], "by") == 4.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPolygonIndexes()[0], "bx") == 3.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPolygonIndexes()[0], "by") == 1.0);

    // Max bound
    runVectorScript<REAL, REAL>("bx = boundsMax.p; by = boundsMax.q; bz = boundsMax.r; bt = boundsMax.s;", v1);

    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "bx") == 1.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "by") == 2.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "bz") == 3.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[0], "bt") == 4.0);
    
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[1], "bx") == 0.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[1], "by") == 0.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[5], "bx") == 4.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPointIndexes()[5], "by") == 4.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPolygonIndexes()[0], "bx") == 5.0);
    REQUIRE(v1.template getProperty<REAL>(v1.getPolygonIndexes()[0], "by") == 2.5);
}

TEST_CASE( "Vector addition", "[vector]") {

    // Create GeoJSON string
    std::string json1 = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"A": 10}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 20}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 30}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 40}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 50}, "type": "Feature"},
        {"geometry": {"coordinates": [2, 0.75], "type": "Point"},
            "properties": {"A": 60}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    std::string json2 = R"({"features": [
        {"geometry": {"coordinates": [5, 6.5], "type": "Point"},
            "properties": {"B": 15}, "type": "Feature"},
        {"geometry": {"coordinates": [[5, 5], [6, 6], [7, 5], [8, 6]], "type": "LineString"},
            "properties": {"B": 25}, "type": "Feature"},
        {"geometry": {"coordinates": [[[5, 5], [6, 5], [6, 6], [5, 6], [5, 5]], [[5.25, 5.25], [5.25, 5.75], [5.75, 5.75], [5.75, 5.25], [5.25, 5.25]]], "type": "Polygon"},
            "properties": {"B": 35}, "type": "Feature"},
        {"geometry": {"coordinates": [[[5.5, 5.5], [6.5, 5.5], [6.5, 6.5], [5.5, 6.5], [5.5, 5.5]]], "type": "Polygon"},
            "properties": {"B": 45}, "type": "Feature"},
        {"geometry": {"coordinates": [[[5.5, 5.5], [6.5, 5.5], [6.5, 6.5], [5.5, 6.5], [5.5, 5.5]]], "type": "Polygon"},
            "properties": {"B": 55}, "type": "Feature"},
        {"geometry": {"coordinates": [7, 5.75], "type": "Point"},
            "properties": {"B": 65}, "type": "Feature"}
        ], "type": "FeatureCollection"})";

    // Parse GeoJSON string
    auto v1 = GeoJson<REAL>::geoJsonToVector(json1);
    auto v2 = GeoJson<REAL>::geoJsonToVector(json2);

    // Add vectors
    v1+=v2;

    // Test values
    REQUIRE(v1.template getProperty<int>( 0, "A") == 10);
    REQUIRE(v1.template getProperty<int>( 5, "A") == 60);
    REQUIRE(isInvalid<int>(v1.template getProperty<int>( 6, "A")));
    REQUIRE(isInvalid<int>(v1.template getProperty<int>(11, "A")));

    REQUIRE(isInvalid<int>(v1.template getProperty<int>( 0, "B")));
    REQUIRE(isInvalid<int>(v1.template getProperty<int>( 5, "B")));
    REQUIRE(v1.template getProperty<int>( 6, "B") == 15);
    REQUIRE(v1.template getProperty<int>(11, "B") == 65);

    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[0]) == Coordinate<REAL>({ 0, 1.5 }) );
    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[1]) == Coordinate<REAL>({ 2, 0.75 }) );
    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[2]) == Coordinate<REAL>({ 5, 6.5 }) );
    REQUIRE( v1.getPointCoordinate(v1.getPointIndexes()[3]) == Coordinate<REAL>({ 7, 5.75 }) );

}

TEST_CASE( "Geohash", "[vector]" ) {

    // Create vector data
    Coordinate<double> c( { 144.9631, -37.8136 } );
    std::string geoHash = c.getGeoHash();

    // Test values
    REQUIRE( geoHash == "r1r0fsnzv41c" );
}

TEST_CASE( "GeoJSON", "[vector]" ) {

    // Create GeoJSON string
    std::string inputGeoJSON = R"({"features": [
        {"geometry": {"coordinates": [0, 0.5], "type": "Point"},
            "properties": {"p0": "pstr", "p1": 1, "p2": 1.1000000000000001}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"l0": "lstr", "l1": 2, "l2": 2.2000000000000002}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"y0": "ystr", "y1": 3, "y2": 3.2999999999999998}, "type": "Feature"}], "type": "FeatureCollection", "bbox":[0,0,3,1]})";

    // Parse GeoJSON string
    auto vector = GeoJson<double>::geoJsonToVector(inputGeoJSON);

    // Convert Vector to GeoJSON
    std::string outputGeoJSON = GeoJson<double>::vectorToGeoJson(vector);

    // Test strings
    REQUIRE( Strings::removeWhitespace(outputGeoJSON) == Strings::removeWhitespace(inputGeoJSON) );

    // Test properties
    auto &pointIndexes = vector.getPointIndexes();
    auto &lineStringIndexes = vector.getLineStringIndexes();
    auto &polygonIndexes = vector.getPolygonIndexes();
    REQUIRE( vector.template getProperty<std::string>(pointIndexes[0], "p0") == "pstr" );
    REQUIRE( vector.template getProperty<int>(lineStringIndexes[0], "l1") == 2 );
    REQUIRE( vector.template getProperty<double>(polygonIndexes[0], "y2") == (double)3.2999999999999998 );
}

TEST_CASE( "GeoWKT", "[vector]" ) {

    // Create GeoWKT string
    std::string inputGeoWKT = R"(GEOMETRYCOLLECTION (POINT (0.000000 0.500000), LINESTRING (0.000000 0.000000, 1.000000 1.000000, 2.000000 0.000000, 3.000000 1.000000), POLYGON ((0.000000 0.000000, 1.000000 0.000000, 1.000000 1.000000, 0.000000 1.000000, 0.000000 0.000000), (0.250000 0.250000, 0.250000 0.750000, 0.750000 0.750000, 0.750000 0.250000, 0.250000 0.250000))))";

    // Convert Vector to GeoWKT
    auto v = GeoWKT<double>::geoWKTToVector(inputGeoWKT);
    std::string outputGeoWKT = GeoWKT<double>::vectorToGeoWKT(v);
    REQUIRE( inputGeoWKT == outputGeoWKT );

    auto v0 = Vector<double>();
    v0.addPoint( { 0.0, 0.0 } );
    std::string outputGeoWKTPoint = GeoWKT<double>::vectorToGeoWKT(v0);
    REQUIRE( outputGeoWKTPoint == std::string("POINT (0.000000 0.000000)") );

    auto v1 = Vector<double>();
    v1.addLineString( { { 0.0, 0.0 }, { 1.0, 1.0 } } );
    std::string outputGeoWKTLineString = GeoWKT<double>::vectorToGeoWKT(v1);
    REQUIRE( outputGeoWKTLineString == std::string("LINESTRING (0.000000 0.000000, 1.000000 1.000000)") );

    auto v2 = Vector<double>();
    v2.addPolygon( { { { 0.0, 0.0 }, { 1.0, 1.0 }, { 2.0, 2.0 } } } );
    std::string outputGeoWKTPolygon = GeoWKT<double>::vectorToGeoWKT(v2);
    REQUIRE( outputGeoWKTPolygon == std::string("POLYGON ((0.000000 0.000000, 1.000000 1.000000, 2.000000 2.000000, 0.000000 0.000000))") );

    auto v3 = Vector<double>();
    Coordinate<double> c0, c1;

    std::vector<cl_uint> ids;
    ids = GeoWKT<double>::parseString(v3, "POINT (0.0 0.5)");
    REQUIRE( ids.size() == 1 );
    REQUIRE( ids[0] == 0 );
    c0 = v3.getPointCoordinate(ids[0]);
    REQUIRE( c0 == Coordinate<double>( { 0.0, 0.5 } ) );

    ids = GeoWKT<double>::parseString(v3, "POINT (1.0 1.5)");
    REQUIRE( ids.size() == 1 );
    REQUIRE( ids[0] == 1 );
    c0 = v3.getPointCoordinate(ids[0]);
    REQUIRE( c0 == Coordinate<double>( { 1.0, 1.5 } ) );

    ids = GeoWKT<double>::parseStrings(v3, { "POINT (2.0 2.5)", "POINT (3.0 3.5)" } );
    REQUIRE( ids.size() == 2 );
    REQUIRE( ids[0] == 2 );
    REQUIRE( ids[1] == 3 );
    c0 = v3.getPointCoordinate(ids[0]);
    c1 = v3.getPointCoordinate(ids[1]);
    REQUIRE( c0 == Coordinate<double>( { 2.0, 2.5 } ) );
    REQUIRE( c1 == Coordinate<double>( { 3.0, 3.5 } ) );
}

TEST_CASE( "Vector projection from WKT1", "[projection]" ) {

    // Create projections
    std::string projWKT_EPSG4326 = R"(GEOGCS["WGS 84",DATUM["WGS_1984",SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]])";
    auto proj_EPSG4326 = Projection::parseWKT(projWKT_EPSG4326);

    std::string projWKT_EPSG3111 = R"(PROJCS["GDA94 / Vicgrid94",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Lambert_Conformal_Conic_2SP"],PARAMETER["standard_parallel_1",-36],PARAMETER["standard_parallel_2",-38],PARAMETER["latitude_of_origin",-37],PARAMETER["central_meridian",145],PARAMETER["false_easting",2500000],PARAMETER["false_northing",2500000],AUTHORITY["EPSG","3111"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG3111 = Projection::parseWKT(projWKT_EPSG3111);

    std::string projWKT_EPSG3577 = R"(PROJCS["GDA94 / Australian Albers",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Albers_Conic_Equal_Area"],PARAMETER["standard_parallel_1",-18],PARAMETER["standard_parallel_2",-36],PARAMETER["latitude_of_center",0],PARAMETER["longitude_of_center",132],PARAMETER["false_easting",0],PARAMETER["false_northing",0],AUTHORITY["EPSG","3577"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG3577 = Projection::parseWKT(projWKT_EPSG3577);

    std::string projWKT_EPSG28355 = R"(PROJCS["GDA94 / MGA zone 55",GEOGCS["GDA94",DATUM["Geocentric_Datum_of_Australia_1994",
        SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6283"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.01745329251994328,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],UNIT["metre",1,AUTHORITY["EPSG","9001"]],
        PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",147],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",500000],PARAMETER["false_northing",10000000],AUTHORITY["EPSG","28355"],AXIS["Easting",EAST],AXIS["Northing",NORTH]])";
    auto proj_EPSG28355 = Projection::parseWKT(projWKT_EPSG28355);

    std::string projWKT_EPSG3832 = R"(PROJCS["WGS 84 / PDC Mercator",GEOGCS["WGS 84",DATUM["World Geodetic System 1984",
        SPHEROID["WGS 84",6378137.0,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0.0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.017453292519943295],AXIS["Geodetic latitude",NORTH],AXIS["Geodetic longitude",EAST],AUTHORITY["EPSG","4326"]],
        PROJECTION["Mercator_1SP"],PARAMETER["latitude_of_origin",0.0],PARAMETER["central_meridian",150.0],PARAMETER["scale_factor",1.0],PARAMETER["false_easting",0.0],PARAMETER["false_northing",0.0],UNIT["m",1.0],AXIS["Easting",EAST],AXIS["Northing",NORTH],AUTHORITY["EPSG","3832"]])";
    auto proj_EPSG3832 = Projection::parseWKT(projWKT_EPSG3832);

    std::string projWKT_EPSG3857 = R"(PROJCS["WGS 84 / Pseudo-Mercator",GEOGCS["WGS 84",DATUM["WGS_1984",
        SPHEROID["WGS 84",6378137,298.257223563,AUTHORITY["EPSG","7030"]],AUTHORITY["EPSG","6326"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4326"]],
        PROJECTION["Mercator_1SP"],PARAMETER["central_meridian",0],PARAMETER["scale_factor",1],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["X",EAST],AXIS["Y",NORTH],
        EXTENSION["PROJ4","+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs"],AUTHORITY["EPSG","3857"]])";
    auto proj_EPSG3857 = Projection::parseWKT(projWKT_EPSG3857);

    // Test coordinate
    Coordinate<double> c_Elliptical = { 144.9631, -37.8136 };

    // Test Lambert conformal conic
    Coordinate<double> c_Lambert = c_Elliptical;
    Coordinate<double> c_Lambert_Target = { 2496750.96316546, 2409712.42995386 };
    Projection::convert(c_Lambert, proj_EPSG3111, proj_EPSG4326);
    double delta_Lambert = std::hypot(c_Lambert.p-c_Lambert_Target.p, c_Lambert.q-c_Lambert_Target.q);
    REQUIRE( delta_Lambert < 1.0E-3 );

    Projection::convert(c_Lambert, proj_EPSG4326, proj_EPSG3111);
    delta_Lambert = std::hypot(c_Lambert.p-c_Elliptical.p, c_Lambert.q-c_Elliptical.q);
    REQUIRE( delta_Lambert < 1.0E-6 );

    // Test Albers conic
    Coordinate<double> c_Albers = c_Elliptical;
    Coordinate<double> c_Albers_Target = { 1146469.071166, -4192119.31810244 };
    Projection::convert(c_Albers, proj_EPSG3577, proj_EPSG4326);
    double delta_Albers = std::hypot(c_Albers.p-c_Albers_Target.p, c_Albers.q-c_Albers_Target.q);
    REQUIRE( delta_Albers < 1.0E-3 );

    Projection::convert(c_Albers, proj_EPSG4326, proj_EPSG3577);
    delta_Albers = std::hypot(c_Albers.p-c_Elliptical.p, c_Albers.q-c_Elliptical.q);
    REQUIRE( delta_Albers < 1.0E-6 );

    // Test Transverse Mercator
    Coordinate<double> c_TM = c_Elliptical;
    Coordinate<double> c_TM_Target = { 320704.446321103, 5812911.69953155 };
    Projection::convert(c_TM, proj_EPSG28355, proj_EPSG4326);
    double delta_TM = std::hypot(c_TM.p-c_TM_Target.p, c_TM.q-c_TM_Target.q);
    REQUIRE( delta_TM < 1.0E-3 );

    Projection::convert(c_TM, proj_EPSG4326, proj_EPSG28355);
    delta_TM = std::hypot(c_TM.p-c_Elliptical.p, c_TM.q-c_Elliptical.q);
    REQUIRE( delta_TM < 1.0E-6 );

    // Test Mercator
    Coordinate<double> c_Mercator = c_Elliptical;
    Coordinate<double> c_Mercator_Target = { -560705.14, -4526927.40 };
    Projection::convert(c_Mercator, proj_EPSG3832, proj_EPSG4326);
    double delta_Mercator = std::hypot(c_Mercator.p-c_Mercator_Target.p, c_Mercator.q-c_Mercator_Target.q);
    REQUIRE( delta_Mercator < 1.0E-2 );

    Projection::convert(c_Mercator, proj_EPSG4326, proj_EPSG3832);
    delta_Mercator = std::hypot(c_Mercator.p-c_Elliptical.p, c_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Mercator < 1.0E-6 );

    // Test Web Mercator
    Coordinate<double> c_Web_Mercator = c_Elliptical;
    Coordinate<double> c_Web_Mercator_Target = { 16137218.48, -4553127.11 };
    Projection::convert(c_Web_Mercator, proj_EPSG3857, proj_EPSG4326);
    double delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Web_Mercator_Target.p, c_Web_Mercator.q-c_Web_Mercator_Target.q);
    REQUIRE( delta_Web_Mercator < 1.0E-2 );

    Projection::convert(c_Web_Mercator, proj_EPSG4326, proj_EPSG3857);
    delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Elliptical.p, c_Web_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Web_Mercator < 1.0E-6 );

    // Test multi Lambert conformal conic
    std::vector<Coordinate<REAL> > c_Lambert_multi;
    Coordinate<REAL> c_Lambert_Target_REAL = { (REAL)2496750.96316546, (REAL)2409712.42995386 };
    for (int i = 0; i < 10; i++) {
        c_Lambert_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Lambert_multi, proj_EPSG3111, proj_EPSG4326);
    REAL delta_Lambert_REAL = std::hypot(c_Lambert_multi[0].p-c_Lambert_Target_REAL.p, c_Lambert_multi[0].q-c_Lambert_Target_REAL.q);
    REQUIRE( delta_Lambert_REAL < 10.0 );

    // Test multi Albers
    std::vector<Coordinate<REAL> > c_Albers_multi;
    Coordinate<REAL> c_Albers_Target_REAL = { (REAL)1146469.071166, (REAL)-4192119.31810244 };
    for (int i = 0; i < 10; i++) {
        c_Albers_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Albers_multi, proj_EPSG3577, proj_EPSG4326);
    REAL delta_Albers_REAL = std::hypot(c_Albers_multi[0].p-c_Albers_Target_REAL.p, c_Albers_multi[0].q-c_Albers_Target_REAL.q);
    REQUIRE( delta_Albers_REAL < 10.0 );

    // Test multi Transverse Mercator
    std::vector<Coordinate<REAL> > c_TM_multi;
    Coordinate<REAL> c_TM_Target_REAL = { (REAL)320704.446321103, (REAL)5812911.69953155 };
    for (int i = 0; i < 10; i++) {
        c_TM_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_TM_multi, proj_EPSG28355, proj_EPSG4326);
    REAL delta_UTM_REAL = std::hypot(c_TM_multi[0].p-c_TM_Target_REAL.p, c_TM_multi[0].q-c_TM_Target_REAL.q);
    REQUIRE( delta_UTM_REAL < 1.0 );

    // Test multi Mercator
    std::vector<Coordinate<REAL> > c_Mercator_multi;
    Coordinate<REAL> c_Mercator_Target_REAL = { (REAL)-560705.14, (REAL)-4526927.40 };
    for (int i = 0; i < 10; i++) {
        c_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Mercator_multi, proj_EPSG3832, proj_EPSG4326);
    REAL delta_Mercator_REAL = std::hypot(c_Mercator_multi[0].p-c_Mercator_Target_REAL.p, c_Mercator_multi[0].q-c_Mercator_Target_REAL.q);
    REQUIRE( delta_Mercator_REAL < 10.0 );

    // Test multi Web Mercator
    std::vector<Coordinate<REAL> > c_Web_Mercator_multi;
    Coordinate<REAL> c_Web_Mercator_Target_REAL = { (REAL)16137218.48, (REAL)-4553127.11 };
    for (int i = 0; i < 10; i++) {
        c_Web_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Web_Mercator_multi, proj_EPSG3857, proj_EPSG4326);
    REAL delta_Web_Mercator_REAL = std::hypot(c_Web_Mercator_multi[0].p-c_Web_Mercator_Target_REAL.p, c_Web_Mercator_multi[0].q-c_Web_Mercator_Target_REAL.q);
    REQUIRE( delta_Web_Mercator_REAL < 10.0 );
}

TEST_CASE( "Vector projection from WKT2", "[projection]" ) {

    // Create projections
    std::string projWKT_EPSG4326 = R"delimiter(GEOGCRS["WGS 84",ENSEMBLE["World Geodetic System 1984 ensemble",MEMBER["World Geodetic System 1984 (Transit)"],MEMBER["World Geodetic System 1984 (G730)"],MEMBER["World Geodetic System 1984 (G873)"],MEMBER["World Geodetic System 1984 (G1150)"],MEMBER["World Geodetic System 1984 (G1674)"],MEMBER["World Geodetic System 1984 (G1762)"],MEMBER["World Geodetic System 1984 (G2139)"],ELLIPSOID["WGS 84",6378137,298.257223563,LENGTHUNIT["metre",1]],ENSEMBLEACCURACY[2.0]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],CS[ellipsoidal,2],AXIS["geodetic latitude (Lat)",north,ORDER[1],ANGLEUNIT["degree",0.0174532925199433]],AXIS["geodetic longitude (Lon)",east,ORDER[2],ANGLEUNIT["degree",0.0174532925199433]],USAGE[SCOPE["Horizontal component of 3D system."],AREA["World."],BBOX[-90,-180,90,180]],ID["EPSG",4326]])delimiter";
    auto proj_EPSG4326 = Projection::parseWKT(projWKT_EPSG4326);

    std::string projWKT_EPSG3111 = R"delimiter(PROJCRS["GDA94 / Vicgrid",BASEGEOGCRS["GDA94",DATUM["Geocentric Datum of Australia 1994",ELLIPSOID["GRS 1980",6378137,298.257222101,LENGTHUNIT["metre",1]]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",4283]],CONVERSION["Vicgrid",METHOD["Lambert Conic Conformal (2SP)",ID["EPSG",9802]],PARAMETER["Latitude of false origin",-37,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8821]],PARAMETER["Longitude of false origin",145,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8822]],PARAMETER["Latitude of 1st standard parallel",-36,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8823]],PARAMETER["Latitude of 2nd standard parallel",-38,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8824]],PARAMETER["Easting at false origin",2500000,LENGTHUNIT["metre",1],ID["EPSG",8826]],PARAMETER["Northing at false origin",2500000,LENGTHUNIT["metre",1],ID["EPSG",8827]]],CS[Cartesian,2],AXIS["(E)",east,ORDER[1],LENGTHUNIT["metre",1]],AXIS["(N)",north,ORDER[2],LENGTHUNIT["metre",1]],USAGE[SCOPE["State-wide spatial data management."],AREA["Australia - Victoria."],BBOX[-39.2,140.96,-33.98,150.04]],ID["EPSG",3111]])delimiter";
    auto proj_EPSG3111 = Projection::parseWKT(projWKT_EPSG3111);

    std::string projWKT_EPSG3577 = R"delimiter(PROJCRS["GDA94 / Australian Albers",BASEGEOGCRS["GDA94",DATUM["Geocentric Datum of Australia 1994",ELLIPSOID["GRS 1980",6378137,298.257222101,LENGTHUNIT["metre",1]]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",4283]],CONVERSION["Australian Albers",METHOD["Albers Equal Area",ID["EPSG",9822]],PARAMETER["Latitude of false origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8821]],PARAMETER["Longitude of false origin",132,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8822]],PARAMETER["Latitude of 1st standard parallel",-18,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8823]],PARAMETER["Latitude of 2nd standard parallel",-36,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8824]],PARAMETER["Easting at false origin",0,LENGTHUNIT["metre",1],ID["EPSG",8826]],PARAMETER["Northing at false origin",0,LENGTHUNIT["metre",1],ID["EPSG",8827]]],CS[Cartesian,2],AXIS["(E)",east,ORDER[1],LENGTHUNIT["metre",1]],AXIS["(N)",north,ORDER[2],LENGTHUNIT["metre",1]],USAGE[SCOPE["Statistical analysis."],AREA["Australia - Australian Capital Territory; New South Wales; Northern Territory; Queensland; South Australia; Tasmania; Western Australia; Victoria."],BBOX[-43.7,112.85,-9.86,153.69]],ID["EPSG",3577]])delimiter";
    auto proj_EPSG3577 = Projection::parseWKT(projWKT_EPSG3577);

    std::string projWKT_EPSG28355 = R"delimiter(PROJCRS["GDA94 / MGA zone 55",BASEGEOGCRS["GDA94",DATUM["Geocentric Datum of Australia 1994",ELLIPSOID["GRS 1980",6378137,298.257222101,LENGTHUNIT["metre",1]]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",4283]],CONVERSION["Map Grid of Australia zone 55",METHOD["Transverse Mercator",ID["EPSG",9807]],PARAMETER["Latitude of natural origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8801]],PARAMETER["Longitude of natural origin",147,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8802]],PARAMETER["Scale factor at natural origin",0.9996,SCALEUNIT["unity",1],ID["EPSG",8805]],PARAMETER["False easting",500000,LENGTHUNIT["metre",1],ID["EPSG",8806]],PARAMETER["False northing",10000000,LENGTHUNIT["metre",1],ID["EPSG",8807]]],CS[Cartesian,2],AXIS["(E)",east,ORDER[1],LENGTHUNIT["metre",1]],AXIS["(N)",north,ORDER[2],LENGTHUNIT["metre",1]],USAGE[SCOPE["Engineering survey, topographic mapping."],AREA["Australia - onshore and offshore between 144°E and 150°E."],BBOX[-50.89,144,-9.23,150.01]],ID["EPSG",28355]])delimiter";
    auto proj_EPSG28355 = Projection::parseWKT(projWKT_EPSG28355);

    std::string projWKT_EPSG3832 = R"delimiter(PROJCRS["WGS 84 / PDC Mercator",BASEGEOGCRS["WGS 84",ENSEMBLE["World Geodetic System 1984 ensemble",MEMBER["World Geodetic System 1984 (Transit)"],MEMBER["World Geodetic System 1984 (G730)"],MEMBER["World Geodetic System 1984 (G873)"],MEMBER["World Geodetic System 1984 (G1150)"],MEMBER["World Geodetic System 1984 (G1674)"],MEMBER["World Geodetic System 1984 (G1762)"],MEMBER["World Geodetic System 1984 (G2139)"],ELLIPSOID["WGS 84",6378137,298.257223563,LENGTHUNIT["metre",1]],ENSEMBLEACCURACY[2.0]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",4326]],CONVERSION["Pacific Disaster Center Mercator",METHOD["Mercator (variant A)",ID["EPSG",9804]],PARAMETER["Latitude of natural origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8801]],PARAMETER["Longitude of natural origin",150,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8802]],PARAMETER["Scale factor at natural origin",1,SCALEUNIT["unity",1],ID["EPSG",8805]],PARAMETER["False easting",0,LENGTHUNIT["metre",1],ID["EPSG",8806]],PARAMETER["False northing",0,LENGTHUNIT["metre",1],ID["EPSG",8807]]],CS[Cartesian,2],AXIS["(E)",east,ORDER[1],LENGTHUNIT["metre",1]],AXIS["(N)",north,ORDER[2],LENGTHUNIT["metre",1]],USAGE[SCOPE["Topographic mapping (small scale)."],AREA["Pacific Ocean - American Samoa, Antarctica, Australia, Brunei Darussalam, Cambodia, Canada, Chile, China, China - Hong Kong, China - Macao, Cook Islands, Ecuador, Fiji, French Polynesia, Guam, Indonesia, Japan, Kiribati, Democratic People's Republic of Korea (North Korea), Republic of Korea (South Korea), Malaysia, Marshall Islands, Federated States of Micronesia, Nauru, New Caledonia, New Zealand, Niue, Norfolk Island, Northern Mariana Islands, Palau, Panama, Papua New Guinea (PNG), Peru, Philippines, Pitcairn, Russian Federation, Samoa, Singapore, Solomon Islands, Taiwan, Thailand, Tokelau, Tonga, Tuvalu, United States (USA), United States Minor Outlying Islands, Vanuatu, Venezuela, Vietnam, Wallis and Futuna."],BBOX[-60,98.69,66.67,-68]],ID["EPSG",3832]])delimiter";
    auto proj_EPSG3832 = Projection::parseWKT(projWKT_EPSG3832);

    std::string projWKT_EPSG3857 = R"delimiter(PROJCRS["WGS 84 / Pseudo-Mercator",BASEGEOGCRS["WGS 84",ENSEMBLE["World Geodetic System 1984 ensemble",MEMBER["World Geodetic System 1984 (Transit)"],MEMBER["World Geodetic System 1984 (G730)"],MEMBER["World Geodetic System 1984 (G873)"],MEMBER["World Geodetic System 1984 (G1150)"],MEMBER["World Geodetic System 1984 (G1674)"],MEMBER["World Geodetic System 1984 (G1762)"],MEMBER["World Geodetic System 1984 (G2139)"],ELLIPSOID["WGS 84",6378137,298.257223563,LENGTHUNIT["metre",1]],ENSEMBLEACCURACY[2.0]],PRIMEM["Greenwich",0,ANGLEUNIT["degree",0.0174532925199433]],ID["EPSG",4326]],CONVERSION["Popular Visualisation Pseudo-Mercator",METHOD["Popular Visualisation Pseudo Mercator",ID["EPSG",1024]],PARAMETER["Latitude of natural origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8801]],PARAMETER["Longitude of natural origin",0,ANGLEUNIT["degree",0.0174532925199433],ID["EPSG",8802]],PARAMETER["False easting",0,LENGTHUNIT["metre",1],ID["EPSG",8806]],PARAMETER["False northing",0,LENGTHUNIT["metre",1],ID["EPSG",8807]]],CS[Cartesian,2],AXIS["easting (X)",east,ORDER[1],LENGTHUNIT["metre",1]],AXIS["northing (Y)",north,ORDER[2],LENGTHUNIT["metre",1]],USAGE[SCOPE["Web mapping and visualisation."],AREA["World between 85.0°S and 85.0°N."],BBOX[-85.06,-180,85.06,180]],ID["EPSG",3857]])delimiter";
    auto proj_EPSG3857 = Projection::parseWKT(projWKT_EPSG3857);

    // Test coordinate
    Coordinate<double> c_Elliptical = { 144.9631, -37.8136 };

    // Test Lambert conformal conic
    Coordinate<double> c_Lambert = c_Elliptical;
    Coordinate<double> c_Lambert_Target = { 2496750.96316546, 2409712.42995386 };
    Projection::convert(c_Lambert, proj_EPSG3111, proj_EPSG4326);
    double delta_Lambert = std::hypot(c_Lambert.p-c_Lambert_Target.p, c_Lambert.q-c_Lambert_Target.q);
    REQUIRE( delta_Lambert < 1.0E-3 );

    Projection::convert(c_Lambert, proj_EPSG4326, proj_EPSG3111);
    delta_Lambert = std::hypot(c_Lambert.p-c_Elliptical.p, c_Lambert.q-c_Elliptical.q);
    REQUIRE( delta_Lambert < 1.0E-6 );

    // Test Albers conic
    Coordinate<double> c_Albers = c_Elliptical;
    Coordinate<double> c_Albers_Target = { 1146469.071166, -4192119.31810244 };
    Projection::convert(c_Albers, proj_EPSG3577, proj_EPSG4326);
    double delta_Albers = std::hypot(c_Albers.p-c_Albers_Target.p, c_Albers.q-c_Albers_Target.q);
    REQUIRE( delta_Albers < 1.0E-3 );

    Projection::convert(c_Albers, proj_EPSG4326, proj_EPSG3577);
    delta_Albers = std::hypot(c_Albers.p-c_Elliptical.p, c_Albers.q-c_Elliptical.q);
    REQUIRE( delta_Albers < 1.0E-6 );

    // Test Transverse Mercator
    Coordinate<double> c_TM = c_Elliptical;
    Coordinate<double> c_TM_Target = { 320704.446321103, 5812911.69953155 };
    Projection::convert(c_TM, proj_EPSG28355, proj_EPSG4326);
    double delta_TM = std::hypot(c_TM.p-c_TM_Target.p, c_TM.q-c_TM_Target.q);
    REQUIRE( delta_TM < 1.0E-3 );

    Projection::convert(c_TM, proj_EPSG4326, proj_EPSG28355);
    delta_TM = std::hypot(c_TM.p-c_Elliptical.p, c_TM.q-c_Elliptical.q);
    REQUIRE( delta_TM < 1.0E-6 );

    // Test Mercator
    Coordinate<double> c_Mercator = c_Elliptical;
    Coordinate<double> c_Mercator_Target = { -560705.14, -4526927.40 };
    Projection::convert(c_Mercator, proj_EPSG3832, proj_EPSG4326);
    double delta_Mercator = std::hypot(c_Mercator.p-c_Mercator_Target.p, c_Mercator.q-c_Mercator_Target.q);
    REQUIRE( delta_Mercator < 1.0E-2 );

    Projection::convert(c_Mercator, proj_EPSG4326, proj_EPSG3832);
    delta_Mercator = std::hypot(c_Mercator.p-c_Elliptical.p, c_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Mercator < 1.0E-6 );

    // Test Web Mercator
    Coordinate<double> c_Web_Mercator = c_Elliptical;
    Coordinate<double> c_Web_Mercator_Target = { 16137218.48, -4553127.11 };
    Projection::convert(c_Web_Mercator, proj_EPSG3857, proj_EPSG4326);
    double delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Web_Mercator_Target.p, c_Web_Mercator.q-c_Web_Mercator_Target.q);
    REQUIRE( delta_Web_Mercator < 1.0E-2 );

    Projection::convert(c_Web_Mercator, proj_EPSG4326, proj_EPSG3857);
    delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Elliptical.p, c_Web_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Web_Mercator < 1.0E-6 );

    // Test multi Lambert conformal conic
    std::vector<Coordinate<REAL> > c_Lambert_multi;
    Coordinate<REAL> c_Lambert_Target_REAL = { (REAL)2496750.96316546, (REAL)2409712.42995386 };
    for (int i = 0; i < 10; i++) {
        c_Lambert_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Lambert_multi, proj_EPSG3111, proj_EPSG4326);
    REAL delta_Lambert_REAL = std::hypot(c_Lambert_multi[0].p-c_Lambert_Target_REAL.p, c_Lambert_multi[0].q-c_Lambert_Target_REAL.q);
    REQUIRE( delta_Lambert_REAL < 10.0 );

    // Test multi Albers
    std::vector<Coordinate<REAL> > c_Albers_multi;
    Coordinate<REAL> c_Albers_Target_REAL = { (REAL)1146469.071166, (REAL)-4192119.31810244 };
    for (int i = 0; i < 10; i++) {
        c_Albers_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Albers_multi, proj_EPSG3577, proj_EPSG4326);
    REAL delta_Albers_REAL = std::hypot(c_Albers_multi[0].p-c_Albers_Target_REAL.p, c_Albers_multi[0].q-c_Albers_Target_REAL.q);
    REQUIRE( delta_Albers_REAL < 10.0 );

    // Test multi Transverse Mercator
    std::vector<Coordinate<REAL> > c_TM_multi;
    Coordinate<REAL> c_TM_Target_REAL = { (REAL)320704.446321103, (REAL)5812911.69953155 };
    for (int i = 0; i < 10; i++) {
        c_TM_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_TM_multi, proj_EPSG28355, proj_EPSG4326);
    REAL delta_UTM_REAL = std::hypot(c_TM_multi[0].p-c_TM_Target_REAL.p, c_TM_multi[0].q-c_TM_Target_REAL.q);
    REQUIRE( delta_UTM_REAL < 1.0 );

    // Test multi Mercator
    std::vector<Coordinate<REAL> > c_Mercator_multi;
    Coordinate<REAL> c_Mercator_Target_REAL = { (REAL)-560705.14, (REAL)-4526927.40 };
    for (int i = 0; i < 10; i++) {
        c_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Mercator_multi, proj_EPSG3832, proj_EPSG4326);
    REAL delta_Mercator_REAL = std::hypot(c_Mercator_multi[0].p-c_Mercator_Target_REAL.p, c_Mercator_multi[0].q-c_Mercator_Target_REAL.q);
    REQUIRE( delta_Mercator_REAL < 10.0 );

    // Test multi Web Mercator
    std::vector<Coordinate<REAL> > c_Web_Mercator_multi;
    Coordinate<REAL> c_Web_Mercator_Target_REAL = { (REAL)16137218.48, (REAL)-4553127.11 };
    for (int i = 0; i < 10; i++) {
        c_Web_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Web_Mercator_multi, proj_EPSG3857, proj_EPSG4326);
    REAL delta_Web_Mercator_REAL = std::hypot(c_Web_Mercator_multi[0].p-c_Web_Mercator_Target_REAL.p, c_Web_Mercator_multi[0].q-c_Web_Mercator_Target_REAL.q);
    REQUIRE( delta_Web_Mercator_REAL < 10.0 );
}

TEST_CASE( "Vector projection from PROJ4", "[projection]" ) {

    // Create projections
    std::string projPROJ4_EPSG4326 = R"(+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs)";
    auto proj_EPSG4326 = Projection::parsePROJ4(projPROJ4_EPSG4326);

    std::string projPROJ4_EPSG3111 = R"(+proj=lcc +lat_1=-36 +lat_2=-38 +lat_0=-37 +lon_0=145 +x_0=2500000 +y_0=2500000 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_EPSG3111 = Projection::parsePROJ4(projPROJ4_EPSG3111);

    std::string projPROJ4_EPSG3577 = R"(+proj=aea +lat_1=-18 +lat_2=-36 +lat_0=0 +lon_0=132 +x_0=0 +y_0=0 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_EPSG3577 = Projection::parsePROJ4(projPROJ4_EPSG3577);

    std::string projPROJ4_UTM_EPSG28355 = R"(+proj=utm +zone=55 +south +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_UTM_EPSG28355 = Projection::parsePROJ4(projPROJ4_UTM_EPSG28355);

    std::string projPROJ4_TM_EPSG28355 = R"(+proj=tmerc +lat_0=0 +lon_0=147 +k=0.9995999932289124 +x_0=500000 +y_0=10000000 +a=6378137 +b=6356752.314475018 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs)";
    auto proj_TM_EPSG28355 = Projection::parsePROJ4(projPROJ4_TM_EPSG28355);

    std::string projPROJ4_EPSG3832 = R"(+proj=merc +lon_0=150 +k=1 +x_0=0 +y_0=0 +datum=WGS84 +units=m +no_defs)";
    auto proj_EPSG3832 = Projection::parsePROJ4(projPROJ4_EPSG3832);

    std::string projPROJ4_EPSG3857 = R"(+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +no_defs)";
    auto proj_EPSG3857 = Projection::parsePROJ4(projPROJ4_EPSG3857);

    std::string projPROJ4_SRORG6974 = R"(+proj=sinu +lon_0=0 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs)";
    auto proj_SRORG6974 = Projection::parsePROJ4(projPROJ4_SRORG6974);

    // Test coordinate
    Coordinate<double> c_Elliptical = { 144.9631, -37.8136 };

    // Test Lambert conformal conic
    Coordinate<double> c_Lambert = c_Elliptical;
    Coordinate<double> c_Lambert_Target = { 2496750.96316546, 2409712.42995386 };
    Projection::convert(c_Lambert, proj_EPSG3111, proj_EPSG4326);
    double delta_Lambert = std::hypot(c_Lambert.p-c_Lambert_Target.p, c_Lambert.q-c_Lambert_Target.q);
    REQUIRE( delta_Lambert < 1.0E-3 );

    Projection::convert(c_Lambert, proj_EPSG4326, proj_EPSG3111);
    delta_Lambert = std::hypot(c_Lambert.p-c_Elliptical.p, c_Lambert.q-c_Elliptical.q);
    REQUIRE( delta_Lambert < 1.0E-6 );

    // Test Albers conic
    Coordinate<double> c_Albers = c_Elliptical;
    Coordinate<double> c_Albers_Target = { 1146469.071166, -4192119.31810244 };
    Projection::convert(c_Albers, proj_EPSG3577, proj_EPSG4326);
    double delta_Albers = std::hypot(c_Albers.p-c_Albers_Target.p, c_Albers.q-c_Albers_Target.q);
    REQUIRE( delta_Albers < 1.0E-3 );

    Projection::convert(c_Albers, proj_EPSG4326, proj_EPSG3577);
    delta_Albers = std::hypot(c_Albers.p-c_Elliptical.p, c_Albers.q-c_Elliptical.q);
    REQUIRE( delta_Albers < 1.0E-6 );

    // Test Transverse Mercator
    Coordinate<double> c_TM = c_Elliptical;
    Coordinate<double> c_TM_Target = { 320704.447532, 5812911.72786 };
    Projection::convert(c_TM, proj_TM_EPSG28355, proj_EPSG4326);
    double delta_TM = std::hypot(c_TM.p-c_TM_Target.p, c_TM.q-c_TM_Target.q);
    REQUIRE( delta_TM < 1.0E-3 );

    Projection::convert(c_TM, proj_EPSG4326, proj_TM_EPSG28355);
    delta_TM = std::hypot(c_TM.p-c_Elliptical.p, c_TM.q-c_Elliptical.q);
    REQUIRE( delta_TM < 1.0E-6 );

    // Test Universal Transverse Mercator
    Coordinate<double> c_UTM = c_Elliptical;
    Coordinate<double> c_UTM_Target = { 320704.446321103, 5812911.69953155 };
    Projection::convert(c_UTM, proj_UTM_EPSG28355, proj_EPSG4326);
    double delta_UTM = std::hypot(c_UTM.p-c_UTM_Target.p, c_UTM.q-c_UTM_Target.q);
    REQUIRE( delta_UTM < 1.0E-3 );

    Projection::convert(c_UTM, proj_EPSG4326, proj_UTM_EPSG28355);
    delta_UTM = std::hypot(c_UTM.p-c_Elliptical.p, c_UTM.q-c_Elliptical.q);
    REQUIRE( delta_UTM < 1.0E-6 );

    // Test Mercator
    Coordinate<double> c_Mercator = c_Elliptical;
    Coordinate<double> c_Mercator_Target = { -560705.14, -4526927.40 };
    Projection::convert(c_Mercator, proj_EPSG3832, proj_EPSG4326);
    double delta_Mercator = std::hypot(c_Mercator.p-c_Mercator_Target.p, c_Mercator.q-c_Mercator_Target.q);
    REQUIRE( delta_Mercator < 1.0E-2 );

    Projection::convert(c_Mercator, proj_EPSG4326, proj_EPSG3832);
    delta_Mercator = std::hypot(c_Mercator.p-c_Elliptical.p, c_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Mercator < 1.0E-6 );

    // Test Web Mercator
    Coordinate<double> c_Web_Mercator = c_Elliptical;
    Coordinate<double> c_Web_Mercator_Target = { 16137218.48, -4553127.11 };
    Projection::convert(c_Web_Mercator, proj_EPSG3857, proj_EPSG4326);
    double delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Web_Mercator_Target.p, c_Web_Mercator.q-c_Web_Mercator_Target.q);
    REQUIRE( delta_Web_Mercator < 1.0E-2 );

    Projection::convert(c_Web_Mercator, proj_EPSG4326, proj_EPSG3857);
    delta_Web_Mercator = std::hypot(c_Web_Mercator.p-c_Elliptical.p, c_Web_Mercator.q-c_Elliptical.q);
    REQUIRE( delta_Web_Mercator < 1.0E-6 );

    // Test Sinusoidal
    Coordinate<double> c_Sinusoidal = c_Elliptical;
    Coordinate<double> c_Sinusoidal_Target = { 12764626.0593, -4186808.60447 };
    Projection::convert(c_Sinusoidal, proj_SRORG6974, proj_EPSG4326);
    double delta_Sinusoidal = std::hypot(c_Sinusoidal.p-c_Sinusoidal_Target.p, c_Sinusoidal.q-c_Sinusoidal_Target.q);
    REQUIRE( delta_Sinusoidal < 1.0E-2 );

    Projection::convert(c_Sinusoidal, proj_EPSG4326, proj_SRORG6974);
    delta_Sinusoidal = std::hypot(c_Sinusoidal.p-c_Elliptical.p, c_Sinusoidal.q-c_Elliptical.q);
    REQUIRE( delta_Sinusoidal < 1.0E-6 );

    // Test multi Lambert conformal conic
    std::vector<Coordinate<REAL> > c_Lambert_multi;
    Coordinate<REAL> c_Lambert_Target_REAL = { (REAL)2496750.96316546, (REAL)2409712.42995386 };
    for (int i = 0; i < 10; i++) {
        c_Lambert_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Lambert_multi, proj_EPSG3111, proj_EPSG4326);
    REAL delta_Lambert_REAL = std::hypot(c_Lambert_multi[0].p-c_Lambert_Target_REAL.p, c_Lambert_multi[0].q-c_Lambert_Target_REAL.q);
    REQUIRE( delta_Lambert_REAL < 10.0 );

    // Test multi Albers
    std::vector<Coordinate<REAL> > c_Albers_multi;
    Coordinate<REAL> c_Albers_Target_REAL = { (REAL)1146469.071166, (REAL)-4192119.31810244 };
    for (int i = 0; i < 10; i++) {
        c_Albers_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Albers_multi, proj_EPSG3577, proj_EPSG4326);
    REAL delta_Albers_REAL = std::hypot(c_Albers_multi[0].p-c_Albers_Target_REAL.p, c_Albers_multi[0].q-c_Albers_Target_REAL.q);
    REQUIRE( delta_Albers_REAL < 10.0 );

    // Test multi Universal Transverse Mercator
    std::vector<Coordinate<REAL> > c_UTM_multi;
    Coordinate<REAL> c_UTM_Target_REAL = { (REAL)320704.446321103, (REAL)5812911.69953155 };
    for (int i = 0; i < 10; i++) {
        c_UTM_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_UTM_multi, proj_UTM_EPSG28355, proj_EPSG4326);
    REAL delta_UTM_REAL = std::hypot(c_UTM_multi[0].p-c_UTM_Target_REAL.p, c_UTM_multi[0].q-c_UTM_Target_REAL.q);
    REQUIRE( delta_UTM_REAL < 1.0 );

    // Test multi Mercator
    std::vector<Coordinate<REAL> > c_Mercator_multi;
    Coordinate<REAL> c_Mercator_Target_REAL = { (REAL)-560705.14, (REAL)-4526927.40 };
    for (int i = 0; i < 10; i++) {
        c_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Mercator_multi, proj_EPSG3832, proj_EPSG4326);
    REAL delta_Mercator_REAL = std::hypot(c_Mercator_multi[0].p-c_Mercator_Target_REAL.p, c_Mercator_multi[0].q-c_Mercator_Target_REAL.q);
    REQUIRE( delta_Mercator_REAL < 10.0 );

    // Test multi Web Mercator
    std::vector<Coordinate<REAL> > c_Web_Mercator_multi;
    Coordinate<REAL> c_Web_Mercator_Target_REAL = { (REAL)16137218.48, (REAL)-4553127.11 };
    for (int i = 0; i < 10; i++) {
        c_Web_Mercator_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Web_Mercator_multi, proj_EPSG3857, proj_EPSG4326);
    REAL delta_Web_Mercator_REAL = std::hypot(c_Web_Mercator_multi[0].p-c_Web_Mercator_Target_REAL.p, c_Web_Mercator_multi[0].q-c_Web_Mercator_Target_REAL.q);
    REQUIRE( delta_Web_Mercator_REAL < 10.0 );

    // Test multi Sinusoidal
    std::vector<Coordinate<REAL> > c_Sinusoidal_multi;
    Coordinate<REAL> c_Sinusoidal_Target_REAL = { (REAL)12764626.0593, (REAL)-4186808.60447 };
    for (int i = 0; i < 10; i++) {
        c_Sinusoidal_multi.push_back( { (REAL)(144.9631+0.1*i), (REAL)(-37.8136+0.1*i) } );
    }
    Projection::convert(c_Sinusoidal_multi, proj_SRORG6974, proj_EPSG4326);
    REAL delta_Sinusoidal_REAL = std::hypot(c_Sinusoidal_multi[0].p-c_Sinusoidal_Target_REAL.p, c_Sinusoidal_multi[0].q-c_Sinusoidal_Target_REAL.q);
    REQUIRE( delta_Sinusoidal_REAL < 10.0 );

}

TEST_CASE( "Time parsing", "[time]" ) {

    // Create vector data
    auto timeSinceEpoch = Strings::iso8601toEpoch("2010-01-10T08:05:10Z");
    auto failTest = Strings::iso8601toEpoch("FAIL_TEST");

    // Test values
    REQUIRE( timeSinceEpoch == 1263110710000 );
    REQUIRE( failTest == getNullValue<int64_t>() );
}

TEST_CASE( "Series", "[series]" ) {

    // Test constant series
    Series<double, double> s0;
    s0.addValue(1.0, 2.0);

    REQUIRE( s0.isInitialised() == true );
    REQUIRE( s0.isConstant() == true );
    REQUIRE( s0(1.0) == 2.0 );

    // Test series
    Series<double, double> s1;
    s1.addValues( { { 1.0, 20.0 }, { 0.0, 10.0 }, { 10.0, 0.0 }, { 2.0, 30.0 } } );

    REQUIRE( s1.isInitialised() == true );
    REQUIRE( s1.isConstant() == false );
    REQUIRE( s1.get_xMin() == 0.0 );
    REQUIRE( s1.get_xMax() == 10.0 );
    REQUIRE( s1.get_yMin() == 0.0 );
    REQUIRE( s1.get_yMax() == 30.0 );
    REQUIRE( s1(0.5) == 15.0 );
    REQUIRE( s1(1.5) == 25.0 );

    s1.setCapping(SeriesCapping::Uncapped);
    REQUIRE( s1(-1.0) != s1(-1.0) );
    REQUIRE( s1(20.0) != s1(20.0) );

    s1.setCapping(SeriesCapping::Capped);
    REQUIRE( s1(-1.0) == 10.0 );
    REQUIRE( s1(20.0) == 0.0 );

    // Test vector return
    auto sReadAbscissas = s1.getAbscissas();
    REQUIRE( sReadAbscissas.size() == 4 );
    REQUIRE( sReadAbscissas[2] == 2.0 );
    auto sReadOrdinates = s1.getOrdinates();
    REQUIRE( sReadOrdinates.size() == 4 );
    REQUIRE( sReadOrdinates[2] == 30.0 );

    // Test series monotone interpolation
    s1.setInterpolation(SeriesInterpolation::MonotoneCubic);
    REQUIRE( s1(6.0) == 18.75 );

    // Test vector return
    auto sRead = s1( { 0.0, 1.0, 6.0, 10.0 } );
    REQUIRE( sRead.size() == 4 );
    REQUIRE( sRead[1] == 20.0 );
    REQUIRE( sRead[2] == 18.75 );
    REQUIRE( sRead[3] == 0.0 );

    // Test bounded linear interpolation
    s1.setInterpolation(SeriesInterpolation::BoundedLinear);
    s1.setBounds(0.0, 40.0);
    REQUIRE( s1(6.0) == 35.0 );

    // Test string-based series
    Series<double, double> s2;
    s2.addValues( { { "1.0", 20.0 }, { "0.0", 10.0 }, { "10.0", 0.0 }, { "2.0", 30.0 } } );

    REQUIRE( s2.isInitialised() == true );
    REQUIRE( s2.isConstant() == false );
    REQUIRE( s2.get_xMin() == 0.0 );
    REQUIRE( s2.get_xMax() == 10.0 );
    REQUIRE( s2.get_yMin() == 0.0 );
    REQUIRE( s2.get_yMax() == 30.0 );
    REQUIRE( s2(0.5) == 15.0 );
    REQUIRE( s2(1.5) == 25.0 );

    // Test long series
    Series<int64_t, double> s3;
    s3.addValues( { { 1, 20.0 }, { 0, 10.0 }, { 10, 0.0 }, { 2, 30.0 } } );

    REQUIRE( s3.isInitialised() == true );
    REQUIRE( s3.isConstant() == false );
    REQUIRE( s3.get_xMin() == 0.0 );
    REQUIRE( s3.get_xMax() == 10.0 );
    REQUIRE( s3.get_yMin() == 0.0 );
    REQUIRE( s3.get_yMax() == 30.0 );
    REQUIRE( s3(6) == 15.0 );
}

TEST_CASE( "Time series", "[series]" ) {

    // Test string-based series
    Series<int64_t, double> s0;
    s0.addValues( {
        { "2010-01-10T08:06:10Z", 20.0 },
        { "2010-01-10T08:05:10Z", 10.0 },
        { "2010-01-10T08:15:10Z", 0.0 },
        { "2010-01-10T08:07:10Z", 30.0 }
    } );

    REQUIRE( s0.isInitialised() == true );
    REQUIRE( s0.isConstant() == false );
    REQUIRE( s0.get_xMin() == Strings::iso8601toEpoch("2010-01-10T08:05:10Z") );
    REQUIRE( s0.get_xMax() == Strings::iso8601toEpoch("2010-01-10T08:15:10Z") );
    REQUIRE( s0.get_yMin() == 0.0 );
    REQUIRE( s0.get_yMax() == 30.0 );
    REQUIRE( s0(Strings::iso8601toEpoch("2010-01-10T08:05:40Z")) == 15.0 );
    REQUIRE( s0(Strings::iso8601toEpoch("2010-01-10T08:06:40Z")) == 25.0 );
}

TEST_CASE( "Variables", "[variables]" ) {

    Variables<REAL, std::string> vars;
    for (int i = 0; i < 10; i++) {
        vars.set("a", (REAL)i, i);
        vars.set("b", (REAL)(2*i), i);
    }
    vars.set("c", (REAL)3.0);
    
    REQUIRE( vars.get("a", 5) == (REAL)5.0 );
    REQUIRE( vars.get("b", 5) == (REAL)10.0 );
    REQUIRE( vars.get("c") == (REAL)3.0 );
}

TEST_CASE( "Variable scripting", "[variables]" ) {

    Variables<REAL, std::string> vars;
    for (int i = 0; i < 10; i++) {
        vars.set("a", (REAL)i, i);
        vars.set("b", (REAL)(2*i), i);
    }
    vars.set("c", (REAL)1.0, 0);
    vars.set("c", (REAL)2.0, 1);
    vars.set("d", (REAL)3.0);
    
    REQUIRE( vars.get("a", 5) == (REAL)5.0 );
    REQUIRE( vars.get("b", 5) == (REAL)10.0 );
    REQUIRE( vars.get("d") == (REAL)3.0 );

    std::vector<std::string> oplist = {"+", "*", "-", "/"};
    REAL value[4] = {13.0, 30.0, 7.0, (REAL)3.3333};
    for (size_t i = 0; i < 4; i++){
        vars.runScript("a = b " + oplist[i] + " d;");
        REQUIRE( fabs(vars.get("a", 5) - value[i]) < 1e-3 );
    }
    
    // reset the values
    for (int i = 0; i < 10; i++) {
        vars.set("a", (REAL)i, i);
    }
    
    REQUIRE( vars.get("a", 5) == (REAL)5.0 );
    REQUIRE( vars.get("b", 5) == (REAL)10.0 );
    REQUIRE( vars.get("d") == (REAL)3.0 );

    for (int i = 10; i < 20; i++) {
        vars.set("a", (REAL)i, i);
        vars.set("b", (REAL)(2*i), i);
    }

    vars.runScript("a = b*d;");

    REQUIRE( vars.get("a", 15) == (REAL)90.0 );
    REQUIRE( vars.get("b", 15) == (REAL)30.0 );
    REQUIRE( vars.get("d") == (REAL)3.0 );
}

TEST_CASE( "Network", "[solver]" ) {

    // Create GeoJSON network string
    std::string json = R"({"features":[
    {"geometry":{"coordinates":[[100,140],[100,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.3,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,0],[100,-165.818]],"type":"LineString"},"properties":{"constant":100,"diameter":0.3,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,0],[-10,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.2,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,-165.818],[-10,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-10,0],[-10,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-10,0],[-120,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.2,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-10,-120],[-120,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[100,140],[-60,140]],"type":"LineString"},"properties":{"constant":100,"diameter":0.2,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-60,140],[-120,0]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[[-120,0],[-120,-120]],"type":"LineString"},"properties":{"constant":100,"diameter":0.18,"type":1},"type":"Feature"},
    {"geometry":{"coordinates":[100,140],"type":"Point"},"properties":{"flow":0.126},"type":"Feature"},
    {"geometry":{"coordinates":[100,0],"type":"Point"},"properties":{"flow":-0.025},"type":"Feature"},
    {"geometry":{"coordinates":[100,-165.818],"type":"Point"},"properties":{"flow":-0.05},"type":"Feature"},
    {"geometry":{"coordinates":[-10,0],"type":"Point"},"properties":{"flow":0},"type":"Feature"},
    {"geometry":{"coordinates":[-10,-120],"type":"Point"},"properties":{"flow":0},"type":"Feature"},
    {"geometry":{"coordinates":[-60,140],"type":"Point"},"properties":{"flow":0},"type":"Feature"},
    {"geometry":{"coordinates":[-120,0],"type":"Point"},"properties":{"flow":-0.025},"type":"Feature"}],"type":"FeatureCollection"})";

    // Parse GeoJSON string
    auto v = GeoJson<REAL>::geoJsonToVector(json, false);

    // Create flow solver instance
    NetworkFlowSolver<REAL> networkFlowSolver;
    bool initSuccess = networkFlowSolver.init(v);
    REQUIRE( initSuccess == true );

    // Run flow solver
    bool runSuccess = networkFlowSolver.run();
    REQUIRE( runSuccess == true );

    // Get values
    auto &vn = networkFlowSolver.getNetwork();
    auto &li = vn.getLineStringIndexes();
    auto &properties = vn.getProperties();
    auto &flow = properties.template getPropertyRef<std::vector<REAL> >("flow");

    // Get flow rates, compare to 4 loop analysis iterations:
    REAL flow_err_0 = fabs(flow[li[0]]-0.10190);
    REAL flow_err_1 = fabs(flow[li[1]]-0.02410);
    REAL flow_err_2 = fabs(flow[li[2]]-0.05511);
    REAL flow_err_3 = fabs(flow[li[3]]-0.02179);
    REAL flow_err_4 = fabs(flow[li[4]]-0.00511);
    REAL flow_err_5 = fabs(flow[li[5]]-0.00841);
    REAL flow_err_6 = fabs(flow[li[6]]-0.01338);
    REAL flow_err_7 = fabs(flow[li[7]]-0.01352);
    REAL flow_err_8 = fabs(flow[li[8]]-0.02410);
    REAL flow_err_9 = fabs(flow[li[9]]-0.01248);

    REQUIRE( flow_err_0 < 1.0E-3 );
    REQUIRE( flow_err_1 < 1.0E-3 );
    REQUIRE( flow_err_2 < 1.0E-3 );
    REQUIRE( flow_err_3 < 1.0E-3 );
    REQUIRE( flow_err_4 < 1.0E-3 );
    REQUIRE( flow_err_5 < 1.0E-3 );
    REQUIRE( flow_err_6 < 1.0E-3 );
    REQUIRE( flow_err_7 < 1.0E-3 );
    REQUIRE( flow_err_8 < 1.0E-3 );
    REQUIRE( flow_err_9 < 1.0E-3 );
}

TEST_CASE( "Level set", "[solver]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create starting conditions
    auto v = Vector<REAL>();
    v.addPoint( { 0.0, 0.0 } );
    v.setProperty(0, "radius", 10);

    // Create input raster list
    auto pType = std::make_shared<Raster<uint32_t, REAL> >("type");
    auto pParam = std::make_shared<Raster<REAL, REAL> >("param");
    std::vector<RasterBasePtr<REAL> > inputLayers = { pType, pParam };

    // Create output raster list
    auto pOutput = std::make_shared<Raster<REAL, REAL> >("output");
    std::vector<RasterBasePtr<REAL> > outputLayers = { pOutput };

    // Create variables
    std::shared_ptr<Variables<REAL, std::string> > variables;
    variables = std::make_shared<Variables<REAL, std::string> >();
    variables->set("varA", 77.0);
    variables->set("varB", 88.0);
    variables->set("varC", 11.0, 0);
    variables->set("varC", 22.0, 1);
    variables->set("varC", 33.0, 2);
    variables->set("varD", 44.0, 0);
    variables->set("varD", 55.0, 1);
    variables->set("varD", 66.0, 2);

    // Create type raster
    Raster<uint32_t, REAL> &type = *pType;
    type.init2D(1000, 1000, 1.0, 1.0, -500, -500);
    Raster<REAL, REAL> &param = *pParam;
    param.init2D(1000, 1000, 1.0, 1.0, -500, -500);
    runScript<REAL>("type = x > 0 && x < 100 && y > 0 && y < 100 ? 2 : 1;", { type } );

    // Create solver configuration
    std::string config = R"({
        "resolution": 0.5,
        "initialisationScript": "class = type; param = 0.5;",
        "buildScript": "if (class == 1) { speed = param; } else if (class == 2) { speed = type; }",
        "updateScript": "output = class*varA+varB+varC[2]; for (int i = 0; i < varD_length; i++) { output+=varD[i]; }"
    })";

    // Initialise solver
    auto solver = LevelSet<REAL>();
    bool initSuccess = solver.init(config, v, variables, inputLayers, outputLayers);
    REQUIRE( initSuccess == true );

    // Run solver
    bool runSuccess = true;
    while (runSuccess && solver.getParameters().time < 100.0) {
        runSuccess &= solver.step();
    }
    REQUIRE( runSuccess == true );

    // Check values
    REQUIRE( fabs(solver.getArrival().getBilinearValue(0, -50.0)-80.0) < 1.0 );
    REQUIRE( fabs(solver.getArrival().getBilinearValue(-50.0, 0)-80.0) < 1.0 );
    REQUIRE( fabs(solver.getArrival().getBilinearValue(-50.0*M_SQRT1_2, -50.0*M_SQRT1_2)-80.0) < 1.0 );
    REQUIRE( fabs(solver.getArrival().getBilinearValue(50.0*M_SQRT1_2, 50.0*M_SQRT1_2)-20.0) < 1.0 );

    Raster<REAL, REAL> &output = *pOutput;
    REQUIRE( output.getNearestValue(0, -50.0) == (REAL)(1*77+88+33+44+55+66) );
    REQUIRE( output.getNearestValue(-50.0, 0) == (REAL)(1*77+88+33+44+55+66) );
    REQUIRE( output.getNearestValue(-50.0*M_SQRT1_2, -50.0*M_SQRT1_2) == (REAL)(1*77+88+33+44+55+66) );
    REQUIRE( output.getNearestValue(50.0*M_SQRT1_2, 50.0*M_SQRT1_2) == (REAL)(2*77+88+33+44+55+66) );

}

TEST_CASE( "Particle", "[solver]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create particles
    auto p = Vector<REAL>();
    p.addProperty("test");
    for (int c = 0; c < 10; c++) {
        auto id = p.addPoint( {0, 0, 0} );
        p.setProperty(id, "test", (REAL)c);
    }

    // Create solver configuration
    std::shared_ptr<Variables<REAL, std::string> > variables;
    variables = std::make_shared<Variables<REAL, std::string> >();
    variables->set("g", 9.8, 1); // Varaible array test
    std::string config = R"({
        "dt": 0.001,
        "scheme": "RK23",
        "initialisationScript": "radius = 1.0; velocity.x = sqrt(g[1]/4.0); velocity.y = sqrt(g[1]/4.0); velocity.z = sqrt(g[1]/2.0);",
        "advectionScript": "time += dt; test += 1.0; if (position.z < 0.0) { radius = -1.0; }",
        "updateScript": "acceleration.z = -g[1];",
        "randomSeed": 12345
    } )";

    // Initialise solver    
    Particle<REAL> solver;
    bool initSuccess = solver.init(config, p, variables);

    // Run solver
    bool runSuccess = true;
    for (int i = 0; runSuccess && i < 1000; i++) {
        runSuccess &= solver.step();
    }
    REQUIRE( runSuccess == true );

    REAL test = p.template getProperty<REAL>(0, "test");
    REQUIRE( test == 1000.0 );

    REAL x = p.getCoordinate(0).p;
    REAL y = p.getCoordinate(0).q;
    REQUIRE( fabs(sqrt(x*x+y*y)-1.0) < 1.0E-3 );
}

TEST_CASE( "Multigrid", "[solver]" ) {

#ifdef USE_TILE_CACHING
    auto &tileCacheManager = TileCacheManager<REAL, REAL>::instance();
    REQUIRE( tileCacheManager.getHostAllocationBytes() == 0 );
    REQUIRE( tileCacheManager.getDeviceAllocationBytes() == 0 );
#endif

    // Create forcing grid
    makeRaster(f, REAL, REAL)
    auto pf = std::make_shared<Raster<REAL, REAL> >("f");
    pf->init2D(500, 500, 1.0, 1.0);
    pf->setAllCellValues(0.0);
    pf->setCellValue(100.0, 250, 250);

    // Create solver configuration
    std::string config = R"({
        "initialisationScript" : "b = f;"
    } )";

    // Initialise solver
    Multigrid<REAL> solver;
    bool initSuccess = solver.init(config, { pf });

    // Run solver
    bool runSuccess = solver.step();
    REQUIRE( runSuccess == true );

    // Get solution Raster
    auto &u = solver.getSolution();

    // Test two points on the Raster for zero second derivative
    REAL d_50_50 =
        u.getCellValue(49, 50)+
        u.getCellValue(51, 50)+
        u.getCellValue(50, 49)+
        u.getCellValue(50, 51)-
        4.0*u.getCellValue(50, 50);
    REQUIRE( fabs(d_50_50) < 1.0E-3 );

    REAL d_400_100 =
        u.getCellValue(399, 100)+
        u.getCellValue(401, 100)+
        u.getCellValue(400,  99)+
        u.getCellValue(400, 101)-
        4.0*u.getCellValue(400, 100);
    REQUIRE( fabs(d_400_100) < 1.0E-3 );
}


TEST_CASE( "Multigrid pyramids", "[solver]" ) {

    // Create raster
    makeRaster(A, REAL, REAL)
    auto pf = std::make_shared<Raster<REAL, REAL> >("f");
    pf->init2D(1000, 1000, 1, 1);
    runScriptNoOut<REAL>("f = x*y", { *pf } );

    // Create multigrid solver
    auto solver = Multigrid<REAL>();
    std::string config = R"({
        "initialisationScript": "b = f;"
    })";
    solver.init(config, { pf } );

    // Create pyramids
    solver.pyramids();
    
    // Test values
    auto dims_in = solver.getForcing().getRasterDimensions().d;
    for (int level = 0; level < 8; level++) {
        auto dims = solver.getForcingLevel(level).getRasterDimensions().d;
        REQUIRE( dims.nx == (dims_in.nx>>level) );
        REQUIRE( dims.ny == (dims_in.ny>>level) );
        REQUIRE( fabs(solver.getForcingLevel(level).getBilinearValue(500, 500)-2.5E5) < 1.0E5 );
    }
}

TEST_CASE( "ODE", "[solver]" ) {

    // Test 1: dv/dt = -3 t^2 e^v, v(0) = 0
    {
        for (int i = 0; i < 2; i++) {

            std::shared_ptr<Variables<REAL, std::string> > variables;
            variables = std::make_shared<Variables<REAL, std::string> >();
            variables->set("v", 0.0);
            variables->set("k", -3.0);

            // Create solver configuration
            json11::Json config = json11::Json::object {
                { "dt", 0.01 },
                { "scheme", (i == 0 ? "RK23" : "RK45") },
                { "functionScripts", json11::Json::array {
                    json11::Json::object { { "v", "return k*t*t*exp(v);" } } } 
                }
            };

            // Initialise solver
            auto solver = ODE<REAL>();
            bool initSuccess = solver.init(config.dump(), variables);
            REQUIRE( initSuccess == true );
        
            // Run solver
            REAL time = 0.0;
            for (int i = 0; i < 100; i++) {
                solver.step();
                time += 0.01;
            }
            REAL numerical = variables->get("v");
            REAL analytic = -log(pow(time, 3.0)+1.0);

            REQUIRE( fabs(numerical-analytic) < 1.0E-3);
        }
    }
    
    // Test 2: du/dt = u/t + 3 sqrt(t/u), u(1) = 1
    {
        for (int i = 0; i < 2; i++) {

            std::shared_ptr<Variables<REAL, std::string> > variables;
            variables = std::make_shared<Variables<REAL, std::string> >();
            variables->set("u", 1.0);
        
            // Create solver configuration
            json11::Json config = json11::Json::object {
                { "dt", 0.01 },
                { "scheme", (i == 0 ? "RK23" : "RK45") },
                { "functionScripts", json11::Json::array {
                    json11::Json::object { { "u", "return (u/t)+3.0*sqrt(t/u);" } } } 
                }
            };

            // Initialise solver
            auto solver = ODE<REAL>();
            bool initSuccess = solver.init(config.dump(), variables);
            REQUIRE( initSuccess == true );
        
            // Run solver
            REAL time = 1.0;
            solver.setTime(time);
            for (int i = 0; i < 100; i++) {
                solver.step();
                time += 0.01;
            }
            REAL numerical = variables->get("u");
            REAL analytic = time*pow(4.5*log(time)+1.0, 2.0/3.0);

            REQUIRE( fabs(numerical-analytic) < 1.0E-3);
        }
    }
    
    // Test 3: parabolic under gravity, d^2x/dt^2 = 0, d^2y/dt^2 = -1, x(0) = 0, y(0) = 0, u(0) = dx(0)/dt = 2, v(0) = dy(0)/dt = 2 (g = 1)
    {
        for (int i = 0; i < 2; i++) {

            std::shared_ptr<Variables<REAL, std::string> > variables;
            variables = std::make_shared<Variables<REAL, std::string> >();
            variables->set("x", 0.0);
            variables->set("y", 0.0);
            variables->set("u", 2.0);
            variables->set("v", 2.0);
        
            // Create solver configuration
            json11::Json config = json11::Json::object {
                { "dt", 0.1 },
                { "scheme", (i == 0 ? "RK23" : "RK45") },
                { "functionScripts", json11::Json::array {
                    json11::Json::object { { "x", "return u;" } },
                    json11::Json::object { { "y", "return v;" } },
                    json11::Json::object { { "u", "return 0.0;" } },
                    json11::Json::object { { "v", "return -1.0;" } } } 
                }
            };

            // Initialise solver
            auto solver = ODE<REAL>();
            bool initSuccess = solver.init(config.dump(), variables);
            REQUIRE( initSuccess == true );
        
            // Run solver
            REAL time = 0.0;
            for (int i = 0; i < 40; i++) {
                solver.step();
            } 
            REAL numerical_x = variables->get("x");
            REAL numerical_y = variables->get("y");
            REQUIRE( fabs(numerical_x-(REAL)8.0) < 1.0E-3);
            REQUIRE( fabs(numerical_y-(REAL)0.0) < 1.0E-3);
        }
    }
    
    // Test 4: Lotka�Volterra equations
    {
        for (int i = 0; i < 2; i++) {

            std::shared_ptr<Variables<REAL, std::string> > variables;
            variables = std::make_shared<Variables<REAL, std::string> >();
            variables->set("alpha", 1.1);
            variables->set("beta", 0.4);
            variables->set("delta", 0.1);
            variables->set("gamma", 0.4);

            variables->set("x", 10.0);
            variables->set("y", 10.0);
        
            // Create solver configuration
            json11::Json config = json11::Json::object {
                { "dt", 0.1 },
                { "scheme", (i == 0 ? "RK23" : "RK45") },
                { "functionScripts", json11::Json::array {
                    json11::Json::object { { "x", "return alpha*x-beta*x*y;" } },
                    json11::Json::object { { "y", "return delta*x*y-gamma*y;" } } } 
                }
            };

            // Initialise solver
            auto solver = ODE<REAL>();
            bool initSuccess = solver.init(config.dump(), variables);
            REQUIRE( initSuccess == true );
        
            // Run solver
            REAL time = 0.0;
            for (int i = 0; i < 1000; i++) {
                solver.step();
                time += 0.1;
            } 

            REAL numerical_x = variables->get("x");
            REAL numerical_y = variables->get("y");
            //REQUIRE( fabs(numerical_x-(REAL)27.2473) < 1.0E-3); // TODO generate comparison with another solver
            //REQUIRE( fabs(numerical_y-(REAL)4.55964) < 1.0E-3); // TODO generate comparison with another solver
        }
    }

    // Test 5: Lorenz system
    {
        for (int i = 0; i < 2; i++) {

            std::shared_ptr<Variables<REAL, std::string> > variables;
            variables = std::make_shared<Variables<REAL, std::string> >();
            variables->set("x", 10.0, 0);
            variables->set("y", 10.0, 0);
            variables->set("z", 10.0, 0);

            variables->set("x", 11.0, 1);
            variables->set("y", 11.0, 1);
            variables->set("z", 11.0, 1);

            variables->set("sigma", 10.0);
            variables->set("rho", 28.0);
            variables->set("beta", 8.0/3.0);
        
            // Create solver configuration
            json11::Json config = json11::Json::object {
                { "dt", 0.1 },
                { "scheme", (i == 0 ? "RK23" : "RK45") },
                { "functionScripts", json11::Json::array {
                    json11::Json::object { { "x", "return sigma*(y-x);" } },
                    json11::Json::object { { "y", "return x*(rho-z)-y;" } },
                    json11::Json::object { { "z",  "return x*y-beta*z;" } } } 
                }
            };

            // Initialise solver
            auto solver = ODE<REAL>();
            bool initSuccess = solver.init(config.dump(), variables);
            REQUIRE( initSuccess == true );        
        
            REAL time = 0.0;
            for (int i = 0; i < 10000; i++) {
                solver.step();
                time += 0.01;
            } 

            REAL numerical_x0 = variables->get("x", 0);
            REAL numerical_y0 = variables->get("y", 0);
            REAL numerical_z0 = variables->get("z", 0);
            REQUIRE( numerical_x0 > -25.0 );
            REQUIRE( numerical_x0 < 25.0 );
            REQUIRE( numerical_y0 > -25.0 );
            REQUIRE( numerical_y0 < 25.0 );
            REQUIRE( numerical_z0 > 0.0 );
            REQUIRE( numerical_z0 < 50.0 );

            REAL numerical_x1 = variables->get("x", 1);
            REAL numerical_y1 = variables->get("y", 1);
            REAL numerical_z1 = variables->get("z", 1);
            REQUIRE( numerical_x1 > -25.0 );
            REQUIRE( numerical_x1 < 25.0 );
            REQUIRE( numerical_y1 > -25.0 );
            REQUIRE( numerical_y1 < 25.0 );
            REQUIRE( numerical_z1 > 0.0 );
            REQUIRE( numerical_z1 < 50.0 );
        }
     }
}

TEST_CASE( "Operation", "[operation]") {
    // Test 1 (variables)
    SECTION("Variables operation") {        
        auto jsonConfig = R"({
                "operations": [
                    {
                        "variables": {
                            "A": [1.0, 1.0, 1.0, 1.0],
                            "B": [1.0, 2.0, 3.0, 4.0],
                            "C": {
                                "size": 4
                            }
                        }
                    },
                    {
                        "runVariablesScript": {
                            "script": "B = A"
                        }
                    }
                ]
            })";
        
        std::ofstream configFile;
        configFile.open("variable_operation_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();
        
        Operation<REAL>::runFromConfigFile("variable_operation_config.json");
        
        std::string filename = "variable_operation_config.json"; 
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);
    }
    
    // Test 2 (raster create)
    SECTION("Create raster operation") {
        // make a test raster
        makeRaster(r, REAL, REAL);
        r.init(5, 5, 1, 1.0, 1.0, 0.0);
        r.setAllCellValues(0.0);
        r.setProjectionParameters(Projection::fromEPSG("4326"));
        
        runScript<REAL>("r = randomNormal(0.0, 1.0);", { r });
        auto r_value = r.getCellValue(0, 0);
        
        r.write("test_create_raster.tif");
        
        auto jsonConfig = R"({
                "operations": [
                    {
                        "createRaster": {
                              "name": "testRasterA",
                              "type": "REAL",
                              "source": "test_create_raster.tif",
                              "projection": "+proj=longlat +datum=WGS84 +no_defs +type=crs"
                        }
                    },
                    {
                        "runRasterScript": {
                            "script": "testRasterA = testRasterA * 2"
                        }
                    },
                    {
                        "write": {
                            "name": "testRasterA",
                            "destination": "output_create_raster.tif"
                        }
                    }
                ]
            })";

        std::ofstream configFile;
        configFile.open("create_raster_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();

        Operation<REAL>::runFromConfigFile("create_raster_config.json");
        
        makeRaster(r2, REAL, REAL);
        r2.read("output_create_raster.tif");

        REQUIRE(r2.getCellValue(0, 0) == (REAL) (r_value * 2));
        
        r2.closeFile();

        std::string filename = "create_raster_config.json"; 
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "test_create_raster.tif";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "output_create_raster.tif";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

    }
    
    // Test 3 (create vector)
    SECTION("Create vector operation") {
        // Create GeoJSON string
        std::string json = R"({"features": [
            {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
                "properties": {"C": 10.0}, "type": "Feature"},
            {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
                "properties": {"C": 20.0}, "type": "Feature"},
            {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
                "properties": {"C": 30.0}, "type": "Feature"},
            {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
                "properties": {"C": 40.0}, "type": "Feature"},
            {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
                "properties": {"C": 50.0}, "type": "Feature"},
            {"geometry": {"coordinates": [2, 0.75], "type": "Point"},
                "properties": {"C": 60.0}, "type": "Feature"}
            ], "type": "FeatureCollection"})";

        auto v1 = GeoJson<REAL>::geoJsonToVector(json);

        // write GeoJSON file
        std::ofstream vectorFile;
        vectorFile.open("test_create_vector.geojson", std::ios::out | std::ios::trunc); 
        vectorFile << json;
        vectorFile.close();
        
        auto jsonConfig = R"({
                "operations": [
                    {
                        "createVector": {
                              "name": "testVectorA",
                              "source": "test_create_vector.geojson",
                              "projection": "EPSG:4326"
                        }
                    },
                    {
                        "runVectorScript": {
                            "name": "testVectorA",
                            "script": "C += 1.0;"
                        }
                    },
                    {
                        "write": {
                            "name": "testVectorA",
                            "destination": "output_create_vector.geojson"
                        }
                    }
                ]
            })";
        
        std::ofstream configFile;
        configFile.open("create_vector_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();

        Operation<REAL>::runFromConfigFile("create_vector_config.json");
        
        auto v2 = GeoJson<REAL>::geoJsonFileToVector("output_create_vector.geojson");

        REQUIRE(v2.getProperty<REAL>(0, "C") == (REAL) (v1.getProperty<REAL>(0, "C") + 1));

        std::string filename = "create_vector_config.json";
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "test_create_vector.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "output_create_vector.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

    }
    
    // Test 4 (area script)
    SECTION("Area script operation") {
        // Create raster
        makeRaster(testRasterA, REAL, REAL)
        testRasterA.init2D(21, 21, 1.0, 1.0);
        testRasterA.setAllCellValues((REAL)0.0);
        testRasterA.setCellValue((REAL)99.9, 10, 10);

        testRasterA.write("test_raster_areascript.tif");
        
        auto jsonConfig = R"({
                "operations": [
                    {
                        "createRaster": {
                              "name": "testRasterA",
                              "type": "REAL",
                              "source": "test_raster_areascript.tif",
                              "projection": "+proj=longlat +datum=WGS84 +no_defs +type=crs"
                        }
                    },
                    {
                        "runAreaScript": {
                            "create": "testRasterB",
                            "width": 3,
                            "script": "if (isValid_REAL(testRasterA)) {if isValid_REAL(output) {output += testRasterA;} else {output = testRasterA;} sum += 1.0;}"
                        }
                    },
                    {
                        "write": {
                            "name": "testRasterB",
                            "destination": "output_raster_areascript.tif"
                        }
                    }
                ]
            })";

        std::ofstream configFile;
        configFile.open("areascript_raster_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();

        Operation<REAL>::runFromConfigFile("areascript_raster_config.json");
        
        makeRaster(testRasterB, REAL, REAL);
        testRasterB.read("output_raster_areascript.tif");

        // Test values
        REQUIRE( testRasterB.hasData() == true );
        REQUIRE( testRasterB.getCellValue(10, 10) == ((REAL)99.9/(REAL)49.0) );

        testRasterB.closeFile();

        std::string filename = "areascript_raster_config.json"; 
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "test_raster_areascript.tif";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "output_raster_areascript.tif";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);        
    }

    // Test 5 (convert vector)
    SECTION("Convert vector") {
        // Create GeoJSON string
        std::string firstJson = R"({"features": [
            {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
                "properties": {"C": 10}, "type": "Feature"},
            {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
                "properties": {"C": 20}, "type": "Feature"},
            {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
                "properties": {"C": 30}, "type": "Feature"},
            {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
                "properties": {"C": 40}, "type": "Feature"},
            {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
                "properties": {"C": 50}, "type": "Feature"},
            {"geometry": {"coordinates": [2, 0.75], "type": "Point"},
                "properties": {"C": 60}, "type": "Feature"}
            ], "type": "FeatureCollection"})";

        auto v1 = GeoJson<REAL>::geoJsonToVector(firstJson);
        auto pointVector = v1.convert(GeometryType::Point);

        // write GeoJSON file
        std::ofstream vectorFile;
        vectorFile.open("test_convert_vector.geojson", std::ios::out | std::ios::trunc); 
        vectorFile << firstJson;
        vectorFile.close();
        
        auto jsonConfig = R"({
                "operations": [
                    {
                        "createVector": {
                              "name": "testVectorA",
                              "source": "test_convert_vector.geojson",
                              "projection": "epsg:4326"
                        }
                    },
                    {
                        "convertVector": {
                            "name": "testVectorA",
                            "type": "point",
                            "create": "testVectorB"
                        }
                    },
                    {
                        "write": {
                            "name": "testVectorB",
                            "destination": "output_convert_vector.geojson"
                        }
                    }
                ]
            })";
        
        std::ofstream configFile;
        configFile.open("convert_vector_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();

        Operation<REAL>::runFromConfigFile("convert_vector_config.json");
        
        auto v2 = GeoJson<REAL>::geoJsonFileToVector("output_convert_vector.geojson");
        
        REQUIRE(pointVector.getPointCount() == v2.getPointCount());
        REQUIRE(pointVector.getLineStringCount() == v2.getLineStringCount());
        REQUIRE(pointVector.getPolygonCount() == v2.getPolygonCount());

        std::string filename = "convert_vector_config.json";
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "test_convert_vector.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "output_convert_vector.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

    }
    
    // Test 6 (mapDistance)
    SECTION("mapDistance") {
        Vector<REAL> v1;
        v1.addProperty("level");
        auto v1p1 = v1.addPoint( {  1,  1} );
        auto v1p2 = v1.addPoint( { -1,  1} );
        auto v1p3 = v1.addPoint( { -1, -1} );
        v1.setProperty(v1p1, "level", (uint32_t)1);
        v1.setProperty(v1p2, "level", (uint32_t)1);
        v1.setProperty(v1p3, "level", (uint32_t)1);
        v1.setProperty(v1p1, "radius", (REAL)0.5);
        v1.setProperty(v1p2, "radius", (REAL)0.5);
        v1.setProperty(v1p3, "radius", (REAL)0.5);
        v1.setProjectionParameters(Projection::fromEPSG("4326"));
        
        auto vectorJson = GeoJson<REAL>::vectorToGeoJson(v1);
        
        // write GeoJSON file
        std::ofstream vectorFile;
        vectorFile.open("test_mapDistance.geojson", std::ios::out | std::ios::trunc); 
        vectorFile << vectorJson;
        vectorFile.close();
        
        
        auto jsonConfig = R"({
                "operations": [
                    {
                        "createVector": {
                              "name": "testVectorA",
                              "source": "test_mapDistance.geojson",
                              "projection": "+proj=longlat +datum=WGS84 +no_defs +type=crs"
                        }
                    },
                    {
                        "mapDistance": {
                            "name": "testVectorA",
                            "create": "testRasterA",
                            "resolution": 0.01
                        }
                    },
                    {
                        "write": {
                            "name": "testRasterA",
                            "destination": "output_mapDistance.tif"
                        }
                    }
                ]
            })";
            
        std::ofstream configFile;
        configFile.open("mapDistance_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();

        Operation<REAL>::runFromConfigFile("mapDistance_config.json");
        
        REAL delta = 0.01;
        makeRaster(distance, REAL, REAL);
        distance.read("output_mapDistance.tif");
        
        REQUIRE( fabs(distance.getNearestValue( 0,  0)-sqrt(2.0)) < delta );
        REQUIRE( fabs(distance.getNearestValue( 1,  0)-1.0) < delta );
        REQUIRE( fabs(distance.getNearestValue( 0, -1)-1.0) < delta );
        REQUIRE( fabs(distance.getNearestValue( 1,  1)) < delta );        

        distance.closeFile();

        std::string filename = "mapDistance_config.json";
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "test_mapDistance.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "output_mapDistance.tif";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);
    }

    // Test 7 (rasterise)
    SECTION("rasterise") {
    
    // Create GeoJSON string
    std::string vectorJson = R"({"features": [
        {"geometry": {"coordinates": [0, 1.5], "type": "Point"},
            "properties": {"A": 1, "level": 0}, "type": "Feature"},
        {"geometry": {"coordinates": [[0, 0], [1, 1], [2, 0], [3, 1]], "type": "LineString"},
            "properties": {"A": 2, "level": 1}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]], [[0.25, 0.25], [0.25, 0.75], [0.75, 0.75], [0.75, 0.25], [0.25, 0.25]]], "type": "Polygon"},
            "properties": {"A": 3, "level": 2}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 4, "level": 3}, "type": "Feature"},
        {"geometry": {"coordinates": [[[0.5, 0.5], [1.5, 0.5], [1.5, 1.5], [0.5, 1.5], [0.5, 0.5]]], "type": "Polygon"},
            "properties": {"A": 5, "level": 4}, "type": "Feature"}
        ], "type": "FeatureCollection"})";
        
        // write GeoJSON file
        std::ofstream vectorFile;
        vectorFile.open("test_rasterise.geojson", std::ios::out | std::ios::trunc); 
        vectorFile << vectorJson;
        vectorFile.close();
        
        auto jsonConfig = R"({
                "operations": [
                    {
                        "createVector": {
                              "name": "testVectorA",
                              "source": "test_rasterise.geojson",
                              "projection": "EPSG:4326"
                        }
                    },
                    {
                        "rasterise": {
                            "name": "testVectorA",
                            "create": "testRasterA",
                            "resolution": 0.02,
                            "script": "output = A;"
                        }
                    },
                    {
                        "write": {
                            "name": "testRasterA",
                            "destination": "output_rasterise.tif"
                        }
                    }
                ]
            })";
            
        std::ofstream configFile;
        configFile.open("rasterise_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();

        Operation<REAL>::runFromConfigFile("rasterise_config.json");
        
        makeRaster(testRasterB, REAL, REAL);
        testRasterB.read("output_rasterise.tif");
        
        REQUIRE( testRasterB.getCellValue(1, 76) == (REAL)1.0 );
        REQUIRE( testRasterB.getCellValue(25, 25) == (REAL)5.0 );
        REQUIRE( testRasterB.getCellValue(12, 12) == (REAL)3.0 );
        REQUIRE( testRasterB.getCellValue(45, 45) == (REAL)5.0 );
        REQUIRE( testRasterB.getCellValue(70, 51) == (REAL)5.0 );

        testRasterB.closeFile();

        std::string filename = "rasterise_config.json";
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "test_rasterise.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "output_rasterise.tif";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);
    }
    
    // Test 8 (vectorise)
    SECTION("Vectorise") {
        Vector<REAL> v1;
        v1.addPoint({ 0, 0 });
        v1.setProjectionParameters(Projection::fromEPSG("4326"));
        
        auto pointJson = GeoJson<REAL>::vectorToGeoJson(v1);
        
        // write GeoJSON file
        std::ofstream vectorFile;
        vectorFile.open("test_vectorise.geojson", std::ios::out | std::ios::trunc);
        vectorFile << pointJson;
        vectorFile.close();

       auto jsonConfig = R"({
                "operations": [
                    {
                        "createVector": {
                              "name": "testVectorA",
                              "source": "test_vectorise.geojson",
                              "projection": "+proj=longlat +datum=WGS84 +no_defs +type=crs"
                        }
                    },
                    {
                        "mapDistance": {
                            "name": "testVectorA",
                            "create": "testRasterA",
                            "resolution": 0.01,
                            "bounds": [-2, -2, 2, 2]
                        }
                    },
                    {
                        "vectorise": {
                            "name": "testRasterA",
                            "values": [1],
                            "nullValue": 2,
                            "create": "testVectorB"
                        }
                    },
                    {
                        "write": {
                            "name": "testVectorB",
                            "destination": "output_vectorise.geojson"
                        }
                    }
                ]
            })";
                    
        std::ofstream configFile;
        configFile.open("vectorise_config.json", std::ios::out | std::ios::trunc);
        configFile << jsonConfig;
        configFile.close();

        Operation<REAL>::runFromConfigFile("vectorise_config.json");
        
        auto vector_out = GeoJson<REAL>::geoJsonFileToVector("output_vectorise.geojson");
        
        // Check bounds
        auto bMin = vector_out.getBounds().min;
        auto bMax = vector_out.getBounds().max;

        REQUIRE( fabs(bMin.p+1.0) < 1.0E-3 );
        REQUIRE( fabs(bMin.q+1.0) < 1.0E-3 );
        REQUIRE( bMin.r == 0.0 );
        REQUIRE( bMin.s == 0.0 );

        REQUIRE( fabs(bMax.p-1.0) < 1.0E-3 );
        REQUIRE( fabs(bMax.q-1.0) < 1.0E-3 );
        REQUIRE( bMax.r == 0.0 );
        REQUIRE( bMax.s == 0.0 );
        
        std::string filename = "vectorise_config.json";
        auto rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "test_vectorise.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

        filename = "output_vectorise.geojson";
        rc = remove(filename.c_str());
        REQUIRE (rc == 0);

    }
}

auto totalTime = duration<double, std::micro>(steady_clock::now()-opTime).count()*1.0E-6;

#endif
