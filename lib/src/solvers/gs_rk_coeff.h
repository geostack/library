/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 

 /*
 * Get Runge-Kutta coeffs
 */
 static std::string getButcherTableau(std::string scheme){
    std::string butcherTableau;    
    if (scheme == "RK23") {
        // Bogacki-Shampine
        butcherTableau = R"(
            // Runge-Kutta coefficients for Bogacki-Shampine;
            #define _C2 (REAL)(1.0/2.0)
            #define _C3 (REAL)(3.0/4.0)
            #define _C4 (REAL)1.0

            #define _A21 (REAL)(1.0/2.0)

            #define _A31 (REAL)0.0
            #define _A32 (REAL)(3.0/4.0)

            #define _A41 (REAL)(2.0/9.0)
            #define _A42 (REAL)(1.0/3.0)
            #define _A43 (REAL)(4.0/9.0)

            #define _B1 (REAL)(7.0/24.0)
            #define _B2 (REAL)(1.0/4.0)
            #define _B3 (REAL)(1.0/3.0)
            #define _B4 (REAL)(1.0/8.0)
        )";
    } else if (scheme == "RK45") {
        // Dormand-Price https://core.ac.uk/download/pdf/81989096.pdf
        butcherTableau = R"(
            // Runge-Kutta coefficients for Dormand-Price;
            #define _C2 (REAL)(1.0/5.0)
            #define _C3 (REAL)(3.0/10.0)
            #define _C4 (REAL)(4.0/5.0)
            #define _C5 (REAL)(8.0/9.0)
            #define _C6 (REAL)1.0
            #define _C7 (REAL)1.0

            #define _A21 (REAL)(1.0/5.0)

            #define _A31 (REAL)(3.0/40.0)
            #define _A32 (REAL)(9.0/40.0)

            #define _A41 (REAL)(44.0/45.0)
            #define _A42 (REAL)(-56.0/15.0)
            #define _A43 (REAL)(32.0/9.0)

            #define _A51 (REAL)(19372.0/6561.0)
            #define _A52 (REAL)(-25360.0/2187.0)
            #define _A53 (REAL)(64448.0/6561.0)
            #define _A54 (REAL)(-212.0/729.0)

            #define _A61 (REAL)(9017.0/3168.0)
            #define _A62 (REAL)(-355.0/33.0)
            #define _A63 (REAL)(46732.0/5247.0)
            #define _A64 (REAL)(49.0/176.0)
            #define _A65 (REAL)(-5103.0/18656.0)

            #define _A71 (REAL)(35.0/384.0)
            #define _A72 (REAL)0.0
            #define _A73 (REAL)(500.0/1113.0)
            #define _A74 (REAL)(125.0/192.0)
            #define _A75 (REAL)(-2187.0/6784.0)
            #define _A76 (REAL)(11.0/84.0)

            #define _B1 (REAL)(5179.0/57600.0)
            #define _B2 (REAL)0.0
            #define _B3 (REAL)(7571.0/16695.0)
            #define _B4 (REAL)(393.0/640.0)
            #define _B5 (REAL)(-92097.0/339200.0)
            #define _B6 (REAL)(187.0/2100.0)
            #define _B7 (REAL)(1.0/40.0)
        )";
    }
    return butcherTableau;
}