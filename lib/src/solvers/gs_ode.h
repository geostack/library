/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_ODE_SOLVER_H
#define GEOSTACK_ODE_SOLVER_H

#include <string>
#include <vector>
#include <set>

#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_series.h"

// Link to external resources
extern const char R_cl_ode_c[];
extern const uint32_t R_cl_ode_c_size;

namespace Geostack
{
	
    /**
    * %ODE solver class for Lagrangian transport
    */
    template <typename TYPE>
    class ODE {

        public:
        
            /**
            * ODE constructor
            */
            ODE():initialised(false),
                iters(0), dt(0.0), time(0.0), variableSize(0) { }
            ~ODE() { }

            // Initialisation
            bool init(
                std::string jsonConfig,
                std::shared_ptr<Variables<TYPE, std::string> > variables_ = nullptr);

            // Execution
            bool step();
            TYPE getTime() {
                return time;
            }
            void setTime(TYPE time_);
            void setTimeStep(TYPE dt_);

            // Get series data
            TYPE get(std::string varName2, std::string varName1, TYPE var1, uint32_t index = 0);
            const Series<double, TYPE> &getSeries(std::string varName2, std::string varName1, uint32_t index = 0);

            bool isInitialised() {return initialised;};

        private:            

            // Internal variables
            volatile bool initialised; ///< Solver initialised
            uint64_t iters;            ///< Number of iterations
            TYPE dt;                   ///< Solver time step
            TYPE time;                 ///< Solver time

            // Internal data
            std::shared_ptr<Variables<TYPE, std::string> > variables; ///< Pointer to user-defined variables
            
            // Field data
            std::size_t variableSize;
            std::set<std::string> functionVariables;
            std::set<std::string> independentVariables;
            std::set<std::string> scriptVariables;
            std::set<std::string> variableArrays; ///< List of variable arrays

            // Series data
            void updateSeries();
            std::map<std::pair<std::string, std::string>, std::vector<Series<double, TYPE> > > seriesMap;

            // OpenCL data
            cl::Kernel solveKernel;        ///< Solution kernel
            size_t solveWorkitemMultiple;  ///< Solution kernel workitem multiple
    };
}

#endif