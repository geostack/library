/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_LEVEL_SET_SOLVER_H
#define GEOSTACK_LEVEL_SET_SOLVER_H

#include <chrono>
#include <string>
#include <vector>
#include <set>

#include "date.h"
#include "gs_raster.h"
#include "gs_variables.h"

// Link to external resources
extern const char R_cl_date_head_c[];
extern const uint32_t R_cl_date_head_c_size;
extern const char R_cl_level_set_advect_c[];
extern const uint32_t R_cl_level_set_advect_c_size;
extern const char R_cl_level_set_head_c[];
extern const uint32_t R_cl_level_set_head_c_size;
extern const char R_cl_level_set_init_c[];
extern const uint32_t R_cl_level_set_init_c_size;
extern const char R_cl_level_set_build_c[];
extern const uint32_t R_cl_level_set_build_c_size;
extern const char R_cl_level_set_update_c[];
extern const uint32_t R_cl_level_set_update_c_size;
extern const char R_cl_level_set_reinit_c[];
extern const uint32_t R_cl_level_set_reinit_c_size;

namespace Geostack
{    
    /**
    * %LevelSetParameters structure
    */
    template <typename TYPE>
    struct alignas(8) LevelSetParameters {
    
        TYPE time;           // Current time
        TYPE dt;             // Current time step
        TYPE maxSpeed;       // Maximum speed in domain
        TYPE area;           // Area within perimeter
        TYPE bandWidth;      // Width of narrow band in world units
        TYPE JulianDate;     // Current Julian date
    };

    /**
    * %LevelSet layer types.
    */
    namespace LevelSetLayers {
        enum Type {
            Distance,           ///< Distance raster
            DistanceUpdate,     ///< Distance update raster
            Rate,               ///< Time integration rate raster
            Speed,              ///< Speed raster
            Arrival,            ///< Arrival time raster
            Advect_x,           ///< Advection vector x component
            Advect_y,           ///< Advection vector y component
            LevelSetLayers_END  ///< Placeholder for count of layers
        };
    }

    /**
    * %Advective layer class
    */
    class AdvectiveLayer {

    public:
        AdvectiveLayer(): value(), reduction(Reduction::None), force(false) { }
        AdvectiveLayer(std::string _value, Reduction::Type _reduction, bool _force): 
            value(_value), reduction(_reduction), force(_force) { }
        
        std::string value;          // The variable or value to use when initialising
        Reduction::Type reduction;  // The reduction type, currently only mean for REAL and max for UINT types
        bool force;                 // Force creation of the output even if not specified in any scripts
    };
	
    /**
    * %LevelSet class for perimeter growth. 
    */
    template <typename TYPE>
    class LevelSet {

        public:
            using TimePoint = std::chrono::time_point<std::chrono::system_clock, std::chrono::milliseconds>;

            /**
            * LevelSet constructor
            */
            LevelSet():initialised(false), p(), resolution((TYPE)0.0), levels(1) {}
            ~LevelSet() { }

            // Initialisation
            bool init(
                std::string jsonConfig, 
                Vector<TYPE> sources_, 
                std::shared_ptr<Variables<TYPE, std::string> > variables_ = nullptr,
                std::vector<RasterBasePtr<TYPE> > inputLayers_ = std::vector<RasterBasePtr<TYPE> >(),
                std::vector<RasterBasePtr<TYPE> > outputLayers_ = std::vector<RasterBasePtr<TYPE> >());

            // Execution
            bool step();

            // Add source to level set
            void addSource(const Vector<TYPE> &v);

            // Force domain resize
            void resizeDomain(uint32_t nx, uint32_t ny, uint32_t ox, uint32_t oy);

            /**
            * Return level set classification %Raster
            */
            Raster<uint32_t, TYPE> &getClassification() { 
                return classbits;
            }

            /**
            * Return level set distance %Raster
            */
            Raster<TYPE, TYPE> &getDistance() { 
                return layersReal[LevelSetLayers::Distance];
            }

            /**
            * Return arrival time %Raster
            */
            Raster<TYPE, TYPE> &getArrival() { 
                return layersReal[LevelSetLayers::Arrival];
            }

            /**
            * Return advection x-component %Raster
            */
            Raster<TYPE, TYPE> &getAdvect_x() { 
                return layersReal[LevelSetLayers::Advect_x];
            }

            /**
            * Return advection y-component %Raster
            */
            Raster<TYPE, TYPE> &getAdvect_y() { 
                return layersReal[LevelSetLayers::Advect_y];
            }

            // Return named output layer
            RasterBase<TYPE> &getOutput(std::string name);

            //! Get all user-defined output layers.
            auto getUserDefinedOutputLayers() -> std::vector<std::reference_wrapper<RasterBase<TYPE>>>;
            
            /**
            * Return LevelSetLayer %Raster
            */
            Raster<TYPE, TYPE> &getLevelSetLayer(std::uint32_t LevelSetLayer);

            /**
            * Return solver %LevelSetParameters
            */
            LevelSetParameters<TYPE> &getParameters() { 
                return p;
            }

            /**
            * Return epoch time
            */
            uint64_t getEpochMilliseconds() { 
                return currentEpochMilliseconds;
            }

            /*! Get the start time of the solver, if it has been set (via the configuration string). 
                \return  The solver start time (as a system clock timepoint); or nullptr if it has not been set
             */ 
            auto getStartTime() const -> const TimePoint*;

        private:

            // Static variables
            static const TYPE CFL;                   ///< CFL ratio
            static const TYPE timeStepMax;           ///< Maximum time step
            static const TYPE narrowBandCells;       ///< Level set narrow band width
            
            static std::string buildNormal;          ///< Definition of normal vector
            static std::string buildAdvectNormal;    ///< Definition of advective normal vector
            static std::string buildAdvectMag;       ///< Definition of advective vector magnitude
            static std::string buildAdvectVector;    ///< Definition of advective vector
            static std::string buildAdvectDotNormal; ///< Definition of dot product of advective normal and advective vector

            // Internal variables
            uint64_t iters;                          ///< Number of iterations
            LevelSetParameters<TYPE> p;              ///< Parameter structure
            uint32_t levels;                         ///< Number of simulation levels
            uint32_t randomSeed;                     ///< Simulation random seed
            TYPE resolution;                         ///< Cell size in world units
            volatile bool initialised;               ///< Solver initialised
            TYPE lastTime;                           ///< Last time value
            TYPE timeMultiple;                       ///< Target time multiple
            Vector<TYPE> currentSources;             ///< Sources to be applied
            uint64_t advectiveLayerStructSize;       ///< Size of advective struct

            // Time and date variables
            bool hasStartDate;
            uint64_t currentEpochMilliseconds;
            uint64_t localZoneOffsetMinutes;
            TimePoint startTime;

            // Internal raster layers
            Raster<uint32_t, TYPE> classbits;               ///< Classification structure: 1 bit state, 23 bits class and 8 bits sub-class
            std::vector<Raster<uint32_t, TYPE> > layersInt; ///< User-defined integer input layers
            std::vector<Raster<TYPE, TYPE> > layersReal;    ///< User-defined floating point input layers

            // Internal generators
            RasterKernelRequirements reinitReq;            ///< Reinitialisation kernel raster requirements    

            RasterKernelGenerator initKernelGenerator;     ///< Initialisation generator
            RasterKernelGenerator advectKernelGenerator;   ///< Advection generator
            RasterKernelGenerator buildKernelGenerator;    ///< Build generator
            RasterKernelGenerator updateKernelGenerator;   ///< Update generator
            RasterKernelGenerator reinitKernelGenerator;   ///< Reinit generator

            RasterBaseRefs<TYPE> initRefs;           ///< Initialisation kernel rasters
            RasterBaseRefs<TYPE> advectRefs;         ///< Advection kernel rasters
            RasterBaseRefs<TYPE> buildRefs;          ///< Build kernel rasters
            RasterBaseRefs<TYPE> updateRefs;         ///< Update kernel rasters
            RasterBaseRefs<TYPE> reinitRefs;         ///< Reinitialisation kernel rasters
            RasterBaseRefs<TYPE> reinitFlipRefs;     ///< Reinitialisation kernel flipped distance rasters
            
            // Internal kernels
            cl::Kernel initKernel;           ///< Initialisation kernel
            cl::Kernel advectKernel;         ///< Advection kernel for active tiles
            cl::Kernel advectInactiveKernel; ///< Advection kernel for inactive tiles
            cl::Kernel buildKernel;          ///< Build kernel
            cl::Kernel updateKernel;         ///< Update kernel for active tiles
            cl::Kernel updateInactiveKernel; ///< Update kernel for inactive tiles
            cl::Kernel reinitKernel;         ///< Reinitialisation kernel
            std::size_t buildKernelMultiple; ///< Optimal workgroup multiple of build kernel
            
            // Solver data
            Vector<TYPE> sources;                                     ///< Level set sources
            std::shared_ptr<Variables<TYPE, std::string> > variables; ///< Pointer to user-defined variables
            std::vector<RasterBasePtr<TYPE> > inputLayers;            ///< User-defined input layers
            std::vector<RasterBasePtr<TYPE> > outputLayers;           ///< User-defined output layers
            std::map<std::string, std::size_t> outputLayerMap;        ///< User-defined output layer map
            std::map<std::string, AdvectiveLayer> advectiveLayers;    ///< Map of user-defined advective layers and initialisation value

            // Status data
            std::vector<cl_uint> statusVector;
            cl::Buffer statusBuffer;
            cl::Event statusMapEvent;

            // Raster index data
            std::vector<cl_uint> rasterIndexCount;   ///< Narrow band index count
            cl::Buffer rasterIndexCountBuffer;       ///< Narrow band index count buffer
            cl::Buffer rasterIndexBuffer;            ///< Narrow band index buffer
            cl::Event rasterIndexMapEvent;           ///< Narrow band index read event

            // Tile processing
            tileIndexSet newTileIndexes;             ///< Indexes of new tiles
            tileIndexSet activeTiles;                ///< Indexes of active tiles
            tileIndexSet inactiveTiles;              ///< Indexes of inactive tiles

            // Member functions
            Dimensions<TYPE> boundDimensions(Vector<TYPE> &v);
            void resizeTiles(uint32_t nx, uint32_t ny, uint32_t ox, uint32_t oy);
    };
}

#endif
