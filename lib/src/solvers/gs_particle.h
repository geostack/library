/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_PARTICLE_SOLVER_H
#define GEOSTACK_PARTICLE_SOLVER_H

#include <string>
#include <vector>
#include <set>
#include <random>

#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"

// Link to external resources
extern const char R_cl_random_c[];
extern const uint32_t R_cl_random_c_size;
extern const char R_cl_particle_init_c[];
extern const uint32_t R_cl_particle_init_c_size;
extern const char R_cl_particle_update_c[];
extern const uint32_t R_cl_particle_update_c_size;

namespace Geostack
{

    /**
    * Model type
    */
    namespace ParticleModelType {
        enum Type {
            None = 0,           ///< No model defined
            Acceleration  = 1,  ///< Acceleration-based non-stochasic
            Velocity = 2,       ///< Velocity-based non-stochasic
            Stochastic = 3,     ///< Stochastic
        };
    }
	
    /**
    * %Particle solver class for Lagrangian transport
    */
    template <typename TYPE>
    class Particle {

        public:
        
            /**
            * Particle constructor
            */
            Particle():initialised(false), addInitVars(false), addUpdateVars(false), 
                modelType(ParticleModelType::None), iters(0), dt(0.0), pci(0), 
                pcx(0), pcy(0), pcz(0), pnx(0), pny(0), pnz(0) { }
            ~Particle() { }

            // Initialisation
            bool init(
                std::string jsonConfig,
                Vector<TYPE> particles_,
                std::shared_ptr<Variables<TYPE, std::string> > variables_ = nullptr,
                std::vector<RasterBasePtr<TYPE> > inputLayers_ = std::vector<RasterBasePtr<TYPE> >());

            // Execution
            bool step();
            void setTimeStep(TYPE dt_);

            // Particle functions
            void addParticles(Vector<TYPE> particles_);
            Vector<TYPE> &getParticles();

            // Sample plane
            cl_uint getSamplePlaneIndexCount();
            std::vector<cl_uint> getSamplePlaneIndexes();

        private:            

            // Internal variables
            volatile bool initialised; ///< Solver initialised
            uint64_t iters;            ///< Number of iterations
            REAL dt;                   ///< Solver time step

            // Internal data
            ParticleModelType::Type modelType;                        ///< Solver model type
            Vector<TYPE> particles;                                   ///< Vector of particles stored as Points
            VariablesVector<Coordinate<TYPE> > dynamics;              ///< Dynamic particle information, velocity x, y, z components p, q, r and radius s
            uint32_t randomSeed;                                      ///< Simulation random seed
            VariablesVector<RandomState> randomState;                 ///< Random state vector, consists of 4 uints
            std::shared_ptr<Variables<TYPE, std::string> > variables; ///< Pointer to user-defined variables
            std::vector<RasterBasePtr<TYPE> > inputLayers;            ///< User-defined input layers

            // Sampling plane
            cl_uint pci;                    ///< Count of current particles crossing sample plane
            cl::Buffer bci;                 ///< Count buffer
            void *pbci;                     ///< Count mapping pointer
            VariablesVector<cl_uint> pi;    ///< Indexes of particles crossing sample plane
            TYPE pcx, pcy, pcz;             ///< Sample plane coordinate
            TYPE pnx, pny, pnz;             ///< Sample plane normal

            // Field data
            std::set<std::string> initFields;
            std::set<std::string> updateFields;
            std::set<std::string> accFields;

            // Variable data
            std::set<std::string> variableArrays; ///< List of variable arrays
            bool addInitVars;                     ///< Flag for variables in initialisation model
            bool addUpdateVars;                   ///< Flag for variables in update model

            // OpenCL data
            cl::Kernel initKernel;          ///< Initialisation kernel
            cl::Kernel updateKernel;        ///< Update kernel

            size_t initWorkitemMultiple;    ///< Init kernel workitem multiple
            size_t updateWorkitemMultiple;  ///< Update kernel workitem multiple
    };
}

#endif