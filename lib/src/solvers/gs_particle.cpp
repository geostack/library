/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <sstream>
#include <regex>

#include "json11/json11.hpp"

#include "gs_property.h"
#include "gs_particle.h"
#include "gs_rk_coeff.h"

namespace Geostack
{
    using namespace json11;

    template <typename TYPE>
    bool Particle<TYPE>::init(
        std::string jsonConfig,
        Vector<TYPE> particles_,
        std::shared_ptr<Variables<TYPE, std::string> > variables_,
        std::vector<RasterBasePtr<TYPE> > inputLayers_) {

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Reset iterations
        iters = 0;

        // Copy start conditions and layers
        particles = particles_;
        variables = variables_;
        inputLayers = inputLayers_;

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }

        // Get and check parameters
        dt = 0.0;
        if (config["dt"].is_number()) {
            dt = (REAL)config["dt"].number_value();
        }
        if (dt <= 0.0) {
            throw std::runtime_error("Time step must be greater than zero.");
        }

        // Get and random seed
        randomSeed = std::chrono::system_clock::now().time_since_epoch().count();
        if (config["randomSeed"].is_number()) {
            randomSeed = (uint32_t)config["randomSeed"].number_value();
        } else if (config["random_seed"].is_number()) {
            randomSeed = (uint32_t)config["random_seed"].number_value();
        }

        std::string scheme = "RK23";
        if (config["scheme"].is_string()) {
            scheme = (std::string)config["scheme"].string_value();
        }
        if (scheme != "RK23" && scheme != "RK45") {
            std::string err = std::string("Unrecognised scheme '") + scheme + "'";
            throw std::runtime_error(err);
        } else if (scheme == "RK45") {
            std::string err = std::string("Scheme '") + scheme + " not yet implemented.'";
            throw std::runtime_error(err);
        }

        // Define Butcher tableau for Adaptive Runge-Kutta scheme
        auto butcherTableau = getButcherTableau(scheme);

        uint32_t adaptiveMaximumSteps = 10;
        if (config["adaptiveMaximumSteps"].is_number()) {
            adaptiveMaximumSteps = (uint32_t)config["adaptiveMaximumSteps"].number_value();
        }
        if (adaptiveMaximumSteps == 0 || adaptiveMaximumSteps > 16) {
            throw std::runtime_error("Maximum adaptive steps must be between 1 and 2^16.");
        }

        double adaptiveTolerance = 1.0E-3;
        if (config["adaptiveTolerance"].is_number()) {
            adaptiveTolerance = (double)config["adaptiveTolerance"].number_value();
        }
        if (adaptiveTolerance < 0.0) {
            throw std::runtime_error("Adaptive tolerance must be greater than zero.");
        }        

        // Get scripts
        std::string initScript;
        if (config["initialisationScript"].is_string()) {
            initScript = config["initialisationScript"].string_value();
        }
        if (initScript.length() == 0) {
            std::cout << "WARNING: No 'initialisationScript' value supplied." << std::endl;
        }
        
        std::string advectScript;
        if (config["advectionScript"].is_string()) {
            advectScript = config["advectionScript"].string_value();
        }
        if (advectScript.length() == 0) {
            std::cout << "WARNING: No 'advectionScript' value supplied, default advection vector of (0, 0, 0) applied." << std::endl;
        }
        
        std::string postUpdateScript;
        if (config["postUpdateScript"].is_string()) {
            postUpdateScript = config["postUpdateScript"].string_value();
        }

        std::vector<std::string> updateScripts;
        if (config["updateScript"].is_array()) {
            for (auto script : config["updateScript"].array_items()) {
                updateScripts.push_back(script.string_value());
            }
        } else if (config["updateScript"].is_string()) {
            updateScripts.push_back(config["updateScript"].string_value());
        }
        if (updateScripts.size() == 0) {
            std::cout << "WARNING: No 'updateScript' value supplied." << std::endl;
            updateScripts.push_back("");
        }

        // Scan scripts
        modelType = ParticleModelType::None;
        if (updateScripts.size() == 1) {
            if (std::regex_search(updateScripts[0], std::regex("\\bacceleration\\b"))) {
                modelType = ParticleModelType::Acceleration;
            } else if (std::regex_search(updateScripts[0], std::regex("\\bvelocity\\b"))) {
                modelType = ParticleModelType::Velocity;
            }
        } else if (updateScripts.size() > 1) {
            modelType = ParticleModelType::Stochastic;
        }

        pcx = 0.0; pcy = 0.0; pcz = 0.0;
        pnx = 0.0; pny = 0.0; pnz = 1.0;
        bool useSamplePlane = false;
        if (config["samplingPlane"].is_object()) {
            auto samplingPlane = config["samplingPlane"];
                    
            // Set sample plane flag
            useSamplePlane = true;

            // Get plane point
            if (samplingPlane["point"].is_array()) {
                auto point = samplingPlane["point"].array_items();
                if (point.size() == 3) {
                    pcx = (TYPE)point[0].number_value();
                    pcy = (TYPE)point[1].number_value();
                    pcz = (TYPE)point[2].number_value();
                    
                    // Get plane normal
                    if (samplingPlane["normal"].is_array()) {
                        auto normal = samplingPlane["normal"].array_items();
                        if (normal.size() == 3) {
                            pnx = (TYPE)normal[0].number_value();
                            pny = (TYPE)normal[1].number_value();
                            pnz = (TYPE)normal[2].number_value();

                            // Ensure normal is normalised
                            TYPE norm = pnx*pnx+pny*pny+pnz*pnz;
                            if (norm != 0.0) {
                                pnx/=norm;
                                pny/=norm;
                                pnz/=norm;
                            }
                        }
                    }
                }
            }
        }

        // Search for fields
        initFields.clear();
        updateFields.clear();
        accFields.clear();
        auto &properties = particles.getProperties();
        for (auto n: properties.getPropertyNames()) {
            if (std::regex_search(initScript, std::regex("\\b" + n + "\\b"))) {
                auto type = properties.getPropertyType(n);
                if (type == PropertyType::Undefined ||
                    #if defined(REAL_FLOAT)
                    type == PropertyType::Float) {
                    #elif defined(REAL_DOUBLE)
                    type == PropertyType::Double) {
                    #endif

                    // Add index, integer and float fields, assume undefined will be populated
                    initFields.insert(n);
                }
            }

            if (std::regex_search(advectScript, std::regex("\\b" + n + "\\b")) || 
                std::regex_search(postUpdateScript, std::regex("\\b" + n + "\\b"))) {
                auto type = properties.getPropertyType(n);
                if (type == PropertyType::Undefined ||
                    #if defined(REAL_FLOAT)
                    type == PropertyType::Float) {
                    #elif defined(REAL_DOUBLE)
                    type == PropertyType::Double) {
                    #endif

                    // Add index, integer and float fields, assume undefined will be populated
                    updateFields.insert(n);
                }
            }

            for (auto &updateScript : updateScripts) {
                if (std::regex_search(updateScript, std::regex("\\b" + n + "\\b"))) {
                    auto type = properties.getPropertyType(n);
                    if (type == PropertyType::Undefined ||
                        #if defined(REAL_FLOAT)
                        type == PropertyType::Float) {
                        #elif defined(REAL_DOUBLE)
                        type == PropertyType::Double) {
                        #endif

                        // Add index, integer and float fields, assume undefined will be populated
                        accFields.insert(n);
                        updateFields.insert(n);
                    }
                }
            }
        }

        // Set fields
        std::string initArgsList, initVariableList, initPostList;
        for (auto &fieldName : initFields) {
                
            // Add to argument list       
            initArgsList += ",\n__global REAL *_" + fieldName;
            initVariableList += "REAL " + fieldName + " = *(_" + fieldName + "+index);\n";
            initPostList += "*(_" + fieldName + "+index) = " + fieldName + ";\n";
        }
        std::string updateArgsList, updateVariableList, updatePostList;
        for (auto &fieldName : updateFields) { 
            
            // Add to argument list
            updateArgsList += ",\n__global REAL *_" + fieldName;
            updateVariableList += "REAL " + fieldName + " = *(_" + fieldName + "+index);\n";
            updatePostList += "*(_" + fieldName + "+index) = " + fieldName + ";\n";
        }
        std::string fnArgsList, fnCallArgsList, fnVariableList;
        for (auto &fieldName : accFields) { 
            
            // Add to argument list
            fnArgsList += ",\nREAL " + fieldName;
            fnCallArgsList += ", " + fieldName;
        }

        // Update variable list
        addInitVars = false;
        addUpdateVars = false;
        variableArrays.clear();
        if (variables != nullptr && variables->hasData()) {

            // Get type
            auto vType = variables->getOpenCLTypeString();

            // Loop over variables
            bool addFnVars = false;
            for (auto vi : variables->getIndexes()) {

                // Check for duplicate names
                std::string vName = vi.first;
                if (updateFields.find(vName) != updateFields.end()) {
                    throw std::runtime_error("variable name '" + vName + "' cannot have the same name as another variable");
                }
                auto vFindVar = std::regex("\\b" + vName + "\\b");
                
                if (std::regex_search(initScript, vFindVar)) {
                    auto vFind = std::regex("\\b" + vName + "\\s*\\[\\s*(.+?)\\s*\\]");
                    if (std::regex_search(initScript, vFind)) {

                        // Use square brackets for variable array offset
                        initArgsList += ",\nconst uint " + vName + "_length";
                        initArgsList += ",\nconst uint " + vName + "_offset";
                        initScript = std::regex_replace(initScript, vFind, 
                            std::string("(*(_vars+") + vName + "_offset+min_uint_DEF(($1), " + vName + "_length-1)))");
                        variableArrays.insert(vName);
                    } else {

                        // Add variable to script
                        initVariableList += vType + " " + 
                            vName + " = *(_vars+" + std::to_string(vi.second) + ");\n";
                    }
                    addInitVars = true;
                }
                for (auto &updateScript : updateScripts) {
                    if (std::regex_search(updateScript, vFindVar)) {
                        auto vFind = std::regex("\\b" + vName + "\\s*\\[\\s*(.+?)\\s*\\]");
                        if (std::regex_search(updateScript, vFind)) {

                            // Use square brackets for variable array offset
                            updateArgsList += ",\nconst uint " + vName + "_length";
                            updateArgsList += ",\nconst uint " + vName + "_offset";
                            fnArgsList += ",\nconst uint " + vName + "_length";
                            fnArgsList += ",\nconst uint " + vName + "_offset";
                            fnCallArgsList += ", " + vName + "_length";
                            fnCallArgsList += ", " + vName + "_offset";
                            updateScript = std::regex_replace(updateScript, vFind, 
                                std::string("(*(_vars+") + vName + "_offset+min_uint_DEF(($1), " + vName + "_length-1)))");
                            variableArrays.insert(vName);
                        } else {

                            // Add variable to script
                            fnVariableList += vType + " " + 
                                vName + " = *(_vars+" + std::to_string(vi.second) + ");\n";
                        }
                        addUpdateVars = true;
                        addFnVars = true;
                    }
                }
                if (std::regex_search(advectScript, vFindVar)) {
                    auto vFind = std::regex("\\b" + vName + "\\s*\\[\\s*(.+?)\\s*\\]");
                    if (std::regex_search(advectScript, vFind)) {

                        // Use square brackets for variable array offset
                        updateArgsList += ",\nconst uint " + vName + "_length";
                        updateArgsList += ",\nconst uint " + vName + "_offset";
                        advectScript = std::regex_replace(advectScript, vFind, 
                            std::string("(*(_vars+") + vName + "_offset+min_uint_DEF(($1), " + vName + "_length-1)))");
                        variableArrays.insert(vName);
                    } else {

                        // Add variable to script
                        updateVariableList += vType + " " + 
                            vName + " = *(_vars+" + std::to_string(vi.second) + ");\n";
                    }                    
                    addUpdateVars = true;
                }
            }

            if (addInitVars) {
                initArgsList += ",\nconst __global " + vType + " *_vars\n";
            }
            if (addUpdateVars) {
                updateArgsList += ",\nconst __global " + vType + " *_vars\n";
            }
            if (addFnVars) {
                fnCallArgsList += ", _vars";
                fnArgsList += ",\nconst __global " + vType + " *_vars\n";
            }
        }

        // Parse scripts
        advectScript = Solver::processScript(advectScript);
        advectScript = std::regex_replace(advectScript, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
        advectScript = std::regex_replace(advectScript, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
        advectScript = std::regex_replace(advectScript, std::regex("\\brandomUniform\\("), std::string("randomUniform_REAL(_prsl, "));

        postUpdateScript = Solver::processScript(postUpdateScript);
        postUpdateScript = std::regex_replace(postUpdateScript, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
        postUpdateScript = std::regex_replace(postUpdateScript, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
        postUpdateScript = std::regex_replace(postUpdateScript, std::regex("\\brandomUniform\\("), std::string("randomUniform_REAL(_prsl, "));
        
        for (auto &script : updateScripts) {
            script = Solver::processScript(script);
        }

        // Check for stochastic solve
        if (modelType == ParticleModelType::Stochastic) {
            
            // Check for individual scripts
            std::vector<std::string> stochasticUpdateScripts(3);
            auto mu_regex = std::regex("\\bmu\\b");
            auto sigma_regex = std::regex("\\bsigma\\b");
            auto dW_regex = std::regex("\\bdW\\b");
            for (auto &script : updateScripts) {
                if (std::regex_search(script, mu_regex)) {
                    stochasticUpdateScripts[0] = script;
                } else if (std::regex_search(script, sigma_regex)) {
                    stochasticUpdateScripts[1] = script;
                } else if (std::regex_search(script, dW_regex)) {
                    stochasticUpdateScripts[2] = script;
                }
            }

            // Check required scripts are present
            if (stochasticUpdateScripts[0].size() != 0 && stochasticUpdateScripts[1].size() != 0) {
                
                // Add default uncorrelated Wiener process
                if (stochasticUpdateScripts[2].size() == 0) {                
                    stochasticUpdateScripts[2] = R"(
                    REAL sqrt_dt = sqrt(dt);
                    dW = (REALVEC3)(
                        sigma.x != 0.0 ? randomNormal_REAL(_prsl, 0.0, sqrt_dt) : 0.0,
                        sigma.y != 0.0 ? randomNormal_REAL(_prsl, 0.0, sqrt_dt) : 0.0,
                        sigma.z != 0.0 ? randomNormal_REAL(_prsl, 0.0, sqrt_dt) : 0.0);
                    )";
                }
                
                // Update scripts
                updateScripts = stochasticUpdateScripts;
                
                // Replace random definitions
                for (auto &script : updateScripts) {
                    script = std::regex_replace(script, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
                    script = std::regex_replace(script, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
                    script = std::regex_replace(script, std::regex("\\brandomUniform\\("), std::string("randomUniform_REAL(_prsl, "));
                }
            }
        }
        
        // Create initialisation kernel
        initScript = Solver::processScript(initScript);
        initScript = std::regex_replace(initScript, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
        initScript = std::regex_replace(initScript, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
        initScript = std::regex_replace(initScript, std::regex("\\brandomUniform\\("), std::string("randomUniform_REAL(_prsl, "));
        std::string initKernelStr = std::string(R_cl_random_c)+std::string(R_cl_particle_init_c);
        initKernelStr = std::regex_replace(initKernelStr, std::regex("\\/\\*__CODE__\\*\\/"), initScript);
        initKernelStr = std::regex_replace(initKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), initArgsList);
        initKernelStr = std::regex_replace(initKernelStr, std::regex("\\/\\*__VARS__\\*\\/"), initVariableList);
        initKernelStr = std::regex_replace(initKernelStr, std::regex("\\/\\*__POST__\\*\\/"), initPostList);
        
        auto initHash = solver.buildProgram(initKernelStr);
        if (initHash == solver.getNullHash()) {
            throw std::runtime_error("Cannot build program");
        }            
        initKernel = solver.getKernel(initHash, "init");
        initWorkitemMultiple = solver.getKernelPreferredWorkgroupMultiple(initKernel);

        // Create update kernel 
        std::string updateKernelStr = std::string(R_cl_random_c)+std::string(R_cl_particle_update_c);
        updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__MAX_STEPS__"), std::to_string(adaptiveMaximumSteps));
        updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__TOLERANCE__"), std::to_string(adaptiveTolerance));
        updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__RK_COEFF__\\*\\/"), butcherTableau);
        
        if (useSamplePlane) {
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__SAMPLE__|__SAMPLE__\\*\\/"), std::string(""));
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__S_C_X__"), std::to_string(pcx));
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__S_C_Y__"), std::to_string(pcy));
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__S_C_Z__"), std::to_string(pcz));
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__S_N_X__"), std::to_string(pnx));
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__S_N_Y__"), std::to_string(pny));
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("__S_N_Z__"), std::to_string(pnz));
        }
        if (modelType == ParticleModelType::Stochastic) {
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__INIT2__\\*\\/"), advectScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__CODE2__\\*\\/"), updateScripts[0]);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__CODE3__\\*\\/"), updateScripts[1]);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__CODE4__\\*\\/"), updateScripts[2]);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), updateArgsList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__VARS__\\*\\/"), updateVariableList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST1__\\*\\/"), postUpdateScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST2__\\*\\/"), updatePostList);
        } else {

            if (modelType == ParticleModelType::Acceleration) {
                updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__ACC_CODE__\\*\\/"), updateScripts[0]);
            } else if (modelType == ParticleModelType::Velocity) {
                updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__VEL_CODE__\\*\\/"), updateScripts[0]);
            }

            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__INIT1__\\*\\/"), advectScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), updateArgsList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__VARS__\\*\\/"), updateVariableList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST1__\\*\\/"), postUpdateScript);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__POST2__\\*\\/"), updatePostList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__FN_VARS__\\*\\/"), fnVariableList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__FN_ARGS__\\*\\/"), fnArgsList);
            updateKernelStr = std::regex_replace(updateKernelStr, std::regex("\\/\\*__FN_CALL_ARGS__\\*\\/"), fnCallArgsList);
        }

        auto updateHash = solver.buildProgram(updateKernelStr);
        if (updateHash == solver.getNullHash()) {
            throw std::runtime_error("Cannot build program");
        }
        if (modelType == ParticleModelType::Acceleration) {
            updateKernel = solver.getKernel(updateHash, "updateAcceleration");
        } else if (modelType == ParticleModelType::Velocity) {
            updateKernel = solver.getKernel(updateHash, "updateVelocity");
        } else if (modelType == ParticleModelType::Stochastic) {
            updateKernel = solver.getKernel(updateHash, "updateStochastic");
        } else {
            updateKernel = solver.getKernel(updateHash, "updateNone");
        }
        updateWorkitemMultiple = solver.getKernelPreferredWorkgroupMultiple(updateKernel);

        // Create sample plane buffer
        pci = 0;
        bci = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &pci);
        pbci = queue.enqueueMapBuffer(bci, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
        if (pbci != static_cast<void *>(&pci)) {
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }

        return true;
    }

    template <typename TYPE>
    bool Particle<TYPE>::step() {

        // Check size
        if (particles.hasData()) {

            // Get OpenCL solver handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            
            // Get particle count
            auto nParticles = particles.getVertexSize();

            // Set field kernel arguments
            std::vector<TYPE> field;
            const auto &pointIndexes = particles.getPointIndexes();
            if (nParticles != pointIndexes.size()) {
                throw std::runtime_error("Particles must be point geometry");
            }

            // Check dynamics vector size
            if (dynamics.size() != nParticles) {

                // Resize vectors
                auto lastSize = dynamics.size();
                dynamics.getData().resize(nParticles);
                randomState.getData().resize(nParticles);

                // Populate initialisation kernel
                cl_uint arg = 0;
                initKernel.setArg(arg++, particles.getVertexBuffer());
                initKernel.setArg(arg++, dynamics.getBuffer());
                initKernel.setArg(arg++, (cl_uint)lastSize);
                initKernel.setArg(arg++, (cl_uint)nParticles);
                initKernel.setArg(arg++, (cl_uint)randomSeed); // Random seed
                initKernel.setArg(arg++, randomState.getBuffer());

                // Add fields
                for (auto &fieldName : initFields) {
                    initKernel.setArg(arg++, particles.getPropertyBuffer(fieldName));   
                }
            
                // Add variables
                if (addInitVars) {
                    const auto &varIndexes = variables->getIndexes();
                    for (auto &vName : variableArrays) {
                        initKernel.setArg(arg++, (cl_uint)variables->getSize(vName));
                        initKernel.setArg(arg++, (cl_uint)varIndexes.at(vName));
                    }
                    initKernel.setArg(arg++, variables->getBuffer());
                }                       

                // Execute initialisation kernel
                auto size = nParticles-lastSize;
                size_t particlePaddedWorkgroupSize = 
                    (size_t)(size+((initWorkitemMultiple-(size%initWorkitemMultiple))%initWorkitemMultiple));
                queue.enqueueNDRangeKernel(initKernel, cl::NullRange, 
                    cl::NDRange(particlePaddedWorkgroupSize), cl::NDRange(initWorkitemMultiple));
            }
            
            // Check sample plane vector size
            pi.getData().assign(nParticles, getNullValue<cl_uint>());
            pci = 0;
            queue.enqueueUnmapMemObject(bci, pbci);

            // Populate update kernel arguments
            cl_uint arg = 0;
            if (modelType == ParticleModelType::Stochastic) {
                updateKernel.setArg(arg++, particles.getVertexBuffer());
                updateKernel.setArg(arg++, (cl_uint)nParticles);
                updateKernel.setArg(arg++, dt);
                updateKernel.setArg(arg++, randomState.getBuffer());
            } else {
                updateKernel.setArg(arg++, particles.getVertexBuffer());
                updateKernel.setArg(arg++, dynamics.getBuffer());
                updateKernel.setArg(arg++, (cl_uint)nParticles);
                updateKernel.setArg(arg++, dt);
                updateKernel.setArg(arg++, randomState.getBuffer());
                updateKernel.setArg(arg++, pi.getBuffer());
                updateKernel.setArg(arg++, bci);
            }

            // Add fields
            for (auto &fieldName : updateFields) {
                updateKernel.setArg(arg++, particles.getPropertyBuffer(fieldName));   
            }
            
            // Add variables
            if (addUpdateVars) {
                const auto &varIndexes = variables->getIndexes();
                for (auto &vName : variableArrays) {
                    updateKernel.setArg(arg++, (cl_uint)variables->getSize(vName));
                    updateKernel.setArg(arg++, (cl_uint)varIndexes.at(vName));
                }
                updateKernel.setArg(arg++, variables->getBuffer());
            }

            // Execute update kernel
            size_t particlePaddedWorkgroupSize = 
                (size_t)(nParticles+((updateWorkitemMultiple-(nParticles%updateWorkitemMultiple))%updateWorkitemMultiple));
            queue.enqueueNDRangeKernel(updateKernel, cl::NullRange, 
                cl::NDRange(particlePaddedWorkgroupSize), cl::NDRange(updateWorkitemMultiple));

            // Map sample count buffer
            pbci = queue.enqueueMapBuffer(bci, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
            if (pbci != static_cast<void *>(&pci))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }

        return true;
    }

    template <typename TYPE>
    void Particle<TYPE>::setTimeStep(TYPE dt_) {

        // Check time step
        if (dt_ <= 0.0) {
            throw std::runtime_error("Time step must be greater than zero.");
        }

        // Update time step
        dt = dt_;
    }

    template <typename TYPE>
    void Particle<TYPE>::addParticles(Vector<TYPE> particles_) {

        if (particles_.hasData()) {

            // Add particles to solver
            auto proj = particles.getProjectionParameters();
            if (particles.getProjectionParameters() != proj) {

                // Convert projection
                particles += particles_.convert(proj);
            } else {
                particles += particles_;
            }
        }
    }
    
    template <typename TYPE>
    cl_uint Particle<TYPE>::getSamplePlaneIndexCount() {
        return pci;
    }
    
    template <typename TYPE>
    std::vector<cl_uint> Particle<TYPE>::getSamplePlaneIndexes() {

        // Get sample plane indexes
        std::vector<cl_uint> r;
        if (modelType != ParticleModelType::Stochastic) {
            if (pci > 0) {
                auto &iv = pi.getData();
                r.assign(iv.begin(), iv.begin()+std::min((std::size_t)pci, iv.size()));
            }
        }
        return r;
    }
    
    template <typename TYPE>
    Vector<TYPE> &Particle<TYPE>::getParticles() {
        return particles;
    }

    // Float type definitions
    template class Particle<float>;

    // Double type definitions
    template class Particle<double>;
}
