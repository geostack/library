/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <limits>
#include <cmath>
#include <cstring>
#include <sstream>
#include <numeric>
#include <regex>
#include <set>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <random>

#include "gs_models.h"
#include "gs_solver.h"
#include "gs_raster.h"
#include "gs_vector.h"
#include "gs_string.h"
#include "gs_dap.h"
#include "gs_ascii.h"
#include "gs_flt.h"
#include "gs_geotiff.h"
#include "gs_netcdf.h"
#include "gs_gsr.h"
#include "gs_png.h"

namespace Geostack
{
    /**
     * Get maximum possible size of a given raster tile
     * @param rasterBaseRefs %RasterBaseRefs object.
     * @return tile size
    */
    template <typename CTYPE>
    float getMaxTileDataSize(RasterBaseRefs<CTYPE> rasterBaseRefs) {
        auto rasterCount = rasterBaseRefs.size();
        float maxTileSize = 0.0;
        for (size_t i = 0; i < rasterCount; i++) {
            RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
            auto rDim = rasterBase.getRasterDimensions();
            maxTileSize = std::max(maxTileSize,
                                   (float)(rDim.d.nz * TileMetrics::tileSizeSquared * sizeof(CTYPE)));
        }
        maxTileSize = (float)(maxTileSize / std::pow(1024, 3));
        return maxTileSize;
    }

    /**
    * %Dimension structure output stream
    * @param os output stream.
    * @param r %RasterDimensions structure.
    * @return updated ostream.
    */
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const Dimensions<CTYPE> &r) {

        // Write data
        return os
            << sizeof(r) << ", " << 
            r.nx << ", " <<
            r.ny << ", " << 
            r.nz << ", " << 
            r.hx << ", " << 
            r.hy << ", " << 
            r.hz << ", " << 
            r.ox << ", " << 
            r.oy << ", " << 
            r.oz;
    }

    /**
    * %Raster dimension structure output stream
    * @param os output stream.
    * @param r %RasterDimensions structure.
    * @return updated ostream.
    */
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const RasterDimensions<CTYPE> &r) {

        // Write data
        return os
            << sizeof(r) << ", " << 
            r.d.nx << ", " <<
            r.d.ny << ", " << 
            r.d.nz << ", " << 
            r.d.hx << ", " << 
            r.d.hy << ", " << 
            r.d.hz << ", " << 
            r.d.ox << ", " << 
            r.d.oy << ", " << 
            r.d.oz << ", " << 
            r.ex << ", " << 
            r.ey << ", " << 
            r.ez << ", " << 
            r.tx << ", " << 
            r.ty;
    }
    
    /**
    * Singleton instance of RasterKernelCache
    */
    RasterKernelCache& RasterKernelCache::getKernelCache() {

        static RasterKernelCache kernelCache;
        return kernelCache;
    }

    void RasterKernelCache::setKernelGenerator(std::size_t scriptHash, RasterKernelGenerator kernelGenerator) {

        // Check for generator
        generatorMap[scriptHash] = kernelGenerator;
    }

    RasterKernelGenerator RasterKernelCache::getKernelGenerator(std::size_t scriptHash) {

        // Check for generator
        auto it = generatorMap.find(scriptHash);
        if (it != generatorMap.end()) {
            return it->second;
        }
        return RasterKernelGenerator();
    }

    /**
    * %RasterFileHandler constructor.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::RasterFileHandler() {

        // Create file stream
        fileStream = std::make_shared<std::fstream>();
    }

    /**
    * RasterFileHandler copy constructor.
    * @param rf %RasterFileHandler to copy.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::RasterFileHandler(const RasterFileHandler<RTYPE, CTYPE> &rf): 
        readDataHandler(rf.readDataHandler), writeDataHandler(rf.writeDataHandler) {

        // Set file stream
        if (fileStream == nullptr)
            fileStream = std::make_shared<std::fstream>();
        else
            fileStream = rf.fileStream;
    }

    /**
    * %RasterFileHandler destructor.
    */
    template <typename RTYPE, typename CTYPE>
    RasterFileHandler<RTYPE, CTYPE>::~RasterFileHandler() {
    }
    
    /**
     * %RasterFileHandler close file.
    */
    template <typename RTYPE, typename CTYPE>
    void RasterFileHandler<RTYPE, CTYPE>::closeFile() {
        if (fileStream != nullptr) {
            if (fileStream->is_open()) {
                fileStream->close();
            }
        }
    }

    /**
    * Register function to read tile data to %Raster.
    * @param dataHandler_ function for data handler.
    */
    template <typename RTYPE, typename CTYPE>
    void RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(dataHandlerReadFunction<RTYPE, CTYPE> dataHandler_) {

        // Move function handle
        readDataHandler = std::move(dataHandler_);
    }

    /**
    * Register function to write tile data from %Raster.
    * @param dataHandler_ function for data handler.
    */
    template <typename RTYPE, typename CTYPE>
    void RasterFileHandler<RTYPE, CTYPE>::registerWriteDataHandler(dataHandlerWriteFunction<RTYPE, CTYPE> dataHandler_) {

        // Move function handle
        writeDataHandler = std::move(dataHandler_);
    }

    /**
    * Default %RasterBase constructor
    * dimensions will be POD initialised to zero
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::RasterBase():
        PropertyMap(),
        regionCacheBufferSize(0),
        dim(),
        proj(),
        interpolation(RasterInterpolation::Nearest),
        isConst(false) {

        // Create variable data
        vars = std::make_shared<Variables<CTYPE, std::string> >();
    }
        
    /**
    * Destructor
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::~RasterBase() {

        // Ensure queue operations are finished to prevent buffer deallocation before kernel execution
        auto &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        queue.finish();
    }

    /**
    * Copy constructor
    * Creates shallow copy
    */
    template <typename CTYPE>
    RasterBase<CTYPE>::RasterBase(const RasterBase<CTYPE> &rb):
        PropertyMap(rb),
        regionCacheBufferSize(rb.regionCacheBufferSize),
        vars(rb.vars),
        proj(rb.proj),
        dim(rb.dim),
        interpolation(rb.interpolation),
        isConst(rb.isConst) {

        // Copy data
        tree = rb.tree;
        coordsCache = rb.coordsCache;
        regionCacheBuffer = rb.regionCacheBuffer;
    }

    /**
    * Assignment operator
    * Creates shallow copy
    * @param r %Raster to assign.
    */
    template <typename CTYPE>
    RasterBase<CTYPE> &RasterBase<CTYPE>::operator=(const RasterBase<CTYPE> &rb) {
    
        if (this != &rb) {

            // Derived assigment
            PropertyMap::operator=(rb);

            // Copy variables
            vars = rb.vars;
            proj = rb.proj;
            tree = rb.tree;
            dim = rb.dim;
            interpolation = rb.interpolation;
            isConst = rb.isConst;
            
            // Copy data
            tree = rb.tree;
            coordsCache = rb.coordsCache;
            regionCacheBufferSize = rb.regionCacheBufferSize;
            regionCacheBuffer = rb.regionCacheBuffer;
        }

        return *this; 
    }

    /**
    * Set %RasterBase %ProjectionParameters.
    */
    template <typename CTYPE>
    void RasterBase<CTYPE>::setProjectionParameters(ProjectionParameters<double> proj_) {

        // Set projection
        proj = proj_;
    }

    /**
     * Return variable names for %RasterBase
     * @return set of variable names
    */
    template <typename CTYPE>
    std::set<std::string> RasterBase<CTYPE>::getVariableNames() {
        std::set<std::string> varnames;
        if (vars != nullptr) {
            for (auto it: vars->getIndexes()) {
                varnames.insert((std::string)it.first);
            }
        }
        return varnames;
    }

    /**
    * Return variable data for %RasterBase.
    * @param name the variable name.
    * @return copy of variable data vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    RTYPE RasterBase<CTYPE>::getVariableData(std::string name) {

        // Check data
        if (vars != nullptr) {
            return vars->get(name);
        } else {
            return getNullValue<RTYPE>();
        }
    }

    /**
    * Return variable data for %RasterBase.
    * @param name the variable name.
    * @param index the array index to set.
    * @return copy of variable data vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    RTYPE RasterBase<CTYPE>::getVariableData(std::string name, std::size_t index) {

        // Check data
        if (vars != nullptr) {
            return vars->get(name, index);
        } else {
            return getNullValue<RTYPE>();
        }
    }

    /**
    * Set variable data for %RasterBase.
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    void RasterBase<CTYPE>::setVariableData(std::string name, RTYPE value) {

        // Check data
        if (vars != nullptr) {
            vars->set(name, value);
        }
    }

    /**
    * Set variable data for %RasterBase.
    * @param name the variable name.
    * @param value the variable value to set.
    * @param index the array index to set.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    void RasterBase<CTYPE>::setVariableData(std::string name, RTYPE value, std::size_t index) {

        // Check data
        if (vars != nullptr) {
            vars->set(name, value, index);
        }
    }
    
    /**
    * Return variable indexes for %RasterBase.
    * @return index map of variables.
    */
    template <typename CTYPE>
    const std::map<std::string, std::size_t> &RasterBase<CTYPE>::getVariablesIndexes() {

        // Check data
        if (vars != nullptr) {
            return vars->getIndexes();
        } else {
            throw std::runtime_error("RasterBase has no variables");
        }
    }
    
    /**
    * Return variable array size for %RasterBase.
    * @return array size of variables.
    */
    template <typename CTYPE>
    const std::size_t RasterBase<CTYPE>::getVariableSize(std::string name) {

        // Check data
        if (vars != nullptr) {
            return vars->getSize(name);
        } else {
            throw std::runtime_error("RasterBase has no variables");
        }
    }
    
    /**
    * Return variable buffer for %RasterBase.
    * @return reference to variable buffer.
    */
    template <typename CTYPE>
    cl::Buffer &RasterBase<CTYPE>::getVariablesBuffer() {

        // Check data
        if (vars != nullptr) {
            return vars->getBuffer();
        } else {
            throw std::runtime_error("RasterBase has no variables");
        }
    }

    /**
    * Return variable type for %RasterBase.
    * @return string type of variables.
    */
    template <typename CTYPE>
    std::string RasterBase<CTYPE>::getVariablesType() {

        // Check data
        if (vars != nullptr) {
            return vars->getOpenCLTypeString();
        } else {
            throw std::runtime_error("RasterBase has no variables");
        }
    }
    
    template <typename CTYPE>
    void RasterBase<CTYPE>::deleteVariableData() {
        if (vars != nullptr) {
            vars->clear();
        }
    }
    
    /**
    * Return cache buffer of given size for %RasterBase.
    * @param dataSize required data size.
    * @return Buffer of given size.
    */
    template <typename CTYPE>
    cl::Buffer &RasterBase<CTYPE>::getRegionCacheBuffer(std::size_t dataSize) {

        if (regionCacheBufferSize < dataSize) {

            // Create new buffer
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            regionCacheBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, dataSize);
            regionCacheBufferSize = dataSize;
        }

        // Return reference to buffer
        return regionCacheBuffer;
    }

    /**
    * Get hash string.
    * @return hash string for %RasterBase.
    */
    template <typename CTYPE>
    std::string RasterBase<CTYPE>::getHashString() {
        std::string hs;
        hs += "name [" + PropertyMap::template getProperty<std::string>("name") + "] ";
        hs += "type [" + getOpenCLTypeString() + "] ";
        hs += "proj [" + proj.str() + "] ";
        hs += "nz [" + std::to_string(dim.d.nz) + "] ";
        hs += "int [" + std::to_string(getInterpolationType()) + "] ";            
        hs += "nbr [" + std::to_string(getRequiredNeighbours()) + "] ";
        hs += "red [" + std::to_string(getReductionType()) + "] ";
        hs += "stat [" + std::to_string(getNeedsStatus()) + "] ";
        hs += "read [" + std::to_string(getNeedsRead()) + "] ";
        hs += "wrt [" + std::to_string(getNeedsWrite()) + "] ";
        return hs;
    }

    /**
     * Get footprint of %RasterBase.
     * @return footprint of %RasterBase
    */
    template <typename CTYPE>
    Vector<CTYPE> RasterBase<CTYPE>::getRasterFootprint() {
        auto v = Vector<CTYPE>();
        CoordinateList<CTYPE> cs;
        std::vector<CoordinateList<CTYPE> > pcs;
        
        CTYPE pointx = 0.0;
        CTYPE pointy = 0.0;
        
        // bottom edge
        for (int i = 0; i < dim.d.nx; i++) {
            pointx = dim.d.ox + i * dim.d.hx;
            pointy = dim.d.oy;
            cs.push_back({pointx, pointy});
        }

        // right edge
        pointx += dim.d.hx;
        for (int i = 0; i < dim.d.ny; i++) {
            pointy = dim.d.oy + i * dim.d.hy;
            cs.push_back({pointx, pointy});
        }

        //top edge
        pointy += dim.d.hy;
        for (int i = dim.d.nx; i > -1; i--) {
            pointx = dim.d.ox + i * dim.d.hx;
            cs.push_back({pointx, pointy});
        }


        // left edge
        for (int i = dim.d.ny-1; i > 0; i--) {
            pointy = dim.d.oy + i * dim.d.hy;
            cs.push_back({pointx, pointy});
        }

        pcs.push_back(cs);
        v.addPolygon(pcs);
        
        return v;
    }

    /**
    * Default %Raster constructor
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster():
        requiredNeighbours(Neighbours::None),
        reductionType(Reduction::None),
        needsStatus(false),
        needsRead(true),
        needsWrite(true) {

        // Set empty name
        RasterBase<CTYPE>::template setProperty<std::string>("name", "");
    }
    
    /**
     * Static %Raster types
    */
    template <>
    RasterDataType::Type Raster<uint8_t, float>::getRasterDataType() {
        return RasterDataType::UInt8;
    }
    
    template <>
    RasterDataType::Type Raster<uint32_t, float>::getRasterDataType() {
        return RasterDataType::UInt32;
    }
    
    template <>
    RasterDataType::Type Raster<float, float>::getRasterDataType() {
        return RasterDataType::Float;
    }
    
    template <>
    RasterDataType::Type Raster<uint8_t, double>::getRasterDataType() {
        return RasterDataType::UInt8;
    }
    
    template <>
    RasterDataType::Type Raster<uint32_t, double>::getRasterDataType() {
        return RasterDataType::UInt32;
    }
    
    template <>
    RasterDataType::Type Raster<double, double>::getRasterDataType() {
        return RasterDataType::Double;
    }

    /**
    * Named %Raster constructor
    * dimensions will be POD initialised to zero
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster(std::string name_):
        requiredNeighbours(Neighbours::None),
        reductionType(Reduction::None),
        needsStatus(false),
        needsRead(true),
        needsWrite(true) {

        // Set name
        RasterBase<CTYPE>::template setProperty<std::string>("name", name_);
    }
        
    /**
    * Destructor
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::~Raster() {
    }

    /**
    * Copy constructor
    * Creates shallow copy
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE>::Raster(const Raster<RTYPE, CTYPE> &r):
        RasterBase<CTYPE>::RasterBase(r),
        requiredNeighbours(r.requiredNeighbours),
        reductionType(r.reductionType),
        needsStatus(r.needsStatus),
        needsRead(r.needsRead),
        needsWrite(r.needsWrite) {

        // Copy data
        tiles.clear();
        tiles = r.tiles;
        fileHandlerIn = r.fileHandlerIn;
        fileHandlerOut = r.fileHandlerOut;

        // Copy buffers
        nullBuffer = r.nullBuffer;
        randomBuffer = r.randomBuffer;
    }

    /**
    * Assignment operator
    * Creates shallow copy
    * @param r %Raster to assign.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> &Raster<RTYPE, CTYPE>::operator=(const Raster<RTYPE, CTYPE> &r) { 

        if (this != &r) {

            // Get name
            auto name = PropertyMap::template getProperty<std::string>("name");
            if (name.empty()) {

                // Get name from input raster
                name = r.template getProperty<std::string>("name");
            }

            // Derived assigment
            RasterBase<CTYPE>::operator=(r);

            // Copy data
            tiles.clear();
            tiles = r.tiles;
            requiredNeighbours = r.requiredNeighbours;
            reductionType = r.reductionType;
            needsStatus = r.needsStatus;
            needsRead = r.needsRead;
            needsWrite = r.needsWrite;
            fileHandlerIn = r.fileHandlerIn;
            fileHandlerOut = r.fileHandlerOut;

            // Copy buffers
            nullBuffer = r.nullBuffer;
            randomBuffer = r.randomBuffer;

            // Set name
            RasterBase<CTYPE>::template setProperty<std::string>("name", name);
        }

        return *this; 
    }    

    /**
    * Set %Raster origin for 2D rasters
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setOrigin_z(CTYPE oz_) {
        if (dim.d.nz == 1) {
            dim.d.oz = oz_;
            dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

            // Initialise tiles
            for (uint32_t tj = 0; tj < dim.ty; tj++)
                for (uint32_t ti = 0; ti < dim.tx; ti++) {
                    TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
                    tp->setOrigin_z(oz_);
                }

        } else {
            throw std::runtime_error("Origin can only be set for 2D rasters");
        }
    }

    /**
    * Get type string for RTYPE data.
    * @return data type string.
    */
    template <typename RTYPE, typename CTYPE>
    std::string Raster<RTYPE, CTYPE>::getDataTypeString() {
        return Solver::getTypeString<RTYPE>();
    }
    
    /**
    * Get type string for RTYPE data.
    * @return RTYPE as OpenCL compatible type string.
    */
    template <typename RTYPE, typename CTYPE>
    std::string Raster<RTYPE, CTYPE>::getOpenCLTypeString() {
        return Solver::getOpenCLTypeString<RTYPE>();
    }
    
    /**
    * Get type size for RTYPE data.
    * @return size of RTYPE in bytes.
    */
    template <typename RTYPE, typename CTYPE>
    std::size_t Raster<RTYPE, CTYPE>::getOpenCLTypeSize() {
        return sizeof(RTYPE);
    }

    /**
    * %Raster comparison operator
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Raster<RTYPE, CTYPE> &l, const Raster<RTYPE, CTYPE> &r) { 

        // Compare objects
        if (&l == &r) {
            return true;
        }

        // Compare metrics
        if (!equalSpatialMetrics(l.dim.d, r.dim.d)) {
            return false;
        }
        
        // Compare number of tiles
        auto &ltiles = l.tiles;
        auto &rtiles = r.tiles;
        if (ltiles.size() != rtiles.size()) {
            return false;
        }
        
        // Compare tiles
        for (std::size_t c = 0; c < ltiles.size(); c++) {
            
            // Compare tile values
            if (!tileDataEqual<RTYPE, CTYPE>(*ltiles[c], *rtiles[c])) {
                return false;
            }
        }

        return true; 
    }

    /**
    * Search Rtree for the given bounds and get the tiles.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::searchTiles(BoundingBox<CTYPE> bounds,
                                          std::vector<GeometryBasePtr<CTYPE> > &tileList) {
        // Check raster
        if (hasData()) {

            // Get region
            tree.search(bounds, tileList, GeometryType::Tile);
        } else {
            std::stringstream err;
            err << "Raster is not initialised";
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Search Rtree for a given bounds and get the nearest tiles.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::nearestTiles(BoundingBox<CTYPE> bounds,
                                            std::vector<GeometryBasePtr<CTYPE> > &tileList) {
        // Check raster
        if (hasData()) {

            // Get region
            tree.nearest(bounds, tileList, GeometryType::Tile);
        } else {
            std::stringstream err;
            err << "Raster is not initialised";
            throw std::runtime_error(err.str());
        }
    }
        
    /**
    * %Raster %Tile creation.
    * Creates %Tile at index ti and tj
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE> &Raster<RTYPE, CTYPE>::getTile(uint32_t ti, uint32_t tj) {

        std::stringstream err;
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            err << "Requested tile " << ti << "," << tj << " out of range in raster";
            throw std::range_error(err.str());
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            err << "Requested tile " << ti+tj*dim.tx << " in raster is empty";
            throw std::range_error(err.str());
        }
            
        // Get tile
        Tile<RTYPE, CTYPE> &tile = *tp;
            
#ifdef USE_TILE_CACHING

        // Populate tile
        if (!tile.hasData()) {

            // Create data
            tile.createData();

            if (tile.isCached()) {

                // Read from cache
                tile.cacheRead();

            } else {
                    
                // Populate data
                if (fileHandlerIn != nullptr) {

                    // The file handler may not write to tile edges, so clear boundary tiles
                    if (ti == (dim.tx-1) || tj == (dim.ty-1)) {
                        tile.clearData();
                    }

                    // Read from file
                    tile.readData(*this, fileHandlerIn->getReadDataHandler());

                } else {

                    // Set to null
                    tile.clearData();
                }
            }
        }           

        // Update cache
        auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
        tileCacheManager.updateHost(tp.get(), this->getConst());

#else

        // Populate tile
        if (!tile.hasData()) {

            // Create data
            tile.createData();
                    
            // Populate data
            if (fileHandlerIn != nullptr) {

                // The file handler may not write to tile edges, so clear boundary tiles
                if (ti == (dim.tx-1) || tj == (dim.ty-1)) {
                    tile.clearData();
                }

                // Read from file
                tile.readData(*this, fileHandlerIn->getReadDataHandler());

            } else {

                // Set to null
                tile.clearData();
            }
        }           

#endif

        // Return tile pointer
        return tile;
    }  

    /**
    * %Raster null %Tile buffer.
    * @return Buffer from Tile containing null data
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getNullBuffer() {
    
        // Check for tile
        if (!nullBuffer) {
        
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Create buffer
            std::size_t dataSize = dim.d.nz*TileMetrics::tileSizeSquared;
            nullBuffer = std::make_shared<cl::Buffer>(context, CL_MEM_READ_WRITE, dataSize*sizeof(RTYPE));
            solver.fillBuffer<RTYPE>(*nullBuffer, getNullValue<RTYPE>(), dataSize);
        }
        
        // Return buffer
        return *nullBuffer;
    }  

    /**
    * %Raster random %Tile buffer.
    * @return Buffer from Tile containing random data
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getRandomBuffer() {
    
        // Check for tile
        if (!randomBuffer) {
        
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Create buffer
            cl_uint N = (cl_uint)dim.d.nz*TileMetrics::tileSizeSquared;
            std::vector<cl_uint> seeds(N);

            // Get raster seed
            cl_uint random_seed = std::chrono::system_clock::now().time_since_epoch().count(); // Default random seed
            if (RasterBase<CTYPE>::hasProperty("random_seed")) {
                cl_uint rasterSeed = RasterBase<CTYPE>::template getProperty<cl_uint>("random_seed");
                if (isValid<cl_uint>(rasterSeed)) {
                    random_seed = rasterSeed; 
                }
            } else if (RasterBase<CTYPE>::hasProperty("randomSeed")) {
                cl_uint rasterSeed = RasterBase<CTYPE>::template getProperty<cl_uint>("randomSeed");
                if (isValid<cl_uint>(rasterSeed)) {
                    random_seed = rasterSeed; 
                }
            }

            // Create seeds
            std::mt19937 rng(static_cast<unsigned int>(random_seed));
            std::uniform_int_distribution<cl_uint> dist(0, std::numeric_limits<cl_uint>::max());
            for (cl_uint i = 0; i < N; i++) {
                seeds[i] = dist(rng);
            }

            // Create buffers
            AutoBuffer<cl_uint> seedBuffer(context, queue, seeds);
            randomBuffer = std::make_shared<cl::Buffer>(context, CL_MEM_READ_WRITE, N*sizeof(RandomState));
            randomBufferSnapshot = cl::Buffer(context, CL_MEM_READ_WRITE, N*sizeof(RandomState));

            // Initialise buffer
            std::size_t clHash = solver.buildProgram(Models::cl_random_c);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build program");
            }

            auto initRandomBufferKernel = solver.getKernel(clHash, "initRandomBuffer");
            initRandomBufferKernel.setArg(0, *randomBuffer);
            initRandomBufferKernel.setArg(1, seedBuffer.getBuffer());
            initRandomBufferKernel.setArg(2, N);
            
            // Execute vector kernel
            queue.enqueueNDRangeKernel(initRandomBufferKernel, cl::NullRange, cl::NDRange(N), cl::NDRange(TileMetrics::workgroupSize));

            // Store current state
            queue.enqueueCopyBuffer(*randomBuffer, randomBufferSnapshot, 0, 0, N*sizeof(RandomState));
        }
        
        // Return buffer
        return *randomBuffer;
    }

    /**
    * Store current random state of %Raster
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::saveRandomState() {

        auto &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();
        
        cl_uint N = (cl_uint)dim.d.nz*TileMetrics::tileSizeSquared;
        auto currentRandomBuffer = getRandomBuffer();
        queue.enqueueCopyBuffer(currentRandomBuffer, randomBufferSnapshot, 0, 0, N*sizeof(RandomState));
    }

    /**
    * Restore random state of %Raster
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::restoreRandomState() {

        auto &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        
        cl_uint N = (cl_uint)dim.d.nz*TileMetrics::tileSizeSquared;
        auto currentRandomBuffer = getRandomBuffer();
        queue.enqueueCopyBuffer(randomBufferSnapshot, currentRandomBuffer, 0, 0, N*sizeof(RandomState));
    }
    
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::tileDataOnDevice(uint32_t ti , uint32_t tj) {

        std::stringstream err;
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            err << "Requested tile " << ti << "," << tj << " out of range in raster";
            throw std::range_error(err.str());
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            err << "Requested tile " << ti+tj*dim.tx << " in raster is empty";
            throw std::range_error(err.str());
        }
            
        // Get tile
        return tp->dataOnDevice();
    }
            
    /**
    * %Raster initialisation.
    * Initialises %Raster, creating tiles and internal raster variables.
    * @param nx_ the number of cells in the x dimension.
    * @param ny_ the number of cells in the y dimension.
    * @param nz_ the number of cells in the z dimension.
    * @param hx_ the spacing in the x direction.
    * @param hy_ the spacing in the y direction.
    * @param hz_ the spacing in the z direction.
    * @param ox_ the origin in the x direction.
    * @param oy_ the origin in the y direction.
    * @param oz_ the origin in the z direction.
    * @return \b true if successfully initialised, \b false otherwise.
    */     
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(
        uint32_t nx_, 
        uint32_t ny_, 
        uint32_t nz_, 
        CTYPE hx_, 
        CTYPE hy_, 
        CTYPE hz_, 
        CTYPE ox_, 
        CTYPE oy_,  
        CTYPE oz_) {

        // Clear any existing data
        tiles.clear();
        tree.clear();
              
        // Check bounds
        if (hx_ < 0.0) return false;
        if (hy_ < 0.0) return false;
        if (hz_ < 0.0) return false;
            
        if (nx_ == 0) nx_ = 1;
        if (ny_ == 0) ny_ = 1;
        if (nz_ == 0) nz_ = 1;
                            
        // Set variables
        dim.d.nx = nx_; dim.d.ny = ny_; dim.d.nz = nz_;
        dim.d.hx = hx_; dim.d.hy = hy_; dim.d.hz = hz_;
        dim.d.ox = ox_; dim.d.oy = oy_; dim.d.oz = oz_;
        dim.d.mx = nx_; dim.d.my = ny_; 

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Calculate number of tiles needed
        dim.tx = (uint32_t)ceil((CTYPE)nx_/(CTYPE)TileMetrics::tileSize);
        dim.ty = (uint32_t)ceil((CTYPE)ny_/(CTYPE)TileMetrics::tileSize);

        // Initialise tiles
        for (uint32_t tj = 0; tj < dim.ty; tj++)
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
            
                // Create empty tile
                TilePtr<RTYPE, CTYPE> tp = std::make_shared<Tile<RTYPE, CTYPE> >();
                if (!tp->init(ti, tj, dim))
                    return false;

                // Add to tiles
                tiles.push_back(tp);

                // Add to tree
                tree.insert(tp);
            }

        return true;
    }

    /**
    * Two-dimensional raster initialisation.
    * Initialises raster, creating tiles and internal raster variables.
    * @param nx_ the number of cells in the x dimension.
    * @param ny_ the number of cells in the y dimension.
    * @param hx_ the spacing in the x direction.
    * @param hy_ the spacing in the y direction.
    * @param ox_ the origin in the x direction.
    * @param oy_ the origin in the y direction.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init2D(
        uint32_t nx_, 
        uint32_t ny_, 
        CTYPE hx_, 
        CTYPE hy_, 
        CTYPE ox_, 
        CTYPE oy_, 
        CTYPE oz_) {
            return init(nx_, ny_, 1, hx_, hy_, 1.0, ox_, oy_, oz_);
    }

    /**
    * Raster initialisation from dimensions.
    * Initialises %Raster, creating tiles and internal raster variables.
    * @param dim %Raster dimensions.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(const Dimensions<CTYPE> &dim) {
        return init(dim.nx, dim.ny, dim.nz, dim.hx, dim.hy, dim.hz, dim.ox, dim.oy, dim.oz);
    }

    /**
    * Raster initialisation from bounding box.
    * Initialises raster, creating tiles and internal raster variables.
    * @param bounds %Raster bounds.
    * @param hx_ %Raster x-spacing.
    * @param hy_ %Raster y-spacing.
    * @param hz_ %Raster z-spacing.
    * @return \b true if successfully initialised, \b false otherwise.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::init(const BoundingBox<CTYPE> bounds, CTYPE hx_, CTYPE hy_, CTYPE hz_) {

        // Check resolution
        if (hx_ > 0.0) {

            // Set any null spacings from x-spacing
            if (hy_ <= 0.0) {
                hy_ = hx_;
            }
            if (hz_ <= 0.0) {
                hz_ = hx_;
            }

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/hx_);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/hy_);
            uint32_t nz = (uint32_t)ceil((bounds.max.r-bounds.min.r)/hz_);

            // Initialise raster
            return init(nx, ny, nz, hx_, hy_, hz_, bounds.min.p, bounds.min.q, bounds.min.r);

        } else {

            // Exception for invalid raster resolutions
            throw std::runtime_error("Raster resolution must be positive");
        }

        return false;
    }
    
    /**
    * Two-dimensional raster resize.
    * Resizes raster to new number of tile dimensions.
    * @param tnx_ the new number of tiles in the x dimension.
    * @param tny_ the new number of tiles in the y dimension.
    * @param tox_ the tile x origin.
    * @param toy_ the tile y origin.
    * @return set of new tiles as pairs of tile indexes.
    */  
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::resize2D(uint32_t tnx_, uint32_t tny_, uint32_t tox_, uint32_t toy_) {
                  
        // Check bounds
        if (tnx_ == 0 || tny_ == 0) return false;
        if (tox_ > tnx_-1 || toy_ > tny_-1) return false;

        // Store current tiles
        auto oldTiles = tiles;
        uint32_t otx = dim.tx;
        uint32_t oty = dim.ty;

        // Clear any existing data
        tiles.clear();
        tree.clear();
                            
        // Set variables
        dim.d.nx = tnx_*TileMetrics::tileSize; 
        dim.d.ny = tny_*TileMetrics::tileSize;

        dim.d.mx = dim.d.nx; 
        dim.d.my = dim.d.ny; 

        dim.d.ox -= (CTYPE)(tox_*TileMetrics::tileSize)*dim.d.hx;
        dim.d.oy -= (CTYPE)(toy_*TileMetrics::tileSize)*dim.d.hy;

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Set new number of tiles
        dim.tx = tnx_;
        dim.ty = tny_;

        // Initialise tiles
        TilePtr<RTYPE, CTYPE> tp;
        for (uint32_t tj = 0; tj < tny_; tj++) {
            for (uint32_t ti = 0; ti < tnx_; ti++) {
            
                // Check for old tiles
                if (ti >= tox_ && tj >= toy_ && ti < tox_+otx  && tj < toy_+oty) {

                    // Use existing tile
                    tp = oldTiles[(ti-tox_)+(tj-toy_)*otx];
                    tp->setIndex(ti, tj);

                } else {

                    // Create new empty tile
                    tp = std::make_shared<Tile<RTYPE, CTYPE> >();
                }

                // Update tile dimensions
                if (!tp->init(ti, tj, dim))
                    return false;

                // Add to tiles
                tiles.push_back(tp);

                // Add to tree
                tree.insert(tp);
            }
        }

        return true;
    }

    /**
    * Indexes for two-dimensional raster resize.
    * @param tnx_ the new number of tiles in the x dimension.
    * @param tny_ the new number of tiles in the y dimension.
    * @param tox_ the tile x origin.
    * @param toy_ the tile y origin.
    * @return set of pairs of new tile indexes.
    */  
    template <typename RTYPE, typename CTYPE>
    tileIndexSet Raster<RTYPE, CTYPE>::resize2DIndexes(uint32_t tnx_, uint32_t tny_, uint32_t tox_, uint32_t toy_) {
    
        // Create return set
        tileIndexSet newTiles;
              
        // Check bounds
        if (tnx_ == 0 || tny_ == 0) return newTiles;
        if (tox_ > tnx_-1 || toy_ > tny_-1) return newTiles;

        // Store current tiles
        uint32_t otx = dim.tx;
        uint32_t oty = dim.ty;

        // Initialise tiles
        TilePtr<RTYPE, CTYPE> tp;
        for (uint32_t tj = 0; tj < tny_; tj++) {
            for (uint32_t ti = 0; ti < tnx_; ti++) {
            
                // Check for old tiles
                if (ti < tox_ || tj < toy_ || ti >= tox_+otx  || tj >= toy_+oty) {
                    newTiles.insert( { ti, tj } );
                }
            }
        }

        return newTiles;
    }

    /**
    * Maximum operator.
    * @return maximum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::max() {
    
        // Find maximum value in Raster
        RTYPE max = std::numeric_limits<RTYPE>::lowest();
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
            
                // Get tile
                auto &t = getTile(ti, tj);
                
                // Find maximum
                max = Geostack::max<RTYPE>(t.max(), max);
            }
        }

        // Return null if limits are not found    
        return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
    }

    /**
    * Minimum operator.
    * @return minimum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::min() {
    
        // Find minimum value in Raster
        RTYPE min = std::numeric_limits<RTYPE>::max();
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {

                // Get tile
                auto &t = getTile(ti, tj);

                // Find minimum
                min = Geostack::min<RTYPE>(t.min(), min);
            }
        }

        // Return null if limits are not found
        return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;
    }

    /**
    * Raster reduction operator.
    * @return reduction value for %Raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::reduce() const {
    
        // Reduce values
        switch (reductionType) {

            case(Reduction::Maximum): {
                RTYPE max = std::numeric_limits<RTYPE>::lowest();
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        max = Geostack::max<RTYPE>(tp->reduce(reductionType), max);
                    }
                }

                // Return null if limits are not found    
                return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
            }
            
            case(Reduction::Minimum): {
                RTYPE min = std::numeric_limits<RTYPE>::max();
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        min = Geostack::min<RTYPE>(tp->reduce(reductionType), min);
                    }
                }

                // Return null if limits are not found
                return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;
            }
            
            case(Reduction::Sum):
            case(Reduction::SumSquares): {
                RTYPE sum = getNullValue<RTYPE>();
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        RTYPE v = tp->reduce(reductionType);
                        if (isValid<RTYPE>(v)) {
                            sum = isValid<RTYPE>(sum) ? sum + v : v;
                        }
                    }
                }
                return sum;
            }
            
            case(Reduction::Count): {
                RTYPE count = 0.0;
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        count += tp->reduce(reductionType);
                    }
                }
                return count;
            }
            
            case(Reduction::Mean): {
                RTYPE sum = 0.0;
                RTYPE count = 0.0;
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        sum += tp->reduce(Reduction::Sum);
                        count += tp->reduce(Reduction::Count);
                    }
                }
                return count == 0.0 ? getNullValue<RTYPE>() : sum/count;
            }
            
            default:
                break;
        }

        // Return null value
        return getNullValue<RTYPE>();
    }

    /**
    * Layer reduction operator.
    * @return vector of per-layer reduction value in %Raster.
    */  
    template <typename RTYPE, typename CTYPE>
    std::vector<RTYPE> Raster<RTYPE, CTYPE>::reduceByLayer() const {
    
        // Reduce values
        switch (reductionType) {

            case(Reduction::Maximum): {
                std::vector<RTYPE> max(dim.d.nz, std::numeric_limits<RTYPE>::lowest());
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        for (std::size_t k = 0; k < dim.d.nz; k++) {
                            auto tv = tp->reduceByLayer(reductionType);
                            max[k] = Geostack::max<RTYPE>(tv[k], max[k]);
                        }
                    }
                }
            
                // Return null if limits are not found
                for (std::size_t k = 0; k < dim.d.nz; k++) {
                    if (max[k] == std::numeric_limits<RTYPE>::lowest()) {
                        max[k] = getNullValue<RTYPE>();
                    }
                }
                return max;
            }
            
            case(Reduction::Minimum): {
                std::vector<RTYPE> min(dim.d.nz, std::numeric_limits<RTYPE>::max());
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        for (std::size_t k = 0; k < dim.d.nz; k++) {
                            auto tv = tp->reduceByLayer(reductionType);
                            min[k] = Geostack::min<RTYPE>(tv[k], min[k]);
                        }
                    }
                }                
            
                // Return null if limits are not found
                for (std::size_t k = 0; k < dim.d.nz; k++) {
                    if (min[k] == std::numeric_limits<RTYPE>::max()) {
                        min[k] = getNullValue<RTYPE>();
                    }
                }
                return min;
            }
            
            case(Reduction::Sum):
            case(Reduction::SumSquares): {
                std::vector<RTYPE> sum(dim.d.nz, getNullValue<RTYPE>());
                for (auto &tp : tiles) {
                    if (tp != nullptr && tp->hasData()) {
                        for (std::size_t k = 0; k < dim.d.nz; k++) {
                            auto tv = tp->reduceByLayer(reductionType);
                            sum[k] = isValid<RTYPE>(sum[k]) ? sum[k]+tv[k] : tv[k];
                        }
                    }
                }
                return sum;
            }
            
            case(Reduction::Count): {

                // Count is scalar and cannot be used for multiple layers
                throw std::runtime_error("Count reduction not available for multiple layers");
            }
            
            case(Reduction::Mean): {

                // Mean requires count, which is scalar and cannot be used for multiple layers
                throw std::runtime_error("Mean reduction not available for multiple layers");
            }
            
            default:
                break;
        }
        
        // Return null values
        return std::vector<RTYPE>(dim.d.nz, getNullValue<RTYPE>());
    }

    /**
    * Gets %Tile data buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param dataBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getTileDataBuffer(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);
        
        // Return buffer
        return t.getDataBuffer();
    }
    
    /**
    * Gets %Tile reduction buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param reductionBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getTileReduceBuffer(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);
        
        // Return buffer
        return t.getReduceBuffer();
    }
    
    /**
    * Gets %Tile reduction value from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return reduction value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getTileReduction(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);
        
        // Return buffer
        return t.reduce(reductionType);
    }

    /**
    * Resets reduction buffer of %Tile in %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::resetTileReduction(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Reset reduction buffer
        t.resetReduction();
    }    

    /**
    * Gets %Tile status buffer from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param statusBuffer a reference to the data buffer to return.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Raster<RTYPE, CTYPE>::getTileStatusBuffer(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return buffer
        return t.getStatusBuffer();
    }
    
    /**
    * Gets %Dimensions for a %Tile from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return %TileDimensions of %Tile.
    */
    template <typename RTYPE, typename CTYPE>
    TileDimensions<CTYPE> Raster<RTYPE, CTYPE>::getTileDimensions(uint32_t ti, uint32_t tj) {
    
        std::stringstream err;
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            err << "Requested tile " << ti << "," << tj << " out of range in raster";
            throw std::range_error(err.str());
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            err << "Requested tile " << ti+tj*dim.tx << " in raster is empty";
            throw std::range_error(err.str());
        }
            
        // Get tile
        return tp->getTileDimensions();
    }
    
    /**
    * Gets %BoundingBox for a %Tile from %Raster.
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @return %BoundingBox of %Tile.
    */
    template <typename RTYPE, typename CTYPE>
    BoundingBox<CTYPE> Raster<RTYPE, CTYPE>::getTileBounds(uint32_t ti, uint32_t tj) {
    
        std::stringstream err;
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            err << "Requested tile " << ti << "," << tj << " out of range in raster";
            throw std::range_error(err.str());
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            err << "Requested tile " << ti+tj*dim.tx << " in raster is empty";
            throw std::range_error(err.str());
        }
            
        // Get tile
        return tp->getBounds();
    }

    /**
    * Get copy of tile data from %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param data handle to vector of type RTYPE to copy raster data into.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::getTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &tileData) {
    
        // Check bounds
        if (ti >= dim.tx || tj >= dim.ty) {
            throw std::range_error("Requested tile out of range in raster");
        }

        // Get pointer
        TilePtr<RTYPE, CTYPE> &tp = tiles[ti+tj*dim.tx];
        if (tp == nullptr) {
            throw std::range_error("Requested tile in raster is empty");
        }
            
        // Get tile
        tp->extractData(tileData, *this);
    }

    /**
    * Set tile data in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param data handle to vector of type CTYPE to copy raster data from.
    * @return true if data is successfully set, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data) {

        // Get tile
        auto &t = getTile(ti, tj);

        // Check size
        size_t dataSize = t.getDataSize();
        if (data.size() != dataSize) {
            throw std::runtime_error("Tile data size is different from requested write size");
        }

        // Copy tile data
        t.setData(data.begin(), data.begin()+dataSize, 0);
    }

    /**
    * Get tile status in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param reference to status.
    */
    template <typename RTYPE, typename CTYPE>
    cl_uint Raster<RTYPE, CTYPE>::getTileStatus(uint32_t ti, uint32_t tj) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return status
        return t.getStatus();
    }

    /**
    * Set tile status in %Raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param newStatus new status value.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return status
        t.setStatus(newStatus);
    }

    /**
    * Get tile status in %Raster and reset to new status.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param reference to status.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::getResetTileStatus(uint32_t ti, uint32_t tj, cl_uint &rStatus, cl_uint newStatus) {

        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return status
        rStatus = t.getResetStatus(newStatus);
        return true;
    }

    /**
    * Set tile values in raster.
    * @param ti x tile index.
    * @param tj y tile index.
    * @param val value to set.
    * @return true if data is successfully set, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::setAllTileCellValues(uint32_t ti, uint32_t tj, RTYPE val) {

        // Get tile
        auto &t = getTile(ti, tj);
        
        // Set all values in tile
        t.setAllCellValues(val);

        return true;
    }

    /**
    * Get value from raster cell.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getCellValue(uint32_t i, uint32_t j, uint32_t k) {
        
        // Check bounds
        if (i > dim.d.nx-1 || j > dim.d.ny-1 || k > dim.d.nz-1)
            return getNullValue<RTYPE>();

        // Get tile, tile index is quotient (using bitshift)
        uint32_t ti = i>>TileMetrics::tileSizePower;
        uint32_t tj = j>>TileMetrics::tileSizePower;
        auto &t = getTile(ti, tj);

        // Return value in tile, tile cell index is remainder (using mask)
        return t(i & TileMetrics::tileSizeMask, j & TileMetrics::tileSizeMask, k);
    }

    /**
     * convert index to raster coordinate.
     * @param i x cell index
     * @param j y cell index
     * @param k z cell index
     */
    template <typename RTYPE, typename CTYPE>
    Coordinate<CTYPE> Raster<RTYPE, CTYPE>::ijk2xyz(uint32_t i, uint32_t j, uint32_t k) {
        
        Coordinate<CTYPE> xyz;
        
        // Convert to raster coordinates
        CTYPE x = (i + 0.5) * dim.d.hx + dim.d.ox;
        CTYPE y = (j + 0.5) * dim.d.hy + dim.d.oy;
        CTYPE z = (k + 0.5) * dim.d.hz + dim.d.oz;

        // Check bounds
        if (x < dim.d.ox || y < dim.d.oy || z < dim.d.oz || 
            x >= dim.ex || y >= dim.ey || z >= dim.ez) {
            xyz = {(CTYPE)0.0, (CTYPE)0.0, (CTYPE)0.0};
        } else {
            xyz = {x, y, z};
        }
        return xyz;
    }

    /**
     * convert coordinate to raster index.
     * @param x x coordinate
     * @param y y coordinate
     * @param z z coordinate
     */
    template <typename RTYPE, typename CTYPE>
    std::list<uint32_t> Raster<RTYPE, CTYPE>::xyz2ijk(CTYPE x, CTYPE y, CTYPE z) {
        
        std::list<uint32_t> ijk;
        
        // Convert to raster coordinates
        CTYPE rx = (x-dim.d.ox)/dim.d.hx;
        CTYPE ry = (y-dim.d.oy)/dim.d.hy;
        CTYPE rz = (z-dim.d.oz)/dim.d.hz;

        // Check bounds
        if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
            rx >= (CTYPE)dim.d.nx || ry >= (CTYPE)dim.d.ny || rz >= (CTYPE)dim.d.nz) {
            ijk.push_back(getNullValue<uint32_t>());
            ijk.push_back(getNullValue<uint32_t>());
            ijk.push_back(getNullValue<uint32_t>());
        } else {
            // Convert to cell coordinates
            ijk.push_back((uint32_t)rx);
            ijk.push_back((uint32_t)ry);
            ijk.push_back((uint32_t)rz);

        }
        return ijk;
    }

    /**
    * Get nearest-neighbour value at position in raster coordinates. f
    * @param[in] x x-position in world coordinates.
    * @param[in] y y-position in world coordinates.
    * @param[in] z z-position in world coordinates.
    * @return Nearest-neighbour cell-centred value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getNearestValue(CTYPE x, CTYPE y, CTYPE z) {
    
        // Convert to raster coordinates
        CTYPE rx = (x-dim.d.ox)/dim.d.hx;
        CTYPE ry = (y-dim.d.oy)/dim.d.hy;
        CTYPE rz = (z-dim.d.oz)/dim.d.hz;
        
        // Check bounds
        if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
            rx >= (CTYPE)dim.d.nx || ry >= (CTYPE)dim.d.ny || rz >= (CTYPE)dim.d.nz) {
            return getNullValue<RTYPE>();
        } else {
        
            // Convert to cell coordinates
            uint32_t i = (uint32_t)rx;
            uint32_t j = (uint32_t)ry;
            uint32_t k = (uint32_t)rz;

            return getCellValue(i, j, k);
        }
    }

    /**
    * Get bilinearly interpolated value at position in raster coordinates. 
    * @param[in] x x-position in world coordinates.
    * @param[in] y y-position in world coordinates.
    * @param[in] z z-position in world coordinates.
    * @return Bilinearly interpolated cell-centred value.
    */
    template <typename RTYPE, typename CTYPE>
    RTYPE Raster<RTYPE, CTYPE>::getBilinearValue(CTYPE x, CTYPE y, CTYPE z) {
        
        // Convert to raster coordinates
        double rx = (double)(x-dim.d.ox)/dim.d.hx;
        double ry = (double)(y-dim.d.oy)/dim.d.hy;
        double rz = (double)(z-dim.d.oz)/dim.d.hz;
        
        // Check bounds
        if (rx < 0.0 || ry < 0.0 || rz < 0.0 || 
            rx > (double)dim.d.nx || ry > (double)dim.d.ny || rz > (double)dim.d.nz) {
            return getNullValue<RTYPE>();
        } else {        
        
            // Offset to cell centre for interpolation
            rx-=0.5;
            ry-=0.5;
            rz-=0.5;
            
            // Offset within grid cell
            double a = 0.0;
            double b = 0.0;
            double c = 0.0;
            
            uint32_t i, ip;
            if (rx < 0.0) {
                i = 0;
                ip = 0;
                a = 0.0;
            } else if (rx < (double)(dim.d.nx-1)) {
                i = (uint32_t)rx;
                ip = i+1;
                a = rx-(double)i;
            } else {
                i = dim.d.nx-1;
                ip = dim.d.nx-1;
                a = 0.0;
            }
            
            uint32_t j, jp;
            if (ry < 0.0) {
                j = 0;
                jp = 0;
                b = 0.0;
            } else if (ry < (double)(dim.d.ny-1)) {
                j = (uint32_t)ry;
                jp = j+1;
                b = ry-(double)j;
            } else {
                j = dim.d.ny-1;
                jp = dim.d.ny-1;
                b = 0.0;
            }
            
            uint32_t k, kp;
            if (rz < 0.0) {
                k = 0;
                kp = 0;
                c = 0.0;
            } else if (rz < (double)(dim.d.nz-1)) {
                k = (uint32_t)rz;
                kp = k+1;
                c = rz-(double)k;
            } else {
                k = dim.d.nz-1;
                kp = dim.d.nz-1;
                c = 0.0;
            }

            double u000 = (double)getCellValue(i , j , k );
            double u100 = (double)getCellValue(ip, j , k );
            double u010 = (double)getCellValue(i , jp, k );
            double u110 = (double)getCellValue(ip, jp, k );
            double u001 = (double)getCellValue(i , j , kp);
            double u101 = (double)getCellValue(ip, j , kp);
            double u011 = (double)getCellValue(i , jp, kp);
            double u111 = (double)getCellValue(ip, jp, kp);
            
            // Calculate interpolated value
            double v = 0.0;
            v = (double)(u111*     a *     b *     c );
            v+= (double)(u011*(1.0-a)*     b *     c );
            v+= (double)(u101*     a *(1.0-b)*     c );
            v+= (double)(u110*     a *     b *(1.0-c));
            v+= (double)(u001*(1.0-a)*(1.0-b)*     c );
            v+= (double)(u010*(1.0-a)*     b *(1.0-c));
            v+= (double)(u100*     a *(1.0-b)*(1.0-c));
            v+= (double)(u000*(1.0-a)*(1.0-b)*(1.0-c));

            return (RTYPE)v;
        }
    }

    /**
    * Set value in raster cell.
    * This is an expensive operation.
    * @param val value to set.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setCellValue(RTYPE val, uint32_t i, uint32_t j, uint32_t k) {
    
        // Check bounds
        if (i > dim.d.nx-1 || j > dim.d.ny-1 || k > dim.d.nz-1)
            return;
        
        // Get tile, tile index is quotient (using bitshift)
        uint32_t ti = i>>TileMetrics::tileSizePower;
        uint32_t tj = j>>TileMetrics::tileSizePower;
        
        // Get tile handle
        auto &t = getTile(ti, tj);

        // Return value in tile, tile cell index is remainder (using mask)
        t(i & TileMetrics::tileSizeMask, j & TileMetrics::tileSizeMask, k) = val;
    }

    /**
    * Set value in all raster cells.
    * Sets entire raster to specified value
    * @param val value to set.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::setAllCellValues(RTYPE val) { 
    
        // Set value
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {
        
                // Create new tile if no tile currently exists
                auto &t = getTile(ti, tj);

                // Set all values in tile
                t.setAllCellValues(val);
            }
        }
    }
    
    /**
    * Deletes all %Raster data in tiles
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::deleteRasterData() {
    
        // Set value
        for (uint32_t tj = 0; tj < dim.ty; tj++) {
            for (uint32_t ti = 0; ti < dim.tx; ti++) {

                // Get tile pointer
                TilePtr<RTYPE, CTYPE> tp = tiles[ti+tj*dim.tx];
                if (tp != nullptr) {

                    // Delete tile data
                    tp->deleteData();
                }
            }
        }
    }
    
    /**
    * Map %Vector distances into %Raster
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param widthPropertyName name of feature width property
    * @param levelPropertyName name of feature level property
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::mapVector(Vector<CTYPE> &v, std::string script, size_t geometryTypes, 
            std::string widthPropertyName, std::string levelPropertyName) {
    
        try {

            // Script creation variables
            PropertyMap &properties = v.getProperties();
            std::vector<std::string> fields;
            std::string argsList;
            std::string variableList;

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection) {

                // Check projections are valid
                if (proj.type == 0 || vproj.type == 0) {
                    throw std::runtime_error("One or more projections is undefined for distance mapping");
                }

                // Check projections are the same type
                if ((proj.type == 1 && vproj.type == 2) || (proj.type == 2 && vproj.type == 1)) {
                    throw std::runtime_error("Projections must have the same length units");
                }
            
                // Create argument list
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            }

            // Check level property
            bool usingLevel = false;
            if (v.isPropertyNumeric(levelPropertyName)) {
                fields.push_back(levelPropertyName);
                usingLevel = true;
            }
            
            // Check for empty script
            bool useAll = false;
            if (script.length() == 0) {

                if (v.isPropertyNumeric(widthPropertyName)) {

                    // Set default script with width
                    script = "output = fmin(output, length(d)-" + widthPropertyName + ");";
                    fields.push_back(widthPropertyName);

                } else {

                    // Set default script without width
                    script = "output = fmin(output, length(d));";
                }

            } else {

                // Set flag to use all vector geometry in mapping
                useAll = true;

                // Check for underscores in script, these are reserved for internal variables
                if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                    throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                }

                // Search for fields
                for (auto fieldName: properties.getPropertyNames()) {
                    if (std::regex_search(script, std::regex("\\b" + fieldName + "\\b"))) {

                        // Check property type
                        if (properties.getPropertyType(fieldName) != PropertyType::Undefined) {

                            if (v.isPropertyNumeric(fieldName)) {

                                // Add defined properties as fields
                                fields.push_back(fieldName);

                            } else {

                                // Throw error
                                throw std::runtime_error(std::string("Property '") + fieldName +
                                    std::string("' must be numeric to be used in script"));
                            }

                        } else {

                            // Add dummy variable
                            variableList += "REAL " + fieldName + " = noData_REAL;\n";
                        }
                    }
                }
            }

            // Process fields
            for (std::size_t i = 0; i < fields.size(); i++) {

                std::string &fieldName = fields[i];

                // Get type
                auto type = properties.getOpenCLTypeString(fieldName);

                // Add to argument list
                argsList += std::string(",\n__global ") + type + " *_" + fieldName;

                // Update script for field value
                variableList += type + " " + fieldName + " = *(_" + fieldName + "+_p[index]);\n";
            }

            // Add code to handle level skip
            if (usingLevel) {
                variableList += "if ((uint)(" + levelPropertyName + ") != _k) { continue; }\n";
            }

            // Parse script
            script = Solver::processScript(script);

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {

                // Update projection definitions
                auto projStr = Models::cl_projection_head_c;
                if (proj.type == 1) {
                    std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(proj.cttype) + "\n";
                    projStr = projDef + projStr;
                }
                if (vproj.type == 1) {
                    std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(vproj.cttype) + "\n";
                    projStr = projDef + projStr;
                }
                kernelStr = projStr + Models::cl_map_distance_c;
            
                // Parse script
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));

            } else {

                // Create program string without projection
                kernelStr = Models::cl_map_distance_c;
            }            

            // Process denominator
            if (std::regex_search(script, std::regex("\\bdenom\\b"))) {
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__DENOM__|__DENOM__\\*\\/"), std::string(""));
            }

            // Patch script
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);

            // Build program
            auto &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build program");
            }

            // Loop through tiles
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Get handle to tile
                    auto &t = getTile(ti, tj);

                    // Map vector to tile
                    t.mapVector(v, clHash, proj, geometryTypes, fields, useAll);
                }
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }
    
    /**
    * Map %Vector distances into %Raster %Tile at index ti and tj
    * Raster and Vector projections must be the same
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param script the mapping script to use
    * @param levelPropertyName name of feature level property
    * @param widthPropertyName name of feature width property
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::mapTileVector(uint32_t ti, uint32_t tj, Vector<CTYPE> &v, 
        size_t geometryTypes, std::string widthPropertyName, std::string levelPropertyName) { 
            
            // Processing variables
            PropertyMap &properties = v.getProperties();
            std::string script;
            std::vector<std::string> fields;

            // Check width property
            if (v.isPropertyNumeric(widthPropertyName)) {

                // Set default script with width
                script = "output = fmin(output, length(d)-" + widthPropertyName + ");";
                fields.push_back(widthPropertyName);

            } else {

                // Set default script without width
                script = "output = fmin(output, length(d));";
            }

            // Check level property
            bool usingLevel = false;
            if (v.isPropertyNumeric(levelPropertyName)) {
                fields.push_back(levelPropertyName);
                usingLevel = true;
            }

            auto scriptHash = createScriptHash(script, Models::cl_map_distance_c, *this);
            auto mapTileGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash); // TODO ADD LEVEL
            if (mapTileGenerator.programHash == Solver::getNullHash()) {

                // Parse script
                script = Solver::processScript(script);

                // Script creation variables
                std::string kernelStr;
                std::string argsList;
                std::string variableList;

                // Check projection
                auto vproj = v.getProjectionParameters();
                bool usingProjection = (proj != vproj);
                if (usingProjection) {

                    // Check projections are valid
                    if (proj.type == 0 || vproj.type == 0) {
                        throw std::runtime_error("One or more projections is undefined for distance mapping");
                    }

                    // Check projections are the same type
                    if ((proj.type == 1 && vproj.type == 2) || (proj.type == 2 && vproj.type == 1)) {
                        throw std::runtime_error("Projections must have the same length units");
                    }

                    // Update projection definitions
                    auto projStr = Models::cl_projection_head_c;
                    if (proj.type == 1) {
                        std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(proj.cttype) + "\n";
                        projStr = projDef + projStr;
                    } 
                    if (vproj.type == 1) {
                        std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(vproj.cttype) + "\n";
                        projStr = projDef + projStr;
                    }
                    kernelStr = projStr + Models::cl_map_distance_c;
            
                    // Parse script
                    kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));
                
                    // Create argument list
                    argsList += ", const ProjectionParameters _rproj";
                    argsList += ", const ProjectionParameters _vproj";

                } else {

                    // Create program string without projection
                    kernelStr = Models::cl_map_distance_c;
                }

                // Process fields
                for (std::size_t i = 0; i < fields.size(); i++) {

                    std::string &fieldName = fields[i];

                    // Get type
                    auto type = properties.getOpenCLTypeString(fieldName);

                    // Add to argument list
                    argsList += std::string(",\n__global ") + type + " *_" + fieldName;

                    // Update script for field value
                    variableList += type + " " + fieldName + " = *(_" + fieldName + "+_p[index]);\n";
                }

                // Add code to handle level skip
                if (usingLevel) {
                    variableList += "if ((uint)(" + levelPropertyName + ") != _k) { continue; }\n";
                }
            
                // Patch script
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);

                // Build program
                auto &solver = Geostack::Solver::getSolver();
                std::size_t clHash = solver.buildProgram(kernelStr);
                if (clHash == solver.getNullHash()) {
                    throw std::runtime_error("Cannot build program");
                }

                // Save to generator
                mapTileGenerator.script = kernelStr;
                mapTileGenerator.programHash = clHash;

                // Register kernel in cache
                RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, mapTileGenerator);
            }

        try { 
        
            // Get handle to tile
            auto &t = getTile(ti, tj);

            // Map vector to tile
            t.mapVector(v, mapTileGenerator.programHash, proj, geometryTypes, fields);

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }
    
    /**
    * Rasterise %Vector onto %Raster
    * @param v the %Vector to map.
    * @param parameters the types of geometry to map
    * @param script script to run for rasterisation
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::rasterise(Vector<CTYPE> &v, std::string script, size_t parameters, 
        std::string levelPropertyName) {
    
        try {
        
            // Processing variables
            PropertyMap &properties = v.getProperties();
            std::vector<std::string> fields;
            std::string argsList;
            std::string variableList;

            // Check geometry types
            if (!(parameters & GeometryType::All)) {
                throw std::runtime_error("No requested geometry types to map");
            }

            // Check for underscores in script, these are reserved for internal variables
            if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
            }

            // Check for empty script
            if (script.length() == 0) {

                // Default script
                script = "output = 1.0;";
            }

            // Check script
            if (!std::regex_search(script, std::regex("\\boutput\\b|\\batomic_\\w*\\("))) {
                throw std::runtime_error("Rasterise kernel code must contain 'output' variable or atomic function");
            }

            // Parse script
            script = Solver::processScript(script);

            // Check level property
            bool usingLevel = false;
            if (v.isPropertyNumeric(levelPropertyName)) {
                fields.push_back(levelPropertyName);
                usingLevel = true;
            }

            // Check projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (proj != vproj);
            if (usingProjection && (proj.type == 0 || vproj.type == 0)) {
                throw std::runtime_error("One or more projections is undefined for rasterisation");
            }

            // Build kernel code
            std::string kernelStr;
            if (usingProjection) {

                // Update projection definitions
                auto projStr = Models::cl_projection_head_c;
                if (proj.type == 1) {
                    std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(proj.cttype) + "\n";
                    projStr = projDef + projStr;
                } 
                if (vproj.type == 1) {
                    std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(vproj.cttype) + "\n";
                    projStr = projDef + projStr;
                }
                kernelStr = projStr + Models::cl_rasterise_c;

            } else {
                kernelStr = Models::cl_rasterise_c;
            }

            // Replace type
            kernelStr = std::regex_replace(kernelStr, std::regex("TYPE"), Solver::getOpenCLTypeString<RTYPE>());
            
            // Create argument list
            if (usingProjection) {
                argsList += ", const ProjectionParameters _rproj";
                argsList += ", const ProjectionParameters _vproj";
            }

            // Search for fields
            for (auto fieldName : properties.getPropertyNames()) {
                if (std::regex_search(script, std::regex("\\b" + fieldName + "\\b"))) {
                
                    // Check property type
                    if (properties.getPropertyType(fieldName) != PropertyType::Undefined) {

                        if (v.isPropertyNumeric(fieldName)) {

                            // Add defined properties as fields
                            fields.push_back(fieldName);

                        } else {

                            // Throw error
                            throw std::runtime_error(std::string("Property '") + fieldName +
                                std::string("' must be numeric to be used in script"));
                        }

                    } else {

                        // Add dummy variable
                        variableList += "REAL " + fieldName + " = noData_REAL;\n";
                    }
                }
            }

            // Process fields
            for (std::size_t i = 0; i < fields.size(); i++) {

                std::string &fieldName = fields[i];

                // Get type
                auto type = properties.getOpenCLTypeString(fieldName);

                // Add to argument list
                argsList += std::string(",\n__global ") + type + " *_" + fieldName;

                // Update script for field value
                variableList += type + " " + fieldName + " = *(_" + fieldName + "+_p[index]);\n";
            }

            // Add code to handle level skip
            std::string levelCondition;
            if (usingLevel) {
                levelCondition += "if ((uint)(" + levelPropertyName + ") != _k) { continue; }\n";
            }

            // Search for output pointer
            if (std::regex_search(script, std::regex("\\batomic_\\w*\\("))) {

                // Replace atomic functions
                script = std::regex_replace(script, std::regex("\\batomic_(inc|dec)\\(\\)"), 
                    std::string("atomic_$1(_pval2D_a(_u, _i, _j))"));
                script = std::regex_replace(script, std::regex("\\batomic_(add|sub|xchg|cmpxchg|min|max|and|or)\\((.*)\\)"), 
                    std::string("atomic_$1(_pval2D_a(_u, _i, _j), $2)"));

            } else {

                // Enable write
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__WRITE__|__WRITE__\\*\\/"), std::string(""));
            }

            // Parse script
            script = Solver::processScript(script);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__LEVEL__\\*\\/"), levelCondition);
            kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);
            if (usingProjection) {
                kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));
            }
                        
            // Build program
            auto &solver = Geostack::Solver::getSolver();
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build program");
            }

            // Loop through tiles
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Get handle to tile
                    auto &t = getTile(ti, tj);

                    // Map vector to tile
                    t.rasterise(v, clHash, fields, proj, parameters);
                }
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }    

    /**
    * Index all separated components on the Raster
    * @param rasterBaseRef input raster.
    */
    template <typename CTYPE>
    Raster<cl_uint, CTYPE> RasterBase<CTYPE>::indexComponents(std::string script) {
        
        // Create Raster
        makeRaster(index, cl_uint, CTYPE)
        index.init(dim.d);

        // Build comparison script
        std::string name = RasterBase<CTYPE>::template getProperty<std::string>("name");
        if (script.length() == 0) {
            script = name + " > 0.0";
        }
        
        try {

            // Get OpenCL handles
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            auto verbosity = solver.getVerboseLevel();

            if (solver.openCLInitialised()) {

                // Get generator
                RasterBaseRefs<CTYPE> rb = { *this };
                auto scriptHash = createScriptHash(script, Models::cl_raster_index_components_c, rb);
                auto kernelGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
                if (kernelGenerator.programHash == Solver::getNullHash()) {

                    // Generate kernel
                    kernelGenerator = generateKernel<CTYPE>(script, Models::cl_raster_index_components_c, rb,
                        RasterNullValue::Null, nullptr, true, false);
                
                    // Check script
                    if (kernelGenerator.script.size() == 0) {
                        throw std::runtime_error("Cannot generate script");
                    }

                    // Ensure index is used
                    kernelGenerator.reqs.rasterRequirements[0].used = true;
                
                    // Build kernel
                    if (buildRasterKernel(kernelGenerator) == solver.getNullHash()) {
                        throw std::runtime_error("Cannot get kernel for script");
                    }

                    // Register kernel in cache
                    RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
                }

                // Get kernel
                cl::Kernel &kernel_index = solver.getKernel(kernelGenerator.programHash, "indexComponents");
                cl::Kernel &kernel_find = solver.getKernel(kernelGenerator.programHash, "findComponents");
                cl::Kernel &kernel_reindex = solver.getKernel(kernelGenerator.programHash, "reindex");

                if (verbosity <= Verbosity::Debug) {
                    std::cout << "Executing kernel" << std::endl;
                }

                // Run over all tiles
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {

                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0 && tj == 0) {
                                std::cout << "Index components" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }

                        cl_uint local_tile_size = (cl_uint) sqrt(TileMetrics::tileSize);
                        // Set kernel arguments
                        kernel_index.setArg(0, TileMetrics::tileSize * sizeof(cl_uint), NULL); // TODO hardcoded 256 tile
                        kernel_index.setArg(1, index.getTileDataBuffer(ti, tj));
                        setTileKernelArguments<CTYPE>(ti, tj, kernel_index, rb, kernelGenerator.reqs, 2);
                    
                        // Execute kernel
                        queue.enqueueNDRangeKernel(kernel_index, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, dim.d.nz), 
                            cl::NDRange(local_tile_size, local_tile_size, 1)); // TODO hardcoded 256 tile

                        if (verbosity <= Verbosity::Debug) {
                            std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                        }
                    }
                }
                
                // Create count buffer
                cl_uint count = 0;
                cl::Buffer countBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint));

                // Create neighbour arrays
                std::size_t indexArraySize = TileMetrics::tileSizeSquared>>1;
                cl::Buffer indexArrayBufferA = cl::Buffer(context, CL_MEM_WRITE_ONLY, indexArraySize*sizeof(cl_uint));
                cl::Buffer indexArrayBufferB = cl::Buffer(context, CL_MEM_WRITE_ONLY, indexArraySize*sizeof(cl_uint));
                std::vector<cl_uint> indexArrayA(indexArraySize);
                std::vector<cl_uint> indexArrayB(indexArraySize);
                
                cl_uint gidx = 0;
                cl_uint nroots = 0;
                std::vector<cl_uint> indexUnion;
                std::vector<cl_uint> indexUnionTileIndexes;
                std::map<cl_ulong, cl_uint> indexMap;
                std::vector<std::set<cl_ulong> > indexSets(dim.tx*dim.ty);

                if (verbosity <= Verbosity::Debug) {
                    std::cout << "Executing kernel" << std::endl;
                }

                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {

                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0&&tj == 0) {
                                std::cout << "Find components" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }

                        cl_uint local_tile_size = (cl_uint) sqrt(TileMetrics::tileSize);

                        // Store index count
                        solver.fillBuffer<cl_uint>(countBuffer, 0, 1);
                    
                        // Set kernel arguments
                        kernel_find.setArg(0, index.getTileDataBuffer(ti, tj));
                        kernel_find.setArg(1, ti > 0 ? index.getTileDataBuffer(ti-1, tj) : index.getNullBuffer());
                        kernel_find.setArg(2, tj > 0 ? index.getTileDataBuffer(ti, tj-1) : index.getNullBuffer());
                        kernel_find.setArg(3, countBuffer);
                        kernel_find.setArg(4, indexArrayBufferA);
                        kernel_find.setArg(5, indexArrayBufferB);
                        kernel_find.setArg(6, index.getTileDimensions(ti, tj).d);
                    
                        // Execute kernel
                        queue.enqueueNDRangeKernel(kernel_find, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, dim.d.nz), 
                            cl::NDRange(local_tile_size, local_tile_size, 1)); // TODO hardcoded 256 tile

                        // Read buffers
                        queue.enqueueReadBuffer(countBuffer, CL_TRUE, 0, sizeof(cl_uint), static_cast<void *>(&count));
                        if (count > 0) {
                            queue.enqueueReadBuffer(indexArrayBufferA, CL_TRUE, 0, count*sizeof(cl_uint), static_cast<void *>(indexArrayA.data()));
                            queue.enqueueReadBuffer(indexArrayBufferB, CL_TRUE, 0, count*sizeof(cl_uint), static_cast<void *>(indexArrayB.data()));
                        
                            // Create index set
                            cl_uint tidx = ti+tj*dim.tx;
                            auto &indexSet = indexSets[tidx];
                            for (std::size_t i = 0; i < count; i++) {
                                if (indexArrayA[i] == indexArrayB[i]) {

                                    // Store root entry
                                    indexUnion.push_back(gidx);
                                    indexMap[(cl_ulong)tidx<<32 | indexArrayB[i]] = gidx;
                                    gidx++;

                                } else {

                                    // Store unique relation
                                    indexSet.insert((cl_ulong)indexArrayA[i]<<32 | indexArrayB[i]);
                                }
                            }
                        }

                        // Store count of indexes
                        indexUnionTileIndexes.push_back(indexUnion.size());
                        
                        if (verbosity <= Verbosity::Debug) {
                            std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                        }
                    }
                }
                
                // Create global index map
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0&&tj == 0) {
                                std::cout << "Global index map" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }
                        cl_uint tidx = ti+tj*dim.tx;
                        auto indexSet = indexSets[tidx];
                        for (auto idxPair : indexSet) {

                            // Get indexes
                            cl_uint idx_A = (cl_uint)(idxPair>>32);
                            cl_uint idx_B = (cl_uint)(idxPair&0xFFFFFFFF);
                            
                            // Mask to 30 bits
                            cl_ulong tidx_A = idx_A&0x3FFFFFFF;
                            cl_ulong tidx_B = idx_B&0x3FFFFFFF;

                            // Check for neighbouring tile bits
                            if (idx_A & 0xC0000000) {
                                
                                // Check for south bit
                                if (idx_A & 0x40000000) {
                                    tidx_A |= (cl_ulong)(tidx-dim.tx)<<32;
                                }

                                // Check for west bit
                                if (idx_A & 0x80000000) {
                                    tidx_A |= (cl_ulong)(tidx-1)<<32;
                                }
                            } else {
                                tidx_A |= (cl_ulong)tidx<<32;
                            }

                            if (idx_B & 0xC0000000) {
                                
                                // Check for south bit
                                if (idx_B & 0x40000000) {
                                    tidx_B |= (cl_ulong)(tidx-dim.tx)<<32;
                                }

                                // Check for west bit
                                if (idx_B & 0x80000000) {
                                    tidx_B |= (cl_ulong)(tidx-1)<<32;
                                }
                            } else {
                                tidx_B |= (cl_ulong)tidx<<32;
                            }

                            // Look up indexes
                            idx_A = indexMap[tidx_A];
                            idx_B = indexMap[tidx_B];

                            // Find
                            while (idx_A != indexUnion[idx_A]) {
                                indexUnion[idx_A] = indexUnion[indexUnion[idx_A]];
                                idx_A = indexUnion[idx_A];
                            }
                            while (idx_B != indexUnion[idx_B]) {
                                indexUnion[idx_B] = indexUnion[indexUnion[idx_B]];
                                idx_B = indexUnion[idx_B];
                            }

                            // Union
                            if (idx_A < idx_B) {
                                indexUnion[idx_B] = idx_A;
                            } else if (idx_A > idx_B) {
                                indexUnion[idx_A] = idx_B;
                            }
                        }
                        if (verbosity <= Verbosity::Debug) {
                            std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                        }
                    }
                }

                // Compress indexes
                uint32_t componentCount = 0;
                std::map<cl_uint, cl_uint> compressMap;
                for (auto idx : indexUnion) {

                    // Check for root
                    if (idx == indexUnion[idx] && compressMap.find(idx) == compressMap.end()) {
                        compressMap[idx] = componentCount++;
                    }
                }
                index.template setProperty<uint32_t>("componentCount", componentCount);
                 
                // Re-index
                auto it = indexMap.begin();
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0&&tj == 0) {
                                std::cout << "Re-index" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }
                        // Map to flat arrays
                        cl_uint tidx = ti+tj*dim.tx;
                        indexArrayA.clear();
                        indexArrayB.clear();
                        auto ite = indexMap.upper_bound((cl_ulong)tidx<<32 | 0xFFFFFFFF);
                        while (it != indexMap.end() && it != ite) {

                            // Find
                            cl_uint idx = it->second;
                            while (idx != indexUnion[idx]) {
                                indexUnion[idx] = indexUnion[indexUnion[idx]];
                                idx = indexUnion[idx];
                            }

                            // Add to array
                            indexArrayA.push_back((cl_uint)(it->first&0xFFFFFFFF));
                            indexArrayB.push_back(compressMap[idx]);
                            it++;
                        }

                        // Run indexing kernel
                        if (indexArrayA.size() > 0) {
                            queue.enqueueWriteBuffer(indexArrayBufferA, CL_TRUE, 0, indexArrayA.size()*sizeof(cl_uint), indexArrayA.data());
                            queue.enqueueWriteBuffer(indexArrayBufferB, CL_TRUE, 0, indexArrayB.size()*sizeof(cl_uint), indexArrayB.data());
                    
                            // Set kernel arguments
                            kernel_reindex.setArg(0, index.getTileDataBuffer(ti, tj));
                            kernel_reindex.setArg(1, indexArrayBufferA);
                            kernel_reindex.setArg(2, indexArrayBufferB);
                            kernel_reindex.setArg(3, (cl_uint)indexArrayA.size());
                            kernel_reindex.setArg(4, index.getTileDimensions(ti, tj).d);
                            
                            // Execute kernel
                            queue.enqueueNDRangeKernel(kernel_reindex, 
                                cl::NullRange, 
                                cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, dim.d.nz), 
                                cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                            
                            if (verbosity <= Verbosity::Debug) {
                                std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                            }
                        }
                    }
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

        // Return raster
        return index;
    }

    /**
    * Index all separated components on the Raster and output a Vector
    * @param indexScript script to define indexing comparison.
    * @param reduceScript script to define reduction operation.
    * @param reduction reduction type.
    * @param overwrite flag to overwrite %Raster with index.
    */
    template <typename CTYPE>
    Vector<CTYPE> RasterBase<CTYPE>::indexComponentsVector(std::string indexScript, std::string reduceScript, 
        Reduction::Type reduction, bool overwrite) {
        
        // Check reduction
        if ((reduction != Reduction::None) && 
            (reduction != Reduction::Count) && 
            (reduction != Reduction::Mean) && 
            (reduction != Reduction::Sum)) {
            throw std::runtime_error("Only reductions of type Count, Mean and Sum are supported");
        }
        
        // Create index Raster and Vector
        Vector<CTYPE> v;
        v.setProjectionParameters(proj);
        makeRaster(index, cl_uint, CTYPE)
        index.init(dim.d);

        // Build comparison script
        std::string name = RasterBase<CTYPE>::template getProperty<std::string>("name");
        if (name == "weight") {
            throw std::runtime_error("Indexing cannot use raster of reserved name 'weight'");
        }
        if (indexScript.length() == 0) {
            indexScript = name + " > 0.0";
        }
        
        try {

            // Get OpenCL handles
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            auto verbosity = solver.getVerboseLevel();

            if (solver.openCLInitialised()) {

                // Get index generator
                RasterBaseRefs<CTYPE> rb = { *this };
                auto scriptIndexHash = createScriptHash(indexScript, Models::cl_raster_index_components_c, rb);
                auto indexGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptIndexHash);
                if (indexGenerator.programHash == Solver::getNullHash()) {

                    // Generate kernel
                    indexGenerator = generateKernel<CTYPE>(indexScript, Models::cl_raster_index_components_c, rb,
                        RasterNullValue::Null, nullptr, true, false);
                
                    // Check script
                    if (indexGenerator.script.size() == 0) {
                        throw std::runtime_error("Cannot generate script");
                    }

                    // Ensure index is used
                    indexGenerator.reqs.rasterRequirements[0].used = true;
                
                    // Build kernel
                    if (buildRasterKernel(indexGenerator) == solver.getNullHash()) {
                        throw std::runtime_error("Cannot get kernel for script");
                    }

                    // Register kernel in cache
                    RasterKernelCache::getKernelCache().setKernelGenerator(scriptIndexHash, indexGenerator);
                }
                
                // Get reduction generator
                auto scriptReduceHash = createScriptHash(reduceScript, Models::cl_raster_index_reduce_c, rb);
                auto reduceGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptReduceHash);
                if (reduceGenerator.programHash == Solver::getNullHash()) {

                    // Generate kernel
                    reduceGenerator = generateKernel<CTYPE>(reduceScript, Models::cl_raster_index_reduce_c, rb);
                
                    // Check script
                    if (reduceGenerator.script.size() == 0) {
                        throw std::runtime_error("Cannot generate script");
                    }

                    // Ensure index is used
                    reduceGenerator.reqs.rasterRequirements[0].used = true;
                
                    // Build kernel
                    if (buildRasterKernel(reduceGenerator) == solver.getNullHash()) {
                        throw std::runtime_error("Cannot get kernel for script");
                    }

                    // Register kernel in cache
                    RasterKernelCache::getKernelCache().setKernelGenerator(scriptReduceHash, reduceGenerator);
                }

                // Get kernel
                cl::Kernel &kernel_index = solver.getKernel(indexGenerator.programHash, "indexComponents");
                cl::Kernel &kernel_find = solver.getKernel(indexGenerator.programHash, "findComponents");
                cl::Kernel &kernel_reindex = solver.getKernel(indexGenerator.programHash, "reindex");
                cl::Kernel &kernel_overwrite = solver.getKernel(indexGenerator.programHash, "overwrite");
                cl::Kernel &kernel_reduce = solver.getKernel(reduceGenerator.programHash, "reduce");

                if (verbosity <= Verbosity::Debug) {
                    std::cout << "Executing kernel" << std::endl;
                }

                // Run over all tiles
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {

                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0&&tj == 0) {
                                std::cout << "Index components" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }

                        cl_uint local_tile_size = (cl_uint) sqrt(TileMetrics::tileSize);
                        // Set kernel arguments
                        kernel_index.setArg(0, TileMetrics::tileSize * sizeof(cl_uint), NULL); // TODO hardcoded 256 tile
                        kernel_index.setArg(1, index.getTileDataBuffer(ti, tj));
                        setTileKernelArguments<CTYPE>(ti, tj, kernel_index, rb, indexGenerator.reqs, 2);
                    
                        // Execute kernel
                        queue.enqueueNDRangeKernel(kernel_index, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, dim.d.nz), 
                            cl::NDRange(local_tile_size, local_tile_size, 1)); // TODO hardcoded 256 tile

                        if (verbosity <= Verbosity::Debug) {
                            std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                        }
                    }
                }              
                
                // Create count buffer
                cl_uint count = 0;
                cl::Buffer countBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint));

                // Create neighbour arrays
                std::size_t indexArraySize = TileMetrics::tileSizeSquared>>1;
                cl::Buffer indexArrayBufferA = cl::Buffer(context, CL_MEM_WRITE_ONLY, indexArraySize*sizeof(cl_uint));
                cl::Buffer indexArrayBufferB = cl::Buffer(context, CL_MEM_WRITE_ONLY, indexArraySize*sizeof(cl_uint));
                std::vector<cl_uint> indexArrayA(indexArraySize);
                std::vector<cl_uint> indexArrayB(indexArraySize);
                
                cl_uint gidx = 0;
                cl_uint nroots = 0;
                std::vector<cl_uint> indexUnion;
                std::vector<cl_uint> indexUnionTileIndexes;
                std::map<cl_ulong, cl_uint> indexMap;
                std::vector<std::set<cl_ulong> > indexSets(dim.tx*dim.ty);

                if (verbosity <= Verbosity::Debug) {
                    std::cout << "Executing kernel" << std::endl;
                }

                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {

                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0&&tj == 0) {
                                std::cout << "Find components" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }

                        cl_uint local_tile_size = (cl_uint) sqrt(TileMetrics::tileSize);

                        // Store index count
                        solver.fillBuffer<cl_uint>(countBuffer, 0, 1);
                    
                        // Set kernel arguments
                        kernel_find.setArg(0, index.getTileDataBuffer(ti, tj));
                        kernel_find.setArg(1, ti > 0 ? index.getTileDataBuffer(ti-1, tj) : index.getNullBuffer());
                        kernel_find.setArg(2, tj > 0 ? index.getTileDataBuffer(ti, tj-1) : index.getNullBuffer());
                        kernel_find.setArg(3, countBuffer);
                        kernel_find.setArg(4, indexArrayBufferA);
                        kernel_find.setArg(5, indexArrayBufferB);
                        kernel_find.setArg(6, index.getTileDimensions(ti, tj).d);
                    
                        // Execute kernel
                        queue.enqueueNDRangeKernel(kernel_find, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, dim.d.nz), 
                            cl::NDRange(local_tile_size, local_tile_size, 1)); // TODO hardcoded 256 tile

                        // Read buffers
                        queue.enqueueReadBuffer(countBuffer, CL_TRUE, 0, sizeof(cl_uint), static_cast<void *>(&count));
                        if (count > 0) {
                            cl::Event readIndexA, readIndexB;
                            queue.enqueueReadBuffer(indexArrayBufferA, CL_TRUE, 0, count*sizeof(cl_uint), static_cast<void *>(indexArrayA.data()));
                            queue.enqueueReadBuffer(indexArrayBufferB, CL_TRUE, 0, count*sizeof(cl_uint), static_cast<void *>(indexArrayB.data()));
                        
                            // Create index set
                            cl_uint tidx = ti+tj*dim.tx;
                            auto &indexSet = indexSets[tidx];
                            for (std::size_t i = 0; i < count; i++) {
                                if (indexArrayA[i] == indexArrayB[i]) {

                                    // Store root entry
                                    indexUnion.push_back(gidx);
                                    indexMap[(cl_ulong)tidx<<32 | indexArrayB[i]] = gidx;
                                    gidx++;

                                } else {

                                    // Store unique relation
                                    indexSet.insert((cl_ulong)indexArrayA[i]<<32 | indexArrayB[i]);
                                }
                            }
                        }

                        // Store count of indexes
                        indexUnionTileIndexes.push_back(indexUnion.size());

                        if (verbosity <= Verbosity::Debug) {
                            std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                        }
                    }
                }
                
                // Create global index map
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0&&tj == 0) {
                                std::cout << "Global index map" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }
                        cl_uint tidx = ti+tj*dim.tx;
                        auto indexSet = indexSets[tidx];
                        for (auto idxPair : indexSet) {

                            // Get indexes
                            cl_uint idx_A = (cl_uint)(idxPair>>32);
                            cl_uint idx_B = (cl_uint)(idxPair&0xFFFFFFFF);
                            
                            // Mask to 30 bits
                            cl_ulong tidx_A = idx_A&0x3FFFFFFF;
                            cl_ulong tidx_B = idx_B&0x3FFFFFFF;

                            // Check for neighbouring tile bits
                            if (idx_A & 0xC0000000) {
                                
                                // Check for south bit
                                if (idx_A & 0x40000000) {
                                    tidx_A |= (cl_ulong)(tidx-dim.tx)<<32;
                                }

                                // Check for west bit
                                if (idx_A & 0x80000000) {
                                    tidx_A |= (cl_ulong)(tidx-1)<<32;
                                }
                            } else {
                                tidx_A |= (cl_ulong)tidx<<32;
                            }

                            if (idx_B & 0xC0000000) {
                                
                                // Check for south bit
                                if (idx_B & 0x40000000) {
                                    tidx_B |= (cl_ulong)(tidx-dim.tx)<<32;
                                }

                                // Check for west bit
                                if (idx_B & 0x80000000) {
                                    tidx_B |= (cl_ulong)(tidx-1)<<32;
                                }
                            } else {
                                tidx_B |= (cl_ulong)tidx<<32;
                            }

                            // Look up indexes
                            idx_A = indexMap[tidx_A];
                            idx_B = indexMap[tidx_B];

                            // Find
                            while (idx_A != indexUnion[idx_A]) {
                                indexUnion[idx_A] = indexUnion[indexUnion[idx_A]];
                                idx_A = indexUnion[idx_A];
                            }
                            while (idx_B != indexUnion[idx_B]) {
                                indexUnion[idx_B] = indexUnion[indexUnion[idx_B]];
                                idx_B = indexUnion[idx_B];
                            }

                            // Union
                            if (idx_A < idx_B) {
                                indexUnion[idx_B] = idx_A;
                            } else if (idx_A > idx_B) {
                                indexUnion[idx_A] = idx_B;
                            }
                        }
                        
                        if (verbosity <= Verbosity::Debug) {
                            std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                        }
                    }
                }

                // Compress indexes
                uint32_t componentCount = 0;
                std::map<cl_uint, cl_uint> compressMap;
                for (auto idx : indexUnion) {

                    // Check for root
                    if (idx == indexUnion[idx] && compressMap.find(idx) == compressMap.end()) {
                        compressMap[idx] = componentCount++;
                    }
                }
                index.template setProperty<uint32_t>("componentCount", componentCount);
                
                if (verbosity <= Verbosity::Debug) {
                    std::cout << "Number of components: " << componentCount << std::endl;
                }
                
                // Create reduction buffers
                cl_uint reduceSize = std::max(componentCount, (cl_uint)1)*4*TileMetrics::tileSize;
                cl::Buffer reduceBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, reduceSize*sizeof(CTYPE));
                solver.fillBuffer<CTYPE>(reduceBuffer, 0.0, reduceSize);

                // Re-index
                auto it = indexMap.begin();
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                        if (verbosity <= Verbosity::Debug) {
                            if (ti == 0&&tj == 0) {
                                std::cout << "Re-index" << std::endl;
                            }
                            std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                        }
                        // Map to flat arrays
                        cl_uint tidx = ti+tj*dim.tx;
                        indexArrayA.clear();
                        indexArrayB.clear();
                        auto ite = indexMap.upper_bound((cl_ulong)tidx<<32 | 0xFFFFFFFF);
                        while (it != indexMap.end() && it != ite) {

                            // Find
                            cl_uint idx = it->second;
                            while (idx != indexUnion[idx]) {
                                indexUnion[idx] = indexUnion[indexUnion[idx]];
                                idx = indexUnion[idx];
                            }

                            // Add to array
                            indexArrayA.push_back((cl_uint)(it->first&0xFFFFFFFF));
                            indexArrayB.push_back(compressMap[idx]);
                            it++;
                        }

                        // Run kernels
                        if (indexArrayA.size() > 0) {
                            queue.enqueueWriteBuffer(indexArrayBufferA, CL_TRUE, 0, indexArrayA.size()*sizeof(cl_uint), indexArrayA.data());
                            queue.enqueueWriteBuffer(indexArrayBufferB, CL_TRUE, 0, indexArrayB.size()*sizeof(cl_uint), indexArrayB.data());
                    
                            // Set kernel arguments
                            kernel_reindex.setArg(0, index.getTileDataBuffer(ti, tj));
                            kernel_reindex.setArg(1, indexArrayBufferA);
                            kernel_reindex.setArg(2, indexArrayBufferB);
                            kernel_reindex.setArg(3, (cl_uint)indexArrayA.size());
                            kernel_reindex.setArg(4, index.getTileDimensions(ti, tj).d);
                            
                            // Execute reindexing kernel
                            queue.enqueueNDRangeKernel(kernel_reindex, 
                                cl::NullRange, 
                                cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, dim.d.nz), 
                                cl::NDRange(TileMetrics::workgroupSize, 1, 1));

                            // Set kernel arguments
                            kernel_reduce.setArg(0, reduceBuffer);
                            kernel_reduce.setArg(1, index.getTileDataBuffer(ti, tj));
                            setTileKernelArguments<CTYPE>(ti, tj, kernel_reduce, rb, reduceGenerator.reqs, 2);
                    
                            // Execute reduction kernel
                            queue.enqueueNDRangeKernel(kernel_reduce, 
                                cl::NullRange, 
                                cl::NDRange(TileMetrics::tileSize), 
                                cl::NDRange(TileMetrics::workgroupSize));

                            if (verbosity <= Verbosity::Debug) {
                                std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                            }
                        }
                    }
                }

                // Read reduction buffers
                std::vector<CTYPE> reduce(reduceSize);
                queue.enqueueReadBuffer(reduceBuffer, CL_TRUE, 0, reduceSize*sizeof(CTYPE), static_cast<void *>(reduce.data()));

                for (std::size_t c = 0; c < componentCount*TileMetrics::tileSize; c+=TileMetrics::tileSize) {
                    for (std::size_t i = 1; i < TileMetrics::tileSize; i++) {
                        reduce[0+c*4] += reduce[0+(i+c)*4];
                        reduce[1+c*4] += reduce[1+(i+c)*4];
                        reduce[2+c*4] += reduce[2+(i+c)*4];
                        reduce[3+c*4] += reduce[3+(i+c)*4];
                    }
                }

                // Create vector
                for (std::size_t c = 0; c < componentCount; c++) {

                    // Get data
                    CTYPE x = reduce[0+c*4*TileMetrics::tileSize];
                    CTYPE y = reduce[1+c*4*TileMetrics::tileSize];
                    CTYPE N = reduce[2+c*4*TileMetrics::tileSize];
                    CTYPE r = reduce[3+c*4*TileMetrics::tileSize];

                    // Add point
                    auto id = v.addPoint( { x/N, y/N } );

                    // Add properties
                    v.template setProperty<cl_uint>(id, "index", (cl_uint)c);
                    
                    switch(reduction) {
                        case Reduction::Count:                            
                            v.template setProperty<CTYPE>(id, name, (CTYPE)(N));
                            break;

                        case Reduction::Sum:
                            v.template setProperty<CTYPE>(id, name, (CTYPE)(r));
                            break;

                        case Reduction::Mean:
                            v.template setProperty<CTYPE>(id, name, (CTYPE)(r/N));
                            break;

                        default:
                            break;
                    }
                }

                // Overwrite Raster
                if (overwrite) {
                    for (uint32_t tj = 0; tj < dim.ty; tj++) {
                        for (uint32_t ti = 0; ti < dim.tx; ti++) {
                            if (verbosity <= Verbosity::Debug) {
                                if (ti == 0&&tj == 0) {
                                    std::cout << "Overwrite raster" << std::endl;
                                }
                                std::cout << "Processing tile: " << ti << "," << tj << std::endl;
                            }

                            // Set kernel arguments
                            kernel_overwrite.setArg(0, index.getTileDataBuffer(ti, tj));
                            kernel_overwrite.setArg(1, getTileDataBuffer(ti, tj));
                            kernel_overwrite.setArg(2, index.getTileDimensions(ti, tj).d);

                            // Execute overwrite kernel
                            queue.enqueueNDRangeKernel(kernel_overwrite, 
                                cl::NullRange, 
                                cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, dim.d.nz), 
                                cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                        
                            if (verbosity <= Verbosity::Debug) {
                                std::cout << "Finished processing tile: " << ti << "," << tj << std::endl;
                            }
                        }
                    }
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

        // Return Vector
        return v;
    }

    /**
    * Index %Vector geometry from %Raster
    * @param v the %Vector to map.
    */
    template <typename RTYPE, typename CTYPE>
    std::vector<RasterIndex<CTYPE> > Raster<RTYPE, CTYPE>::getVectorTileIndexes(Vector<CTYPE> &v, 
        uint32_t ti, uint32_t tj, std::size_t clHash, cl_uchar parameters) {

        // Create return vector
        std::vector<RasterIndex<CTYPE> > rasterIndexes;

        // Get handle to tile
        auto &t = getTile(ti, tj);

        // Map vector to tile
        t.index(v, rasterIndexes, clHash, proj, parameters);

        return rasterIndexes;
    }

    /**
    * Create %Vector of cell centres from %Raster data.
    * @return %Vector containing points of cell centres.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> Raster<RTYPE, CTYPE>::cellCentres(bool mapValues) {
        
        // Create vector
        Vector<CTYPE> v;
        
        if (mapValues) {

            // Get raster name
            std::string name = RasterBase<CTYPE>::template getProperty<std::string>("name");
            if (name.empty()) {
                name = "value";
            }
            v.addProperty(name);
            
            // Create points
            CTYPE py = dim.d.oy+0.5*dim.d.hy;
            for (uint32_t j = 0; j < dim.d.ny; j++) {
                CTYPE px = dim.d.ox+0.5*dim.d.hx;
                for (uint32_t i = 0; i < dim.d.nx; i++) {

                    // Add point
                    auto id = v.addPoint( { px, py } );

                    if (dim.d.nz > 1) {

                        // Get raster values - TODO value type needs to be RTYPE
                        std::vector<CTYPE> values;
                        CTYPE pz = dim.d.oz+0.5*dim.d.hz;
                        for (uint32_t k = 0; k < dim.d.nz; k++) {
                            values.push_back((CTYPE)getNearestValue(px, py, pz));
                            pz += dim.d.hz;
                        }

                        // Write values to Point
                        v.template setProperty<std::vector<CTYPE> >(id, name, values);

                    } else {
                        CTYPE pz = dim.d.oz + 0.5 * dim.d.hz;
                        // Get raster value
                        RTYPE value = getNearestValue(px, py, pz);

                        // Write value to Point
                        v.template setProperty<RTYPE>(id, name, value);
                    }
                    px += dim.d.hx;
                }
                py += dim.d.hy;
            }
        } else {
        
            // Create points
            CTYPE py = dim.d.oy+0.5*dim.d.hy;
            for (uint32_t j = 0; j < dim.d.ny; j++) {
                CTYPE px = dim.d.ox+0.5*dim.d.hx;
                for (uint32_t i = 0; i < dim.d.nx; i++) {

                    // Add point
                   v.addPoint( { px, py } );

                    px += dim.d.hx;
                }
                py += dim.d.hy;
            }
        }

        // Set projection
        v.setProjectionParameters(proj);
        
        return v;
    }

    /**
    * Create %Vector of cell polygons from %Raster data.
    * @return %Vector containing polygons of cells.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> Raster<RTYPE, CTYPE>::cellPolygons(bool mapValues) {
        
        // Create vector
        Vector<CTYPE> v;
        
        if (mapValues) {

            // Get raster name
            std::string name = RasterBase<CTYPE>::template getProperty<std::string>("name");
            if (name.empty()) {
                name = "value";
            }
            v.addProperty(name);
        
            // Create points
            CTYPE py = dim.d.oy;
            CTYPE pcy = dim.d.oy+0.5*dim.d.hy;
            CTYPE py_p1 = py+dim.d.hy;
            for (uint32_t j = 0; j < dim.d.ny; j++) {
                CTYPE px = dim.d.ox;
                CTYPE pcx = dim.d.ox+0.5*dim.d.hx;
                CTYPE px_p1 = px+dim.d.hx;
                for (uint32_t i = 0; i < dim.d.nx; i++) {

                    // Add polygon
                    auto id = v.addPolygon( { { { px, py }, { px, py_p1 }, { px_p1, py_p1 }, { px_p1, py } } } );
                    
                    if (dim.d.nz > 1) {

                        // Get raster values - TODO value type needs to be RTYPE
                        std::vector<CTYPE> values;
                        CTYPE pz = dim.d.oz+0.5*dim.d.hz;
                        for (uint32_t k = 0; k < dim.d.nz; k++) {
                            values.push_back((CTYPE)getNearestValue(pcx, pcy, pz));
                            pz += dim.d.hz;
                        }
                        
                        // Write values to Polygon
                        v.template setProperty<std::vector<CTYPE> >(id, name, values);

                    } else {                    

                        CTYPE pcz = dim.d.oz + 0.5 * dim.d.hz;
                        // Get raster value
                        RTYPE value = getNearestValue(pcx, pcy, pcz);

                        // Write value to Polygon
                        v.template setProperty<RTYPE>(id, name, value);
                    }
                    px = px_p1;
                    pcx += dim.d.hx;
                    px_p1 += dim.d.hx;
                }
                py = py_p1;
                pcy += dim.d.hy;
                py_p1 += dim.d.hy;
            }
        } else {

            // Create points
            CTYPE py = dim.d.oy;
            CTYPE pcy = dim.d.oy+0.5*dim.d.hy;
            CTYPE py_p1 = py+dim.d.hy;
            for (uint32_t j = 0; j < dim.d.ny; j++) {
                CTYPE px = dim.d.ox;
                CTYPE pcx = dim.d.ox+0.5*dim.d.hx;
                CTYPE px_p1 = px+dim.d.hx;
                for (uint32_t i = 0; i < dim.d.nx; i++) {

                    // Add polygon
                    v.addPolygon( { { { px, py }, { px, py_p1 }, { px_p1, py_p1 }, { px_p1, py } } } );

                    px = px_p1;
                    pcx += dim.d.hx;
                    px_p1 += dim.d.hx;
                }
                py = py_p1;
                pcy += dim.d.hy;
                py_p1 += dim.d.hy;
            }
        }

        // De-duplicate vertices
        v.deduplicateVertices();

        // Set projection
        v.setProjectionParameters(proj);
        
        return v;
    }

    /**
    * Vectorise %Raster data, returning a closed contour.
    * @return contour %Vector of %Raster.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> Raster<RTYPE, CTYPE>::vectorise(std::vector<RTYPE> countourValues, RTYPE noDataValue) {
        
        // Create vector
        Vector<CTYPE> v;

        // Check contour values
        if (countourValues.size() == 0) {
            std::cout << "WARNING: No contour values to vectorise" << std::endl;
            return v;
        }

        try {

            // Get OpenCL handles
            cl_int err = CL_SUCCESS;
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Build kernel code
            std::string kernelStr = Models::cl_vectorise_c;

            // Build program
            std::size_t clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build vectorise program");
            }

            // Get kernel
            auto vectoriseKernel = solver.getKernel(clHash, "vectorise");

            // Create index and coordinate buffers
            cl::Buffer bi = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_uint), NULL, &err);
            cl::Buffer bc = cl::Buffer(context, CL_MEM_WRITE_ONLY, 8*TileMetrics::tileSizeSquared*sizeof(CTYPE), NULL, &err);
            cl::Buffer br = cl::Buffer(context, CL_MEM_WRITE_ONLY, 4*TileMetrics::tileSizeSquared*sizeof(cl_uint), NULL, &err);
            if (err != CL_SUCCESS) {
                throw std::runtime_error("Cannot create vectorise buffers");
            }

            // Clear coordinate buffer
            std::size_t nVerbose = 0;
            std::size_t nVerboseCount = 0;
            std::size_t verboseMax = dim.tx*dim.ty*countourValues.size();
            
            // Loop over contour values
            const CTYPE epsSqr = 1.0E-6;
            for (auto contourValue : countourValues) {

                // Coordinates for each tile
                std::vector<CoordinateList<CTYPE> > coordListAll;

                // Loop through tiles
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                    
                        // Get tile handle
                        auto &t = getTile(ti, tj);
                        
                        // Clear index buffer
                        solver.fillBuffer<cl_uint>(bi, 0, 1);

                        // Vectorise tile
                        vectoriseKernel.setArg( 0, contourValue);
                        vectoriseKernel.setArg( 1, t.getDataBuffer());

                        // Add north buffer
                        if (tj < dim.ty-1) {
                            auto &tN = getTile(ti, tj+1);
                            vectoriseKernel.setArg( 2, tN.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 2, getNullBuffer());
                        }

                        // Add east buffer
                        if (ti < dim.tx-1) {
                            auto &tE = getTile(ti+1, tj);
                            vectoriseKernel.setArg( 3, tE.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 3, getNullBuffer());
                        }

                        // Add north east buffer
                        if (ti < dim.tx-1 && tj < dim.ty-1) {
                            auto &tNE = getTile(ti+1, tj+1);
                            vectoriseKernel.setArg( 4, tNE.getDataBuffer());
                        } else {
                            vectoriseKernel.setArg( 4, getNullBuffer());
                        }

                        vectoriseKernel.setArg( 5, t.getDimensions());
                        vectoriseKernel.setArg( 6, noDataValue);
                        vectoriseKernel.setArg( 7, bc);
                        vectoriseKernel.setArg( 8, br);
                        vectoriseKernel.setArg( 9, bi);
                        queue.enqueueNDRangeKernel(vectoriseKernel,  
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, 1), 
                            cl::NDRange(TileMetrics::workgroupSize, 1, 1));

                        // Flush queue
                        queue.flush();

                        // Read number of coordinates
                        cl_uint nc = 0;
                        queue.enqueueReadBuffer(bi, CL_TRUE, 0, sizeof(cl_uint), &nc);
            
                        if (nc > 0) {

                            // Read coordinates
                            std::vector<CTYPE> coords(2*nc);
                            queue.enqueueReadBuffer(bc, CL_TRUE, 0, 2*nc*sizeof(CTYPE), coords.data());
                            std::vector<cl_uint> indexes(nc);
                            queue.enqueueReadBuffer(br, CL_TRUE, 0, nc*sizeof(cl_uint), indexes.data());

                            // Create index map
                            std::set<std::size_t> unprocessedEdges;
                            std::multimap<uint64_t, std::size_t> indexMap;
                            for (std::size_t c = 0; c < nc; c+=2) {

                                // Add index
                                uint64_t i = indexes[c  ];
                                uint64_t j = indexes[c+1];
                                indexMap.insert( { (i<<32)|j, c } );

                                // Add edge id
                                unprocessedEdges.insert(c>>1);
                            }
                            
                            // Join edges
                            while (!unprocessedEdges.empty()) {

                                coordListAll.push_back(CoordinateList<CTYPE>());
                                CoordinateList<CTYPE> &coordList = coordListAll.back();
                                auto firstEdgeID = *unprocessedEdges.begin();

                                // Search forwards and backwards along edge
                                for (int k = 0; k < 2; k++) {

                                    // Get initial edge
                                    auto eit = unprocessedEdges.find(firstEdgeID);
                                    bool foundEdge = true;

                                    // Parse connected edges
                                    while (foundEdge) {

                                        // Get current edge
                                        auto currentEdge = *eit;
                                        unprocessedEdges.erase(eit);

                                        // Get edge coordinates
                                        std::size_t cOff = currentEdge<<2;
                                        auto x0 = coords[cOff  ];
                                        auto y0 = coords[cOff+1];
                                        auto x1 = coords[cOff+2];
                                        auto y1 = coords[cOff+3];
                                
                                        // Get cell index
                                        std::size_t iOff = currentEdge<<1;
                                        uint64_t i = indexes[iOff];
                                        uint64_t j = indexes[iOff+1];

                                        // Find joining edge
                                        foundEdge = false;
                                        for (int s = 0; s < 4; s++) {
                                            
                                            // Get joining edge index
                                            uint64_t sIndex;
                                            if (s == 0) {
                                                if (i == dim.d.nx-1) continue;
                                                sIndex = ((i+1)<<32)|j;
                                            } else if (s == 1) {
                                                if (j == dim.d.ny-1) continue;
                                                sIndex = (i<<32)|(j+1);
                                            } else if (s == 2) {
                                                if (i == 0) continue;
                                                sIndex = ((i-1)<<32)|j;
                                            } else if (s == 3) {
                                                if (j == 0) continue;
                                                sIndex = (i<<32)|(j-1);
                                            }

                                            // Loop over cells
                                            for (auto its = indexMap.lower_bound(sIndex); its != indexMap.upper_bound(sIndex); its++) {
                                                auto sit = unprocessedEdges.find(its->second>>1);
                                                if (sit != unprocessedEdges.end()) {
                                            
                                                    // Search for connecting point
                                                    std::size_t offs = its->second<<1;                                 
                                                    if (distanceSqr2D(x0, y0, coords[offs  ], coords[offs+1]) < epsSqr) {
                                                        foundEdge = true;
                                                        coordList.push_back( { x0, y0 } );
                                                    } else if (distanceSqr2D(x0, y0, coords[offs+2], coords[offs+3]) < epsSqr) {
                                                        foundEdge = true;
                                                        coordList.push_back( { x0, y0 } );
                                                    } else if (distanceSqr2D(x1, y1, coords[offs  ], coords[offs+1]) < epsSqr) {
                                                        foundEdge = true;
                                                        coordList.push_back( { x1, y1 } );
                                                    } else if (distanceSqr2D(x1, y1, coords[offs+2], coords[offs+3]) < epsSqr) {
                                                        foundEdge = true;
                                                        coordList.push_back( { x1, y1 } );
                                                    }

                                                    // Move to next edge
                                                    if (foundEdge) {
                                                        eit = sit;
                                                        break;
                                                    }
                                                }
                                            }
                                            // Move to next edge
                                            if (foundEdge) {
                                                break;
                                            }
                                        }

                                        // Add last point
                                        if (!foundEdge) {
                                            if (coordList.size() == 0 || distanceSqr2D<CTYPE>(x0, y0, coordList.back().p, coordList.back().q) < epsSqr) {
                                                coordList.push_back( { x1, y1 } );
                                            } else {
                                                coordList.push_back( { x0, y0 } );
                                            }
                                        }
                                    }

                                    if (k == 0) {

                                       // Flip current edges
                                       std::reverse(coordList.begin(), coordList.end());

                                       // Re-add first edge to trigger backwards search
                                       unprocessedEdges.insert(firstEdgeID);
                                    }
                                }
                            }
                        }
                    }
                }

                // Check for joins over tiles
                bool foundLink = true;
                while (foundLink) {
                    foundLink = false;
                    for (std::size_t i = 0; i < coordListAll.size(); i++) {
                        for (std::size_t j = 0; j < coordListAll.size(); j++) {
                            if (i != j && coordListAll[i].size() > 0 && coordListAll[j].size() > 0) {
                                if (Coordinate<CTYPE>::distanceSqr2D(coordListAll[i].back(), coordListAll[j].front()) < epsSqr) {
                                    coordListAll[i].insert(coordListAll[i].end(), coordListAll[j].begin()+1, coordListAll[j].end());
                                    coordListAll[j].clear();
                                    foundLink = true;
                                } else if (Coordinate<CTYPE>::distanceSqr2D(coordListAll[j].back(), coordListAll[i].front()) < epsSqr) {
                                    coordListAll[j].insert(coordListAll[j].end(), coordListAll[i].begin()+1, coordListAll[i].end());
                                    coordListAll[i].clear();
                                    foundLink = true;
                                } else if (Coordinate<CTYPE>::distanceSqr2D(coordListAll[i].front(), coordListAll[j].front()) < epsSqr) { 
                                    coordListAll[i].insert(coordListAll[i].begin(), coordListAll[j].rbegin(), coordListAll[j].rend()-1);
                                    coordListAll[j].clear();
                                    foundLink = true;
                                } else if (Coordinate<CTYPE>::distanceSqr2D(coordListAll[j].back(), coordListAll[i].back()) < epsSqr) {   
                                    coordListAll[j].insert(coordListAll[j].end(), coordListAll[i].rbegin()+1, coordListAll[i].rend());
                                    coordListAll[i].clear();
                                    foundLink = true;
                                }
                            }
                        }
                    }
                }
                
                // Update vector
                for (auto &coordList : coordListAll) {
                    if (coordList.size() > 0) {
                        auto id = v.addLineString(coordList);
                        v.template setProperty<CTYPE>(id, "value", contourValue);
                    }
                }
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

        // De-duplicate
        v.deduplicateVertices();

        // Set projection
        v.setProjectionParameters(proj);
        
        return v;
    }
    
    /**
    * Creates an OpenCL buffer from a %Raster region.
    * @param bounds the %BoundingBox of the region.
    * @param regionBuffer a reference to a region buffer to return.
    * @param rasterRegionDim a reference to a %RasterDimensions object to return.
    * @return true if the raster overlaps the region, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    bool Raster<RTYPE, CTYPE>::createRegionBuffer(
        BoundingBox<CTYPE> bounds, 
        cl::Buffer &rasterRegionBuffer, 
        RasterDimensions<CTYPE> &rasterRegionDim) {

        try {

            // Check raster
            if (hasData()) {
            
                // Get region
                std::vector<GeometryBasePtr<CTYPE> > tileList;
                tree.search(bounds, tileList, GeometryType::Tile);

                // Check for null result
                if (tileList.size() != 0) {

                    // Get tile index bounds
                    uint32_t ti_min = std::numeric_limits<uint32_t>::max();
                    uint32_t ti_max = 0;
                    uint32_t tj_min = std::numeric_limits<uint32_t>::max();
                    uint32_t tj_max = 0;
                    for (auto &g : tileList) {

                        // Get tile pointer
                        auto tp = std::static_pointer_cast<Tile<RTYPE, CTYPE> >(g);
                        
                        // Tile relative position
                        auto tDim = tp->getTileDimensions();
                        ti_min = std::min(tDim.ti, ti_min);
                        ti_max = std::max(tDim.ti, ti_max);
                        tj_min = std::min(tDim.tj, tj_min);
                        tj_max = std::max(tDim.tj, tj_max);
                    }
                    uint32_t ntx = ti_max-ti_min+1;
                    uint32_t nty = tj_max-tj_min+1;
                    
                    // Calculate end offsets
                    uint32_t dx = std::max((int64_t)(ti_max+1)*TileMetrics::tileSize-(int64_t)dim.d.nx, (int64_t)0);
                    uint32_t dy = std::max((int64_t)(tj_max+1)*TileMetrics::tileSize-(int64_t)dim.d.ny, (int64_t)0);

                    // Set dimensions
                    rasterRegionDim.d.hx = dim.d.hx;
                    rasterRegionDim.d.hy = dim.d.hy;
                    rasterRegionDim.d.hz = dim.d.hz;
                    rasterRegionDim.d.nx = ntx*TileMetrics::tileSize-dx;
                    rasterRegionDim.d.ny = nty*TileMetrics::tileSize-dy;
                    rasterRegionDim.d.nz = dim.d.nz;
                    rasterRegionDim.d.ox = dim.d.ox+dim.d.hx*ti_min*TileMetrics::tileSize;
                    rasterRegionDim.d.oy = dim.d.oy+dim.d.hy*tj_min*TileMetrics::tileSize;
                    rasterRegionDim.d.oz = dim.d.oz;
                    rasterRegionDim.d.mx = ntx*TileMetrics::tileSize;
                    rasterRegionDim.d.my = nty*TileMetrics::tileSize;
                    rasterRegionDim.ex = dim.d.ox+dim.d.hx*(ti_max+1)*TileMetrics::tileSize;
                    rasterRegionDim.ey = dim.d.oy+dim.d.hy*(tj_max+1)*TileMetrics::tileSize;
                    rasterRegionDim.ez = dim.ez;
                    rasterRegionDim.tx = ntx;
                    rasterRegionDim.ty = nty;

                    // Return tile buffer if only one tile is found
                    if (ntx == 1 && nty == 1) {
                        rasterRegionBuffer = getTile(ti_min, tj_min).getDataBuffer();
                        return true;
                    }

                    // Get OpenCL handles
                    cl_int err = CL_SUCCESS;
                    auto &solver = Geostack::Solver::getSolver();
                    auto &context = solver.getContext();
                    auto &queue = solver.getQueue();

                    // Set block sizes
                    cl::detail::size_t_array srcOrigin;
                    srcOrigin[0] = 0;
                    srcOrigin[1] = 0;
                    srcOrigin[2] = 0;

                    cl::detail::size_t_array region;
                    region[0] = TileMetrics::tileSize*sizeof(RTYPE);
                    region[1] = TileMetrics::tileSize;
                    region[2] = dim.d.nz;

                    cl::detail::size_t_array dstOrigin;
                    dstOrigin[2] = 0;

                    // Get buffer
                    std::size_t dataSize = (std::size_t)ntx*nty*TileMetrics::tileSizeSquared*dim.d.nz;
                    rasterRegionBuffer = RasterBase<CTYPE>::getRegionCacheBuffer(dataSize*sizeof(RTYPE));
                    solver.fillBuffer<RTYPE>(rasterRegionBuffer, getNullValue<RTYPE>(), dataSize);

                    // Loop over tiles
                    for (uint32_t rtj = 0; rtj < nty; rtj++) {
                        for (uint32_t rti = 0; rti < ntx; rti++) {
                                        
                            // Set destination block
                            dstOrigin[0] = (cl::size_type)rti*TileMetrics::tileSize*sizeof(RTYPE);
                            dstOrigin[1] = (cl::size_type)rtj*TileMetrics::tileSize;

                            // Copy tiles into buffer
                            queue.enqueueCopyBufferRect(getTile(ti_min+rti, tj_min+rtj).getDataBuffer(), 
                                rasterRegionBuffer, srcOrigin, dstOrigin, region, 
                                (cl::size_type)TileMetrics::tileSize*sizeof(RTYPE), 
                                (cl::size_type)TileMetrics::tileSizeSquared*sizeof(RTYPE), 
                                (cl::size_type)ntx*TileMetrics::tileSize*sizeof(RTYPE), 
                                (cl::size_type)ntx*nty*TileMetrics::tileSizeSquared*sizeof(RTYPE));
                        }
                    }
                    return true;
                }
            }
            return false;

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    /**
     * %Raster close file.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::closeFile() {
        if (fileHandlerIn != nullptr) {
            fileHandlerIn->closeFile();
        }
        if (fileHandlerOut != nullptr) {
            fileHandlerOut->closeFile();
        }
    }

    /**
    * Read file to %Raster.
    * @param fileName name of file to read.
    * @return true on successful read.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::read(std::string fileName, std::string jsonConfig) {
    
        if (fileName.size() > 4 && fileName.substr(0, 4) == "http") {
        
            fileHandlerIn = std::make_shared<DAPHandler<RTYPE, CTYPE> >();

        } else {

            // Get filename extension
            auto split = Strings::splitPath(fileName);

            if (split[2] == "asc" || split[2] == "ascii") {

                // Create handler
                fileHandlerIn = std::make_shared<AsciiHandler<RTYPE, CTYPE> >();

            } else if (split[2] == "flt") {

                // Create handler
                fileHandlerIn = std::make_shared<FltHandler<RTYPE, CTYPE> >();

            } else if (split[2] == "tif" || split[2] == "tiff"|| split[2] == "gtiff" || split[2] == "geotiff") {

                // Create handler
                fileHandlerIn = std::make_shared<GeoTIFFHandler<RTYPE, CTYPE> >();

            } else if (split[2] == "nc" || split[2] == "nc3" || split[2] == "netcdf") {

                // Create handler
                fileHandlerIn = std::make_shared<NetCDFHandler<RTYPE, CTYPE> >();

            } else if (split[2] == "gsr") {

                // Create handler
                fileHandlerIn = std::make_shared<GsrHandler<RTYPE, CTYPE> >();

            } else if (split[2] == "png") {

                // Create handler
                fileHandlerIn = std::make_shared<PngHandler<RTYPE, CTYPE> >();

            } else {
                std::stringstream err;
                err << "File name has unrecognised extension '" << split[2] << "'";
                throw std::runtime_error(err.str());
            }
        }

        // Set as constant data source
        this->setConst(true);

        // Process file
        fileHandlerIn->read(fileName, *this, jsonConfig);
    }

    /**
    * Write file to %Raster.
    * @param fileName name of file to read.
    * @return true on successful write.
    */
    template <typename RTYPE, typename CTYPE>
    void Raster<RTYPE, CTYPE>::write(std::string fileName, std::string jsonConfig) {
    
        // Check for data
        if (!hasData()) {
            throw std::runtime_error("Raster has no data to write");
        }
    
        // Get filename extension
        auto split = Strings::splitPath(fileName);

        if (split[2] == "asc") {

            // Create handler
            fileHandlerOut = std::make_shared<AsciiHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "flt") {

            // Create handler
            fileHandlerOut = std::make_shared<FltHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "tif" || split[2] == "tiff") {

            // Create handler
            fileHandlerOut = std::make_shared<GeoTIFFHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "gsr") {

            // Create handler
            fileHandlerOut = std::make_shared<GsrHandler<RTYPE, CTYPE> >();

        } else if (split[2] == "png") {

            // Create handler
            fileHandlerOut = std::make_shared<PngHandler<RTYPE, CTYPE> >();

        } else {
        
            std::stringstream err;
            err << "File name has unrecognised extension '" << split[2] << "'";
            throw std::runtime_error(err.str());
        }

        // Process file
        fileHandlerOut->write(fileName, *this, jsonConfig);
    }
    
    /**
    * Create script hash.
    * @param script OpenCL script to run.
    * @param kernelBlock OpenCL code block.
    * @param rasterBaseRefs list of input raster references.
    * @return script hash.
    */
    template <typename CTYPE>
    std::size_t createScriptHash(
        std::string script, 
        std::string kernelBlock, 
        RasterBaseRefs<CTYPE> rasterBaseRefs) {

        // Build library lookup
        std::string scriptLookup;
        if (rasterBaseRefs.size() > 1) {
            auto dims = rasterBaseRefs[0].get().getRasterDimensions().d;
            for (int i = 1; i < rasterBaseRefs.size(); i++) {
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
                scriptLookup += equalSpatialMetrics2D(dims, rasterBase.getRasterDimensions().d) ? "S" : "D";
            }
        }
        for (int i = 0; i < rasterBaseRefs.size(); i++) {
            RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
            scriptLookup += rasterBase.getHashString();
        }
        scriptLookup+=script;
        scriptLookup+=kernelBlock;

        // Search for program
        std::hash<std::string> hasher;
        return hasher(scriptLookup);
    }
    
    /**
    * Create script hash.
    * @param script OpenCL script to run.
    * @param kernelBlock OpenCL code block.
    * @param rasterBase input Raster.
    * @return script hash.
    */
    template <typename CTYPE>
    std::size_t createScriptHash(
        std::string script, 
        std::string kernelBlock, 
        RasterBase<CTYPE> &rasterBase) {

        // Build library lookup
        std::string scriptLookup = rasterBase.getHashString();
        scriptLookup+=script;
        scriptLookup+=kernelBlock;

        // Search for program
        std::hash<std::string> hasher;
        return hasher(scriptLookup);
    }

    /**
    * Generates OpenCL kernel.
    * @param script OpenCL script to run.
    * @param kernelBlock OpenCL code block.
    * @param rasterBaseRefs list of input raster references.
    * @param rasterNullValue handling type for null values.
    * @param variables named Variables to use in the kernel.
    * @param useConst force all input variables to be const.
    * @return processed OpenCL code block, null script on error.
    */
    template <typename CTYPE>
    RasterKernelGenerator generateKernel(
        std::string script, 
        std::string kernelBlock, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        RasterNullValue::Type rasterNullValue,
        VariablesBasePtr<std::string> variables,
        bool useConst,
        bool addTerminator) {

        // Create variable strings
        std::string argsList;
        std::string variableListHi;
        std::string variableListLow;
        std::string reduceHead;
        std::string reduceCode;
        std::string reducePost;
        std::string reduceVarPost;
        std::string post;
        std::set<std::string> names;

        // Create generator        
        RasterKernelGenerator kernelGenerator;
        kernelGenerator.reqs.rasterRequirements.resize(rasterBaseRefs.size());

        // Strip all single-line comments from strings
        script = std::regex_replace(script, std::regex("\\s*//.*"), std::string(""));
        kernelBlock = std::regex_replace(kernelBlock, std::regex("\\s*//.*"), std::string(""));

        // Create full script
        std::string fullScript = kernelBlock+script;

        // Create cache data
        std::map<std::string, std::pair<uint32_t, std::string> > cacheItems;

        // Update variable list
        std::map<std::string, std::string> variableTable;
        if (variables != nullptr && variables->hasData()) {

            // Get type
            auto vType = variables->getOpenCLTypeString();

            // Add to globals
            argsList += "__global " + vType + " *_vars,\n";

            // Loop over variables
            for (auto vi : variables->getIndexes()) {

                // Check for duplicate names
                std::string vName = vi.first;
                if (names.find(vName) != names.end()) {
                    throw std::runtime_error("variable name '" + vName + "' cannot have the same name as another variable");
                }
                names.insert(vName);
                
                auto vFindVar = std::regex("\\b" + vName + "\\b");
                if (std::regex_search(fullScript, vFindVar)) {
                    auto vFind = std::regex("\\b" + vName + "\\s*\\[\\s*(.+?)\\s*\\]");
                    if (std::regex_search(fullScript, vFind)) {

                        // Use square brackets for variable array offset
                        variableListHi += "const uint " + vName + "_length = " + std::to_string(variables->getSize(vName)) + ";\n";
                        script = std::regex_replace(script, vFind, 
                            std::string("*(_vars+") + std::to_string(vi.second) + "+min_uint_DEF(($1), " + vName + "_length-1))");
                    } else {

                        // Add variable to script
                        variableListHi += vType + " " + 
                            vName + " = *(_vars+" + std::to_string(vi.second) + ");\n";
                        if (!useConst) {
                            post += "*(_vars+" + std::to_string(vi.second) + ") = " + vName + ";\n";
                        }
                    }
                }
            }
        }

        bool needsReduction = false;

        // Parse rasters
        if (rasterBaseRefs.size() > 0) {

            bool needsNeighbours = false;
            bool hasProjection = false;

            // Add variable for valid raster count
            if (rasterNullValue != RasterNullValue::Null) {
                variableListHi += "uint __layer_count = 0;\n";
            }

            // Get handle to anchor layer
            RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
            auto dimFirst = rasterBaseFirst.getRasterDimensions();            
            auto projFirst = rasterBaseFirst.getProjectionParameters();

            // Add random buffer
            if (std::regex_search(fullScript, std::regex("\\brandom\\b")) ||
                std::regex_search(fullScript, std::regex("\\brandomNormal\\b")) ||
                std::regex_search(fullScript, std::regex("\\brandomUniform\\b"))) {

                // Add random code
                kernelGenerator.reqs.usesRandom = true;

                // Add random variables
                argsList += "__global RandomState *_rs,\n";
                bool is2D = (dimFirst.d.nz == 1);
                if (is2D) {
                    variableListHi += "RandomState _rsl = _val2D_a(_rs, _i, _j);\n";
                    post += "_val2D_a(_rs, _i, _j) = _rsl;\n";
                } else {
                    variableListHi += "RandomState _rsl = _val3D_a(_rs, _i, _j, _k);\n";
                    post += "_val3D_a(_rs, _i, _j, _k) = _rsl;\n";
                }
                variableListHi += "RandomState *_prsl = &_rsl;\n";
                script = std::regex_replace(script, std::regex("\\brandom\\b"), std::string("random_REAL(_prsl)"));
                script = std::regex_replace(script, std::regex("\\brandomNormal\\("), std::string("randomNormal_REAL(_prsl, "));
                script = std::regex_replace(script, std::regex("\\brandomUniform\\("), std::string("randomUniform_REAL(_prsl, "));
            }

            // Add boundary handling
            bool needsBoundary = false;
            if (std::regex_search(fullScript, std::regex("\\bisBoundary(_N|_E|_S|_W)\\b"))) {
                needsBoundary = true;
                kernelGenerator.reqs.usesBoundary = true;
            }

            // Check all names
            for (int i = 0; i < rasterBaseRefs.size(); i++) {
                    
                // Get raster handle
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();

                // Check Raster is initialised
                if (!rasterBase.hasData()) {
                    throw std::runtime_error("Kernel cannot use uninitialised raster");
                }
                
                // Get and check name
                if (!rasterBase.hasProperty("name")) {
                    throw std::runtime_error("Raster in kernel has no name");
                }
                std::string rName = rasterBase.template getProperty<std::string>("name");
                if (rName.empty()) {
                    throw std::runtime_error("Raster in kernel has empty name");
                }
                for (char &c : rName) {
                    if (std::isalnum(static_cast<unsigned char>(c)) == 0 && c != '_') {
                        throw std::runtime_error("Raster name '" + rName + "' must contain only alphanumeric characters or underscores");
                    }
                }
                if (names.find(rName) != names.end()) {
                    throw std::runtime_error("Raster '" + rName + "' cannot have the same name as another raster or variable");
                }

                // Patch any indexed kernel names
                std::string varReplace = "\\/\\*__VAR" + std::to_string(i) + "__\\*\\/";
                kernelBlock = std::regex_replace(kernelBlock, std::regex(varReplace), rName);
            }

            // Loop over rasters
            for (int i = 0; i < rasterBaseRefs.size(); i++) {
                    
                // Get raster handle
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();

                // Check for name in script
                std::string rName = rasterBase.template getProperty<std::string>("name");
                auto regName = std::regex("\\b_?" + rName + "(_layers|_N|_E|_S|_W|_NE|_SE|_SW|_NW)?\\b");
                if (!std::regex_search(script, regName) && 
                    !std::regex_search(kernelBlock, regName) &&
                    rasterBase.getReductionType() == Reduction::None) {

                    // Skip processing
                    kernelGenerator.reqs.rasterRequirements[i].used = false;
                    continue;
                }

                // Process raster
                kernelGenerator.reqs.rasterRequirements[i].used = true;
                names.insert(rName);

                // Check projection
                auto rProj = rasterBase.getProjectionParameters();
                if (i == 0 && rProj.type != 0) {
                    hasProjection = true;
                }
                if (rProj.type == 0 && hasProjection) {
                    throw std::runtime_error("One or more raster projections is undefined");
                }

                // Check projection type
                if (hasProjection && !Projection::checkProjection(rProj)) {
                    throw std::runtime_error("Invalid projection type for Raster '" + rName + "'");
                }

                // Update variable list
                if (rasterBase.hasVariables()) {
                
                    // Get type
                    auto vType = rasterBase.getVariablesType();

                    // Add variable
                    std::string rasterVarName = "_vars_" + rName;
                    argsList += "__global " + vType + " *" + rasterVarName + ",\n";

                    // Loop over variables
                    for (auto vi : rasterBase.getVariablesIndexes()) {
                    
                        // Check variable name
                        for (const char &c : vi.first) {
                            if (std::isalnum(static_cast<unsigned char>(c)) == 0 && c != '_') {
                                throw std::runtime_error("Variable name '" + vi.first + "' must contain only alphanumeric characters or underscores");
                            }
                        }

                        // Check for duplicate names
                        std::string vName = rName + "_" + vi.first;
                        if (names.find(vName) != names.end()) {
                            throw std::runtime_error("raster name '" + vName + "' is not unique");
                        }
                        names.insert(vName);
                        
                        auto vFind = std::regex("\\b" + rName + "::" + vi.first + "\\s*\\[\\s*(.+?)\\s*\\]");
                        if (std::regex_search(script, vFind)) {

                            // Use square brackets for variable array offset
                            script = std::regex_replace(script, vFind, 
                                std::string("*(") + rasterVarName + "+" + std::to_string(vi.second) + "+min_uint_DEF(($1), " + 
                                std::to_string(rasterBase.getVariableSize(vi.first)-1) + "))");
                        } else {

                            // Add to kernel variable list
                            variableListHi += vType + " " + vName + 
                                " = *(" + rasterVarName + "+" + std::to_string(vi.second) + ");\n";
                            if (!useConst) {
                                post += "*(" + rasterVarName + "+" + std::to_string(vi.second) + ") = " + vName + ";\n";
                            }

                            // Add variable to lookup table
                            variableTable[rName + "::" + vi.first] = vName;
                        }
                    }
                }

                // Get raster information
                auto rType = rasterBase.getOpenCLTypeString();
                auto rasterDim = rasterBase.getRasterDimensions();
                auto reduction = rasterBase.getReductionType();
                bool is2D = (rasterDim.d.nz == 1);

                // Add raster types to generator
                kernelGenerator.rasterTypes.insert(rType);
            
                // Check for aligned layers these are be optimised for access
                if (equalSpatialMetrics2D(rasterDim.d, dimFirst.d)) {

                    // Update parameter list
                    argsList += "__global " + rType + " *_" + rName + ",\n";

                    // Update script for raster value
                    if (rasterBase.getNeedsRead()) {
                        if (is2D) {
                            variableListHi += rType + " " + rName + " = _val2D_a(_" + rName + ", _i, _j);\n";
                        } else {
                    
                            if (rasterDim.d.nz == dimFirst.d.nz) {

                                // Use aligned value
                                variableListHi += rType + " " + rName + " = _val3D_a(_" + rName + ", _i, _j, _k);\n";

                            } else {
                                
                                // Check for 3D references
                                if (!script.empty() && !std::regex_search(script, std::regex("\\b" + rName + "\\s*\\["))) {
                                    if (reduction != Reduction::None) {
                                        throw std::runtime_error("3D raster '" + rName + "' cannot be reduced when using 2D anchor layer");
                                    } else {
                                        throw std::runtime_error("3D raster '" + rName + "' has no layer specified when referenced by 2D raster");
                                    }
                                }
                            }

                            // Use square brackets for k-index
                            if (!script.empty()) {

                                if (dimFirst.d.nz == 1) {

                                    // Allow access to 3D layers if anchor layer is 2D                                
                                    script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                        std::string("_val3D_a(_") + rName + ", _i, _j, min_uint_DEF(($1), " + std::to_string(rasterDim.d.nz-1) + "))");

                                } else {

                                    // Otherwise disable access to 3D layers
                                    script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                        std::string("getValueAtZ_" + rType + "(_") + rName + ", _i, _j, ($1), _dim.mx, _dim.my, " + std::to_string(rasterDim.d.nz) + ")");
                                }
                            }

                            // Add range variable                        
                            variableListHi += "const uint " + rName + "_layers = " + std::to_string(rasterDim.d.nz) +";\n";
                        }
                    } else {
                        variableListHi += rType + " " + rName + " = noData_" + rType + ";\n";
                    }

                    // Check for neighbours
                    std::vector<std::string> neighbourSuffix = { "N", "NE", "E", "SE", "S", "SW", "W", "NW" };
                    uint8_t requiredNeighbours = rasterBase.getRequiredNeighbours();
                    if (std::regex_search(fullScript, std::regex("\\b" + rName + "(_N|_E|_S|_W|_NE|_SE|_SW|_NW)\\b"))) {
                        for (uint32_t i = 0; i < 8; i++) {
                            if (std::regex_search(fullScript, std::regex("\\b" + rName + "(_" + neighbourSuffix[i] + ")\\b"))) {
                                requiredNeighbours |= (uint8_t)(1 << i);
                            }
                        }
                    }

                    // If diagonal neighbours are required, adjacent neighbours must also be added
                    if (requiredNeighbours & Neighbours::NE) {
                        requiredNeighbours |= Neighbours::N;
                        requiredNeighbours |= Neighbours::E;
                    }
                    if (requiredNeighbours & Neighbours::SE) {
                        requiredNeighbours |= Neighbours::S;
                        requiredNeighbours |= Neighbours::E;
                    }
                    if (requiredNeighbours & Neighbours::SW) {
                        requiredNeighbours |= Neighbours::S;
                        requiredNeighbours |= Neighbours::W;
                    }
                    if (requiredNeighbours & Neighbours::NW) {
                        requiredNeighbours |= Neighbours::N;
                        requiredNeighbours |= Neighbours::W;
                    }

                    // Check for operators
                    auto rGrad = std::regex("\\bgrad\\(\\s*" + rName + "\\s*\\)");
                    if (std::regex_search(script, rGrad)) {
                        script = std::regex_replace(script, rGrad, std::string("_grad_a(") + rName + ")");
                        requiredNeighbours = Neighbours::Queen;
                    } else if (std::regex_search(script, std::regex("\\b" + rName + "\\b\\("))) {

                        // General offset operator
                        requiredNeighbours = Neighbours::Queen;
                        if (is2D) {
                            script = std::regex_replace(script, std::regex("\\b" + rName + "\\b\\("), 
                                std::string("_getValue2D(_") + rName + ","
                                "_" + rName + "_N, " +
                                "_" + rName + "_E, " +
                                "_" + rName + "_S, " +
                                "_" + rName + "_W, " +
                                "_" + rName + "_NE, " +
                                "_" + rName + "_SE, " +
                                "_" + rName + "_SW, " +
                                "_" + rName + "_NW, " +
                                "_dim, _i, _j, ");
                        }
                    }

                    // Update raster list
                    kernelGenerator.reqs.rasterRequirements[i].requiredNeighbours = requiredNeighbours;
                                
                    // Add neighbours to arguments
                    for (uint32_t i = 0; i < 8; i++) {
                        if (requiredNeighbours & (uint8_t)(1 << i)) {
                            argsList += "__global " + rType + " *_" + rName + "_" + neighbourSuffix[i] + ",\n";
                            if (is2D) {
                                variableListHi += rType + " " + rName + "_" + neighbourSuffix[i] + " = _val2D_a_" + neighbourSuffix[i] + "(_" + rName + ", _i, _j);\n";
                            } else {
                                variableListHi += rType + " " + rName + "_" + neighbourSuffix[i] + " = _val3D_a_" + neighbourSuffix[i] + "(_" + rName + ", _i, _j, _k);\n";
                            }
                            needsNeighbours = true;
                        }
                    }

                    // Check whether reductions are required on raster
                    if (reduction != Reduction::None) {

                        // Update reduction parameter list
                        argsList += "__global " + rType + " *_" + rName + "_reduce,\n";
                        std::string reduceVar = "_" + rName + "_cache+_lid";

                        // Set reduction function
                        if (reduction == Reduction::Maximum) {
                            reduceVarPost += "*(" + reduceVar + ") = " + rName + ";\n";
                            reduceHead += "__local " + rType + " _" + rName + "_cache[" + std::to_string(TileMetrics::workgroupSize) + "];\n";
                            reduceHead += "*(" + reduceVar + ") = noData_" + rType + ";\n";
                            reduceCode += "if (isValid_" + rType + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") = fmax(*(" + reduceVar + "), *(" + reduceVar + "+_step)); }\n";
                            reducePost += "*(_" + rName + "_reduce+gid) = *_" + rName + "_cache;\n";
                        }
                    
                        if (reduction == Reduction::Minimum) {
                            reduceVarPost += "*(" + reduceVar + ") = " + rName + ";\n";
                            reduceHead += "__local " + rType + " _" + rName + "_cache[" + std::to_string(TileMetrics::workgroupSize) + "];\n";
                            reduceHead += "*(" + reduceVar + ") = noData_" + rType + ";\n";
                            reduceCode += "if (isValid_" + rType + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") = fmin(*(" + reduceVar + "), *(" + reduceVar + "+_step)); }\n";
                            reducePost += "*(_" + rName + "_reduce+gid) = *_" + rName + "_cache;\n";
                        }
                    
                        if (reduction == Reduction::Sum || reduction == Reduction::SumSquares || reduction == Reduction::Mean) {
                            if (reduction == Reduction::SumSquares) {
                                reduceVarPost += "*(" + reduceVar + ") = isValid_" + rType + "(" + rName + ") ? " + rName + "*" + rName + " : 0.0;\n";
                            } else {
                                reduceVarPost += "*(" + reduceVar + ") = isValid_" + rType + "(" + rName + ") ? " + rName + " : 0.0;\n";
                            }
                            reduceHead += "__local " + rType + " _" + rName + "_cache[" + std::to_string(TileMetrics::workgroupSize) + "];\n";
                            reduceHead += "*(" + reduceVar + ") = noData_" + rType + ";\n";
                            reduceCode += "if (isValid_" + rType + "(*(" + reduceVar + "+_step))) { *(" + reduceVar + ") += *(" + reduceVar + "+_step); }\n";
                            reducePost += "*(_" + rName + "_reduce+gid) = *_" + rName + "_cache;\n";
                        }
                    
                        if (reduction == Reduction::Count || reduction == Reduction::Mean) {    
                            reduceVarPost += "if (isValid_" + rType + "(" + rName + ")) { atomic_inc(&_" + rName + "_count); } \n";                
                            reduceHead += "__local uint _" + rName + "_count; if (_lid == 0) { _" + rName + "_count = 0; } \n";
                            reducePost += "atomic_add(_" + rName + "_status, _" + rName + "_count);\n";                            
                        }

                        // Set flag to enable reduction code
                        needsReduction = true;
                    }

                    // Check whether status bits are required on raster
                    if (rasterBase.getNeedsStatus() || 
                        reduction == Reduction::Count ||
                        reduction == Reduction::Mean) {

                        // Update reduction parameter list
                        argsList += "__global uint *_" + rName + "_status,\n";
                    }

                    if (rasterBase.getNeedsWrite()) {

                        // Update script for writing aligned layers
                        if (is2D) {
                            post += "_val2D_a(_" + rName + ", _i, _j) = " + rName + ";\n";
                        } else if (rasterDim.d.nz == dimFirst.d.nz) {
                            post += "_val3D_a(_" + rName + ", _i, _j, _k) = " + rName + ";\n";
                        }
                    }

                } else {

                    // Check for operators
                    auto rGrad = std::regex("\\bgrad\\(\\s*" + rName + "\\s*\\)");
                    if (std::regex_search(script, rGrad)) {
                        script = std::regex_replace(script, rGrad, std::string("_grad(") + rName + ")");
                    }

                    // Update parameter list
                    argsList += "__global " + rType + " *_" + rName + ",\n";
                    argsList += (useConst ? "const Dimensions _dim_" : "Dimensions _dim_") + rName + ",\n";

                    // Update script for raster value                
                    if (rasterBase.getNeedsRead()) {
                        if (projFirst == rProj) {

                            // Use same projection
                            auto interpolation = rasterBase.getInterpolationType();
                            if (interpolation == RasterInterpolation::Nearest || rType == "UINT" || rType == "UCHAR") {
                            
                                // Nearest neighbour interpolation 
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = getNearestValue2D_" + rType + 
                                        "(_" + rName + ", x, y, _dim_" + rName + ");\n";
                                } else {
                                
                                    // Use square brackets for k-index
                                    if (!script.empty() && std::regex_search(script, std::regex("\\b" + rName + "\\s*\\["))) {

                                        script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                            std::string("getNearestValueAtZ_") + rType + "(_" + rName + ", x, y, min_uint_DEF(($1), " + 
                                            std::to_string(rasterDim.d.nz-1) + "), _dim_" + rName + ")");

                                        // Add range variable                        
                                        variableListHi += "const uint " + rName + "_layers = " + std::to_string(rasterDim.d.nz) +";\n";

                                    } else {

                                        // Use un-projected raster value
                                        variableListHi += rType + " " + rName + " = getNearestValue3D_" + rType + 
                                            "(_" + rName + ", x, y, z, _dim_" + rName + ");\n";
                                    }
                                }

                            } else if (interpolation == RasterInterpolation::Bilinear) {
                            
                                // Bilinear interpolation 
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = getBilinearValue2D_" + rType + 
                                        "(_" + rName + ", x, y, _dim_" + rName + ");\n";
                                } else {
                                
                                    // Use square brackets for k-index
                                    if (!script.empty() && std::regex_search(script, std::regex("\\b" + rName + "\\s*\\["))) {

                                        script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                            std::string("getBilinearValueAtZ_") + rType + "(_" + rName + ", x, y, min_uint_DEF(($1), " + 
                                            std::to_string(rasterDim.d.nz-1) + "), _dim_" + rName + ")");

                                        // Add range variable                        
                                        variableListHi += "const uint " + rName + "_layers = " + std::to_string(rasterDim.d.nz) +";\n";

                                    } else {

                                        // Use un-projected raster value
                                        variableListHi += rType + " " + rName + " = getBilinearValue3D_" + rType + 
                                            "(_" + rName + ", x, y, z, _dim_" + rName + ");\n";
                                    }
                                }

                            } else {

                                // Error
                                std::string err = std::string("Invalid interpolation rType '") + std::to_string(interpolation) + "'";
                                throw std::runtime_error(err);
                            }

                        } else {

                            // Add projection information
                            if (projFirst.type == 1)
                                kernelGenerator.projectionTypes.insert(projFirst.cttype);
                            if (rProj.type == 1)
                                kernelGenerator.projectionTypes.insert(rProj.cttype);

                            // Add coordinate and projection
                            argsList += (useConst ? "const ProjectionParameters _proj_" : "ProjectionParameters _proj_") + rName + ",\n";
                            variableListHi += "Coordinate c_" + rName + " = {x, y, z, 0.0};\n";
                                
                            // Add projection code
                            auto interpolation = rasterBase.getInterpolationType();
                            if (interpolation == RasterInterpolation::Nearest || rType == "UINT" || rType == "UCHAR") {
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = convert(&c_" + rName + ", &_proj_" + rName + ", &_proj) ? getNearestValue2D_" + rType + 
                                        "(_" + rName + ", c_" + rName + ".p, c_" + rName + ".q, _dim_" + rName + ") : noData_" + rType + ";\n";
                                } else {

                                    // Use square brackets for k-index
                                    if (!script.empty() && std::regex_search(script, std::regex("\\b" + rName + "\\s*\\["))) {
                                    
                                        variableListHi += "const bool c_" + rName + "_valid = convert(&c_" + rName + ", &_proj_" + rName + ", &_proj);\n";
                                        script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                            std::string("c_") + rName + "_valid ? getNearestValueAtZ_" + rType + "(_" + rName + ", c_" + rName + 
                                            ".p, c_" + rName + ".q, min_uint_DEF(($1), " + 
                                            std::to_string(rasterDim.d.nz-1) + "), _dim_" + rName + ") : noData_" + rType);

                                        // Add range variable
                                        variableListHi += "const uint " + rName + "_layers = " + std::to_string(rasterDim.d.nz) +";\n";

                                    } else {
                                    
                                        // Use projected raster value
                                        variableListHi += rType + " " + rName + " = convert(&c_" + rName + ", &_proj_" + rName + ", &_proj) ? getNearestValue3D_" + rType + 
                                            "(_" + rName + ", c_" + rName + ".p, c_" + rName + ".q, c_" + rName + ".r, _dim_" + rName + ") : noData_" + rType + ";\n";
                                    }
                                }
                    
                            } else if (interpolation == RasterInterpolation::Bilinear) {

                                // Bilinear interpolation
                                if (is2D) {
                                    variableListHi += rType + " " + rName + " = convert(&c_" + rName + ", &_proj_" + rName + ", &_proj) ? getBilinearValue2D_" + rType + 
                                        "(_" + rName + ", c_" + rName + ".p, c_" + rName + ".q, _dim_" + rName + ") : noData_" + rType + ";\n";
                                } else {

                                    // Use square brackets for k-index
                                    if (!script.empty() && std::regex_search(script, std::regex("\\b" + rName + "\\s*\\["))) {
                                    
                                        variableListHi += "const bool c_" + rName + "_valid = convert(&c_" + rName + ", &_proj_" + rName + ", &_proj);\n";
                                        script = std::regex_replace(script, std::regex("\\b" + rName + "\\s*\\[\\s*(.+?)\\s*\\]"), 
                                            std::string("c_") + rName + "_valid ? getBilinearValueAtZ_" + rType + "(_" + rName + 
                                            ", c_" + rName + ".p, c_" + rName + ".q, min_uint_DEF(($1), " + 
                                            std::to_string(rasterDim.d.nz-1) + "), _dim_" + rName + ") : noData_" + rType);

                                        // Add range variable
                                        variableListHi += "const uint " + rName + "_layers = " + std::to_string(rasterDim.d.nz) +";\n";

                                    } else {

                                        // Use projected raster value
                                        variableListHi += rType + " " + rName + " = convert(&c_" + rName + ", &_proj_" + rName + ", &_proj) ? getBilinearValue3D_" + rType + 
                                            "(_" + rName + ", c_" + rName + ".p, c_" + rName + ".q, c_" + rName + ".r, _dim_" + rName + ") : noData_" + rType + ";\n";
                                    }
                                }
                            } else {

                                // Error
                                std::string err = std::string("Invalid interpolation type '") + std::to_string(interpolation) + "'";
                                throw std::runtime_error(err);
                            }
                        }
                    } else {
                        variableListHi += rType + " " + rName + " = noData_" + rType + ";\n";
                    }
                }

                if (rasterNullValue != RasterNullValue::Null) {

                    // Update script count for non-null values
                    variableListHi += "if (isValid_" + rType + "(" + rName + ")) { __layer_count++; }\n";

                    // Update script processing for fill values
                    if (rasterNullValue == RasterNullValue::Zero)
                        variableListLow += "if (__layer_count > 0 && !isValid_" + rType + "(" + rName + ")) " + rName + " = ("+ rType +")0;\n";
                    else if (rasterNullValue == RasterNullValue::One)
                        variableListLow += "if (__layer_count > 0 && !isValid_" + rType + "(" + rName + ")) " + rName + " = ("+ rType +")1;\n";
                }

            }

            // Add neighbour memory boundary variable
            if (needsNeighbours) {
                std::string boundaryScript;
                boundaryScript += "const bool _boundary_W = (_i == 0);\n";
                boundaryScript += "const bool _boundary_S = (_j == 0);\n";
                boundaryScript += "const bool _boundary_E = (_i == _dim.mx-1);\n";
                boundaryScript += "const bool _boundary_N = (_j == _dim.my-1);\n";
                variableListHi = boundaryScript + variableListHi;
            }

            // Add variable for valid raster count
            if (rasterNullValue != RasterNullValue::Null) {
                variableListLow += "const uint _layer_count = __layer_count;";
            }
        
            // Add additional parameters
            if (!kernelGenerator.projectionTypes.empty()) {
                argsList += (useConst ? "const ProjectionParameters _proj,\n" : "ProjectionParameters _proj,\n");
            }
            argsList += (useConst ? "const Dimensions _dim,\n" : "Dimensions _dim,\n");

            // Add boundary flags
            if (needsBoundary) {
                std::string boundaryScript;
                boundaryScript += "const bool isBoundary_W = ((_i == 0) && (_isBoundaryTile&(uchar)0x01));\n";
                boundaryScript += "const bool isBoundary_S = ((_j == 0) && (_isBoundaryTile&(uchar)0x02));\n";
                boundaryScript += "const bool isBoundary_E = ((_i == _dim.nx-1) && (_isBoundaryTile&(uchar)0x04));\n";
                boundaryScript += "const bool isBoundary_N = ((_j == _dim.ny-1) && (_isBoundaryTile&(uchar)0x08));\n";
                variableListHi = variableListHi + boundaryScript;
                argsList += "uchar _isBoundaryTile,\n";
            }
        }

        // Remove trailing comma and newline
        argsList.pop_back();
        argsList.pop_back();
                    
        // Load kernel codes
        kernelGenerator.script = kernelBlock;
        
        // Check if cache is required
        if (cacheItems.size() != 0) {
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__CACHE__|__CACHE__\\*\\/"), std::string(""));

            std::string cacheHead;
            std::string cacheVars;
            for (auto &cacheItem : cacheItems) {
                
                cacheHead += "__local " + cacheItem.first + " _cache_" + cacheItem.first + "[" + std::to_string(cacheItem.second.first) + "];\n";
                cacheVars += cacheItem.second.second;
            }
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__CACHE_HEAD__"), cacheHead);
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__CACHE_VARS__"), cacheVars);
        }

        // Check if reduction is required
        if (needsReduction) {
            
            // Enable reduction code
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__REDUCE__|__REDUCE__\\*\\/"), std::string(""));
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__REDUCE_HEAD__"), reduceHead);
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__REDUCE_CODE__"), reduceCode);
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("__REDUCE_POST__"), reducePost);
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__REDUCE_VAR_POST__\\*\\/"), reduceVarPost);
        }

        // Patch kernel template with code
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__VARS__\\*\\/"), variableListHi + variableListLow);
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__POST__\\*\\/"), post);

        // Patch kernel template with script
        if (addTerminator && !script.empty()) {
            script = Solver::processScript(script);
        }
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\/\\*__CODE__\\*\\/"), script);

        // Find all variables in string
        std::smatch variableMatch;
        std::string::const_iterator searchStart(kernelGenerator.script.cbegin());
        std::set<std::string> replacementVariables;
        while (std::regex_search(searchStart, kernelGenerator.script.cend(), variableMatch, std::regex("\\w+:{2}\\w+"))) {

            // Check if variable is valid
            if (variableTable.find(variableMatch[0]) == variableTable.end()) {
                std::string err = std::string("Invalid variable name '") + variableMatch[0].str() + "'";
                throw std::runtime_error(err);
            }

            // Add to replacement set
            replacementVariables.insert(variableMatch[0]);

            // Update search position
            searchStart = variableMatch.suffix().first;
        }

        // Patch kernel script with variable names
        for (auto &replacement : replacementVariables) {
            kernelGenerator.script = std::regex_replace(kernelGenerator.script, 
                std::regex("\\b" + replacement + "\\b"), variableTable[replacement]);
        }

        // Replace spatial indexes
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\bipos\\b"), std::string("_i"));
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\bjpos\\b"), std::string("_j"));
        kernelGenerator.script = std::regex_replace(kernelGenerator.script, std::regex("\\bkpos\\b"), std::string("_k"));

        //// Output script
        //std::cout << kernelGenerator.script << std::endl; // TEST

        // Return processed script
        return kernelGenerator;
    }

    /**
    * Generates OpenCL kernel.
    * @param kernelGenerator processed OpenCL code block.
    * @return hash of program string
    */
    std::size_t buildRasterKernel(RasterKernelGenerator &kernelGenerator) {
        
        // Get solver handles
        auto &solver = Geostack::Solver::getSolver();

        // Create hash
        std::size_t clHash = solver.getNullHash();

        try {

            // Load kernel codes
            std::string kernelStr = Models::cl_raster_head_c;
            if (!kernelGenerator.rasterTypes.empty()) {
            
                // Update raster definitions
                for (auto rType : kernelGenerator.rasterTypes) {
                    std::string rasterDef = "#define USE_RASTER_TYPE_" + rType + "\n";
                    kernelStr = rasterDef + kernelStr;
                }
            }
            if (!kernelGenerator.projectionTypes.empty()) {
            
                // Update projection definitions
                auto projStr = Models::cl_projection_head_c;
                for (auto pid : kernelGenerator.projectionTypes) {
                    std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(pid) + "\n";
                    projStr = projDef + projStr;
                }
                kernelStr += projStr;
            }

            if (kernelGenerator.reqs.usesRandom) {
                kernelStr += Models::cl_random_c;
            }

            kernelStr += kernelGenerator.script;
                        
            // Build program
            clHash = solver.buildProgram(kernelStr);
            if (clHash == solver.getNullHash()) {

                // Output kernel code
                //std::cout << "ERROR: Cannot build program" << std::endl;
                //std::cout << kernelStr << std::endl;

                throw std::runtime_error("Cannot build program");
            }

            // Set generator hash
            kernelGenerator.programHash = clHash;

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        } catch (std::regex_error e) {
            std::stringstream err;
            err << "Regex exception '" << e.what();
            throw std::runtime_error(err.str());
        }
        return clHash;
    }
    
    /**
    * Sets kernel arguments for a %Raster %Tile based on generated script
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param kernel OpenCL kernel to run.
    * @param rasterBaseRefs list of input raster references.
    * @param startArg the index of the first argument to set.
    * @param variables pointer to a general %Variables object for the kernel.
    * @return true if kernel arguments are correctly set, false otherwise
    */
    template <typename CTYPE>
    bool setTileKernelArguments(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        RasterKernelRequirements reqs, 
        int startArg,
        VariablesBasePtr<std::string> variables,
        bool useDimensions) {

        // Check references against requirements
        if (rasterBaseRefs.size() != reqs.rasterRequirements.size()) {
            throw std::runtime_error("Kernel rasters and requirements are different lengths");
        }

        try {
            
            // Get OpenCL solver handles
            auto &solver = Geostack::Solver::getSolver();

            if (solver.openCLInitialised()) {

                // Populate variable argument
                int arg = startArg;
                if (variables != nullptr && variables->hasData()) {
                    kernel.setArg(arg++, variables->getBuffer());
                }

                if (rasterBaseRefs.size() > 0) {

                    // Get handle to anchor layer
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
                    auto rasterName = rasterBaseFirst.template getProperty<std::string>("name");
                    auto rc = rasterBaseFirst.hasProperty("randomSeed");
                    auto randomSeed = getNullValue<std::uint32_t>();
                    if (rc) {
                        randomSeed = rasterBaseFirst.template getProperty<std::uint32_t>("randomSeed");
                    }

                    auto dimFirst = rasterBaseFirst.getRasterDimensions();            
                    auto projFirst = rasterBaseFirst.getProjectionParameters();

                    // Add random buffer
                    if (reqs.usesRandom) {
                        kernel.setArg(arg++, rasterBaseFirst.getRandomBuffer());
                    }

                    // Get tile dimensions
                    TileDimensions<CTYPE> tDimFirst = rasterBaseFirst.getTileDimensions(ti, tj);

                    // Populate kernel arguments
                    bool needsProjection = false;
                    for (int i = 0; i < rasterBaseRefs.size(); i++) {

                        // Skip unused Rasters
                        if (!reqs.rasterRequirements[i].used) {
                            continue;
                        }

                        // Get Raster handle
                        auto &rasterBase = rasterBaseRefs[i].get();
                        
                        // Reset variables for reduction operations
                        auto reduction = rasterBase.getReductionType();
                        if (reduction != Reduction::None) {
                            rasterBase.resetTileReduction(ti, tj);
                            if (reduction == Reduction::Count || reduction == Reduction::Mean)
                                rasterBase.setTileStatus(ti, tj, 0);
                        }

                        // Update variable list
                        if (rasterBase.hasVariables()) {
                            kernel.setArg(arg++, rasterBase.getVariablesBuffer());
                        }
                    
                        // Check if raster has equal dimensions to this raster
                        if (equalSpatialMetrics2D(rasterBase.getRasterDimensions().d, dimFirst.d)) {

                            // Get required neighbours
                            auto requiredNeighbours = reqs.rasterRequirements[i].requiredNeighbours;

                            // Add buffer and dimensions to kernel
                            kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti, tDimFirst.tj));

                            // Check and add neighbours
                            if (requiredNeighbours & Neighbours::N) {
                                if (tDimFirst.tj < dimFirst.ty-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti, tDimFirst.tj+1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }
                            
                            if (requiredNeighbours & Neighbours::NE) {
                                if (tDimFirst.ti < dimFirst.tx-1 && tDimFirst.tj < dimFirst.ty-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti+1, tDimFirst.tj+1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::E) {
                                if (tDimFirst.ti < dimFirst.tx-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti+1, tDimFirst.tj));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }
                            
                            if (requiredNeighbours & Neighbours::SE) {
                                if (tDimFirst.ti < dimFirst.tx-1 && tDimFirst.tj > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti+1, tDimFirst.tj-1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::S) {
                                if (tDimFirst.tj > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti, tDimFirst.tj-1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::SW) {
                                if (tDimFirst.ti > 0 && tDimFirst.tj > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti-1, tDimFirst.tj-1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::W) {
                                if (tDimFirst.ti > 0) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti-1, tDimFirst.tj));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            if (requiredNeighbours & Neighbours::NW) {
                                if (tDimFirst.ti > 0 && tDimFirst.tj < dimFirst.ty-1) {
                                    kernel.setArg(arg++, rasterBase.getTileDataBuffer(tDimFirst.ti-1, tDimFirst.tj+1));
                                } else {
                                    kernel.setArg(arg++, rasterBase.getNullBuffer());
                                }
                            }

                            // Check whether reductions are required on raster
                            if (reduction != Reduction::None) {
                        
                                // Add reduction buffer
                                kernel.setArg(arg++, rasterBase.getTileReduceBuffer(tDimFirst.ti, tDimFirst.tj));
                            }

                            // Check whether status bits are required on raster
                            if (rasterBase.getNeedsStatus() || 
                                reduction == Reduction::Count ||
                                reduction == Reduction::Mean) {

                                // Add status buffer
                                kernel.setArg(arg++, rasterBase.getTileStatusBuffer(tDimFirst.ti, tDimFirst.tj));
                            }

                        } else {

                            // Get raster projection
                            auto rasterProj = rasterBase.getProjectionParameters();
                            bool differentProjections = (projFirst != rasterProj);

                            // Get bounds and reproject if necessary
                            auto tileBounds = rasterBaseFirst.getTileBounds(ti, tj);
                            if (differentProjections) {

                                // Reproject bounds
                                tileBounds = tileBounds.convert(rasterProj, projFirst);
                                needsProjection = true;
                            }
                        
                            // Get raster region intersecting current tile
                            RasterDimensions<CTYPE> rasterDim;
                            cl::Buffer kernelBuffer;
                            if (rasterBase.createRegionBuffer(tileBounds, kernelBuffer, rasterDim)) {
                        
                                // Add buffer and dimensions to kernel
                                kernel.setArg(arg++, kernelBuffer);
                                kernel.setArg(arg++, rasterDim.d);
                                if (differentProjections)
                                    kernel.setArg(arg++, ProjectionParameters<CTYPE>(rasterProj));

                            } else {

                                // Add null buffer and dimensions to kernel, this will be detected in the kernel
                                kernel.setArg(arg++, rasterBase.getNullBuffer());
                                kernel.setArg(arg++, Dimensions<CTYPE>());
                                if (differentProjections)
                                    kernel.setArg(arg++, ProjectionParameters<CTYPE>()); // TODO
                            }
                        }
                    }
                    
                    if (needsProjection)
                        kernel.setArg(arg++, ProjectionParameters<CTYPE>(projFirst));
                    if (useDimensions)
                        kernel.setArg(arg++, tDimFirst.d);

                    // Add boundary flag
                    if (reqs.usesBoundary) {
                        kernel.setArg(arg++, (cl_uchar)(
                            (cl_uchar)(tDimFirst.ti == 0) | 
                            ((cl_uchar)(tDimFirst.tj == 0)<<1) | 
                            ((cl_uchar)(tDimFirst.ti == dimFirst.tx-1)<<2) |
                            ((cl_uchar)(tDimFirst.tj == dimFirst.ty-1)<<3)));
                    }

                    return true;
                }

                // Return false if rasterBaseRefs is empty
                return false;
            }
        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
        return false;
    }

    /**
    * Runs a kernel on the %Tile at index ti and tj in the first %Raster 
    * @param ti the x %Tile index.
    * @param tj the y %Tile index.
    * @param kernel OpenCL kernel to run.
    * @param rasterBaseRefs list of input raster references.
    * @param startArg the index of the first argument to set.
    * @param variables pointer to a general %Variables object for the kernel.
    * @return true if kernel executes, false otherwise.
    */
    template <typename CTYPE>
    bool runTileKernel(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs,
        RasterKernelRequirements reqs,
        int startArg,
        VariablesBasePtr<std::string> variables,
        size_t debug) {

        // Check references against requirements
        if (rasterBaseRefs.size() != reqs.rasterRequirements.size()) {
            throw std::runtime_error("Kernel rasters and requirements are different lengths");
        }
    
        try {
            
            // Get OpenCL solver handles
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            uint8_t verbose = solver.getVerboseLevel();

            if (solver.openCLInitialised()) {

                if (rasterBaseRefs.size() > 0) {

                    // Set arguments
                    if (!setTileKernelArguments<CTYPE>(ti, tj, kernel, rasterBaseRefs, reqs, startArg, variables))
                        return false;

                    // Get handle to anchor layer
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();

                    // Get tile dimensions
                    TileDimensions<CTYPE> tDimFirst = rasterBaseFirst.getTileDimensions(ti, tj);

                    // Execute kernel
                    if (debug == RasterDebug::Enable) {

                        // Run over 1 cell only
                        queue.enqueueNDRangeKernel(kernel, 
                            cl::NullRange, 
                            cl::NDRange(1, 1, tDimFirst.d.nz), 
                            cl::NDRange(1, 1, 1));
                    } else {

                        // Run over range
                        queue.enqueueNDRangeKernel(kernel, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, tDimFirst.d.ny, tDimFirst.d.nz), 
                            cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                    }

                    // Flush queue
                    //queue.flush();

                    return true;
                }

                // Return false if rasterBaseRefs is empty
                return false;
            }
        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
        return false;
    }

    /**
    * Run script on %Raster
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    */
    template <typename CTYPE>
    void runScriptNoOut(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters) {

        // Check for rasters
        if (rasterBaseRefs.size() == 0) {
            throw std::runtime_error("No rasters for script");
        }
            
        // Get OpenCL handles
        auto &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Get paramaters
            RasterDebug::Type rasterDebug = static_cast<RasterDebug::Type>(parameters & 0x3000);
            RasterSort::Type rasterSort = static_cast<RasterSort::Type>(parameters & 0xC000);

            // Get anchor raster
            auto &rasterFirst = rasterBaseRefs[0].get();
            auto dimFirst = rasterFirst.getRasterDimensions();

            // Get generator
            auto scriptHash = createScriptHash(script, Models::cl_raster_block_c, rasterBaseRefs);
            auto kernelGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (kernelGenerator.programHash == Solver::getNullHash()) {

                // Check for underscores in script, these are reserved for internal variables
                if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                    throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                }

                // Check if script has an output variable
                if (std::regex_search(script, std::regex("\\boutput\\b"))) {  
                    throw std::runtime_error("Script cannot contain 'output' definition");   
                }

                // Generate kernel
                RasterNullValue::Type rasterNullValue = static_cast<RasterNullValue::Type>(parameters & 0xC0);
                kernelGenerator = generateKernel<CTYPE>(script, Models::cl_raster_block_c, 
                    rasterBaseRefs, rasterNullValue, nullptr, rasterDebug != RasterDebug::Enable);
                    
                // Check script
                if (kernelGenerator.script.size() == 0) {
                    throw std::runtime_error("Cannot generate script");
                }

                // Build script
                if (buildRasterKernel(kernelGenerator) == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
            }

            // Set sort
            cl::Kernel sortKernel;
            RasterKernelGenerator sortGenerator;
            RasterBaseRefs<CTYPE> sortRefs;
            if (rasterSort == RasterSort::PreScript || rasterSort == RasterSort::PostScript) {
            
                // Add output to sort references
                sortRefs.push_back(rasterFirst);

                // Get generator
                auto scriptHash = createScriptHash("", Models::cl_sort_c, sortRefs);
                sortGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
                if (sortGenerator.programHash == Solver::getNullHash()) {

                    // Generate kernel script
                    sortGenerator.script = Models::cl_sort_c; //Geostack::generateKernel<CTYPE>("", Models::cl_sort_c, sortRefs);
                    sortGenerator.reqs.rasterRequirements.push_back(RasterKernelRequirement(0));
                    // if (sortGenerator.script.size() == 0) {
                    //     throw std::runtime_error("Cannot generate sort script");
                    // }

                    // // Force first Raster to be used
                    // sortGenerator.reqs.rasterRequirements[0].used = true;

                    // Build raster kernel
                    if (buildRasterKernel(sortGenerator) == solver.getNullHash()) {
                        throw std::runtime_error("Cannot get kernel for script");
                    }

                    // Register kernel in cache
                    RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, sortGenerator);
                }
                
                // Select sort kernel based on data type
                sortKernel = solver.getKernel(sortGenerator.programHash, "sort_" + rasterFirst.getOpenCLTypeString()); 
            }

            // Get kernel
            auto &rasterKernel = solver.getKernel(kernelGenerator.programHash, "raster"); 

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            auto maxTileSize = getMaxTileDataSize(rasterBaseRefs);
            if (verbose <= Geostack::Verbosity::Info) {
                std::cout << "Maximum tile size = " << std::setprecision(6) << maxTileSize << " Gb" << std::endl;
            }

            if (rasterDebug == RasterDebug::Enable) {

                // Run over single tile only
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "Debug mode" << std::endl;
                }

                // Apply pre-script sort
                if (rasterSort == RasterSort::PreScript) {

                    // Set arguments
                    // setTileKernelArguments<CTYPE>(0, 0, sortKernel, sortRefs, sortGenerator.reqs);
                    sortKernel.setArg(0, rasterFirst.getTileDataBuffer(0, 0));
                    sortKernel.setArg(1, rasterFirst.getTileDimensions(0, 0).d);
                    
                    // Execute kernel
                    queue.enqueueNDRangeKernel(sortKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, dimFirst.d.ny, 1), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                }

                // Run kernel
                runTileKernel<CTYPE>(0, 0, rasterKernel, rasterBaseRefs, kernelGenerator.reqs, 0, nullptr, rasterDebug);

                // Apply post-script sort
                if (rasterSort == RasterSort::PostScript) {

                    // Set arguments
                    // setTileKernelArguments<CTYPE>(0, 0, sortKernel, sortRefs, sortGenerator.reqs);
                    sortKernel.setArg(0, rasterFirst.getTileDataBuffer(0, 0));
                    sortKernel.setArg(1, rasterFirst.getTileDimensions(0, 0).d);

                    // Execute kernel
                    queue.enqueueNDRangeKernel(sortKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, dimFirst.d.ny, 1), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                }

            } else {
            
                // Order processing so any tiles on device are run first
                std::list<uint64_t> tileIDs;
                for (uint32_t tj = 0; tj < dimFirst.ty; tj++) {
                    for (uint32_t ti = 0; ti < dimFirst.tx; ti++) {
                        if (rasterFirst.tileDataOnDevice(ti, tj)) {
                            tileIDs.push_front((uint64_t)ti | ((uint64_t)tj << 32));
                        } else {
                            tileIDs.push_back((uint64_t)ti | ((uint64_t)tj << 32));
                        }
                    }
                }

                // Run over all tiles
                std::size_t nVerbose = 0;
                std::size_t nVerboseCount = 0;

                for (auto tid : tileIDs) {

                    uint32_t ti = tid&0xFFFFFFFF;
                    uint32_t tj = tid>>32;

                    // Apply pre-script sort
                    if (rasterSort == RasterSort::PreScript) {

                        // Set arguments
                        // setTileKernelArguments<CTYPE>(ti, tj, sortKernel, sortRefs, sortGenerator.reqs);
                        sortKernel.setArg(0, rasterFirst.getTileDataBuffer(ti, tj));
                        sortKernel.setArg(1, rasterFirst.getTileDimensions(ti, tj).d);
                        
                        // Execute kernel
                        queue.enqueueNDRangeKernel(sortKernel, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, dimFirst.d.ny, 1), 
                            cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                    }
                    
                    // Run kernel
                    runTileKernel<CTYPE>(ti, tj, rasterKernel, rasterBaseRefs, kernelGenerator.reqs, 0, nullptr, rasterDebug);
                    
                    // Apply post-script sort
                    if (rasterSort == RasterSort::PostScript) {

                        // Set arguments
                        // setTileKernelArguments<CTYPE>(ti, tj, sortKernel, sortRefs, sortGenerator.reqs);
                        sortKernel.setArg(0, rasterFirst.getTileDataBuffer(ti, tj));
                        sortKernel.setArg(1, rasterFirst.getTileDimensions(ti, tj).d);

                        // Execute kernel
                        queue.enqueueNDRangeKernel(sortKernel, 
                            cl::NullRange, 
                            cl::NDRange(TileMetrics::tileSize, dimFirst.d.ny, 1), 
                            cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                    }

                    // Output progress
                    if (verbose <= Geostack::Verbosity::Info) {
                        if (nVerbose == 0) {
                            nVerbose = tileIDs.size()/10;
                            std::cout << std::setprecision(2) << 100.0*(double)nVerboseCount/(double)tileIDs.size() << "%" << std::endl;
                        } else {
                            nVerbose--;
                        }
                        nVerboseCount++;
                    }
                }
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "100%" << std::endl;
                }
            }

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }
    }

    /**
    * Run script on %Raster
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    * @return output %Raster
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runScript(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters) {

        // Check raster list is populated
        if (rasterBaseRefs.size() == 0) {
            throw std::runtime_error("Scripts must use one or more Rasters");
        }

        // Get OpenCL handles
        auto &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        uint8_t verbosity = solver.getVerboseLevel();

        if (solver.openCLInitialised()) {

            // Run simplified script if there is no output specified
            if (!std::regex_search(script, std::regex("\\boutput\\b"))) {
                runScriptNoOut<CTYPE>(script, rasterBaseRefs, parameters);
                return Raster<RTYPE, CTYPE>();
            }

            // Get parameters
            RasterCombination::Type rasterCombination = static_cast<RasterCombination::Type>(parameters & 0x03);
            RasterResolution::Type rasterResolution = static_cast<RasterResolution::Type>(parameters & 0x0C);
            RasterNullValue::Type rasterNullValue = static_cast<RasterNullValue::Type>(parameters & 0xC0);
            Reduction::Type rasterOutputReduction = static_cast<Reduction::Type>(parameters & 0xF00);
            RasterDebug::Type rasterDebug = static_cast<RasterDebug::Type>(parameters & 0x3000);
            RasterSort::Type rasterSort = static_cast<RasterSort::Type>(parameters & 0xC000);

            // Initialise bounds of output
            RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
            auto dimFirst = rasterBaseFirst.getRasterDimensions();
            auto projFirst = rasterBaseFirst.getProjectionParameters();
            uint32_t nx = dimFirst.d.nx;
            uint32_t ny = dimFirst.d.ny;
            uint32_t nz = dimFirst.d.nz;
            CTYPE hx = dimFirst.d.hx;
            CTYPE hy = dimFirst.d.hy;
            CTYPE hz = dimFirst.d.hz;
            CTYPE xMin = dimFirst.d.ox;
            CTYPE yMin = dimFirst.d.oy;
            CTYPE zMin = dimFirst.d.oz;
            CTYPE xMax = dimFirst.ex;
            CTYPE yMax = dimFirst.ey;
            CTYPE zMax = dimFirst.ez;

            // Check rasters all have a projection, or none have a projection
            bool hasProjection = projFirst.type != 0;

            // Check projection type
            if (hasProjection && !Projection::checkProjection(projFirst)) {
                throw std::runtime_error("Invalid projection type");
            }
                    
            // Parse rasters
            for (int i = 1; i < rasterBaseRefs.size(); i++) {     
                    
                // Get raster handle
                RasterBase<CTYPE> &rasterBase = rasterBaseRefs[i].get();
                auto dim = rasterBase.getRasterDimensions();
                auto proj = rasterBase.getProjectionParameters();

                // Check raster
                if (!rasterBase.hasData()) {
                    std::stringstream err;
                    err << "Raster index '" << i << "' in script has no data";
                    throw std::runtime_error(err.str());
                }

                // Check projection
                if (proj.type == 0 && hasProjection) {
                    throw std::runtime_error("One or more raster projections is undefined");
                }

                // Check projection type
                if (hasProjection && !Projection::checkProjection(proj)) {
                    throw std::runtime_error("Invalid projection type");
                }

                // Get bounds
                auto bounds = rasterBase.getBounds();
                auto rhx = dim.d.hx;
                auto rhy = dim.d.hy;
                auto rhz = dim.d.hz;
                if (projFirst != proj) {
                
                    // Convert distance
                    auto centroid = bounds.centroid();
                    auto offsetCentroid = centroid;
                    offsetCentroid.p += rhx;
                    offsetCentroid.q += rhy;
                    Projection::convert(centroid, projFirst, proj);
                    Projection::convert(offsetCentroid, projFirst, proj);
                    auto offset = centroid-offsetCentroid;
                    CTYPE rh = 0.5*(fabs(offset.p)+fabs(offset.q));
                    rhx = rh;
                    rhy = rh;

                    // Re-project and process bounds
                    bounds = bounds.convert(projFirst, proj);
                }

                // Process resolution
                hx = (rasterResolution == RasterResolution::Maximum) ? std::max(hx, rhx) : std::min(hx, rhx);
                hy = (rasterResolution == RasterResolution::Maximum) ? std::max(hy, rhy) : std::min(hy, rhy);
                hz = (rasterResolution == RasterResolution::Maximum) ? std::max(hz, rhz) : std::min(hz, rhz);

                // Process bounds
                xMin = (rasterCombination == RasterCombination::Union) ? std::min(xMin, bounds.min.p) : std::max(xMin, bounds.min.p);
                yMin = (rasterCombination == RasterCombination::Union) ? std::min(yMin, bounds.min.q) : std::max(yMin, bounds.min.q);
                zMin = (rasterCombination == RasterCombination::Union) ? std::min(zMin, bounds.min.r) : std::max(zMin, bounds.min.r);
                xMax = (rasterCombination == RasterCombination::Union) ? std::max(xMax, bounds.max.p) : std::min(xMax, bounds.max.p);
                yMax = (rasterCombination == RasterCombination::Union) ? std::max(yMax, bounds.max.q) : std::min(yMax, bounds.max.q);
                zMax = (rasterCombination == RasterCombination::Union) ? std::max(zMax, bounds.max.r) : std::min(zMax, bounds.max.r);
            }

            // Check difference from base dimensions
            if (xMin != dimFirst.d.ox || yMin != dimFirst.d.oy || zMin != dimFirst.d.oz || 
                xMax != dimFirst.ex || yMax != dimFirst.ey || zMax != dimFirst.ez || 
                hx != dimFirst.d.hx || hy != dimFirst.d.hy || hz != dimFirst.d.hz) {

                // Calculate extent
                CTYPE lx = xMax-xMin;
                CTYPE ly = yMax-yMin;
                CTYPE lz = zMax-zMin;
                if (lx <= 0.0 || ly <= 0.0 || lz <= 0.0) {
                    throw std::runtime_error("Output raster size from script is zero");
                }

                // Calculate number of cells
                nx = (uint32_t)std::ceil(lx/hx);
                ny = (uint32_t)std::ceil(ly/hy);
                nz = (uint32_t)std::ceil(lz/hz);
            }

            // Create output
            Raster<RTYPE, CTYPE> rasterOut;
            rasterOut.init(nx, ny, nz, hx, hy, hz, xMin, yMin, zMin);
            rasterOut.template setProperty<std::string>("name", "output");
            rasterOut.setProjectionParameters(projFirst);
            rasterOut.setReductionType(rasterOutputReduction);

            // The output must be the anchor layer at the front of the vector
            RasterBaseRefs<CTYPE> rasterBaseRefsWithOutput;
            rasterBaseRefsWithOutput.push_back(rasterOut);
            rasterBaseRefsWithOutput.insert(rasterBaseRefsWithOutput.end(), rasterBaseRefs.begin(), rasterBaseRefs.end());

            // Get generator
            auto scriptHash = createScriptHash(script, Models::cl_raster_block_c, rasterBaseRefsWithOutput);
            auto kernelGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (kernelGenerator.programHash == Solver::getNullHash()) {

                // Check for underscores in script, these are reserved for internal variables
                if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                    throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                }

                // Generate kernel
                kernelGenerator = generateKernel<CTYPE>(
                    script, Models::cl_raster_block_c, rasterBaseRefsWithOutput, 
                        rasterNullValue, nullptr, rasterDebug != RasterDebug::Enable);

                // Check script
                if (kernelGenerator.script.size() == 0) {
                    throw std::runtime_error("Cannot generate script");
                }

                // Build kernel
                if (buildRasterKernel(kernelGenerator) == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
            }

            // Set sort
            cl::Kernel sortKernel;
            RasterKernelGenerator sortGenerator;
            RasterBaseRefs<CTYPE> sortRefs;
            if (rasterSort == RasterSort::PostScript) {
            
                // Add output to sort references
                sortRefs.push_back(rasterOut);

                // Get generator
                auto scriptHash = createScriptHash("", Models::cl_sort_c, sortRefs);
                sortGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
                if (sortGenerator.programHash == Solver::getNullHash()) {

                    // Generate kernel script
                    // sortGenerator = Geostack::generateKernel<CTYPE>("", Models::cl_sort_c, sortRefs);
                    sortGenerator.script = Models::cl_sort_c;
                    sortGenerator.reqs.rasterRequirements.push_back(RasterKernelRequirement(0));
                    // if (sortGenerator.script.size() == 0) {
                    //     throw std::runtime_error("Cannot generate sort script");
                    // }

                    // Force first Raster to be used
                    // sortGenerator.reqs.rasterRequirements[0].used = true;

                    // Build raster kernel
                    if (buildRasterKernel(sortGenerator) == solver.getNullHash()) {
                        throw std::runtime_error("Cannot get kernel for script");
                    }

                    // Register kernel in cache
                    RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, sortGenerator);
                }
                
                // Select sort kernel based on data type
                sortKernel = solver.getKernel(sortGenerator.programHash, "sort_" + rasterOut.getOpenCLTypeString()); 

            } else if (rasterSort == RasterSort::PreScript) {

                // Error for runScript for pre-sort
                throw std::runtime_error("Pre-script sort invalid for scripts with output");
            }

            // Get kernel
            cl::Kernel &rasterKernel = solver.getKernel(kernelGenerator.programHash, "raster");

            auto maxTileSize = getMaxTileDataSize(rasterBaseRefs);
            if (verbosity <= Geostack::Verbosity::Info) {
                std::cout << "Maximum tile size = " << std::setprecision(6) << maxTileSize << " Gb" << std::endl;
            }

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            auto dimOut = rasterOut.getRasterDimensions();
            if (rasterDebug == RasterDebug::Enable) {

                // Run over single tile only
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "Debug mode" << std::endl;
                }
                runTileKernel<CTYPE>(0, 0, rasterKernel, rasterBaseRefsWithOutput, kernelGenerator.reqs, 0, nullptr, rasterDebug);

                // Apply post-script sort
                if (rasterSort == RasterSort::PostScript) {

                    // Set arguments
                    // setTileKernelArguments<CTYPE>(0, 0, sortKernel, sortRefs, sortGenerator.reqs);
                    sortKernel.setArg(0, rasterOut.getTileDataBuffer(0, 0));
                    sortKernel.setArg(1, rasterOut.getTileDimensions(0, 0).d);

                    // Execute kernel
                    queue.enqueueNDRangeKernel(sortKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, ny, 1), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                }

            } else {

                // Run over all tiles
                for (uint32_t tj = 0; tj < dimOut.ty; tj++) {
                    for (uint32_t ti = 0; ti < dimOut.tx; ti++) {

                        // Run kernel
                        runTileKernel<CTYPE>(ti, tj, rasterKernel, rasterBaseRefsWithOutput, kernelGenerator.reqs, 0, nullptr, rasterDebug);

                        // Apply post-script sort
                        if (rasterSort == RasterSort::PostScript) {

                            // Set arguments
                            // setTileKernelArguments<CTYPE>(ti, tj, sortKernel, sortRefs, sortGenerator.reqs);
                            sortKernel.setArg(0, rasterOut.getTileDataBuffer(ti, tj));
                            sortKernel.setArg(1, rasterOut.getTileDimensions(ti, tj).d);

                            // Execute kernel
                            queue.enqueueNDRangeKernel(sortKernel, 
                                cl::NullRange, 
                                cl::NDRange(TileMetrics::tileSize, ny, 1), 
                                cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                        }
                    }

                    // Output progress
                    if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                        std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dimOut.ty << "%" << std::endl;
                    }
                }
                if (verbose <= Geostack::Verbosity::Info) {
                    std::cout << "100%" << std::endl;
                }
            }

            // Return raster
            return rasterOut;

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }

        // Return empty raster
        return Raster<RTYPE, CTYPE>();
    }
    
    /**
    * Runs a script on the raster, returning a resulting raster.
    * @param script OpenCL script to run.
    * @param rasterBaseRef input raster.
    * @param parameters parameters for sampling and output raster.
    */
    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runAreaScript(
        std::string script, 
        RasterBase<CTYPE> &rasterBase, 
        cl_int width) {

        // Check for underscores in script, these are reserved for internal variables
        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
            throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
        }

        // Parse script
        script = Solver::processScript(script);

        // Get OpenCL solver handles
        auto &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Get raster properties
            std::string name = rasterBase.template getProperty<std::string>("name");
            auto dim = rasterBase.getRasterDimensions();
            auto proj = rasterBase.getProjectionParameters();

            // Create output
            Raster<RTYPE, CTYPE> rasterOut;
            rasterOut.init(dim.d);
            rasterOut.template setProperty<std::string>("name", "output");
            rasterOut.setProjectionParameters(proj);
            rasterOut.setNeedsWrite(false);       
            
            // Replace variable name in script
            std::string kernelBlock = Models::cl_raster_area_op2D_c;
            kernelBlock = std::regex_replace(kernelBlock, std::regex("__VARIABLE__"), name);
            
            // Store and set raster properties
            bool rasterBaseNeedsWrite = rasterBase.getNeedsWrite();
            uint8_t rasterBaseNeighbours = rasterBase.getRequiredNeighbours();
            rasterBase.setNeedsWrite(false);
            rasterBase.setRequiredNeighbours(Neighbours::Queen);
            
            // Generate kernel
            RasterBaseRefs<CTYPE> rasterBaseRefs;            
            rasterBaseRefs.push_back(rasterBase);
            rasterBaseRefs.push_back(rasterOut);

            // Get generator
            auto scriptHash = createScriptHash(script, kernelBlock, rasterBaseRefs);
            auto kernelGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (kernelGenerator.programHash == Solver::getNullHash()) {

                // Generate kernel
                kernelGenerator = generateKernel<CTYPE>(
                    script, kernelBlock, rasterBaseRefs, RasterNullValue::Null);
                
                // Check script
                if (kernelGenerator.script.size() == 0) {
                    throw std::runtime_error("Cannot generate script");
                }
                
                // Build kernel
                if (buildRasterKernel(kernelGenerator) == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
            }

            // Get kernel
            cl::Kernel &rasterKernel = solver.getKernel(kernelGenerator.programHash, "rasterArea2D");        

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            auto dimOut = rasterOut.getRasterDimensions();
            for (uint32_t tj = 0; tj < dimOut.ty; tj++) {
                for (uint32_t ti = 0; ti < dimOut.tx; ti++) {

                    // Run kernel
                    rasterKernel.setArg(0, width);
                    runTileKernel<CTYPE>(ti, tj, rasterKernel, rasterBaseRefs, kernelGenerator.reqs, 1);
                }

                // Output progress
                if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                    std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dimOut.ty << "%" << std::endl;
                }
            }
            if (verbose <= Geostack::Verbosity::Info) {
                std::cout << "100%" << std::endl;
            }

            // Preserve original properties
            rasterOut.setNeedsWrite(true);
            rasterBase.setNeedsWrite(rasterBaseNeedsWrite);
            rasterBase.setRequiredNeighbours(rasterBaseNeighbours);

            return rasterOut;

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }

        // Return empty raster
        return Raster<RTYPE, CTYPE>();
    }
    
    /**
    * Permute vector using vector of permutation indexes
    * Adapted from: https://stackoverflow.com/questions/17074324/how-can-i-sort-two-vectors-in-the-same-way-with-criteria-that-uses-only-one-of
    * @param v Vector to permute
    * @param p Vector of permutation indices
    */
    template <typename T>
    void PermutateVector(std::vector<T> &v, const std::vector<std::size_t> &p) {
        std::vector<bool> done(v.size());
        for (std::size_t i = 0; i < v.size(); i++) {
            if (done[i]) {
                continue;
            }
            done[i] = true;
            auto prev_j = i;
            auto j = p[i];
            while (i != j) {
                std::swap(v[prev_j], v[j]);
                done[j] = true;
                prev_j = j;
                j = p[j];
            }
        }
    }
    
    /**
    * Stipple %Raster data, creating a set of new Points
    * @return %Vector of %Raster.
    */
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> stipple(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        std::vector<std::string> fields, 
        uint32_t nPerCell) {

        // Create vector
        Vector<CTYPE> v;
        
        try {

            // Get OpenCL handles
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            uint8_t verbosity = solver.getVerboseLevel();
            
            size_t clWorkgroupSize, clPaddedWorkgroupSize;
            if (solver.openCLInitialised()) {

                // Check number of points per cell
                if (nPerCell == 0) {
                    throw std::runtime_error("Number of points per cell must be greater than zero");
                }

                if (rasterBaseRefs.size() > 0) {

                    auto maxTileSize = getMaxTileDataSize(rasterBaseRefs);
                    if (verbosity <= Geostack::Verbosity::Info) {
                        std::cout << "Maximum tile size = " << std::setprecision(6) << maxTileSize << " Gb" << std::endl;
                    }

                    // Get dimensions
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
                    auto dimFirst = rasterBaseFirst.getRasterDimensions();
                    auto randomSeed = getNullValue<std::uint32_t>();
                    auto rc = rasterBaseFirst.hasProperty("randomSeed");
                    if (rc) {
                        randomSeed = rasterBaseFirst.template getProperty<std::uint32_t>("randomSeed");
                    }

                    // Get generator
                    auto scriptHash = createScriptHash(script, Models::cl_stipple_c, rasterBaseRefs);
                    auto stippleGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
                    if (stippleGenerator.programHash == Solver::getNullHash()) {
            
                        // Check for underscores in script, these are reserved for internal variables
                        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                            throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                        }

                        // Disable raster writes
                        std::vector<bool> needsWrite;
                        for (auto &r : rasterBaseRefs) {
                            needsWrite.push_back(r.get().getNeedsWrite());
                        }

                        // Generate raster kernel script
                        stippleGenerator = Geostack::generateKernel<CTYPE>(script, Models::cl_stipple_c, rasterBaseRefs);
                        if (stippleGenerator.script.size() == 0) {
                            throw std::runtime_error("Cannot generate script");
                        }

                        // Restore raster writes
                        for (std::size_t i = 0; i < rasterBaseRefs.size(); i++) {
                            rasterBaseRefs[i].get().setNeedsWrite(needsWrite[i]);
                        }

                        // Create fields
                        stippleGenerator.fieldNames.clear();
                        std::set<std::string> fieldsCheck;
                        for (auto &fieldName : fields) {

                            if (std::regex_search(script, std::regex("\\b" + fieldName + "\\b"))) {
                    
                                // Check for duplicate names
                                if (fieldsCheck.find(fieldName) != fieldsCheck.end()) {
                                    throw std::runtime_error("Field name '" + fieldName + "' is not unique");
                                }
                                fieldsCheck.insert(fieldName);
                                stippleGenerator.fieldNames.push_back(fieldName);
                            }
                        }

                        // Add fields
                        std::string fieldArgsList;
                        std::string variableList;
                        std::string post;
                        for (auto &fieldName : stippleGenerator.fieldNames) {

                            // Add to argument list
                            fieldArgsList += ",\n__global REAL *_" + fieldName;
                            variableList += "REAL " + fieldName + " = noData_REAL;\n";
                            post += "_" + fieldName + "[_idx] = " + fieldName + ";\n";
                        }

                        // Patch raster kernel template with additional code
                        fieldArgsList += ",";
                        stippleGenerator.script = std::regex_replace(stippleGenerator.script, std::regex("\\/\\*__ARGS2__\\*\\/"), fieldArgsList);
                        stippleGenerator.script = std::regex_replace(stippleGenerator.script, std::regex("\\/\\*__VARS2__\\*\\/"), variableList);
                        stippleGenerator.script = std::regex_replace(stippleGenerator.script, std::regex("\\/\\*__POST2__\\*\\/"), post);

                        // Build raster kernel
                        if (buildRasterKernel(stippleGenerator) == solver.getNullHash()) {
                            throw std::runtime_error("Cannot get kernel for script");
                        }

                        // Register kernel in cache
                        RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, stippleGenerator);
                    }
                    auto &stippleKernel = solver.getKernel(stippleGenerator.programHash, "stipple"); 
                    auto &stippleCountKernel = solver.getKernel(stippleGenerator.programHash, "stipple_count"); 
                    
                    // Order processing so any tiles on device are run first
                    std::list<uint64_t> tileIDs;
                    for (uint32_t tj = 0; tj < dimFirst.ty; tj++) {
                        for (uint32_t ti = 0; ti < dimFirst.tx; ti++) {
                            if (rasterBaseFirst.tileDataOnDevice(ti, tj)) {
                                tileIDs.push_front((uint64_t)ti | ((uint64_t)tj << 32));
                            } else {
                                tileIDs.push_back((uint64_t)ti | ((uint64_t)tj << 32));
                            }
                        }
                    }

                    // Create count buffer
                    cl_uint count = 0;
                    cl::Buffer countBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_uint));
                    solver.fillBuffer<cl_uint>(countBuffer, 0, 1);          

                    // save random state if needed
                    rasterBaseFirst.saveRandomState();

                    // Run over all tiles
                    for (auto tid : tileIDs) {

                        uint32_t ti = tid&0xFFFFFFFF;
                        uint32_t tj = tid>>32;

                        // Set kernel arguments
                        stippleCountKernel.setArg(0, countBuffer);
                        stippleCountKernel.setArg(1, nPerCell);

                        // Run kernel
                        runTileKernel(ti, tj, stippleCountKernel, rasterBaseRefs, stippleGenerator.reqs, 2);
                    }

                    // Get count
                    queue.enqueueReadBuffer(countBuffer, CL_TRUE, 0, sizeof(cl_uint), static_cast<void *>(&count));
                    if (count > 0) {

                        // Create coordinates
                        auto pCoords = std::make_shared<VariablesVector<Coordinate<CTYPE> > >();
                        VariablesVector<Coordinate<CTYPE> > &coords = *pCoords.get();
                        coords.getData().resize(count);

                        // Create index buffer
                        cl::Buffer indexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY, count*sizeof(cl_ulong));

                        // Create fields                        
                        auto pProperties = std::make_shared<PropertyMap>();
                        PropertyMap &properties = *pProperties.get();
                        for (auto &fieldName : stippleGenerator.fieldNames) {                            
                            properties.addProperty(fieldName);
                            std::vector<RTYPE> &p = properties.template getPropertyRef<std::vector<RTYPE> >(fieldName);
                            p.resize(count);
                        }

                        // Reset count
                        solver.fillBuffer<cl_uint>(countBuffer, 0, 1);

                        // Set stippling kernel arguments
                        stippleKernel.setArg(0, coords.getBuffer());
                        stippleKernel.setArg(1, indexBuffer);
                        stippleKernel.setArg(2, countBuffer);
                        stippleKernel.setArg(3, nPerCell);
                        stippleKernel.setArg(4, dimFirst.tx<<TileMetrics::tileSizePower);
                        stippleKernel.setArg(5, dimFirst.ty<<TileMetrics::tileSizePower);
                        cl_uint arg = 8;
                        for (auto &fieldName : stippleGenerator.fieldNames) {   
                            stippleKernel.setArg(arg++, properties.getPropertyBuffer(fieldName));
                        }

                        // restore random state if needed
                        rasterBaseFirst.restoreRandomState();
                        
                        // Run stippling
                        for (auto tid : tileIDs) {

                            uint32_t ti = tid&0xFFFFFFFF;
                            uint32_t tj = tid>>32;

                            // Set tile indexes
                            stippleKernel.setArg(6, ti);
                            stippleKernel.setArg(7, tj);

                            // Run kernel
                            runTileKernel(ti, tj, stippleKernel, rasterBaseRefs, stippleGenerator.reqs, arg);
                        }

                        // Read index buffer
                        std::vector<cl_ulong> indexes(count);
                        queue.enqueueReadBuffer(indexBuffer, CL_TRUE, 0, count*sizeof(cl_ulong), static_cast<void *>(indexes.data()));
                        
                        // Create permutation vectors
                        std::vector<std::size_t> permute(indexes.size());
                        std::iota(permute.begin(), permute.end(), 0);
                        std::sort(permute.begin(), permute.end(), [&](std::size_t i, std::size_t j) { 
                            return indexes[i] < indexes[j];
                        });

                        // Apply permutation
                        PermutateVector(coords.getData(), permute);
                        for (auto &name : properties.getPropertyNames()) {
                            std::vector<RTYPE> &p = properties.template getPropertyRef<std::vector<RTYPE> >(name);
                            PermutateVector(p, permute);
                        }

                        // Populate Vector
                        v = Vector<CTYPE>(pCoords, pProperties);
                    }

                    // Set projection
                    v.setProjectionParameters(rasterBaseFirst.getProjectionParameters());

                } else {
                    throw std::runtime_error("No rasters in stipple script");
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

        return v;
    }

    /**
    * Sort column values in z-direction.
    * @param rasterBaseRef input raster.
    */
    template <typename CTYPE>
    void sortColumns(RasterBase<CTYPE> &rasterBase) {
    
        // Get OpenCL solver handles
        auto &solver = Geostack::Solver::getSolver();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            // Get raster properties
            std::string name = rasterBase.template getProperty<std::string>("name");
            auto dim = rasterBase.getRasterDimensions();
            
            // Store and set raster properties
            bool rasterBaseNeedsWrite = rasterBase.getNeedsWrite();
            rasterBase.setNeedsWrite(true);
            
            // Generate references
            RasterBaseRefs<CTYPE> rasterBaseRefs;            
            rasterBaseRefs.push_back(rasterBase);
            
            // Get generator
            auto scriptHash = createScriptHash("", Models::cl_sort_c, rasterBaseRefs);
            auto sortGenerator = RasterKernelCache::getKernelCache().getKernelGenerator(scriptHash);
            if (sortGenerator.programHash == Solver::getNullHash()) {

                // Generate kernel script
                sortGenerator.script = Models::cl_sort_c;
                sortGenerator.reqs.rasterRequirements.push_back(RasterKernelRequirement(0));

                // Build raster kernel
                if (buildRasterKernel(sortGenerator) == solver.getNullHash()) {
                    throw std::runtime_error("Cannot get kernel for script");
                }

                // Register kernel in cache
                RasterKernelCache::getKernelCache().setKernelGenerator(scriptHash, sortGenerator);
            }

            // Select kernel based on data type
            auto &sortKernel = solver.getKernel(sortGenerator.programHash, "sort_" + rasterBase.getOpenCLTypeString()); 

            // Loop through tiles
            uint8_t verbose = solver.getVerboseLevel();
            try {

                // Order processing so any tiles on device are run first
                std::list<uint64_t> tileIDs;
                for (uint32_t tj = 0; tj < dim.ty; tj++) {
                    for (uint32_t ti = 0; ti < dim.tx; ti++) {
                        if (rasterBase.tileDataOnDevice(ti, tj)) {
                            tileIDs.push_front((uint64_t)ti | ((uint64_t)tj << 32));
                        } else {
                            tileIDs.push_back((uint64_t)ti | ((uint64_t)tj << 32));
                        }
                    }
                }

                // Run over all tiles
                std::size_t nVerbose = 0;
                std::size_t nVerboseCount = 0;                
                for (auto tid : tileIDs) {

                    uint32_t ti = tid&0xFFFFFFFF;
                    uint32_t tj = tid>>32;

                    // Set arguments                    
                    sortKernel.setArg(0, rasterBase.getTileDataBuffer(ti, tj));
                    sortKernel.setArg(1, rasterBase.getTileDimensions(ti, tj).d);

                    // Execute kernel
                    queue.enqueueNDRangeKernel(sortKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, dim.d.ny, 1), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));

                    // Output progress
                    if (verbose <= Geostack::Verbosity::Info) {
                        if (nVerbose == 0) {
                            nVerbose = tileIDs.size()/10;
                            std::cout << std::setprecision(2) << 100.0*(double)nVerboseCount/(double)tileIDs.size() << "%" << std::endl;
                        } else {
                            nVerbose--;
                        }
                        nVerboseCount++;
                    }
                }

            } catch (cl::Error e) {
                std::stringstream err;
                err << "OpenCL exception '" << e.what() << "': " << e.err();
                throw std::runtime_error(err.str());
            }

            if (verbose <= Geostack::Verbosity::Info) {
                std::cout << "100%" << std::endl;
            }

            // Preserve original properties
            rasterBase.setNeedsWrite(rasterBaseNeedsWrite);

        } else {
            throw std::runtime_error("OpenCL not initialised");
        }
    }

    // CTYPE float definitions
    template class RasterBase<float>;
    template std::ostream &operator<<(std::ostream &, const Dimensions<float> &);
    template std::ostream &operator<<(std::ostream &, const RasterDimensions<float> &);
    template void runScriptNoOut<float>(std::string, RasterBaseRefs<float>, size_t);
    template RasterKernelGenerator generateKernel(std::string, std::string, RasterBaseRefs<float>, RasterNullValue::Type, VariablesBasePtr<std::string>, bool, bool);
    template bool setTileKernelArguments<float>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<float>, RasterKernelRequirements, int, VariablesBasePtr<std::string>, bool);
    template bool runTileKernel<float>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<float>, RasterKernelRequirements, int, VariablesBasePtr<std::string>, size_t);
    template float getMaxTileDataSize<float>(RasterBaseRefs<float>);
    template void sortColumns<float>(RasterBase<float> &);

    // CTYPE double definitions
    template class RasterBase<double>;
    template std::ostream &operator<<(std::ostream &, const Dimensions<double> &);
    template std::ostream &operator<<(std::ostream &, const RasterDimensions<double> &);
    template void runScriptNoOut<double>(std::string, RasterBaseRefs<double>, size_t);
    template RasterKernelGenerator generateKernel(std::string, std::string, RasterBaseRefs<double>, RasterNullValue::Type, VariablesBasePtr<std::string>, bool, bool);
    template bool setTileKernelArguments<double>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<double>, RasterKernelRequirements, int, VariablesBasePtr<std::string>, bool);
    template bool runTileKernel<double>(uint32_t, uint32_t, cl::Kernel &, RasterBaseRefs<double>, RasterKernelRequirements, int, VariablesBasePtr<std::string>, size_t);
    template float getMaxTileDataSize<double>(RasterBaseRefs<double>);
    template void sortColumns<double>(RasterBase<double> &);

    // RTYPE float, CTYPE float definitions
    template class RasterFileHandler<float, float>;
    template class Raster<float, float>;

    template float RasterBase<float>::getVariableData<float>(std::string);
    template float RasterBase<float>::getVariableData<float>(std::string, std::size_t);
    template void RasterBase<float>::setVariableData<float>(std::string, float);
    template void RasterBase<float>::setVariableData<float>(std::string, float, std::size_t);
    template bool operator==(const Raster<float, float> &, const Raster<float, float> &);
    template Raster<float, float> runScript<float, float>(std::string, RasterBaseRefs<float>, size_t);
    template Raster<float, float> runAreaScript<float, float>(std::string, RasterBase<float> &, cl_int);
    template Vector<float> stipple<float, float>(std::string, RasterBaseRefs<float>, std::vector<std::string>, uint32_t);
    
    // RTYPE uint, CTYPE float definitions
    template class RasterFileHandler<uint32_t, float>;
    template class Raster<uint32_t, float>;
    
    template uint32_t RasterBase<float>::getVariableData<uint32_t>(std::string);
    template uint32_t RasterBase<float>::getVariableData<uint32_t>(std::string, std::size_t);
    template void RasterBase<float>::setVariableData<uint32_t>(std::string, uint32_t);
    template void RasterBase<float>::setVariableData<uint32_t>(std::string, uint32_t, std::size_t);
    template bool operator==(const Raster<uint32_t, float> &, const Raster<uint32_t, float> &);
    template Raster<uint32_t, float> runScript<uint32_t, float>(std::string, RasterBaseRefs<float>, size_t);
    template Raster<uint32_t, float> runAreaScript<uint32_t, float>(std::string, RasterBase<float> &, cl_int);
    template Vector<float> stipple<uint32_t, float>(std::string, RasterBaseRefs<float>, std::vector<std::string>, uint32_t);

    // RTYPE uchar, CTYPE float definitions
    template class RasterFileHandler<uint8_t, float>;
    template class Raster<uint8_t, float>;
    
    template uint8_t RasterBase<float>::getVariableData<uint8_t>(std::string);
    template uint8_t RasterBase<float>::getVariableData<uint8_t>(std::string, std::size_t);
    template void RasterBase<float>::setVariableData<uint8_t>(std::string, uint8_t);
    template void RasterBase<float>::setVariableData<uint8_t>(std::string, uint8_t, std::size_t);
    template bool operator==(const Raster<uint8_t, float> &, const Raster<uint8_t, float> &);
    template Raster<uint8_t, float> runScript<uint8_t, float>(std::string, RasterBaseRefs<float>, size_t);
    template Raster<uint8_t, float> runAreaScript<uint8_t, float>(std::string, RasterBase<float> &, cl_int);
    template Vector<float> stipple<uint8_t, float>(std::string, RasterBaseRefs<float>, std::vector<std::string>, uint32_t);
    
    // RTYPE double, CTYPE double definitions
    template class RasterFileHandler<double, double>;
    template class Raster<double, double>;

    template double RasterBase<double>::getVariableData<double>(std::string);
    template double RasterBase<double>::getVariableData<double>(std::string, std::size_t);
    template void RasterBase<double>::setVariableData<double>(std::string, double);
    template void RasterBase<double>::setVariableData<double>(std::string, double, std::size_t);
    template bool operator==(const Raster<double, double> &, const Raster<double, double> &);
    template Raster<double, double> runScript<double, double>(std::string, RasterBaseRefs<double>, size_t);
    template Raster<double, double> runAreaScript<double, double>(std::string, RasterBase<double> &, cl_int);
    template Vector<double> stipple<double, double>(std::string, RasterBaseRefs<double>, std::vector<std::string>, uint32_t);

    // RTYPE uint, CTYPE double definitions
    template class RasterFileHandler<uint32_t, double>;
    template class Raster<uint32_t, double>;
    
    template uint32_t RasterBase<double>::getVariableData<uint32_t>(std::string);
    template uint32_t RasterBase<double>::getVariableData<uint32_t>(std::string, std::size_t);
    template void RasterBase<double>::setVariableData<uint32_t>(std::string, uint32_t);
    template void RasterBase<double>::setVariableData<uint32_t>(std::string, uint32_t, std::size_t);
    template bool operator==(const Raster<uint32_t, double> &, const Raster<uint32_t, double> &);
    template Raster<uint32_t, double> runScript<uint32_t, double>(std::string, RasterBaseRefs<double>, size_t);
    template Raster<uint32_t, double> runAreaScript<uint32_t, double>(std::string, RasterBase<double> &, cl_int);
    template Vector<double> stipple<uint32_t, double>(std::string, RasterBaseRefs<double>, std::vector<std::string>, uint32_t);

    // RTYPE uchar, CTYPE double definitions
    template class RasterFileHandler<uint8_t, double>;
    template class Raster<uint8_t, double>;
    
    template uint8_t RasterBase<double>::getVariableData<uint8_t>(std::string);
    template uint8_t RasterBase<double>::getVariableData<uint8_t>(std::string, std::size_t);
    template void RasterBase<double>::setVariableData<uint8_t>(std::string, uint8_t);
    template void RasterBase<double>::setVariableData<uint8_t>(std::string, uint8_t, std::size_t);
    template bool operator==(const Raster<uint8_t, double> &, const Raster<uint8_t, double> &);
    template Raster<uint8_t, double> runScript<uint8_t, double>(std::string, RasterBaseRefs<double>, size_t);
    template Raster<uint8_t, double> runAreaScript<uint8_t, double>(std::string, RasterBase<double> &, cl_int);
    template Vector<double> stipple<uint8_t, double>(std::string, RasterBaseRefs<double>, std::vector<std::string>, uint32_t);
}
