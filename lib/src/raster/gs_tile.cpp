/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <limits>
#include <cmath>
#include <cstring>
#include <sstream>
#include <numeric>
#include <regex>
#include <set>
#include <iomanip>
#include <cstdio>
#include <cstdlib>
#include <cctype>
#include <random>

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_tile.h"
#include "gs_raster.h"
#include "gs_vector.h"

#include "miniz.h"

#ifdef WIN32
#include <direct.h>
#define mkdir_DEF(filename) _mkdir(filename);
#define separator_DEF "\\"
#else
#include <sys/stat.h>
#define mkdir_DEF(filename) mkdir(filename, 0755);
#define separator_DEF "/"
#endif

namespace Geostack
{
    /**
    * 2D dimensions equality check.
    * @param l input dimensions.
    * @param r input dimensions.
    * @return true if dimensions match
    */
    template <typename CTYPE>
    bool equalSpatialMetrics2D(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r) {
    
        return 
            l.nx == r.nx && l.ny == r.ny &&
            l.hx == r.hx && l.hy == r.hy &&
            l.ox == r.ox && l.oy == r.oy;
    }

    /**
    * 3D dimensions equality check.
    * @param l input dimensions.
    * @param r input dimensions.
    * @return true if dimensions match
    */
    template <typename CTYPE>
    bool equalSpatialMetrics(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r) {
    
        return 
            l.nx == r.nx && l.ny == r.ny && l.nz == r.nz &&
            l.hx == r.hx && l.hy == r.hy && l.hz == r.hz &&
            l.ox == r.ox && l.oy == r.oy && l.oz == r.oz;
    }

#ifdef USE_TILE_CACHING

    /**
    * %TileCacheManager constructor.
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE>::TileCacheManager():
        cacheHostAllocationBytes(0), maxHostAllocationBytes(0), 
        cacheDeviceAllocationBytes(0), maxDeviceAllocationBytes(0) {
    
        // Get handles
        auto &solver = Geostack::Solver::getSolver();
        uint8_t verbose = solver.getVerboseLevel();

        // Get temporary directory
        char *cacheDirectoryStr = std::getenv("GEOSTACK_TEMP_DIR");
        if (cacheDirectoryStr != NULL) {
            cacheDirectory = std::string(cacheDirectoryStr);
        } else {
            cacheDirectoryStr = std::getenv("GEOSTACK_TMP_DIR");
            if (cacheDirectoryStr != NULL) {
                cacheDirectory = std::string(cacheDirectoryStr);
            }
        }
        
        // Set temporary directory if not specified
        if (cacheDirectory.empty()) {
            std::string tempPath = std::string(std::tmpnam(nullptr));
            auto split = Strings::splitPath(tempPath);
            cacheDirectory = split[0];

        }

        // Output temporary directory
        if (verbose <= Geostack::Verbosity::Info) {
            std::cout << "TileCacheManager: Using tile cache directory '" << cacheDirectory << "'" << std::endl;
        }

        // Create subdirectories
        std::string createDirectory0 = cacheDirectory + "_geostack";
        mkdir_DEF(createDirectory0.c_str());
        for (uint32_t i = 0; i < 10; i++) {
            std::string createDirectory1 = createDirectory0 + separator_DEF + std::to_string(i);
            mkdir_DEF(createDirectory1.c_str());
            for (uint32_t j = 0; j < 10; j++) {
                std::string createDirectory2 = createDirectory1 + separator_DEF + std::to_string(j);
                mkdir_DEF(createDirectory2.c_str());
                for (uint32_t k = 0; k < 10; k++) {
                    std::string createDirectory3 = createDirectory2 + separator_DEF + std::to_string(k);
                    mkdir_DEF(createDirectory3.c_str());
                }
            }
        }

        // Get allocation limits
        maxHostAllocationBytes = solver.getHostMemoryLimit();
        maxDeviceAllocationBytes = (uint64_t)(0.75*solver.getDeviceMemoryLimit());

        // Set compression point
        cacheHostCompressItem = cacheHostList.end();
    }

    /**
    * Singleton instance of %TileCacheManager.
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE> &TileCacheManager<RTYPE, CTYPE>::instance() {

        static TileCacheManager tileManager;
        return tileManager;
    }
    
    /**
    * TileCacheManager destructor
    */
    template <typename RTYPE, typename CTYPE>
    TileCacheManager<RTYPE, CTYPE>::~TileCacheManager() {
    }

    /**
    * Update host tile cache entries and write tile data to cache if required
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::updateHost(Tile<RTYPE, CTYPE> *tile, bool isConst) {

        if (maxHostAllocationBytes > 0) {

            // Check for tile entry
            auto ith = cacheHostMap.find(tile);
            if (ith == cacheHostMap.end()) {

                // Update cache
                cacheHostList.push_front(TileCacheItem<RTYPE, CTYPE>(tile, isConst));
                cacheHostMap[tile] = cacheHostList.begin();

            } else {
 
                // Swap to front
                cacheHostList.splice(cacheHostList.begin(), cacheHostList, ith->second);
            }

            // Check cache size
            while (cacheHostAllocationBytes > maxHostAllocationBytes) {

                // Get least recently used item
                auto &tileCacheItem = cacheHostList.back();
                auto *cacheTile = tileCacheItem.pData;

                // Write to cache
                if (!tileCacheItem.dataConst) {
                    tileCacheItem.pData->cacheWrite();
                }

                // Clear data and cache entries
                cacheTile->deleteData(false);
                cacheHostMap.erase(cacheTile);
                cacheHostList.pop_back();
            }
        }
    }

    /**
    * Update host tile cache entries and write tile data to cache if required
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::updateDevice(Tile<RTYPE, CTYPE> *tile) {

        if (maxDeviceAllocationBytes > 0) {

            // Get tile from host map
            auto ith = cacheHostMap.find(tile);
            if (ith == cacheHostMap.end()) {
                throw std::runtime_error("Tile not in host cache map");
            }

            // Check for tile entry
            auto itd = cacheDeviceMap.find(tile);
            if (itd == cacheDeviceMap.end()) {
 
                // Update cache
                cacheDeviceList.push_front(TileCacheItem<RTYPE, CTYPE>(tile, ith->second->dataConst));
                cacheDeviceMap[tile] = cacheDeviceList.begin();

            } else {
 
                // Swap to front
                cacheDeviceList.splice(cacheDeviceList.begin(), cacheDeviceList, itd->second);
            }

            // Check cache size
            while (cacheDeviceAllocationBytes > maxDeviceAllocationBytes) {

                // Get least recently used item
                auto &tileCacheItem = cacheDeviceList.back();
                auto *cacheTile = tileCacheItem.pData;

                // Compress tile
                if (!tileCacheItem.dataConst) {
                    cacheTile->ensureDataBufferOnHost();
                    cacheTile->cacheCompress();
                }

                // Clear data and cache entries
                cacheTile->clearDataBuffer();
                cacheTile->deleteData(false);
                cacheDeviceMap.erase(cacheTile);
                cacheDeviceList.pop_back();
            }
        }
    }

    /**
    * Remove tile cache entries
    */
    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::removeHostEntry(Tile<RTYPE, CTYPE> *tile) {

        // Remove existing host map entries
        auto ith = cacheHostMap.find(tile);
        if (ith != cacheHostMap.end()) {
            cacheHostList.erase(ith->second);
            cacheHostMap.erase(ith);
        }
    }

    template <typename RTYPE, typename CTYPE>
    void TileCacheManager<RTYPE, CTYPE>::removeDeviceEntry(Tile<RTYPE, CTYPE> *tile) {

        // Remove existing device map entries
        auto itd = cacheDeviceMap.find(tile);
        if (itd != cacheDeviceMap.end()) {
            cacheDeviceList.erase(itd->second);
            cacheDeviceMap.erase(itd);
        }
    }

#endif

    /**
    * Default %Tile constructor
    * dimensions will be POD initialised to zero
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::Tile():
        dim(), 
        dataBufferCreated(false),
        reduceBufferCreated(false),
        reduceBufferSize(0),
        statusBufferCreated(false),
        dataBufferOnDevice(false),
        reduceBufferOnDevice(false),
        statusBufferOnDevice(false),
        dataInitialised(false),
        status(0) { }
     
    /**
    * %Tile Destructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::~Tile() {

#ifdef USE_TILE_CACHING

        auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();

        // Update host cache
        if (dataInitialised) {
            tileCacheManager.updateCacheHostAllocationBytes(-(uint64_t)(dim.d.mx+1)*dim.d.my*dim.d.nz*sizeof(RTYPE));
        }
        tileCacheManager.removeHostEntry(this);
        
        // Update device cache
        if (dataBufferCreated) {
            tileCacheManager.updateCacheDeviceAllocationBytes(-(int64_t)getDataSize()*sizeof(RTYPE));
        }
        tileCacheManager.removeDeviceEntry(this);

#endif

    }

    /**
    * %Tile Comparison operator
    * @param l left-hand Raster to compare.
    * @param r right-hand Raster to compare.
    */
    template <typename RTYPE, typename CTYPE>
    bool tileDataEqual(Tile<RTYPE, CTYPE> &l, Tile<RTYPE, CTYPE> &r) { 

        // Compare objects
        if (&l == &r) {
            return true;
        }
        if (!equalSpatialMetrics(l.dim.d, r.dim.d)) {
            return false;
        }

        // Ensure buffers are on host
        l.ensureDataBufferOnHost();
        r.ensureDataBufferOnHost();
        
        // Compare data vectors
        if (l.dataVec.size() != r.dataVec.size()) {
            return false;
        }
        for (std::size_t k = 0; k < l.dim.d.nz; k++) {
            for (std::size_t j = 0; j < l.dim.d.ny; j++) {
                for (std::size_t i = 0; i < l.dim.d.nx; i++) {
                    if (l.dataVec[i+(j+k*l.dim.d.my)*l.dim.d.mx] != 
                        r.dataVec[i+(j+k*l.dim.d.my)*l.dim.d.mx]) {
                        return false;
                    }
                }
            }
        }

        return true; 
    }
        
#ifdef USE_TILE_CACHING
    /**
    * %TileCache constructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::TileCache::TileCache():
        isCompressed(false),
        isWritten(false) {
    }

    /**
    * %TileCache destructor
    */
    template <typename RTYPE, typename CTYPE>
    Tile<RTYPE, CTYPE>::TileCache::~TileCache() {
        
        // Delete cache file
        if (isWritten) {
            std::remove(cacheName.c_str());
        }
    }
    
    /**
    * %TileCache read
    * Read from either compressed map or file into %Tile
    * @param tile %Tile reference
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::TileCache::readTileData(std::vector<RTYPE> &tileData) {

        // Open file
        std::FILE *pf = std::fopen(cacheName.c_str(), "rb");

        // Check file
        if (!pf) {
            std::stringstream err;
            err << "Cannot open cache file '" << cacheName << "' for reading";
            throw std::runtime_error(err.str());
        }

        // Read length
        uint32_t dataLength = 0;
        std::size_t rc = std::fread(reinterpret_cast<char *>(&dataLength), sizeof(uint32_t), 1, pf);
        if (std::feof(pf)) {
            throw std::runtime_error("Unexpected end of file while reading data length");
        }

        // Read data
        std::vector<unsigned char> compressedData(dataLength);
        rc = std::fread(reinterpret_cast<char *>(compressedData.data()), sizeof(unsigned char), dataLength, pf);
        if (std::feof(pf)) {
            throw std::runtime_error("Unexpected end of file while reading data");
        }

        // Close file
        std::fclose(pf);
            
        // Uncompress
        uncompressTileData(compressedData, tileData);
    }
    
    /**
    * %TileCache write
    * Write compressed %Tile to file
    * @param tile %Tile reference
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::TileCache::writeTileData(std::vector<unsigned char> &compressedData) {

        // Generate cache name
        if (cacheName.empty()) {
            
            // Get user-defined temporary directory
            const auto &cacheDirectory = TileCacheManager<RTYPE, CTYPE>::instance().getCacheDirectory();

            // Append directory split
            std::string currentCacheDirectory;
            std::string tempPath = std::string(std::tmpnam(nullptr));
            auto split = Strings::splitPath(tempPath);
            if (cacheDirectory.empty()) {
                currentCacheDirectory = split[0];
            } else {
                currentCacheDirectory = cacheDirectory;
            }

            // Generate cache name
            cacheName = cacheDirectory + "_geostack" + 
                separator_DEF + std::to_string(rand()%10) +
                separator_DEF + std::to_string(rand()%10) +
                separator_DEF + std::to_string(rand()%10) + 
                separator_DEF + split[1] + split[2] + ".tile";
        }

        // Open file
        std::FILE *pf = std::fopen(cacheName.c_str(), "wb");

        // Check file
        if (!pf) {
            std::stringstream err;
            err << "Cannot open cache file '" << cacheName << "' for writing";
            throw std::runtime_error(err.str());
        }
        
        // Write data length
        uint32_t writeDataLength = (uint32_t)compressedData.size();
        std::fwrite(reinterpret_cast<const char *>(&writeDataLength), sizeof(uint32_t), 1, pf);

        // Write tile data
        std::fwrite(reinterpret_cast<const char *>(compressedData.data()), sizeof(unsigned char), writeDataLength, pf);

        // Close file
        std::fclose(pf);
    }
    
    /**
    * %TileCache compression
    * Compress %Tile and add to compression map
    * @param tile %Tile reference
    */
    template <typename RTYPE, typename CTYPE>
    std::vector<unsigned char> Tile<RTYPE, CTYPE>::TileCache::compressTile(std::vector<RTYPE> &tileData) {

        // Set data sizes
        uint64_t dataLength = tileData.size()*sizeof(RTYPE);
        mz_ulong compressedDataLen = compressBound((mz_ulong)dataLength);

        // Compress
        std::vector<unsigned char> compressedData(compressedDataLen);
        mz_ulong len = compressedDataLen;
        int status = compress2(compressedData.data(), &len, 
            reinterpret_cast<const unsigned char *>(tileData.data()), (mz_ulong)dataLength, MZ_BEST_SPEED);

        if (status != Z_OK) {
            std::stringstream err;
            err << "Compression failed '" << mz_error(status) << "'";
            throw std::runtime_error(err.str());
        }

        // Shrink vector
        compressedData.resize(len);
        compressedData.shrink_to_fit();

        return compressedData;
    }
    
    /**
    * %TileCache uncompression
    * Uncompress %Tile and remove from compression map
    * @param tile %Tile reference
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::TileCache::uncompressTileData(std::vector<unsigned char> &compressedData, std::vector<RTYPE> &tileData) {

        // Uncompress
        mz_ulong len = (mz_ulong)tileData.size()*sizeof(RTYPE);
        int status = uncompress(reinterpret_cast<unsigned char *>(tileData.data()), &len, 
            reinterpret_cast<const unsigned char *>(compressedData.data()), (mz_ulong)compressedData.size());

        // Check status
        if (status != Z_OK) {
            std::stringstream err;
            err << "Uncompression failed '" << mz_error(status) << "'";
            throw std::runtime_error(err.str());
        } 

        // Check size
        if (len != (mz_ulong)tileData.size()*sizeof(RTYPE)) {
            throw std::runtime_error("Unexpected data size during uncompression");
        }
    }

#endif

    /**
    * Create data %Tile data.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::createData() {
        
        if (!dataInitialised) {

            // Get handles
            auto &solver = Geostack::Solver::getSolver();

            // Check data size
            size_t dataSize = getDataSize();
            if (dataSize*sizeof(RTYPE) >= solver.getMaxAllocSizeBytes()) {
                std::stringstream err;
                err << "Cannot allocate OpenCL memory for tile of size " 
                    << (dataSize>>20) << " Mb, maximum allowable size is " 
                    << (solver.getMaxAllocSizeBytes()>>20) << " Mb";
                throw std::runtime_error(err.str());
            }
        
            // Allocate data buffer
            dataVec.resize(dataSize);

#ifdef USE_TILE_CACHING

            // Update allocation size
            auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
            tileCacheManager.updateCacheHostAllocationBytes((uint64_t)(dim.d.mx+1)*dim.d.my*dim.d.nz*sizeof(RTYPE));

#endif

            // Reset status
            status = 0;

            // Map to host
            dataBufferOnDevice = false;
            reduceBufferOnDevice = false;
            statusBufferOnDevice = false;

            // Set initialisation flag
            dataInitialised = true;
        }
    }

    /**
    * Reset %Tile to null.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::clearData() {
                
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Check and move data to host
        if (dataBufferOnDevice) {

            // Reset device cell values
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            solver.fillBuffer<RTYPE>(dataBuffer, getNullValue<RTYPE>(), getDataSize());

        } else {
        
            // Reset host cell values
            dataVec.assign(getDataSize(), getNullValue<RTYPE>());
        }
    }

    /**
    * Read %Tile data from handler.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::readData(Raster<RTYPE, CTYPE> &r, dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler) {
    
        // Check data handler
        if (readDataHandler != nullptr) {        

            // Read from file
            readDataHandler(dim, dataVec, r);

            // Check data on return
            if (dataVec.size() != getDataSize()) {
                throw std::runtime_error("Tile data from handler is invalid");
            }
        }
    }

    /**
    * Delete %Tile data.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::deleteData(bool updateCache) {

#ifdef USE_TILE_CACHING

        // Update cache
        auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
        if (dataInitialised) {
            tileCacheManager.updateCacheHostAllocationBytes(-(uint64_t)(dim.d.mx+1)*dim.d.my*dim.d.nz*sizeof(RTYPE));
        }
        if (updateCache) {
            tileCacheManager.removeHostEntry(this);
        }
#endif

        // Clear host data
        std::vector<RTYPE>().swap(dataVec);
        std::vector<RTYPE>().swap(reduceVec);
        status = 0;

        // Clear buffers
        if (dataBufferCreated) {
            dataBuffer = cl::Buffer();
            dataBufferCreated = false;

#ifdef USE_TILE_CACHING

            // Update device allocation size
            tileCacheManager.updateCacheDeviceAllocationBytes(-(int64_t)getDataSize()*sizeof(RTYPE));
#endif
        }

#ifdef USE_TILE_CACHING

        if (updateCache) {
            tileCacheManager.removeDeviceEntry(this);
        }

#endif

        if (reduceBufferCreated) {
            reduceBuffer = cl::Buffer();
            reduceBufferCreated = false; 
            reduceBufferSize = 0;
        }
        if (statusBufferCreated) {
            statusBuffer = cl::Buffer();
            statusBufferCreated = false;
        }

        // Set buffer flags
        dataBufferOnDevice = false;
        reduceBufferOnDevice = false;
        statusBufferOnDevice = false;

        // Set initialisation flag
        dataInitialised = false;
    }

    /**
    * %Tile initialisation.
    * @param nz_ the number of cells in the z dimension.
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::init(
        uint32_t ti_, 
        uint32_t tj_, 
        RasterDimensions<CTYPE> rdim_) {

        // Ensure all data is on host
        if (dataInitialised) {
            ensureDataBufferOnHost();
            ensureReduceBufferOnHost();
            ensureStatusBufferOnHost();
        }

        // Set tile index
        dim.ti = ti_;
        dim.tj = tj_;
        
        // Set raster offset
        dim.d.ox = rdim_.d.ox+(CTYPE)(dim.ti*TileMetrics::tileSize)*rdim_.d.hx;
        dim.d.oy = rdim_.d.oy+(CTYPE)(dim.tj*TileMetrics::tileSize)*rdim_.d.hy; 
        dim.d.oz = rdim_.d.oz;
        
        // Set size
        dim.d.nx = std::min(rdim_.d.nx-TileMetrics::tileSize*dim.ti, TileMetrics::tileSize);
        dim.d.ny = std::min(rdim_.d.ny-TileMetrics::tileSize*dim.tj, TileMetrics::tileSize);
        dim.d.nz = rdim_.d.nz;

        // Set memory size
        dim.d.mx = TileMetrics::tileSize;
        dim.d.my = TileMetrics::tileSize;

        // Set spacing
        dim.d.hx = rdim_.d.hx;
        dim.d.hy = rdim_.d.hy;
        dim.d.hz = rdim_.d.hz;

        // Calculate end points
        dim.ex = dim.d.ox+(CTYPE)dim.d.nx*dim.d.hx;
        dim.ey = dim.d.oy+(CTYPE)dim.d.ny*dim.d.hy;
        dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;

        // Set buffer flags
        dataBufferOnDevice = false;
        reduceBufferOnDevice = false;
        statusBufferOnDevice = false;

        return true;
    }

    /**
    * Get %Tile OpenCL data buffer.
    * @return Reference %Tile data buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getDataBuffer() {

        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Ensure data buffer is on device
        if (!dataBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapDataBuffer(queue);
        }
        
#ifdef USE_TILE_CACHING

        // Update device
        auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
        tileCacheManager.updateDevice(this);
#endif

        // Return reference to data buffer
        return dataBuffer;
    }

    /**
    * Get %Tile OpenCL reduction buffer.
    * @return Reference %Tile reduction buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getReduceBuffer() {
    
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Ensure reduction buffer is on device
        if (!reduceBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapReduceBuffer(queue);
        }
        
        // Return reference to reduce buffer
        return reduceBuffer;
    }

    /**
    * Get %Tile OpenCL status buffer.
    * @return Reference %Tile status buffer.
    */
    template <typename RTYPE, typename CTYPE>
    cl::Buffer &Tile<RTYPE, CTYPE>::getStatusBuffer() {
    
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Ensure reduction buffer is on device
        if (!statusBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            unmapStatusBuffer(queue);
        }
        
        // Return reference to status buffer
        return statusBuffer;
    }

    /**
    * Ensure tile data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureDataBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Check and move data to host
        if (dataBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapDataBuffer(queue);
        }
    }

    /**
    * Ensure reduction data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureReduceBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Check and move data to host
        if (reduceBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapReduceBuffer(queue);
        }
    }

    /**
    * Ensure status data is on the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::ensureStatusBufferOnHost() {
    
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Check and move data to host
        if (statusBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapStatusBuffer(queue);
        }
    }

    /**
    * Handle to %Tile cell.
    * @param i x cell index.
    * @param j y cell index.
    * @param k z cell index.
    * @return Reference to cell data.
    */
    template <typename RTYPE, typename CTYPE>
    inline RTYPE &Tile<RTYPE, CTYPE>::operator()(uint32_t i, uint32_t j, uint32_t k) { 

        // Ensure data is available
        ensureDataBufferOnHost();

        // Return reference to value
        return dataVec[i+((j+(k<<TileMetrics::tileSizePower))<<TileMetrics::tileSizePower)];
    }

    /**
    * Check tile for data.
    * @return true is %Tile contains data.
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::hasData() {
    
        // Check vector
        return !dataVec.empty();
    }

    /**
    * Extract data from %Tile bypassing all memory paging, safe to call without getTile
    * @param data vector to copy into.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::extractData(std::vector<RTYPE> &data, Raster<RTYPE, CTYPE> &r) {

        // Get data size
        size_t dataSize = getDataSize();
        
#ifdef USE_TILE_CACHING

        // Get data
        if (tileCache.getIsWritten()) {

            // Read and uncompress tile
            data.resize(dataSize);
            tileCache.readTileData(data);

        } else if (tileCache.getIsCompressed()) {

            // Get cache manager handle
            auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
        
            // Get compressed data
            data.resize(dataSize);
            auto &compressedMap = tileCacheManager.compressedTiles;
            auto cit = compressedMap.find(this);
            if (cit == compressedMap.end()) {
                throw std::runtime_error("Tile is not compressed");
            }
            auto &compressedData = cit->second;

            // Uncompress tile
            tileCache.uncompressTileData(compressedData, data);

        } else if (dataBufferOnDevice) {
#else
        if (dataBufferOnDevice) {
#endif
            // Copy data from device
            data.resize(dataSize);
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            queue.enqueueReadBuffer(dataBuffer, CL_TRUE, 0, dataSize*sizeof(RTYPE), static_cast<void *>(data.data()));

        } else if (hasData()) { 

            // Copy data from host
            data = dataVec;
        
        } else {
        
            // Create data
            data.assign(dataSize, getNullValue<RTYPE>());

            // Call data handler
            const auto &fileHandlerIn = r.getFileInputHandler();
            if (fileHandlerIn != nullptr) {

                // Read from file
                const dataHandlerReadFunction<RTYPE, CTYPE> &readDataHandler = fileHandlerIn->getReadDataHandler();
                readDataHandler(dim, data, r); 

                // Check data on return
                if (data.size() != getDataSize()) {
                    throw std::runtime_error("Tile data from handler is invalid");
                }

            }
        }
    }

    /**
    * Copy data from %Tile.
    * @param iTo vector iterator to copy into.
    * @param start start position.
    * @param start end position.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::getData(tIterator iTo, size_t start, size_t end) {

        // Ensure data is available
        ensureDataBufferOnHost();

        // Copy data
        std::copy(dataVec.begin()+start, dataVec.begin()+end, iTo);
    }
    
    /**
    * Copy data to %Tile.
    * @param data vector to copy into.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setData(tIterator iFromStart, tIterator iFromEnd, size_t start) {
    
        // Ensure data is available
        ensureDataBufferOnHost();

        // Copy data
        std::copy(iFromStart, iFromEnd, dataVec.begin()+start);
    }

    /**
    * Set %Tile index.
    * @param ti %Tile x index.
    * @param tj %Tile y index.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setIndex(uint32_t ti_, uint32_t tj_) {

        // Update indexes
        dim.ti = ti_;
        dim.tj = tj_;
    }
    
    /**
    * Set value in all %Tile cells.
    * Sets entire raster to specified value
    * @param v Value to set.
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setAllCellValues(RTYPE val) {
                    
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Check and move data to host
        if (dataBufferOnDevice) {

            // Set device cell values
            auto &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            solver.fillBuffer<RTYPE>(dataBuffer, val, getDataSize());

        } else {
        
            // Set host cell values
            tIterator iStart = dataVec.begin();
            tIterator iEnd = dataVec.begin()+dim.d.nx;
            for (uint32_t k = 0; k < dim.d.mx*dim.d.my*dim.d.nz; k+=dim.d.mx*dim.d.my)
                for (uint32_t j = 0; j < dim.d.mx*dim.d.ny; j+=dim.d.mx)
                    std::fill(iStart+j+k, iEnd+j+k, val); 
        }
    }

    /**
    * Maximum operator.
    * @return maximum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::max() {
    
        // Return null for non-initialised tiles
        if (!dataInitialised) {
            return getNullValue<RTYPE>();
        }

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        RTYPE max = std::numeric_limits<RTYPE>::lowest();
        for (auto val : dataVec)
            max = Geostack::max<RTYPE>(val, max);
            
        // Return null if limits are not found    
        return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
    }

    /**
    * Minimum operator for uint8_t.
    * @return minimum value in raster.
    */  
    template <>
    uint8_t Tile<uint8_t, float>::min() {

        // Return null for non-initialised tiles
        if (!dataInitialised) {
            return getNullValue<uint8_t>();
        }

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        uint8_t min = std::numeric_limits<uint8_t>::max();
        for (uint32_t k = 0; k < dim.d.nz; k++) {
            for (uint32_t j = 0; j < dim.d.ny; j++) {
                for (uint32_t i = 0; i < dim.d.nx; i++) {
                    min = std::min(min, operator()(i, j, k));
                }
            }
        }
        return min;
    }

    template <>
    uint8_t Tile<uint8_t, double>::min() {

        // Return null for non-initialised tiles
        if (!dataInitialised) {
            return 0;
        }

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        uint8_t min = std::numeric_limits<uint8_t>::max();
        for (uint32_t k = 0; k < dim.d.nz; k++) {
            for (uint32_t j = 0; j < dim.d.ny; j++) {
                for (uint32_t i = 0; i < dim.d.nx; i++) {
                    min = std::min(min, operator()(i, j, k));
                }
            }
        }
        return min;
    }

    /**
    * Minimum operator.
    * @return minimum value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::min() {

        // Return null for non-initialised tiles
        if (!dataInitialised) {
            return getNullValue<RTYPE>();
        }

        // Ensure data is available
        ensureDataBufferOnHost();

        // Run through tile
        RTYPE min = std::numeric_limits<RTYPE>::max();
        for (auto val : dataVec)
            min = Geostack::min<RTYPE>(val, min);
            
        // Return null if limits are not found    
        return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;    
    }

    /**
    * Reduction operator.
    * @return reduction value in raster.
    */  
    template <typename RTYPE, typename CTYPE>
    RTYPE Tile<RTYPE, CTYPE>::reduce(Reduction::Type type) {

        // Reduce values
        switch (type) {

            case(Reduction::Maximum): {

                // Ensure data is available
                ensureReduceBufferOnHost();
                
                // Get data
                RTYPE max = std::numeric_limits<RTYPE>::lowest();
                for (auto v : reduceVec)
                    max = Geostack::max<RTYPE>(v, max);
            
                // Return null if limits are not found    
                return (max == std::numeric_limits<RTYPE>::lowest()) ? getNullValue<RTYPE>() : max;  
            }
            
            case(Reduction::Minimum): {

                // Ensure data is available
                ensureReduceBufferOnHost();
                
                // Get data
                RTYPE min = std::numeric_limits<RTYPE>::max();
                for (auto v : reduceVec)
                    min = Geostack::min<RTYPE>(v, min);
            
                // Return null if limits are not found    
                return (min == std::numeric_limits<RTYPE>::max()) ? getNullValue<RTYPE>() : min;    
            }
            
            case(Reduction::Sum):
            case(Reduction::SumSquares): {

                // Ensure data is available
                ensureReduceBufferOnHost();

                // Get data
                RTYPE sum = getNullValue<RTYPE>();
                for (auto v : reduceVec) {

                    // Check for no-data
                    if (isValid<RTYPE>(v)) {
                        sum = isValid<RTYPE>(sum) ? sum+v : v;
                    }
                }
                return sum;    
            }
            
            case(Reduction::Count): {

                // Ensure data is available
                ensureStatusBufferOnHost();
                
                // Get data
                return (RTYPE)status;    
            }
            
            default:
                break;
        }

        // Return null value
        return getNullValue<RTYPE>();
    }

    /**
    * Layer reduction operator.
    * @return vector of per-layer reduction value in %Tile.
    */  
    template <typename RTYPE, typename CTYPE>
    std::vector<RTYPE> Tile<RTYPE, CTYPE>::reduceByLayer(Reduction::Type type) {

        // Reduce values
        switch (type) {

            case(Reduction::Maximum): {

                // Ensure data is available
                ensureReduceBufferOnHost();
                
                // Get data
                std::vector<RTYPE> max(dim.d.nz, std::numeric_limits<RTYPE>::lowest());
                std::size_t vi = 0;
                for (std::size_t k = 0; k < dim.d.nz; k++) {
                    for (std::size_t ij = 0; ij < TileMetrics::reductionMultiple*dim.d.ny; ij++) {
                        max[k] = Geostack::max<RTYPE>(reduceVec[vi++], max[k]);
                    }
                }
            
                // Return null if limits are not found
                for (std::size_t k = 0; k < dim.d.nz; k++) {
                    if (max[k] == std::numeric_limits<RTYPE>::lowest()) {
                        max[k] = getNullValue<RTYPE>();
                    }
                }
                return max;
            }
            
            case(Reduction::Minimum): {

                // Ensure data is available
                ensureReduceBufferOnHost();

                // Get data
                std::vector<RTYPE> min(dim.d.nz, std::numeric_limits<RTYPE>::max());
                std::size_t vi = 0;
                for (std::size_t k = 0; k < dim.d.nz; k++) {
                    for (std::size_t ij = 0; ij < TileMetrics::reductionMultiple*dim.d.ny; ij++) {
                        min[k] = Geostack::min<RTYPE>(reduceVec[vi++], min[k]);
                    }
                }
            
                // Return null if limits are not found
                for (std::size_t k = 0; k < dim.d.nz; k++) {
                    if (min[k] == std::numeric_limits<RTYPE>::max()) {
                        min[k] = getNullValue<RTYPE>();
                    }
                }
                return min;
            }
            
            case(Reduction::Sum):
            case(Reduction::SumSquares): {

                // Ensure data is available
                ensureReduceBufferOnHost();

                // Get data
                std::vector<RTYPE> sum(dim.d.nz, getNullValue<RTYPE>());
                std::size_t vi = 0;
                for (std::size_t k = 0; k < dim.d.nz; k++) {
                    for (std::size_t ij = 0; ij < TileMetrics::reductionMultiple*dim.d.ny; ij++) {
                        sum[k] = isValid<RTYPE>(sum[k]) ? sum[k]+reduceVec[vi++] : reduceVec[vi++];
                    }
                }
                return sum;    
            }
            
            case(Reduction::Count): {
            
                // Count is scalar and cannot be used for multiple layers
                throw std::runtime_error("Count reduction not available for multiple layers");
            }
            
            case(Reduction::Mean): {
            
                // Mean requires, count which is a scalar and cannot be used for multiple layers
                throw std::runtime_error("Mean reduction is not available, as Count is not available for multiple layers");
            }

            default:
                break;
        }

        // Return null values
        return std::vector<RTYPE>(dim.d.nz, getNullValue<RTYPE>());
    }
    
    /**
    * Clear reduction buffer
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::resetReduction() {
                
        // Check if data has been created
        if (!dataInitialised) {
            throw std::range_error("Tile has no data");
        }

        // Check if reduction buffer exists
        if (reduceBufferCreated) {

            // Check and move data to host
            if (reduceBufferOnDevice) {

                // Reset device cell values
                auto &solver = Geostack::Solver::getSolver();
                auto &queue = solver.getQueue();
                solver.fillBuffer<RTYPE>(reduceBuffer, getNullValue<RTYPE>(), reduceBufferSize);

            } else {
        
                // Reset host cell values
                reduceVec.assign(reduceBufferSize, getNullValue<RTYPE>());
            }
        }
    }    

    /**
    * Get status.
    * @return status bits.
    */  
    template <typename RTYPE, typename CTYPE>
    cl_uint Tile<RTYPE, CTYPE>::getStatus() {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Return status
        return status;
    }

    /**
    * Set status.
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setStatus(cl_uint newStatus) {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Set status
        status = newStatus;
    }

    /**
    * Get status and reset.
    * @param newStatus new value for status.
    * @return status bits.
    */  
    template <typename RTYPE, typename CTYPE>
    cl_uint Tile<RTYPE, CTYPE>::getResetStatus(cl_uint newStatus) {

        // Ensure data is available
        ensureStatusBufferOnHost();

        // Return status
        cl_uint lastStatus = status;
        status = newStatus;
        return lastStatus;
    }

    /**
    * Check whether %Tile data is on device
    * @return true if %Tile is on device, false otherwise
    */
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::dataOnDevice() {

        // Return cache status
        return dataBufferOnDevice;
    }

#ifdef USE_TILE_CACHING

    /**
    * Check whether %Tile data is cached
    * @return true if %Tile is cached, false otherwise
    */  
    template <typename RTYPE, typename CTYPE>
    bool Tile<RTYPE, CTYPE>::isCached()  {
    
        // Return cache status
        return (tileCache.getIsCompressed() || tileCache.getIsWritten());
    }

    /**
    * Write %Tile data to cache
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::cacheWrite() {

        if (tileCache.getIsCompressed()) {

            // Get cache manager handle
            auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
        
            // Get compressed data
            auto &compressedMap = tileCacheManager.compressedTiles;
            auto cit = compressedMap.find(this);
            if (cit == compressedMap.end()) {
                throw std::runtime_error("Tile is not compressed");
            }
            auto &compressedData = cit->second;

            // Write data to cache
            tileCache.writeTileData(compressedData);
            
            // Update allocation size
            tileCacheManager.updateCacheHostAllocationBytes(-compressedData.size());

            // Delete compressed map entry
            compressedMap.erase(cit);

        } else {

            // Map to host
            ensureDataBufferOnHost();

            // Compress data
            auto compressedData = tileCache.compressTile(dataVec);

            // Write data to cache
            tileCache.writeTileData(compressedData);
        }

        // Update flags
        tileCache.setIsCompressed(false);
        tileCache.setIsWritten(true);
    }

    /**
    * Read %Tile data from cache
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::cacheRead() {

        if (tileCache.getIsWritten()) {

            // Read and uncompress tile
            tileCache.readTileData(dataVec);

        } else if (tileCache.getIsCompressed()) {

            // Get cache manager handle
            auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
        
            // Get compressed data
            auto &compressedMap = tileCacheManager.compressedTiles;
            auto cit = compressedMap.find(this);
            if (cit == compressedMap.end()) {
                throw std::runtime_error("Tile is not compressed");
            }
            auto &compressedData = cit->second;

            // Uncompress tile
            tileCache.uncompressTileData(compressedData, dataVec);
            
            // Update allocation size
            tileCacheManager.updateCacheHostAllocationBytes(-compressedData.size());

            // Delete compressed map entry
            compressedMap.erase(cit);
        }

        // Update flags
        tileCache.setIsCompressed(false);
        tileCache.setIsWritten(false);
    }

    /**
    * Compress tile and add to compression map
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::cacheCompress() {

        if (!tileCache.getIsCompressed()) {

            // Get cache manager handle
            auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
        
            // Get compressed data
            auto &compressedMap = tileCacheManager.compressedTiles;

            // Update map
            auto compressedData = tileCache.compressTile(dataVec);
            compressedMap[this] = compressedData;

            // Update allocation size
            tileCacheManager.updateCacheHostAllocationBytes(compressedData.size());
                        
            // Update flags
            tileCache.setIsCompressed(true);
        }
    }

#endif

    /**
    * Clear data buffer
    */  
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::clearDataBuffer() {

        if (dataInitialised && dataBufferCreated) {

            // Ensure data is on host
            ensureDataBufferOnHost();

            // Clear buffer
            dataBuffer = cl::Buffer();
            dataBufferCreated = false;
            dataBufferOnDevice = false;

#ifdef USE_TILE_CACHING
            // Update device allocation size
            auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
            tileCacheManager.updateCacheDeviceAllocationBytes(-(int64_t)getDataSize()*sizeof(RTYPE));
#endif
        }
    }

    /**
    * Get bounds of tile.
    * @return %Tile %BoundingBox
    */
    template <typename RTYPE, typename CTYPE>
    BoundingBox<CTYPE> Tile<RTYPE, CTYPE>::getBounds() const { 
        return { { dim.d.ox, dim.d.oy }, { dim.ex, dim.ey} }; 
    }
    
    template <typename RTYPE, typename CTYPE>
    Coordinate<CTYPE> Tile<RTYPE, CTYPE>::getCentroid() const { 
        return { (CTYPE)0.5*(dim.d.ox+dim.d.oy), (CTYPE)0.5*(dim.ex+dim.ey) };
    }
    
    /**
    * Set %Tile origin for 2D rasters
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::setOrigin_z(CTYPE oz_) {
        if (dim.d.nz == 1) {
            dim.d.oz = oz_;
            dim.ez = dim.d.oz+(CTYPE)dim.d.nz*dim.d.hz;
        } else {
            throw std::runtime_error("Origin can only be set for 2D tiles");
        }
    }
    
    /**
    * Map OpenCL data buffer to host, this gives exclusive access to the host
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapDataBuffer(cl::CommandQueue &queue) {
        if (dataBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            if (solver.getUseMapping()) {
                void *pb = queue.enqueueMapBuffer(dataBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, getDataSize()*sizeof(RTYPE));
                if (pb != static_cast<void *>(dataVec.data())) {
                    throw std::range_error("Mapped pointer differs for data buffer");
                }
            } else {
                queue.enqueueReadBuffer(dataBuffer, CL_TRUE, 0, getDataSize()*sizeof(RTYPE), 
                    static_cast<void *>(dataVec.data()));
            }
        }
        dataBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL data buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapDataBuffer(cl::CommandQueue &queue) {

        if (!dataBufferOnDevice) {

            // Create buffer if needed
            auto &solver = Geostack::Solver::getSolver();
            if (!dataBufferCreated) {

                // Create buffer
                auto &context = solver.getContext();
                cl_int err = CL_SUCCESS;
                if (solver.getUseMapping()) {
                    dataBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, getDataSize()*sizeof(RTYPE), 
                        static_cast<void *>(dataVec.data()), &err);
                    void *pb = queue.enqueueMapBuffer(dataBuffer, CL_TRUE, CL_MAP_WRITE_INVALIDATE_REGION, 0, getDataSize()*sizeof(RTYPE));
                    if (pb != static_cast<void *>(dataVec.data())) {
                        throw std::range_error("Mapped pointer differs for data buffer");
                    }
                } else {
                    dataBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, getDataSize()*sizeof(RTYPE), NULL, &err);
                }
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for tile buffer");
                }
            
#ifdef USE_TILE_CACHING

                // Update device allocation size
                auto &tileCacheManager = TileCacheManager<RTYPE, CTYPE>::instance();
                tileCacheManager.updateCacheDeviceAllocationBytes(getDataSize()*sizeof(RTYPE));
#endif
                // Set buffer flag
                dataBufferCreated = true;
            }

            // Unmap buffer
            if (solver.getUseMapping()) {
                queue.enqueueUnmapMemObject(dataBuffer, static_cast<void *>(dataVec.data()));
            } else {
                queue.enqueueWriteBuffer(dataBuffer, CL_FALSE, 0, getDataSize()*sizeof(RTYPE), 
                    static_cast<void *>(dataVec.data()));
            }
        }

        dataBufferOnDevice = true;
    }
    
    /**
    * Map OpenCL reduction buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapReduceBuffer(cl::CommandQueue &queue) {

        // request mapping to host
        if (reduceBufferOnDevice) {
            auto &solver = Geostack::Solver::getSolver();
            if (solver.getUseMapping()) {
                void *pb = queue.enqueueMapBuffer(reduceBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, 
                    reduceBufferSize*sizeof(RTYPE));
                if (pb != static_cast<void *>(reduceVec.data())) {
                    throw std::range_error("Mapped pointer differs for reduction buffer");
                }
            } else {
                queue.enqueueReadBuffer(reduceBuffer, CL_TRUE, 0, reduceBufferSize*sizeof(RTYPE), 
                    static_cast<void *>(reduceVec.data()));
            }
        }
        reduceBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL reduction buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapReduceBuffer(cl::CommandQueue &queue) {

        // Map to device
        if (!reduceBufferOnDevice) {

            auto &solver = Geostack::Solver::getSolver();

            // Create buffer if needed
            if (!reduceBufferCreated) {

                // Set reduction buffer size
                reduceBufferSize = TileMetrics::reductionMultiple*dim.d.ny*dim.d.nz;
        
                // Allocate reduction buffer
                reduceVec.resize(reduceBufferSize, getNullValue<RTYPE>());

                // Create buffer
                auto &context = solver.getContext();
                cl_int err = CL_SUCCESS;
                if (solver.getUseMapping()) {
                    reduceBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, 
                        reduceBufferSize*sizeof(RTYPE), static_cast<void *>(reduceVec.data()), &err);
                    void *pb = queue.enqueueMapBuffer(reduceBuffer, CL_TRUE, CL_MAP_WRITE_INVALIDATE_REGION, 0, 
                        reduceBufferSize*sizeof(RTYPE));
                    if (pb != static_cast<void *>(reduceVec.data())) {
                        throw std::range_error("Mapped pointer differs for reduction buffer");
                    }
                } else {
                    reduceBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, reduceBufferSize*sizeof(RTYPE), NULL, &err);
                }
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for reduction buffer");
                }

                // Map to populate pointer
                reduceBufferCreated = true;
            }
            

            // Unmap buffer
            if (solver.getUseMapping()) {
                queue.enqueueUnmapMemObject(reduceBuffer, static_cast<void *>(reduceVec.data()));
            }
        }
        reduceBufferOnDevice = true;
    }
    
    /**
    * Map OpenCL status buffer to host, this gives exclusive access to the host
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapStatusBuffer(cl::CommandQueue &queue) {
        auto &solver = Geostack::Solver::getSolver();
        if (solver.getUseMapping()) {
            void *pb = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, sizeof(cl_uint));
            if (pb != static_cast<void *>(&status)) {
                throw std::range_error("Mapped pointer differs for status buffer");
            }
        } else {
            queue.enqueueReadBuffer(statusBuffer, CL_TRUE, 0, sizeof(cl_uint), static_cast<void *>(&status));
        }
        statusBufferOnDevice = false;
    }

    /**
    * Unmap OpenCL status buffer from host, this gives exclusive access to the device
    */    
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::unmapStatusBuffer(cl::CommandQueue &queue) {
        if (!statusBufferOnDevice) {

            auto &solver = Geostack::Solver::getSolver();

            // Create buffer if needed
            if (!statusBufferCreated) {

                // Create buffer
                auto &context = solver.getContext();
                cl_int err = CL_SUCCESS;
                if (solver.getUseMapping()) {
                    statusBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), 
                        static_cast<void *>(&status), &err);
                    void *pb = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_WRITE_INVALIDATE_REGION, 0, sizeof(cl_uint));
                    if (pb != static_cast<void *>(&status)) {
                        throw std::range_error("Mapped pointer differs for status buffer");
                    }
                } else {
                    statusBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, sizeof(cl_uint), NULL, &err);
                }

                // Map to populate pointer
                statusBufferCreated = true;
            }

            // Unmap buffer
            if (solver.getUseMapping()) {
                queue.enqueueUnmapMemObject(statusBuffer, static_cast<void *>(&status));
            } else {
                queue.enqueueWriteBuffer(statusBuffer, CL_TRUE, 0, sizeof(cl_uint), static_cast<void *>(&status));
            }
            
        }
        statusBufferOnDevice = true;
    }

    /**
    * Map 2D %Vector distances into %Tile
    * @param v the %Vector to map.
    * @param clHash the program hash
    * @param rproj the raster projection hash
    * @param geometryTypes the types of geometry to map
    * @param fields list of property fields
    * @param useAll flag to use all vector elements
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::mapVector(Vector<CTYPE> &v, std::size_t clHash, ProjectionParameters<double> rproj, 
        size_t geometryTypes, std::vector<std::string> fields, bool useAll) {

        // Check geometry types
        if (!(geometryTypes & (GeometryType::Point | GeometryType::LineString | GeometryType::Polygon))) {
            throw std::runtime_error("No requested geometry types to map");
        }

        try { 
            
            // Get OpenCL handles
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all z and time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, std::numeric_limits<CTYPE>::lowest(), std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, std::numeric_limits<CTYPE>::max(), std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }

            // Get geometry nearest to tile
            auto vTile = useAll ? v : v.nearest(b, geometryTypes);

            // Map points
            if (geometryTypes & GeometryType::Point) {            
                
                // Get points
                const auto &pointIndexes = vTile.getPointIndexes();
                    
                // Check size
                if (pointIndexes.size() > 0) {
                
                    // Point data
                    std::vector<CTYPE> cx;
                    std::vector<CTYPE> cy;
                    
                    // Build coordinate list
                    uint32_t nCoords = 0;
                    for (auto &p : pointIndexes) {
                        const auto c = vTile.getPointCoordinate(p);
                        cx.push_back(c.p);
                        cy.push_back(c.q);
                        nCoords++;
                    }

                    // Create buffers
                    AutoBuffer<CTYPE> cxBuffer(context, queue, cx);
                    AutoBuffer<CTYPE> cyBuffer(context, queue, cy);
                    AutoBuffer<uint32_t> indexBuffer(context, queue, pointIndexes);

                    // Populate points kernel arguments
                    cl_uint arg = 0;
                    auto map2DPointDistanceKernel = solver.getKernel(clHash, "map2DPointDistance");
                    map2DPointDistanceKernel.setArg(arg++, getDataBuffer());
                    map2DPointDistanceKernel.setArg(arg++, dim.d);
                    map2DPointDistanceKernel.setArg(arg++, cxBuffer.getBuffer());
                    map2DPointDistanceKernel.setArg(arg++, cyBuffer.getBuffer());
                    map2DPointDistanceKernel.setArg(arg++, indexBuffer.getBuffer());
                    map2DPointDistanceKernel.setArg(arg++, nCoords); 
                    if (usingProjection) {
                        map2DPointDistanceKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        map2DPointDistanceKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Add field buffers
                    for (auto &n : fields) {
                        map2DPointDistanceKernel.setArg(arg++, v.getPropertyBuffer(n));
                    }

                    // Execute points kernel
                    queue.enqueueNDRangeKernel(map2DPointDistanceKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, dim.d.ny, dim.d.nz), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                }
            }
                
            // Map line strings
            if (geometryTypes & GeometryType::LineString) {
                
                // Get line strings
                const auto &lineStringIndexes = vTile.getLineStringIndexes();
                    
                // Check size
                if (lineStringIndexes.size() > 0) {
            
                    // Line string data
                    std::vector<CTYPE> cx;
                    std::vector<CTYPE> cy;
                    std::vector<uint32_t> offset; // Offset vector
                    std::vector<uint32_t> level;  // Geometry level vector
                    
                    // Build coordinate list
                    uint32_t nCoords = 0;
                    for (auto &l : lineStringIndexes) {

                        // Get line string coordinates
                        auto lc = vTile.getLineStringCoordinates(l);

                        // Append coordinates
                        for (auto &c : lc) {
                            cx.push_back(c.p);
                            cy.push_back(c.q);
                            nCoords++;
                        }

                        // Update offset list
                        offset.push_back(nCoords-1);
                    }

                    // Create buffers
                    AutoBuffer<CTYPE> cxBuffer(context, queue, cx);
                    AutoBuffer<CTYPE> cyBuffer(context, queue, cy);
                    AutoBuffer<uint32_t> indexBuffer(context, queue, lineStringIndexes);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);
                
                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    auto map2DLineStringDistanceKernel = solver.getKernel(clHash, "map2DLineStringDistance");
                    map2DLineStringDistanceKernel.setArg(arg++, getDataBuffer());
                    map2DLineStringDistanceKernel.setArg(arg++, dim.d);
                    map2DLineStringDistanceKernel.setArg(arg++, cxBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(arg++, cyBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(arg++, indexBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(arg++, offsetBuffer.getBuffer());
                    map2DLineStringDistanceKernel.setArg(arg++, (cl_uint)offset.size());
                    if (usingProjection) {
                        map2DLineStringDistanceKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        map2DLineStringDistanceKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }                    

                    // Add field buffers
                    for (auto &n : fields) {
                        map2DLineStringDistanceKernel.setArg(arg++, v.getPropertyBuffer(n));
                    }

                    // Execute line string kernel
                    queue.enqueueNDRangeKernel(map2DLineStringDistanceKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, dim.d.ny, dim.d.nz), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                }
            }

            // Map polygons
            if (geometryTypes & GeometryType::Polygon) {            

                // Get polygon indexes
                const auto &polygonIndexes = vTile.getPolygonIndexes();
                    
                // Check size
                if (polygonIndexes.size() > 0) {
                
                    // Polygon data
                    std::vector<CTYPE> cx;
                    std::vector<CTYPE> cy;
                    std::vector<CTYPE> bx;
                    std::vector<CTYPE> by;
                    std::vector<uint32_t> indexes;  // Geometry property index
                    std::vector<uint32_t> offset; // Offset vector

                    // Build coordinate list
                    uint32_t nCoords = 0;
                    for (auto &p : polygonIndexes) {

                        // Get polygon coordinates
                        auto pc = vTile.getPolygonCoordinates(p);

                        // Get polygon sub-indexes
                        uint32_t currentOffset = nCoords;
                        for (const auto &subIndex : vTile.getPolygonSubIndexes(p)) {
                            currentOffset += (uint32_t)subIndex;
                            offset.push_back(currentOffset-1);
                            indexes.push_back(p);
                        }

                        // Append coordinates
                        for (auto &c : pc) {
                            cx.push_back(c.p);
                            cy.push_back(c.q);
                            nCoords++;
                        }

                        // Append bounds
                        for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                            bx.push_back(subBounds.min.p);
                            by.push_back(subBounds.min.q);
                            bx.push_back(subBounds.max.p);
                            by.push_back(subBounds.max.q);
                        }
                    }

                    // Create buffers
                    AutoBuffer<CTYPE> cxBuffer(context, queue, cx);
                    AutoBuffer<CTYPE> cyBuffer(context, queue, cy);
                    AutoBuffer<CTYPE> bxBuffer(context, queue, bx);
                    AutoBuffer<CTYPE> byBuffer(context, queue, by);
                    AutoBuffer<uint32_t> indexBuffer(context, queue, indexes);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);

                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    auto map2DPolygonDistanceKernel = solver.getKernel(clHash, "map2DPolygonDistance");
                    map2DPolygonDistanceKernel.setArg(arg++, getDataBuffer());
                    map2DPolygonDistanceKernel.setArg(arg++, dim.d);
                    map2DPolygonDistanceKernel.setArg(arg++, cxBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(arg++, cyBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(arg++, indexBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(arg++, offsetBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(arg++, bxBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(arg++, byBuffer.getBuffer());
                    map2DPolygonDistanceKernel.setArg(arg++, (cl_uint)offset.size());
                    if (usingProjection) {
                        map2DPolygonDistanceKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        map2DPolygonDistanceKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }                  

                    // Add field buffers
                    for (auto &n : fields) {
                        map2DPolygonDistanceKernel.setArg(arg++, v.getPropertyBuffer(n));
                    }

                    // Execute line string kernel
                    queue.enqueueNDRangeKernel(map2DPolygonDistanceKernel, 
                        cl::NullRange,
                        cl::NDRange(TileMetrics::tileSize, dim.d.ny, dim.d.nz), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));

                }
            }

            // Flush queue
            queue.flush();

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Rasterise %Vector onto %Tile
    * @param v the %Vector to map.
    * @param geometryTypes the types of geometry to map
    * @param script script to run for rasterisation
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::rasterise(Vector<CTYPE> &v, std::size_t clHash, 
        std::vector<std::string> fields, ProjectionParameters<double> rproj, size_t parameters) {

        try { 
            
            // Get OpenCL handles
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, 0.0, std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, 0.0, std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }
            
            // Get geometry within tile
            Vector<CTYPE> vTile = v.region(b, parameters);

            // Rasterise points
            if (parameters & GeometryType::Point) {
            
                // Point data
                CoordinateList<CTYPE> coords; // Coordinate vector
                
                // Get points
                const auto &pointIndexes = vTile.getPointIndexes();
                    
                // Check size
                if (pointIndexes.size() > 0) {

                    // Build coordinate list
                    for (auto &p : pointIndexes) {
                        coords.push_back(vTile.getPointCoordinate(p));
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> indexBuffer(context, queue, pointIndexes);

                    // Get kernel
                    auto rasterise2DPointKernel = solver.getKernel(clHash, "rasterise2DPoint"); 

                    // Populate points kernel arguments
                    cl_uint arg = 0;
                    rasterise2DPointKernel.setArg(arg++, getDataBuffer());
                    rasterise2DPointKernel.setArg(arg++, getDimensions());
                    rasterise2DPointKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DPointKernel.setArg(arg++, indexBuffer.getBuffer());
                    rasterise2DPointKernel.setArg(arg++, (cl_uint)coords.size());     
                    if (usingProjection) {
                        rasterise2DPointKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        rasterise2DPointKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Add field buffers
                    for (auto &n : fields) {
                        rasterise2DPointKernel.setArg(arg++, v.getPropertyBuffer(n));
                    }
                    
                    // Execute point kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterise2DPointKernel);
                    clPaddedWorkgroupSize = (size_t)(coords.size()+((clWorkgroupSize-(coords.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(rasterise2DPointKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));
                }
            }
                
            // Rasterise line strings
            if (parameters & GeometryType::LineString) {
            
                // Line string data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<uint32_t> offset; // Offset vector
                
                // Get line strings
                const auto &lineStringIndexes = vTile.getLineStringIndexes();
                    
                // Check size
                if (lineStringIndexes.size() > 0) {
                    
                    // Build coordinate list
                    for (auto &l : lineStringIndexes) {

                        // Get line string coordinates
                        auto lc = vTile.getLineStringCoordinates(l);

                        // Append coordinates
                        coords.insert(coords.end(), lc.begin(), lc.end());

                        // Update offset list
                        offset.push_back((uint32_t)coords.size()-1);
                    }

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> indexBuffer(context, queue, lineStringIndexes);
                    AutoBuffer<uint32_t> offsetBuffer(context, queue, offset);

                    // Get kernel
                    auto rasterise2DLineStringKernel = solver.getKernel(clHash, "rasterise2DLineString"); 
                
                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    rasterise2DLineStringKernel.setArg(arg++, getDataBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, getDimensions());
                    rasterise2DLineStringKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, indexBuffer.getBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, offsetBuffer.getBuffer());
                    rasterise2DLineStringKernel.setArg(arg++, (cl_uint)offset.size());
                    if (usingProjection) {
                        rasterise2DLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        rasterise2DLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Add field buffers
                    for (auto &n : fields) {
                        rasterise2DLineStringKernel.setArg(arg++, v.getPropertyBuffer(n));
                    }

                    // Execute line string kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterise2DLineStringKernel);
                    clPaddedWorkgroupSize = (size_t)(offset.size()+((clWorkgroupSize-(offset.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(rasterise2DLineStringKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));
                }
            }
            
            // Rasterise polygons
            if (parameters & GeometryType::Polygon) {
            
                // Polygon data
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<int32_t> offset;  // Offset vector
                CoordinateList<CTYPE> bounds; // Bounding box vector
                std::vector<uint32_t> indexes;  // Geometry property index

                // Get polygon indexes
                const auto &polygonIndexes = vTile.getPolygonIndexes();
                    
                // Check size
                if (polygonIndexes.size() > 0) {

                    // Build geometry list
                    for (auto &p : polygonIndexes) {
                    
                        // Get polygon coordinates
                        auto pc = vTile.getPolygonCoordinates(p);

                        // Append coordinates
                        coords.insert(coords.end(), pc.begin(), pc.end());

                        // Get polygon sub-indexes
                        auto &si = vTile.getPolygonSubIndexes(p);

                        // Add sub-indexes, sub-polygons are marked as negative
                        offset.push_back((int32_t)si[0]);
                        for (cl_uint s = 1; s < si.size(); s++) {
                            offset.push_back(-(int32_t)si[s]);
                        }

                        // Append bounds
                        for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                            bounds.push_back(subBounds.min);
                            bounds.push_back(subBounds.max);
                        }

                        // Add index
                        indexes.push_back(p);
                    }

                    // Add terminator to offset
                    offset.push_back(0);

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<uint32_t> indexBuffer(context, queue, indexes);
                    AutoBuffer<int32_t> offsetBuffer(context, queue, offset);
                    AutoBuffer<Coordinate<CTYPE> > boundsBuffer(context, queue, bounds);

                    // Get kernel
                    auto rasterise2DPolygonKernel = solver.getKernel(clHash, "rasterise2DPolygon");

                    // Check local allocation
                    cl_uint localCacheSize = TileMetrics::workgroupSize*dim.d.nz*sizeof(REAL);
                    if (localCacheSize > solver.getLocalMemorySizeBytes()) {
                        std::stringstream err;
                        err << "Insufficient local memory (" << solver.getLocalMemorySizeBytes() << " B) required for rasterisation (" << localCacheSize << " B)";
                        throw std::runtime_error(err.str());
                    }
                    
                    // Create indexing parameters
                    cl_uchar index_parameters = 0;
                    if (static_cast<VectorIndexingOptions::Type>(parameters & 0x300) == VectorIndexingOptions::All) {
                        index_parameters = 3;
                    } else if (static_cast<VectorIndexingOptions::Type>(parameters & 0x300) == VectorIndexingOptions::Interior) {
                        index_parameters = 1;
                    } else if (static_cast<VectorIndexingOptions::Type>(parameters & 0x300) == VectorIndexingOptions::Edges) {
                        index_parameters = 2;
                    }

                    // Populate line string kernel arguments
                    cl_uint arg = 0;
                    rasterise2DPolygonKernel.setArg(arg++, getDataBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, getDimensions());
                    rasterise2DPolygonKernel.setArg(arg++, coordsBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, indexBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, offsetBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, boundsBuffer.getBuffer());
                    rasterise2DPolygonKernel.setArg(arg++, index_parameters);
                    rasterise2DPolygonKernel.setArg(arg++, localCacheSize, NULL);
                    rasterise2DPolygonKernel.setArg(arg++, (cl_uint)(offset.size()-1));
                    if (usingProjection) {
                        rasterise2DPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        rasterise2DPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }

                    // Add field buffers
                    for (auto &n : fields) {
                        rasterise2DPolygonKernel.setArg(arg++, v.getPropertyBuffer(n));
                    }

                    // Execute polygon kernel
                    queue.enqueueNDRangeKernel(rasterise2DPolygonKernel, 
                        cl::NullRange, 
                        cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, 1), 
                        cl::NDRange(TileMetrics::workgroupSize, 1, 1));
                }
            }

            // Flush queue
            queue.flush();

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Index %Vector geometry and weighting from %Tile
    * @param v the %Vector to map.
    * @param rasterIndexes reference to vector of %RasterIndex
    * @param clHash hash of OpenCL program
    * @param rproj %Tile projection
    * @param parameters indexing parameters, bit 1 = polygon intersections, bit 2 = polygon interior
    */
    template <typename RTYPE, typename CTYPE>
    void Tile<RTYPE, CTYPE>::index(Vector<CTYPE> &v, std::vector<RasterIndex<CTYPE> > &rasterIndexes,
        std::size_t clHash, ProjectionParameters<double> rproj, cl_uchar parameters) {

        try { 

            // Get OpenCL handles
            auto &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;

            // Get projection
            auto vproj = v.getProjectionParameters();
            bool usingProjection = (rproj != vproj);

            // Get bounds and expand to cover all time
            BoundingBox<CTYPE> b({ 
                { dim.d.ox, dim.d.oy, 0.0, std::numeric_limits<CTYPE>::lowest() }, 
                { dim.ex, dim.ey, 0.0, std::numeric_limits<CTYPE>::max() } } );

            // Reproject bounds
            if (usingProjection) {
                b = b.convert(vproj, rproj);
            }
            
            // Get geometry within tile
            Vector<CTYPE> vTile = v.region(b);

            // Create raster indexes
            std::size_t rasterIndexTotalSize = rasterIndexes.size();

            // Get points
            const auto &pointIndexes = vTile.getPointIndexes();
            if (pointIndexes.size() > 0) {

                // Create array index buffer
                cl_uint globalIndex = 0;
                cl::Buffer globalIndexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &globalIndex);
                void *globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                queue.enqueueUnmapMemObject(globalIndexBuffer, globalIndexBufferPtr);

                // Build coordinate list
                CoordinateList<CTYPE> coords; // Coordinate vector
                for (auto &p : pointIndexes) {
                    coords.push_back(vTile.getPointCoordinate(p));
                }

                // Update index vector
                std::size_t cellCount = pointIndexes.size();
                rasterIndexes.resize(rasterIndexTotalSize+cellCount);
                auto pRasterIndex = rasterIndexes.data()+rasterIndexTotalSize;
                
                // Create raster index buffer
                auto rasterIndexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                    cellCount*sizeof(RasterIndex<CTYPE>), pRasterIndex);

                void *rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                queue.enqueueUnmapMemObject(rasterIndexBuffer, rasterIndexBufferPtr);

                // Create buffers
                AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                AutoBuffer<cl_uint> geometryIndexBuffer(context, queue, pointIndexes);

                // Get kernel
                auto indexPointsKernel = solver.getKernel(clHash, "indexPoints"); 

                // Populate points kernel arguments
                cl_uint arg = 0;
                indexPointsKernel.setArg(arg++, getDataBuffer());
                indexPointsKernel.setArg(arg++, getDimensions());
                indexPointsKernel.setArg(arg++, coordsBuffer.getBuffer());
                indexPointsKernel.setArg(arg++, geometryIndexBuffer.getBuffer());
                indexPointsKernel.setArg(arg++, rasterIndexBuffer);
                indexPointsKernel.setArg(arg++, globalIndexBuffer);
                indexPointsKernel.setArg(arg++, (cl_uint)pointIndexes.size());
                if (usingProjection) {
                    indexPointsKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                    indexPointsKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                }
                    
                // Execute point kernel
                clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(indexPointsKernel);
                clPaddedWorkgroupSize = (size_t)(pointIndexes.size()+((clWorkgroupSize-(pointIndexes.size()%clWorkgroupSize))%clWorkgroupSize));
                queue.enqueueNDRangeKernel(indexPointsKernel, cl::NullRange, 
                    cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                // Map index data
                rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, pointIndexes.size()*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");        
                    
                // Get linestring cell count
                globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                // Adjust vector
                rasterIndexes.resize(rasterIndexTotalSize+globalIndex);
                rasterIndexTotalSize += globalIndex;
            }

            // Get line strings
            const auto &lineStringIndexes = vTile.getLineStringIndexes();
            if (lineStringIndexes.size() > 0) {
            
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<uint32_t> offset; // Offset vector

                // Build coordinate list
                CTYPE length = 0.0;
                for (auto &l : lineStringIndexes) {

                    // Get line string coordinates
                    auto lc = vTile.getLineStringCoordinates(l);

                    // Calculate length
                    for (std::size_t i = 0; i < lc.size()-1; i++) {
                        length += std::hypot(
                            1.0+fabs((lc[i].p-lc[i+1].p)/dim.d.hx), 1.0+fabs((lc[i].q-lc[i+1].q)/dim.d.hy));
                    }
                    length += std::hypot(
                        1.0+fabs((lc[lc.size()-1].p-lc[0].p)/dim.d.hx), 1.0+fabs((lc[lc.size()-1].q-lc[0].q)/dim.d.hy));

                    // Append coordinates
                    coords.insert(coords.end(), lc.begin(), lc.end());

                    // Update offset list
                    offset.push_back((uint32_t)coords.size()-1);
                }
                
                // Index cells
                uint32_t cellCount = (uint32_t)length;
                if (cellCount > 0) {

                    // Create array index buffer
                    cl_uint globalIndex = 0;
                    cl::Buffer globalIndexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &globalIndex);
                    void *globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                    if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                    queue.enqueueUnmapMemObject(globalIndexBuffer, globalIndexBufferPtr);

                    // Create buffers
                    AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                    AutoBuffer<cl_uint> offsetBuffer(context, queue, offset);
                    AutoBuffer<cl_uint> geometryIndexBuffer(context, queue, lineStringIndexes);
                    
                    // Update index vector
                    rasterIndexes.resize(rasterIndexTotalSize+cellCount);
                    auto pRasterIndex = rasterIndexes.data()+rasterIndexTotalSize;
                
                    // Create raster index buffer
                    auto rasterIndexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                        cellCount*sizeof(RasterIndex<CTYPE>), pRasterIndex);
                    void *rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                    if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                    queue.enqueueUnmapMemObject(rasterIndexBuffer, rasterIndexBufferPtr);     
                
                    // Get line string index kernel
                    auto indexLineStringKernel = solver.getKernel(clHash, "indexLineStrings");

                    cl_uint arg = 0;
                    indexLineStringKernel.setArg(arg++, getDataBuffer());
                    indexLineStringKernel.setArg(arg++, getDimensions());
                    indexLineStringKernel.setArg(arg++, coordsBuffer.getBuffer());
                    indexLineStringKernel.setArg(arg++, offsetBuffer.getBuffer());
                    indexLineStringKernel.setArg(arg++, geometryIndexBuffer.getBuffer());
                    indexLineStringKernel.setArg(arg++, rasterIndexBuffer);
                    indexLineStringKernel.setArg(arg++, globalIndexBuffer);
                    indexLineStringKernel.setArg(arg++, (cl_uint)offset.size());  
                    if (usingProjection) {
                        indexLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                        indexLineStringKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                    }                
                
                    // Get optimal workgroup size
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(indexLineStringKernel);
                    clPaddedWorkgroupSize = (size_t)(offset.size()+((clWorkgroupSize-(offset.size()%clWorkgroupSize))%clWorkgroupSize));

                    // Execute line string index kernel
                    queue.enqueueNDRangeKernel(indexLineStringKernel, cl::NullRange, 
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));     

                    // Map index data
                    rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                    if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");         
                    
                    // Get linestring cell count
                    globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                    if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                    // Adjust vector
                    rasterIndexes.resize(rasterIndexTotalSize+globalIndex);
                    rasterIndexTotalSize += globalIndex;
                }
            }

            // Get polygon indexes
            const auto &polygonIndexes = vTile.getPolygonIndexes();
            if (polygonIndexes.size() > 0) {
            
                CoordinateList<CTYPE> coords; // Coordinate vector
                std::vector<int32_t> offset;  // Offset vector
                CoordinateList<CTYPE> bounds; // Bounding box vector
                uint32_t cellCount = 0;       // Number of cells for current geometry

                // Build geometry list
                CTYPE area = 0.0;
                for (auto &p : polygonIndexes) {
                    
                    // Get polygon coordinates
                    auto pc = vTile.getPolygonCoordinates(p);

                    // Append coordinates
                    coords.insert(coords.end(), pc.begin(), pc.end());

                    // Get polygon sub-indexes
                    auto &si = vTile.getPolygonSubIndexes(p);

                    // Add sub-indexes, sub-polygons are marked as negative
                    offset.push_back((int32_t)si[0]);
                    for (cl_uint s = 1; s < si.size(); s++) {
                        offset.push_back(-(int32_t)si[s]);
                    }

                    // Append bounds
                    for (const auto &subBounds : vTile.getPolygonSubBounds(p)) {
                        bounds.push_back(subBounds.min);
                        bounds.push_back(subBounds.max);
                    }
                    
                    // Estimate area from bounds
                    auto pb = vTile.getGeometry(p)->getBounds();
                    pb.extend2D(std::max(dim.d.hx, dim.d.hy));
                    pb = pb.intersect(b);

                    if (usingProjection) {

                        // Reproject bounds
                        pb = pb.convert(rproj, vproj);
                    }

                    // Calculate area in cell units
                    CTYPE pArea = std::ceil(pb.area2D())/(dim.d.hx*dim.d.hy);

                    pArea = std::min(pArea, (CTYPE)TileMetrics::tileSizeSquared);
                    area += pArea;
                }
                cellCount = 1+(uint32_t)(area);

                // Add terminator to offset
                offset.push_back(0);

                // Create array index buffer
                cl_uint globalIndex = 0;
                cl::Buffer globalIndexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, sizeof(cl_uint), &globalIndex);
                void *globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                queue.enqueueUnmapMemObject(globalIndexBuffer, globalIndexBufferPtr);

                // Create buffers
                AutoBuffer<Coordinate<CTYPE> > coordsBuffer(context, queue, coords);
                AutoBuffer<int32_t> offsetBuffer(context, queue, offset);
                AutoBuffer<Coordinate<CTYPE> > boundsBuffer(context, queue, bounds);
                AutoBuffer<cl_uint> geometryIndexBuffer(context, queue, polygonIndexes);                
                
                // Update index vector
                rasterIndexes.resize(rasterIndexTotalSize+cellCount);
                auto pRasterIndex = rasterIndexes.data()+rasterIndexTotalSize;
                
                // Create raster index buffer
                auto rasterIndexBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                    cellCount*sizeof(RasterIndex<CTYPE>), pRasterIndex);
                void *rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                queue.enqueueUnmapMemObject(rasterIndexBuffer, rasterIndexBufferPtr); 

                // Get polygon index kernel
                auto indexPolygonKernel = solver.getKernel(clHash, "indexPolygons"); 

                // Populate polygon kernel arguments
                cl_uint arg = 0;
                indexPolygonKernel.setArg(arg++, getDataBuffer());
                indexPolygonKernel.setArg(arg++, getDimensions());
                indexPolygonKernel.setArg(arg++, coordsBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, offsetBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, boundsBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, geometryIndexBuffer.getBuffer());
                indexPolygonKernel.setArg(arg++, rasterIndexBuffer);
                indexPolygonKernel.setArg(arg++, globalIndexBuffer);
                indexPolygonKernel.setArg(arg++, (cl_uchar)parameters);
                indexPolygonKernel.setArg(arg++, (cl_uint)(offset.size()-1));
                if (usingProjection) {
                    indexPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(rproj));
                    indexPolygonKernel.setArg(arg++, ProjectionParameters<CTYPE>(vproj));
                }

                // Execute polygon count kernel
                queue.enqueueNDRangeKernel(indexPolygonKernel,  
                    cl::NullRange, 
                    cl::NDRange(TileMetrics::tileSize, TileMetrics::tileSize, 1), 
                    cl::NDRange(TileMetrics::workgroupSize, 1, 1));

                // Map index data
                rasterIndexBufferPtr = queue.enqueueMapBuffer(rasterIndexBuffer, CL_TRUE, CL_MAP_READ, 0, cellCount*sizeof(RasterIndex<CTYPE>));
                if (rasterIndexBufferPtr != static_cast<void *>(pRasterIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                        
                // Get polygon cell count
                globalIndexBufferPtr = queue.enqueueMapBuffer(globalIndexBuffer, CL_TRUE, CL_MAP_WRITE, 0, sizeof(cl_uint));
                if (globalIndexBufferPtr != static_cast<void *>(&globalIndex))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                // Adjust vector
                rasterIndexes.resize(rasterIndexTotalSize+globalIndex);
                rasterIndexTotalSize += globalIndex;
            }

            // Flush queue
            queue.flush(); 

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }
    
    // CTYPE float definitions
    template bool equalSpatialMetrics2D(const Dimensions<float> l, const Dimensions<float> r);
    template bool equalSpatialMetrics(const Dimensions<float> l, const Dimensions<float> r);
    
    // CTYPE double definitions
    template bool equalSpatialMetrics2D(const Dimensions<double> l, const Dimensions<double> r);
    template bool equalSpatialMetrics(const Dimensions<double> l, const Dimensions<double> r);

    // RTYPE float, CTYPE float definitions
#ifdef USE_TILE_CACHING
    template class TileCacheManager<float, float>;
#endif
    template class Tile<float, float>;
    template bool tileDataEqual(Tile<float, float> &, Tile<float, float> &);
    
    // RTYPE uint, CTYPE float definitions
#ifdef USE_TILE_CACHING
    template class TileCacheManager<uint32_t, float>;
#endif
    template class Tile<uint32_t, float>;
    template bool tileDataEqual(Tile<uint32_t, float> &, Tile<uint32_t, float> &);

    // RTYPE uchar, CTYPE float definitions
#ifdef USE_TILE_CACHING
    template class TileCacheManager<uint8_t, float>;
#endif
    template class Tile<uint8_t, float>;
    template bool tileDataEqual(Tile<uint8_t, float> &, Tile<uint8_t, float> &);
    
    // RTYPE double, CTYPE double definitions
#ifdef USE_TILE_CACHING
    template class TileCacheManager<double, double>;
#endif
    template class Tile<double, double>;
    template bool tileDataEqual(Tile<double, double> &, Tile<double, double> &);

    // RTYPE uint, CTYPE double definitions
#ifdef USE_TILE_CACHING
    template class TileCacheManager<uint32_t, double>;
#endif
    template class Tile<uint32_t, double>;
    template bool tileDataEqual(Tile<uint32_t, double> &, Tile<uint32_t, double> &);

    // RTYPE uchar, CTYPE double definitions
#ifdef USE_TILE_CACHING
    template class TileCacheManager<uint8_t, double>;
#endif
    template class Tile<uint8_t, double>;
    template bool tileDataEqual(Tile<uint8_t, double> &, Tile<uint8_t, double> &);
}
