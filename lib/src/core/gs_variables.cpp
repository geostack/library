/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <set>
#include <regex>
#include <sstream>
#include <iostream>

#include "gs_models.h"
#include "gs_geometry.h"
#include "gs_variables.h"

namespace Geostack
{
    /**
    * Default %Variables constructor
    */
    template <typename RTYPE, typename KTYPE>
    Variables<RTYPE, KTYPE>::Variables():
        bufferOnDevice(false),
        dataChanged(true) {
    }

    /**
    * %Variables Destructor
    */
    template <typename RTYPE, typename KTYPE>
    Variables<RTYPE, KTYPE>::~Variables() {
    }

    /**
    * %Variables copy constructor
    */
    template <typename RTYPE, typename KTYPE>
    Variables<RTYPE, KTYPE>::Variables(Variables<RTYPE, KTYPE> const &var):
        bufferOnDevice(false),
        dataChanged(true) {

        // Copy data
        dataMap = var.dataMap;

        // Refresh data
        refreshData();
    }
    
    /**
    * %Variables assigment operator
    */
    template <typename RTYPE, typename KTYPE>
    Variables<RTYPE, KTYPE> &Variables<RTYPE, KTYPE>::operator=(const Variables<RTYPE, KTYPE> &var) {

        if (this != &var) {

            // Clear data
            clear();

            // Copy data
            dataMap = var.dataMap;

            // Refresh data
            refreshData();
        }

        return *this;
    }

    /**
    * Clear %Variables data
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::clear() {
        ensureDataOnHost();
        dataIndexes.clear();
        dataLengths.clear();
        std::vector<RTYPE>().swap(dataVec);
        bufferOnDevice = false;
        dataChanged = true;
        dataMap.clear();
    }

    /**
    * Set variable value in map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::set(KTYPE key, RTYPE value) {

        // Ensure data is on host
        ensureDataOnHost();

        // Find key
        auto dit = dataMap.find(key);
        if (dit == dataMap.end()) {
            dit = dataMap.insert(std::make_pair(key, std::vector<RTYPE>(1))).first;
        }

        // Set value
        auto &mapVec = dit->second;
        mapVec[0] = value;
        dataChanged = true;
    }

    /**
    * Set variable value in map at index
    * @param name the variable name.
    * @param value the variable value to set.
    * @param index the array index to set.
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::set(KTYPE key, RTYPE value, std::size_t index) {

        // Ensure data is on host
        ensureDataOnHost();

        // Find key
        auto dit = dataMap.find(key);
        if (dit == dataMap.end()) {
            dit = dataMap.insert(std::make_pair(key, std::vector<RTYPE>(index+1))).first;
        }

        // Set value
        auto &mapVec = dit->second;
        if (index >= mapVec.size()) {
            mapVec.resize(index+1);
        }
        mapVec[index] = value;
        dataChanged = true;
    }

    /**
    * Get variable value from map
    * @param name the variable name.
    * @param value the variable value to set.
    */
    template <typename RTYPE, typename KTYPE>
    RTYPE Variables<RTYPE, KTYPE>::get(KTYPE key) {

        // Ensure data is on host
        ensureDataOnHost();

        // Find key
        auto dit = dataMap.find(key);
        if (dit == dataMap.end()) {
            return getNullValue<RTYPE>();
        }

        // Return value
        auto &mapVec = dit->second;
        return mapVec[0];
    }

    /**
    * Get variable value from map at index
    * @param name the variable name.
    * @param index the array index to get.
    */
    template <typename RTYPE, typename KTYPE>
    RTYPE Variables<RTYPE, KTYPE>::get(KTYPE key, std::size_t index) {

        // Ensure data is on host
        ensureDataOnHost();

        // Find key
        auto dit = dataMap.find(key);
        if (dit == dataMap.end()) {
            return getNullValue<RTYPE>();
        }

        // Return value
        auto &mapVec = dit->second;
        return mapVec.at(index);
    }

    /**
    * Get variable map indexes
    * @return map of names and offsets.
    */
    template <typename RTYPE, typename KTYPE>
    const std::map<KTYPE, std::size_t> &Variables<RTYPE, KTYPE>::getIndexes() {

        // Copy data from map to vector
        refreshData();

        // Return map
        return dataIndexes;
    }

    /**
    * Get variable array size
    * @param name the variable name.
    * @return size of the array.
    */
    template <typename RTYPE, typename KTYPE>
    std::size_t Variables<RTYPE, KTYPE>::getSize(KTYPE key) {

        // Ensure data is on host
        ensureDataOnHost();

        // Find key
        auto dit = dataMap.find(key);
        if (dit == dataMap.end()) {
            return 0;
        }

        // Return value
        auto &mapVec = dit->second;
        return mapVec.size();
    }

    /**
     * @brief Check if variable is valid
     * @param name the variable name
     * @return true is variable is valid, false otherwise
     */
    template <typename RTYPE, typename KTYPE>
    bool Variables<RTYPE, KTYPE>::hasVariable(KTYPE key) {

        // Find key
        auto dit = dataMap.find(key);
        if (dit == dataMap.end()) {
            return false;
        }
        return true;
    }

    /**
    * Map OpenCL variable buffer to host, this gives exclusive access to the host
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::mapBuffer(cl::CommandQueue &queue) {

        if (hasData()) {

            // Map buffer
            if (bufferOnDevice) {
                auto &solver = Geostack::Solver::getSolver();
                if (solver.getUseMapping()) {
                    void *pb = queue.enqueueMapBuffer(buffer, CL_TRUE, CL_MAP_WRITE, 0, dataVec.size()*sizeof(RTYPE));
                    if (pb != static_cast<void *>(dataVec.data())) {
                        throw std::range_error("Mapped pointer differs for data buffer");
                    }
                } else {
                    queue.enqueueReadBuffer(buffer, CL_TRUE, 0, dataVec.size()*sizeof(RTYPE), 
                        static_cast<void *>(dataVec.data()));
                }
            }
            bufferOnDevice = false;
        }
    }

    /**
    * Unmap OpenCL variable buffer from host, this gives exclusive access to the device
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::unmapBuffer(cl::CommandQueue &queue) {

        if (hasData()) {

            // Unmap buffer
            if (!bufferOnDevice) {            
                auto &solver = Geostack::Solver::getSolver();
                if (solver.getUseMapping()) {
                    queue.enqueueUnmapMemObject(buffer, dataVec.data());
                } else {
                    queue.enqueueWriteBuffer(buffer, CL_FALSE, 0, dataVec.size()*sizeof(RTYPE), 
                        static_cast<void *>(dataVec.data()));
                }
            }
            bufferOnDevice = true;
        }
    }

    /**
    * Copy data from map to vector and create buffer
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::refreshData() {

        // Ensure data is on host
        ensureDataOnHost();

        // Check size
        bool refresh = false;
        std::vector<cl_uint> checkDataLengths;
        for (auto &dit : dataMap) {
            auto &mapVec = dit.second;
            checkDataLengths.push_back(mapVec.size());
        }
        if (dataLengths.size() != checkDataLengths.size()) {
            refresh = true;
        } else {
            for (std::size_t i = 0; i < dataLengths.size(); i++) {
                if (dataLengths[i] != checkDataLengths[i]) {
                    refresh = true;
                    break;
                }
            }
        }

        // Check data sizes
        if (refresh) {

            // Get solver handles
            cl_int err = CL_SUCCESS;
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            auto &context = solver.getContext();

            // Reset data
            dataIndexes.clear();
            dataVec.clear();

            // Copy data into vector and create indexes
            std::size_t i = 0;
            for (auto &dit : dataMap) {
                auto &mapVec = dit.second;
                dataVec.insert(dataVec.end(), mapVec.begin(), mapVec.end());
                dataIndexes[dit.first] = i;
                i+=mapVec.size();
            }
            
            // Create buffers
            dataLengths = checkDataLengths;
            buffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                dataVec.size()*sizeof(RTYPE), dataVec.data(), &err);
            if (err != CL_SUCCESS) {
                throw std::range_error("Cannot allocate memory for variable buffer");
            }

            // Initialise data buffer
            bufferOnDevice = true;
            mapBuffer(queue);

        } else if (dataChanged) {

            // Copy data into vector
            std::size_t currentIndex = 0;
            for (auto &dit : dataMap) {
                auto &mapVec = dit.second;
                currentIndex = dataIndexes[dit.first];
                std::copy(mapVec.begin(), mapVec.end(), dataVec.begin()+currentIndex);
            }
        }
    }

    /**
    * Ensure variable data is on the host
    */
    template <typename RTYPE, typename KTYPE>
    void Variables<RTYPE, KTYPE>::ensureDataOnHost() {

        // Check if data is on device
        if (bufferOnDevice) {

            // Map data from device to host
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapBuffer(queue);

            // Copy data into map
            std::size_t currentIndex = 0;
            for (auto &dit : dataMap) {
                auto &mapVec = dit.second;
                currentIndex = dataIndexes[dit.first];
                mapVec.assign(dataVec.begin()+currentIndex, dataVec.begin()+currentIndex+mapVec.size());
            }
            dataChanged = false;
        }
    }

    /**
    * Get %Variables buffer.
    * @return reference to %Variables buffer.
    */
    template <typename RTYPE, typename KTYPE>
    cl::Buffer &Variables<RTYPE, KTYPE>::getBuffer() {

        // Check data
        if (hasData()) {

            // Ensure reduction buffer is on device
            if (!bufferOnDevice) {

                // Get solver handles
                Geostack::Solver &solver = Geostack::Solver::getSolver();
                auto &queue = solver.getQueue();

                // Copy data from map to vector
                refreshData();

                // Map to device
                unmapBuffer(queue);
            }

            // Return reference to buffer
            return buffer;

        } else {

            // Throw exception if Variable has no data
            throw std::range_error("Variable has no data");
        }
    }

    /**
    * Get %Variables vector.
    * @return reference to %Variables vector.
    */
    template <typename RTYPE, typename KTYPE>
    const std::vector<RTYPE> &Variables<RTYPE, KTYPE>::getData() {

        // Copy data from map to vector
        refreshData();

        // Return vector
        return dataVec;
    }
    
    /**
    * Run script over %Variables
    * @param script script to run.
    */
    template <typename RTYPE>
    void runScriptImpl(Variables<RTYPE, std::string> &v, std::string script, std::string vin) {

        // Check for empty script
        if (script.length() == 0) {
            return;
        }

        // Check data
        if (v.hasData()) {

            try {

                Geostack::Solver &solver = Geostack::Solver::getSolver();
                auto &context = solver.getContext();
                auto &queue = solver.getQueue();

                if (solver.openCLInitialised()) {

                    // Copy data from map to vector
                    v.refreshData();

                    // Get variable size
                    std::size_t maxVarSize = 0;
                    if (vin.length() == 0) {

                        // Use maximum variable size
                        for (auto var : v.dataMap) {
                            auto varSize = var.second.size();
                            maxVarSize = std::max(maxVarSize, varSize);
                        }
                    } else {

                        // Use given variable size
                        auto it = v.dataMap.find(vin);
                        if (it == v.dataMap.end()) {
                            throw std::runtime_error("Cannot find variable'" + vin + "'");
                        } else {
                            maxVarSize = it->second.size();
                        }
                    }

                    // Create script hash
                    std::string scriptLookup;
                    for (auto var : v.dataMap) {
                        auto vSize = var.second.size();
                        if (vSize == maxVarSize) {
                            scriptLookup += var.first + "_N, ";
                        } else if (vSize == 1) {
                            scriptLookup += var.first + "_1, ";
                        }
                    }
                    scriptLookup += script;
                    scriptLookup += Models::cl_variables_block_c;
                    std::hash<std::string> hasher;
                    auto scriptHash = hasher(scriptLookup);

                    auto kernelGenerator = KernelCache::getKernelCache().getKernelGenerator(scriptHash);
                    if (kernelGenerator.programHash == Solver::getNullHash()) {

                        // Create variable strings
                        std::string argsList;
                        std::string variableListHi;
                        std::string variableListLow;
                        std::string post;                        

                        // Check for underscores in script, these are reserved for internal variables
                        if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                            throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                        }

                        // Parse script
                        script = Solver::processScript(script);

                        // Get type
                        auto vType = v.getOpenCLTypeString();

                        // Add to globals
                        argsList += ",\n__global " + vType + " *_vars\n";

                        // Loop over variables
                        std::size_t index = 0;
                        for (auto vi : v.dataIndexes) {
                            std::string vName = vi.first;                
                            auto vFindVar = std::regex("\\b" + vName + "\\b");
                            if (std::regex_search(script, vFindVar)) {

                                // Get variable size
                                auto varSize = v.dataMap[vName].size();

                                // Add variable to script
                                if (varSize == maxVarSize) {
                                    variableListHi += "const uint _offset_" + vName + " = *(_vo+" +std::to_string(index) + ");\n";
                                    variableListLow += vType + " " + 
                                        vName + " = *(_vars+_id+_offset_" + vName + ");\n";
                                    post += "*(_vars+_id+_offset_" + vName + ") = " + vName + ";\n";
                                } else if (varSize == 1) {
                                    variableListHi += "const uint _offset_" + vName + " = *(_vo+" +std::to_string(index) + ");\n";
                                    variableListLow += "const " + vType + " " + 
                                        vName + " = *(_vars+_offset_" + vName + ");\n";
                                }  else {
                                    if (vin.length() == 0) {
                                        throw std::runtime_error("Variable'" + vName + "' must be scalar or equal in size to other variables");
                                    } else {
                                        throw std::runtime_error("Variable'" + vName + "' must be equal in size to '" + vin + "'");
                                    }
                                }

                                // Add to used fields list
                                kernelGenerator.fieldNames.push_back(vName);
                                index++;
                            }
                        }

                        // Patch kernel template with code
                        auto kernelStr = Models::cl_variables_block_c;
                        kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), argsList);
                        kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableListHi + variableListLow);
                        kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);
                        kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__POST__\\*\\/"), post);
            
                        // Build kernel
                        kernelGenerator.programHash = solver.buildProgram(kernelStr);
                        if (kernelGenerator.programHash == solver.getNullHash()) {
                            throw std::runtime_error("Cannot build program");
                        }

                        // Register kernel in cache
                        KernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
                    }

                    // Build offsets
                    std::vector<cl_uint> offsets;
                    for (auto vName : kernelGenerator.fieldNames) {
                        offsets.push_back(v.dataIndexes[vName]);
                    }
                    auto offsetBuffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 
                        offsets.size()*sizeof(cl_uint), offsets.data());

                    // Set kernel arguments
                    auto &variableKernel = solver.getKernel(kernelGenerator.programHash, "variables");
                    variableKernel.setArg(0, (cl_uint)maxVarSize);
                    variableKernel.setArg(1, offsetBuffer);
                    variableKernel.setArg(2, v.getBuffer());

                    // Execute kernel
                    auto clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(variableKernel);
                    auto clPaddedWorkgroupSize = (size_t)(maxVarSize+((clWorkgroupSize-(maxVarSize%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(variableKernel, cl::NullRange,
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                } else {
                    throw std::runtime_error("OpenCL not initialised");
                }


            } catch (cl::Error e) {
                std::stringstream err;
                err << "OpenCL exception '" << e.what() << "': " << e.err();
                throw std::runtime_error(err.str());
            }

        } else {

            // Throw exception if Variable has no data
            throw std::range_error("Variable has no data");
        }
    }
    
    template <>
    void Variables<cl_uint, uint32_t>::runScript(std::string script, std::string var) {
        throw std::runtime_error("Variable runScript can only be used with string key types");
    }
    
    template <>
    void Variables<cl_uchar, uint32_t>::runScript(std::string script, std::string var) {
        throw std::runtime_error("Variable runScript can only be used with string key types");
    }

    template <>
    void Variables<float, std::string>::runScript(std::string script, std::string var) {
        runScriptImpl<float>(*this, script, var);
    }

    template <>
    void Variables<double, std::string>::runScript(std::string script, std::string var) {
        runScriptImpl<double>(*this, script, var);
    }

    /**
    * %VariablesVector constructor
    */
    template <typename RTYPE>
    VariablesVector<RTYPE>::VariablesVector():
        bufferOnDevice(false),
        bufferSize(0) {
    }

    /**
    * %VariablesVector copy constructor
    */
    template <typename RTYPE>
    VariablesVector<RTYPE>::VariablesVector(VariablesVector const &r):
        bufferOnDevice(false),
        bufferSize(0) {

        r.ensureBufferOnHost();
        dataVec = r.dataVec;
    }

    /**
    * %VariablesVector assignment operator
    */
    template <typename RTYPE>
    VariablesVector<RTYPE> &VariablesVector<RTYPE>::operator=(VariablesVector const &r) {

        if (this != &r) {

            ensureBufferOnHost();
            r.ensureBufferOnHost();
            dataVec = r.dataVec;
        }
        return *this;
    }

    /**
    * Clone VariablesVector data
    * Makes a copy of the %VariablesVector
    */
    template <typename RTYPE>
    VariablesVector<RTYPE> *VariablesVector<RTYPE>::clone() {

        // Create new VariablesVector
        VariablesVector<RTYPE> *r = new VariablesVector<RTYPE>();

        // Copy data
        r->dataVec = dataVec;

        return r;
    }

    /**
    * Get buffer from %VariablesVector.
    * @return %VariablesVector buffer
    */
    template <typename RTYPE>
    const cl::Buffer &VariablesVector<RTYPE>::getBuffer() {

        // Check if data has been created
        if (size() == 0) {
            throw std::range_error("Array has no data");
        }

        // Ensure data buffer is on device
        if (!bufferOnDevice) {

            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();

            // Check data size
            if (size() != bufferSize) {

                // Update buffer
                bufferSize = size();
                cl_int err = CL_SUCCESS;
                auto &context = solver.getContext();
                buffer = cl::Buffer(context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, bufferSize*sizeof(RTYPE), dataVec.data(), &err);
                if (err != CL_SUCCESS) {
                    throw std::range_error("Cannot allocate memory for variable buffer");
                }

                // Map to host
                bufferOnDevice = true;
                mapBuffer(queue);
            }

            // Unmap buffer
            unmapBuffer(queue);
        }

        // Return reference to data buffer
        return buffer;
    }

    /**
    * Map OpenCL data buffer to host, this gives exclusive access to the host
    */
    template <typename RTYPE>
    void VariablesVector<RTYPE>::mapBuffer(cl::CommandQueue &queue) const {
        if (bufferOnDevice) {
            if (size() != bufferSize) {
                throw std::range_error("Data size differs from buffer size");
            }
            auto &solver = Geostack::Solver::getSolver();
            if (solver.getUseMapping()) {
                void *pb = queue.enqueueMapBuffer(buffer, CL_TRUE, CL_MAP_READ | CL_MAP_WRITE, 0, bufferSize*sizeof(RTYPE));
                if (pb != static_cast<void *>(dataVec.data())) {
                    throw std::range_error("Mapped pointer differs for data buffer");
                }
            } else {
                queue.enqueueReadBuffer(buffer, CL_TRUE, 0, bufferSize*sizeof(RTYPE), 
                    static_cast<void *>(dataVec.data()));
            }
        }
        bufferOnDevice = false;
    }

    /**
    * Unmap OpenCL data buffer from host, this gives exclusive access to the device
    */
    template <typename RTYPE>
    void VariablesVector<RTYPE>::unmapBuffer(cl::CommandQueue &queue) {
        if (!bufferOnDevice) {          
            auto &solver = Geostack::Solver::getSolver();
            if (solver.getUseMapping()) {
                queue.enqueueUnmapMemObject(buffer, dataVec.data());
            } else {
                queue.enqueueWriteBuffer(buffer, CL_FALSE, 0, bufferSize*sizeof(RTYPE), 
                    static_cast<void *>(dataVec.data()));
            }
        }
        bufferOnDevice = true;
    }

    /**
    * Ensure data is on the host
    */
    template <typename RTYPE>
    void VariablesVector<RTYPE>::ensureBufferOnHost() const {

        // Check and move data to host
        if (bufferOnDevice) {
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &queue = solver.getQueue();
            mapBuffer(queue);
        }
    }

    // Variable definitions
    template class Variables<cl_uint, uint32_t>;
    template class Variables<cl_uchar, uint32_t>;
    template class Variables<float, std::string>;
    template class Variables<double, std::string>;
    // template void Variables<float, std::string>::runScript(std::string, std::string);
    // template void Variables<double, std::string>::runScript(std::string, std::string);

    // Variable array definitions
    template class VariablesVector<cl_int>;
    template class VariablesVector<cl_uint>;
    template class VariablesVector<cl_uchar>;
    template class VariablesVector<float>;
    template class VariablesVector<double>;
    template class VariablesVector<RandomState>;
    template class VariablesVector<Coordinate<float> >;
    template class VariablesVector<Coordinate<double> >;
}
