/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <algorithm>
#include <cctype>
#include <sstream>
#include <iostream>
#include <regex>

#include "date.h"
#include "gs_solver.h"
#include "gs_string.h"

namespace Geostack
{
    namespace Strings
    {
        /*
        * Check if string is numeric
        */
        bool isNumber(const std::string &str) {

            // Check empty string
            if (str.length() == 0)
                return false;
            
            // Check for leading minus
            int strStart = 0;
            if (str[0] == '-')
                strStart = 1;

            // Parse string
            bool hasDecimal = false;
            for (int i = strStart; i < str.size(); i++) {

                if (str[i] == '.') {

                    // Check decimal point
                    if (hasDecimal)
                        return false;
                    hasDecimal = true;

                } else {

                    // Check digit
                    if (!::isdigit(str[i]))
                        return false;
                }
            }
            return true;
        }
      
        /*
        * Convert string to float
        */
        template<> float toNumber(const std::string &str) {
            try {
                std::size_t s;
                float v = std::stof(str, &s);
                return s == 0 ? (float)0 : v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for float conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to double
        */
        template<> double toNumber(const std::string &str) {
            try {
                std::size_t s;
                double v = std::stod(str, &s);
                return s == 0 ? (double)0 : v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for double conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to char
        */
        template<> int8_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (int8_t)0 : (int8_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for char conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to int16
        */
        template<> int16_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (int16_t)0 : (int16_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for word conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to int32
        */
        template<> int32_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (int32_t)0 : (int32_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for int32 conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to int64
        */
        template<> int64_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (int64_t)0 : (int64_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for int64 conversion.";
                throw std::runtime_error(err.str());
            }
        }
        

        /*
        * Convert string to unsigned char
        */
        template<> uint8_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (uint8_t)0 : (uint8_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for char conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to unsigned int16
        */
        template<> uint16_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (uint16_t)0 : (uint16_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for word conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to unsigned int32
        */
        template<> uint32_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (uint32_t)0 : (uint32_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for int32 conversion.";
                throw std::runtime_error(err.str());
            }
        }

        /*
        * Convert string to unsigned int64
        */
        template<> uint64_t toNumber(const std::string &str) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? (uint64_t)0 : (uint64_t)v;
            } catch(std::invalid_argument) {
                std::stringstream err;
                err << "Invalid string '" << str << "' for int64 conversion.";
                throw std::runtime_error(err.str());
            }
        }


        /*
        * Convert string to float with default
        */
        template<> float toNumberWithDefault(const std::string &str, float def) {
            try {
                std::size_t s;
                float v = std::stof(str, &s);
                return s == 0 ? def : v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to double with default
        */
        template<> double toNumberWithDefault(const std::string &str, double def) {
            try {
                std::size_t s;
                double v = std::stod(str, &s);
                return s == 0 ? def : v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to char with default
        */
        template<> int8_t toNumberWithDefault(const std::string &str, int8_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (int8_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to int16 with default
        */
        template<> int16_t toNumberWithDefault(const std::string &str, int16_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (int16_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to int32 with default
        */
        template<> int32_t toNumberWithDefault(const std::string &str, int32_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (int32_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to int64 with default
        */
        template<> int64_t toNumberWithDefault(const std::string &str, int64_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (int64_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }
        
        /*
        * Convert string to unsigned char with default
        */
        template<> uint8_t toNumberWithDefault(const std::string &str, uint8_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (uint8_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to unsigned int16 with default
        */
        template<> uint16_t toNumberWithDefault(const std::string &str, uint16_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (uint16_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to unsigned int32 with default
        */
        template<> uint32_t toNumberWithDefault(const std::string &str, uint32_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (uint32_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Convert string to unsigned int64 with default
        */
        template<> uint64_t toNumberWithDefault(const std::string &str, uint64_t def) {
            try {
                std::size_t s;
                long long v = std::stoll(str, &s);
                return s == 0 ? def : (uint64_t)v;
            } catch (std::invalid_argument) {
                return def;
            }
        }

        /*
        * Split path into directory, filename and extension
        */
        std::vector<std::string> splitPath(const std::string &path) {
            
            // Create vector
            std::vector<std::string> split;
            split.resize(3);

            // Handle empty paths
            if (path.size() == 0) {
                split[0] = ".";

            } else {
            
                // Scan for file split
                auto rname = path.rbegin();
                while (rname != path.rend() && *rname != '/' && *rname != '\\') 
                    ++rname; 
            
                // Set parts
                split[0] = std::string(path.begin(), rname.base());
                split[1] = std::string(rname.base(), path.end());

                // Split filename
                if (split[1].length() > 0) {

                    auto rext = split[1].rbegin();
                    while (rext != split[1].rend() && (*rext != '.'))
                        ++rext; 

                    if (rext != split[1].rend()) {

                        // Set extension
                        split[2] = std::string(rext.base(), split[1].end());

                        // Skip '.' character
                        if (rext != split[1].rend() && *rext == '.')
                            ++rext;

                        // Set name
                        split[1] = std::string(split[1].begin(), rext.base());

                    }
                }
            }

            return split;
        }

        auto extractFilename(const std::string& path, bool incl_ext) -> std::string
        {
            const auto pathComponents = splitPath(path);
            if (pathComponents.size() != 3) {
                throw std::runtime_error{"Unexpected path components list returned by splitPath()"};
            }
            auto filename = pathComponents[1];
            if (incl_ext && !pathComponents[2].empty()) {
                filename += "." + pathComponents[2];
            }
            return filename;
        }

        /**
        * String splitting
        */
        std::vector<std::string> split(std::string str, char del, bool concatenate) {
        
            std::vector<std::string> strings;
            std::size_t start = 0;
            std::size_t end = 0;
            while (end < str.size()) {
                while (end < str.size() && str[end] != del) {
                    end++;
                }
                strings.push_back(str.substr(start, end-start));
                if (concatenate) {
                    while (end < str.size() && str[end] == del) {
                        end++;
                    }
                } else {
                    end++;
                }
                start = end;
            }
            return strings;
        }
        
        /**
        * String replace substring
        */
        std::string replace(std::string &str, std::string search, std::string replace) {
        
            // Parse string
            std::string r;
            auto it = str.begin();

            // Search for first occurrence
            auto next = std::search(it, str.end(), search.begin(), search.end());
            while (next != str.end()) {

                // Add previous block to return string
                r.append(it, next);
                r.append(replace);

                // Skip replacement
                it = next + search.size();

                // Search for next occurrence
                next = std::search(it, str.end(), search.begin(), search.end());
            }

            // Add last block
            r.append(it, next);
            return r;
        }

        /**
        * String white space removal, defined by isspace
        */
        std::string removeWhitespace(std::string str) {

            str.erase(std::remove_if(str.begin(), str.end(), 
                [](unsigned char c) { return std::isspace(c); }), str.end());
            return str;
        }

        /**
        * String character removal
        */
        std::string removeCharacter(std::string str, char c) {

            str.erase(std::remove(str.begin(), str.end(), c), str.end());
            return str;
        }
        
        /**
        * String conversion to uppercase
        */
        std::string toUpper(std::string str) {
        
            // Copy string
            std::string out(str);

            // Convert to uppercase
            std::transform(out.begin(), out.end(), out.begin(), ::toupper);

            return out;
        }        

        /**
        * String conversion to lowercase
        */
        std::string toLower(std::string str) {
        
            // Copy string
            std::string out(str);

            // Convert to lowercase
            std::transform(out.begin(), out.end(), out.begin(), ::tolower);

            return out;
        }
        
        /**
        * Clip string to length and pad with character
        */
        std::string pad(std::string str, char padChar, std::size_t length) {
            
            // Initialise string
            std::string out(length, padChar);

            // Copy string
            std::copy(str.begin(), str.begin()+std::min(length, str.size()), out.begin());

            return out;
        }

        /**
        * Convert date string to millisecond epoch time
        * @param str ISO8601 date time string
        * @return epoch time in milliseconds
        */
        int64_t iso8601toEpoch(std::string str) {

            date::sys_time<std::chrono::milliseconds> tp;

            if (str.back() == 'Z') {

                // Try to parse Zulu format
                std::istringstream in(str);
                in >> date::parse("%FT%TZ", tp);
                if (!in.fail())
                    return tp.time_since_epoch().count();

            } else {

                // Try to parse time zone
                std::istringstream in(str);
                in >> date::parse("%FT%T%Ez", tp);
                if (!in.fail())
                    return tp.time_since_epoch().count();
            }

            // Return null
            return getNullValue<int64_t>();
        }

        /**
        * Convert date string to millisecond time zone offset
        * @param str ISO8601 date time string
        * @return offset time in milliseconds
        */
        int64_t iso8601toTimeZoneOffset(std::string str) {

            int64_t offset = 0;
            try {
                std::smatch tzMatch;
                std::regex_search(str, tzMatch, std::regex("(\\+|-)\\d{2}:\\d{2}$"));
                if (tzMatch.size() > 0) {

                    // Form of +-##:##
                    auto match = tzMatch.str(0);
                    offset = 
                        toNumber<int64_t>(match.substr(1, 2))*3600000+ // hours
                        toNumber<int64_t>(match.substr(4, 2))*60000;   // minutes
                    if (match[0] == '-') {
                        offset = -offset;
                    }

                } else {                
                    std::regex_search(str, tzMatch, std::regex("(\\+|-)\\d{2}$"));
                    if (tzMatch.size() > 0) {

                        // Form of +-##
                        auto match = tzMatch.str(0);
                        offset = toNumber<int64_t>(match.substr(0, 3))*3600000;   // minutes

                    } else {
                        std::regex_search(str, tzMatch, std::regex("(\\+|-)\\d{4}$"));
                        if (tzMatch.size() > 0) {

                            // Form of +-####
                            auto match = tzMatch.str(0);
                            offset = 
                                toNumber<int64_t>(match.substr(1, 2))*3600000+ // hours
                                toNumber<int64_t>(match.substr(3, 2))*60000;   // minutes
                            if (match[0] == '-') {
                                offset = -offset;
                            }
                        }
                    }
                }

            } catch(std::regex_error e) {
                std::stringstream err;
                err << "Regex exception '" << e.what();
                throw std::runtime_error(err.str());
            }

            return offset;
        }
    }
}
