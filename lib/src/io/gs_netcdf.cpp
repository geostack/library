/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <set>
#include <iomanip>
#include <cmath>

#include "json11.hpp"

#include "gs_string.h"
#include "gs_netcdf.h"

using namespace json11;

namespace Geostack
{    
    /**
    * Flip endian of variable.
    * @param v big endian variable.
    * @return variable in system endian.
    */
    template <typename T>
    static T swapEndian(const T &v) {

        // Create local union
        union {
            T va;
            uint8_t vb[sizeof(T)];
        } s, d;
        
        // Swap bytes
        s.va = v;
        for (size_t k = 0; k < sizeof(T); k++)
            d.vb[k] = s.vb[sizeof(T)-k-1];
        return d.va;
    }
    
    /**
    * Convert big endian variable to or from system endian.
    * @param v input variable.
    * @return output variable.
    */
    template <typename T>
    static void convertBigEndian(T &v) {
    
        #if defined(ENDIAN_LITTLE)
        v = swapEndian(v);
        #endif
    }
    
    /**
    * Read big-endian 32-bit double word from file
    */
    uint32_t readDoubleWord(std::fstream &fileStream) {
        uint32_t r;
        fileStream.read(reinterpret_cast<char *>(&r), sizeof(r));
        convertBigEndian(r);
        return r;
    }
    
    /**
    * Read big-endian 64-bit quad word from file
    */
    uint64_t readQuadWord(std::fstream &fileStream) {
        uint64_t r;
        fileStream.read(reinterpret_cast<char *>(&r), sizeof(r));
        convertBigEndian(r);
        return r;
    }
    
    /**
    * Read data from record
    */    
    template <typename T>
    std::vector<T> readVariableData(std::fstream &fileStream, uint64_t nRecords, uint64_t recordSize) {

        // Read data
        std::vector<T> data(nRecords);
        if (recordSize != 0) {

            // Record variables
            for (std::size_t i = 0; i < nRecords; i++) {
                fileStream.read(reinterpret_cast<char *>(&data[i]), sizeof(T));
                fileStream.seekg(recordSize-sizeof(T), std::ios_base::cur);
            }

        } else {

            // Non-record variables
            fileStream.read(reinterpret_cast<char *>(&data[0]), nRecords*sizeof(T));
        }
        
        std::for_each(data.begin(), data.end(), convertBigEndian<T>);
        return data;
    }
    
    /**
    * Read attributes into PropertyMap
    */
    PropertyMap readAttributes(std::fstream &fileStream) {
    
        // Create map
        PropertyMap properties;

        // Read attribute list
        uint32_t tag = readDoubleWord(fileStream);
        if (tag == (uint32_t)0x0C) {

            // Read attributes
            uint32_t nattrs = readDoubleWord(fileStream);

            std::string name;
            for (std::size_t i = 0; i < nattrs; i++) {

                // Read name size
                uint32_t nameSize = readDoubleWord(fileStream);

                // Pad to nearest 4 bytes
                uint32_t nameSizePadded = (nameSize+(((uint32_t)4-(nameSize&0x03))&0x03));
                name.resize(nameSizePadded);

                // Read name
                fileStream.read(reinterpret_cast<char *>(&name[0]), nameSizePadded);
                name.resize(nameSize);

                // Read attribute type
                uint32_t attrType = readDoubleWord(fileStream);

                // Read dimension size
                uint32_t attrSize = readDoubleWord(fileStream);

                // Read attribute value
                switch(attrType) {
                    
                    case NetCDFData::NC_CHAR:
                    case NetCDFData::NC_BYTE: {

                        // Read data
                        uint32_t attrSizePadded = (attrSize+(((uint32_t)4-(attrSize&0x03))&0x03));
                        std::string data;
                        data.resize(attrSizePadded);
                        fileStream.read(reinterpret_cast<char *>(&data[0]), attrSizePadded);
                        data.resize(attrSize);

                        // Update property map
                        properties.template setProperty<std::string>(name, data);

                    } break;
                    
                    case NetCDFData::NC_SHORT: {

                        // Read data
                        uint32_t attrSizePadded = ((attrSize<<1)+(((uint32_t)4-((attrSize<<1)&0x03))&0x03));
                        std::vector<int16_t> data(attrSizePadded);
                        fileStream.read(reinterpret_cast<char *>(&data[0]), attrSizePadded);
                        std::for_each(data.begin(), data.end(), convertBigEndian<int16_t>);
                        data.resize(attrSize);

                        // Update property map
                        if (data.size() == 1) {
                            properties.template setProperty<int>(name, (int)data[0]);
                        } else if (data.size() > 1) {
                            std::vector<int> data32(data.begin(), data.end());
                            properties.template setProperty<std::vector<int> >(name, data32);
                        }
                    } break;
                    
                    case NetCDFData::NC_INT: {

                        // Read data
                        std::vector<int32_t> data(attrSize);
                        fileStream.read(reinterpret_cast<char *>(&data[0]), attrSize*sizeof(int32_t));
                        std::for_each(data.begin(), data.end(), convertBigEndian<int32_t>);

                        // Update property map
                        if (data.size() == 1) {
                            properties.template setProperty<int>(name, data[0]);
                        } else if (data.size() > 1) {
                            properties.template setProperty<std::vector<int> >(name, data);
                        }
                    } break;
                    
                    case NetCDFData::NC_FLOAT: {
                    
                        // Read data
                        std::vector<float> data(attrSize);
                        fileStream.read(reinterpret_cast<char *>(&data[0]), attrSize*sizeof(float));
                        std::for_each(data.begin(), data.end(), convertBigEndian<float>);

                        // Update property map
                        if (data.size() == 1) {
                            properties.template setProperty<float>(name, data[0]);
                        } else if (data.size() > 1) {
                            properties.template setProperty<std::vector<float> >(name, data);
                        }
                    } break;
                    
                    case NetCDFData::NC_DOUBLE: {

                        // Read data
                        std::vector<double> data(attrSize);
                        fileStream.read(reinterpret_cast<char *>(&data[0]), attrSize*sizeof(double));
                        std::for_each(data.begin(), data.end(), convertBigEndian<double>);

                        // Update property map
                        if (data.size() == 1) {
                            properties.template setProperty<double>(name, data[0]);
                        } else if (data.size() > 1) {
                            properties.template setProperty<std::vector<double> >(name, data);
                        }
                    } break;

                    default:  {
                        std::stringstream err;
                        err << "Unsupported NetCDF data type '" << attrType << "'";
                        throw std::runtime_error(err.str());
                    }
                }
            }
        } else if (tag == (uint32_t)0) {

            // Check for second zero
            tag = readDoubleWord(fileStream);
            if (tag != (uint32_t)0) {
                std::stringstream err;
                err << "Unexpected tag '" << tag << "' found when expecting zero";
                throw std::runtime_error(err.str());
            }

        } else {

            std::stringstream err;
            err << "Unexpected tag '" << tag << "' found when expecting attribute";
            throw std::runtime_error(err.str());
        }

        // Return property map
        return properties;
    }

    /**
    * Read vaiable from NetCDF3 file to raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void NetCDFHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {        

        #if !defined(ENDIAN_BIG) && !defined(ENDIAN_LITTLE)
        #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
        #endif

        // Clear variables
        vars.clear();
        varName.clear();
        nx = 0;
        ny = 0;
        nz = 0;
        oz = getNullValue<CTYPE>();
        recordSize = 0;
        xFlipped = false;
        yFlipped = false;
        zFlipped = false;
        layers.clear();
        uint32_t layersStart = getNullValue<uint32_t>();
        uint32_t layersEnd = getNullValue<uint32_t>();
        uint32_t layersStep = 1;
        globalAttributes.clear();

        // Parse config
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }
              
            // Set variable name type
            if (config["variable"].is_string()) {
                varName = config["variable"].string_value();
            }
              
            // Set layers
            if (config["layers"].is_number()) {

                // Set single value
                layers.push_back((uint32_t)config["layers"].number_value());

            } else if (config["layers"].is_array()) {

                // Create items
                for (auto &layer : config["layers"].array_items()) {
                    if (layer.is_number()) {
                        layers.push_back((uint32_t)layer.number_value());
                    }
                }

            } else if (config["layers"].is_string()) {

                // Check for slice notation
                std::string sliceString = config["layers"].string_value();
                if (sliceString.front() == '[' && sliceString.back() == ']') {

                    // Split into parts
                    auto parts = Strings::split(sliceString.substr(1, sliceString.size()-2), ':');
                    if (parts.size() > 0) {
                        layersStart = 0;
                        if (parts[0].size() > 0) {
                            layersStart = Strings::toNumber<uint32_t>(parts[0]);
                        }
                        if (parts.size() > 1) {
                            if (parts[1].size() > 0) {
                                layersEnd = Strings::toNumber<uint32_t>(parts[1]);
                            }
                            if (parts.size() > 2) {
                                layersStep = Strings::toNumber<uint32_t>(parts[2]);
                            }
                        }
                    }
                }
            }
              
            // Set z-origin
            if (config["origin_z"].is_number()) {
                oz = config["origin_z"].number_value();
            }
        }

        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr) {
        
            // Get file stream handle
            std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

            // Open file
            if (!fileStream.is_open()) {
                fileStream.open(fileName, std::ios::in | std::ios::binary);
            }

            // Check file
            if (!fileStream) {
                std::stringstream err;
                err << "Cannot open '" << fileName << "' for reading";
                throw std::runtime_error(err.str());
            }
            
            // Data tag
            uint32_t tag = 0;

            // Read 'CDF' tag
            uint8_t headerTag[3];
            fileStream.read(reinterpret_cast<char *>(&headerTag), sizeof(headerTag));
            if (headerTag[0] != 'C' || headerTag[1] != 'D' || headerTag[2] != 'F') {
                std::stringstream err;
                err << "Invalid NetCDF file '" << fileName << "'";
                throw std::runtime_error(err.str());
            }

            // Read version
            uint8_t version = 0;
            fileStream.read(reinterpret_cast<char *>(&version), sizeof(version));
            if (version != 1 && version != 2) {
                std::stringstream err;
                err << "Unsupported NetCDF version '" << version << "'";
                throw std::runtime_error(err.str());
            }

            // Read streaming tag
            uint32_t numrecs = 0;
            fileStream.read(reinterpret_cast<char *>(&numrecs), sizeof(numrecs));
            convertBigEndian<uint32_t>(numrecs);
            if (numrecs == (uint32_t)0xFFFF) {
                throw std::runtime_error("Streaming NetCDF not supported");
            }

            // Read dimension list
            std::set<std::string> dimNames;
            std::vector< std::pair<std::string, uint32_t> > dims;
            tag = readDoubleWord(fileStream);
            if (tag == (uint32_t)0x0A) {

                // Read dimensions
                uint32_t ndims = readDoubleWord(fileStream);

                std::string name;
                for (std::size_t i = 0; i < ndims; i++) {

                    // Read name size
                    uint32_t nameSize = readDoubleWord(fileStream);

                    // Pad to nearest 4 bytes
                    uint32_t nameSizePadded = (nameSize+(((uint32_t)4-(nameSize&0x03))&0x03));
                    name.resize(nameSizePadded);

                    // Read name
                    fileStream.read(reinterpret_cast<char *>(&name[0]), nameSizePadded);
                    name.resize(nameSize);

                    // Read dimension size
                    uint32_t dimSize = readDoubleWord(fileStream);

                    // Add to list
                    dimNames.insert(name);
                    dims.push_back( { name, dimSize } );
                }
            } else if (tag == (uint32_t)0) {

                // Check for second zero
                tag = readDoubleWord(fileStream);
                if (tag != (uint32_t)0) {
                    std::stringstream err;
                    err << "Unexpected tag '" << tag << "' found when expecting zero";
                    throw std::runtime_error(err.str());
                }

            } else {

                std::stringstream err;
                err << "Unexpected tag '" << tag << "' found when expecting dimension";
                throw std::runtime_error(err.str());
            }

            // Read global attributes
            globalAttributes = readAttributes(fileStream);

            // Read variable list
            std::set<std::string> dataVars;
            tag = readDoubleWord(fileStream);
            if (tag == (uint32_t)0x0B) {

                // Read attributes
                uint32_t nvars = readDoubleWord(fileStream);

                std::string name;
                for (std::size_t i = 0; i < nvars; i++) {

                    NetCDFVariable<CTYPE> var;

                    // Read name size
                    uint32_t nameSize = readDoubleWord(fileStream);

                    // Pad to nearest 4 bytes
                    uint32_t nameSizePadded = (nameSize+(((uint32_t)4-(nameSize&0x03))&0x03));
                    name.resize(nameSizePadded);

                    // Read name
                    fileStream.read(reinterpret_cast<char *>(&name[0]), nameSizePadded);
                    name.resize(nameSize);

                    // Read number of elements
                    uint32_t nElems = readDoubleWord(fileStream);

                    // Read dimension IDs
                    var.dims.resize(nElems);
                    if (nElems > 0) {
                        fileStream.read(reinterpret_cast<char *>(&var.dims[0]), nElems*sizeof(uint32_t));
                        std::for_each(var.dims.begin(), var.dims.end(), convertBigEndian<uint32_t>);
                    }
                    
                    // Read attributes
                    var.attr = readAttributes(fileStream);

                    // Read variable type
                    var.type = readDoubleWord(fileStream);

                    // Read variable size in bytes
                    var.size = readDoubleWord(fileStream);

                    // Check whether this is a record dimension
                    if (var.dims.size() > 0 && var.dims[0] < dims.size() && dims[var.dims[0]].second == 0) {
                        var.isRecord = true;
                        recordSize += var.size;
                    }

                    // Read variable file offset
                    if (version == 1) {

                        // 32-bit offsets
                        var.offset = (uint64_t)readDoubleWord(fileStream);

                    } else {

                        // 64-bit offsets
                        var.offset = (uint64_t)readQuadWord(fileStream);
                    }

                    // Add to variable map
                    vars[name] = var;

                    // Check for non-dimension variables
                    if (dimNames.find(name) == dimNames.end() && (var.dims.size() == 2 || var.dims.size() == 3)) {
                        dataVars.insert(name);
                    }
                }
            } else if (tag == (uint32_t)0) {

                // Check for second zero
                tag = readDoubleWord(fileStream);
                if (tag != (uint32_t)0) {
                    std::stringstream err;
                    err << "Unexpected tag '" << tag << "' found when expecting zero";
                    throw std::runtime_error(err.str());
                }

            } else {

                std::stringstream err;
                err << "Unexpected tag '" << tag << "' found when expecting variable";
                throw std::runtime_error(err.str());
            }

            // Check for non-dimension variables
            if (dataVars.size() == 0) {

                // No variables
                throw std::runtime_error("No data variables found in NetCDF file");

            } else if (dataVars.size() == 1 && varName.empty()) {

                // Set to first variable by default
                varName = *dataVars.begin();

            } else if (varName.empty() || (dataVars.find(varName) == dataVars.end())) {

                std::stringstream err;
                if (varName.empty()) {
                    err << "NetCDF variable name required, variables are: ";
                } else {
                    err << "NetCDF variable '" << varName << "' not found, variables are: ";
                }
                for (auto nit : dataVars) {
                    err << "'" << nit << "', ";
                }
                std::string errStr = err.str();
                errStr.resize(errStr.size()-2);
                throw std::runtime_error(errStr);
            }

            // Read dimension variables
            auto &dataVar = vars.find(varName)->second;
            for (auto &dimID : dataVar.dims) {

                // Check ID
                if (dimID > dims.size()-1) {
                    std::stringstream err;
                    err << "Invalid dimension index '" << dimID << "'";
                    throw std::runtime_error(err.str());
                }
                auto &dim = dims[dimID];

                // Search for dimension variables
                auto vit = vars.find(dim.first);
                if (vit == vars.end()) {
                    std::stringstream err;
                    err << "Cannot find dimensions variable '" << dim.first << "' in NetCDF file";
                    throw std::runtime_error(err.str());
                }
                auto &var = vit->second;
                
                // Check dimensionality
                if (var.dims.size() != 1) {
                    std::stringstream err;
                    err << "Dimensions variable '" << dim.first << "' is not 1D";
                    throw std::runtime_error(err.str());
                }
                
                // Check size
                if (var.size == 0) {
                    std::stringstream err;
                    err << "Dimensions variable '" << dim.first << "' contains no data";
                    throw std::runtime_error(err.str());
                }
                                
                // Jump to data location
                fileStream.seekg(var.offset);

                // Calculate number of records
                uint64_t nRecords = dim.second == 0 ? numrecs : dim.second;

                // Check dimension length
                if (var.size < 2) {
                    std::stringstream err;
                    err << "Dimensions variable '" << dim.first << "' must contain two or more values";
                    throw std::runtime_error(err.str());
                }

                // Read attribute value
                std::vector<CTYPE> dimData;
                switch(var.type) {
                    
                    case NetCDFData::NC_CHAR:
                    case NetCDFData::NC_BYTE: {
                        
                        // Read and convert data
                        auto data = readVariableData<int8_t>(fileStream, nRecords, dim.second == 0 ? recordSize : 0);
                        dimData = std::vector<CTYPE>(data.begin(), data.end());

                    } break;
                    
                    case NetCDFData::NC_SHORT: {
                    
                        // Read and convert data
                        auto data = readVariableData<int16_t>(fileStream, nRecords, dim.second == 0 ? recordSize : 0);
                        dimData = std::vector<CTYPE>(data.begin(), data.end());

                    } break;
                    
                    case NetCDFData::NC_INT: {                        
                    
                        // Read and convert data
                        auto data = readVariableData<int32_t>(fileStream, nRecords, dim.second == 0 ? recordSize : 0);
                        dimData = std::vector<CTYPE>(data.begin(), data.end());

                    } break;
                    
                    case NetCDFData::NC_FLOAT: {
                    
                        // Read and convert data
                        auto data = readVariableData<float>(fileStream, nRecords, dim.second == 0 ? recordSize : 0);
                        dimData = std::vector<CTYPE>(data.begin(), data.end());

                    } break;
                    
                    case NetCDFData::NC_DOUBLE: {
                    
                        // Read and convert data
                        auto data = readVariableData<double>(fileStream, nRecords, dim.second == 0 ? recordSize : 0);
                        dimData = std::vector<CTYPE>(data.begin(), data.end());

                    } break;

                    default: {
                        std::stringstream err;
                        err << "Unsupported NetCDF data type '" << var.type << "'";
                        throw std::runtime_error(err.str());
                    }
                }

                // Get spacing
                CTYPE dMin = dimData[0];
                if (dimData.size() > 1) {
                    CTYPE deltaMean = 0.0;
                    CTYPE deltaVar = 0.0;
                    for (std::size_t i = 1; i < dimData.size(); i++) {
                        dMin = fmin(dimData[i], dMin);
                        REAL d = dimData[i]-dimData[i-1];
                        REAL deltaMeanLast = deltaMean;
                        deltaMean += (d-deltaMean)/(CTYPE)i;
                        deltaVar += (d-deltaMeanLast)*(d-deltaMean);
                    }

                    // Check spacing
                    if (deltaMean == 0.0) {
                        std::stringstream err;
                        err << "Dimensions variable '" << dim.first << "' has zero spacing";
                        throw std::runtime_error(err.str());
                    }

                    // Coefficient of variation
                    REAL deltaC = sqrt(deltaVar)/(fabs(deltaMean)*(CTYPE)dimData.size());
                    if (deltaC > 1.0E-3) {
                        std::stringstream err;
                        err << "NetCDF dimensions variable '" << dim.first << "' must have equal spacing";
                        throw std::runtime_error(err.str());
                    }

                    // Set spacing
                    var.delta = deltaMean;
                }

                // Set origin
                var.start = dMin;
            }           

            // Set Raster dimensions
            CTYPE hx = 0.0;
            CTYPE hy = 0.0;
            CTYPE hz = 0.0;
            CTYPE dox = 0.0;
            CTYPE doy = 0.0;
            CTYPE doz = 0.0;

            for (auto &dimID : dataVar.dims) {
                auto &dim = dims[dimID];
                if (dim.first == "longitude" || dim.first == "lon" || dim.first == "x") {

                    auto &var = vars[dim.first];
                    nx = dim.second == 0 ? numrecs : dim.second;
                    hx = fabs(var.delta);
                    dox = var.start-0.5*hx;
                    xFlipped = var.delta < 0.0;

                } else if (dim.first == "latitude" || dim.first == "lat" || dim.first == "y") {
                    
                    auto &var = vars[dim.first];
                    ny = dim.second == 0 ? numrecs : dim.second;
                    hy = fabs(var.delta);
                    doy = var.start-0.5*hy;
                    yFlipped = var.delta < 0.0;

                } else if (dim.first == "time") {
                    
                    auto &var = vars[dim.first];
                    nz = dim.second == 0 ? numrecs : dim.second;
                    hz = fabs(var.delta);
                    doz = var.start;
                    zFlipped = var.delta < 0.0;

                }
            }

            // Check number of dimensions
            if (nx == 0 || ny == 0) {
                std::stringstream err;
                err << "NetCDF file '" << fileName << "' must have 2 or 3 dimensions";
                throw std::runtime_error(err.str());
            }
            if (nz == 0) {
                nz = 1;
            }
            if (hz == 0) hz = 1.0;
            if (isValid<CTYPE>(oz)) doz = oz;

            // Update layers
            if (layers.size() == 0 && isValid<uint32_t>(layersStart)) {
                if (isInvalid<uint32_t>(layersEnd)) {
                    layersEnd = nz;
                }
                for (std::size_t i = layersStart; i < layersEnd; i += layersStep) {
                    layers.push_back(i);
                }
            }
            
            if (layers.size() > 0) {

                // Sort layers in order
                std::sort(layers.begin(), layers.end());

                // Check layers
                for (auto &layer : layers) {
                    if (layer > nz-1) {
                        std::stringstream err;
                        err << "Requested layer " << layer << " is greater than maximum (" << nz-1 << ")";
                        throw std::runtime_error(err.str());
                    }
                }
                nz = layers.size();
            } else {
                for (std::size_t i = 0; i < nz; i++) {
                    layers.push_back(i);
                }
            }

            // Create raster
            r.init(nx, ny, nz, hx, hy, hz, dox, doy, doz);
            
            // Register data callback
            using namespace std::placeholders;
            RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
                std::bind(&NetCDFHandler<RTYPE, CTYPE>::readDataFunction, this, _1, _2, _3));

            // Add maps as properties
            r.template setProperty<PropertyMap>("global", globalAttributes);
            PropertyMap variableMap;
            for (auto &var : vars) {
                variableMap.template setProperty<PropertyMap>(var.first, var.second.attr);
            }
            r.template setProperty<PropertyMap>("variables", variableMap);
        }
    }
    
    /**
    * Read data block to vector
    * @param tdim %TileDimensions of tile.
    * @param v vector to fill.
    * @param r %Raster handle.
    */
    template <typename RTYPE, typename CTYPE>
    template <typename VTYPE, typename DTYPE>
    void NetCDFHandler<RTYPE, CTYPE>::readData(std::fstream &fileStream, TileDimensions<CTYPE> &tdim, std::vector<RTYPE> &v, NetCDFVariable<CTYPE> &var) {
    
        // Set data sizes
        uint64_t fRowSize = (uint64_t)nx*sizeof(DTYPE);
        uint64_t fSliceSize = var.isRecord ? recordSize : (uint64_t)ny*fRowSize;
        uint64_t baseOffset = var.offset;
        int32_t jDataInc = 1;
        if (yFlipped) {
            baseOffset += (uint64_t)tdim.ti*TileMetrics::tileSize*sizeof(DTYPE)+
                (uint64_t)(ny-1-tdim.tj*TileMetrics::tileSize)*fRowSize;
            jDataInc = -1;
        } else {
            baseOffset += (uint64_t)tdim.ti*TileMetrics::tileSize*sizeof(DTYPE)+
                (uint64_t)tdim.tj*TileMetrics::tileSize*fRowSize;
            jDataInc = 1;
        }
            
        // Data vector
        std::vector<DTYPE> data(TileMetrics::tileSize);

        // Read window dimensions
        uint32_t width = TileMetrics::tileSize;
        if ((tdim.ti+1)*TileMetrics::tileSize > nx) {
            width = nx-tdim.ti*TileMetrics::tileSize;
        }
        uint32_t height = TileMetrics::tileSize;
        if ((tdim.tj+1)*TileMetrics::tileSize > ny) {
            height = ny-tdim.tj*TileMetrics::tileSize;
        }

        // Get offset and scale
        RTYPE scale = 1.0;
        if (var.attr.hasProperty("scale_factor")) {
            scale = var.attr.template getProperty<RTYPE>("scale_factor");
        }
        RTYPE offset = 0.0;
        if (var.attr.hasProperty("add_offset")) {
            offset = var.attr.template getProperty<RTYPE>("add_offset");
        }

        if (var.attr.hasProperty("missing_value") || var.attr.hasProperty("_FillValue")) {
        
            // Get nodata value
            VTYPE noData = VTYPE();
            if (var.attr.hasProperty("missing_value")) {
                noData = var.attr.template getProperty<VTYPE>("missing_value");
            } else {
                noData = var.attr.template getProperty<VTYPE>("_FillValue");
            }

            // Read data with null handling
            for (uint32_t k = 0; k < nz; k++) {
                int32_t jData = 0;
                uint64_t startOffset = baseOffset+layers[k]*fSliceSize;
                for (uint32_t j = 0; j < height; j++, jData += jDataInc) {

                    // Read data line
                    fileStream.seekg(startOffset+jData*fRowSize);
                    fileStream.read(reinterpret_cast<char *>(&data[0]), width*sizeof(DTYPE));
                            
                    // Write data line with null handling
                    uint64_t vOffset = ((uint64_t)j+(uint64_t)k*TileMetrics::tileSize)*TileMetrics::tileSize;
                    for (uint32_t i = 0; i < width; i++) {
                        convertBigEndian<DTYPE>(data[i]);
                        if (data[i] == noData) {
                            v[i+vOffset] = getNullValue<RTYPE>();
                        } else {
                            v[i+vOffset] = offset+scale*(RTYPE)data[i];
                        }
                    }
                }
            }
        } else {

            // Read data
            for (uint32_t k = 0; k < nz; k++) {
                int32_t jData = 0;
                int32_t jDataInc = yFlipped ? -1 : 1;
                uint64_t startOffset = baseOffset+layers[k]*fSliceSize;
                for (uint32_t j = 0; j < height; j++, jData += jDataInc) {

                    // Read data line
                    fileStream.seekg(startOffset+jData*fRowSize);
                    fileStream.read(reinterpret_cast<char *>(&data[0]), width*sizeof(DTYPE));
                            
                    // Write data line
                    uint64_t vOffset = ((uint64_t)j+(uint64_t)k*TileMetrics::tileSize)*TileMetrics::tileSize;
                    for (uint32_t i = 0; i < width; i++) {
                        convertBigEndian<DTYPE>(data[i]);
                        v[i+vOffset] = offset+scale*(RTYPE)data[i];
                    }
                }
            }
        }
    }

    /**
    * Supply data to %Tile through callback function
    * @param tdim %TileDimensions of tile.
    * @param v vector to fill.
    * @param r %Raster handle.
    */
    template <typename RTYPE, typename CTYPE>
    void NetCDFHandler<RTYPE, CTYPE>::readDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r) {
        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr && 
            *RasterFileHandler<RTYPE, CTYPE>::fileStream) {
        
            // Get file stream handle
            std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

            // Get variable
            auto vit = vars.find(varName);
            if (vit == vars.end()) {
                std::stringstream err;
                err << "Cannot find variable '" << varName << "' in NetCDF file";
                throw std::runtime_error(err.str());
            }
            auto &var = vit->second;
            // Read data
            switch(var.type) {
                    
                case NetCDFData::NC_CHAR:
                case NetCDFData::NC_BYTE:
                    readData<int32_t, int8_t>(fileStream, tdim, v, var); 
                    break;
                    
                case NetCDFData::NC_SHORT:
                    readData<int32_t, int16_t>(fileStream, tdim, v, var); 
                    break;
                    
                case NetCDFData::NC_INT:
                    readData<int32_t, int32_t>(fileStream, tdim, v, var); 
                    break;
                    
                case NetCDFData::NC_FLOAT:
                    readData<float, float>(fileStream, tdim, v, var);
                    break;
                    
                case NetCDFData::NC_DOUBLE:
                    readData<double, double>(fileStream, tdim, v, var);
                    break;

                default: {
                    std::stringstream err;
                    err << "Unsupported NetCDF data type '" << var.type << "'";
                    throw std::runtime_error(err.str());
                }
            }
        } else {
          std::stringstream err;
          err << "No file stream is found";
          throw std::runtime_error(err.str());
        }

    }

    /**
    * Write raster to NetCDF3 format.
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void NetCDFHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // TODO
        throw std::runtime_error("NetCDF write functions are not yet available");
    }
    
    // RTYPE float, CTYPE float definitions
    template class NetCDFHandler<float, float>;

    // RTYPE uchar, CTYPE float definitions
    template class NetCDFHandler<uint8_t, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class NetCDFHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class NetCDFHandler<double, double>;

    // RTYPE uchar, CTYPE double definitions
    template class NetCDFHandler<uint8_t, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class NetCDFHandler<uint32_t, double>;
}
