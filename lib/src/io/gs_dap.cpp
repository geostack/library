/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <regex>

#include "json11.hpp"

#include "date.h"
#include "gs_string.h"
#include "gs_dap.h"

using namespace json11;
using namespace httplib;

using JsonPair = const std::pair<std::string, json11::Json>;

namespace Geostack
{
   /**
    * Flip endian of variable.
    * @param v big endian variable.
    * @return variable in system endian.
    */
    template <typename T>
    static T swapEndian(const T &v) {

        // Create local union
        union {
            T va;
            uint8_t vb[sizeof(T)];
        } s, d;
        
        // Swap bytes
        s.va = v;
        for (size_t k = 0; k < sizeof(T); k++)
            d.vb[k] = s.vb[sizeof(T)-k-1];
        return d.va;
    }
    
    /**
    * Convert big endian variable to or from system endian.
    * @param v input variable.
    * @return output variable.
    */
    template <typename T>
    static void convertBigEndian(T &v) {
    
        #if defined(ENDIAN_LITTLE)
        v = swapEndian(v);
        #endif
    }

    template <>
    void convertBigEndian(uint8_t &v) {
        // Null operation as 1-byte doesn't need to be swapped
    }

    template <typename T>
    std::vector<T> readData(char *pData, std::size_t &pos, std::size_t size) {

        // Get sizes
        uint32_t n0, n1;
        if (pos+sizeof(uint32_t) > size) {
            throw std::runtime_error("Unexpected end of data block");
        }
        n0 = *reinterpret_cast<uint32_t *>(pData+pos);
        convertBigEndian<uint32_t>(n0);
        pos += sizeof(uint32_t);
        
        if (pos+sizeof(uint32_t) > size) {
            throw std::runtime_error("Unexpected end of data block");
        }
        n1 = *reinterpret_cast<uint32_t *>(pData+pos);
        convertBigEndian<uint32_t>(n1);
        pos += sizeof(uint32_t);

        // Check data sizes
        if (n0 != n1) {
            throw std::runtime_error("Data size inconsistent");
        }

        // Read data
        T v;
        std::vector<T> values;
        for (std::size_t i = 0; i < n0; i++) {
            if (pos+sizeof(T) > size) {
                throw std::runtime_error("Unexpected end of data block");
            }
            v = *reinterpret_cast<T *>(pData+pos);
            convertBigEndian<T>(v);
            values.push_back(v);
            pos += sizeof(T);
        }
        return values;
    }
    
    /**
    * Parse data chunk
    */
    template <typename T>
    std::vector<T> parseData(std::string &data, std::string type) {

        // Search for 'Data' block
        std::size_t pos = 0;
        do {
            if (data[pos  ] == 'D' && 
                data[pos+1] == 'a' && 
                data[pos+2] == 't' && 
                data[pos+3] == 'a' && 
                data[pos+4] == ':' &&  
                data[pos+5] == 0x0A) {
                break;
            }            
        } while (pos++ < data.size()-5);

        if (pos < data.size()-5) {

            // Move to data start
            pos+=6;
            if (pos > data.size()-1) {
                throw std::runtime_error("Unexpected end of data block");
            }

            // Parse raw data
            type = Strings::toUpper(type);
            auto *pData = const_cast <char *>(data.data());
            if (type == "BYTE") {
                auto values = readData<uint8_t>(pData, pos, data.size());
                return std::vector<T>(values.begin(), values.end());
            } else if (type == "INT16") {
                auto values = readData<int16_t>(pData, pos, data.size());
                return std::vector<T>(values.begin(), values.end());
            } else if (type == "UINT16") {
                auto values = readData<uint16_t>(pData, pos, data.size());
                return std::vector<T>(values.begin(), values.end());
            } else if (type == "INT32") {
                auto values = readData<int32_t>(pData, pos, data.size());
                return std::vector<T>(values.begin(), values.end());
            } else if (type == "UINT32") {
                auto values = readData<uint32_t>(pData, pos, data.size());
                return std::vector<T>(values.begin(), values.end());
            } else if (type == "FLOAT32") {
                auto values = readData<float>(pData, pos, data.size());
                return std::vector<T>(values.begin(), values.end());
            } else if (type == "FLOAT64") {
                auto values = readData<double>(pData, pos, data.size());
                return std::vector<T>(values.begin(), values.end());
            } else {
                throw std::runtime_error("Unsupported type '" + type + "'");
            }
        } else {
            throw std::runtime_error("Data block not found");
        }
    }
    
    /**
    * Parse DDS chunk
    */
    Json::object parseDDSChunk(std::string chunk) {

        Json::object json;
        std::size_t pos = 0;
        if (chunk.size() == 0) {
            throw std::runtime_error("Empty chunk in dds parsing");
        }
        if (chunk[0] == '{') {
            pos++;
        }
        if (chunk.back() == '}') {
            chunk.pop_back();
        }
        
        do {

            // Skip whitespace
            while (std::isspace(chunk[pos]) && pos++ < chunk.size());
            if (pos == chunk.size()) {
                break;
            }

            // Create value object
            std::vector<std::pair<std::string, int> > variables;

            // Get type
            auto typeStart = pos;
            while (!std::isspace(chunk[pos]) && pos++ < chunk.size());
            if (pos == chunk.size()) {
                throw std::runtime_error("Unexpected end of dds block");
            }
            auto type = Strings::removeWhitespace(chunk.substr(typeStart, pos-typeStart));

            // Handle tags
            if (Strings::toUpper(type) == "ARRAY:" || Strings::toUpper(type) == "MAPS:") {

                // Skip whitespace
                while (std::isspace(chunk[pos]) && pos++ < chunk.size());
                typeStart = pos;
                while (!std::isspace(chunk[pos]) && pos++ < chunk.size());
                type = Strings::removeWhitespace(chunk.substr(typeStart, pos-typeStart));
            }

            // Skip whitespace
            while (std::isspace(chunk[pos]) && pos++ < chunk.size());
        
            // Get data
            auto blockStart = pos;
            auto blockEnd = pos;
            auto nameEnd = pos;
            do {
                if (std::isspace(chunk[pos])) {
                    blockEnd = pos;
                } else if (chunk[pos] == '{') {
                    std::size_t level = 0;
                    do {
                        if (chunk[pos] == '{') level++;
                        if (chunk[pos] == '}') level--;
                    } while (level != 0 && pos++ < chunk.size());
                } else if (chunk[pos] == '[') {

                    // Get dimension name
                    auto dimStart = pos+1;
                    while (chunk[pos] != '=' && pos++ < chunk.size());
                    if (pos == chunk.size()) {
                        throw std::runtime_error("Unexpected end of dds block in dimension");
                    }
                    auto dimName = Strings::removeWhitespace(chunk.substr(dimStart, pos-dimStart));
                    if (dimName.empty()) {
                        throw std::runtime_error("Dimension has no name");
                    }
                    
                    // Get dimension value
                    dimStart = pos+1;
                    while (chunk[pos] != ']' && pos++ < chunk.size());
                    if (pos == chunk.size()) {
                        throw std::runtime_error("Unexpected end of dds block in dimension");
                    }
                    auto dimValue = Strings::toNumber<int>(chunk.substr(dimStart, pos-dimStart));
                    
                    // Get create dimension variable
                    variables.push_back( { dimName, dimValue } );
                } else {
                    nameEnd = pos;
                }
            } while (chunk[pos] != ';' && pos++ < chunk.size());
            if (pos == chunk.size()) {
                throw std::runtime_error("Unexpected end of dds block");
            }

            // Get block
            auto block = chunk.substr(blockStart, blockEnd-blockStart);

            // Get and clean name
            auto name = Strings::removeWhitespace(chunk.substr(blockEnd, nameEnd-blockEnd));
            std::size_t n = 0;
            while (name[n] != '[' && n++ < name.size());
            name = name.substr(0, n);

            // Update json
            Json::object val;
            val["type"] = type;
            if (block.size() > 0) {
                val["data"] = parseDDSChunk(block);
            }
            if (variables.size() > 1) {
                Json::array vars;
                for (auto & var : variables) {
                    vars.push_back(var.first);
                }
                val["variables"] = vars;
            } else if (variables.size() == 1) {
                val[variables[0].first] = variables[0].second;
            }
            json[name] = val;

        } while (pos++ < chunk.size());
        return json;
    }
    
    /**
    * Parse DAS chunk
    */
    Json::object parseDASChunk(std::string chunk) {

        Json::object json;
        std::size_t pos = 0;
        if (chunk.size() == 0) {
            throw std::runtime_error("Empty chunk in das parsing");
        }
        
        do {

            // Skip whitespace
            while (std::isspace(chunk[pos]) && pos++ < chunk.size());
            if (pos == chunk.size()) {
                break;
            }

            // Get name
            auto headStart = pos;
            while (!std::isspace(chunk[pos]) && pos++ < chunk.size());
            if (pos == chunk.size()) {
                throw std::runtime_error("Unexpected end of das block");
            }
            auto head = Strings::removeWhitespace(chunk.substr(headStart, pos-headStart));

            // Skip whitespace
            while (std::isspace(chunk[pos]) && pos++ < chunk.size());

            // Check for objects
            Json::object val;
            std::string name;
            if (chunk[pos] == '{') {

                auto blockStart = pos+1;
                std::size_t level = 0;
                do {
                    if (chunk[pos] == '{') level++;
                    else if (chunk[pos] == '}') level--;
                } while (level != 0 && pos++ < chunk.size());

                // Find closing bracket
                while (chunk[pos] != '}' && pos++ < chunk.size());
                if (pos == chunk.size()) {
                    throw std::runtime_error("Unexpected end of das block");
                }

                // Get block
                auto block = chunk.substr(blockStart, pos-blockStart);
                val["data"] = parseDASChunk(block);
                name = head;

            } else {

                // Get name
                auto nameStart = pos;
                while (!std::isspace(chunk[pos]) && pos++ < chunk.size());
                if (pos == chunk.size()) {
                    throw std::runtime_error("Unexpected end of das block");
                }
                name = Strings::removeWhitespace(chunk.substr(nameStart, pos-nameStart));

                // Skip whitespace
                while (std::isspace(chunk[pos]) && pos++ < chunk.size());
                if (pos == chunk.size()) {
                    throw std::runtime_error("Unexpected end of das block");
                }

                // Get data
                auto dataStart = pos;                
                if (chunk[pos] == '"') {
                    dataStart++;
                    std::size_t quotes = 0;
                    do {
                        if (chunk[pos] == '"' && chunk[pos-1] != '\\') quotes++;
                    } while (quotes != 2 && pos++ < chunk.size());
                    if (pos == chunk.size()) {
                        throw std::runtime_error("Unexpected end of das block");
                    }
                } 
                while (chunk[pos] != ';' && pos++ < chunk.size());
                if (pos == chunk.size()) {
                    throw std::runtime_error("Unexpected end of das block");
                }
                auto data = chunk.substr(dataStart, pos-dataStart);
                val["type"] = head;
                val["data"] = data;
            }

            json[name] = val;

        } while (pos++ < chunk.size());
        return json;
    }

    // Apply C-style formatting to number or string
    std::string formatItem(JsonPair &item, std::string format) {

        const uint32_t sN = 1024;
        char sBuffer[sN];
        if (item.second.is_number()) {

            // Check for integer value
            int n = -1;
            double num = item.second.number_value();
            if ((int)num == num) {
                if (format.empty()) {
                    format = "%d";
                }
                n = std::snprintf(sBuffer, sN, format.c_str(), (int)num);
            } else {                            
                if (format.empty()) {
                    format = "%g";
                }
                n = std::snprintf(sBuffer, sN, format.c_str(), num);
            }

            // Write path variable with formatting
            if (n >= 0 && n < sN) {
                return std::string(sBuffer);
            } else {                                
                throw std::runtime_error("Path variable '" + item.first + "' cannot be formatted as number");
            }

        } else if (item.second.is_string()) {
                        
            if (format.empty()) {
                format = "%s";
            }

            // Write path variable with formatting
            int n = std::snprintf(sBuffer, sN, format.c_str(), item.second.string_value().c_str());
            if (n >= 0 && n < sN) {
                return std::string(sBuffer);
            } else {                                
                throw std::runtime_error("Path variable '" + item.first + "' cannot be formatted as string");
            }

        }                            
        throw std::runtime_error("Path variable '" + item.first + "' must be a string or number");
    }

    /**
    * Read data from remote DAP server file to raster.
    * @param url DAP url.
    * @param r raster to populate.
    * @param jsonConfig configuration parameters.
    */
    template <typename RTYPE, typename CTYPE>
    void DAPHandler<RTYPE, CTYPE>::read(std::string url, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {        
    
        // Set up layers
        uint32_t layersStart = getNullValue<uint32_t>();
        uint32_t layersEnd = getNullValue<uint32_t>();
        uint32_t layersStep = 1;

        // Parse config
        paths.clear();
        gridName.clear();
        std::string user;
        std::string password;
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            Json config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }
        
            // Get parameters
            gridName = config["grid"].string_value();
            user = config["user"].string_value();
            password = config["password"].string_value();

            // Check path string
            auto pathBase = config["path"].string_value();
            if (pathBase.empty()) {            
                throw std::runtime_error("'path' must be specified in configuration");
            }

            // Replace path variables
            if (config["pathVariables"].is_object()) {

                // Get handle to variable table
                auto &vars = config["pathVariables"].object_items();

                // Find all variable
                std::list<std::string> pathList;
                pathList.push_back(pathBase);
                std::smatch variableMatch;
                auto variableRegex = std::regex("\\{\\b\\w+\\b\\:?[\\%\\.\\:\\*\\#\\+\\-\\/\\d\\w]*\\}");

                // Construct map holding variable name and vector of original and format strings
                std::map<std::string, std::vector<std::pair<std::string, std::string> > > varMap;
                std::sregex_iterator i = std::sregex_iterator(pathBase.begin(), pathBase.end(), variableRegex);
                while (i != std::sregex_iterator()) {

                    // Create formatted output
                    std::string format;
                    std::string var = i->str();
                    var = var.substr(1, var.size()-2); // Remove enclosing braces
                    if (var.find(':') != std::string::npos) {
                        auto split = Strings::split(var, ':');
                        if (split.size() == 2) {
                            var = Strings::removeWhitespace(split[0]); 
                            format = Strings::removeWhitespace(split[1]);
                        } else {
                            throw std::runtime_error("Path variable '" + var + "' has incorrect formatting");
                        }
                    }

                    // Update list
                    varMap[var].push_back( { i->str(), format } );
                    i++;
                }

                // Replace variables
                for (auto vit = varMap.begin(); vit != varMap.end(); vit++) {

                    auto &var = vit->first;
                    auto iVar = vars.find(var);
                    if (iVar != vars.end()) {
                        std::vector<std::vector<std::pair<std::string, std::string> > > vals;
                        auto &item = *iVar;
                        if (item.second.is_number() || item.second.is_string()) {

                            // Format item
                            std::vector<std::pair<std::string, std::string> > fVals;
                            for (auto fp : vit->second) {
                                fVals.push_back( { fp.first, formatItem(item, fp.second) } );
                            }
                            vals.push_back(fVals);

                        } else if (item.second.is_array() || item.second.is_object()) {

                            // Create new path entries
                            if (item.second.is_array()) {
                                for (auto &arrayItem : item.second.array_items()) {
                            
                                    // Format items
                                    std::vector<std::pair<std::string, std::string> > fVals;
                                    for (auto fp : vit->second) {
                                        fVals.push_back( { fp.first, formatItem( { item.first, arrayItem }, fp.second) } );
                                    }
                                    vals.push_back(fVals);
                                }
                            } else {
                                if (item.second["start"].is_number() && item.second["end"].is_number()) {

                                    // Loop over range
                                    auto start = item.second["start"].number_value();
                                    auto end = item.second["end"].number_value();
                                    auto step = item.second["step"].number_value();
                                    if (step <= 0.0) {
                                        step = 1.0;
                                    }
                                    if (end > start) {
                                        for (auto val = start; val <= end; val += step) {

                                            // Format item
                                            std::vector<std::pair<std::string, std::string> > fVals;
                                            for (auto fp : vit->second) {
                                                fVals.push_back( { fp.first, formatItem( { item.first, { val } }, fp.second) } );
                                            }
                                            vals.push_back(fVals);
                                        }
                                    } else {                                
                                        throw std::runtime_error("Path variable range 'end' must be greater than 'start' value");
                                    }

                                } else if (item.second["start"].is_string() && 
                                    (item.second["end"].is_string() || item.second["steps"].is_number())) {

                                    // Create dates
                                    auto startTime = date::sys_time<std::chrono::seconds>();
                                    auto startDateString = item.second["start"].string_value();
                                    auto endTime = date::sys_time<std::chrono::seconds>();
                                    auto endDateString = item.second["end"].string_value();

                                    // Parse dates
                                    for (std::size_t i = 0; i < (endDateString.empty() ? 1 : 2); i++) {
                                        auto dateString = i == 0 ? startDateString : endDateString;
                                        std::istringstream in(dateString);
                                        if (dateString.back() == 'Z') {

                                            // Parse Zulu format
                                            in >> date::parse("%FT%TZ", i == 0 ? startTime : endTime);

                                        } else {

                                            // Parse with time zone
                                            in >> date::parse("%FT%T%Ez", i == 0 ? startTime : endTime);
                                        }

                                        if (in.fail()) {
                                            throw std::runtime_error("Cannot parse ISO8601 date '" + dateString + "'");
                                        }
                                    }

                                    // Set steps
                                    int step = (int)item.second["step"].number_value();
                                    int steps = (int)item.second["steps"].number_value();
                                    if (steps < 0) {
                                        throw std::runtime_error("Number of steps must be positive");
                                    } else if (steps == 0) {
                                        if (endTime > startTime) {
                                            if (step <= 0) {
                                                throw std::runtime_error("Path variable 'step' must be positive for date range");
                                            }
                                            for (auto t = startTime; t <= endTime; t += std::chrono::seconds(step)) {
                                                steps++;
                                            }
                                        } else {
                                            throw std::runtime_error("Path variable range 'end' must be greater than 'start' value");
                                        }
                                    } else {
                                        if (!endDateString.empty()) {
                                            throw std::runtime_error("Path variable end date or number of steps must be provided");
                                        }
                                    }
                                    
                                    // Loop over range
                                    auto t = startTime;
                                    for (int i = 0; i < steps; i++) {

                                        // Format item
                                        std::vector<std::pair<std::string, std::string> > fVals;
                                        for (auto fp : vit->second) {
                                            fVals.push_back( { fp.first, date::format(fp.second, t) } );
                                        }
                                        vals.push_back(fVals);

                                        // Increment time
                                        t += std::chrono::seconds(step);
                                    }

                                } else {                                
                                    throw std::runtime_error("Path variable range must include 'start' and 'end' numeric values");
                                }
                            }
                        } else {                                
                            throw std::runtime_error("Path variable '" + var + "' must be a string or number");
                        }

                        if (vals.empty()) {
                            throw std::runtime_error("Path variable array has no entries");
                        }

                        // Update path list
                        auto path = pathList.begin();
                        while (path != pathList.end()) {

                            // Insert additional paths
                            for (std::size_t i = 1; i < vals.size(); i++) {
                                path = pathList.insert(path, *path);
                            }

                            // Update paths
                            for (std::size_t i = 0; i < vals.size(); i++) {
                                for (std::size_t j = 0; j < vals[i].size(); j++) {
                                    *path = Strings::replace(*path, vals[i][j].first, vals[i][j].second);
                                }
                                path++;
                            }
                        };

                    } else {
                        throw std::runtime_error("Path variable '" + var + "' not found");
                    }
                }

                // Convert to vector
                paths = { std::begin(pathList), std::end(pathList) };

            } else {

                // Add single path
                paths.push_back(pathBase);
            }
            
            // Set layers
            if (config["layers"].is_number()) {

                // Set single value
                layers.push_back((uint32_t)config["layers"].number_value());

            } else if (config["layers"].is_string()) {

                // Check for slice notation
                std::string sliceString = config["layers"].string_value();
                if (sliceString.front() == '[' && sliceString.back() == ']') {

                    // Split into parts
                    auto parts = Strings::split(sliceString.substr(1, sliceString.size()-2), ':');
                    if (parts.size() > 0) {
                        layersStart = 0;
                        if (parts[0].size() > 0) {
                            layersStart = Strings::toNumber<uint32_t>(parts[0]);
                        }
                        if (parts.size() > 1) {
                            if (parts[1].size() > 0) {
                                layersEnd = Strings::toNumber<uint32_t>(parts[1]);
                            }
                            if (parts.size() > 2) {
                                layersStep = Strings::toNumber<uint32_t>(parts[2]);
                            }
                        }
                    } else {
                        throw std::runtime_error("Cannot parse layer string '" + sliceString + "'");
                    }
                } else {
                    throw std::runtime_error("Cannot parse layer string '" + sliceString + "'");
                }
            }
        }

        // Check paths
        if (paths.empty()) {            
            throw std::runtime_error("No valid paths found in configuration");
        }

        // Create client
	#if (__cplusplus < 201403L)
		pClient = httplib::detail::make_unique<Client>(url);
	#else
		pClient = std::make_unique<Client>(url);
	#endif

        auto &client = *pClient;
        if (!user.empty() && !password.empty()) {
            #ifdef CPPHTTPLIB_OPENSSL_SUPPORT
            client.set_digest_auth(user, password);
            #else 
            client.set_basic_auth(user, password);
            #endif
        }
        client.set_follow_location(true);
        client.set_keep_alive(true);
        headers.clear();
        headers.insert({"Accept-Encoding", "gzip, deflate"}); 

        // Get data descriptor object 
        auto res = client.Get(paths[0] + ".dds", headers);
        if (res.error() == Error::Success) {
            auto status = res->status;
            auto body = res->body;

            if (status == StatusCode::OK_200) {

                // Parse dds into json
                auto json = Json(parseDDSChunk(body));

                // Check json for grid descriptors
                if (!json.is_null() && json.is_object()) {

                    // Reset grids
                    grids.clear();

                    // Parse root
                    auto &root = json.object_items();
                    if (root.size() == 1) {
                        const auto &itr = root.cbegin();
                        auto rootName = itr->first;
                        if (itr->second.is_object()) {

                            // Parse root items
                            auto &rootItems = itr->second.object_items();
                            auto iRootType = rootItems.find("type");
                            auto iRootData = rootItems.find("data");
                            if (iRootType == rootItems.end() || iRootData == rootItems.end()) {
                                throw std::runtime_error("dds root object '" + rootName + "' contains no data");
                            }
                            if (iRootType->second.is_string() && iRootType->second.string_value() == "Dataset") {
                                if (iRootData->second.is_object()) {

                                    // Parse data items
                                    auto &rootItems = iRootData->second.object_items();
                                    for (const auto &rit : rootItems) {
                                        auto name = rit.first;
                                        auto &item = rit.second;
                                        if (item.is_object()) {
                                            auto &objectItems = item.object_items();
                                            auto iObjectType = objectItems.find("type");
                                            if (iObjectType == objectItems.end()) {
                                                throw std::runtime_error("dds item '" + name+ "' must contain 'type' string");
                                            }

                                            // Parse variables
                                            if (iObjectType->second.is_string()) {
                                                auto varType = iObjectType->second.string_value();
                                                if (Strings::toUpper(varType) == "GRID") {

                                                    // Get data
                                                    auto iObjectData = objectItems.find("data");
                                                    if (iObjectData == objectItems.end()) {
                                                        throw std::runtime_error("dds item '" + name + "' does not contain item data");
                                                    }
                                                    auto &varItems = iObjectData->second.object_items();

                                                    // Create grid object
                                                    DAPGrid<CTYPE> grid;

                                                    // Check for name of object in grid
                                                    auto iVarItem = varItems.find(name);
                                                    if (iVarItem == varItems.end()) {
                                                        throw std::runtime_error("dds variable '" + name + "' does not contain item data");
                                                    }
                                                    if (!iVarItem->second.is_object()) {
                                                        throw std::runtime_error("dds variable '" + name + "' does not contain item definition");
                                                    }
                                                    auto &varDef = iVarItem->second.object_items();

                                                    // Get variable type
                                                    auto iVarType = varDef.find("type");
                                                    if (iVarType == varDef.end() && iVarType->second.is_string()) {
                                                        throw std::runtime_error("dds variable '" + name + "' must contain 'type' string");
                                                    }
                                                    grid.type = Strings::toUpper(iVarType->second.string_value());
                                                    
                                                    // Check variables
                                                    auto iVarNames = varDef.find("variables");
                                                    if (iVarNames == varDef.end() && iVarNames->second.is_array()) {
                                                        throw std::runtime_error("dds variable '" + name + "' must contain 'variables' array");
                                                    }

                                                    for (const auto &vit : iVarNames->second.array_items()) {

                                                        // Check variable
                                                        if (!vit.is_string()) {
                                                            throw std::runtime_error("dds variable '" + name + "' 'variables' array must contain strings");
                                                        }
                                                        auto vName = vit.string_value();

                                                        // Get dimensions
                                                        auto dit = varItems.find(vName);
                                                        if (dit == varItems.end()) {
                                                            throw std::runtime_error("dds variable '" + name + "' does not contain definition for '" + vName + "'");
                                                        }

                                                        if (dit->second.is_object()) {
                                                            auto dimItem = dit->second.object_items();
                                                            DAPDimension<CTYPE> dim;

                                                            // Get variable type
                                                            auto iDimItemType = dimItem.find("type");
                                                            if (iDimItemType == dimItem.end()) {
                                                                throw std::runtime_error("dds dimension '" + vName + "' must contain 'type'");
                                                            }
                                                            if (!iDimItemType->second.is_string()) {
                                                                throw std::runtime_error("dds dimension '" + vName + "' must contain 'type' string");
                                                            }
                                                            
                                                            // Get variable count
                                                            auto iDimItemN = dimItem.find(vName);
                                                            if (iDimItemN == dimItem.end()) {
                                                                throw std::runtime_error("dds dimension '" + vName + "' must contain count");
                                                            }
                                                            if (!iDimItemN->second.is_number()) {
                                                                throw std::runtime_error("dds dimension '" + vName + "' must contain count number");
                                                            }

                                                            // Set dimension information
                                                            dim.name = vName;
                                                            dim.type = iDimItemType->second.string_value();
                                                            dim.n = (uint32_t)iDimItemN->second.number_value();
                                                            grid.dims.push_back(dim);

                                                        } else {
                                                            throw std::runtime_error("dds dimension '" + vName + "' contains no data");
                                                        }
                                                    }
                                                    
                                                    // Add to grid map
                                                    grids[name] = grid;
                                                }
                                            } else {
                                                throw std::runtime_error("dds item '" + name + "' contains unexpected data");
                                            }
                                        } else {
                                            throw std::runtime_error("dds item '" + name + "' contains no data");
                                        }
                                    }
                                } else {
                                    throw std::runtime_error("dds root item '" + rootName + "' has no 'data' object");
                                }
                            } else {
                                throw std::runtime_error("dds root item '" + rootName + "' must be 'Dataset' type");
                            }
                        } else {
                            throw std::runtime_error("dds root item '" + rootName + "' contains no data");
                        }
                    } else {
                        throw std::runtime_error("dds root contains more than one item");
                    }

                    // Get grid
                    if (grids.size() == 0) {
                        throw std::runtime_error("dds contains no grid definitions");
                    }
                    auto itg = grids.begin();
                    if (!gridName.empty()) {
                        itg = grids.find(gridName);
                        if (itg == grids.end()) {
                            throw std::runtime_error("dds contains no grid named '" + gridName + "'");
                        }
                    } else {
                        gridName = itg->first;
                    }
                    auto &grid = itg->second;

                    // Get dimension variables
                    for (auto &dim : grid.dims) {

                        // Update layers
                        std::string range;
                        auto dimName = Strings::toUpper(dim.name);
                        if (dimName == "Z" || dimName == "TIME") {
                            if (isInvalid<uint32_t>(layersStart)) {
                                layersStart = 0;
                            }
                            if (isInvalid<uint32_t>(layersEnd)) {
                                layersEnd = dim.n;
                            }
                            if (layersStep == 0) {
                                throw std::runtime_error("Layer step must be positive");
                            }
                            layers.clear();
                            for (std::size_t i = layersStart; i < std::min(dim.n, layersEnd); i += layersStep) {
                                layers.push_back(i);
                            }    
                            range += "[" + 
                                std::to_string(layersStart) + ":" + 
                                std::to_string(layersStep) + ":" + 
                                std::to_string(layersEnd-1) + "]";
                        }
                    
                        // Request variable data
                        std::string body;
                        auto res = client.Get(paths[0] + ".dods?" + dim.name + range, headers, 
                            [&](const char *data, size_t data_length) {
                                body.append(data, data+data_length);
                                return true;
                            }
                        );
                        auto status = res->status;
                        if (status == StatusCode::OK_200) { 

                            // Get variable data
                            auto dimData = parseData<CTYPE>(body, dim.type);

                            // Get spacing
                            CTYPE dMin = dimData[0];
                            if (dimData.size() > 1) {
                                CTYPE deltaMean = 0.0;
                                CTYPE deltaVar = 0.0;
                                for (std::size_t i = 1; i < dimData.size(); i++) {
                                    dMin = fmin(dimData[i], dMin);
                                    CTYPE d = dimData[i]-dimData[i-1];
                                    CTYPE deltaMeanLast = deltaMean;
                                    deltaMean += (d-deltaMean)/(CTYPE)i;
                                    deltaVar += (d-deltaMeanLast)*(d-deltaMean);
                                }

                                // Check spacing
                                if (deltaMean == 0.0) {
                                        throw std::runtime_error("dimensions variable '" + dim.name + "' has zero spacing");
                                    }

                                // Coefficient of variation
                                CTYPE deltaC = sqrt(deltaVar)/(fabs(deltaMean)*(CTYPE)dimData.size());
                                if (deltaC > 1.0E-3) {
                                        throw std::runtime_error("dimensions variable '" + dim.name + "' must have equal spacing");
                                    }

                                // Set spacing
                                dim.delta = deltaMean;
                            }

                            // Set default spacing for single layers
                            if (dim.delta == 0) {
                                dim.delta = 1;
                            }

                            // Set origin
                            dim.origin = dMin;

                        } else {
                            std::string err = "Connection error: " + body;
                            throw std::runtime_error(err);
                        }
                    }
                    
                    // Set raster dimensions
                    Dimensions<CTYPE> dims = Dimensions<CTYPE>();
                    for (auto &dim : grid.dims) {
                        auto dimName = Strings::toUpper(dim.name);
                        if (dimName == "X" || dimName == "LON" || dimName == "LONGITUDE") {
                            dims.nx = dim.n;
                            dims.hx = fabs(dim.delta);
                            dims.ox = dim.origin-0.5*dims.hx;
                        } else if (dimName == "Y" || dimName == "LAT" || dimName == "LATITUDE") {
                            dims.ny = dim.n;
                            dims.hy = fabs(dim.delta);
                            dims.oy = dim.origin-0.5*dims.hy;
                        } else if (dimName == "Z" || dimName == "TIME") {
                            dims.nz = dim.n;
                            dims.hz = fabs(dim.delta);  // TODO May be incorrect when layers are used
                            dims.oz = dim.origin;       // TODO May be incorrect when layers are used
                        } else {
                            throw std::runtime_error("dds dimension name '" + dim.name + "' not recognised");
                        }
                    }
                    if (dims.nz == 0) {
                        dims.nz = 1;
                        dims.hz = 1.0;
                    }
                    dims.nz*=paths.size();
                    if (layers.size() == 0) {
                        layers.push_back(0);
                    }
                    r.init(dims);
            
                    // Register data callback
                    using namespace std::placeholders;
                    RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
                        std::bind(&DAPHandler<RTYPE, CTYPE>::readDataFunction, this, _1, _2, _3));

                } else {
                    throw std::runtime_error("dds contains no data");
                }
            } else {
                std::string err = "Connection error: " + body;
                throw std::runtime_error(err);
            }
        } else {
            std::string err = "Cannot connect to url: " + to_string(res.error());
            throw std::runtime_error(err);
        }

        res = client.Get(paths[0] + ".das", headers);
        if (res.error() == Error::Success) {
            auto status = res->status;
            auto body = res->body;

            if (status == StatusCode::OK_200) {

                // Parse das
                auto json = Json(parseDASChunk(body));

                // Get grid data
                noData = json["Attributes"]["data"][gridName]["data"]["_FillValue"]["data"].string_value();

            } else {
                std::string err = "Connection error: " + body;
                throw std::runtime_error(err);
            }
        } else {
            std::string err = "Cannot connect to url: " + to_string(res.error());
            throw std::runtime_error(err);
        }
    }

    /**
    * Supply data to %Tile through callback function
    * @param tdim %TileDimensions of tile.
    * @param v vector to fill.
    * @param r %Raster handle.
    */
    template <typename RTYPE, typename CTYPE>
    void DAPHandler<RTYPE, CTYPE>::readDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r) {
    
        // Get client
        if (pClient) {
            auto &client = *pClient;

            // Get raster name
            auto dims = r.getRasterDimensions();

            // Get grid
            if (gridName.empty()) {
                throw std::runtime_error("No named grid in read");
            }
            auto itg = grids.find(gridName);
            if (itg == grids.end()) {
                throw std::runtime_error("Cannot read grid '" + gridName + "'");
            }
            auto &grid = itg->second;

            // Check paths
            if (paths.empty()) {            
                throw std::runtime_error("No valid paths found in configuration");
            }

            // Create range and check dimensions
            uint32_t kIndex = getNullValue<uint32_t>();
            std::string range[3];
            uint32_t iRange = 0;
            uint32_t jRange = 0;
            bool flip_j = false;
            for (std::size_t i = 0; i < grid.dims.size(); i++) {
                auto &dim = grid.dims[i];
                auto dimName = Strings::toUpper(dim.name);
                if (dimName == "X" || dimName == "LON" || dimName == "LONGITUDE") {
                    if (dims.d.nx != dim.n) {
                        throw std::runtime_error("Grid '" + gridName + "' x-dimensions differ in read");
                    }
                    uint32_t iStart = tdim.ti*TileMetrics::tileSize;
                    uint32_t iEnd = std::min(iStart+TileMetrics::tileSize, dims.d.nx);
                    iRange = iEnd-iStart;
                    range[i] = "[" + std::to_string(iStart) + ":" + std::to_string(iEnd-1) + "]";
                } else if (dimName == "Y" || dimName == "LAT" || dimName == "LATITUDE") {
                    if (dims.d.ny != dim.n) {
                        throw std::runtime_error("Grid '" + gridName + "' y-dimensions differ in read");
                    }
                    int64_t jStart, jEnd;
                    if (dim.delta > 0) {
                        jStart = tdim.tj*TileMetrics::tileSize;
                        jEnd = jStart+TileMetrics::tileSize;
                        if (jEnd > dims.d.ny) {
                            jEnd = dims.d.ny;
                        }
                        flip_j = false;
                    } else {
                        uint32_t dj = dims.tx*TileMetrics::tileSize-dims.d.ny;
                        jStart = (int64_t)(dims.tx-1-tdim.tj)*TileMetrics::tileSize-dj;
                        jEnd = jStart+TileMetrics::tileSize;
                        if (jStart < 0) {
                            jStart = 0;
                        }
                        flip_j = true;
                    }
                    jRange = jEnd-jStart;
                    range[i] = "[" + std::to_string(jStart) + ":" + std::to_string(jEnd-1) + "]";
                } else if (dimName == "Z" || dimName == "TIME") {
                    //if (dims.d.nz != dim.n) {
                    //    throw std::runtime_error("Grid '" + name + "' z-dimensions differ in read");
                    //} TODO CANNOT CHECK AS LAYERS OVERWRITES
                    kIndex = i;
                    range[i] = "[0:0]";
                } else {
                    throw std::runtime_error("dds dimension name '" + dimName + "' not recognised");
                }
            }
            if (iRange == 0 || jRange == 0) {
                throw std::runtime_error("Zero range in grid dimensions");
            }

            // Get verbose handle
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            uint8_t verbose = solver.getVerboseLevel();

            // Request grid data
            for (std::size_t p = 0; p < paths.size(); p++) {
                for (auto k : layers) {

                    // Set z range
                    if (isValid<uint32_t>(kIndex)) {
                        auto kStr = std::to_string(k);
                        range[kIndex] = "[" + kStr + ":" + kStr + "]";
                    }

                    // Adjust k for paths
                    k += p*layers.size();

                    // Create path
                    std::string path = paths[p] + ".dods?" + gridName + range[0] + range[1] + range[2];
                    if (verbose <= Geostack::Verbosity::Debug) {
                        std::cout << path << std::endl;
                    }

                    // Get data
                    std::string body;
                    auto res = client.Get(path, headers, 
                        [&](const char *data, size_t data_length) {
                            body.append(data, data+data_length);
                            return true;
                        }
                    );
                    if (res) {
                        auto status = res->status;
                        if (status == StatusCode::OK_200) {
                            auto values = parseData<RTYPE>(body, grid.type);

                            // Patch nodata values
                            if (!noData.empty()) {
                                RTYPE noDataValue = Strings::toNumber<RTYPE>(noData);
                                std::replace(values.begin(), values.end(), noDataValue, getNullValue<RTYPE>());
                            }

                            if (flip_j) {
                                uint32_t sourceOff = (jRange-1)*iRange;
                                auto destOff = v.begin()+k*TileMetrics::tileSizeSquared;
                                for (std::size_t j = 0; j < jRange; j++, sourceOff-=iRange, destOff+=TileMetrics::tileSize) {
                                    std::copy(values.begin()+sourceOff, values.begin()+sourceOff+iRange, destOff);
                                }
                            } else {
                                auto iSource = values.begin();
                                uint32_t sourceOff = 0;
                                auto destOff = v.begin()+k*TileMetrics::tileSizeSquared;
                                for (std::size_t j = 0; j < jRange; j++, sourceOff+=iRange, destOff+=TileMetrics::tileSize) {
                                    std::copy(values.begin()+sourceOff, values.begin()+sourceOff+iRange, destOff);
                                }
                            }
                        } else {
                            std::cout << "WARNING: Ignoring layer, bad response for path " << paths[p] << std::endl;
                        }
                    } else {
                        std::cout << "WARNING: Ignoring layer, response error for path " << paths[p] << std::endl;
                    }
                }
            }

        } else {
            throw std::runtime_error("No connection available");
        }
    }

    /**
    * Write raster to DAP.
    * @param fileName output file name.
    * @param r raster to write.
    * @param jsonConfig write configuration.
    */
    template <typename RTYPE, typename CTYPE>
    void DAPHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {
        throw std::runtime_error("DAP write functions are not available");
    }
    
    // RTYPE float, CTYPE float definitions
    template class DAPHandler<float, float>;
    
    // RTYPE uchar, CTYPE float definitions
    template class DAPHandler<uint8_t, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class DAPHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class DAPHandler<double, double>;

    // RTYPE uchar, CTYPE double definitions
    template class DAPHandler<uint8_t, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class DAPHandler<uint32_t, double>;
}
