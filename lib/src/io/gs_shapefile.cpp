/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <algorithm>

#include "gs_string.h"
#include "gs_projection.h"
#include "gs_shapefile.h"

#if !defined(ENDIAN_BIG) && !defined(ENDIAN_LITTLE)
#error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
#endif

using namespace json11;

namespace Geostack
{
    
    /**
    * Enum for endian type.
    */
    namespace EndianType {
        enum Type {
            Big,
            Little
        };
    }

    /**
    * Enum for shape type.
    */
    namespace ShapeType {
        enum Type {
            Null = 0,
            Point = 1,
            Polyline = 3,
            Polygon = 5,
            MultiPoint = 8,
            PointZ = 11,
            PolylineZ = 13,
            PolygonZ = 15,
            MultiPointZ = 18,
            PointM = 21,
            PolylineM = 23,
            PolygonM = 25,
            MultiPointM = 28,
            MultiPatch = 31
        };
    }
    
    /**
    * Enum for DBF type.
    */
    namespace DType {
        enum Type {
            None,
            String,
            Float,
            Integer
        };
    }
    
    /**
    * Class to hold DBF field information for reading.
    */
    class DFieldRead {
    
    public:

        DFieldRead(): type(DType::None), ignore(false) { }
        ~DFieldRead() { }
        
        std::string name;
        DType::Type type;
        std::vector<char> data;
        bool ignore;
    };
    
    /**
    * Class to hold DBF field remapping information
    */
    class DFieldRemap {
    
    public:

        DFieldRemap(): type(DType::None) { }
        ~DFieldRemap() { }

        DType::Type type;
        std::string name;
    };
    
    /**
    * Flip endian of variable.
    * @param v big endian variable.
    * @return variable in system endian.
    */
    template <typename T>
    static T swapEndian(const T &v) {

        // Create local union
        union {
            T va;
            uint8_t vb[sizeof(T)];
        } s, d;
        
        // Swap bytes
        s.va = v;
        for (size_t k = 0; k < sizeof(T); k++)
            d.vb[k] = s.vb[sizeof(T)-k-1];
        return d.va;
    }
    
    /**
    * Convert big endian variable to or from system endian.
    * @param v input variable.
    * @return output variable.
    */
    template <typename T>
    static T convertBigEndian(const T &v) {
    
        #if defined(ENDIAN_LITTLE)
        return swapEndian(v);
        #else
        return v;
        #endif
    }
    
    /**
    * Convert little endian variable to or from system endian.
    * @param v input variable.
    * @return output variable.
    */
    template <typename T>
    static T convertLittleEndian(const T &v) {
    
        #if defined(ENDIAN_BIG)
        return swapEndian(v);
        #else
        return v;
        #endif
    }

    /**
    * Read data from stream into variable of type T.
    * @param f file stream.
    * @param v variable.
    * @param endianType Endian type in file
    * @return true if data is read, false otherwise.
    */
    template <typename T>
    static bool readFromStream(std::ifstream &f, T &v, EndianType::Type endianType) {
        
        // Read from file
        f.read(reinterpret_cast<char *>(&v), sizeof(T));
        if (f.eof())
            return false;

        // Process endian
        if (endianType == EndianType::Big) {
            v = convertBigEndian(v);
        } else if (endianType == EndianType::Little) {
            v = convertLittleEndian(v);
        }
        return true;
    }
    
    /**
    * Write T into data stream.
    * @param f file stream.
    * @param v variable.
    * @param endianType Endian type to write to file
    * @return true if data is written, false otherwise.
    */
    template <typename T>
    static bool writeToStream(std::ofstream &f, T v, EndianType::Type endianType) {        

        // Process endian
        if (endianType == EndianType::Big) {
            v = convertBigEndian(v);
        } else if (endianType == EndianType::Little) {
            v = convertLittleEndian(v);
        }

        // Write to file
        f.write(reinterpret_cast<char *>(&v), sizeof(T));

        return true;
    }

    /**
    * Shapefile reader.
    * Converts a Shapefile to a %Vector object
    * @param shapefileName filename.
    * @param jsonConfig reader configuration.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> ShapeFile<T>::shapefileToVector(std::string shapefileName, std::string jsonConfig) {
        return ShapeFile<T>::shapefileToVectorWithBounds(shapefileName, BoundingBox<T>(),
                ProjectionParameters<double>(), jsonConfig);
    }

    /**
    * Shapefile reader.
    * Converts a Shapefile to a %Vector object
    * @param shapefileName filename.
    * @param Geostack %BoundingBox object.
    * @param Geostack %ProjectionParameters object.
    * @param jsonConfig reader configuration.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> ShapeFile<T>::shapefileToVectorWithBounds(std::string shapefileName,
        BoundingBox<T> boundingRegion, ProjectionParameters<double> boundRegionProj,
        std::string jsonConfig) {

        // Create return Vector
        Vector<T> v;

        // Parse config
        std::map<std::string, DFieldRemap> fieldRemap;
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }

            if (config["properties"].is_array()) {

                // Create items
                for (auto &p : config["properties"].array_items()) {
                    auto obj = p.object_items();
                    if (obj["name"].is_string()) {                        

                        // Set type
                        DFieldRemap remap;
                        if (obj["type"].is_string()) {
                            auto type = Strings::toUpper(obj["type"].string_value());
                            if (type == "STRING") {
                                remap.type = DType::String;
                            } else if (type == "REAL") {
                                remap.type = DType::Float;
                            } else if (type == "INTEGER") {
                                remap.type = DType::Integer;
                            }
                        }

                        // Set name
                        if (obj["rename"].is_string()) {
                            remap.name = obj["rename"].string_value();
                        }

                        // Add remapping information
                        fieldRemap[obj["name"].string_value()] = remap;
                    }
                }

            }
        }

        //Open dbf file
        auto fileNameSplit = Geostack::Strings::splitPath(shapefileName);

        // Set projection
        std::string projFileName = fileNameSplit[0] + fileNameSplit[1] + ".prj";
        std::ifstream fp(projFileName);
        if (fp) {

            // Read file
            std::stringstream fps;
            fps << fp.rdbuf();

            // Parse projection
            auto proj = Projection::parseWKT(fps.str());

            // Set projection
            v.setProjectionParameters(proj);
        }

        // Open dbf file
        std::string dbfFileName = fileNameSplit[0] + fileNameSplit[1] + ".dbf";
        std::ifstream fd(dbfFileName, std::ios::in | std::ios::binary);
        std::vector<DFieldRead> dFields;
        if (fd) {

            // Read dbf header
            fd.seekg(4, std::ios_base::cur);

            uint32_t nRecords;
            readFromStream(fd, nRecords, EndianType::Little);
            
            int16_t headerLength;
            readFromStream(fd, headerLength, EndianType::Little);

            int16_t recordLength;
            readFromStream(fd, recordLength, EndianType::Little);

            // Skip to end of header
            fd.seekg(20, std::ios_base::cur);

            // Calculate number of fields
            uint32_t nFields = ((headerLength-1)>>5)-1;
            
            // Read fields
            std::vector<char> nameData;
            for (uint32_t i = 0; i < nFields; i++) {

                // Create record
                DFieldRead dField;

                // Read name as bytes 1-11
                nameData.assign(12, 0);
                fd.read(nameData.data(), 11);

                // Convert name data to string
                dField.name = std::string(nameData.data());
                dField.name = Strings::removeWhitespace(dField.name);

                // Read type
                uint8_t type;
                readFromStream(fd, type, EndianType::Little);

                // Skip bytes
                fd.seekg(4, std::ios_base::cur);

                // Read length
                uint8_t length;
                readFromStream(fd, length, EndianType::Little);
                dField.data.resize(length);
                
                // Read decimals
                uint8_t decimals;
                readFromStream(fd, decimals, EndianType::Little);
                
                // Set type
                dField.type = DType::None;
                switch (type) {
                
                    // Ascii data
                    case 'C': 
                    case 'D':
                    case 'L': 
                    case 'M': 

                        dField.type = DType::String;
                        break;

                    // Numeric data
                    case 'N':                         
                        dField.type = decimals == 0 ? DType::Integer : DType::Float;
                        break;

                    case 'F': 
                        dField.type = DType::Float;
                        break;
                }

                // Apply type conversion 
                if (fieldRemap.size() > 0) {

                    // Get name
                    auto it = fieldRemap.find(dField.name);
                    if (it != fieldRemap.end()) {
                        if (it->second.type != DType::None) {
                            dField.type = it->second.type;
                        }
                        if (!it->second.name.empty()) {
                            dField.name = it->second.name;
                        }
                    } else {

                        // Ignore all unspecified fields
                        dField.ignore = true;
                    }
                }

                // Add to fields
                dFields.push_back(dField);
                
                // Skip to end of field
                fd.seekg(14, std::ios_base::cur);
            }
            
            // Look for check character
            char check = 0;
            readFromStream(fd, check, EndianType::Little);
            if (fd.eof() || check != 0x0D) {
                throw std::runtime_error("Unexpected end of shapefile database");
            }
        }

        // Open shapefile
        std::ifstream fs(shapefileName, std::ios::in | std::ios::binary);
        if (fs) {

            // Store file length
            fs.seekg(0, std::ios::end);
            auto fileEnd = fs.tellg();
            fs.seekg(0, std::ios::beg);

            // Check data exists for 100 byte header
            std::streampos nextRecord = fs.tellg()+(std::streamoff)100;            
            if (nextRecord > fileEnd) {
                throw std::runtime_error("Unexpected end of shapefile header");
            }

            // Read header
            uint32_t fileCode;
            readFromStream(fs, fileCode, EndianType::Big);
            if (fileCode != 9994) {
                throw std::runtime_error("Shapefile start code must be 9994");
            }

            // Skip bytes
            fs.seekg(20, std::ios_base::cur);

            uint32_t fileLength;
            readFromStream(fs, fileLength, EndianType::Big);

            uint32_t version;
            readFromStream(fs, version, EndianType::Little);
            if (version != 1000) {
                throw std::runtime_error("Only shapefile version 1000 is supported");
            }

            uint32_t shapeType;
            readFromStream(fs, shapeType, EndianType::Little);

            // Shape bounds
            double xmin, xmax, ymin, ymax, zmin, zmax, mmin, mmax;
            readFromStream(fs, xmin, EndianType::Little);
            readFromStream(fs, ymin, EndianType::Little);
            readFromStream(fs, xmax, EndianType::Little);
            readFromStream(fs, ymax, EndianType::Little);
            readFromStream(fs, zmin, EndianType::Little);
            readFromStream(fs, zmax, EndianType::Little);
            readFromStream(fs, mmin, EndianType::Little);
            readFromStream(fs, mmax, EndianType::Little);
            
            bool hasBoundingRegion = false;
            bool intersectionContains = false;
            if (boundingRegion != BoundingBox<T>() && boundingRegion.area2D() > 0){
                hasBoundingRegion = true;
                auto geomBounds = BoundingBox<T>( { {(T)xmin, (T)ymin}, {(T)xmax, (T)ymax} } );
                
                if (boundRegionProj != ProjectionParameters<double>() &&
                    v.getProjectionParameters() != ProjectionParameters<double>()) {
                    if (v.getProjectionParameters() != boundRegionProj) {
                        boundingRegion = boundingRegion.convert(v.getProjectionParameters(),
                            boundRegionProj);
                    }
                }
                intersectionContains = (BoundingBox<T>::boundingBoxIntersects(
                    boundingRegion, geomBounds) || BoundingBox<T>::boundingBoxContains(
                    boundingRegion, geomBounds));
                
                if (!intersectionContains) {
                    throw std::runtime_error("No overlap in bounding region and vector file");
                }   
            }

            // Measure data flag, no-data represented by numbers less than -1.0E38
            bool hasM = (mmin >= -1.0E38) && (mmax >= -1.0E38);

            // Parse shape data
            uint32_t recordNumber;
            uint32_t contentLength;
            uint32_t recordType;
            std::vector<uint32_t> parts;
            std::vector<Coordinate<double> > coords;
            std::vector<cl_uint> newIndexes;

            readFromStream(fs, recordNumber, EndianType::Big);
            while (!fs.eof()) {

                // Read record header
                readFromStream(fs, contentLength, EndianType::Big);

                // Set position of next record
                nextRecord = fs.tellg()+(std::streamoff)((uint64_t)contentLength<<1); // contentLength in 16-bit words

                // Check data exists for the next record
                if (nextRecord > fileEnd) {
                    throw std::runtime_error("Unexpected end of shapefile record");
                }

                // Read the record type
                readFromStream(fs, recordType, EndianType::Little);

                // Skip null records
                newIndexes.clear();
                switch(recordType) {

                    case ShapeType::Null:
                    break;

                    case ShapeType::Point:
                    case ShapeType::PointM:
                    case ShapeType::PointZ:
                    {
                        Coordinate<double> coord;

                        // Read x and y coordinates
                        readFromStream(fs, coord.p, EndianType::Little);
                        readFromStream(fs, coord.q, EndianType::Little);

                        if (recordType == ShapeType::PointZ) {

                            // Read z coordinate
                            readFromStream(fs, coord.r, EndianType::Little);
                        } 
                        
                        if (hasM && (fs.tellg() < nextRecord) &&
                            (recordType == ShapeType::PointZ || recordType == ShapeType::PointM)) {

                            // Read m coordinate
                            readFromStream(fs, coord.s, EndianType::Little);
                        }

                        intersectionContains = true;
                        if (hasBoundingRegion) {
                            auto geomBounds = BoundingBox<T>({ {(T)coord.p, (T)coord.q}, {(T)coord.p, (T)coord.q} });
                            intersectionContains = BoundingBox<T>::boundingBoxContains(
                                boundingRegion, geomBounds) || BoundingBox<T>::boundingBoxIntersects(
                                boundingRegion, geomBounds);
                        }

                        if (intersectionContains) {
                            // Add point
                            newIndexes.push_back(v.addPoint( { (T)coord.p, (T)coord.q, (T)coord.r, (T)coord.s } ));                            
                        }

                    } break;

                    case ShapeType::MultiPoint:
                    case ShapeType::MultiPointM:
                    case ShapeType::MultiPointZ:
                    {
                        // Read bounds
                        double bounds[4];
                        readFromStream(fs, bounds[0], EndianType::Little);
                        readFromStream(fs, bounds[1], EndianType::Little);
                        readFromStream(fs, bounds[2], EndianType::Little);
                        readFromStream(fs, bounds[3], EndianType::Little);

                        // Read counts
                        uint32_t nCoords;
                        readFromStream(fs, nCoords, EndianType::Little);

                        // Read parts
                        coords.resize(nCoords);

                        // Read x and y coordinates
                        for (uint32_t i = 0; i < nCoords; i++) {
                            readFromStream(fs, coords[i].p, EndianType::Little);
                            readFromStream(fs, coords[i].q, EndianType::Little);
                        }

                        if (recordType == ShapeType::MultiPointZ) {
                        
                            // Read z range
                            double zMin, zMax;
                            readFromStream(fs, zMin, EndianType::Little);
                            readFromStream(fs, zMax, EndianType::Little);

                            // Read z coordinates
                            for (uint32_t i = 0; i < nCoords; i++) {
                                readFromStream(fs, coords[i].r, EndianType::Little);
                            }

                        } 
                        
                        if (hasM && (fs.tellg() < nextRecord) &&
                            (recordType == ShapeType::MultiPointZ || recordType == ShapeType::MultiPointM)) {
                            
                            // Read m range
                            double mMin, mMax;
                            readFromStream(fs, mMin, EndianType::Little);
                            readFromStream(fs, mMax, EndianType::Little);

                            // Read m coordinates
                            for (uint32_t i = 0; i < nCoords; i++) {
                                readFromStream(fs, coords[i].s, EndianType::Little);
                            }
                        }

                        intersectionContains = true;
                        if (hasBoundingRegion) {
                            auto geomBounds = BoundingBox<T>({ {(T)bounds[0], (T)bounds[1]}, {(T)bounds[2], (T)bounds[3]} });
                            intersectionContains = BoundingBox<T>::boundingBoxContains(
                                boundingRegion, geomBounds) || BoundingBox<T>::boundingBoxIntersects(
                                boundingRegion, geomBounds);
                        }
                        
                        // Add points
                        if (intersectionContains) {
                            for (auto &coord : coords) {
                                newIndexes.push_back(v.addPoint( { (T)coord.p, (T)coord.q, (T)coord.r, (T)coord.s } ));
                            }
                        }

                    } break;
                    
                    case ShapeType::Polyline:
                    case ShapeType::PolylineM:
                    case ShapeType::PolylineZ:
                    case ShapeType::Polygon:
                    case ShapeType::PolygonM:
                    case ShapeType::PolygonZ:
                    {

                        // Read bounds
                        double bounds[4];
                        readFromStream(fs, bounds[0], EndianType::Little);
                        readFromStream(fs, bounds[1], EndianType::Little);
                        readFromStream(fs, bounds[2], EndianType::Little);
                        readFromStream(fs, bounds[3], EndianType::Little);
                        
                        // Read counts
                        uint32_t nParts;
                        uint32_t nCoords;
                        readFromStream(fs, nParts, EndianType::Little);
                        readFromStream(fs, nCoords, EndianType::Little);

                        // Read parts
                        parts.resize(nParts);
                        coords.resize(nCoords);

                        // Read parts
                        for (uint32_t i = 0; i < nParts; i++) {
                            readFromStream(fs, parts[i], EndianType::Little);
                        }
                        parts.push_back(nCoords);

                        // Read x and y coordinates
                        for (uint32_t i = 0; i < nCoords; i++) {
                            readFromStream(fs, coords[i].p, EndianType::Little);
                            readFromStream(fs, coords[i].q, EndianType::Little);
                        }

                        if (recordType == ShapeType::PolylineZ || recordType == ShapeType::PolygonZ) {
                        
                            // Read z range
                            double zMin, zMax;
                            readFromStream(fs, zMin, EndianType::Little);
                            readFromStream(fs, zMax, EndianType::Little);

                            // Read z coordinates
                            for (uint32_t i = 0; i < nCoords; i++) {
                                readFromStream(fs, coords[i].r, EndianType::Little);
                            }

                        } 
                        
                        if (hasM && (fs.tellg() < nextRecord) &&
                            (recordType == ShapeType::PolylineZ || recordType == ShapeType::PolygonZ ||
                             recordType == ShapeType::PolylineM || recordType == ShapeType::PolygonM)) {
                            
                            // Read m range
                            double mMin, mMax;
                            readFromStream(fs, mMin, EndianType::Little);
                            readFromStream(fs, mMax, EndianType::Little);

                            // Read m coordinates
                            for (uint32_t i = 0; i < nCoords; i++) {
                                readFromStream(fs, coords[i].s, EndianType::Little);
                            }
                        }

                        // Create geometry
                        CoordinateList<T> cs;
                        std::vector<CoordinateList<T> > pcs;
                        for (uint32_t p = 0; p < nParts; p++) {
                                
                            // Add part
                            cs.clear();
                            for (auto i = parts[p]; i < parts[p+1]; i++) {
                                cs.push_back( { (T)coords[i].p, (T)coords[i].q, (T)coords[i].r, (T)coords[i].s } );
                            }
                                
                            // Add extra Polygon node if first and last nodes are different
                            if (recordType == ShapeType::Polygon) {
                                if (cs.front() != cs.back())
                                    cs.push_back(cs.front());
                            }

                            // Check winding using stabilised shoelace formula
                            // Shapefiles should have clockwise exterior rings (A < 0.0), counter-clockwise interior rings (A > 0.0)
                            double A = 0.0;
                            double xMean = 0.0;
                            double yMean = 0.0;
                            for (std::size_t i = 0; i < cs.size()-1; i++) {
                                xMean += cs[i].p;
                                yMean += cs[i].q;
                            }
                            xMean/=(double)(cs.size()-1);
                            yMean/=(double)(cs.size()-1);
                            for (std::size_t i = 0; i < cs.size()-1; i++) {
                                A += (cs[i].p-xMean)*(cs[i+1].q-yMean)-
                                    (cs[i+1].p-xMean)*(cs[i].q-yMean);
                            }

                            // Split exterior rings into multiple polygons
                            if (recordType == ShapeType::Polygon || 
                                recordType == ShapeType::PolygonZ ||
                                recordType == ShapeType::PolygonM) {
                            
                                // Flip order, Geostack polygons have opposite winding
                                std::reverse(cs.begin(), cs.end());

                                // Check for separate exterior ring
                                if (p > 0 && A < 0.0) {

                                    // Add polygon
                                    newIndexes.push_back(v.addPolygon(pcs));
                                    pcs.clear();
                                }
                            }

                            // Add to polygon coordinate list
                            pcs.push_back(cs);
                        }

                        // check if bounds intersects user supplied bounds                        
                        intersectionContains = true;
                        if (hasBoundingRegion) {
                            auto geomBounds = BoundingBox<T>({ {(T)bounds[0], (T)bounds[1]}, {(T)bounds[2], (T)bounds[3]} });
                            intersectionContains = BoundingBox<T>::boundingBoxContains(
                                boundingRegion, geomBounds) || BoundingBox<T>::boundingBoxIntersects(
                                boundingRegion, geomBounds);
                        }

                        if (intersectionContains) {
                            if (recordType == ShapeType::Polygon || 
                                recordType == ShapeType::PolygonZ ||
                                recordType == ShapeType::PolygonM) {

                                // Add polygon
                                newIndexes.push_back(v.addPolygon(pcs));

                            } else if (recordType == ShapeType::Polyline || 
                                    recordType == ShapeType::PolylineZ || 
                                    recordType == ShapeType::PolylineM) {

                                // Add line strings
                                for (auto &lcs : pcs) {
                                    newIndexes.push_back(v.addLineString(lcs));
                                }
                            }
                        }

                    } break;

                    default:
                        std::stringstream err;
                        err << "Unsupported shape type '" << recordType << "'";
                        throw std::runtime_error(err.str());
                }
                
                // Add properties
                if (fd) {

                    // Look for check character
                    char check = 0;
                    readFromStream(fd, check, EndianType::Little);
                    if (check != 0x20 && check != 0x2A) {
                        throw std::runtime_error("Cannot find record header in shapefile database");
                    }

                    for (auto &dField : dFields) {

                        // Read data
                        fd.read(dField.data.data(), dField.data.size());
                        if (fd.eof()) {
                            throw std::runtime_error("Unexpected end of shapefile database");
                        }

                        // Skip record
                        if (dField.ignore) {
                            continue;
                        }

                        // Convert data to string
                        std::string strData = std::string(dField.data.begin(), dField.data.end());
                        strData = Strings::removeWhitespace(strData);

                        // Convert name data to string
                        std::string name(dField.name.data());
                        name = Strings::removeWhitespace(name);

                        try {
                            switch(recordType) {

                                case ShapeType::Point:
                                case ShapeType::PointM:
                                case ShapeType::PointZ:
                                case ShapeType::MultiPoint:
                                case ShapeType::MultiPointM:
                                case ShapeType::MultiPointZ:
                                
                                    // Set point property
                                    switch (dField.type) {
                    
                                        case DType::String:
                                            for (auto &index : newIndexes) {
                                                v.setProperty(index, dField.name, strData);
                                            }
                                            break;

                                        case DType::Float:
                                            for (auto &index : newIndexes) {
                                                if (strData.size() != 0) 
                                                    v.setProperty(index, dField.name, 
                                                        Strings::toNumberWithDefault<T>(strData, getNullValue<T>()));
                                                else
                                                    v.setProperty(index, dField.name, getNullValue<T>());
                                            }
                                            break;

                                        case DType::Integer:
                                            for (auto &index : newIndexes) {
                                                if (strData.size() != 0) 
                                                    v.setProperty(index, dField.name, 
                                                        Strings::toNumberWithDefault<int32_t>(strData, getNullValue<int32_t>()));
                                                else
                                                    v.setProperty(index, dField.name, getNullValue<int32_t>());
                                            }
                                            break;
                                        
                                        default:
                                            break;
                                    }
                                    break;

                                case ShapeType::Polyline:
                                case ShapeType::PolylineM:
                                case ShapeType::PolylineZ:

                                    // Set line property
                                    switch (dField.type) {
                    
                                        case DType::String:
                                            for (auto &index : newIndexes) {
                                                v.setProperty(index, dField.name, strData);
                                            }
                                            break;

                                        case DType::Float:
                                            for (auto &index : newIndexes) {
                                                if (strData.size() != 0) 
                                                    v.setProperty(index, dField.name, 
                                                        Strings::toNumberWithDefault<T>(strData, getNullValue<T>()));
                                                else
                                                    v.setProperty(index, dField.name, getNullValue<T>());
                                            }
                                            break;

                                        case DType::Integer:
                                            for (auto &index : newIndexes) {
                                                if (strData.size() != 0) 
                                                    v.setProperty(index, dField.name, 
                                                        Strings::toNumberWithDefault<int32_t>(strData, getNullValue<int32_t>()));
                                                else
                                                    v.setProperty(index, dField.name, getNullValue<int32_t>());
                                            }
                                            break;
                                        
                                        default:
                                            break;
                                    }
                                    break;
                                            
                                case ShapeType::Polygon:
                                case ShapeType::PolygonM:
                                case ShapeType::PolygonZ:

                                    // Set polygon property
                                    switch (dField.type) {
                    
                                        case DType::String:
                                            for (auto &index : newIndexes) {
                                                v.setProperty(index, dField.name, strData);
                                            }
                                            break;

                                        case DType::Float:
                                            for (auto &index : newIndexes) {
                                                if (strData.size() != 0) 
                                                    v.setProperty(index, dField.name, 
                                                        Strings::toNumberWithDefault<T>(strData, getNullValue<T>()));
                                                else
                                                    v.setProperty(index, dField.name, getNullValue<T>());
                                            }
                                            break;

                                        case DType::Integer:
                                            for (auto &index : newIndexes) {
                                                if (strData.size() != 0) 
                                                    v.setProperty(index, dField.name, 
                                                        Strings::toNumberWithDefault<int32_t>(strData, getNullValue<int32_t>()));
                                                else
                                                    v.setProperty(index, dField.name, getNullValue<int32_t>());
                                            }
                                            break;
                                        
                                        default:
                                            break;
                                    }
                                    break;
                            }
                        } catch (const std::exception& e) {
                            std::stringstream err;
                            err << "Unable to parse field = " << dField.name << ", value = " << strData << "\n";
                            err << e.what();
                            throw std::runtime_error(err.str());
                        }
                    }
                }

                // Check file length
                if (fs.tellg() >= (std::streamoff)((uint64_t)fileLength<<1)) // fileLength in 16-bit words
                    break;

                // Jump to next record
                fs.seekg(nextRecord);
                readFromStream(fs, recordNumber, EndianType::Big);
            }

        } else {
            std::stringstream err;
            err << "Cannot open '" << shapefileName << "'";
            throw std::runtime_error(err.str());
        }

        // Return Vector
        return v;
    }

    /**
    * Shapefile writer.
    * Write a %Vector to a Shapefile
    * @param shapefileName filename.
    * @return true if successfully written.
    */
    template <typename T>
    bool ShapeFile<T>::vectorToShapefile(const Geostack::Vector<T> &v, std::string shapefileName, GeometryType::Type writeType) {
    
        // Split file name
        auto fileNameSplit = Geostack::Strings::splitPath(shapefileName);

        // Get geometry
        auto &geometryIndexes = v.getGeometryIndexes();

        // Get properties
        auto &properties = v.getProperties();
        auto stringProperties = properties.template getPropertyRefs<std::vector<std::string> >();
        auto intProperties = properties.template getPropertyRefs<std::vector<int> >();
        auto floatProperties = properties.template getPropertyRefs<std::vector<float> >();
        auto doubleProperties = properties.template getPropertyRefs<std::vector<double> >();
        auto indexProperties = properties.template getPropertyRefs<std::vector<cl_uint> >();

        // Open dbf file
        std::string dbfFileName = fileNameSplit[0] + fileNameSplit[1] + ".dbf";
        std::ofstream fd(dbfFileName, std::ios::out | std::ios::binary);
        if (!fd) {

            // Could not create database file
            std::stringstream err;
            err << "Could not create shapefile database '" << dbfFileName << "'";
            throw std::runtime_error(err.str());

        } else {
            
            // Store start position
            auto fdStart = fd.tellp();

            // Store format, must be 3
            uint8_t format = 3;
            writeToStream(fd, format, EndianType::Little);
            
            // Store date, default to 0
            uint8_t yy = 0;
            uint8_t mm = 0;
            uint8_t dd = 0;
            writeToStream(fd, yy, EndianType::Little);
            writeToStream(fd, mm, EndianType::Little);
            writeToStream(fd, dd, EndianType::Little);
            
            // Store number of records in database
            auto fdRecords = fd.tellp();
            uint32_t nRecords = 0;
            writeToStream(fd, nRecords, EndianType::Little);
            
            // Store length of this header
            auto fdHeaderLength = fd.tellp();
            uint16_t headerLength = 0;
            writeToStream(fd, headerLength, EndianType::Little);
            
            // Store length of records
            auto fdRecordLength = fd.tellp();
            uint16_t recordLength = 1; // Must be 1 by default
            writeToStream(fd, recordLength, EndianType::Little);
            
            // Next bytes are either reserved or zero for shapefiles
            char zero = 0;
            for (int i = 0; i < 20; i++)
                fd.write(&zero, 1);

            // Write field descriptor array
            std::vector<std::pair<std::string, char> > fields;
            for (auto &pi : stringProperties) {
                fields.push_back( { pi.first, 'C'} );
            }
            for (auto &pi : intProperties) {
                fields.push_back( { pi.first, 'N'} );
            }
            for (auto &pi : floatProperties) {
                fields.push_back( { pi.first, 'F'} );
            }
            for (auto &pi : doubleProperties) {
                fields.push_back( { pi.first, 'F'} );
            }
            for (auto &pi : indexProperties) {
                fields.push_back( { pi.first, 'N'} );
            }
            
            // Find maximum string size
            std::size_t maxStringSize = 1;
            for (const auto &index : geometryIndexes) {

                // Check type
                auto &g = v.getGeometry(index);
                if (g->isType(writeType)) {
                
                    for (auto &pi : stringProperties) {
                        auto &data = pi.second.get()[g->getID()];
                        maxStringSize = std::max(maxStringSize, data.size());
                    }
                }
            }
            maxStringSize = std::min(maxStringSize, (std::size_t)254);

            // Check number of fields
            if (fields.size() > 2046) {
                std::stringstream err;
                err << "Total number of fields in dbf cannot exceed 2046";
                throw std::runtime_error(err.str());
            }

            // Write fields
            const uint8_t lengthF = 21;
            const uint8_t decimalsF = 10;
            const uint8_t lengthN = 8;
            const uint8_t decimalsN = 0;
            const uint8_t lengthC = (uint8_t)maxStringSize;
            const uint8_t decimalsC = 0;
            for (const auto &f : fields) {
            
                // Write name, truncate and zero pad to 11 bytes
                std::string fieldName = Strings::pad(f.first, '\0', 11);

                // Append type character and write
                fieldName += f.second;

                // Write name
                fd.write(reinterpret_cast<char *>(&fieldName[0]), 12);
                    
                // Write reserved bytes
                for (int i = 0; i < 4; i++)
                    fd.write(&zero, 1);
                        
                // Write field length and decimals
                switch (f.second) {

                    case 'F':
                        writeToStream(fd, lengthF, EndianType::Little);
                        recordLength+=lengthF;

                        writeToStream(fd, decimalsF, EndianType::Little);
                        break;

                    case 'N':
                        writeToStream(fd, lengthN, EndianType::Little);
                        recordLength+=lengthN;

                        writeToStream(fd, decimalsN, EndianType::Little);
                        break;

                    case 'C':
                        writeToStream(fd, lengthC, EndianType::Little);
                        recordLength+=lengthC;

                        writeToStream(fd, decimalsC, EndianType::Little);
                        break;

                    default:
                        throw std::runtime_error("Unknown field code in shapefile");
                        break;
                }

                // Write reserved bytes
                for (int i = 0; i < 14; i++)
                    fd.write(&zero, 1);
            }

            // Write field descriptor end
            char fieldDescriptorEnd = 0x0D;
            fd.write(&fieldDescriptorEnd, 1);

            // Write records
            char recordStart = 0x20;
            for (const auto &index : geometryIndexes) {
            
                // Check type
                auto &g = v.getGeometry(index);
                if (g->isType(writeType)) {

                    // Record start
                    nRecords++;
                    fd.write(&recordStart, 1);
                
                    // Write data
                    std::string data;
                    for (auto &pi : stringProperties) {
                        data = pi.second.get()[g->getID()];
                        data = Strings::pad(data, ' ', lengthC);
                        fd.write(reinterpret_cast<char *>(&data[0]), lengthC);
                    }
                    for (auto &pi : intProperties) {
                        int val = pi.second.get()[g->getID()];
                        data = isValid<int>(val) ? std::to_string(val) : "";
                        data = Strings::pad(data, ' ', lengthN);
                        fd.write(reinterpret_cast<char *>(&data[0]), lengthN);
                    }
                    for (auto &pi : floatProperties) {
                        float val = pi.second.get()[g->getID()];
                        data = isValid<float>(val) ? std::to_string(val) : "";
                        data = Strings::pad(data, ' ', lengthF);
                        fd.write(reinterpret_cast<char *>(&data[0]), lengthF);
                    }
                    for (auto &pi : doubleProperties) {
                        double val = pi.second.get()[g->getID()];
                        data = isValid<double>(val) ? std::to_string(val) : "";
                        data = Strings::pad(data, ' ', lengthF);
                        fd.write(reinterpret_cast<char *>(&data[0]), lengthF);
                    }
                    for (auto &pi : indexProperties) {
                        cl_uint val = pi.second.get()[g->getID()];
                        data = isValid<cl_uint>(val) ? std::to_string(val) : "";
                        data = Strings::pad(data, ' ', lengthN);
                        fd.write(reinterpret_cast<char *>(&data[0]), lengthN);
                    }
                }
            }

            // Write end of file
            char fileEnd = 0x1A;
            fd.write(&fileEnd, 1);

            // Write number of records
            fd.seekp(fdRecords, std::ios::beg);
            writeToStream(fd, nRecords, EndianType::Little);

            // Write header length
            headerLength = (uint16_t)(32+32*fields.size()+1);
            fd.seekp(fdHeaderLength, std::ios::beg);
            writeToStream(fd, headerLength, EndianType::Little);

            // Write record length
            fd.seekp(fdRecordLength, std::ios::beg);
            writeToStream(fd, recordLength, EndianType::Little);
        }

        // Open shx index file
        std::string shxFileName = fileNameSplit[0] + fileNameSplit[1] + ".shx";
        std::ofstream fx(shxFileName, std::ios::out | std::ios::binary);
        if (!fx) {
        
            // Could not create database file
            std::stringstream err;
            err << "Could not create shapefile index '" << shxFileName << "'";
            throw std::runtime_error(err.str());
        }

        // Open shapefile
        std::ofstream fs(shapefileName, std::ios::out | std::ios::binary);
        if (fs) {
        
            // Store start position
            auto fsStart = fs.tellp();
            auto fxStart = fx.tellp();

            // Write header
            uint32_t fileCode = 9994;
            writeToStream(fs, fileCode, EndianType::Big);
            writeToStream(fx, fileCode, EndianType::Big);

            // Write five reserved zero bytes
            uint32_t zero = 0;
            for (uint32_t i = 0; i < 5; i++) {
                writeToStream(fs, zero, EndianType::Big);
                writeToStream(fx, zero, EndianType::Big);
            }

            // Store file size pointer
            auto fsLengthPos = fs.tellp();
            auto fxLengthPos = fx.tellp();
            uint32_t fileLength = 0;
            writeToStream(fs, fileLength, EndianType::Big);
            writeToStream(fx, fileLength, EndianType::Big);
            
            // Write version
            uint32_t version = 1000;
            writeToStream(fs, version, EndianType::Little);
            writeToStream(fx, version, EndianType::Little);

            // Find ranges
            auto bounds = v.getBounds();
            bool hasZ = (bounds.max.r != 0.0) || (bounds.min.r != 0.0);
            bool hasM = (bounds.max.s != 0.0) || (bounds.min.s != 0.0);

            // Write type
            uint32_t shapeType = ShapeType::Null;
            switch(writeType) {

                case GeometryType::Point:

                    // Point types
                    if (hasZ) {
                        shapeType = ShapeType::PointZ;
                    } else if (hasM) {
                        shapeType = ShapeType::PointM;
                    } else {
                        shapeType = ShapeType::Point;
                    }
                    break;

                case GeometryType::LineString:

                    // Line string types
                    if (hasZ) {
                        shapeType = ShapeType::PolylineZ;
                    } else if (hasM) {
                        shapeType = ShapeType::PolylineM;
                    } else {
                        shapeType = ShapeType::Polyline;
                    }
                    break;

                case GeometryType::Polygon:

                    // Polygon types
                    if (hasZ) {
                        shapeType = ShapeType::PolygonZ;
                    } else if (hasM) {
                        shapeType = ShapeType::PolygonM;
                    } else {
                        shapeType = ShapeType::Polygon;
                    }
                    break;

                default:
                    std::stringstream err;
                    err << "Unsupported output type '" << writeType << "'";
                    throw std::runtime_error(err.str());
            }
            writeToStream(fs, shapeType, EndianType::Little);
            writeToStream(fx, shapeType, EndianType::Little);

            // Write bounds
            double min, max;

            min = (double)bounds.min.p;
            writeToStream(fs, min, EndianType::Little);
            writeToStream(fx, min, EndianType::Little);
            min = (double)bounds.min.q;
            writeToStream(fs, min, EndianType::Little);
            writeToStream(fx, min, EndianType::Little);
            max = (double)bounds.max.p;
            writeToStream(fs, max, EndianType::Little);
            writeToStream(fx, max, EndianType::Little);
            max = (double)bounds.max.q;
            writeToStream(fs, max, EndianType::Little);
            writeToStream(fx, max, EndianType::Little);
            min = (double)bounds.min.r;
            writeToStream(fs, min, EndianType::Little);
            writeToStream(fx, min, EndianType::Little);
            max = (double)bounds.min.r;
            writeToStream(fs, max, EndianType::Little);
            writeToStream(fx, max, EndianType::Little);
            min = hasM ? (double)bounds.min.s : std::numeric_limits<double>::lowest();
            writeToStream(fs, min, EndianType::Little);
            writeToStream(fx, min, EndianType::Little);
            max = hasM ? (double)bounds.max.s : std::numeric_limits<double>::lowest();
            writeToStream(fs, max, EndianType::Little);
            writeToStream(fx, max, EndianType::Little);

            // Write data
            switch(writeType) {

                case GeometryType::Point: {

                    // Set content length
                    uint32_t contentLength = (4+16)>>1; // contentLength in 16-bit words
                    if (hasZ) contentLength += 8>>1;
                    if (hasM) contentLength += 8>>1;
                    
                    // Write points
                    uint32_t recordNumber = 1;
                    for (const auto &index : geometryIndexes) {

                        // Check type
                        auto &g = v.getGeometry(index);
                        if (g->isType(writeType)) {

                            // Cast as point
                            auto p = std::static_pointer_cast<Point<T> >(g);

                            // Get coordinate
                            auto c = v.getPointCoordinate(index);

                            // Write index
                            uint32_t index = (uint32_t)(fs.tellp()-fsStart)>>1; // index in 16-bit words
                            writeToStream(fx, index, EndianType::Big);
                        
                            // Write record number
                            writeToStream(fs, recordNumber, EndianType::Big);
                            recordNumber++;

                            // Write content length
                            writeToStream(fs, contentLength, EndianType::Big);
                            writeToStream(fx, contentLength, EndianType::Big);

                            // Write shape type
                            writeToStream(fs, shapeType, EndianType::Little);

                            // Write x and y data
                            double x = (double)c.p;
                            writeToStream(fs, x, EndianType::Little);

                            double y = (double)c.q;
                            writeToStream(fs, y, EndianType::Little);

                            // Write z data
                            if (hasZ) {
                                double z = (double)c.r;
                                writeToStream(fs, z, EndianType::Little);
                            }
                        
                            // Write m data
                            if (hasM) {
                                double m = (double)c.s;
                                writeToStream(fs, m, EndianType::Little);
                            }
                        }
                    }

                    } break;

                case GeometryType::LineString: {

                    // Write lines
                    uint32_t recordNumber = 1;
                    for (const auto &index : geometryIndexes) {
                    
                        // Check type
                        auto &g = v.getGeometry(index);
                        if (g->isType(writeType)) {

                            // Cast as line string
                            auto l = std::static_pointer_cast<LineString<T> >(g);

                            // Get coordinates
                            const auto coords = v.getLineStringCoordinates(index);

                            // Write index
                            uint32_t index = (uint32_t)(fs.tellp()-fsStart)>>1; // index in 16-bit words
                            writeToStream(fx, index, EndianType::Big);

                            // Write record number
                            writeToStream(fs, recordNumber, EndianType::Big);
                            recordNumber++;

                            // Write content length
                            auto contentLengthPos = fs.tellp();
                            uint32_t contentLength = 0;
                            writeToStream(fs, contentLength, EndianType::Big);

                            // Store record start position
                            auto recordStart = fs.tellp();

                            // Write shape type
                            writeToStream(fs, shapeType, EndianType::Little);

                            // Write bounding box placeholder
                            min = (double)0;
                            max = (double)0;
                            auto boundingBoxPos = fs.tellp();
                            writeToStream(fs, min, EndianType::Little);
                            writeToStream(fs, min, EndianType::Little);
                            writeToStream(fs, max, EndianType::Little);
                            writeToStream(fs, max, EndianType::Little);

                            // Write number of parts, each vector lineString is a single part
                            uint32_t nParts = 1;
                            writeToStream(fs, nParts, EndianType::Little);

                            // Write number of coordinates
                            uint32_t nCoords = (uint32_t)coords.size();
                            writeToStream(fs, nCoords, EndianType::Little);

                            // Write single parts
                            uint32_t parts = 0;
                            writeToStream(fs, parts, EndianType::Little);

                            // Create limits
                            double xMin = std::numeric_limits<double>::max();
                            double xMax = std::numeric_limits<double>::lowest();
                            double yMin = std::numeric_limits<double>::max();
                            double yMax = std::numeric_limits<double>::lowest();
                            double zMin = hasZ ? std::numeric_limits<double>::max() : 0.0;
                            double zMax = hasZ ? std::numeric_limits<double>::lowest() : 0.0;
                            double mMin = hasM ? std::numeric_limits<double>::max() : std::numeric_limits<double>::lowest();
                            double mMax = hasM ? std::numeric_limits<double>::lowest() : std::numeric_limits<double>::lowest();
                        
                            // Write points
                            for (const auto &c : coords) {

                                // Write x and y data
                                double x = (double)c.p;
                                xMin = std::min(x, xMin);
                                xMax = std::max(x, xMax);
                                writeToStream(fs, x, EndianType::Little);

                                double y = (double)c.q;
                                yMin = std::min(y, yMin);
                                yMax = std::max(y, yMax);
                                writeToStream(fs, y, EndianType::Little);

                                // Find z data bounds
                                if (hasZ) {
                                    double z = (double)c.r;
                                    zMin = std::min(z, zMin);
                                    zMax = std::max(z, zMax);
                                }
                        
                                // Find m data bounds
                                if (hasM) {
                                    double m = (double)c.s;
                                    mMin = std::min(m, mMin);
                                    mMax = std::max(m, mMax);
                                }
                            }

                            if (hasZ) {

                                // Write z limits
                                writeToStream(fs, zMin, EndianType::Little);
                                writeToStream(fs, zMin, EndianType::Little);

                                // Write z data
                                for (const auto &c : coords) {
                                    double z = (double)c.r;
                                    writeToStream(fs, z, EndianType::Little);
                                }
                            }

                            if (hasM) {

                                // Write m limits
                                writeToStream(fs, mMin, EndianType::Little);
                                writeToStream(fs, mMin, EndianType::Little);

                                // Write m data
                                for (const auto &c : coords) {
                                    double m = (double)c.s;
                                    writeToStream(fs, m, EndianType::Little);
                                }
                            }

                            // Store record end position
                            auto recordEnd = fs.tellp();

                            // Write record length
                            contentLength = (uint32_t)(recordEnd-recordStart)>>1; // contentLength in 16-bit words
                            fs.seekp(contentLengthPos, std::ios::beg);
                            writeToStream(fs, contentLength, EndianType::Big);
                            writeToStream(fx, contentLength, EndianType::Big);
                        
                            // Write bounding box
                            fs.seekp(boundingBoxPos, std::ios::beg);
                            writeToStream(fs, xMin, EndianType::Little);
                            writeToStream(fs, yMin, EndianType::Little);
                            writeToStream(fs, xMax, EndianType::Little);
                            writeToStream(fs, yMax, EndianType::Little);

                            fs.seekp(recordEnd, std::ios::beg);
                        }
                    }

                    } break;

                case GeometryType::Polygon: {

                    // Write polygons
                    uint32_t recordNumber = 1;
                    for (const auto &index : geometryIndexes) {

                        // Check type
                        auto &g = v.getGeometry(index);
                        if (g->isType(writeType)) {
                    
                            // Cast as polygon
                            auto p = std::static_pointer_cast<Polygon<T> >(g);

                            // Get coordinates
                            auto coords = v.getPolygonCoordinates(index);

                            // Get sub-indexes
                            const auto subIndexes = p->getSubIndexes();

                            // Write index
                            uint32_t index = (uint32_t)(fs.tellp()-fsStart)>>1; // index in 16-bit words
                            writeToStream(fx, index, EndianType::Big);

                            // Write record number
                            writeToStream(fs, recordNumber, EndianType::Big);
                            recordNumber++;

                            // Write content length
                            auto contentLengthPos = fs.tellp();
                            uint32_t contentLength = 0;
                            writeToStream(fs, contentLength, EndianType::Big);

                            // Store record start position
                            auto recordStart = fs.tellp();

                            // Write shape type
                            writeToStream(fs, shapeType, EndianType::Little);

                            // Write bounding box placeholder
                            min = (double)0;
                            max = (double)0;
                            auto boundingBoxPos = fs.tellp();
                            writeToStream(fs, min, EndianType::Little);
                            writeToStream(fs, min, EndianType::Little);
                            writeToStream(fs, max, EndianType::Little);
                            writeToStream(fs, max, EndianType::Little);

                            // Write number of parts
                            uint32_t nParts = (uint32_t)subIndexes.size();
                            writeToStream(fs, nParts, EndianType::Little);

                            // Write number of coordinates
                            uint32_t nCoords = (uint32_t)coords.size();
                            writeToStream(fs, nCoords, EndianType::Little);

                            // Write single parts
                            uint32_t part = 0;
                            for (const auto &index : subIndexes) {
                                writeToStream(fs, part, EndianType::Little);
                                part += (uint32_t)index;
                            }

                            // Create limits
                            double xMin = std::numeric_limits<double>::max();
                            double xMax = std::numeric_limits<double>::lowest();
                            double yMin = std::numeric_limits<double>::max();
                            double yMax = std::numeric_limits<double>::lowest();
                            double zMin = hasZ ? std::numeric_limits<double>::max() : 0.0;
                            double zMax = hasZ ? std::numeric_limits<double>::lowest() : 0.0;
                            double mMin = hasM ? std::numeric_limits<double>::max() : std::numeric_limits<double>::lowest();
                            double mMax = hasM ? std::numeric_limits<double>::lowest() : std::numeric_limits<double>::lowest();
                        
                             // Check winding using stabilised shoelace formula
                            cl_uint lastIndex = 0;
                            for (const auto &index : subIndexes) {

                                // Set next index
                                cl_uint nextIndex = lastIndex+index;

                                // Calculate mean position
                                double xMean = 0.0;
                                double yMean = 0.0;
                                for (cl_uint i = lastIndex; i < nextIndex; i++) {
                                    xMean += coords[i].p;
                                    yMean += coords[i].q;
                                }
                                xMean/=(double)index;
                                yMean/=(double)index;

                                // Calculate area
                                double A = 0.0;
                                for (cl_uint i = lastIndex; i < nextIndex-1; i++) {
                                    A += (coords[i].p-xMean)*(coords[i+1].q-yMean)-
                                        (coords[i+1].p-xMean)*(coords[i].q-yMean);
                                }
                                A += (coords[nextIndex-1].p-xMean)*(coords[lastIndex].q-yMean)-
                                    (coords[lastIndex].p-xMean)*(coords[nextIndex-1].q-yMean);

                                // Shapefiles should have clockwise exterior rings (A < 0.0), counter-clockwise interior rings (A > 0.0)
                                if ((lastIndex == 0 && A > 0.0) || (lastIndex != 0 && A < 0.0)) {

                                    // Flip to clockwise
                                    std::reverse(coords.begin()+lastIndex, coords.begin()+nextIndex);
                                }

                                // Update index
                                lastIndex = nextIndex;
                            }
                        
                            // Write points
                            for (const auto &c : coords) {

                                // Write x and y data
                                double x = (double)c.p;
                                xMin = std::min(x, xMin);
                                xMax = std::max(x, xMax);
                                writeToStream(fs, x, EndianType::Little);

                                double y = (double)c.q;
                                yMin = std::min(y, yMin);
                                yMax = std::max(y, yMax);
                                writeToStream(fs, y, EndianType::Little);

                                // Find z data bounds
                                if (hasZ) {
                                    double z = (double)c.r;
                                    zMin = std::min(z, zMin);
                                    zMax = std::max(z, zMax);
                                }
                        
                                // Find m data bounds
                                if (hasM) {
                                    double m = (double)c.s;
                                    mMin = std::min(m, mMin);
                                    mMax = std::max(m, mMax);
                                }
                            }

                            if (hasZ) {

                                // Write z limits
                                writeToStream(fs, zMin, EndianType::Little);
                                writeToStream(fs, zMin, EndianType::Little);

                                // Write z data
                                for (const auto &c : coords) {
                                    double z = (double)c.r;
                                    writeToStream(fs, z, EndianType::Little);
                                }
                            }

                            if (hasM) {

                                // Write m limits
                                writeToStream(fs, mMin, EndianType::Little);
                                writeToStream(fs, mMin, EndianType::Little);

                                // Write m data
                                for (const auto &c : coords) {
                                    double m = (double)c.s;
                                    writeToStream(fs, m, EndianType::Little);
                                }
                            }

                            // Store record end position
                            auto recordEnd = fs.tellp();

                            // Write record length
                            contentLength = (uint32_t)(recordEnd-recordStart)>>1; // contentLength in 16-bit words
                            fs.seekp(contentLengthPos, std::ios::beg);
                            writeToStream(fs, contentLength, EndianType::Big);
                            writeToStream(fx, contentLength, EndianType::Big);
                        
                            // Write bounding box
                            fs.seekp(boundingBoxPos, std::ios::beg);
                            writeToStream(fs, xMin, EndianType::Little);
                            writeToStream(fs, yMin, EndianType::Little);
                            writeToStream(fs, xMax, EndianType::Little);
                            writeToStream(fs, yMax, EndianType::Little);

                            fs.seekp(recordEnd, std::ios::beg);
                        }
                    }

                    } break;

                default:
                    std::stringstream err;
                    err << "Unsupported output type '" << writeType << "'";
                    throw std::runtime_error(err.str());
            }

            // Write file length
            fileLength = (uint32_t)(fs.tellp()-fsStart)>>1; // fileLength in 16-bit words
            fs.seekp(fsLengthPos, std::ios::beg);
            writeToStream(fs, fileLength, EndianType::Big);

            fileLength = (uint32_t)(fx.tellp()-fxStart)>>1; // fileLength in 16-bit words
            fx.seekp(fxLengthPos, std::ios::beg);
            writeToStream(fx, fileLength, EndianType::Big);

        } else {
        
            // Could not create shapefile
            std::stringstream err;
            err << "Could not create shapefile '" << shapefileName << "'";
            throw std::runtime_error(err.str());
        }

        // Create prj
        auto proj = v.getProjectionParameters();
        int crs = 0;
        if (v.hasProperty("crs")) {
            crs = v.template getGlobalProperty<int>("crs");
        }
        if (proj.type != 0) {
            std::string projFileName = fileNameSplit[0] + fileNameSplit[1] + ".prj";
            std::ofstream fp(projFileName, std::ios::out);
            fp << Projection::toWKT(proj, crs);
            if (fp.is_open()) {
                fp.close();
            }
        }
        
        if (fs.is_open()) {
            fs.close();
        }
        
        if (fd.is_open()) {
            fd.close();
        }
        
        if (fx.is_open()) {
            fx.close();
        }

        return true;
    }

    // Float type definitions
    template Geostack::Vector<float> ShapeFile<float>::shapefileToVector(std::string, std::string);
    template Geostack::Vector<float> ShapeFile<float>::shapefileToVectorWithBounds(std::string,
        BoundingBox<float>, ProjectionParameters<double>, std::string);
    template bool ShapeFile<float>::vectorToShapefile(const Geostack::Vector<float> &,
        std::string, GeometryType::Type);
        
    // Double type definitions
    template Geostack::Vector<double> ShapeFile<double>::shapefileToVector(std::string, std::string);
    template Geostack::Vector<double> ShapeFile<double>::shapefileToVectorWithBounds(std::string,
        BoundingBox<double>, ProjectionParameters<double>, std::string);
    template bool ShapeFile<double>::vectorToShapefile(const Geostack::Vector<double> &,
        std::string, GeometryType::Type);
}
