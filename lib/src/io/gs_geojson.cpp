/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>

#include "miniz.h"

#include "gs_geometry.h"
#include "gs_projection.h"
#include "gs_geojson.h"

using namespace json11;

namespace Geostack
{
    /**
    * GeoJSON parser.
    * Converts a GeoJSON string to a %Vector object
    * @param geoJsonStr GeoJSON string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> GeoJson<T>::geoJsonFileToVector(std::string geoJsonFile, bool enforceProjection) {

        // Create return Vector
        Vector<T> v;

        std::ifstream f(geoJsonFile, std::ios::in | std::ios::binary);
        if (f) {

            // Read first character
            char c;
            f.get(c);

            if (c == '{') {

                // Read file into string
                std::string geoJsonStr;
                f.seekg(0, std::ios::end);
                geoJsonStr.resize(f.tellg());
                f.seekg(0, std::ios::beg);
                f.read(&geoJsonStr[0], geoJsonStr.size());
                f.close();

                // Parse string
                if (!parseString(v, geoJsonStr))
                    v.clear();

            } else {

                // Try and open as zip file
                mz_zip_archive zGeoJson;
                std::memset(&zGeoJson, 0, sizeof(zGeoJson));
                if (mz_zip_reader_init_file(&zGeoJson, geoJsonFile.c_str(), 0)) {

                    // Loop through files
                    std::size_t size;
                    for (mz_uint i = 0; i < (mz_uint)mz_zip_reader_get_num_files(&zGeoJson); i++) {

                        mz_zip_archive_file_stat zStat;
                        if (mz_zip_reader_file_stat(&zGeoJson, i, &zStat)) {

                            // Skip directory entries
                            if (!mz_zip_reader_is_file_a_directory(&zGeoJson, i)) {

                                // Read data
                                void *data = mz_zip_reader_extract_file_to_heap(&zGeoJson, zStat.m_filename, &size, 0);

                                // Copy to string
                                std::string geoJsonStr;
                                geoJsonStr.resize(size);
                                std::memcpy(&geoJsonStr[0], data, size);

                                // Free data
                                mz_free(data);

                                // Check data and uncompressed size
                                if (data && zStat.m_uncomp_size == size) {

                                    // Parse string
                                    if (!parseString(v, geoJsonStr))
                                        v.clear();

                                } else {
                                    throw std::runtime_error("Zip uncompression failed");
                                }
                            }
                        }
                    }

                }
            }

        } else {
            std::stringstream err;
            err << "Cannot open '" << geoJsonFile << "' for reading";
            throw std::runtime_error(err.str());
        }

        // Set projection
        if (enforceProjection) {
            setProjection(v);
        }
        
        if (f.is_open()) {
            f.close();
        }
        
        // Return Vector
        return v;
    }

    /**
    * GeoJSON parser.
    * Converts a GeoJSON string to a %Vector object
    * @param geoJsonStr GeoJSON string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    Geostack::Vector<T> GeoJson<T>::geoJsonToVector(std::string geoJsonStr, bool enforceProjection) {

        // Create return Vector
        Vector<T> v;

        // Parse string
        if (!parseString(v, geoJsonStr)) {

            // Reset vector
            v.clear();

        } else {

            // Set projection
            if (enforceProjection) {
                setProjection(v);
            }
        }
        
        // Return Vector
        return v;
    }
    
    /**
    * GeoJSON coordinate handler.
    * Parses GeoJSON coordinate.
    * @param coordinates Json values.
    * @param properties Json properties for time.
    * @return %Coordinate object.
    */
    template <typename T>
    Coordinate<T> GeoJson<T>::parseCoordinate(const std::vector<json11::Json> &coordinates, const std::map<std::string, json11::Json> &properties) {

        // Search for time property
        auto it = properties.find("time");
        if (it != properties.end()) {
            
            // Get time
            T time = (T)0.0;
            if (it->second.is_number())
                time = (T)it->second.number_value();
                
            // Add coordinates with time
            if (coordinates.size() < 3) {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value(), (T)0.0, time } );
            } else {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value(), (T)coordinates[2].number_value(), time } );
            }

        } else {
            
            // Add coordinates
            if (coordinates.size() < 3) {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value() } );
            } else {
                return Coordinate<T>( { (T)coordinates[0].number_value(), (T)coordinates[1].number_value(), (T)coordinates[2].number_value() } );
            }
        }
    }
    
    /**
    * GeoJSON null handler.
    * Add null value to properties
    * @param coordinates Json values.
    * @param properties Json properties for time.
    * @return %Coordinate object.
    */
    template <typename T>
    bool addNullValue(std::string name, cl_uint index, Vector<T> &v) {
    
        // Add null property
        auto &vp = v.getProperties();
        switch (vp.getPropertyType(name)) {

            case PropertyType::Byte:
                v.setProperty(index, name, getNullValue<cl_uchar>());
                return true;

            case PropertyType::Integer:
                v.setProperty(index, name, getNullValue<int32_t>());
                return true;

            case PropertyType::Index:
                v.setProperty(index, name, getNullValue<cl_uint>());
                return true;

            case PropertyType::Float:
                v.setProperty(index, name, getNullValue<T>());
                return true;

            case PropertyType::FloatVector:
                v.setProperty(index, name, std::vector<T>());
                return true;

            case PropertyType::Double:
                v.setProperty(index, name, getNullValue<T>());
                return true;

            case PropertyType::DoubleVector:
                v.setProperty(index, name, std::vector<T>());
                return true;

            case PropertyType::String:
                v.setProperty(index, name, getNullValue<std::string>());
                return true;
            
            default:
                break;
        }
        return false;
    }

    /**
    * GeoJSON parser.
    * Converts a GeoJSON string to a %Vector object
    * @param geoJsonStr GeoJSON string.
    * @return Geostack %Vector object.
    */
    template <typename T>
    bool GeoJson<T>::parseString(Geostack::Vector<T> &v, std::string geoJsonStr) {

        // Create Json object
        std::string err;
        const auto geoJson = Json::parse(geoJsonStr, err);

        // Check for correct type
        if (geoJson["type"].string_value() == "FeatureCollection") {

            // List of coordinates and new geometry
            CoordinateList<T> cs;
            std::vector<cl_uint> newIndexes;
            std::map<std::string, std::vector<cl_uint> > nullIndexes;
        
            // Check for feature array
            if (geoJson["features"].is_array()) {
                for (auto &feature : geoJson["features"].array_items()) {

                    // Check for correct feature type
                    if (feature["type"].string_value() == "Feature") {

                        // Read properties
                        auto properties = feature["properties"].object_items();

                        // Parse geometry
                        auto &geometry = feature["geometry"];
                        std::string geometryType = geometry["type"].string_value();

                        if (geometryType.size() == 0) {

                            // Skip null geometry
                            continue;

                        } else if (geometryType == "Point" || geometryType == "MultiPoint") {

                            // Parse point data
                            if (geometry["coordinates"].is_array()) {

                                newIndexes.clear();
                                if (geometryType == "Point") {

                                    // Add point
                                    newIndexes.push_back(v.addPoint(parseCoordinate(geometry["coordinates"].array_items(), properties)));

                                } else {

                                    // Add multiple points
                                    for (auto &coordinates : geometry["coordinates"].array_items())
                                        newIndexes.push_back(v.addPoint(parseCoordinate(coordinates.array_items(), properties)));
                                }

                                // Add properties
                                for (auto &property : properties) {

                                    // Skip time property
                                    if (property.first == "time")
                                        continue;

                                    // Parse values
                                    if (property.second.is_null()) {
                                        
                                        // Add null property
                                        for (auto &index : newIndexes) {
                                            std::string name = property.first;
                                            v.addProperty(name);
                                            if (!addNullValue<T>(name, index, v)) {
                                                auto nit = nullIndexes.find(name);
                                                if (nullIndexes.find(name) == nullIndexes.end()) {
                                                    nit = nullIndexes.insert( { name, std::vector<cl_uint>() }).first;
                                                }
                                                nit->second.push_back(index);
                                            }
                                        }

                                    } else if (property.second.is_string()) {

                                        // Set string property
                                        for (auto &index : newIndexes) {
                                            v.setProperty(index, property.first, property.second.string_value());
                                        }

                                    } else if (property.second.is_number()) {

                                        // Set number property
                                        for (auto &index : newIndexes) {
                                            v.setProperty(index, property.first, (T)property.second.number_value());
                                        }

									} else if (property.second.is_array()) {

										for (auto &index : newIndexes) {

                                            // Parse array
                                            auto pArray = property.second.array_items();
                                            if (pArray.size() > 0 && pArray[0].is_number()) {

                                                // Numeric arrays
                                                std::vector<T> valArray;
                                                for (auto &val : pArray) {
                                                    if (val.is_number()) {
                                                        valArray.push_back((T)val.number_value());
                                                    } else {
                                                        std::stringstream err;
                                                        err << "Cannot apply JSON arrray '" << property.first << "' as datatype differs in array";
                                                        throw std::runtime_error(err.str());
                                                    }
                                                }
                                                v.setProperty(index, property.first, valArray);
                                            }
                                        }
                                    } else {

                                        // Unsupported types
                                    }
                                }

                            } else {
                                throw std::runtime_error("Point coordinates not found");
                            }
                        } else if (geometryType == "LineString" || geometryType == "MultiLineString") {
                        
                            // Parse line string data
                            if (geometry["coordinates"].is_array()) {
                                
                                newIndexes.clear();
                                if (geometryType == "LineString") {

                                    // Add line string
                                    cs.clear();
                                    for (auto &coordinates : geometry["coordinates"].array_items())
                                        cs.push_back(parseCoordinate(coordinates.array_items(), properties));
                                    newIndexes.push_back(v.addLineString(cs));

                                } else {

                                    // Add multiple line strings
                                    for (auto &line : geometry["coordinates"].array_items()) {
                                        cs.clear();
                                        for (auto &coordinates : line.array_items())
                                            cs.push_back(parseCoordinate(coordinates.array_items(), properties));
                                        newIndexes.push_back(v.addLineString(cs));
                                    }
                                }

                                // Add properties
                                for (auto &property : properties) {

                                    // Skip time property
                                    if (property.first == "time")
                                        continue;

                                    // Parse values
                                    if (property.second.is_null()) {
                                        
                                        // Add null property
                                        for (auto &index : newIndexes) {
                                            std::string name = property.first;
                                            v.addProperty(name);
                                            if (!addNullValue<T>(name, index, v)) {
                                                auto nit = nullIndexes.find(name);
                                                if (nullIndexes.find(name) == nullIndexes.end()) {
                                                    nit = nullIndexes.insert( { name, std::vector<cl_uint>() }).first;
                                                }
                                                nit->second.push_back(index);
                                            }
                                        }

                                    } else if (property.second.is_string()) {

                                        // Set string property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.string_value());

                                    } else if (property.second.is_number()) {

                                        // Set number property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, (T)property.second.number_value());

                                    } else if (property.second.is_array()) {

										for (auto &index : newIndexes) {

                                            // Parse array
                                            auto pArray = property.second.array_items();
                                            if (pArray.size() > 0 && pArray[0].is_number()) {

                                                // Numeric arrays
                                                std::vector<T> valArray;
                                                for (auto &val : pArray) {
                                                    if (val.is_number()) {
                                                        valArray.push_back((T)val.number_value());
                                                    } else {
                                                        std::stringstream err;
                                                        err << "Cannot apply JSON arrray '" << property.first << "' as datatype differs in array";
                                                        throw std::runtime_error(err.str());
                                                    }
                                                }
                                                v.setProperty(index, property.first, valArray);
                                            }
                                        }
                                    } else {

                                        // Unsupported types
                                    }
                                }

                            } else {
                                throw std::runtime_error("Line string coordinates not found");
                            }

                        } else if (geometryType == "Polygon" || geometryType == "MultiPolygon") {
                        
                            // Parse polygon data
                            if (geometry["coordinates"].is_array()) {
                            
                                newIndexes.clear();
                                if (geometryType == "Polygon") {

                                    // Add polygons
                                    std::vector<CoordinateList<T> > pcs;
                                    for (auto &polygon : geometry["coordinates"].array_items()) {

                                        // Add coordinates
                                        cs.clear();
                                        for (auto &coordinates : polygon.array_items())
                                            cs.push_back(parseCoordinate(coordinates.array_items(), properties));

                                        // Check first coordinate is the same as the last
                                        if (cs.front() != cs.back()) {
                                            throw std::runtime_error("Last coordinate in polygon must be same as the first");
                                        }

                                        // Update polygon list
                                        pcs.push_back(cs);
                                    }
                                    newIndexes.push_back(v.addPolygon(pcs));

                                } else {

                                    // Add multiple polygons
                                    std::vector<CoordinateList<T> > pcs;
                                    for (auto &multiPolygon : geometry["coordinates"].array_items()) {
                                        pcs.clear();
                                        for (auto &polygon : multiPolygon.array_items()) {

                                            // Add coordinates
                                            cs.clear();
                                            for (auto &coordinates : polygon.array_items())
                                                cs.push_back(parseCoordinate(coordinates.array_items(), properties));

                                            // Check first coordinate is the same as the last
                                            if (cs.front() != cs.back()) {
                                                throw std::runtime_error("Last coordinate in polygon must be same as the first");
                                            }

                                            // Update polygon list
                                            pcs.push_back(cs);
                                        }
                                        newIndexes.push_back(v.addPolygon(pcs));
                                    }

                                }

                                // Add properties
                                for (auto &property : properties) {

                                    // Skip time property
                                    if (property.first == "time")
                                        continue;

                                    // Parse values
                                    if (property.second.is_null()) {
                                        
                                        // Add null property
                                        for (auto &index : newIndexes) {
                                            std::string name = property.first;
                                            v.addProperty(name);
                                            if (!addNullValue<T>(name, index, v)) {
                                                auto nit = nullIndexes.find(name);
                                                if (nullIndexes.find(name) == nullIndexes.end()) {
                                                    nit = nullIndexes.insert( { name, std::vector<cl_uint>() }).first;
                                                }
                                                nit->second.push_back(index);
                                            }
                                        }

                                    } else if (property.second.is_string()) {

                                        // Set string property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, property.second.string_value());

                                    } else if (property.second.is_number()) {

                                        // Set number property
                                        for (auto &index : newIndexes)
                                            v.setProperty(index, property.first, (T)property.second.number_value());

                                    } else if (property.second.is_array()) {

										for (auto &index : newIndexes) {

                                            // Parse array
                                            auto pArray = property.second.array_items();
                                            if (pArray.size() > 0 && pArray[0].is_number()) {

                                                // Numeric arrays
                                                std::vector<T> valArray;
                                                for (auto &val : pArray) {
                                                    if (val.is_number()) {
                                                        valArray.push_back((T)val.number_value());
                                                    } else {
                                                        std::stringstream err;
                                                        err << "Cannot apply JSON arrray '" << property.first << "' as datatype differs in array";
                                                        throw std::runtime_error(err.str());
                                                    }
                                                }
                                                v.setProperty(index, property.first, valArray);
                                            }
                                        }
                                    } else {

                                        // Unsupported types
                                    }
                                }

                            } else {
                                throw std::runtime_error("Polygon coordinates not found");
                            }

                        } else {
                            std::stringstream err;
                            err << "Cannot parse geometry type '" << geometryType << "' in GeoJSON";
                            throw std::runtime_error(err.str());
                        }

                    } else {
                        throw std::runtime_error("Cannot find feature in GeoJSON");
                    }
                }
            } else {
                throw std::runtime_error("Cannot find feature array in GeoJSON");
            }

            // Add all null features
            for (auto &nullIndex : nullIndexes) {
                v.addProperty(nullIndex.first);
                for (auto &index : nullIndex.second) {
                    addNullValue(nullIndex.first, index, v);
                }
            }

        } else {
            throw std::runtime_error("String is not GeoJSON");
        }

        return true;
    }

    /**
    * GeoJSON parser.
    * Converts a %Vector object to a GeoJSON string
    * @param v %Vector object.
    * @param enforceProjection enforces GeoJSON standard EPSG:4326 CRS if true.
    * @return GeoJSON string.
    */
    template <typename T>
    std::string GeoJson<T>::vectorToGeoJson(Geostack::Vector<T> &v, bool enforceProjection, bool writeNullProperties) {
        
        Json::array jsonFeatures;
        
        // Check projection
        ProjectionParameters<double> proj = v.getProjectionParameters();
        if (proj.type == 0 && enforceProjection) {
            throw std::runtime_error("Vector has no projection for GeoJSON conversion");
        }

        // Get geometry
        auto &geometryIndexes = v.getGeometryIndexes();

        // Get properties
        auto &properties = v.getProperties();
        auto stringProperties = properties.template getPropertyRefs<std::vector<std::string> >();
        auto intProperties = properties.template getPropertyRefs<std::vector<int> >();
        auto floatProperties = properties.template getPropertyRefs<std::vector<float> >();
        auto doubleProperties = properties.template getPropertyRefs<std::vector<double> >();
        auto indexProperties = properties.template getPropertyRefs<std::vector<cl_uint> >();
        auto vectorFloatProperties = properties.template getPropertyRefs<std::vector<std::vector<float> > >();
        auto vectorDoubleProperties = properties.template getPropertyRefs<std::vector<std::vector<double> > >();

        // Check and re-project if necessary to EPSG:4326 as per RFC 7946 (https://tools.ietf.org/html/rfc7946)  
        bool reproject = false;
        ProjectionParameters<double> proj_EPSG4326 = Projection::parsePROJ4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
        if (enforceProjection && v.getProjectionParameters() != proj_EPSG4326) {
            reproject = true;
        }

        // Create locally calculated bounding box
        BoundingBox<T> bounds;

        // Parse geometry
        for (const auto &index : geometryIndexes) {

            // Check type
            auto &g = v.getGeometry(index);
            if (!g->isType(GeometryType::Point | GeometryType::LineString | GeometryType::Polygon))
                continue;
        
            // Create feature
            Json::object feature = Json::object { { "type", "Feature" } };

            // Process type
            if (g->isType(GeometryType::Point)) {
            
                // Create geometry
                auto p = std::static_pointer_cast<Point<T> >(g);
                auto c = v.getPointCoordinate(index);

                // Reproject
                if (reproject) {
                    Projection::convert(c, proj_EPSG4326, proj);
                }

                // Update bounds
                bounds.extend(c);

                // Add coordinate
                feature["geometry"] = Json::object {
                    { "type", "Point" },
                    { "coordinates", Json::array { c.p, c.q } }
                };

            } else if (g->isType(GeometryType::LineString)) {
            
                // Parse coordinates
                auto l = std::static_pointer_cast<LineString<T> >(g);
                Json::array jsonLineGeometry = Json::array();
                for (auto c : v.getLineStringCoordinates(index)) {

                    // Reproject
                    if (reproject) {
                        Projection::convert(c, proj_EPSG4326, proj);
                    }

                    // Update bounds
                    bounds.extend(c);

                    // Add coordinate
                    jsonLineGeometry.push_back( Json::array { c.p, c.q });
                }
            
                // Create geometry
                feature["geometry"] = Json::object {
                    { "type", "LineString" },
                    { "coordinates", jsonLineGeometry }
                };
            
            } else if (g->isType(GeometryType::Polygon)) {
        
                // Create arrays
                Json::array jsonPolygonGeometry = Json::array();
                Json::array jsonSubPolygonGeometry = Json::array();

                // Get coordinates
                auto coords = v.getPolygonCoordinates(index);
            
                // Parse coordinates
                auto p = std::static_pointer_cast<Polygon<T> >(g);
                auto subIndexes = p->getSubIndexes();
                cl_uint currentIndex = 0;
                cl_uint firstIndex = 0;
                for (uint32_t i = 0; i < coords.size(); i++) {

                    // Get coordinate
                    auto c = coords[i];
                
                    // Reproject
                    if (reproject) {
                        Projection::convert(c, proj_EPSG4326, proj);
                    }

                    // Update bounds
                    bounds.extend(c);

                    // Add point to sub-geometry
                    jsonSubPolygonGeometry.push_back( Json::array { c.p, c.q });

                    // Create new entry for each sub-polygon
                    if (--subIndexes[currentIndex] == 0) {

                        // Check last coordinate and append copy if needed
                        if (coords[firstIndex] != coords[i]) {
                            jsonSubPolygonGeometry.push_back(jsonSubPolygonGeometry[0]);
                        }

                        // Update indexes
                        currentIndex++;
                        firstIndex = i+1;

                        // Add to geometry
                        jsonPolygonGeometry.push_back(jsonSubPolygonGeometry);
                        jsonSubPolygonGeometry.clear();
                    }
                }
            
                // Create geometry
                feature["geometry"] = Json::object {
                    { "type", "Polygon" },
                    { "coordinates", jsonPolygonGeometry }
                };
            }

            // Get properties
            Json::object jsonProperties = Json::object();
            for (auto &pi : stringProperties) {
                std::string v = pi.second.get()[g->getID()];
                if (isValid<std::string>(v)) {
                    jsonProperties[pi.first] = v;
                } else if (writeNullProperties) {
                    jsonProperties[pi.first] = Json();
                }
            }
            for (auto &pi : intProperties) {
                int v = pi.second.get()[g->getID()];
                if (isValid<int>(v)) {
                    jsonProperties[pi.first] = v;
                } else if (writeNullProperties) {
                    jsonProperties[pi.first] = Json();
                }
            }
            for (auto &pi : floatProperties) {
                float v = pi.second.get()[g->getID()];
                if (isValid<float>(v)) {
                    jsonProperties[pi.first] = v;
                } else if (writeNullProperties) {
                    jsonProperties[pi.first] = Json();
                }
            }
            for (auto &pi : doubleProperties) {
                double v = pi.second.get()[g->getID()];
                if (isValid<double>(v)) {
                    jsonProperties[pi.first] = v;
                } else if (writeNullProperties) {
                    jsonProperties[pi.first] = Json();
                }
            }
            for (auto &pi : indexProperties) {
                cl_uint v = pi.second.get()[g->getID()];
                if (isValid<cl_uint>(v)) {
                    jsonProperties[pi.first] = (int)v;
                } else if (writeNullProperties) {
                    jsonProperties[pi.first] = Json();
                }
            }
            for (auto &pi : vectorFloatProperties) {
                std::vector<float> &pv = pi.second.get()[g->getID()];
                if (pv.size() > 0) {
                    Json::array arr;
                    for (auto &val : pv) {
                        arr.push_back(val);
                    }
                    jsonProperties[pi.first] = arr;
                } else if (writeNullProperties) {
                    jsonProperties[pi.first] = Json();
                }
            }
            for (auto &pi : vectorDoubleProperties) {
                std::vector<double> &pv = pi.second.get()[g->getID()];
                if (pv.size() > 0) {
                    Json::array arr;
                    for (auto &val : pv) {
                        arr.push_back(val);
                    }
                    jsonProperties[pi.first] = arr;
                } else if (writeNullProperties) {
                    jsonProperties[pi.first] = Json();
                }
            }
            feature["properties"] = jsonProperties;

            // Add to features
            jsonFeatures.push_back(feature);
        }

        // Create bounding box
        Json::array jsonBounds = { bounds.min.p, bounds.min.q, bounds.max.p, bounds.max.q };

        // Create object
        Json featuresJson = Json::object {
            { "type", "FeatureCollection" }, 
            { "features", jsonFeatures }
        };
        Json foreignJson = Json::object {
            { "bbox", jsonBounds }
        };        

        // Convert to string, ensure foreign keys are at the end
        auto features = featuresJson.dump();
        features.back() = ',';

        auto foreign = foreignJson.dump();
        foreign.front() = ' ';

        // Append string
        features += foreign;

        return features;
    }
    
    /**
    * Set GeoJSON projection and check bounds
    * @param v %Vector object.
    */
    template <typename T>
    void GeoJson<T>::setProjection(Vector<T> &v) {

        // Set projection to EPSG:4326, as per the standard
        std::string projPROJ4_EPSG4326 = R"(+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs)";
        auto proj_EPSG4326 = Projection::parsePROJ4(projPROJ4_EPSG4326);
        v.setProjectionParameters(proj_EPSG4326);

        // Check bounds
        auto bounds = v.getBounds();
        if (bounds.min.p < -180.0 || bounds.max.p > 180.0 || bounds.min.q < -90.0 || bounds.max.q > 90.0) {

            // Give warning and set projection to empty
            std::cout << "WARNING: GeoJSON data out of bounds, " << 
                bounds.min.p << " - " << bounds.max.p << ", " <<
                bounds.min.q << " - " << bounds.max.q << ", CRS unknown" << std::endl;
            v.setProjectionParameters(ProjectionParameters<double>());
        }
    }

    // Float type definitions
    template Geostack::Vector<float> GeoJson<float>::geoJsonFileToVector(std::string, bool);
    template Geostack::Vector<float> GeoJson<float>::geoJsonToVector(std::string, bool);
    template std::string GeoJson<float>::vectorToGeoJson(Geostack::Vector<float> &, bool, bool);
        
    // Double type definitions
    template Geostack::Vector<double> GeoJson<double>::geoJsonFileToVector(std::string, bool);
    template Geostack::Vector<double> GeoJson<double>::geoJsonToVector(std::string, bool);
    template std::string GeoJson<double>::vectorToGeoJson(Geostack::Vector<double> &, bool, bool);
}
