/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#include <stdexcept>
#include <cmath>

#include "gs_palette.h"

namespace Geostack
{
    // Values from https://waldyrious.net/viridis-palette-generator/
    const std::map<std::string, std::vector<std::vector<double> > > Palette::Palettes = {
        { "grey", { 
            { 0, 0, 0 },
            { 64, 64, 64 },
            { 128, 128, 128 },
            { 192, 192, 192 },
            { 255, 255, 255 }
        } }, 
        { "viridis", { 
            { 253, 231, 37 }, 
            { 94, 201, 98 }, 
            { 33, 145, 140 }, 
            { 59, 82, 139 }, 
            { 68, 1, 84 }
        } },
        { "inferno", {
            { 252, 255, 164 },
            { 249, 142, 9 }, 
            { 188, 55, 84 }, 
            { 87, 16, 110 }, 
            { 0, 0, 4 }
        } },
        { "magma", {
            { 252, 253, 191 },
            { 252, 137, 97 },
            { 183, 55, 121 },
            { 81, 18, 124 },
            { 0, 0, 4 }
        } },
        { "plasma", {
            { 240, 249, 33 },
            { 248, 149, 64 },
            { 204, 71, 120 },
            { 126, 3, 168 },
            { 13, 8, 135 }
        } }
    };

    std::vector<double> Palette::getRGB(const std::string name, double v) {

        // Check range
        if (v < 0.0 || v > 1.0) {
            throw std::runtime_error("Palette value out of range (0-1)");
        }

        // Check name
        auto pit = Palettes.find(name);
        if (pit == Palettes.end()) {
            throw std::runtime_error("Palette name '" + name + "' not found");
        }

        // Get stops
        v *= 4.0;
        std::uint32_t start = std::floor(v);
        std::uint32_t stop = std::ceil(v);
        v -= (double)start;

        // Get RGB triplet
        auto palette = pit->second;
        auto RGBStart = palette[start];
        auto RGBStop = palette[stop];
        return { 
            (1.0-v)*RGBStart[0]+v*RGBStop[0],
            (1.0-v)*RGBStart[1]+v*RGBStop[1],
            (1.0-v)*RGBStart[2]+v*RGBStop[2]
        };
    }
    
}