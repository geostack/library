/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <iostream>
#include <sstream>

#include "json11.hpp"
#include "base64.hpp"
#include "miniz.h"

#include "gs_string.h"
#include "gs_raster_json.h"

using namespace json11;

namespace Geostack
{
    /**
    * Write Raster to Json string
    * @param compress deflate data vector.
    * @param jsonConfig configuration as json string
    * @return Json string with header and data.
    */
    template <typename RTYPE, typename CTYPE>
    std::string JsonHandler<RTYPE, CTYPE>::toJson(Raster<RTYPE, CTYPE> &r, bool compress, std::string jsonConfig) {
        
        Json::object jsonRoot;
        RTYPE noDataValue;
        RTYPE rasterNullValue = JsonHandler<RTYPE, CTYPE>::getNullValue();
        // set default values
        bool legacy = false;
        std::string encoding = "base64";

        if (!jsonConfig.empty()) {
                
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }

            // check if writing legacy format (used in Spark2.0 and INDRA)
            if (config["legacy"].is_null()) {
                legacy = false;
            } else {
                legacy = config["legacy"].bool_value();
            }
            
            // check if encoding is specified in jsonConfig
            if (config["encoding"].is_null() || config["encoding"] == "base64") {
                encoding = "base64";
            } else if (config["encoding"] == "ascii") {
                // if encoding is set to ascii, compress flag doesn't have any effect
                encoding = "ascii";
            }

            // check if noDataValue is specified, if not, assign to rasterNullValue
            if (config["noDataValue"].is_null()) {
                noDataValue = rasterNullValue;
            } else {
                if (config["noDataValue"].is_number()) {
                    noDataValue = config["noDataValue"].number_value();
                } else if (config["noDataValue"].is_string()) {
                    noDataValue = Strings::toNumber<RTYPE>(config["noDataValue"].string_value());
                }
            }
        }

        // handle noDataValue, set noDataValue in the output json
        switch (r.getRasterDataType()) {
            case RasterDataType::UInt8:
                jsonRoot["noDataValue"] = (int)noDataValue;
                jsonRoot["dataType"] = "uint8";
                break;
            case RasterDataType::UInt32:
                jsonRoot["noDataValue"] = (int)noDataValue;
                jsonRoot["dataType"] = "uint32";
                break;
            case RasterDataType::Float:
                jsonRoot["noDataValue"] = (float)noDataValue;
                jsonRoot["dataType"] = "float32";
                break;
            case RasterDataType::Double:
                jsonRoot["noDataValue"] = (double)noDataValue;
                jsonRoot["dataType"] = "float64";
                break;
        }
        
        // Set name        
        jsonRoot["name"] = r.template getProperty<std::string>("name");

        // Set dimensions
        auto dims = r.getRasterDimensions();
        if (!legacy) {
            jsonRoot["nx"] = (int)dims.d.nx;
            jsonRoot["ny"] = (int)dims.d.ny;
            jsonRoot["nz"] = (int)1;
            jsonRoot["hx"] = dims.d.hx;
            jsonRoot["hy"] = dims.d.hy;
            jsonRoot["hz"] = dims.d.hz;
            jsonRoot["ox"] = dims.d.ox;
            jsonRoot["oy"] = dims.d.oy;
            jsonRoot["oz"] = dims.d.oz;
        
            // Set projection
            jsonRoot["proj4"] = Projection::toPROJ4(r.getProjectionParameters());
            
            // Set endianess
            #if defined(ENDIAN_LITTLE)
            jsonRoot["endian"] = "little";
            #else
            jsonRoot["endian"] = "big";
            #endif
        } else {
            jsonRoot["xCells"] = (int)dims.d.nx;
            jsonRoot["yCells"] = (int)dims.d.ny;
            jsonRoot["zCells"] = (int)1;
            jsonRoot["xCellSize"] = dims.d.hx;
            jsonRoot["yCellSize"] = dims.d.hy;
            jsonRoot["zcellSize"] = dims.d.hz;
            jsonRoot["xMin"] = dims.d.ox;
            jsonRoot["yMin"] = dims.d.oy;
            jsonRoot["zMin"] = dims.d.oz;
            jsonRoot["xMax"] = dims.ex;
            jsonRoot["yMax"] = dims.ey;
            jsonRoot["zMax"] = dims.ez;
        
            // Set projection
            jsonRoot["projection"] = Projection::toPROJ4(r.getProjectionParameters());

            // Set endianess
            #if defined(ENDIAN_LITTLE)
            jsonRoot["databyteorder"] = "lsbfirst";
            #else
            jsonRoot["databyteorder"] = "msbfirst";
            #endif
        }

        // Set bounds
        auto boundNW = Coordinate<double>( { dims.d.ox, dims.ey } );
        auto boundNE = Coordinate<double>( { dims.ex, dims.ey } );
        auto boundSE = Coordinate<double>( { dims.ex, dims.d.oy } );
        auto boundSW = Coordinate<double>( { dims.d.ox, dims.d.oy } );
        
        // Convert bounds to EPSG4326
        auto proj = r.getProjectionParameters();
        ProjectionParameters<double> proj_EPSG4326 = Projection::parsePROJ4("+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs");
        Projection::convert(boundNW, proj_EPSG4326, proj);
        Projection::convert(boundNE, proj_EPSG4326, proj);
        Projection::convert(boundSE, proj_EPSG4326, proj);
        Projection::convert(boundSW, proj_EPSG4326, proj);

        jsonRoot["bounds"] = Json::array( {
            Json::array( { boundNW.p, boundNW.q } ),
            Json::array( { boundNE.p, boundNE.q } ),
            Json::array( { boundSE.p, boundSE.q } ),
            Json::array( { boundSW.p, boundSW.q } )
        } );

        if (dims.d.nz > 1) {
            std::cout << "WARNING: Only first raster layer is converted to JSON" << std::endl;
        }

        // Unpack all data
        uint64_t k = 0; // TODO, write multiple layers
        uint64_t koff = (uint64_t)k*TileMetrics::tileSizeSquared;
        jsonRoot["numberOfRasters"] = 1;

        std::vector<RTYPE> data;
        std::vector<std::vector<RTYPE> > dataCache;
        dataCache.resize(dims.tx);

        for (int32_t tj = 0; tj < dims.ty; tj++) {

            // Populate cache
            for (uint32_t ti = 0; ti < dims.tx; ti++) {
                r.getTileData(ti, dims.ty-1-tj, dataCache[ti]);
            }

            // Unpack data
            uint32_t rowEnd = dims.d.nx&TileMetrics::tileSizeMask;
            uint32_t nj = tj == 0 ? TileMetrics::tileSize*(1-dims.ty)+dims.d.ny : TileMetrics::tileSize;
            for (uint32_t j = 0; j < nj; j++) {

                // Calculate offset
                uint64_t joff = ((uint64_t)(nj-j-1)<<TileMetrics::tileSizePower)+koff;

                // Write lines
                for (uint32_t ti = 0; ti < dims.tx-1; ti++) {
                    auto *pv = dataCache[ti].data()+joff;
                    for (uint32_t i = 0; i < TileMetrics::tileSize; i++, pv++) {
                        auto cellValue = *pv;
                        // replace rasterNullValue with noDataValue, if they are different
                        if (noDataValue != rasterNullValue) {
                            cellValue = (cellValue == rasterNullValue) ? noDataValue : cellValue;
                        }
                        data.push_back(cellValue);
                    }
                }

                auto *pv = dataCache[dims.tx-1].data()+joff;
                for (uint32_t i = 0; i < rowEnd; i++, pv++) {
                    auto cellValue = *pv;
                    // replace rasterNullValue with noDataValue, if they are different
                    if (noDataValue != rasterNullValue) {
                        cellValue = (cellValue == rasterNullValue) ? noDataValue : cellValue;
                    }
                    data.push_back(cellValue);
                }
            }
        }        

        // add default compression
        if (legacy) {
            jsonRoot["zipped"] = false;
        } else {
            jsonRoot["zip"] = false;
        }

        if (encoding == "base64") {
            std::vector<unsigned char> cData;
            if (compress) {
                // Set data sizes
                uint64_t dataLength = data.size()*sizeof(RTYPE);
                mz_ulong compressedDataLen = compressBound((mz_ulong)dataLength);

                // Compress
                cData.resize(compressedDataLen);
                mz_ulong len = compressedDataLen;
                int status = compress2(cData.data(), &len, 
                    reinterpret_cast<const unsigned char *>(data.data()), (mz_ulong)dataLength, MZ_BEST_SPEED);

                if (status != Z_OK) {
                    std::stringstream err;
                    err << "Compression failed '" << mz_error(status) << "'";
                    throw std::runtime_error(err.str());
                }

                // Shrink vector
                cData.resize(len);
                cData.shrink_to_fit();

                // update compression flag
                if (legacy) {
                    jsonRoot["zipped"] = true;
                } else {
                    jsonRoot["zip"] = true;
                }
            } else {
                // write uncompressed base64 string
                unsigned char *pData = reinterpret_cast<unsigned char *>(data.data());
                cData = std::vector<unsigned char>(pData, pData+data.size()*sizeof(RTYPE));
            }

            // Convert to b64
            jsonRoot["data"] = base64::to_base64(cData);
            jsonRoot["encoding"] = "b64";
        } else if (encoding == "ascii") {
            // write array of values
            Json::array dataArray;
            for (int32_t j = dims.d.ny-1; j >= 0 ; j--) {
                Json::array arr;
                for (int32_t i = 0; i < dims.d.nx; i++) {
                    if ((r.getRasterDataType() == RasterDataType::UInt8) || 
                        (r.getRasterDataType() == RasterDataType::UInt32)) {
                            arr.push_back((int)data[i + j * dims.d.nx]);
                    } else {
                        arr.push_back((double)data[i + j * dims.d.nx]);
                    }
                }
                dataArray.push_back(arr);
            }
            jsonRoot["data"] = dataArray;
            jsonRoot["encoding"] = "text";
        }

        Json json = jsonRoot;
        return json.dump();
    }

    /**
    * Read Json to raster.
    * @param fileName input file name.
    * @param r input Raster.
    * @param jsonConfig write configuration.
    */
    template <typename RTYPE, typename CTYPE>
    void JsonHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // TODO
        throw std::runtime_error("Raster JSON read functions are not yet available");
    }

    /**
    * Write Json from raster.
    * @param fileName output file name.
    * @param r input Raster.
    * @param jsonConfig write configuration.
    */
    template <typename RTYPE, typename CTYPE>
    void JsonHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            throw std::runtime_error("Raster has no data to write to json file");
        }

        bool compress;

        if (!jsonConfig.empty()) {

            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }

            // get compress flag from jsonConfig
            // by default, write a zipped string 
            if (config["compress"].is_null()) {
                compress = true;
            } else {
                compress = config["compress"].bool_value();
            }
        }

        // Write data
        std::ofstream out(fileName);
        out << toJson(r, compress, jsonConfig);
        
        if (out.is_open()) {
            out.close();
        }
    }

    /**
    * Return null value type for float RTYPE and float CTYPE.
    * @return lowest possible float value.
    */
    template<> float JsonHandler<float, float>::getNullValue() {
        return std::numeric_limits<float>::lowest();
    }

    /**
    * Return null value for unsigned char RTYPE and float CTYPE.
    * @return unsigned int null value.
    */
    template<> uint8_t JsonHandler<uint8_t, float>::getNullValue() {
        return Geostack::getNullValue<uint8_t>();
    }

    /**
    * Return null value for unsigned int RTYPE and float CTYPE.
    * @return unsigned int null value.
    */
    template<> uint32_t JsonHandler<uint32_t, float>::getNullValue() {
        return Geostack::getNullValue<uint32_t>();
    }

    /**
    * Return null value for double RTYPE and double CTYPE.
    * @return lowest possible double value.
    */
    template<> double JsonHandler<double, double>::getNullValue() {
        return std::numeric_limits<double>::lowest();
    }

    /**
    * Return null value for unsigned char RTYPE and double CTYPE.
    * @return unsigned int null value.
    */
    template<> uint8_t JsonHandler<uint8_t, double>::getNullValue() {
        return Geostack::getNullValue<uint8_t>();
    }

    /**
    * Return null value for unsigned int RTYPE and double CTYPE.
    * @return unsigned int null value.
    */
    template<> uint32_t JsonHandler<uint32_t, double>::getNullValue() {
        return Geostack::getNullValue<uint32_t>();
    }

    // Type definitions
    template class JsonHandler<float, float>;
    template class JsonHandler<uint8_t, float>;
    template class JsonHandler<uint32_t, float>;
    // template std::string JsonHandler<float, float>::toJson(Raster<float, float> &, bool);
    // template std::string JsonHandler<uint32_t, float>::toJson(Raster<uint32_t, float> &, bool);
    // template std::string JsonHandler<uint8_t, float>::toJson(Raster<uint8_t, float> &, bool);
    
    template class JsonHandler<double, double>;
    template class JsonHandler<uint8_t, double>;
    template class JsonHandler<uint32_t, double>;
    // template std::string JsonHandler<double, double>::toJson(Raster<double, double> &, bool);
    // template std::string JsonHandler<uint32_t, double>::toJson(Raster<uint32_t, double> &, bool);
    // template std::string JsonHandler<uint8_t, double>::toJson(Raster<uint8_t, double> &, bool); 
}