/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <functional>
#include <iomanip>
#include <cmath>

#include "json11.hpp"
#include "miniz.h"

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_projection.h"
#include "gs_geotiff.h"

using namespace json11;

namespace Geostack
{
    template <typename DATAVECTOR>
    void readGeoTIFFData(std::fstream &fileStream, uint32_t length, DATAVECTOR &v) {

        // Resize vector
        v.resize(length);

        // Read data
        fileStream.read(reinterpret_cast<char *>(&v[0]), length*sizeof(typename DATAVECTOR::value_type));
    }

    template <typename DATAVECTOR, typename DATATYPE>
    std::fstream::pos_type writeGeoTIFFData(std::fstream &fileStream, uint32_t length, DATAVECTOR &v) {
    
        // Record position of value
        auto valueOffset = fileStream.tellp();

        // Write data
        fileStream.write(reinterpret_cast<char *>(&v[0]), length*sizeof(DATATYPE));
        return valueOffset;
    }
    
    /**
    * Convert vector of type DATATYPE containing data of type INTYPE to vector of type OUTTYPE. 
    * @param in the input vector of type DATATYPE.
    * @param out the output vector of type OUTTYPE.
    * @param nullValueString the no data value stored as a string.
    */
    template <typename DATATYPE, typename INTYPE, typename OUTTYPE>
    void convertGeoTIFFVector(const std::vector<DATATYPE> &in, std::vector<OUTTYPE> &out, std::string nullValueString) {
    
        // Resize vector
        out.clear();
        out.resize(in.size()*sizeof(DATATYPE)/sizeof(INTYPE));

        // Convert input pointer
        const INTYPE *p = reinterpret_cast<const INTYPE *>(&in[0]);

        // Check for valid string
        if (nullValueString.empty()) {

            // Copy data
            for (std::size_t i = 0; i < out.size(); i++) {
                out[i] = (OUTTYPE)*(p+i);
            }

        } else {

            // Get null values
            INTYPE nullValueIn = Strings::toNumber<INTYPE>(nullValueString);
            OUTTYPE nullValueOut = Geostack::getNullValue<OUTTYPE>();

            // Copy data
            for (std::size_t i = 0; i < out.size(); i++) {
                out[i] = *(p+i) == nullValueIn ? nullValueOut : (OUTTYPE)*(p+i);
            }
        }
    }
    
    std::vector<uint32_t> GeoTIFFDirectory::GeoTIFFDataSizes = { 0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8, 0, 0, 0, 8, 8, 8 };
    
    /**
    * GeoTIFFDirectory default constructor
    */
    GeoTIFFDirectory::GeoTIFFDirectory():
        type(0), size(0), length(0), value(0) { }

    /**
    * GeoTIFFDirectory value constructor
    * @param type directory value type.
    * @param length number of values in directory.
    * @param value the value or offset to values.
    */
    GeoTIFFDirectory::GeoTIFFDirectory(GeoTIFFDataTypes::Type type, uint64_t length, uint64_t value):
        type(static_cast<uint16_t>(type)), length(length), value(value) {

        // Cast to enum
        size = GeoTIFFDataSizes[type];
    }
    
    /**
    * GeoTIFFDirectory directory reader.
    * @param fileStream stream to read from.
    * @return the directory tag.
    */
    uint16_t GeoTIFFDirectory::read(std::fstream &fileStream, bool isBigTIFF) {

        // Read data
        uint16_t rTag;
        fileStream.read(reinterpret_cast<char *>(&rTag), sizeof(rTag));
        fileStream.read(reinterpret_cast<char *>(&type), sizeof(type));

        // Get size of type
        if (type <= 18) {
            size = GeoTIFFDataSizes[type];
            if (size == 0) {

                // Unknown size
                throw std::runtime_error("Invalid TIFF data type");
            }
        } else {

            // Unknown type
            throw std::runtime_error("Unknown TIFF data type");
        }

        if (isBigTIFF) {
            fileStream.read(reinterpret_cast<char *>(&length), sizeof(length));
            fileStream.read(reinterpret_cast<char *>(&value), sizeof(value));
        } else {

            uint32_t length32;
            fileStream.read(reinterpret_cast<char *>(&length32), sizeof(length32));
            length = length32;

            uint32_t value32;
            fileStream.read(reinterpret_cast<char *>(&value32), sizeof(value32));
            value = value32;
        }

        // Return tag id
        return rTag;
    }
    
    /**
    * GeoTIFFDirectory directory writer.
    * @param tag tag value for directory.
    * @param fileStream stream to write to.
    * @return the file position of the first value.
    */
    std::fstream::pos_type GeoTIFFDirectory::write(uint16_t tag, std::fstream &fileStream, bool isBigTIFF) {

        // Write data
        fileStream.write(reinterpret_cast<char *>(&tag), sizeof(tag));
        fileStream.write(reinterpret_cast<char *>(&type), sizeof(type));
        if (isBigTIFF) {
            fileStream.write(reinterpret_cast<char *>(&length), sizeof(length));
        } else {
            uint32_t length32 = (uint32_t)length;
            fileStream.write(reinterpret_cast<char *>(&length32), sizeof(length32));
        }

        // Record position of value
        auto valueOffset = fileStream.tellp();
        if (isBigTIFF) {
            fileStream.write(reinterpret_cast<char *>(&value), sizeof(value));
        } else {
            uint32_t value32 = (uint32_t)value;
            fileStream.write(reinterpret_cast<char *>(&value32), sizeof(value32));
        }
        return valueOffset;
    }

    // Read data from entry
    template <typename DATAVECTOR>
    bool GeoTIFFDirectory::readData(std::fstream &fileStream, DATAVECTOR &v, bool isBigTIFF) {

        // Check data sizes
        if (sizeof(typename DATAVECTOR::value_type) == size) {            

            // Check whether value holds the data or the offset
            uint64_t lengthBytes = (uint64_t)size*length;
            if ((isBigTIFF && lengthBytes > 8) || (!isBigTIFF && lengthBytes > 4)) {

                // Jump to data location
                fileStream.seekg(value);

                // Read raw data
                readGeoTIFFData<DATAVECTOR>(fileStream, length, v);

            } else {

                // Resize vector
                v.resize(length);

                // Make a copy of the data in the vector to standardise access
                std::memcpy(reinterpret_cast<typename DATAVECTOR::value_type *>(&v[0]), &value, lengthBytes);
            }
            return true;
        }
        return false;
    }
    
    /**
    * Return sample type for float RTYPE and float CTYPE.
    * @return Float sample type.
    */
    template<> GeoTIFFSampleTypes::Type GeoTIFFHandler<float, float>::getSampleType() {
        return GeoTIFFSampleTypes::Float;
    }

    /**
    * Return sample type for unsigned char RTYPE and float CTYPE.
    * @return Float sample type.
    */
    template<> GeoTIFFSampleTypes::Type GeoTIFFHandler<uint8_t, float>::getSampleType() {
        return GeoTIFFSampleTypes::UnsignedInt;
    }

    /**
    * Return sample type for unsigned int RTYPE and float CTYPE.
    * @return Integer sample type.
    */
    template<> GeoTIFFSampleTypes::Type GeoTIFFHandler<uint32_t, float>::getSampleType() {
        return GeoTIFFSampleTypes::UnsignedInt;
    }

    /**
    * Return sample type for double RTYPE and double CTYPE.
    * @return Float sample type.
    */
    template<> GeoTIFFSampleTypes::Type GeoTIFFHandler<double, double>::getSampleType() {
        return GeoTIFFSampleTypes::Float;
    }

    /**
    * Return sample type for unsigned char RTYPE and double CTYPE.
    * @return Float sample type.
    */
    template<> GeoTIFFSampleTypes::Type GeoTIFFHandler<uint8_t, double>::getSampleType() {
        return GeoTIFFSampleTypes::UnsignedInt;
    }

    /**
    * Return sample type for unsigned int RTYPE and double CTYPE.
    * @return Float sample type.
    */
    template<> GeoTIFFSampleTypes::Type GeoTIFFHandler<uint32_t, double>::getSampleType() {
        return GeoTIFFSampleTypes::UnsignedInt;
    }
    
    /**
    * Return null value type for float RTYPE and float CTYPE.
    * @return lowest possible float value.
    */
    template<> float GeoTIFFHandler<float, float>::getNullValue() {
        return std::numeric_limits<float>::lowest();
    }

    /**
    * Return null value for unsigned char RTYPE and float CTYPE.
    * @return unsigned int null value.
    */
    template<> uint8_t GeoTIFFHandler<uint8_t, float>::getNullValue() {
        return Geostack::getNullValue<uint8_t>();
    }

    /**
    * Return null value for unsigned int RTYPE and float CTYPE.
    * @return unsigned int null value.
    */
    template<> uint32_t GeoTIFFHandler<uint32_t, float>::getNullValue() {
        return Geostack::getNullValue<uint32_t>();
    }

    /**
    * Return null value for double RTYPE and double CTYPE.
    * @return lowest possible double value.
    */
    template<> double GeoTIFFHandler<double, double>::getNullValue() {
        return std::numeric_limits<double>::lowest();
    }

    /**
    * Return null value for unsigned char RTYPE and double CTYPE.
    * @return unsigned int null value.
    */
    template<> uint8_t GeoTIFFHandler<uint8_t, double>::getNullValue() {
        return Geostack::getNullValue<uint8_t>();
    }

    /**
    * Return null value for unsigned int RTYPE and double CTYPE.
    * @return unsigned int null value.
    */
    template<> uint32_t GeoTIFFHandler<uint32_t, double>::getNullValue() {
        return Geostack::getNullValue<uint32_t>();
    }

    /**
    * Read GeoTIFFHandler format to %Raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void GeoTIFFHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {
        
        // Get verbosity level
        uint8_t verbose = Geostack::Solver::getSolver().getVerboseLevel();

        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr) {
        
            // Get file stream handle
            std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

            // Open file
            if (!fileStream.is_open()) {
                fileStream.open(fileName, std::ios::in | std::ios::binary);
            }

            // Check file
            if (!fileStream) {
                std::stringstream err;
                err << "Cannot open '" << fileName << "' for reading";
                throw std::runtime_error(err.str());
            }

            // Read and check endianness
            uint8_t byteOrder[2];
            fileStream.read(reinterpret_cast<char *>(&byteOrder), sizeof(byteOrder));

            #if !defined(ENDIAN_BIG) && !defined(ENDIAN_LITTLE)
            #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
            #endif
        
            if (byteOrder[0] == 'I' && byteOrder[1] == 'I') {

                // Little endian
                #if defined(ENDIAN_BIG)
                throw std::runtime_error("Only little endianness is supported");
                #endif

            } else if (byteOrder[0] == 'M' && byteOrder[1] == 'M') {

                // Big endian
                #if defined(ENDIAN_LITTLE)
                throw std::runtime_error("Only big endianness is supported");
                #endif

            } else {

                // Unknown
                throw std::runtime_error("Invalid TIFF file header");
            }

            // Read version
            uint16_t version;
            uint32_t offset32;
            uint64_t offset;
            uint16_t offsetByteSize = 4;
            bool isBigTIFF = false;
            bool isCOG = false;
            fileStream.read(reinterpret_cast<char *>(&version), sizeof(version));
            if (version == (uint16_t)42) {

                // Read first directory offset
                fileStream.read(reinterpret_cast<char *>(&offset32), sizeof(offset32));
                offset = offset32;
                // std::cout << "version: " << version << "," << "offset: " << offset << "," << "isBigTiff: " << isBigTIFF << std::endl;
            } else if (version == (uint16_t)43) {

                // Set big tiff flag
                isBigTIFF = true;
                
                // Read byte size, should be 8 for big tiffs
                uint16_t byteSize;
                fileStream.read(reinterpret_cast<char *>(&byteSize), sizeof(byteSize));
                if (byteSize != 8) {
                    throw std::runtime_error("Invalid byte size in BigTIFF header");
                }
                
                // Read reserved word, should be zero
                uint16_t reserved;
                fileStream.read(reinterpret_cast<char *>(&reserved), sizeof(reserved));
                if (reserved != 0) {
                    throw std::runtime_error("Invalid BigTIFF file header");
                }

                // Read first directory offset
                fileStream.read(reinterpret_cast<char *>(&offset), sizeof(offset));
                // std::cout << "version: " << version << "," << "offset: " << offset << "," << "isBigTiff: " << isBigTIFF << std::endl;
            } else if (version != (uint16_t)42) {

                // Unknown
                throw std::runtime_error("Unsupported TIFF version");
            }
            
            // read ghost area
            std::map<std::string, std::string> gdalGhostHeader;
            if (offset > 43) {
                // read ghost bytes (as written by GDAL in COG)
                static const auto ghostAreaHeaderSize = 43; 
                // Some geotiffs (e.g. those created using "libtiff") store their IFD(s) after the image data, in which 
                // case we are reading image data (rather than ghost area header, ASCII data) into the buffer. 
                // Place a null character at the end of the buffer (after the bytes read) to prevent code below from 
                // searching beyond the end of the buffer.
                auto ghostBytes = std::vector<char>(ghostAreaHeaderSize + 1); 
                ghostBytes[ghostAreaHeaderSize] = '\0';
                fileStream.read(ghostBytes.data(), ghostAreaHeaderSize);

                auto splitStr = Strings::split(ghostBytes.data(), ' ');
                if (!splitStr.empty()) {
                    splitStr = Strings::split(splitStr[0], '=');
                    if (splitStr.size() > 1) {
                        gdalGhostHeader[splitStr[0]] = splitStr[1];
                    }
                
                    auto p = gdalGhostHeader.find("GDAL_STRUCTURAL_METADATA_SIZE");
                    if (p != gdalGhostHeader.end()) {
                        isCOG = true;
                    
                        // extract size of remaining bytes of ghost header
                        auto remainingBytes = Strings::toNumber<uint16_t>(splitStr[1]);
                    
                        // read remaining header information from ghost header
                        ghostBytes.resize(remainingBytes);
                        fileStream.read(ghostBytes.data(), remainingBytes);
                
                        // split lines and parse information to gdalGhostHeader map
                        splitStr.clear();
                        auto splitHeader = Strings::split(ghostBytes.data(), '\n');
                        for (int i = 0; i < splitHeader.size(); i++) {
                            splitStr = Strings::split(splitHeader[i], '=');
                            if (splitStr.size() > 1) {
                                gdalGhostHeader[splitStr[0]] = splitStr[1];
                            }
                            splitStr.clear();
                        }
                    }
                }
            }
        
            // Create list of directories
            std::vector<std::map<uint16_t, GeoTIFFDirectory> > directoriesList; 
            while (offset != 0) {
                std::map<uint16_t, GeoTIFFDirectory> directories;
                
                // Read number of directories
                fileStream.seekg(offset);
                uint64_t entries;
                if (isBigTIFF) {
                    fileStream.read(reinterpret_cast<char *>(&entries), sizeof(entries));
                } else {
                    uint16_t entries16;
                    fileStream.read(reinterpret_cast<char *>(&entries16), sizeof(entries16));
                    entries = entries16;
                }

                // Read directories
                for (uint16_t i = 0; i < entries; i++) {
                    GeoTIFFDirectory directory;
                    uint16_t tag = directory.read(fileStream, isBigTIFF);
                    directories[tag] = directory;
                }

                // Read next directory offset
                if (isBigTIFF) {
                    fileStream.read(reinterpret_cast<char *>(&offset), sizeof(offset));
                } else {
                    fileStream.read(reinterpret_cast<char *>(&offset32), sizeof(offset32));
                    offset = offset32;
                }
                
                directoriesList.emplace_back(directories);
            };
            
            std::map<uint16_t, GeoTIFFDirectory> directories;
            if (directoriesList.size() > 1 || isCOG) {
                if (verbose <= Geostack::Verbosity::Info) {
                    std::stringstream infoStream;
                    infoStream << "Multiple IFDs found.\n";
                    infoStream << "Number of IFDs are: " << directoriesList.size() << "\n";
                    infoStream << "Reading first IFD";
                    std::cout << infoStream.str() << std::endl;
                }

                // use first directory, it contains raw data (as per spec) 
                // TODO: add reference to spec
                directories = directoriesList[0];
            } else if (directoriesList.size() == 1) {
                directories = directoriesList[0];
            }
        
            // Data vectors
            std::vector<uint8_t> v8;
            std::vector<uint16_t> v16;
            std::vector<uint32_t> v32;

            // Search for single value tags
            valuesIn.clear();
            std::map<std::string, uint16_t> valueTags = {
		        { "ImageWidth", 256 },
		        { "ImageLength", 257 },
		        { "BitsPerSample", 258 },
		        { "Compression", 259 },
		        { "PhotometricInterpretation", 262 },
		        { "StripOffsets", 273 },
		        { "SamplesPerPixel", 277 },
                { "RowsPerStrip", 278 },
		        { "StripByteCounts", 279 },
		        { "PlanarConfiguration", 284 },
		        { "Predictor", 317 },
		        { "TileWidth", 322 },
		        { "TileLength", 323 },
		        { "TileOffsets", 324 },
		        { "TileByteCounts", 325 },
		        { "SampleFormat", 339 } };

            for (auto &t : valueTags) {
                auto p = directories.find(t.second);
                if (p != directories.end()) {

                    // Tag values can be 16 or 32 bits
                    GeoTIFFDirectory &d = (*p).second;

                    if (d.readData<std::vector<uint16_t> >(fileStream, v16, isBigTIFF)) {
                        valuesIn[t.first] = std::vector<uint64_t>(v16.begin(), v16.end());
                    } else if (d.readData<std::vector<uint32_t> >(fileStream, v32, isBigTIFF)) {
                        valuesIn[t.first] = std::vector<uint64_t>(v32.begin(), v32.end());
                    } else if (!d.readData<std::vector<uint64_t> >(fileStream, valuesIn[t.first], isBigTIFF)) {
                        throw std::runtime_error("Cannot read values in TIFF header");
                    }
                }
            }
            
            // Get number of values per pixel
            samplesPerPixel = valuesIn["SamplesPerPixel"][0];
        
            // Get data format for pixel
            sampleFormat = valuesIn["SampleFormat"][0];
            if (sampleFormat == 0 || sampleFormat > 3) {
                std::stringstream err;
                err << "Unhandled pixel format: " << sampleFormat << "'";
                throw std::runtime_error(err.str());
            }

            // Check for GDAL nodata tag (42113)
            auto pNullValue = directories.find(42113);
            if (pNullValue != directories.end()) {

                // Get directory handle
                GeoTIFFDirectory &d = (*pNullValue).second;

                // Read value into string
                if (d.getType() == GeoTIFFDataTypes::Ascii)
                    d.readData<std::string>(fileStream, nullValueString, isBigTIFF);
            }        

            // Get and check dimensions
            nx = valuesIn["ImageWidth"][0];
            if (nx == 0) {
                throw std::runtime_error("TIFF raster width is zero");
            }
            ny = valuesIn["ImageLength"][0];
            if (ny == 0) {
                throw std::runtime_error("TIFF raster height is zero");
            }
            CTYPE hx = r.dim.d.hx;
            CTYPE hy = r.dim.d.hy;
            CTYPE ox = r.dim.d.ox;
            CTYPE oy = r.dim.d.oy;

            // Check for GeoTIFF ModelTransformation tag (34264)
            auto pTransform = directories.find(34264);
            if (pTransform != directories.end()) {

                // Get directory handle
                GeoTIFFDirectory &d = (*pTransform).second;

                // Read values (http://geotiff.maptools.org/spec/geotiff2.6.html)
                std::vector<double> transform;
                if (d.getType() == GeoTIFFDataTypes::Double && 
                    d.readData<std::vector<double> >(fileStream, transform, isBigTIFF)) {

                    // Check transform
                    if (transform.size() != 16) {
                        throw std::runtime_error("TIFF ModelTransformationTag must contain 16 components");
                    }
                    if (transform[12] != 0.0 || transform[13] != 0.0 || transform[14] != 0.0 || transform[15] != 1.0) {
                        throw std::runtime_error("TIFF ModelTransformationTag matrix transform not supported");
                    }
                    if (transform[1] != 0.0 || transform[2] != 0.0 || transform[4] != 0.0 || transform[6] != 0.0) {
                        throw std::runtime_error("TIFF ModelTransformationTag matrix transform not supported");
                    }

                    // Set values
                    flip_y = transform[5] < 0.0;
                    hx = fabs(transform[0]);
                    hy = fabs(transform[5]);
                    ox = transform[3];
                    oy = transform[7];

                    // TODO support z values
                }
            } else {

                // Check for GeoTIFFHandler pixel scale tag (33550)
                auto pPixelScale = directories.find(33550);
                if (pPixelScale != directories.end()) {

                    // Get directory handle
                    GeoTIFFDirectory &d = (*pPixelScale).second;

                    // Read value into string
                    std::vector<double> pixelScale;
                    if (d.getType() == GeoTIFFDataTypes::Double && 
                        d.readData<std::vector<double> >(fileStream, pixelScale, isBigTIFF)) {
                        hx = (CTYPE)pixelScale[0];
                        hy = (CTYPE)pixelScale[1];
                        flip_y = pixelScale[1] > 0.0;
                    }
                }

                // Check for GeoTIFF tie point tag (33922)
                auto pTiePoint = directories.find(33922);
                if (pTiePoint != directories.end()) {

                    // Get directory handle
                    GeoTIFFDirectory &d = (*pTiePoint).second;

                    // Read values
                    std::vector<double> tiePoint;
                    if (d.getType() == GeoTIFFDataTypes::Double && 
                        d.readData<std::vector<double> >(fileStream, tiePoint, isBigTIFF)) {
                        ox = (CTYPE)tiePoint[3];
                        oy = (CTYPE)tiePoint[4];
                    }
                }
            }

            // Flip if required
            if (flip_y) {                
                oy -= (CTYPE)hy*ny;
            }

            if (hx <= (CTYPE)0.0 || hy <= (CTYPE)0.0) {
                throw std::runtime_error("TIFF spacing must be greater than zero");
            }

            // Check for GeoTIFF directory tag (34735) 
            std::map<uint16_t, std::vector<uint16_t> > geoKeyShortValues;
            std::map<uint16_t, std::vector<double> > geoKeyDoubleValues;
            std::map<uint16_t, std::string> geoKeyStringValues;
            auto pGeoKeyShortDirectory = directories.find(34735);
            if (pGeoKeyShortDirectory != directories.end()) {
            
                // Read double values
                std::vector<double> geoKeysDouble;
                auto pGeoKeyDoubleDirectory = directories.find(34736);
                if (pGeoKeyDoubleDirectory != directories.end()) {
                    GeoTIFFDirectory &dDouble = (*pGeoKeyDoubleDirectory).second;
                    if (dDouble.getType() == GeoTIFFDataTypes::Double) {
                        dDouble.readData<std::vector<double> >(fileStream, geoKeysDouble, isBigTIFF);
                    }
                }

                // Read string values
                std::string geoKeysString;
                auto pGeoKeyStringDirectory = directories.find(34737);
                if (pGeoKeyStringDirectory != directories.end()) {
                    GeoTIFFDirectory &dString = (*pGeoKeyStringDirectory).second;
                    if (dString.getType() == GeoTIFFDataTypes::Ascii) {
                        dString.readData<std::string>(fileStream, geoKeysString, isBigTIFF);
                    }
                }
                
                // Read short values
                GeoTIFFDirectory &dShort = (*pGeoKeyShortDirectory).second;
                std::vector<uint16_t> geoKeysShort;
                if (dShort.getType() == GeoTIFFDataTypes::Short &&
                    dShort.readData<std::vector<uint16_t> >(fileStream, geoKeysShort, isBigTIFF) && geoKeysShort.size() > 4) {

                    // Get header
                    uint16_t keyDirectoryVersion = geoKeysShort[0];
                    uint16_t keyRevision = geoKeysShort[1];
                    uint16_t minorRevision = geoKeysShort[2];
                    uint16_t numberOfKeys = geoKeysShort[3];

                    // Read entries
                    if (keyDirectoryVersion == 1) {

                        for (uint16_t keyID = 0; keyID < numberOfKeys; keyID++) {

                            // Read values
                            uint16_t valueID = geoKeysShort[4+keyID*4];
                            uint16_t valueLocation = geoKeysShort[4+keyID*4+1];
                            uint16_t valueCount = geoKeysShort[4+keyID*4+2];
                            uint16_t valueOffset = geoKeysShort[4+keyID*4+3];

                            // Add uint16_t key values to map
                            if (valueLocation == 0) {
                                
                                // Value is a single uint16_t
                                geoKeyShortValues[valueID].push_back(valueOffset);

                            } else if (valueLocation == 34735) {
                                
                                // Copy short data
                                geoKeyShortValues[valueID] = 
                                    std::vector<uint16_t>(geoKeysShort.begin()+valueOffset, geoKeysShort.begin()+(valueOffset+valueCount));

                            } else if (valueLocation == 34736) {

                                // Check data
                                if (geoKeysDouble.size() == 0) {
                                    throw std::runtime_error("No double values in GeoTIFF directory");
                                }

                                // Copy double data
                                geoKeyDoubleValues[valueID] = 
                                    std::vector<double>(geoKeysDouble.begin()+valueOffset, geoKeysDouble.begin()+(valueOffset+valueCount));

                            } else if (valueLocation == 34737) {
                            
                                // Check data
                                if (geoKeysString.length() == 0) {
                                    throw std::runtime_error("No string values in GeoTIFF directory");
                                }

                                // Copy string data
                                geoKeyStringValues[valueID] = 
                                    std::string(geoKeysString, valueOffset, valueCount);
                            }
                        }

                    } else {
                        if (verbose <= Geostack::Verbosity::Warning) {
                            std::cout << "WARNING: Invalid GeoTIFF file header '" << keyDirectoryVersion << "'" << std::endl;
                        }
                    }
                }
            }
              
            // Create raster
            r.init(nx, ny, samplesPerPixel, hx, hy, 1.0, ox, oy, 0.0);

            // Add projection information
            auto itProjType = geoKeyShortValues.find(1024);
            if (itProjType != geoKeyShortValues.end()) {
                
                // Create projection
                auto proj = ProjectionParameters<double>();
                
                // Set type
                proj.type = (uint8_t)itProjType->second[0];

                // Get CRS code
                if (proj.type == 1) {
                    auto iCode = geoKeyShortValues.find(3072);
                    if (iCode != geoKeyShortValues.end()) {
                        r.setProperty("crs", (int)iCode->second[0]);
                    }
                } else if (proj.type == 2) {
                    auto iCode = geoKeyShortValues.find(2048);
                    if (iCode != geoKeyShortValues.end()) {
                        r.setProperty("crs", (int)iCode->second[0]);
                    }
                }

                // Set coordinate transformation type
                auto iCoordTrans = geoKeyShortValues.find(3075);
                if (iCoordTrans != geoKeyShortValues.end())
                    proj.cttype = (uint8_t)iCoordTrans->second[0];

                // Set equatorial radius
                auto iSemiMajorAxis = geoKeyDoubleValues.find(2057);
                if (iSemiMajorAxis != geoKeyDoubleValues.end())
                    proj.a = iSemiMajorAxis->second[0];

                // Set inverse flattening
                auto iInvFlattening = geoKeyDoubleValues.find(2059);
                if (iInvFlattening != geoKeyDoubleValues.end())
                    proj.f = 1.0/iInvFlattening->second[0];
                    
                // Set central meridian longitude
                auto iFalseOriginLong = geoKeyDoubleValues.find(3084);
                if (iFalseOriginLong != geoKeyDoubleValues.end()) {
                    proj.x0 = iFalseOriginLong->second[0];
                } else {
                    auto iNatOriginLong = geoKeyDoubleValues.find(3080);
                    if (iNatOriginLong != geoKeyDoubleValues.end())
                        proj.x0 = iNatOriginLong->second[0];
                }

                // Set central meridian longitude
                auto iFalseOriginLat = geoKeyDoubleValues.find(3085);
                if (iFalseOriginLat != geoKeyDoubleValues.end()) {
                    proj.phi_0 = iFalseOriginLat->second[0];
                } else {
                    auto iNatOriginLat = geoKeyDoubleValues.find(3081);
                    if (iNatOriginLat != geoKeyDoubleValues.end())
                        proj.phi_0 = iNatOriginLat->second[0];
                }

                // Set central meridian longitude
                auto iStdParallel1 = geoKeyDoubleValues.find(3078);
                if (iStdParallel1 != geoKeyDoubleValues.end())
                    proj.phi_1 = iStdParallel1->second[0];

                // Set central meridian longitude
                auto iStdParallel2 = geoKeyDoubleValues.find(3079);
                if (iStdParallel2 != geoKeyDoubleValues.end())
                    proj.phi_2 = iStdParallel2->second[0];

                // Set false easting
                auto iFalseOriginEasting = geoKeyDoubleValues.find(3086);
                if (iFalseOriginEasting != geoKeyDoubleValues.end())
                    proj.fe = iFalseOriginEasting->second[0];

                // Set false northing
                auto iFalseOriginNorthing = geoKeyDoubleValues.find(3087);
                if (iFalseOriginNorthing != geoKeyDoubleValues.end())
                    proj.fn = iFalseOriginNorthing->second[0];

                // Set scale factor
                auto iScaleFactor = geoKeyDoubleValues.find(3092);
                if (iScaleFactor != geoKeyDoubleValues.end())
                    proj.k0 = iScaleFactor->second[0];

                // Set raster projection
                r.setProjectionParameters(proj);
            }
        
            // Block metrics
            isStrips = false;
            isTiled = false;
            bnx = 0; // Size of block in x
            bny = 0; // Size of block in y
            bcols = 0; // Number of columns in data
            if (
                valuesIn.find("StripOffsets") != valuesIn.end() && 
                valuesIn.find("StripByteCounts") != valuesIn.end() && 
                valuesIn.find("RowsPerStrip") != valuesIn.end()) {

                // Data is composed of strips
                isStrips = true;
                bnx = nx;
                bny = valuesIn["RowsPerStrip"][0];
                bcols = 1;
                brows = (ny+bny-1)/bny;

            } else if (
                valuesIn.find("TileOffsets") != valuesIn.end() && 
                valuesIn.find("TileByteCounts") != valuesIn.end() && 
                valuesIn.find("TileWidth") != valuesIn.end() && 
                valuesIn.find("TileLength") != valuesIn.end()) {

                // Data is composed of tiles
                isTiled = true;
                bnx = valuesIn["TileWidth"][0];
                bny = valuesIn["TileLength"][0];
                bcols = (nx+bnx-1)/bnx;
                brows = (ny+bny-1)/bny;

            } else {

                // Unsupported data format
                throw std::runtime_error("Cannot find strip or tile information in TIFF");
            }

            // Create cache
            bCache.clear();
            bCache.resize(brows*bcols);
            bCacheCount.assign(brows*bcols, 0);
        
            if (bnx == TileMetrics::tileSize && bny == TileMetrics::tileSize) {

                // Register data callback
                using namespace std::placeholders;
                RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
                    std::bind(&GeoTIFFHandler<RTYPE, CTYPE>::readDataTileFunction, this, _1, _2, _3));

            } else if (isStrips) {

                // Register data callback
                using namespace std::placeholders;
                RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
                    std::bind(&GeoTIFFHandler<RTYPE, CTYPE>::readDataStripFunction, this, _1, _2, _3));

            } else {
        
                if (verbose <= Geostack::Verbosity::Warning) {
                    std::cout << "WARNING: Using non-optimal geotiff format" 
                              << " [" << Strings::extractFilename(fileName) << "]" << std::endl;
                }

                // Read buffer
                std::vector<RTYPE> rBuffer;

                // Loop over blocks
                for (uint32_t bj = 0; bj < brows; bj++) {
                    for (uint32_t bi = 0; bi < bcols; bi++) {
                
                        // Read data
                        readData(bi, bj, rBuffer);

                        // Slow copy
                        for (uint32_t j = 0; j < bny; j++) {
                            uint32_t rj = bj*bny+j;
                                if (rj >= ny) break;
                            for (uint32_t i = 0; i < bnx; i++) {
                                uint32_t ri = bi*bnx+i;
                                    if (ri >= nx) break;
                                for (uint32_t k = 0; k < samplesPerPixel; k++) {
                                    if (flip_y) {
                                        r.setCellValue(rBuffer[(std::size_t)(i+j*bnx)*samplesPerPixel+k], ri, ny-1-rj, k);
                                    } else {
                                        r.setCellValue(rBuffer[(std::size_t)(i+j*bnx)*samplesPerPixel+k], ri, rj, k);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            // Error for invalid file stream
            throw std::runtime_error("Invalid TIFF file stream");
        }
    }

    /**
    * Read GeoTIFFHandler data block.
    * @param bi block x index.
    * @param bj block y index.
    * @param v vector to read to.
    * @param r %Raster to read to.
    */
    template <typename RTYPE, typename CTYPE>
    void GeoTIFFHandler<RTYPE, CTYPE>::readData(uint32_t bi, uint32_t bj, std::vector<RTYPE> &v) {
    
        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr && 
            *RasterFileHandler<RTYPE, CTYPE>::fileStream) {
        
            // Get file stream handle
            std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

            // Check indexes
            if (bi > bcols || bj > brows)
                return;

            // Get variables
            uint32_t bytesPerSample = valuesIn["BitsPerSample"][0]>>3;
            uint32_t compression = valuesIn["Compression"][0];
            auto &dataOffsets = isStrips ? valuesIn["StripOffsets"] : valuesIn["TileOffsets"];
            auto &dataBytes = isStrips ? valuesIn["StripByteCounts"] : valuesIn["TileByteCounts"];

            // Get data offset and location
            uint64_t offset = dataOffsets[bi+bj*bcols];
            uint32_t bytes = dataBytes[bi+bj*bcols];            

            // Each cell can contain more than one sample for multi-band files
            uint32_t cbnx = bnx*samplesPerPixel;

            // The last strip can have a smaller number of rows than RowsPerStrip
            uint32_t cbny = bny;
            if (isStrips && bj == brows-1) {
                cbny = ny-bny*bj; 
            }

            // Jump to data location
            fileStream.seekg(offset);

            // Read data
            std::vector<uint8_t> v8;
            if (compression == GeoTIFFCompressionTypes::Uncompressed) {

                // Read raw data
                readGeoTIFFData<std::vector<uint8_t> >(fileStream, bytes, v8);

            } else {

                // Read data into input buffer
                std::vector<uint8_t> in;
                readGeoTIFFData<std::vector<uint8_t> >(fileStream, bytes, in);

                // Handle different compression types
                switch(compression) {

                    case GeoTIFFCompressionTypes::LZW: {
                    
                        // Check data
                        if (in.size() < 2) {
                            throw std::runtime_error("No codes found in TIFF LZW data");
                        }

                        // Create string map
                        int codeIndex = 258;
                        std::map<int, std::vector<uint8_t> > codeTable;

                        // Read first two bytes into buffer
                        int bit = 16;
                        std::size_t byte = 2;
                        uint32_t buffer =  ((uint32_t)in[0] << 8) | (uint32_t)in[1];

                        // Code variables
                        uint16_t code;
                        uint8_t codeLength = 9;
                        uint32_t codeMask = ((uint32_t)1 << codeLength)-1;
                        uint16_t lastCode = 0;

                        // Parse data
                        v8.clear();
                        while(byte < in.size()) {

                            // Get code
                            code = (buffer >> (bit-codeLength)) & codeMask;

                            // Update bit position
                            bit -= codeLength;
                        
                            // Parse codes
                            if (code == (uint16_t)257) {

                                // EOI code
                                break;

                            } else if (code == (uint16_t)256) {

                                // Reset table
                                codeTable.clear();
                                for (uint16_t i = 0; i < 256; i++)
                                    codeTable[i].push_back((uint8_t)i);

                                // Reset codes
                                codeIndex = 258;
                                codeLength = 9;
                                codeMask = ((uint32_t)1 << codeLength)-1;

                            } else {

                                // Lookup code
                                if (lastCode == (uint16_t)256) {

                                    // Append to output
                                    auto &codeValue = codeTable[code];
                                    v8.insert(v8.end(), codeValue.begin(), codeValue.end());

                                } else if (codeTable.find(code) != codeTable.end()) {

                                    // Append to output and update table
                                    std::vector<uint8_t> &codeValue = codeTable[code];
                                    auto &newCode = codeTable[codeIndex++];
                                    newCode = codeTable[lastCode];
                                    newCode.push_back(codeValue[0]);
                                    v8.insert(v8.end(), codeValue.begin(), codeValue.end());

                                } else {

                                    // Append to output, create new code and update table
                                    std::vector<uint8_t> &codeValue = codeTable[lastCode];
                                    auto &newCode = codeTable[codeIndex++];
                                    newCode = codeValue;
                                    newCode.push_back(codeValue[0]);
                                    v8.insert(v8.end(), newCode.begin(), newCode.end());
                                }                            
                            }

                            // Update last code
                            lastCode = code;

                            // Check for increase in code length
                            if ((codeIndex ==  511 && codeLength ==  9) ||
                                (codeIndex == 1023 && codeLength == 10) ||
                                (codeIndex == 2047 && codeLength == 11)) {

                                // Update code length and mask
                                codeLength++;
                                codeMask = ((uint32_t)1 << codeLength)-1;
                            }

                            // Update buffer
                            while (bit-codeLength < 0) {
                                buffer = (buffer << 8) | (uint32_t)in[byte++];
                                bit += 8;
                            }
                        }

                        // Get code
                        code = (buffer >> (bit-codeLength)) & codeMask;
                        if (code != (uint16_t)257) {
                            throw std::runtime_error("No EOI code found in TIFF LZW data");
                        }

                    } break;

                    case GeoTIFFCompressionTypes::Deflate: {
                                    
                        // Uncompress
                        v8.resize((size_t)cbnx*cbny*bytesPerSample);
                        mz_ulong len = (mz_ulong)v8.size();
                        int status = uncompress(reinterpret_cast<uint8_t *>(v8.data()), &len, 
                            reinterpret_cast<const uint8_t *>(in.data()), bytes);

                        if (status != Z_OK) {
                            std::stringstream err;
                            err << "Uncompression failed '" << mz_error(status) << "'";
                            throw std::runtime_error(err.str());
                        } 

                        // Check size
                        if (len != (mz_ulong)v8.size()) {
                            throw std::runtime_error("Unexpected data size during uncompression");
                        }

                    } break;

                    default: {
                        std::stringstream err;
                        err << "Unhandled compression type: " << compression;
                        throw std::runtime_error(err.str());
                    } break;
                }
                
                // Invert predictor
                if (valuesIn["Predictor"].size() > 0) {
                    if (valuesIn["Predictor"][0] == GeoTIFFPredictorTypes::HorizontalDifferencing) {

                        if (bytesPerSample == 8) {

                            // 64 bit data
                            int64_t *pPredictorData = reinterpret_cast<int64_t *>(v8.data());
                            for (uint32_t j = 0; j < cbny; j++)
                                for (uint32_t i = samplesPerPixel; i < cbnx; i++) {
                                        pPredictorData[i+cbnx*j] += pPredictorData[i-samplesPerPixel+cbnx*j];
                                }
                        } else if (bytesPerSample == 4) {
                    
                            // 32 bit data
                            int32_t *pPredictorData = reinterpret_cast<int32_t *>(v8.data());
                            for (uint32_t j = 0; j < cbny; j++)
                                for (uint32_t i = samplesPerPixel; i < cbnx; i++) {
                                    pPredictorData[i+cbnx*j] += pPredictorData[i-samplesPerPixel+cbnx*j];
                                }
                        } else if (bytesPerSample == 2) {
                    
                            // 16 bit data
                            int16_t *pPredictorData = reinterpret_cast<int16_t *>(v8.data());
                            for (uint32_t j = 0; j < cbny; j++)
                                for (uint32_t i = samplesPerPixel; i < cbnx; i++) {
                                        pPredictorData[i+cbnx*j] += pPredictorData[i-samplesPerPixel+cbnx*j];
                                }
                        } else if (bytesPerSample == 1) {
                    
                            // 8 bit data
                            for (uint32_t j = 0; j < cbny; j++)
                                for (uint32_t i = samplesPerPixel; i < cbnx; i++) {
                                    v8[i+cbnx*j] += v8[i-samplesPerPixel+cbnx*j];
                                }
                        }
                    } else if (valuesIn["Predictor"][0] == GeoTIFFPredictorTypes::FloatingPoint) {
                    
                        // Set row byte width
                        uint32_t rowByteWidth = bytesPerSample*cbnx;

                        // Apply byte-level horizontal differencing
                        for (uint32_t j = 0; j < cbny; j++)
                            for (uint32_t i = samplesPerPixel; i < rowByteWidth; i++)
                                v8[i+rowByteWidth*j] += v8[i-samplesPerPixel+rowByteWidth*j];

                        // Apply floating point predictor byte shuffle
                        // See Adobe Photoshop TIFF Technical Note 3, 2005
                        std::vector<uint8_t> predictorData(rowByteWidth);

                        for (uint32_t j = 0; j < cbny; j++) {
                            for (uint32_t i = 0; i < cbnx; i++) {

                                // Unshuffle bytes from packed rows
                                for (uint32_t b = 0; b < bytesPerSample; b++) {
                                    #if defined(ENDIAN_BIG)
                                    predictorData[b+bytesPerSample*i]] = v8[b*cbnx+i+j*rowByteWidth]; // TODO test
                                    #else
                                    predictorData[b+bytesPerSample*i] = v8[(bytesPerSample-b-1)*cbnx+i+j*rowByteWidth];
                                    #endif
                                }
                            }
                            std::memcpy(v8.data()+rowByteWidth*j, predictorData.data(), rowByteWidth);
                        }
                    }
                }
            }

            // Convert data
            switch(bytesPerSample) {
            
                case 1:
                    switch(sampleFormat) {

                        case GeoTIFFSampleTypes::UnsignedInt:
                            convertGeoTIFFVector<uint8_t, uint8_t, RTYPE>(v8, v, nullValueString);
                            break;

                        case GeoTIFFSampleTypes::SignedInt:
                            convertGeoTIFFVector<uint8_t, int8_t, RTYPE>(v8, v, nullValueString);
                            break;

                        case GeoTIFFSampleTypes::Float:
                            throw std::runtime_error("Invalid TIFF data format of one byte float");
                    }
                    break;

                case 2:
                    switch(sampleFormat) {

                        case GeoTIFFSampleTypes::UnsignedInt:
                            convertGeoTIFFVector<uint8_t, uint16_t, RTYPE>(v8, v, nullValueString);
                            break;

                        case GeoTIFFSampleTypes::SignedInt:
                            convertGeoTIFFVector<uint8_t, int16_t, RTYPE>(v8, v, nullValueString);
                            break;
                                    
                        case GeoTIFFSampleTypes::Float:
                            throw std::runtime_error("Invalid TIFF data format of two byte float");
                    }
                    break;

                case 4:
                    switch(sampleFormat) {
                            
                        case GeoTIFFSampleTypes::UnsignedInt:
                            convertGeoTIFFVector<uint8_t, uint32_t, RTYPE>(v8, v, nullValueString);
                            break;

                        case GeoTIFFSampleTypes::SignedInt:
                            convertGeoTIFFVector<uint8_t, int32_t, RTYPE>(v8, v, nullValueString);
                            break;
                                    
                        case GeoTIFFSampleTypes::Float:
                            convertGeoTIFFVector<uint8_t, float, RTYPE>(v8, v, nullValueString);
                            break;
                    }
                    break;

                case 8:
                    switch(sampleFormat) {
                            
                        case GeoTIFFSampleTypes::UnsignedInt:
                            convertGeoTIFFVector<uint8_t, uint64_t, RTYPE>(v8, v, nullValueString);
                            break;

                        case GeoTIFFSampleTypes::SignedInt:
                            convertGeoTIFFVector<uint8_t, int64_t, RTYPE>(v8, v, nullValueString);
                            break;
                                    
                        case GeoTIFFSampleTypes::Float:
                            convertGeoTIFFVector<uint8_t, double, RTYPE>(v8, v, nullValueString);
                            break;
                    }
                    break;

                default: {
                    std::stringstream err;
                    err << "Unhandled TIFF data byte size: " << bytesPerSample;
                    throw std::runtime_error(err.str());
                }
            }
        } else {        
            // Error for invalid file stream
            throw std::runtime_error("Invalid TIFF file stream");
        }
    }

    /**
    * Write %Raster to GeoTIFFHandler format
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void GeoTIFFHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            throw std::runtime_error("Raster has no data to write TIFF");
        }

        // Get tile data
        auto dim = r.getRasterDimensions();
        uint32_t nTiles = dim.tx*dim.ty;

        // Get sample format
        sampleFormat = GeoTIFFHandler<RTYPE, CTYPE>::getSampleType();

        // Create no data values
        RTYPE noDataValue = GeoTIFFHandler<RTYPE, CTYPE>::getNullValue();

        // Set config defaults
        bool isBigTIFF = false;
        int compressionLevel = MZ_DEFAULT_LEVEL;
        GeoTIFFCompressionTypes::Type compression = GeoTIFFCompressionTypes::Deflate;
        GeoTIFFPredictorTypes::Type predictor = sampleFormat == GeoTIFFSampleTypes::Float ? 
            GeoTIFFPredictorTypes::FloatingPoint : GeoTIFFPredictorTypes::HorizontalDifferencing;
        std::string description;
        
        // Parse config
        bool split = false;
        std::vector<uint32_t> requestedLayers;
        uint32_t layersStart = Geostack::getNullValue<uint32_t>();
        uint32_t layersEnd = Geostack::getNullValue<uint32_t>();
        uint32_t layersStep = 1;
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }
        
            // Set TIFF type
            if (config["bigTIFF"].is_bool()) {
                isBigTIFF = config["bigTIFF"].bool_value();
            }

            // Set compression type
            if (config["compression"].is_string()) {

                auto compressionStr = config["compression"].string_value();
                if (compressionStr == "uncompressed") {
                    compression = GeoTIFFCompressionTypes::Uncompressed;
                } else if (compressionStr == "deflate") {
                    compression = GeoTIFFCompressionTypes::Deflate;
                } else {
                    std::stringstream err;
                    err << "Unhandled compression type: " << compressionStr;
                    throw std::runtime_error(err.str());
                }
            }
              
            // Set compression level
            if (config["compressionLevel"].is_number()) {

                compressionLevel = config["compressionLevel"].number_value();
                if (compressionLevel < 0 || compressionLevel > 10) {
                    throw std::runtime_error("Compression level must be from 0-10");
                }
            }
        
            // Set predictor type
            if (config["predictor"].is_string()) {
        
                auto predictorStr = config["predictor"].string_value();
                if (predictorStr == "none") {
                    predictor = GeoTIFFPredictorTypes::None;
                } else if (predictorStr == "horizontalDifferencing") {
                    predictor = GeoTIFFPredictorTypes::HorizontalDifferencing; 
                } else if (predictorStr == "floatingPoint") {
                    predictor = GeoTIFFPredictorTypes::FloatingPoint; 
                } else {
                    std::stringstream err;
                    err << "Unhandled predictor type: " << predictorStr;
                    throw std::runtime_error(err.str());
                }
            }
        
            // Set description string
            if (config["description"].is_string()) {
                description = config["description"].string_value();
            }
            description.push_back('\0');

            // Set noDataValue string
            if (config["noDataValue"].is_number()) {
                noDataValue = config["noDataValue"].number_value();
            } else if (config["noDataValue"].is_string()) {
                noDataValue = Strings::toNumber<RTYPE>(config["noDataValue"].string_value());
            }
              
            // Set layers
            if (config["layers"].is_number()) {

                // Set single value
                requestedLayers.push_back((uint32_t)config["layers"].number_value());

            } else if (config["layers"].is_array()) {

                // Create items
                for (auto &layer : config["layers"].array_items()) {
                    if (layer.is_number()) {
                        requestedLayers.push_back((uint32_t)layer.number_value());
                    }
                }

            } else if (config["layers"].is_string()) {

                // Check for slice notation
                std::string sliceString = config["layers"].string_value();
                if (sliceString.front() == '[' && sliceString.back() == ']') {

                    // Split into parts
                    auto parts = Strings::split(sliceString.substr(1, sliceString.size()-2), ':');
                    if (parts.size() > 0) {
                        layersStart = 0;
                        if (parts[0].size() > 0) {
                            layersStart = Strings::toNumber<uint32_t>(parts[0]);
                        }
                        if (parts.size() > 1) {
                            if (parts[1].size() > 0) {
                                layersEnd = Strings::toNumber<uint32_t>(parts[1]);
                            }
                            if (parts.size() > 2) {
                                layersStep = Strings::toNumber<uint32_t>(parts[2]);
                            }
                        }
                    }
                }
            }

            // Set layer splitting
            if (config["split"].is_bool() && config["split"].bool_value()) {
                split = true;
            }
        }

        // Update layers
        if (requestedLayers.size() == 0 && isValid<uint32_t>(layersStart)) {
            if (isInvalid<uint32_t>(layersEnd)) {
                layersEnd = dim.d.nz;
            }
            for (std::size_t i = layersStart; i < layersEnd; i += layersStep) {
                requestedLayers.push_back(i);
            }
        }
            
        if (requestedLayers.size() > 0) {

            // Sort layers in order
            std::sort(requestedLayers.begin(), requestedLayers.end());

            // Check layers
            for (auto &layer : requestedLayers) {
                if (layer > dim.d.nz-1) {
                    std::stringstream err;
                    err << "Requested layer " << layer << " is greater than maximum (" << dim.d.nz-1 << ")";
                    throw std::runtime_error(err.str());
                }
            }
        } else {
            for (std::size_t i = 0; i < dim.d.nz; i++) {
                requestedLayers.push_back(i);
            }
        }

        // Create layer groups
        std::vector<std::vector<uint32_t> > layers;
        if (split) {
            for (auto &layer : requestedLayers) {
                layers.push_back( { layer } );
            }
        } else {
            layers.push_back(requestedLayers);
        }

        // Populate values
        nx = dim.d.nx;
        ny = dim.d.ny;
        bnx = TileMetrics::tileSize;
        bny = TileMetrics::tileSize;
        bcols = dim.tx;
        brows = dim.ty;
        isTiled = true;
        isStrips = false;
        samplesPerPixel = layers[0].size();

        // Open file
        std::vector<std::fstream> fileStreams;
        if (layers.size() > 1) {
            auto fileNameParts = Strings::splitPath(fileName);
            auto indexLength = (int)log10((double)layers.size())+1;
            for (std::size_t n = 0; n < layers.size(); n++) {

                // Construct filename
                std::string indexString = std::to_string(n);
                std::string indexedFileName = fileNameParts[0] + fileNameParts[1] +
                    "_" + std::string(indexLength-indexString.length(), '0') 
                    + indexString + "." + fileNameParts[2];

                // Create stream
                std::fstream fileStream(indexedFileName, std::fstream::out | std::fstream::binary | std::fstream::trunc);
                if (!fileStream.is_open()) {
                    std::stringstream err;
                    err << "Cannot open '" << indexedFileName << "' for writing";
                    throw std::runtime_error(err.str());
                }

                // Add to filestream list
                fileStreams.push_back(std::move(fileStream));
            }
        } else {

            // Open single file
            std::fstream fileStream(fileName, std::fstream::out | std::fstream::binary | std::fstream::trunc);
            if (!fileStream.is_open()) {
                std::stringstream err;
                err << "Cannot open '" << fileName << "' for writing";
                throw std::runtime_error(err.str());
            }
            fileStreams.push_back(std::move(fileStream));
        }

        // Write byte order
        for (std::size_t n = 0; n < layers.size(); n++) {
            #if defined(ENDIAN_BIG)
            fileStreams[n] << "MM";
            #elif defined(ENDIAN_LITTLE)
            fileStreams[n] << "II";
            #else
            #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
            #endif

            // Write version
            if (isBigTIFF) {
                uint16_t version = 43;
                fileStreams[n].write(reinterpret_cast<const char *>(&version), sizeof(version));
                uint16_t byteSize = 8;
                fileStreams[n].write(reinterpret_cast<const char *>(&byteSize), sizeof(byteSize));
                uint16_t reserved = 0;
                fileStreams[n].write(reinterpret_cast<const char *>(&reserved), sizeof(reserved));
            } else {
                uint16_t version = 42;
                fileStreams[n].write(reinterpret_cast<const char *>(&version), sizeof(version));
            }
        }

        // Create no data values for all sizes except byte
        std::string gdalNoData;
        if (sizeof(RTYPE) > 1) {
            gdalNoData = std::to_string(noDataValue);
            gdalNoData.push_back('\0');
        }        
        
        // Create transform tag
        std::vector<double> transform = {
            dim.d.hx,        0, 0, dim.d.ox,
                   0, dim.d.hy, 0, dim.d.oy,
                   0,        0, 0,        0,
                   0,        0, 0,        1
        };

        // Build directories
        std::map<std::string, uint16_t> valueTags = {
		    { "ImageWidth", 256 },
		    { "ImageLength", 257 },
		    { "BitsPerSample", 258 },
		    { "Compression", 259 },
		    { "PhotometricInterpretation", 262 },
		    { "Description", 270 },
		    { "SamplesPerPixel", 277 },
		    { "PlanarConfiguration", 284 },
		    { "Predictor", 317 },
		    { "TileWidth", 322 },
		    { "TileLength", 323 },
		    { "TileOffsets", 324 },
		    { "TileByteCounts", 325 },
		    { "SampleFormat", 339 },
            { "Transform", 34264 },
            { "GDALNoData", 42113 } };

        // Populate directories
        std::map<uint16_t, GeoTIFFDirectory> valuesOut;
        valuesOut[valueTags["ImageWidth"]]                = GeoTIFFDirectory(GeoTIFFDataTypes::Long, 1, dim.d.nx);
        valuesOut[valueTags["ImageLength"]]               = GeoTIFFDirectory(GeoTIFFDataTypes::Long, 1, dim.d.ny);
        valuesOut[valueTags["BitsPerSample"]]             = GeoTIFFDirectory(GeoTIFFDataTypes::Short, 1, sizeof(RTYPE)<<3);
        valuesOut[valueTags["Compression"]]               = GeoTIFFDirectory(GeoTIFFDataTypes::Short, 1, compression);
        valuesOut[valueTags["PhotometricInterpretation"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Short, 1, 1); // Black is zero
        valuesOut[valueTags["SamplesPerPixel"]]           = GeoTIFFDirectory(GeoTIFFDataTypes::Short, 1, (uint16_t)samplesPerPixel);
        valuesOut[valueTags["PlanarConfiguration"]]       = GeoTIFFDirectory(GeoTIFFDataTypes::Short, 1, 1); // Values stored continuously
        valuesOut[valueTags["Predictor"]]                 = GeoTIFFDirectory(GeoTIFFDataTypes::Short, 1, predictor);
        valuesOut[valueTags["TileWidth"]]                 = GeoTIFFDirectory(GeoTIFFDataTypes::Long, 1, bnx);
        valuesOut[valueTags["TileLength"]]                = GeoTIFFDirectory(GeoTIFFDataTypes::Long, 1, bny);
        valuesOut[valueTags["TileOffsets"]]               = GeoTIFFDirectory((isBigTIFF ? GeoTIFFDataTypes::Long8 : GeoTIFFDataTypes::Long), nTiles, 0);
        valuesOut[valueTags["TileByteCounts"]]            = GeoTIFFDirectory((isBigTIFF ? GeoTIFFDataTypes::Long8 : GeoTIFFDataTypes::Long), nTiles, 0);
        valuesOut[valueTags["SampleFormat"]]              = GeoTIFFDirectory(GeoTIFFDataTypes::Short, 1, sampleFormat);
        valuesOut[valueTags["Transform"]]                 = GeoTIFFDirectory(GeoTIFFDataTypes::Double, 16, 0); // Model transform
        
        // Description string
        if (description.size() > 0) {
            if (description.size() > (isBigTIFF ? 8 : 4)) {
                valuesOut[valueTags["Description"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Ascii, (uint32_t)description.length(), 0);
            } else {
                uint64_t v = 0;
                for (uint32_t i = 0; i < description.length(); i++) {
                    v |= (uint64_t)description[i]<<(i<<3);
                }
                valuesOut[valueTags["Description"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Ascii, (uint32_t)description.length(), v);
            }
        }
        
        // GDAL nodata string
        if (gdalNoData.size() > 0) {
            if (gdalNoData.size() > (isBigTIFF ? 8 : 4)) {
                valuesOut[valueTags["GDALNoData"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Ascii, (uint32_t)gdalNoData.length(), 0); 
            } else {
                uint64_t v = 0;
                for (uint32_t i = 0; i < gdalNoData.length(); i++) {
                    v |= (uint64_t)gdalNoData[i]<<(i<<3);
                }
                valuesOut[valueTags["GDALNoData"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Ascii, (uint32_t)gdalNoData.length(), v);
            }
        }

        // Write projection
        auto proj = r.getProjectionParameters();
        std::vector<uint16_t> geoKeyShortValues;
        std::vector<double> geoKeyDoubleValues;
        std::string geoKeyStringValues;
        if (proj.type != 0) {

            valueTags["GeoKeyShortDirectory"] = 34735;
            valueTags["GeoKeyDoubleDirectory"] = 34736;
            valueTags["GeoKeyStringDirectory"] = 34737;
            
            // Build string values
            std::string GTCitationGeoKey = "unknown|";
            std::string GeogCitationGeoKey = "GCS Name = unknown|Datum = unknown|Primem = unknown|";
            geoKeyStringValues = GTCitationGeoKey + GeogCitationGeoKey;
            geoKeyStringValues.push_back('\0');
            
            valuesOut[valueTags["GeoKeyStringDirectory"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Ascii, (uint32_t)geoKeyStringValues.length(), 0);
            
            // Build double values
            geoKeyDoubleValues =
                {proj.a,
                 (1.0/proj.f),
                 proj.x0,
                 proj.k0,
                 proj.fe,
                 proj.fn,
                 proj.phi_0, 
                 proj.phi_1,
                 proj.phi_2,
                 0.0};
            
            valuesOut[valueTags["GeoKeyDoubleDirectory"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Double, (uint32_t)geoKeyDoubleValues.size(), 0);

            // Get ellipsoid
            uint16_t etype = 32767; // Default to user-defined
            auto eName = ProjectionTable::instance().getEllipsoidName(proj.a, proj.f);
            if (!eName.empty()) {
                etype = (uint16_t)ProjectionTable::instance().getEllipsoidCode(eName);
                if (etype == 0)
                    etype = 32767;
            }

            // Build short values
            uint16_t type = (uint16_t)proj.type;
            uint16_t cttype = (uint16_t)proj.cttype;
            uint16_t c1s = GTCitationGeoKey.size();
            uint16_t c2s = GeogCitationGeoKey.size();

            // Get CRS code
            uint16_t crs = 32767; // Default to '32767: user-defined'
            if (r.hasProperty("crs")) {
                crs = r.template getProperty<int>("crs");
            }
            if (proj.type == 1) {

                // Projected parameters
                geoKeyShortValues = {
                        1,      1,    0,      0,   // Header: KeyDirectoryVersion, KeyRevision, MinorRevision, NumberOfKeys
                     1024,      0,    1,   type,   // GTModelTypeGeoKey
                     1025,      0,    1,      1,   // GTRasterTypeGeoKey, set to '1: RasterPixelIsArea'
                     1026,  34737,  c1s,      0,   // GTCitationGeoKey             
                     2054,      0,    1,   9102,   // GeogAngularUnitsGeoKey, set to '9102: angular degree'
                     2057,  34736,    1,      0,   // GeogSemiMajorAxisGeoKey
                     2056,      0,    1,  etype,   // GeogEllipsoidGeoKey
                     2059,  34736,    1,      1,   // GeogInvFlatteningGeoKey
                     2061,  34736,    1,      9,   // GeogPrimeMeridianLongGeoKey, set to '0'
                     3072,      0,    1,    crs,   // ProjectedCSTypeGeoKey
                     3073,  34737,  c2s,    c1s,   // PCSCitationGeoKey
                     3075,      0,    1, cttype,   // ProjCoordTransGeoKey
                     3076,      0,    1,   9001,   // ProjLinearUnitsGeoKey, set to '9001: meter'
                     3078,  34736,    1,      7,   // ProjStdParallel1GeoKey
                     3079,  34736,    1,      8,   // ProjStdParallel2GeoKey
                     3080,  34736,    1,      2,   // ProjFalseOriginLongGeoKey
                     3081,  34736,    1,      6,   // ProjNatOriginLatGeoKey
                     3084,  34736,    1,      2,   // ProjFalseOriginLongGeoKey
                     3085,  34736,    1,      6,   // ProjFalseOriginLatGeoKey
                     3086,  34736,    1,      4,   // ProjFalseOriginEastingGeoKey
                     3087,  34736,    1,      5,   // ProjFalseOriginNorthingGeoKey
                     3092,  34736,    1,      3    // ProjScaleAtNatOriginGeoKey     
                     };
                geoKeyShortValues[3] = (uint16_t)(geoKeyShortValues.size()>>2)-1;

            } else if (proj.type == 2) {
            
                // Geographic parameters
                geoKeyShortValues = {
                        1,      1,    0,     0,   // Header: KeyDirectoryVersion, KeyRevision, MinorRevision, NumberOfKeys
                     1024,      0,    1,   type,   // GTModelTypeGeoKey
                     1025,      0,    1,      1,   // GTRasterTypeGeoKey, set to '1: RasterPixelIsArea'
                     1026,  34737,  c1s,      0,   // GTCitationGeoKey             
                     2048,      0,    1,    crs,   // GeographicTypeGeoKey
                     2049,  34737,  c2s,    c1s,   // GeogCitationGeoKey
                     2054,      0,    1,   9102,   // GeogAngularUnitsGeoKey, set to '9102: angular degree'
                     2057,  34736,    1,      0,   // GeogSemiMajorAxisGeoKey
                     2056,      0,    1,  etype,   // GeogEllipsoidGeoKey
                     2059,  34736,    1,      1,   // GeogInvFlatteningGeoKey
                     2061,  34736,    1,      9,   // GeogPrimeMeridianLongGeoKey, set to '0'
                     };
                geoKeyShortValues[3] = (uint16_t)(geoKeyShortValues.size()>>2)-1;

            }

            valuesOut[valueTags["GeoKeyShortDirectory"]] = GeoTIFFDirectory(GeoTIFFDataTypes::Short, (uint32_t)geoKeyShortValues.size(), 0);
        }

        // Create offset maps
        uint64_t offset = 0;
        std::vector<std::map<std::fstream::pos_type, uint64_t> > offsets(layers.size());
        std::vector<std::map<uint64_t, std::fstream::pos_type> > valueOffsets(layers.size());
        for (std::size_t n = 0; n < layers.size(); n++) {

            // Record current position
            std::fstream::pos_type lastOffset = fileStreams[n].tellp();

            // Write empty offset
            if (isBigTIFF) {
                fileStreams[n].write(reinterpret_cast<const char *>(&offset), sizeof(offset));
            } else {
                uint32_t offset32 = (uint32_t)offset;
                fileStreams[n].write(reinterpret_cast<const char *>(&offset32), sizeof(offset32));
            }

            // Write IFDs. If multiple IFDs are implemented ensure these are sequentially 
            // packed after this IFD for optimised access for e.g. Cloud Optimized GeoTIFFs.
            {
                // Store offset to directory
                offsets[n][lastOffset] = (uint64_t)fileStreams[n].tellp();
            
                // Write number of entries
                if (isBigTIFF) {
                    uint64_t entries = (uint64_t)valuesOut.size();
                    fileStreams[n].write(reinterpret_cast<char *>(&entries), sizeof(entries));
                } else {
                    uint16_t entries = (uint16_t)valuesOut.size();
                    fileStreams[n].write(reinterpret_cast<char *>(&entries), sizeof(entries));
                }

                // Write directory entries
                for (auto &d: valuesOut) {

                    // Store offset to value
                    valueOffsets[n][d.first] = d.second.write(d.first, fileStreams[n], isBigTIFF);
                }

                // Record current position
                lastOffset = fileStreams[n].tellp();

                // Write empty offset
                if (isBigTIFF) {
                    fileStreams[n].write(reinterpret_cast<const char *>(&offset), sizeof(offset));
                } else {
                    uint32_t offset32 = (uint32_t)offset;
                    fileStreams[n].write(reinterpret_cast<const char *>(&offset32), sizeof(offset32));
                }
            }

            // Write data
            if (description.size() > (isBigTIFF ? 8 : 4)) {
                offsets[n][valueOffsets[n][valueTags["Description"]]] = (uint64_t)writeGeoTIFFData<std::string, char>(fileStreams[n], (uint32_t)description.size(), description);
            }
            if (gdalNoData.size() > (isBigTIFF ? 8 : 4)) {
                offsets[n][valueOffsets[n][valueTags["GDALNoData"]]] = (uint64_t)writeGeoTIFFData<std::string, char>(fileStreams[n], (uint32_t)gdalNoData.size(), gdalNoData);
            }
            offsets[n][valueOffsets[n][valueTags["Transform"]]] = (uint64_t)writeGeoTIFFData<std::vector<double>, double>(fileStreams[n], (uint32_t)transform.size(), transform);
        
            // Write projection data
            if (proj.type != 0) {
                offsets[n][valueOffsets[n][valueTags["GeoKeyShortDirectory"]]] = 
                    (uint64_t)writeGeoTIFFData<std::vector<uint16_t>, uint16_t>(fileStreams[n], (uint32_t)geoKeyShortValues.size(), geoKeyShortValues);
                offsets[n][valueOffsets[n][valueTags["GeoKeyDoubleDirectory"]]] = 
                    (uint64_t)writeGeoTIFFData<std::vector<double>, double>(fileStreams[n], (uint32_t)geoKeyDoubleValues.size(), geoKeyDoubleValues);
                offsets[n][valueOffsets[n][valueTags["GeoKeyStringDirectory"]]] = 
                    (uint64_t)writeGeoTIFFData<std::string, char>(fileStreams[n], (uint32_t)geoKeyStringValues.length(), geoKeyStringValues);
            }
        }

        // Create buffers
        std::vector<mz_ulong> writeLen;        
        std::vector<std::vector<RTYPE> > tileInterleavedData;
        std::vector<std::vector<uint8_t> > compressedData;
        std::vector<int> compressStatus;
        std::vector<std::vector<uint64_t> > tileOffsets;
        std::vector<std::vector<uint64_t> > tileByteCounts;
        std::vector<std::vector<uint8_t> > predictorData;
        
        writeLen.resize(layers.size());
        tileInterleavedData.resize(layers.size());
        compressedData.resize(layers.size());
        compressStatus.resize(layers.size());
        tileOffsets.resize(layers.size());
        tileByteCounts.resize(layers.size());
        predictorData.resize(layers.size());        
        
        // Get solver handles
        auto &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Set data sizes
        uint32_t cbnx = bnx*samplesPerPixel;
        uint32_t rowByteWidth = sizeof(RTYPE)*cbnx;
        uint32_t dataLen = (uint32_t)TileMetrics::tileSizeSquared*samplesPerPixel*sizeof(RTYPE);
        mz_ulong compressedDataLen = compressBound(dataLen);
        for (std::size_t n = 0; n < layers.size(); n++) {
            tileInterleavedData[n].resize(TileMetrics::tileSizeSquared*samplesPerPixel);
            compressedData[n].resize(compressedDataLen);
            predictorData[n].resize(rowByteWidth);
            tileOffsets[n].resize(bcols*brows);
            tileByteCounts[n].resize(bcols*brows);
        }
        cl::Buffer tileInterleavedBuffer(context, CL_MEM_WRITE_ONLY, dataLen);

        // Write data
        uint8_t verbose = Geostack::Solver::getSolver().getVerboseLevel();
        for (uint32_t ti = 0; ti < dim.tx; ti++) {
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
         
                // Loop over layers
                for (std::size_t n = 0; n < layers.size(); n++) {

                    // Interleave data
                    try {
                        solver.interleaveBuffer<RTYPE>(r.getTileDataBuffer(ti, tj), tileInterleavedBuffer, noDataValue,
                            TileMetrics::tileSize, TileMetrics::tileSize, samplesPerPixel);
                        queue.enqueueReadBuffer(tileInterleavedBuffer, CL_TRUE, 0, dataLen, tileInterleavedData[n].data());
                    } catch (cl::Error e) {
                        std::stringstream err;
                        err << "OpenCL exception '" << e.what() << "': " << e.err();
                        throw std::runtime_error(err.str());
                    }
                }

                switch(compression) {

                    case GeoTIFFCompressionTypes::Uncompressed: {
                        
                        // Write data
                        for (std::size_t n = 0; n < layers.size(); n++) {
                            char *pTileData = reinterpret_cast<char *>(tileInterleavedData[n].data());
                            tileOffsets[n][ti+tj*bcols] = (uint64_t)fileStreams[n].tellp();
                            tileByteCounts[n][ti+tj*bcols] = dataLen;
                            fileStreams[n].write(pTileData, dataLen);
                        }

                    } break;

                    case GeoTIFFCompressionTypes::Deflate: {

                        long nLayers = (long)layers.size();
                        uint32_t samplesPerPixelLocal = samplesPerPixel;
                        
                        //#pragma omp parallel for shared(nLayers, predictor, tileInterleavedData, samplesPerPixelLocal, predictorData, rowByteWidth)
                        for (long n = 0; n < nLayers; n++) {

                            // Apply predictor
                            if (predictor == GeoTIFFPredictorTypes::HorizontalDifferencing) {

                                if (sizeof(RTYPE) == 8) {

                                    // 64 bit data
                                    int64_t *pPredictorData = reinterpret_cast<int64_t *>(tileInterleavedData[n].data());
                                    for (uint32_t j = 0; j < bny; j++) {
                                        for (uint32_t i = cbnx-1; i >= samplesPerPixelLocal; i--) {
                                            pPredictorData[i+cbnx*j] -= pPredictorData[i-samplesPerPixelLocal+cbnx*j];
                                        }
                                    }
                                } else if (sizeof(RTYPE) == 4) {
                    
                                    // 32 bit data
                                    int32_t *pPredictorData = reinterpret_cast<int32_t *>(tileInterleavedData[n].data());
                                    for (uint32_t j = 0; j < bny; j++) {
                                        for (uint32_t i = cbnx-1; i >= samplesPerPixelLocal; i--) {
                                            pPredictorData[i+cbnx*j] -= pPredictorData[i-samplesPerPixelLocal+cbnx*j];
                                        }
                                    }
                                } else if (sizeof(RTYPE) == 1) {
                    
                                    // 8 bit data
                                    int8_t *pPredictorData = reinterpret_cast<int8_t *>(tileInterleavedData[n].data());
                                    for (uint32_t j = 0; j < bny; j++) {
                                        for (uint32_t i = cbnx-1; i >= samplesPerPixelLocal; i--) {
                                            pPredictorData[i+cbnx*j] -= pPredictorData[i-samplesPerPixelLocal+cbnx*j];
                                        }
                                    }
                                }
                            } else if (predictor == GeoTIFFPredictorTypes::FloatingPoint) {

                                // Apply floating point predictor byte shuffle
                                // See Adobe Photoshop TIFF Technical Note 3, 2005
                                char *pTileData = reinterpret_cast<char *>(tileInterleavedData[n].data());
                                for (uint32_t j = 0; j < bny; j++) {
                                    for (uint32_t i = 0; i < cbnx; i++) {

                                        // Shuffle bytes into packed rows
                                        for (uint32_t b = 0; b < sizeof(RTYPE); b++) {
                                            #if defined(ENDIAN_BIG)
                                            predictorData[n][b*cbnx+i] = pTileData[b+sizeof(RTYPE)*i+j*rowByteWidth+b]; // TODO test
                                            #else
                                            predictorData[n][(sizeof(RTYPE)-b-1)*cbnx+i] = pTileData[b+sizeof(RTYPE)*i+j*rowByteWidth];
                                            #endif
                                        }
                                    }
                                    std::memcpy(pTileData+rowByteWidth*j, predictorData[n].data(), rowByteWidth);
                                }
                        
                                // Apply byte-level horizontal differencing
                                for (uint32_t j = 0; j < bny; j++)
                                    for (uint32_t i = rowByteWidth-1; i >= samplesPerPixelLocal; i--)
                                        pTileData[i+rowByteWidth*j] -= pTileData[i-samplesPerPixelLocal+rowByteWidth*j];
                            }
                        }
            
                        // Reset data lengths
                        std::fill(writeLen.begin(), writeLen.end(), compressedDataLen);
                            
                        // Compress tile
                        if (nLayers > 1) {

                            // Compress in parallel using OpenMP
                            #pragma omp parallel for shared(nLayers, compressStatus, compressedData, writeLen, tileInterleavedData, dataLen, compressionLevel)
                            for (long n = 0; n < nLayers; n++) {
                                compressStatus[n] = compress2(compressedData[n].data(), &writeLen[n], 
                                    reinterpret_cast<uint8_t *>(tileInterleavedData[n].data()), (mz_ulong)dataLen, compressionLevel);
                            }
                        } else {

                            // Compress single layer
                            compressStatus[0] = compress2(compressedData[0].data(), &writeLen[0], 
                                reinterpret_cast<uint8_t *>(tileInterleavedData[0].data()), (mz_ulong)dataLen, compressionLevel);
                        }
                            
                        for (std::size_t n = 0; n < layers.size(); n++) {

                            // Check status
                            if (compressStatus[n] != Z_OK) {
                                std::stringstream err;
                                err << "Compression failed '" << mz_error(compressStatus[n]) << "'";
                                throw std::runtime_error(err.str());
                            }

                            // Store data offset and length
                            tileOffsets[n][ti+tj*bcols] = (uint64_t)fileStreams[n].tellp();
                            tileByteCounts[n][ti+tj*bcols] = writeLen[n];

                            // Write data
                            fileStreams[n].write(reinterpret_cast<const char *>(compressedData[n].data()), writeLen[n]);
                        }

                    } break;

                    default: {
                        std::stringstream err;
                        err << "Unhandled compression type: " << compression;
                        throw std::runtime_error(err.str());

                    } break;
                }
            }
            
            // Output progress
            if (verbose <= Geostack::Verbosity::Info && (ti%10) == 0) {
                std::cout << std::setprecision(2) << 100.0*(double)ti/(double)dim.tx << "%" << std::endl;
            }
        }
        if (verbose <= Geostack::Verbosity::Info) {
            std::cout << "100%" << std::endl;
        }

        // Write tile offsets and byte count
        for (std::size_t n = 0; n < layers.size(); n++) {
            if (tileOffsets[n].size() == 1) {
                offsets[n][valueOffsets[n][valueTags["TileOffsets"]]] = tileOffsets[n][0];
            } else {
                if (isBigTIFF) {
                    offsets[n][valueOffsets[n][valueTags["TileOffsets"]]] = 
                        (uint64_t)writeGeoTIFFData<std::vector<uint64_t>, uint64_t>(fileStreams[n], nTiles, tileOffsets[n]);
                } else {
                    std::vector<uint32_t> tileOffsets32(tileOffsets[n].begin(), tileOffsets[n].end());
                    offsets[n][valueOffsets[n][valueTags["TileOffsets"]]] = 
                        (uint64_t)writeGeoTIFFData<std::vector<uint32_t>, uint32_t>(fileStreams[n], nTiles, tileOffsets32);
                }                
            }

            if (tileByteCounts[n].size() == 1) {
                offsets[n][valueOffsets[n][valueTags["TileByteCounts"]]] = tileByteCounts[n][0];
            } else {
                if (isBigTIFF) {
                    offsets[n][valueOffsets[n][valueTags["TileByteCounts"]]] = 
                        (uint64_t)writeGeoTIFFData<std::vector<uint64_t>, uint64_t>(fileStreams[n], nTiles, tileByteCounts[n]);
                } else {
                    std::vector<uint32_t> tileByteCounts32(tileByteCounts[n].begin(), tileByteCounts[n].end());
                    offsets[n][valueOffsets[n][valueTags["TileByteCounts"]]] = 
                        (uint64_t)writeGeoTIFFData<std::vector<uint32_t>, uint32_t>(fileStreams[n], nTiles, tileByteCounts32);
                } 
            }

            // Populate empty offsets
            for (auto &o : offsets[n]) {
                fileStreams[n].seekp(o.first);
                if (isBigTIFF) {
                    offset = o.second;
                    fileStreams[n].write(reinterpret_cast<const char *>(&offset), sizeof(offset));
                } else {
                    uint32_t offset32 = (uint32_t)o.second;
                    fileStreams[n].write(reinterpret_cast<const char *>(&offset32), sizeof(offset32));
                }                
            }
        }

        // close file streams
        for (auto &fs: fileStreams) {
            if (fs.is_open()) {
                fs.close();
            }
        }
        // clear array of file streams
        fileStreams.clear();
    }
        
    template <typename RTYPE, typename CTYPE>
    auto GeoTIFFHandler<RTYPE, CTYPE>::usesDataTiles() const -> bool
    {
        if (!isTiled && !isStrips) {
            throw std::runtime_error{"Invalid call to usesDataTiles() before a call to read() or write()."};
        }
        return isTiled;
    }

    template <typename RTYPE, typename CTYPE>
    auto GeoTIFFHandler<RTYPE, CTYPE>::usesDataStrips() const -> bool
    {
        if (!isTiled && !isStrips) {
            throw std::runtime_error{"Invalid call to usesDataStrips() before a call to read() or write()."};
        }
        return isStrips;
    }

    template <typename RTYPE, typename CTYPE>    
    auto GeoTIFFHandler<RTYPE, CTYPE>::getDataBlockSize() const -> std::tuple<uint32_t, uint32_t>
    {
        if (!isTiled && !isStrips) {
            throw std::runtime_error{"Invalid call to getDataBlockSize() before a call to read() or write()."};
        }
        return std::make_tuple(bnx, bny);
    }

    /**
    * Supply tile data to %Tile through callback function
    * @param tdim %TileDimensions of tile.
    * @param v vector to read to.
    * @param r %Raster to read to.
    */
    template <typename RTYPE, typename CTYPE>
    void GeoTIFFHandler<RTYPE, CTYPE>::readDataTileFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r) {
    
        if (flip_y) {

            // Calculate offsets and shift
            uint32_t bi = tdim.ti;
            uint32_t bj = brows-1-tdim.tj;
            uint32_t jshift = brows*bny-ny;
            uint32_t jend = bny-jshift;
                
            // Get first tile from GeoTIFFHandler
            std::vector<RTYPE> &bufferFirst = bCache[bi+bj*bcols];
            if (bufferFirst.empty()) {

                if (samplesPerPixel > 1) {

                    // Read data
                    std::vector<RTYPE> tileReadData;
                    readData(bi, bj, tileReadData);

                    // Uninterleave data
                    bufferFirst.resize(TileMetrics::tileSizeSquared*samplesPerPixel);
                    for (uint32_t c = 0; c < TileMetrics::tileSizeSquared; c++) {
                        for (uint32_t k = 0; k < samplesPerPixel; k++) {
                            bufferFirst[c+k*TileMetrics::tileSizeSquared] = tileReadData[c*samplesPerPixel+k];
                        }
                    }

                } else {
                    readData(bi, bj, bufferFirst);
                }
            }

            // Flip data vertically
            for (uint32_t k = 0; k < samplesPerPixel; k++) {
                uint32_t koff = k*TileMetrics::tileSizeSquared;
                for (uint32_t j = 0; j < jend; j++) {
                    std::copy(bufferFirst.begin()+j*bnx+koff, bufferFirst.begin()+(j+1)*bnx+koff, v.begin()+(jend-1-j)*bnx+koff);
                }
            }

            // Check and clear vector if all data has been read
            bCacheCount[bi+bj*bcols]++;
            if (bCacheCount[bi+bj*bcols] > 1) {
                std::vector<RTYPE>().swap(bufferFirst);
            }
        
            if (bj > 0) {

                // Get second tile from GeoTIFFHandler
                std::vector<RTYPE> &bufferSecond = bCache[bi+(bj-1)*bcols];
                if (bufferSecond.empty()) {
            
                    if (samplesPerPixel > 1) {
                
                        // Read data
                        std::vector<RTYPE> tileReadData;
                        readData(bi, bj-1, tileReadData);

                        // Uninterleave data
                        bufferSecond.resize(TileMetrics::tileSizeSquared*samplesPerPixel);
                        for (uint32_t c = 0; c < TileMetrics::tileSizeSquared; c++) {
                            for (uint32_t k = 0; k < samplesPerPixel; k++) {
                                bufferSecond[c+k*TileMetrics::tileSizeSquared] = tileReadData[c*samplesPerPixel+k];
                            }
                        }

                    } else {
                        readData(bi, bj-1, bufferSecond);
                    }
                }

                // Flip data vertically
                for (uint32_t k = 0; k < samplesPerPixel; k++) {
                    uint32_t koff = k*TileMetrics::tileSizeSquared;
                    for (uint32_t j = jend; j < bny; j++) {
                        std::copy(bufferSecond.begin()+j*bnx+koff, bufferSecond.begin()+(j+1)*bnx+koff, v.begin()+(bny-1-j+jend)*bnx+koff);
                    }
                }
            
                // Check and clear vector if all data has been read
                bCacheCount[bi+(bj-1)*bcols]++;
                if (bCacheCount[bi+(bj-1)*bcols] > 1) {
                    std::vector<RTYPE>().swap(bufferSecond);
                }
            } else {

                // Clear first row
                std::vector<RTYPE>().swap(bufferFirst);
            }
        } else {

            if (samplesPerPixel > 1) {
                
                // Read data
                std::vector<RTYPE> tileReadData;
                readData(tdim.ti, tdim.tj, tileReadData);

                // Uninterleave data
                for (uint32_t c = 0; c < TileMetrics::tileSizeSquared; c++) {
                    for (uint32_t k = 0; k < samplesPerPixel; k++) {
                        v[c+k*TileMetrics::tileSizeSquared] = tileReadData[c*samplesPerPixel+k];
                    }
                }

            } else {
                readData(tdim.ti, tdim.tj, v);
            }
        }
    }

    /**
    * Supply strip data to %Tile through callback function
    * @param tdim %TileDimensions of tile.
    * @param v vector to read to.
    * @param r %Raster to read to.
    */
    template <typename RTYPE, typename CTYPE>
    void GeoTIFFHandler<RTYPE, CTYPE>::readDataStripFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r) {
    
        // Calculate offsets
        int64_t istart = (int64_t)tdim.ti*tdim.d.mx;
        int64_t iend = (int64_t)(tdim.ti+1)*tdim.d.mx;
        uint32_t ilen = tdim.d.mx-std::max(iend-nx, (int64_t)0);

        int64_t joff = flip_y ? (int64_t)ny-((int64_t)tdim.tj+1)*tdim.d.my : 0;
        int64_t jstart = flip_y ? std::max(joff, (int64_t)0) : tdim.tj*tdim.d.my;
        int64_t jend = flip_y ? (int64_t)ny-(int64_t)tdim.tj*tdim.d.my : (tdim.tj+1)*tdim.d.my;
        
        uint32_t tj = flip_y ? tdim.d.my+std::min(joff, (int64_t)0)-1 : 0;
        uint32_t sjend = jend/bny;
        uint32_t sjstart = jstart/bny;

        for (uint32_t bj = sjstart; bj < std::min(sjend+1, (uint32_t)bCache.size()); bj++) {
        
            std::vector<RTYPE> &buffer = bCache[bj];
            if (buffer.empty()) {            
                if (samplesPerPixel > 1) {

                    // Read data
                    std::vector<RTYPE> tileReadData;
                    readData(0, bj, tileReadData);

                    // Uninterleave data
                    auto stripSize = tileReadData.size();
                    buffer.resize(stripSize);
                    for (uint32_t c = 0; c < stripSize/samplesPerPixel; c++) {
                        for (uint32_t k = 0; k < samplesPerPixel; k++) {
                            buffer[c+k*stripSize] = tileReadData[c*samplesPerPixel+k];
                        }
                    }

                } else {
                    readData(0, bj, buffer);
                }
            }

            if (flip_y) {

                // Flip data vertically
                uint32_t js = std::max(jstart-(int64_t)bj*bny, (int64_t)0);
                uint32_t je = std::min(jend-(int64_t)bj*bny, (int64_t)bny);
                for (uint32_t k = 0; k < samplesPerPixel; k++) {
                    uint32_t koff = k*tdim.d.mx*tdim.d.my;
                    for (uint32_t j = js; j < je && tj >= 0; j++, tj--) {
                        uint32_t soff = istart+j*bnx+koff;
                        std::copy(buffer.begin()+soff, buffer.begin()+soff+ilen, v.begin()+tj*tdim.d.mx+koff);
                    }
                }

            } else {
            
                uint32_t js = std::max(jstart-(int64_t)bj*bny, (int64_t)0);
                uint32_t je = std::min(jend-(int64_t)bj*bny, (int64_t)bny);
                for (uint32_t k = 0; k < samplesPerPixel; k++) {
                    uint32_t koff = k*tdim.d.mx*tdim.d.my;
                    for (uint32_t j = js; j < je && tj < tdim.d.my; j++, tj++) {
                        uint32_t soff = istart+j*bnx+koff;
                        std::copy(buffer.begin()+soff, buffer.begin()+soff+ilen, v.begin()+tj*tdim.d.mx+koff);
                    }
                }
            }

            // Check and clear vector if all data has been read
            bCacheCount[bj]++;
            uint32_t maxCount = 1+(nx/tdim.d.mx);
            if (bny > 1) {
                maxCount <<= 1;
            }
            if (bCacheCount[bj] >= maxCount) {
                std::vector<RTYPE>().swap(buffer);
            }
        }
    }
        
    // RTYPE float, CTYPE float definitions
    template class GeoTIFFHandler<float, float>;
    
    // RTYPE uchar, CTYPE float definitions
    template class GeoTIFFHandler<uint8_t, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class GeoTIFFHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class GeoTIFFHandler<double, double>;

    // RTYPE uchar, CTYPE double definitions
    template class GeoTIFFHandler<uint8_t, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class GeoTIFFHandler<uint32_t, double>;

}
