/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <algorithm>
#include <string>
#include <cmath>
#include <cctype>
#include <regex>
#include <sstream>
#include <iomanip>

#include "gs_models.h"
#include "gs_string.h"
#include "gs_vector.h"
#include "gs_projection.h"
#include "gs_geojson.h"
#include "gs_shapefile.h"
#include "gs_geowkt.h"
#include "gs_delaunay.h"

namespace Geostack
{
    /**
    * %Point construction.
    * Creates %Point from a %Vertex.
    * @param pointVertex_ %Vertex to use for point.
    */
    template <typename CTYPE>
    void Point<CTYPE>::addVertex(cl_uint pointVertex_) {
        pointVertex = pointVertex_;
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %Point vertex
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *Point<CTYPE>::clone() {

        // Create new point
        Point<CTYPE> *r = new Point<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->pointVertex = pointVertex;

        return r;
    }

    /**
    * Get %Point bounding box.
    * @return pair of co-located coordinates.
    */
    template <typename CTYPE>
    BoundingBox<CTYPE> Point<CTYPE>::getBounds() const {
        return BoundingBox<CTYPE> { bc, bc };
    }

    /**
    * Get %Point centroid box.
    * @return coordinate of %Point of co-located coordinates.
    */
    template <typename CTYPE>
    Coordinate<CTYPE> Point<CTYPE>::getCentroid() const {
        return bc;
    }

    /**
    * Update %Point bounding box.
    */
    template <typename CTYPE>
    void Point<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Store coordinate for bounds
        bc = v.getCoordinate(pointVertex);
    }

    /**
    * Check for %Point vertex index
    */
    template <typename CTYPE>
    bool Point<CTYPE>::hasVertexIndex(cl_uint idx) {
        return idx == pointVertex;
    }

    /**
    * Check if vertex is shared with %Point
    */
    template <typename CTYPE>
    cl_uint Point<CTYPE>::connectedVertices(VectorGeometryPtr<CTYPE> &g) {
        return g->hasVertexIndex(pointVertex) ? 1 : 0;
    }

    /**
    * Check if %Point is within %BoundingBox
    */
    template <typename CTYPE>
    bool Point<CTYPE>::within(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {
        return b.contains(bc);
    }

    /**
    * Check if %Point is intercepts %BoundingBox
    */
    template <typename CTYPE>
    bool Point<CTYPE>::intercepts(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {
        return false;
    }

    /**
    * Check if %Point encloses %BoundingBox
    */
    template <typename CTYPE>
    bool Point<CTYPE>::encloses(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {
        return false;
    }

    /**
    * %LineString construction.
    * Adds a %Vertex to a line string
    * @param pointVertex_ %Vertex to add to line.
    */
    template <typename CTYPE>
    void LineString<CTYPE>::addVertex(cl_uint lineVertex_) {

        // Add to list
        lineVertices.push_back(lineVertex_);
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %LineString vertices
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *LineString<CTYPE>::clone() {

        // Create new linestring
        LineString<CTYPE> *r = new LineString<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->lineVertices = lineVertices;

        // Copy bounds
        r->bounds = bounds;

        return r;
    }

    /**
    * Update %LineString bounding box.
    */
    template <typename CTYPE>
    void LineString<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Reset bounds
        bounds.reset();

        // Update bounds
        for (auto &index : lineVertices) {
            bounds.extend(v.getCoordinate(index));
        }
    }

    /**
    * Check for %LineString vertex index
    */
    template <typename CTYPE>
    bool LineString<CTYPE>::hasVertexIndex(cl_uint idx) {
        for (auto &l : lineVertices) {
            if (l == idx) {
                return true;
            }
        }
        return false;
    }

    /**
    * Count number of vertices shared with %LineString
    */
    template <typename CTYPE>
    cl_uint LineString<CTYPE>::connectedVertices(VectorGeometryPtr<CTYPE> &g) {
        cl_uint nConnected = 0;
        for (auto &i : lineVertices) {
            if (g->hasVertexIndex(i)) {
                nConnected++;
            }
        }
        return nConnected;
    }

    /**
    * Check if %LineString is within %BoundingBox
    */
    template <typename CTYPE>
    bool LineString<CTYPE>::within(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {

        // Check if all linestring vertices are within bounding box
        for (auto &i : lineVertices) {
            if (!b.contains(v.getCoordinate(i))) {
                return false;
            }
        }
        return true;
    }

    /**
    * Check if %LineString intercepts %BoundingBox
    */
    template <typename CTYPE>
    bool LineString<CTYPE>::intercepts(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {

        // Check each segment against bounding box
        for (uint32_t i = 0; i < lineVertices.size()-1; i++) {
            auto &c0 = v.getCoordinate(lineVertices[i]);
            auto &c1 = v.getCoordinate(lineVertices[i+1]);
            if (intercepts2D<CTYPE>(c0.p, c0.q, c1.p, c1.q, b)) {
                return true;
            }
        }
        return false;
    }

    /**
    * Check if %LineString encloses %BoundingBox
    */
    template <typename CTYPE>
    bool LineString<CTYPE>::encloses(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {
        return false;
    }

    /**
    * %Polygon construction.
    * Adds a %Polygon to a line string
    * @param pointVertex_ %Polygon to add to line.
    * @param index sub-polygon index.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::addVertex(cl_uint polygonVertex_) {

        // Add to list
        polygonVertices.push_back(polygonVertex_);
    }

    /**
    * Clone Vertex data
    * Makes a copy of the %Polygon vertices
    */
    template <typename CTYPE>
    VectorGeometry<CTYPE> *Polygon<CTYPE>::clone() {

        // Create new polygon
        Polygon<CTYPE> *r = new Polygon<CTYPE>();

        // Set ID
        r->setID(VectorGeometry<CTYPE>::id);

        // Copy geometry
        r->polygonVertices = polygonVertices;
        r->polygonSubIndexes = polygonSubIndexes;
        r->polygonBounds = polygonBounds;

        return r;
    }

    /**
    * %Polygon construction.
    * Adds a sub-polygon to a %Polygon.
    * The first sub-polygon contains vertices for the external polygon.
    * Subsequent sub-polygon contain vertices for holes in the polygon.
    * @param length number of vertices in sub-polygon.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::addSubPolygon(cl_uint length) {
        polygonSubIndexes.push_back(length);
    }

    /**
    * Update %Polygon bounding box.
    */
    template <typename CTYPE>
    void Polygon<CTYPE>::updateBounds(const Vector<CTYPE> &v) {

        // Clear list
        polygonBounds.clear();

        // Update bounds
        cl_uint index = 0;
        for (auto &len : polygonSubIndexes) {

            // Find sub-polygon bounds
            BoundingBox<CTYPE> subPolygonBounds;
            for (cl_uint i = index; i < index+len; i++) {
                subPolygonBounds.extend(v.getCoordinate(polygonVertices[i]));
            }

            // Add bounds to list
            polygonBounds.push_back(subPolygonBounds);

            // Update index
            index+=len;
        }
    }

    /**
    * Check for %Polygon vertex index
    */
    template <typename CTYPE>
    bool Polygon<CTYPE>::hasVertexIndex(cl_uint idx) {
        auto it = polygonVertices.begin(); it++; // Skip first vertex
        while (it != polygonVertices.end()) {
            if (*it == idx) {
                return true;
            }
            it++;
        }
        return false;
    }

    /**
    * Count number of vertices shared with %Polygon
    */
    template <typename CTYPE>
    cl_uint Polygon<CTYPE>::connectedVertices(VectorGeometryPtr<CTYPE> &g) {
        cl_uint nConnected = 0;
        auto it = polygonVertices.begin(); it++; // Skip first vertex
        while (it != polygonVertices.end()) {
            if (g->hasVertexIndex(*it)) {
                nConnected++;
            }
            it++;
        }
        return nConnected;
    }

    /**
    * Check if %Polygon is within %BoundingBox
    */
    template <typename CTYPE>
    bool Polygon<CTYPE>::within(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {

        // Check if all polygon vertices are within bounding box
        auto it = polygonVertices.begin(); it++; // Skip first vertex
        while (it != polygonVertices.end()) {
            if (!b.contains(v.getCoordinate(*it))) {
                return false;
            }
            it++;
        }
        return true;
    }

    /**
    * Check if %Polygon intercepts %BoundingBox
    */
    template <typename CTYPE>
    bool Polygon<CTYPE>::intercepts(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {

        // Check if any polygon vertex is within bounding box
        std::size_t nWithin = 0;
        for (auto &i : polygonVertices) {
            if (b.contains(v.getCoordinate(i))) {
                nWithin++;
            }
        }
        if (nWithin == polygonVertices.size()) {

            // All vertices are within bounding box, no intercept
            return false;
        }
        if (nWithin > 0) {

            // At least one vertex is within bounding box, intercept
            return true;
        }

        // Check each polygon segment against bounding box
        std::size_t current = 0;
        std::vector<cl_uint> si = polygonSubIndexes;
        for (std::size_t i = 0; i < polygonVertices.size(); i++) {

            // Check for end of ring
            if (--si[current] == 0) {

                // Finish ring and move to next sub-ring
                current++;

            } else {

                // Check segment
                auto &c0 = v.getCoordinate(polygonVertices[i]);
                auto &c1 = v.getCoordinate(polygonVertices[i+1]);
                if (intercepts2D<CTYPE>(c0.p, c0.q, c1.p, c1.q, b)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
    * Check if %Polygon encloses %BoundingBox
    */
    template <typename CTYPE>
    bool Polygon<CTYPE>::encloses(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const {

        // Check against polygon bounding box
        auto pb = getBounds();
        if (!BoundingBox<CTYPE>::boundingBoxContains2D(pb, b)) {
            return false;
        }

        // Count ray intercepts
        std::size_t nIntercept0 = 0;
        std::size_t nIntercept1 = 0;
        std::size_t nIntercept2 = 0;
        std::size_t nIntercept3 = 0;

        // Check each polygon segment against bounding box
        CTYPE boundsEnd = 2.0*(pb.max.p-pb.min.p);
        std::size_t current = 0;
        std::vector<cl_uint> si = polygonSubIndexes;
        for (std::size_t i = 0; i < polygonVertices.size(); i++) {

            // Check for end of ring
            if (--si[current] == 0) {

                // Finish ring and move to next sub-ring
                current++;

            } else {

                // Check rays
                auto &c0 = v.getCoordinate(polygonVertices[i]);
                auto &c1 = v.getCoordinate(polygonVertices[i+1]);

                if (intercepts2D<CTYPE>(c0.p, c0.q, c1.p, c1.q, b.min.p, b.min.q, b.min.p+boundsEnd, b.min.q)) {
                    nIntercept0++;
                }
                if (intercepts2D<CTYPE>(c0.p, c0.q, c1.p, c1.q, b.min.p, b.max.q, b.min.p+boundsEnd, b.max.q)) {
                    nIntercept1++;
                }
                if (intercepts2D<CTYPE>(c0.p, c0.q, c1.p, c1.q, b.max.p, b.min.q, b.max.p+boundsEnd, b.min.q)) {
                    nIntercept2++;
                }
                if (intercepts2D<CTYPE>(c0.p, c0.q, c1.p, c1.q, b.max.p, b.max.q, b.max.p+boundsEnd, b.max.q)) {
                    nIntercept3++;
                }
            }
        }

        if (nIntercept0&0x01 || nIntercept1&0x01 || nIntercept2&0x01 || nIntercept3&0x01) {
            return true;
        }
        return false;
    }

    /**
    * Default %Vector constructor
    * dimensions will be POD initialised to zero
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector():
        proj() {

        // Create data
        pVertices = std::make_shared<VariablesVector<Coordinate<CTYPE> > >();
        pGeometry = std::make_shared<std::vector<VectorGeometryPtr<CTYPE> > >();
        pProperties = std::make_shared<PropertyMap>();
        pGlobalProperties = std::make_shared<PropertyMap>();

        // Set name
        pGlobalProperties->template setProperty<std::string>("name", "");
    }

    /**
    * %Vector constructor with name
    * dimensions will be POD initialised to zero
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector(std::string name):
        proj() {

        // Create data
        pVertices = std::make_shared<VariablesVector<Coordinate<CTYPE> > >();
        pGeometry = std::make_shared<std::vector<VectorGeometryPtr<CTYPE> > >();
        pProperties = std::make_shared<PropertyMap>();
        pGlobalProperties = std::make_shared<PropertyMap>();

        // Set name
        pGlobalProperties->template setProperty<std::string>("name", name);
    }

    /**
    * %Vector constructor for set of %Points with properties
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector(
        std::shared_ptr<VariablesVector<Coordinate<CTYPE> > > _pVertices,
        std::shared_ptr<PropertyMap> _pProperties,
        std::shared_ptr<PropertyMap> _pGlobalProperties):
        proj() {

        // Check vertex data
        if (!_pVertices) {
            throw std::runtime_error("Vector constructor cannot use null vertices pointer");
        }
        pVertices = _pVertices;
        auto cSize = pVertices->size();

        // Check property data
        if (!_pProperties) {
            pProperties = std::make_shared<PropertyMap>();
        } else {

            // Check properties
            auto &properties = *_pProperties;
            for (auto propertyName : properties.getPropertyNames()) {

                if (properties.getPropertyType(propertyName) != PropertyType::Undefined) {

                    // Check for vector type
                    if (properties.getPropertyStructure(propertyName) != PropertyStructure::Vector) {
                        throw std::runtime_error("Vector constructor can only use vector properties");
                    }
                    if (properties.getSize(propertyName) != cSize) {
                        throw std::runtime_error("Vector constructor properties must have same size as number of coordinates");
                    }
                }

            }
            pProperties = _pProperties;
        }

        // Check property data
        if (!_pGlobalProperties) {
            pGlobalProperties = std::make_shared<PropertyMap>();
        } else {

            // Check properties
            auto &properties = *_pGlobalProperties;
            for (auto propertyName : properties.getPropertyNames()) {

                if (properties.getPropertyType(propertyName) != PropertyType::Undefined) {

                    // Check for vector type
                    if (properties.getPropertyStructure(propertyName) != PropertyStructure::Vector) {
                        throw std::runtime_error("Vector constructor can only use vector properties");
                    }
                    if (properties.getSize(propertyName) != cSize) {
                        throw std::runtime_error("Vector constructor properties must have same size as number of coordinates");
                    }
                }

            }
            pGlobalProperties = _pGlobalProperties;
        }

        // Create geometry
        pGeometry = std::make_shared<std::vector<VectorGeometryPtr<CTYPE> > >();
        auto &geometry = *pGeometry;

        // Create points
        auto &coords = pVertices->getData();
        for (std::size_t index = 0; index < cSize; index++) {

            // Create point
            PointPtr<CTYPE> p = std::make_shared<Point<CTYPE> >(coords[index], index);

            // Update tree
            tree.insert(p);

            // Update vector
            geometry.push_back(p);

            // Update geometry indexes
            geometryIndexes.push_back(index);
            pointIndexes.push_back(index);
            p->setID(index);
        }
    }

    /**
    * Destructor
    */
    template <typename CTYPE>
    Vector<CTYPE>::~Vector() {
    }

    /**
    * Copy constructor
    */
    template <typename CTYPE>
    Vector<CTYPE>::Vector(const Vector<CTYPE> &v):
        proj(v.proj),
        pVertices(v.pVertices),
        pGeometry(v.pGeometry),
        geometryIndexes(v.geometryIndexes),
        pointIndexes(v.pointIndexes),
        lineStringIndexes(v.lineStringIndexes),
        polygonIndexes(v.polygonIndexes),
        relations(v.relations),
        relationVector(v.relationVector),
        pProperties(v.pProperties),
        pGlobalProperties(v.pGlobalProperties) {

        // Copy data
        tree = v.tree;
    }

    /**
    * Clear %Vector data
    */
    template <typename CTYPE>
    void Vector<CTYPE>::clear() {

        // Clear projection
        proj = ProjectionParameters<double>();

        // Clear geometry
        tree.clear();
        pVertices->clear();
        pGeometry->clear();
        geometryIndexes.clear();
        pointIndexes.clear();
        lineStringIndexes.clear();
        polygonIndexes.clear();

        // Clear relations
        relations.clear();
        relationVector.clear();

        // Clear properties
        pProperties->clear();
        pGlobalProperties->clear();
    }

    /**
    * Clear and build RTree
    */
    template <typename CTYPE>
    void Vector<CTYPE>::buildTree() {

        // Clear tree
        tree.clear();

        // Build tree
        auto &geometry = *pGeometry;
        for (auto &g : geometry) {

            // Update bounding box
            g->updateBounds(*this);

            // Update tree
            tree.insert(g);
        }
    }

    /**
    * Read data from file
    */
    template <typename CTYPE>
    void Vector<CTYPE>::read(std::string fileName, std::string jsonConfig) {

        auto &v = *this;

        // Get filename extension
        auto split = Strings::splitPath(fileName);

        if (split[2] == "json" || split[2] == "geojson") {

            // Read geojson
            v = GeoJson<CTYPE>::geoJsonFileToVector(fileName);

        } else if (split[2] == "shp") {

            // Read shapefile
            v = ShapeFile<CTYPE>::shapefileToVector(fileName, jsonConfig);

        } else if (split[2] == "wkt") {

            // Read geoWKT
            v = GeoWKT<CTYPE>::geoWKTFileToVector(fileName);

        } else {
            std::stringstream err;
            err << "File name has unrecognised extension '" << split[2] << "'";
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Write data to file
    * @param fileName
    * @param writeType
    * @param enforceProjection
    * @param writeNullProperties
    */
    template <typename CTYPE>
    void Vector<CTYPE>::write(std::string fileName, size_t writeType,
                              bool enforceProjection, bool writeNullProperties) {

        auto &v = *this;

        // Get filename extension
        auto split = Strings::splitPath(fileName);

        if (split[2] == "json" || split[2] == "geojson") {

            // Write geojson
            std::ofstream out(fileName);
            out << GeoJson<CTYPE>::vectorToGeoJson(v, enforceProjection, writeNullProperties);
            if (out.is_open()) {
                out.close();
            }

        } else if (split[2] == "shp") {

            // Check for different geometry types
            cl_uint type = (cl_uint)writeType;
            if (writeType == GeometryType::All) {

                // Detect geometry types
                type = 0;
                if (v.getPointCount() > 0) type |= 0x01;
                if (v.getLineStringCount() > 0) type |= 0x02;
                if (v.getPolygonCount() > 0) type |= 0x04;
            }

            // Write shapefile
            if (type == 0x01) {
                ShapeFile<CTYPE>::vectorToShapefile(v, fileName, GeometryType::Point);
            } else if (type == 0x02) {
                ShapeFile<CTYPE>::vectorToShapefile(v, fileName, GeometryType::LineString);
            } else if (type == 0x04) {
                ShapeFile<CTYPE>::vectorToShapefile(v, fileName, GeometryType::Polygon);
            } else if (type == 0x00) {
                throw std::runtime_error("Shapefile write has no geometry");
            } else {
                throw std::runtime_error("Shapefile write must specify only one geometry type");
            }

        } else if (split[2] == "wkt") {

            // Write geoWKT
            std::ofstream out(fileName);
            out << GeoWKT<CTYPE>::vectorToGeoWKT(v);
            if (out.is_open()) {
                out.close();
            }

        } else {
            std::stringstream err;
            err << "File name has unrecognised extension '" << split[2] << "'";
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Assignment operator
    * @param v %Vector to assign.
    */
    template <typename CTYPE>
    Vector<CTYPE> &Vector<CTYPE>::operator=(const Vector<CTYPE> &v) {

        if (this != &v) {

            // Copy projection
            proj = v.proj;

            // Copy geometry
            tree = v.tree;
            pVertices = v.pVertices;
            pGeometry = v.pGeometry;
            geometryIndexes = v.geometryIndexes;
            pointIndexes = v.pointIndexes;
            lineStringIndexes = v.lineStringIndexes;
            polygonIndexes = v.polygonIndexes;

            // Copy relations
            relations = v.relations;
            relationVector = v.relationVector;

            // Copy properties
            pProperties = v.pProperties;
            pGlobalProperties = v.pGlobalProperties;
        }

        return *this;
    }

    /**
    * Addition operator
    * @param v %Vector to add.
    */
    template <typename CTYPE>
    Vector<CTYPE> &Vector<CTYPE>::operator+=(const Vector<CTYPE> &v) {

        if (this != &v) {

            // Null operation
            if (!v.hasData()) {
                return *this;
            }
            if (!hasData()) {
                operator=(v);
                return *this;
            }

            // Check projection
            if (proj != v.proj) {
                throw std::length_error("Projections do not match in Vector addition");
            }

            // Get data handles
            std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
            PropertyMap &properties = *pProperties;

            // Get original sizes
            auto initialVerticesSize = pVertices->size();
            auto initialGeometrySize = geometry.size();

            // Append vertices
            auto &vertices = pVertices->getData();
            auto &addVertices = v.pVertices->getData();
            vertices.insert(vertices.end(), addVertices.begin(), addVertices.end());

            // Append geometry
            for (auto &g : *v.pGeometry) {

                auto ag = std::shared_ptr<VectorGeometry<CTYPE> >(g->clone());
                ag->shiftVertexIndex(initialVerticesSize);
                add(ag);
            }

            // Get properties
            auto &vProperties = v.getProperties();

            auto stringProperties = vProperties.template getPropertyRefs<std::vector<std::string> >(); // TODO reduce into one function
            auto intProperties = vProperties.template getPropertyRefs<std::vector<int32_t> >();
            auto floatProperties = vProperties.template getPropertyRefs<std::vector<float> >();
            auto doubleProperties = vProperties.template getPropertyRefs<std::vector<double> >();
            auto indexProperties = vProperties.template getPropertyRefs<std::vector<cl_uint> >();
            auto byteProperties = vProperties.template getPropertyRefs<std::vector<cl_uchar> >();
            auto floatVectorProperties = vProperties.template getPropertyRefs<std::vector<std::vector<float> > >();
            auto doubleVectorProperties = vProperties.template getPropertyRefs<std::vector<std::vector<double> > >();

            // Add new properties
            for (auto &pi : stringProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : intProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : floatProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : doubleProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : indexProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : byteProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : floatVectorProperties) {
                properties.addProperty(pi.first);
            }
            for (auto &pi : doubleVectorProperties) {
                properties.addProperty(pi.first);
            }

            // Resize properties
            properties.resize(geometry.size());

            // Copy properties
            for (auto &pi : stringProperties) {
                std::vector<std::string> &p = properties.template getPropertyRef<std::vector<std::string> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<std::string>());
                std::vector<std::string> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : intProperties) {
                std::vector<int32_t> &p = properties.template getPropertyRef<std::vector<int32_t> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<int32_t>());
                std::vector<int32_t> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : floatProperties) {
                std::vector<float> &p = properties.template getPropertyRef<std::vector<float> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<float>());
                std::vector<float> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : doubleProperties) {
                std::vector<double> &p = properties.template getPropertyRef<std::vector<double> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<double>());
                std::vector<double> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : indexProperties) {
                std::vector<cl_uint> &p = properties.template getPropertyRef<std::vector<cl_uint> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<cl_uint>());
                std::vector<cl_uint> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : byteProperties) {
                std::vector<cl_uchar> &p = properties.template getPropertyRef<std::vector<cl_uchar> >(pi.first);
                p.resize(initialGeometrySize, getNullValue<cl_uchar>());
                std::vector<cl_uchar> &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : floatVectorProperties) {
                auto &p = properties.template getPropertyRef<std::vector<std::vector<float> > >(pi.first);
                p.resize(initialGeometrySize, std::vector<float>());
                auto &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
            for (auto &pi : doubleVectorProperties) {
                auto &p = properties.template getPropertyRef<std::vector<std::vector<double> > >(pi.first);
                p.resize(initialGeometrySize, std::vector<double>());
                auto &vp = pi.second.get();
                p.insert(p.end(), vp.begin(), vp.end());
            }
        }

        return *this;
    }

    /**
    * Add @VectorGeometry to %Vector
    * @param g @VectorGeometry to add.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::add(VectorGeometryPtr<CTYPE> g) {

        // Update bounds
        g->updateBounds(*this);

        // Update tree
        tree.insert(g);

        // Update vector
        pGeometry->push_back(g);

        // Update geometry index
        cl_uint index = pGeometry->size()-1;
        geometryIndexes.push_back(index);
        g->setID(index);
        g->updateVector(*this, index);

        // Return index
        return index;
    }

    /**
    * Clone geometry item
    * @param id geometry id
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::clone(cl_uint id) {

        // Get geometry element
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        auto g = geometry[id];

        // Clone geometry
        auto ng = std::shared_ptr<VectorGeometry<CTYPE> >(g->clone());
        auto nid = add(ng);

        // Copy properties
        PropertyMap &properties = *pProperties;
        properties.resize(geometry.size());
        for (auto &name : properties.getPropertyNames()) {
            if (properties.getPropertyStructure(name) == PropertyStructure::Vector) {
                properties.copy(name, id, nid);
            }
        }

        return nid;
    }

    /**
    * Add %Point to %Vector.
    * @param c_ Coordinate pair.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addPoint(Coordinate<CTYPE> c_) {

        // Create point
        PointPtr<CTYPE> p = std::make_shared<Point<CTYPE> >();

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Create vertex
        cl_uint index = vertices.size();
        vertices.push_back(c_);

        // Associate vertex with point
        p->addVertex(index);

        // Add and return index of geometry
        return add(p);
    }

    /**
    * Add %LineString to %Vector
    * @param cs_ List of coordinate pairs.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addLineString(CoordinateList<CTYPE> cs_) {

        // Check size
        if (cs_.size() < 2) {
            throw std::length_error("Two or more coordinates required for line string creation");
        }

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Create new line string
        LineStringPtr<CTYPE> l = std::make_shared<LineString<CTYPE> >();
        for (auto &c : cs_) {

            // Add vertices
            cl_uint index = vertices.size();
            vertices.push_back(c);

            // Add to line
            l->addVertex(index);
        }

        // Add and return index of geometry
        return add(l);
    }

    /**
    * Add %Polygon to %Vector
    * @param pcs_ List of list of coordinate pairs.
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::addPolygon(std::vector<CoordinateList<CTYPE> > pcs_) {

        // Check size
        if (pcs_.size() == 0 || pcs_[0].size() < 3)
            throw std::length_error("Three or more coordinates required for polygon creation");

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Create new polygon
        PolygonPtr<CTYPE> p = std::make_shared<Polygon<CTYPE> >();
        for (auto &cs : pcs_) {

            // Polygon bounds
            BoundingBox<CTYPE> bounds = { cs[0], cs[0] };

            // Add vertices
            cl_uint indexFirst = vertices.size();
            for (auto &c : cs) {

                // Update bounds
                bounds.extend(c);

                // Add vertices
                cl_uint index = vertices.size();
                vertices.push_back(c);

                // Add to polygon
                p->addVertex(index);
            }

            // Check first and last coordinates are the same
            if (cs.front() != cs.back()) {

                // Repeat first index
                p->addVertex(indexFirst);

                // Create new sub polygon
                p->addSubPolygon(cs.size()+1);

            } else {

                // Create new sub polygon
                p->addSubPolygon(cs.size());
            }
        }

        // Add and return index of geometry
        return add(p);
    }

    /**
    * Get %Point %Coordinate from %Vector.
    * @return %Point coordinate
    */
    template <typename CTYPE>
    Coordinate<CTYPE> Vector<CTYPE>::getPointCoordinate(const cl_uint index) const {

        // Get geometry references
        auto &c = pVertices->getData();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Get point reference
        auto &point = *std::static_pointer_cast<Point<CTYPE> >(geometry[index]);

        // Return coordinate
        return c[point.pointVertex];
    }

    /**
    * Get %LineString coordinates from %Vector.
    * @return %LineString coordinates
    */
    template <typename CTYPE>
    CoordinateList<CTYPE> Vector<CTYPE>::getLineStringCoordinates(const cl_uint index) const {

        // Get geometry references
        auto &c = pVertices->getData();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Get line string reference
        auto &lineString = *std::static_pointer_cast<LineString<CTYPE> >(geometry[index]);

        // Create coordinate list
        CoordinateList<CTYPE> cl;
        for (auto li : lineString.lineVertices)
            cl.push_back(c[li]);

        // Return coordinate list
        return cl;
    }

    /**
    * Get %Polygon coordinates from %Vector.
    * @return %Polygon coordinates
    */
    template <typename CTYPE>
    CoordinateList<CTYPE> Vector<CTYPE>::getPolygonCoordinates(const cl_uint index) const {

        // Get geometry references
        auto &c = pVertices->getData();
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Get polygon reference
        auto &polygon = *std::static_pointer_cast<Polygon<CTYPE> >(geometry[index]);

        // Create coordinate list
        CoordinateList<CTYPE> cl;
        for (auto pi : polygon.polygonVertices)
            cl.push_back(c[pi]);

        // Return coordinate list
        return cl;
    }

    /**
    * Get %Polygon indexes from %Vector.
    * @return index list
    */
    template <typename CTYPE>
    const std::vector<cl_uint> &Vector<CTYPE>::getPolygonVertexIndexes(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getVertexIndexes();
    }

    /**
    * Get %Polygon sub-indexes from %Vector.
    * @return index list
    */
    template <typename CTYPE>
    const std::vector<cl_uint> &Vector<CTYPE>::getPolygonSubIndexes(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getSubIndexes();
    }

    /**
    * Get %Polygon sub-bounds from %Vector.
    * @return %BoundingBox list
    */
    template <typename CTYPE>
    const std::vector<BoundingBox<CTYPE> > &Vector<CTYPE>::getPolygonSubBounds(const cl_uint index) const {
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        return std::static_pointer_cast<Polygon<CTYPE> >(geometry[index])->getSubBounds();
    }

    /**
    * Check property name in %Vector.
    * @param name %Property name.
    * @return true if found, false otherwise
    */
    template <typename CTYPE>
    bool Vector<CTYPE>::hasProperty(std::string name) const {

        // Get data handles
        PropertyMap &properties = *pProperties;
        PropertyMap &globalProperties = *pGlobalProperties;
        return properties.hasProperty(name) || globalProperties.hasProperty(name);
    }

    /**
    * Check whether property in %Vector is integer or float
    * @param name %Property name.
    * @return true if found, false otherwise
    */
    template <typename CTYPE>
    bool Vector<CTYPE>::isPropertyNumeric(std::string name) const {

        // Get data handles
        PropertyMap &properties = *pProperties;
        PropertyMap &globalProperties = *pGlobalProperties;
        if (properties.hasProperty(name)) {
            auto type = properties.getPropertyType(name);
            if (type == PropertyType::Byte ||
                type == PropertyType::Index ||
                type == PropertyType::Integer
                #if defined(REAL_FLOAT)
                || type == PropertyType::Float) {
                #elif defined(REAL_DOUBLE)
                || type == PropertyType::Double) {
                #else
                ) {
                #endif
                return true;
            }
        } else if (globalProperties.hasProperty(name)) {
            auto type = globalProperties.getPropertyType(name);
            if (type == PropertyType::Byte ||
                type == PropertyType::Index ||
                type == PropertyType::Integer
                #if defined(REAL_FLOAT)
                || type == PropertyType::Float) {
                #elif defined(REAL_DOUBLE)
                || type == PropertyType::Double) {
                #else
                ) {
                #endif
                return true;
            }
        }
        return false;
    }

    /**
    * Add property to %Vector.
    * @param name %Property name.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::addProperty(std::string name) {

        // Get data handle
        PropertyMap &properties = *pProperties;

        // Add property
        properties.addProperty(name);
    }

    /**
    * Convert property type in %Vector.
    * @param name %Property name.
    */
    template <typename CTYPE>
    template <typename PTYPE>
    void Vector<CTYPE>::convertProperty(std::string name) {

        // Get data handle
        PropertyMap &properties = *pProperties;

        // Add property
        properties.convertProperty<std::vector<PTYPE> >(name);
    }

    /**
    * Remove property from %Vector.
    * @param name %Property name.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::removeProperty(std::string name) {

        // Get data handle
        PropertyMap &properties = *pProperties;

        // remove property
        properties.removeProperty(name);
    }

    /**
    * Set global property for %Vector.
    * @param name %Property name.
    * @param v %Property value.
    */
    template <typename CTYPE>
    template <typename PTYPE>
    void Vector<CTYPE>::setGlobalProperty(std::string name, PTYPE v) {

        // Set property scalar
        PropertyMap &properties = *pGlobalProperties;
        properties.template setProperty<PTYPE>(name, v);
    }

    /**
    * Set property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name %Property name.
    * @param v %Property value.
    */
    template <typename CTYPE>
    template <typename PTYPE>
    void Vector<CTYPE>::setProperty(cl_uint index, std::string name, PTYPE v) {

        // Get data handles
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        PropertyMap &properties = *pProperties;

        // Create property
        properties.addProperty(name);

        // Set property
        if (index < geometry.size()) {

            // Get property vector
            std::vector<PTYPE> &p = properties.template getPropertyRef<std::vector<PTYPE> >(name);

            // Resize vector
            if (p.size() < geometry.size()) {
                properties.resize(geometry.size());
            }

            // Set vector
            p[index] = v;

        } else {

            // Index out of range
            std::stringstream err;
            err << "Vector index " << index << " out of range for property '" << name << "'";
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Set property vector in %Vector.
    * @param name %Property name.
    * @param v %Property value.
    */
    template <typename CTYPE>
    template <typename PTYPE>
    void Vector<CTYPE>::setPropertyVector(std::string name, PTYPE v) {

        // Get data handles
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        PropertyMap &properties = *pProperties;

        // Check data size
        if (v.size() != geometry.size()) {
            std::stringstream err;
            err << "Invalid vector size for setting property '" << name << "'";
            throw std::runtime_error(err.str());
        }

        // Create property
        properties.addProperty(name);

        // Get property vector
        PTYPE &p = properties.template getPropertyRef<PTYPE>(name);

        // Resize vector
        properties.resize(geometry.size());

        // Set property vector
        p = v;
    }

    /**
    * Get global property for %VectorGeometry in %Vector.
    * @param name %Property name.
    * @return %Property value
    */
    template <typename CTYPE>
    template <typename PTYPE>
    PTYPE Vector<CTYPE>::getGlobalProperty(std::string name) const {

        // Get data handles
        PropertyMap &properties = *pGlobalProperties;
        return properties.template getProperty<PTYPE>(name);
    }

    /**
    * Get property for %VectorGeometry in %Vector.
    * @param index %VectorGeometry item.
    * @param name %Property name.
    * @return %Property value
    */
    template <typename CTYPE>
    template <typename PTYPE>
    PTYPE Vector<CTYPE>::getProperty(cl_uint index, std::string name) const {

        // Check data size for defined properties
        PropertyMap &properties = *pProperties;
        if (properties.getPropertyType(name) != PropertyType::Undefined) {
            std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
            if (properties.getSize(name) < geometry.size()) {
                properties.resize(geometry.size());
            }
        }

        // Return with conversion
        return properties.template getPropertyFromVector<PTYPE>(name, index);
    }

    /**
    * Get property type for %VectorGeometry in %Vector.
    * @param name %Property name.
    * @return %PropertyType value
    */
    template <typename CTYPE>
    PropertyType::Type Vector<CTYPE>::getPropertyType(std::string name) const {

        // Check data size for defined properties
        PropertyMap &properties = *pProperties;
        return properties.getPropertyType(name);
    }

    /**
    * Get property vectors in %Vector.
    * @param name %Property name.
    * @return %Property value
    */
    template <typename CTYPE>
    template <typename PTYPE>
    PTYPE &Vector<CTYPE>::getPropertyVectorsRef(std::string name) {

        // Get data handles
        PropertyMap &properties = *pProperties;
        return properties.template getPropertyRef<PTYPE>(name);
    }

    /**
    * Get property vector in %Vector.
    * @param index %VectorGeometry item.
    * @param name %Property name.
    * @return %Property value
    */
    template <typename CTYPE>
    template <typename PTYPE>
    PTYPE &Vector<CTYPE>::getPropertyVectorRef(cl_uint index, std::string name) {
        auto &pv = this->template getPropertyVectorsRef<std::vector<std::vector<CTYPE> > >(name);
        return pv[index];
    }

    /**
    * Check if %Property is buffered in %Vector.
    * @param name %Property name.
    * @return true if %Property has buffer, false otherwise
    */
    template <typename CTYPE>
    bool Vector<CTYPE>::hasPropertyBuffer(std::string name) {

        // Check property
        PropertyMap &properties = *pProperties;
        return properties.hasPropertyBuffer(name);
    }
    /**
    * Get property buffer in %Vector.
    * @param name %Property name.
    * @return OpenCL buffer
    */
    template <typename CTYPE>
    cl::Buffer const &Vector<CTYPE>::getPropertyBuffer(std::string name) {

        // Check data size
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
        PropertyMap &properties = *pProperties;
        if (properties.getSize(name) < geometry.size()) {
            properties.resize(geometry.size());
        }

        // Get data handles
        return properties.getPropertyBuffer(name);
    }

    /**
    * Convert %Vector projection
    * @param to projection to convert to
    * @return %Vector converted into new projection
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::convert(ProjectionParameters<double> to) const {

        Vector<CTYPE> v;

        // Check for data
        if (hasData()) {

            // Clone vertices
            v.pVertices = std::shared_ptr<VariablesVector<Coordinate<CTYPE> > >(pVertices->clone());

            // Copy geometry data
            v.geometryIndexes = geometryIndexes;
            v.pointIndexes = pointIndexes;
            v.lineStringIndexes = lineStringIndexes;
            v.polygonIndexes = polygonIndexes;

            // Copy property data
            v.pProperties = pProperties;

            // Clone data
            auto &geometry = *pGeometry;
            auto &rGeometry = *v.pGeometry;
            for (auto &g : geometry) {
                rGeometry.emplace_back(g->clone());
            }

            // Return empty Vector if there are no coordinates or projection fails
            if (v.pVertices->size() == 0) {
                return v;
            }

            // Convert projection
            Projection::convert(*v.pVertices, to, proj);

            // Build tree
            v.buildTree();

            // Update projection
            v.proj = to;
        }

        return v;
    }

    /**
    * Convert convert %Vector projection (alias of convert)
    * @param to projection to convert to
    * @return %Vector converted into new projection
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::convertProjection(ProjectionParameters<double> to) const {
        return convert(to);
    }

    /**
    * Convert %Vector type
    * @param geometryType type to convert to
    * @return %Vector converted into new type
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::convert(size_t geometryType) const {

        Vector<CTYPE> v;

        // Check for data
        if (hasData()) {

            // Convert to line strings
            if ((geometryType & GeometryType::LineString) || (geometryType & GeometryType::Polygon)) {

                // Get points
                CoordinateList<CTYPE> coords;
                for (auto &p : pointIndexes) {
                    auto c = getPointCoordinate(p);
                    coords.push_back(c); // TODO just copy list
                }

                // Add line vertices
                std::vector<uint64_t> edgeIndexes;
                for (auto &l : lineStringIndexes) {

                    // Add coordinates
                    auto initialIndex = coords.size();
                    auto lc = getLineStringCoordinates(l);
                    for (auto &c : lc) {
                        edgeIndexes.push_back(coords.size());
                        edgeIndexes.push_back(coords.size()+1);
                        coords.push_back(c);
                    }
                    edgeIndexes.pop_back();
                    edgeIndexes.pop_back();

                    // Handle loops
                    if (lc.front() == lc.back()) {
                        coords.pop_back();
                        edgeIndexes.back() = initialIndex;
                    }
                }

                // Add polygon edges
                for (auto &p : polygonIndexes) {

                    auto pc = getPolygonCoordinates(p);
                    auto si = getPolygonSubIndexes(p);
                    std::size_t current = 0;
                    auto initialIndex = coords.size();
                    for (std::size_t i = 0; i < pc.size(); i++) {

                        // Add edges and coordinate
                        edgeIndexes.push_back(coords.size());
                        edgeIndexes.push_back(coords.size()+1);
                        coords.push_back(pc[i]);

                        // Check for end of ring
                        if (--si[current] == 0) {

                            // Finish ring and move to next sub-ring
                            edgeIndexes.pop_back();
                            edgeIndexes.pop_back();
                            coords.pop_back();
                            edgeIndexes.back() = initialIndex;
                            initialIndex = coords.size();
                            current++;
                        }
                    }
                }

                // Triangulate
                v = Delaunay<CTYPE>::triangulate(coords, edgeIndexes, geometryType);

            } else if (geometryType & GeometryType::Point) {

                // Copy points
                for (auto &p : pointIndexes) {
                    v.addPoint(getPointCoordinate(p));
                }

                // Add line vertices
                for (auto &l : lineStringIndexes) {

                    auto lc = getLineStringCoordinates(l);
                    for (auto &c : lc) {
                        v.addPoint(c);
                    }
                }

                // Add polygon vertices
                for (auto &p : polygonIndexes) {

                    auto pc = getPolygonCoordinates(p);
                    for (auto &c : pc) {
                        v.addPoint(c);
                    }

                }

                // Clean up duplicate vertices
                v.deduplicateVertices();
            }

            // Set projection
            v.setProjectionParameters(proj);
        }
        return v;
    }

    /**
    * Convert %Vector type (alias of convert)
    * @param to projection to convert to
    * @return %Vector converted into new projection
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::convertGeometryType(size_t geometryType) const {
        return convert(geometryType);
    }

    /**
    * Get %Vector region from %Vector.
    * Returns all geometry intersecting region.
    * @param bounds Bounds of region
    * @param geometryTypes the types of geometry to include in resulting %Vector
    * @return %Vector of geometry within region.
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::region(BoundingBox<CTYPE> bounds, size_t geometryTypes) {

        // Get region
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.search(bounds, geometryList, geometryTypes);

        // Filter non-intercepting geometry outside bounding box
        auto end = std::remove_if(geometryList.begin(), geometryList.end(), [this, bounds](GeometryBasePtr<CTYPE> &g) {

            VectorGeometryPtr<CTYPE> pg = std::static_pointer_cast<VectorGeometry<CTYPE> >(g);

            // Check for any points within bounding box
            if (pg->within(*this, bounds))
                return false;

            // Check for intercept with bounding box
            if (pg->intercepts(*this, bounds))
                return false;

            // Check if geometry encloses bounding box
            if (pg->encloses(*this, bounds))
                return false;

            return true;
        } );
        geometryList.erase(end, geometryList.end());

        // Create return vector
        Vector<CTYPE> v;

        // Set projection
        v.setProjectionParameters(proj);

        // Sort by ID
        std::sort(geometryList.begin(), geometryList.end(),
            [](const GeometryBasePtr<CTYPE> &l, const GeometryBasePtr<CTYPE> &r) { return l->getID() < r->getID(); });

        // Copy geometry
        v.pVertices = pVertices;
        v.pGeometry = pGeometry;
        for (auto &g : geometryList) {

            // Add geometry
            v.tree.insert(g);

            // Update vector
            v.geometryIndexes.push_back(g->getID());
            std::static_pointer_cast<VectorGeometry<CTYPE> >(g)->updateVector(v, g->getID());
        }

        // Copy properties
        v.pProperties = pProperties;

        return v;
    }

    /**
    * Return nearest %VectorGeometry to bounds
    * @param bounds Bounds of region
    * @param geometryTypes the types of geometry to include in resulting %Vector
    * @return %Vector containing geometry nearest to region.
    */
    template <typename CTYPE>
    Vector<CTYPE> Vector<CTYPE>::nearest(BoundingBox<CTYPE> bounds, size_t geometryTypes) {

        // Find nearest
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.nearest(bounds, geometryList, geometryTypes);

        // Create return vector
        Vector<CTYPE> v;

        // Set projection
        v.setProjectionParameters(proj);

        // Sort by ID
        std::sort(geometryList.begin(), geometryList.end(),
            [](const GeometryBasePtr<CTYPE> &l, const GeometryBasePtr<CTYPE> &r) { return l->getID() < r->getID(); });

        // Copy geometry
        v.pVertices = pVertices;
        v.pGeometry = pGeometry;
        for (auto &g : geometryList) {

            // Add geometry
            v.tree.insert(g);

            // Update vector
            v.geometryIndexes.push_back(g->getID());
            std::static_pointer_cast<VectorGeometry<CTYPE> >(g)->updateVector(v, g->getID());
        }

        // Copy properties
        v.pProperties = pProperties;

        return v;
    }

    /**
    * Get list of geometry attached to coordinate.
    * @param %Coordinate to use.
    * @param geometryTypes the types of geometry to include in list.
    * @return list of geometry within region.
    */
    template <typename CTYPE>
    std::vector<GeometryBasePtr<CTYPE> > Vector<CTYPE>::attached(Coordinate<CTYPE> c, size_t geometryTypes) {

        // Get attached geometry
        std::vector<GeometryBasePtr<CTYPE> > geometryList;
        tree.search({c, c}, geometryList, geometryTypes);
        return geometryList;
    }

    /**
    * Returns all geometry indices intersecting region.
    * @param bounds Bounds of region
    * @param geometryList vector object to store geometry indices
    * @param geometryTypes the types of geometry to include in resulting %Vector
    * @return %Vector of geometry within region.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::regionGeometryIndices(BoundingBox<CTYPE> bounds,
                                              std::vector<GeometryBasePtr<CTYPE> > &geometryList,
                                              size_t geometryTypes) {

        // Get region
        tree.search(bounds, geometryList, geometryTypes);

        // Filter non-intercepting geometry outside bounding box
        auto end = std::remove_if(geometryList.begin(), geometryList.end(), [this, bounds](GeometryBasePtr<CTYPE> &g) {

            VectorGeometryPtr<CTYPE> pg = std::static_pointer_cast<VectorGeometry<CTYPE> >(g);

            // Check for any points within bounding box
            if (pg->within(*this, bounds))
                return false;

            // Check for intercept with bounding box
            if (pg->intercepts(*this, bounds))
                return false;

            // Check if geometry encloses bounding box
            if (pg->encloses(*this, bounds))
                return false;

            return true;
        } );
        geometryList.erase(end, geometryList.end());
    }

    /**
    * Build variables struture containing all neighbours of each element
    * @param geometryTypes the types of geometry to use.
    * @return Variables consisting of geometry ID and neighbouring IDs.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::buildRelations(size_t geometryType, size_t relationType) {

        // Clear relations
        relations.clear();

        if (relationType & RelationType::Neighbour) {

            // Build neighbours
            std::vector<GeometryBasePtr<CTYPE> > attached;
            std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
            for (auto &pg : geometry) {
                if (pg->isType(geometryType)) {

                    // Get attached geometry
                    tree.search(pg->getBounds(), attached, geometryType);

                    // Test vertex connections
                    std::size_t i = 0;
                    for (auto &a : attached) {
                        VectorGeometryPtr<CTYPE> pa = std::static_pointer_cast<VectorGeometry<CTYPE> >(a);
                        if (pg->getID() != pa->getID()) {

                            // Count attached vertices
                            auto nAttached = pg->connectedVertices(pa);
                            if ((geometryType == GeometryType::LineString && nAttached == 1) ||
                                (geometryType == GeometryType::Polygon && nAttached == 2)) {

                                // Add to relations
                                relations.set(pg->getID(), pa->getID(), i++);
                            }
                        }
                    }
                }
            }
        }

        // Create relation data
        relationVector.clear();
        auto relationIndexes = relations.getIndexes();
        for (auto g : geometryIndexes) {
            auto it = relationIndexes.find(g);
            if (it != relationIndexes.end()) {
                relationVector.push_back(relations.getSize(g));
                relationVector.push_back(it->second);
            }
            else {
                relationVector.push_back(0);
                relationVector.push_back(0);
            }
        }
    }

    /**
    * Get relation size within %Vector.
    * @param gid the geometry id.
    * @return relation size of %Vector.
    */
    template <typename CTYPE>
    std::size_t Vector<CTYPE>::getRelationSize(cl_uint gid) {
        if (!relations.hasData()) {
            throw std::length_error("No relations defined");
        }
        return relations.getSize(gid);
    }

    /**
    * Get relation data within %Vector.
    * @param gid the geometry id.
    * @param idx the relation index.
    * @return geometry id for idx
    */
    template <typename CTYPE>
    cl_uint Vector<CTYPE>::getRelationData(cl_uint gid, cl_uint idx) {
        if (!relations.hasData()) {
            throw std::length_error("No relations defined");
        }
        return relations.get(gid, idx);
    }

    /**
    * Remove all coincident vertices from @Vector
    */
    template <typename CTYPE>
    void Vector<CTYPE>::deduplicateVertices() {

        // Get vertex handle
        CoordinateList<CTYPE> &vertices = pVertices->getData();

        // Check size
        if (vertices.size() == 0)
            return;

        // Create z-index list of vertices
        std::vector<std::pair<uint64_t, std::size_t> > zIndexes;
        for (std::size_t i = 0; i < vertices.size(); i++) {
            zIndexes.push_back ( { getBounds().createZIndex2D(vertices[i]), i } );
        }

        // Sort z-indexes
        std::sort(zIndexes.begin(), zIndexes.end(),
            [](const std::pair<uint64_t, std::size_t> &l, const std::pair<uint64_t, std::size_t> &r) { return l.first < r.first; });

        // Find coincident vertices
        std::vector<std::size_t> duplicateMap;
        duplicateMap.resize(vertices.size());
        auto zIndex = zIndexes.begin();
        do {
            // Set first index
            auto currentIndex = zIndex->first;
            auto currentVertex = zIndex->second;
            duplicateMap[currentVertex] = currentVertex;

            // Map all coincident vertices to first index
            while (++zIndex != zIndexes.end() && zIndex->first == currentIndex && vertices[zIndex->second] == vertices[currentVertex]) {
                duplicateMap[zIndex->second] = currentVertex;
            }

        } while (zIndex != zIndexes.end());

        // Build duplicate map and update vertices
        std::size_t newIndex = 0;
        std::map<std::size_t, std::size_t> newIndexes;
        for (std::size_t i = 0; i < vertices.size(); i++) {

            // Check for duplicate
            if (i == duplicateMap[i]) {

                // Update map
                newIndexes.insert( { i, newIndex } );

                // Update vertex
                vertices[newIndex] = vertices[i];

                // Increment index
                newIndex++;
            }
        }

        // Exit if there are no duplicates
        if (vertices.size() == newIndexes.size())
            return;

        // Resize vertices
        vertices.resize(newIndexes.size());

        // Update duplicate indexes
        for (std::size_t i = 0; i < duplicateMap.size(); i++) {
            duplicateMap[i] = newIndexes[duplicateMap[i]];
        }

        // Get geometry handle
        std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

        // Update all geometry
        for (const auto &gi : geometryIndexes) {
            auto &g = geometry[gi];

            // Process type
            if (g->isType(GeometryType::Point)) {

                // Update point vertex index
                auto &p = *std::static_pointer_cast<Point<CTYPE> >(g);
                p.pointVertex = duplicateMap[p.pointVertex];

            } else if (g->isType(GeometryType::LineString)) {

                // Update line string vertex indexes
                auto &lineString = *std::static_pointer_cast<LineString<CTYPE> >(g);
                for (std::size_t i = 0; i < lineString.lineVertices.size(); i++)
                    lineString.lineVertices[i] = duplicateMap[lineString.lineVertices[i]];

            } else if (g->isType(GeometryType::Polygon)) {

                // Update polygon vertex indexes
                auto &polygon = *std::static_pointer_cast<Polygon<CTYPE> >(g);
                for (std::size_t i = 0; i < polygon.polygonVertices.size(); i++)
                    polygon.polygonVertices[i] = duplicateMap[polygon.polygonVertices[i]];
            }
        }

    }

    /**
    * Map distance from geometry, returning a resulting raster.
    * @param resolution resolution of resulting %Raster
    * @param geometryTypes the types of geometry to include in resulting %Raster
    * @param bounds %BoundingBox of resulting %Raster, defaults to %Vector bounding box
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::mapDistance(CTYPE resolution, std::string script, size_t geometryTypes, BoundingBox<CTYPE> bounds) {

        // Create raster
        Raster<RTYPE, CTYPE> r;

        // Check resolution
        if (resolution <= 0.0) {
            throw std::runtime_error("Raster resolution must be positive");
        }

        // Check bounds
        if (bounds.extent().p == 0.0 && bounds.extent().q == 0.0) {

            // Set to vector bounds
            bounds = getBounds();

            // Calculate dimensions with 1 cell boundary
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution)+2;
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution)+2;

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p-resolution, bounds.min.q-resolution);

        } else {

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution);

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p, bounds.min.q);
        }

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.mapVector(*this, script, geometryTypes);

        return r;
    }

    /**
    * Map distance from geometry, returning a resulting raster based on the dimensions of an input Raster
    * @param rasterBase %RasterBase to use for dimensions
    * @param geometryTypes the types of geometry to include in resulting %Raster
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::mapDistance(const RasterBase<CTYPE> &rasterBase, std::string script, size_t geometryTypes) {

        // Create raster
        Raster<RTYPE, CTYPE> r;
        r.init(rasterBase.getRasterDimensions().d);

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.mapVector(*this, script, geometryTypes);

        return r;
    }

    /**
     * Rasterise from geometry, returning a resulting raster based on the dimensions of an input Raster
     * @param rasterBase %RasterBase to use for dimensions
     * @param parameters the types of geometry to include in the resulting %Raster
     * @return %Raster of %Vector
     */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::rasterise(const RasterBase<CTYPE> &rasterBase,
        std::string script, size_t parameters) {

      // Create raster
        Raster<RTYPE, CTYPE> r;
        r.init(rasterBase.getRasterDimensions().d);

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.rasterise(*this, script, parameters);

        return r;
    }

    /**
    * Rasterize vector data, returning a resulting raster.
    * @param resolution resolution of resulting %Raster
    * @param parameters the types of geometry to include in resulting %Raster
    * @param bounds %BoundingBox of resulting %Raster, defaults to %Vector bounding box
    * @return %Raster of %Vector.
    */
    template <typename CTYPE>
    template <typename RTYPE>
    Raster<RTYPE, CTYPE> Vector<CTYPE>::rasterise(CTYPE resolution, std::string script, size_t parameters, BoundingBox<CTYPE> bounds) {

        // Create raster
        Raster<RTYPE, CTYPE> r;

        // Check resolution
        if (resolution <= 0.0) {
            throw std::runtime_error("Raster resolution must be positive");
        }

        // Check bounds
        uint32_t buffer = 0;
        if (bounds.extent().p == 0.0 && bounds.extent().q == 0.0) {

            // Set to vector bounds
            bounds = getBounds();

            // Calculate dimensions with 1 cell boundary
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution)+2;
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution)+2;

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p-resolution, bounds.min.q-resolution);

        } else {

            // Calculate dimensions
            uint32_t nx = (uint32_t)ceil((bounds.max.p-bounds.min.p)/resolution);
            uint32_t ny = (uint32_t)ceil((bounds.max.q-bounds.min.q)/resolution);

            // Initialise raster
            r.init2D(nx, ny, resolution, resolution, bounds.min.p, bounds.min.q);
        }

        // Set name and projection
        r.setProperty("name", "output");
        r.setProjectionParameters(proj);

        // Map vector to raster
        r.rasterise(*this, script, parameters);

        return r;
    }

    /**
    * Sample raster at %Point locations, writing values to %Point property
    * @param r %Raster to sample
    */
    template <typename CTYPE>
    template <typename RTYPE>
    void Vector<CTYPE>::pointSample(Raster<RTYPE, CTYPE> &r) {

        // Check coordinate systems are the same
        if (r.getProjectionParameters() != proj) {
            throw std::runtime_error("Vector and raster projections must be the same for sampling");
        }

        // Check name
        auto name = r.template getProperty<std::string>("name");
        if (name.length() != 0) {

            // Get geometry handle
            std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;

            // Sample at points
            for (auto i : pointIndexes) {

                // Get handle to point
                auto c = getPointCoordinate(i);

                // Get raster value
                RTYPE v = r.getNearestValue(c.p, c.q);

                // Write values to Point
                setProperty<RTYPE>(geometry[i]->getID(), name, v);
            }
        } else {

            // Error if Raster has no name
            throw std::runtime_error("Sampled raster has no name");
        }
    }

    /**
    * Run script on only %Vector object
    * @param script OpenCL script to run.
    * @param v %Vector to run script over.
    */
    template <typename CTYPE>
    void Vector<CTYPE>::runScript(std::string script) {

        // Check for no data
        if (!hasData()) {
            return;
        }

        try {

            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            size_t clWorkgroupSize, clPaddedWorkgroupSize;
            if (solver.openCLInitialised()) {

                // Bitmask for program generation
                const uint64_t usingBounds = 0x01;

                // Create script hash
                PropertyMap &properties = *pProperties;
                std::string scriptLookup;
                for (auto n: properties.getPropertyNames()) {
                    scriptLookup += n + ", ";
                }
                scriptLookup += script;
                scriptLookup += Models::cl_vector_block_c;
                std::hash<std::string> hasher;
                auto scriptHash = hasher(scriptLookup);

                auto kernelGenerator = KernelCache::getKernelCache().getKernelGenerator(scriptHash);
                if (kernelGenerator.programHash == Solver::getNullHash()) {

                    std::string fieldArgsList;
                    std::string variableList;
                    std::string post;

                    // Check for underscores in script, these are reserved for internal variables
                    if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                        throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                    }

                    // Parse script
                    script = Solver::processScript(script);

                    // Search for fields
                    for (auto n: properties.getPropertyNames()) {
                        if (std::regex_search(script, std::regex("\\b" + n + "\\[?\\b"))) {

                            // Check property name
                            for (char &c : n) {
                                if (std::isalnum(static_cast<unsigned char>(c)) == 0 && c != '_') {
                                    throw std::runtime_error("Property name '" + n + "' must contain only alphanumeric characters or underscores");
                                }
                            }

                            // Add fields for scalar reductions
                            auto type = properties.getPropertyType(n);
                            if (type == PropertyType::Index ||
                                type == PropertyType::Integer ||
                                type == PropertyType::Float ||
                                type == PropertyType::Double) {

                                // Add index, integer and float fields
                                kernelGenerator.fieldNames.push_back(n);

                            } else if (type == PropertyType::Undefined) {

                                // Create any undefined fields
                                std::vector<CTYPE> &p = properties.template getPropertyRef<std::vector<CTYPE> >(n);
                                p.assign(getGeometryBaseCount(), getNullValue<CTYPE>());
                                kernelGenerator.fieldNames.push_back(n);

                            } else {

                                // Throw error if property cannot be mapped
                                throw std::runtime_error(std::string("Property '") + n +
                                    std::string("' cannot be used in script, this must be a integer or float type"));
                            }
                        }
                    }

                    // Search for bounds variables
                    if (std::regex_search(script, std::regex("\\bboundsMin\\b|\\bboundsMax\\b"))) {
                        kernelGenerator.programFlags |= usingBounds;
                        fieldArgsList += ",\n__global Coordinate *_gb";
                    }

                    // Add relations
                    if (relations.hasData()) {
                        fieldArgsList += ",\n__global uint *_rl";
                        fieldArgsList += ",\n__global uint *_ro";

                        script = std::regex_replace(script, std::regex("\\brelations_N\\b"), "(_rl[_index*2])");

                        // Replace loops
                        std::smatch relationMatch;
                        if (std::regex_search(script, relationMatch, std::regex("\\bfor\\s*\\(\\s*(\\w+)\\s*:\\s*relations\\s*\\).*"))) {
                            if (relationMatch.size() == 2) {

                                // Replace loop
                                std::string var = relationMatch[1].str();
                                script = std::regex_replace(script, std::regex("for\\s*\\(\\s*\\w+\\s*:\\s*relations\\s*\\)"),
                                    "for (uint _sid = _rl[_index*2+1], " + var + " = _ro[_sid]; _sid < _rl[_index*2+1]+_rl[_index*2]; _sid++, " + var + " = _ro[_sid])");

                                // Replace fields
                                for (auto &field : kernelGenerator.fieldNames) {
                                    script = std::regex_replace(script, std::regex(field + "\\s*\\[\\s*" + var + "\\s*\\]"),
                                        "(*(_" + field + " + " + var + "))");
                                }
                            }
                        }
                    }

                    // Process fields
                    for (std::size_t i = 0; i < kernelGenerator.fieldNames.size(); i++) {

                        std::string &fieldName = kernelGenerator.fieldNames[i];

                        // Get type
                        auto type = properties.getOpenCLTypeString(fieldName);

                        // Add to argument list
                        fieldArgsList += std::string(",\n__global ") + type + " *_" + fieldName;

                        // Update script for field value
                        variableList += type + " " + fieldName + " = *(_" + fieldName + "+_index);\n";
                        post += "*(_" + fieldName + "+_index) = " + fieldName + ";\n";
                    }

                    // Create script
                    auto kernelStr = Models::cl_vector_block_c;
                    kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__CODE__\\*\\/"), script);
                    kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), fieldArgsList);
                    kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__VARS__\\*\\/"), variableList);
                    kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__POST__\\*\\/"), post);
                    if (kernelGenerator.programFlags & usingBounds) {
                        kernelStr = std::regex_replace(kernelStr, std::regex("\\/\\*__BOUNDS__|__BOUNDS__\\*\\/"), std::string(""));
                    }

                    // Build kernel
                    kernelGenerator.programHash = solver.buildProgram(kernelStr);
                    if (kernelGenerator.programHash == solver.getNullHash()) {
                        throw std::runtime_error("Cannot build program");
                    }

                    // Register kernel in cache
                    KernelCache::getKernelCache().setKernelGenerator(scriptHash, kernelGenerator);
                }

                // Get kernel
                auto &vectorKernel = solver.getKernel(kernelGenerator.programHash, "vector");

                // Get geometry
                std::size_t gSize = geometryIndexes.size();

                // Set kernel arguments
                cl_uint arg = 0;
                vectorKernel.setArg(arg++, (cl_uint)gSize);

                // Create and add index buffer
                auto indexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                    gSize*sizeof(cl_uint), geometryIndexes.data());
                void *indexBufferPtr = queue.enqueueMapBuffer(indexBuffer, CL_TRUE, CL_MAP_WRITE, 0, gSize*sizeof(cl_uint));
                if (indexBufferPtr != static_cast<void *>(geometryIndexes.data()))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                queue.enqueueUnmapMemObject(indexBuffer, static_cast<void *>(geometryIndexes.data()));

                vectorKernel.setArg(arg++, indexBuffer);

                cl::Buffer relationBuffer;
                if (relations.hasData()) {

                    // Create and add relation buffer
                    std::size_t rSize = relationVector.size();
                    relationBuffer = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR,
                        rSize*sizeof(cl_uint), relationVector.data());
                    void *relationBufferPtr = queue.enqueueMapBuffer(relationBuffer, CL_TRUE, CL_MAP_WRITE, 0, rSize*sizeof(cl_uint));
                    if (relationBufferPtr != static_cast<void*>(relationVector.data()))
                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                    queue.enqueueUnmapMemObject(relationBuffer, static_cast<void*>(relationVector.data()));

                    vectorKernel.setArg(arg++, relationBuffer);
                    vectorKernel.setArg(arg++, relations.getBuffer());
                }

                // Create and add bounding box buffer
                cl::Buffer boundsBuffer;
                if (kernelGenerator.programFlags & usingBounds) {

                    // Create bounds data
                    CoordinateList<CTYPE> bounds;
                    std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
                    for (auto &g : geometryIndexes) {
                        const auto &gb = geometry[g]->getBounds();
                        bounds.push_back(gb.min);
                        bounds.push_back(gb.max);
                    }

                    // Create buffer
                    cl_int err = CL_SUCCESS;
                    auto size = bounds.size()*sizeof(Coordinate<CTYPE>);
                    boundsBuffer = cl::Buffer(context, CL_MEM_READ_ONLY, size, NULL, &err);
                    if (err != CL_SUCCESS)
                        throw std::runtime_error("Cannot initialise bounds buffer");

                    // Write data buffer
                    queue.enqueueWriteBuffer(boundsBuffer, CL_TRUE, 0, size, bounds.data());

                    // Set kernel argument
                    vectorKernel.setArg(arg++, boundsBuffer);
                }

                // Add field buffers
                std::vector<cl::Buffer> remapBuffers_REAL;
                for (auto &n : kernelGenerator.fieldNames) {
                    if (hasPropertyBuffer(n)) {

                        // Set buffer
                        vectorKernel.setArg(arg++, getPropertyBuffer(n));

                    } else {

                        // Remap data into buffer
                        auto type = properties.getPropertyType(n);
                        if (type == PropertyType::Double) {

                            // Get and convert data
                            auto data = properties.getProperty<std::vector<REAL> >(n);

                            // Create buffer
                            cl_int err = CL_SUCCESS;
                            cl::Buffer remapBuffer_REAL(context, CL_MEM_READ_ONLY, data.size()*sizeof(REAL), NULL, &err);
                            if (err != CL_SUCCESS)
                                throw std::runtime_error("Cannot initialise remapping buffer");

                            // Write buffer
                            queue.enqueueWriteBuffer(remapBuffer_REAL, CL_TRUE, 0, data.size()*sizeof(REAL), data.data());
                            remapBuffers_REAL.push_back(remapBuffer_REAL);

                            // Set buffer
                            vectorKernel.setArg(arg++, remapBuffer_REAL);
                        }
                    }
                }

                // Execute vector kernel
                clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(vectorKernel);
                clPaddedWorkgroupSize = (size_t)(gSize+((clWorkgroupSize-(gSize%clWorkgroupSize))%clWorkgroupSize));
                queue.enqueueNDRangeKernel(vectorKernel, cl::NullRange,
                    cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                // Unmap buffers
                indexBufferPtr = queue.enqueueMapBuffer(indexBuffer, CL_TRUE, CL_MAP_READ, 0, gSize*sizeof(cl_uint));
                if (indexBufferPtr != static_cast<void *>(geometryIndexes.data()))
                    throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                // Apply filter
                geometryIndexes.erase(std::stable_partition(geometryIndexes.begin(), geometryIndexes.end(),
                    [](cl_uint v) {return v != getNullValue<cl_uint>();} ), std::end(geometryIndexes));
                if (gSize != geometryIndexes.size()) {

                    // Clear indexes
                    tree.clear();
                    pointIndexes.clear();
                    lineStringIndexes.clear();
                    polygonIndexes.clear();

                    // Rebuild
                    std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
                    for (auto g : geometryIndexes) {

                        // Update tree
                        tree.insert(geometry[g]);

                        // Update geometry index
                        geometry[g]->updateVector(*this, g);
                    }
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }


        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }

    }

    /**
    * Run script on %Vector with %Raster layers
    * @param script OpenCL script to run.
    * @param v %Vector to run script over.
    * @param rasterBaseRefs list of input raster references.
    * @param reductionType reduction type to apply to the Rasters.
    * @param parameters parameter flags for combination and aliasing.
    */
    template <typename RTYPE, typename CTYPE>
    void runVectorScript(
        std::string script,
        Vector<CTYPE> &v,
        std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs,
        Reduction::Type reductionType,
        size_t parameters) {

        // Check for no data
        if (!v.hasData()) {
            return;
        }

        // Get parameters
        bool ordered = static_cast<VectorOrdering::Type>(parameters & 0x01) == VectorOrdering::Ordered;
        bool useLayers = static_cast<VectorLayerHandling::Type>(parameters & 0x02) == VectorLayerHandling::ByLayer;
        cl_uchar index_parameters = 0;
        if (static_cast<VectorIndexingOptions::Type>(parameters & 0x300) == VectorIndexingOptions::All) {
            index_parameters = 3;
        } else if (static_cast<VectorIndexingOptions::Type>(parameters & 0x300) == VectorIndexingOptions::Interior) {
            index_parameters = 1;
        } else if (static_cast<VectorIndexingOptions::Type>(parameters & 0x300) == VectorIndexingOptions::Edges) {
            index_parameters = 2;
        }

        try {

            // Get OpenCL handles
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            auto &context = solver.getContext();
            auto &queue = solver.getQueue();
            uint8_t verbose = solver.getVerboseLevel();

            if (verbose <= Verbosity::Info) {
                std::stringstream parameter_choices;
                parameter_choices << "ordered: " << ordered << "\n";
                parameter_choices << "useLayers: " << useLayers << "\n";
                parameter_choices << "index_parameter: " << (int)index_parameters << "\n";
                std::cout << parameter_choices.str() << std::endl;
            }

            size_t clWorkgroupSize, clPaddedWorkgroupSize;
            if (solver.openCLInitialised()) {

                // Run script
                if (rasterBaseRefs.size() > 0) {

                    // Check for underscores in script, these are reserved for internal variables
                    if (std::regex_search(script, std::regex("\\b_\\w*"))) {
                        throw std::runtime_error("Kernel code cannot contain leading underscores '_' in script");
                    }

                    // Get dimensions
                    RasterBase<CTYPE> &rasterBaseFirst = rasterBaseRefs[0].get();
                    auto dimFirst = rasterBaseFirst.getRasterDimensions();
                    uint32_t nz = dimFirst.d.nz;

                    // Script strings
                    std::string fieldArgsList;
                    std::string variableList;
                    std::string post;

                    // Search for fields
                    std::vector<std::string> fields;
                    auto &properties = v.getProperties();
                    for (auto n: properties.getPropertyNames()) {
                        if (std::regex_search(script, std::regex("\\b" + n + "\\b"))) {

                            // Check property name
                            for (char &c : n) {
                                if (std::isalnum(static_cast<unsigned char>(c)) == 0 && c != '_') {
                                    throw std::runtime_error("Property name '" + n + "' must contain only alphanumeric characters or underscores");
                                }
                            }

                            // Add fields for scalar reductions
                            auto type = properties.getPropertyType(n);
                            if (reductionType != Reduction::None) {

                                if (type == PropertyBase::getPropertyType<std::vector<RTYPE> >()) {

                                    // Add index, integer and float fields
                                    fields.push_back(n);

                                } else if (type == PropertyType::Undefined) {

                                    // Create any undefined fields
                                    if (useLayers && nz > 1) {

                                        // Create vector field
                                        v.template setProperty<std::vector<RTYPE> >(0, n, std::vector<RTYPE>());

                                        // Create vectors
                                        auto &pv = v.template getPropertyVectorsRef<std::vector<std::vector<RTYPE> > >(n);
                                        for (auto &v : pv) {
                                            v.assign(nz, getNullValue<RTYPE>());
                                        }

                                    } else {

                                        // Create scalar field
                                        v.template setProperty<RTYPE>(0, n, 0.0);
                                    }
                                    fields.push_back(n);

                                } else {

                                    // Throw error if property cannot be mapped
                                    throw std::runtime_error(std::string("Property '") + n +
                                        std::string("' cannot be used in script, this must a float type"));
                                }
                            } else {

                                if (type == PropertyType::Undefined) {

                                    // Add to field list
                                    fields.push_back(n);

                                    // Redefine property as vector
                                    v.template setProperty<std::vector<RTYPE> >(0, n, std::vector<RTYPE>());

                                } else if (type == PropertyBase::getPropertyType<std::vector<std::vector<RTYPE> > >()) {

                                    // Clear properties
                                    auto &pv = v.template getPropertyVectorsRef<std::vector<std::vector<RTYPE> > >(n);
                                    for (auto &v : pv) {
                                        v.clear();
                                    }

                                    // Add to field list
                                    fields.push_back(n);

                                } else {
                                    throw std::runtime_error("Cannot map non floating-point property type to floating-point property vector");
                                }
                            }
                        }
                    }

                    for (std::size_t i = 0; i < fields.size(); i++) {

                        std::string &fieldName = fields[i];

                        // Add to argument list
                        fieldArgsList += ",\n__global REAL *_" + fieldName;

                        // Update script for field value
                        variableList += "REAL " + fieldName + " = *(_" + fieldName + "+_index);\n";
                        post += "*(_" + fieldName + "+_index*_layers+_k) = " + fieldName + ";\n";
                    }

                    // Turn off all raster writes
                    std::vector<bool> rasterBaseNeedsWrite;
                    for (std::size_t i = 0; i < rasterBaseRefs.size(); i++) {
                        rasterBaseNeedsWrite.push_back(rasterBaseRefs[i].get().getNeedsWrite());
                        rasterBaseRefs[i].get().setNeedsWrite(false);
                    }

                    // Generate raster kernel script
                    auto rasterIndexedGenerator = Geostack::generateKernel<CTYPE>(script, Models::cl_raster_indexed_block_c, rasterBaseRefs);
                    if (rasterIndexedGenerator.script.size() == 0) {
                        throw std::runtime_error("Cannot generate script");
                    }

                    // Restore raster writes
                    for (std::size_t i = 0; i < rasterBaseRefs.size(); i++) {
                        rasterBaseRefs[i].get().setNeedsWrite(rasterBaseNeedsWrite[i]);
                    }

                    // Patch raster kernel template with additional code
                    fieldArgsList += ",";
                    rasterIndexedGenerator.script = std::regex_replace(rasterIndexedGenerator.script, std::regex("\\/\\*__ARGS2__\\*\\/"), fieldArgsList);
                    rasterIndexedGenerator.script = std::regex_replace(rasterIndexedGenerator.script, std::regex("\\/\\*__VARS2__\\*\\/"), variableList);
                    rasterIndexedGenerator.script = std::regex_replace(rasterIndexedGenerator.script, std::regex("\\/\\*__POST2__\\*\\/"), post);

                    // Build raster kernel
                    std::size_t rasterIndexedHash = buildRasterKernel(rasterIndexedGenerator);
                    if (rasterIndexedHash == solver.getNullHash()) {
                        throw std::runtime_error("Cannot get kernel for script");
                    }
                    auto &rasterIndexedKernel = solver.getKernel(rasterIndexedHash, "rasterIndexed");

                    // Create indexing kernel code
                    std::string indexKernelStr;
                    bool usingProjection = (rasterBaseFirst.getProjectionParameters() != v.getProjectionParameters());
                    if (usingProjection) {

                        // Update projection definitions
                        auto projStr = Models::cl_projection_head_c;
                        auto rproj = rasterBaseFirst.getProjectionParameters();
                        auto vproj = v.getProjectionParameters();
                        if (rproj.type == 1) {
                            std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(rproj.cttype) + "\n";
                            projStr = projDef + projStr;
                        }
                        if (vproj.type == 1) {
                            std::string projDef = "#define USE_PROJ_TYPE_" + std::to_string(vproj.cttype) + "\n";
                            projStr = projDef + projStr;
                        }
                        indexKernelStr = projStr+Models::cl_index_c;

                    } else {
                        indexKernelStr = Models::cl_index_c;
                    }
                    indexKernelStr = std::regex_replace(indexKernelStr, std::regex("TYPE"), Solver::getOpenCLTypeString<RTYPE>());

                    // Create indexing argument list
                    std::string indexingArgsList;
                    if (usingProjection) {
                        indexingArgsList += ",\nconst ProjectionParameters _rproj";
                        indexingArgsList += ",\nconst ProjectionParameters _vproj";
                    }

                    // Patch indexing script
                    indexKernelStr = std::regex_replace(indexKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), indexingArgsList);
                    if (usingProjection) {
                        indexKernelStr = std::regex_replace(indexKernelStr, std::regex("\\/\\*__PROJECT__|__PROJECT__\\*\\/"), std::string(""));
                    }

                    // Build indexing kernel
                    std::size_t indexingHash = solver.buildProgram(indexKernelStr);
                    if (indexingHash == solver.getNullHash()) {
                        throw std::runtime_error("Cannot build program");
                    }

                    // Create reduction variables
                    auto geometrySize = v.getGeometryIndexes().size();
                    std::vector<std::vector<std::vector<RTYPE> > > fieldValuesReduce;
                    std::vector<std::vector<std::vector<RTYPE> > > fieldValuesCount;

                    if (reductionType != Reduction::None) {

                        // Initialise layers
                        fieldValuesReduce.resize(useLayers ? nz : 1);
                        fieldValuesCount.resize(useLayers ? nz : 1);

                        // Loop over layers
                        for (std::size_t l = 0; l < (useLayers ? nz : 1); l++) {

                            // Initialise reduction
                            fieldValuesReduce[l].resize(fields.size());
                            fieldValuesCount[l].resize(fields.size());

                            // Loop over fields
                            for (std::size_t f = 0; f < fields.size(); f++) {

                                // Create reduction vector
                                fieldValuesReduce[l][f].resize(geometrySize);
                                if (reductionType == Reduction::Mean || reductionType == Reduction::Count)
                                    fieldValuesCount[l][f].resize(geometrySize);

                                // Initialise reduction vector
                                switch (reductionType) {

                                    case(Reduction::Maximum):
                                        std::fill(fieldValuesReduce[l][f].begin(), fieldValuesReduce[l][f].end(), getNullValue<RTYPE>()); // TODO update for other types
                                        break;

                                    case(Reduction::Minimum):
                                        std::fill(fieldValuesReduce[l][f].begin(), fieldValuesReduce[l][f].end(), getNullValue<RTYPE>()); // TODO update for other types
                                        break;

                                    case(Reduction::Sum):
                                    case(Reduction::SumSquares):
                                    case(Reduction::Mean):
                                        std::fill(fieldValuesReduce[l][f].begin(), fieldValuesReduce[l][f].end(), getNullValue<RTYPE>());
                                        std::fill(fieldValuesCount[l][f].begin(), fieldValuesCount[l][f].end(), 0.0);
                                        break;

                                    case(Reduction::Count):
                                        std::fill(fieldValuesCount[l][f].begin(), fieldValuesCount[l][f].end(), 0.0);
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                    }

                    // Create field vectors
                    std::vector<std::vector<RTYPE> > fieldValues;
                    std::vector<cl::Buffer > fieldBuffers;
                    fieldValues.resize(fields.size());
                    fieldBuffers.resize(fields.size());

                    // Loop over tiles
                    for (uint32_t tj = 0; tj < dimFirst.ty; tj++) {
                        for (uint32_t ti = 0; ti < dimFirst.tx; ti++) {

                            // Get indexes
                            auto tileVectorIndexes = rasterBaseFirst.getVectorTileIndexes(v, ti, tj, indexingHash, index_parameters);
                            auto iSize = tileVectorIndexes.size();

                            // Check for cells
                            if (iSize > 0) {

                                // Create array index buffer
                                AutoBuffer<RasterIndex<CTYPE> > tileIndexBuffer(context, queue, tileVectorIndexes);

                                // Set kernel arguments
                                cl_uint arg = 0;
                                cl_uint tiSize = iSize*nz;
                                rasterIndexedKernel.setArg(arg++, tileIndexBuffer.getBuffer());
                                rasterIndexedKernel.setArg(arg++, (cl_uint)iSize);

                                // Set field kernel arguments
                                for (std::size_t f = 0; f < fields.size(); f++) {

                                    // Create and populate values
                                    fieldValues[f].resize(tiSize);
                                    if (reductionType != Reduction::None) {
                                        for (std::size_t j = 0; j < iSize; j++) {

                                            auto type = properties.getPropertyType(fields[f]);
                                            if (type == PropertyBase::getPropertyType<std::vector<std::vector<RTYPE> > >()) {

                                                // Populate vector fields
                                                auto &pv = v.template getPropertyVectorRef<std::vector<RTYPE> >(tileVectorIndexes[j].id, fields[f]);
                                                if (pv.size() != nz) {
                                                    throw std::runtime_error("Property vector length must be equal to the number of layers");
                                                }
                                                for (std::size_t l = 0; l < nz; l++) {
                                                    fieldValues[f][j*nz+l] = pv[l];
                                                }

                                            } else {

                                                // Populate scalar fields
                                                auto fv = v.template getProperty<RTYPE>(tileVectorIndexes[j].id, fields[f]);
                                                for (std::size_t l = 0; l < nz; l++) {
                                                    fieldValues[f][j*nz+l] = fv;
                                                }
                                            }
                                        }
                                    }

                                    // Map memory
                                    fieldBuffers[f] = cl::Buffer(context, CL_MEM_READ_WRITE | CL_MEM_USE_HOST_PTR, tiSize*sizeof(RTYPE), fieldValues[f].data());
                                    void *fieldBufferPtr = queue.enqueueMapBuffer(fieldBuffers[f], CL_TRUE, CL_MAP_WRITE, 0, tiSize*sizeof(RTYPE));
                                    if (fieldBufferPtr != static_cast<void *>(fieldValues[f].data()))
                                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
                                    queue.enqueueUnmapMemObject(fieldBuffers[f], static_cast<void *>(fieldValues[f].data()));

                                    // Set buffer
                                    rasterIndexedKernel.setArg(arg++, fieldBuffers[f]);
                                }

                                // Set remaining arguments
                                Geostack::setTileKernelArguments<CTYPE>(ti, tj, rasterIndexedKernel, rasterBaseRefs, rasterIndexedGenerator.reqs, arg);

                                // Execute vector kernel over indexes in one dimension and layers in second dimension
                                clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(rasterIndexedKernel);
                                clPaddedWorkgroupSize = (size_t)(iSize+((clWorkgroupSize-(iSize%clWorkgroupSize))%clWorkgroupSize));
                                queue.enqueueNDRangeKernel(rasterIndexedKernel, cl::NullRange,
                                    cl::NDRange(clPaddedWorkgroupSize, nz), cl::NDRange(clWorkgroupSize, 1));

                                // Unmap data
                                for (std::size_t f = 0; f < fields.size(); f++) {
                                    void *fieldBufferPtr = queue.enqueueMapBuffer(fieldBuffers[f], CL_TRUE, CL_MAP_READ, 0, tiSize*sizeof(RTYPE));
                                    if (fieldBufferPtr != static_cast<void *>(fieldValues[f].data()))
                                        throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

                                    if (reductionType != Reduction::None) {

                                        // Calculate reduction
                                        switch (reductionType) {

                                            case(Reduction::Maximum):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    for (std::size_t l = 0; l < nz; l++) {
                                                        fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] =
                                                            fmax(fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id], fieldValues[f][j*nz+l]);
                                                    }
                                                }
                                                break;

                                            case(Reduction::Minimum):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    for (std::size_t l = 0; l < nz; l++) {
                                                        fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] =
                                                            fmin(fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id], fieldValues[f][j*nz+l]);
                                                    }
                                                }
                                                break;

                                            case(Reduction::Sum):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    for (std::size_t l = 0; l < nz; l++) {
                                                        auto fv = fieldValues[f][j*nz+l];
                                                        if (isValid<RTYPE>(fv)) {
                                                            auto v = fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id];
                                                            if (isValid<RTYPE>(v)) {
                                                                fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] += fv;
                                                            } else {
                                                                fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] = fv;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;

                                            case(Reduction::SumSquares):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    for (std::size_t l = 0; l < nz; l++) {
                                                        auto fv = fieldValues[f][j*nz+l];
                                                        if (isValid<RTYPE>(fv)) {
                                                            auto v = fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id];
                                                            if (isValid<RTYPE>(v)) {
                                                                fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] += fv*fv;
                                                            } else {
                                                                fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] = fv*fv;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;

                                            case(Reduction::Mean):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    for (std::size_t l = 0; l < nz; l++) {
                                                        auto fv = fieldValues[f][j*nz+l];
                                                        if (isValid<RTYPE>(fv)) {
                                                            auto v = fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id];
                                                            if (isValid<RTYPE>(v)) {
                                                                fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] += fv;
                                                                fieldValuesCount[useLayers ? l : 0][f][tileVectorIndexes[j].id] += 1.0;
                                                            } else {
                                                                fieldValuesReduce[useLayers ? l : 0][f][tileVectorIndexes[j].id] = fv;
                                                                fieldValuesCount[useLayers ? l : 0][f][tileVectorIndexes[j].id] = 1.0;
                                                            }
                                                        }
                                                    }
                                                }
                                                break;

                                            case(Reduction::Count):
                                                for (std::size_t j = 0; j < iSize; j++) {
                                                    for (std::size_t l = 0; l < nz; l++) {
                                                        auto fv = fieldValues[f][j*nz+l];
                                                        if (isValid<RTYPE>(fv)) {
                                                            fieldValuesCount[useLayers ? l : 0][f][tileVectorIndexes[j].id] += 1.0;
                                                        }
                                                    }
                                                }
                                                break;
                                            
                                            default:
                                                break;
                                        }
                                    } else {

                                        // Append to properties
                                        auto &pv = v.template getPropertyVectorsRef<std::vector<std::vector<RTYPE> > >(fields[f]);
                                        for (std::size_t j = 0; j < iSize; j++) {
                                            for (std::size_t l = 0; l < nz; l++) {
                                                pv[tileVectorIndexes[j].id].push_back(fieldValues[f][j*nz+l]);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        // Output progress
                        if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                            std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dimFirst.ty << "%" << std::endl;
                        }
                    }

                    if (reductionType != Reduction::None) {

                        // Write reduction values to vector
                        for (std::size_t l = 0; l < (useLayers ? nz : 1); l++) {
                            for (std::size_t f = 0; f < fields.size(); f++) {
                                for (std::size_t i = 0; i < geometrySize; i++) {

                                    // Apply final reduction step
                                    RTYPE val = getNullValue<RTYPE>();
                                    if (reductionType == Reduction::Count) {

                                        // Set count
                                        val = fieldValuesCount[l][f][i];

                                    } else {

                                        // Apply reduction
                                        val = fieldValuesReduce[l][f][i];
                                        if (reductionType == Reduction::Mean) {
                                            if (fieldValuesCount[l][f][i] > 0.0) {
                                                val/=fieldValuesCount[l][f][i];
                                            }
                                        }
                                    }

                                    // Set value
                                    auto type = properties.getPropertyType(fields[f]);
                                    if (type == PropertyBase::getPropertyType<std::vector<std::vector<RTYPE> > >()) {

                                        // Set vector property
                                        auto &pv = v.template getPropertyVectorRef<std::vector<RTYPE> >(i, fields[f]);
                                        pv[l] = val;

                                    } else {

                                        // Set scalar property
                                        v.template setProperty<RTYPE>(i, fields[f], val);
                                    }
                                }
                            }
                        }

                    } else if (ordered) {

                        // Sort data
                        for (std::size_t f = 0; f < fields.size(); f++) {
                            auto &pv = v.template getPropertyVectorsRef<std::vector<std::vector<RTYPE> > >(fields[f]);
                            for (auto &pvv : pv) {

                                // Remove nodata
                                pvv.erase(std::remove_if(pvv.begin(), pvv.end(), [](const RTYPE &v) { return v != v; } ), pvv.end());

                                // Sort vector
                                std::sort(pvv.begin(), pvv.end());
                            }
                        }

                    }

                    if (verbose <= Geostack::Verbosity::Info) {
                        std::cout << "100%" << std::endl;
                    }

                } else {

                    // Run vector script only
                    v.runScript(script);
                }

            } else {
                throw std::runtime_error("OpenCL not initialised");
            }

        } catch (cl::Error e) {
            std::stringstream err;
            err << "OpenCL exception '" << e.what() << "': " << e.err();
            throw std::runtime_error(err.str());
        }
    }

    // Float type definitions
    template class VectorGeometry<float>;
    template class Point<float>;
    template class LineString<float>;
    template class Polygon<float>;
    template class Vector<float>;

    template void Vector<float>::pointSample<float>(Raster<float, float> &);
    template Raster<float, float> Vector<float>::mapDistance<float>(float, std::string, std::size_t, BoundingBox<float>);
    template Raster<float, float> Vector<float>::mapDistance<float>(const RasterBase<float> &, std::string, std::size_t);
    template Raster<float, float> Vector<float>::rasterise<float>(float, std::string, std::size_t, BoundingBox<float>);
    template Raster<float, float> Vector<float>::rasterise<float>(const RasterBase<float> &, std::string, std::size_t);
    template Raster<uint32_t, float> Vector<float>::rasterise<uint32_t>(float, std::string, std::size_t, BoundingBox<float>);
    template Raster<uint32_t, float> Vector<float>::rasterise<uint32_t>(const RasterBase<float> &, std::string, std::size_t);
    template Raster<uint8_t, float> Vector<float>::rasterise<uint8_t>(float, std::string, std::size_t, BoundingBox<float>);
    template Raster<uint8_t, float> Vector<float>::rasterise<uint8_t>(const RasterBase<float> &, std::string, std::size_t);
    template void runVectorScript<float, float>(std::string, Vector<float> &, std::vector<std::reference_wrapper<RasterBase<float> > >, Reduction::Type, size_t);

    template void Vector<float>::setGlobalProperty<int>(std::string, int);
    template void Vector<float>::setGlobalProperty<std::string>(std::string, std::string);

    template void Vector<float>::setProperty<std::string>(cl_uint, std::string, std::string);
    template void Vector<float>::setProperty<int>(cl_uint, std::string, int);
    template void Vector<float>::setProperty<float>(cl_uint, std::string, float);
    template void Vector<float>::setProperty<cl_uint>(cl_uint, std::string, cl_uint);
    template void Vector<float>::setProperty<cl_uchar>(cl_uint, std::string, cl_uchar);
    template void Vector<float>::setProperty<std::vector<float> >(cl_uint, std::string, std::vector<float> v);

    template void Vector<float>::setPropertyVector<std::vector<std::string> >(std::string, std::vector<std::string>);
    template void Vector<float>::setPropertyVector<std::vector<int> >(std::string, std::vector<int>);
    template void Vector<float>::setPropertyVector<std::vector<float> >(std::string, std::vector<float>);
    template void Vector<float>::setPropertyVector<std::vector<cl_uint> >(std::string, std::vector<cl_uint>);
    template void Vector<float>::setPropertyVector<std::vector<cl_uchar> >(std::string, std::vector<cl_uchar>);

    template int Vector<float>::getGlobalProperty<int>(std::string) const;
    template std::string Vector<float>::getGlobalProperty<std::string>(std::string) const;

    template std::string Vector<float>::getProperty<std::string>(cl_uint, std::string) const;
    template int Vector<float>::getProperty<int>(cl_uint, std::string) const;
    template float Vector<float>::getProperty<float>(cl_uint, std::string) const;
    template cl_uint Vector<float>::getProperty<cl_uint>(cl_uint, std::string) const;
    template cl_uchar Vector<float>::getProperty<cl_uchar>(cl_uint, std::string) const;
    template std::vector<float> Vector<float>::getProperty<std::vector<float> >(cl_uint, std::string) const;

    template std::vector<std::vector<float> > &Vector<float>::getPropertyVectorsRef<std::vector<std::vector<float> > >(std::string);
    template std::vector<float> &Vector<float>::getPropertyVectorRef<std::vector<float> >(cl_uint, std::string);

    template void Vector<float>::convertProperty<std::string>(std::string);
    template void Vector<float>::convertProperty<int>(std::string);
    template void Vector<float>::convertProperty<float>(std::string);
    template void Vector<float>::convertProperty<cl_uint>(std::string);
    template void Vector<float>::convertProperty<cl_uchar>(std::string);

    // Double type definitions
    template class VectorGeometry<double>;
    template class Point<double>;
    template class LineString<double>;
    template class Polygon<double>;
    template class Vector<double>;

    template void Vector<double>::pointSample<double>(Raster<double, double> &);
    template Raster<double, double> Vector<double>::mapDistance<double>(double, std::string, std::size_t, BoundingBox<double>);
    template Raster<double, double> Vector<double>::mapDistance<double>(const RasterBase<double> &, std::string, std::size_t);
    template Raster<double, double> Vector<double>::rasterise<double>(double, std::string, std::size_t, BoundingBox<double>);
    template Raster<double, double> Vector<double>::rasterise<double>(const RasterBase<double> &, std::string, std::size_t);
    template Raster<uint32_t, double> Vector<double>::rasterise<uint32_t>(double, std::string, std::size_t, BoundingBox<double>);
    template Raster<uint32_t, double> Vector<double>::rasterise<uint32_t>(const RasterBase<double> &, std::string, std::size_t);
    template Raster<uint8_t, double> Vector<double>::rasterise<uint8_t>(double, std::string, std::size_t, BoundingBox<double>);
    template Raster<uint8_t, double> Vector<double>::rasterise<uint8_t>(const RasterBase<double> &, std::string, std::size_t);
    template void runVectorScript<double, double>(std::string, Vector<double> &, std::vector<std::reference_wrapper<RasterBase<double> > >, Reduction::Type, size_t);

    template void Vector<double>::setGlobalProperty<int>(std::string, int);
    template void Vector<double>::setGlobalProperty<std::string>(std::string, std::string);

    template void Vector<double>::setProperty<std::string>(cl_uint, std::string, std::string);
    template void Vector<double>::setProperty<int>(cl_uint, std::string, int);
    template void Vector<double>::setProperty<double>(cl_uint, std::string, double);
    template void Vector<double>::setProperty<cl_uint>(cl_uint, std::string, cl_uint);
    template void Vector<double>::setProperty<cl_uchar>(cl_uint, std::string, cl_uchar);
    template void Vector<double>::setProperty<std::vector<double> >(cl_uint, std::string, std::vector<double> v);

    template void Vector<double>::setPropertyVector<std::vector<std::string> >(std::string, std::vector<std::string>);
    template void Vector<double>::setPropertyVector<std::vector<int> >(std::string, std::vector<int>);
    template void Vector<double>::setPropertyVector<std::vector<double> >(std::string, std::vector<double>);
    template void Vector<double>::setPropertyVector<std::vector<cl_uint> >(std::string, std::vector<cl_uint>);
    template void Vector<double>::setPropertyVector<std::vector<cl_uchar> >(std::string, std::vector<cl_uchar>);

    template int Vector<double>::getGlobalProperty<int>(std::string) const;
    template std::string Vector<double>::getGlobalProperty<std::string>(std::string) const;

    template std::string Vector<double>::getProperty<std::string>(cl_uint, std::string) const;
    template int Vector<double>::getProperty<int>(cl_uint, std::string) const;
    template double Vector<double>::getProperty<double>(cl_uint, std::string) const;
    template cl_uint Vector<double>::getProperty<cl_uint>(cl_uint, std::string) const;
    template cl_uchar Vector<double>::getProperty<cl_uchar>(cl_uint, std::string) const;
    template std::vector<double> Vector<double>::getProperty<std::vector<double> >(cl_uint, std::string) const;

    template std::vector<std::vector<double> > &Vector<double>::getPropertyVectorsRef<std::vector<std::vector<double> > >(std::string);
    template std::vector<double> &Vector<double>::getPropertyVectorRef<std::vector<double> >(cl_uint, std::string);

    template void Vector<double>::convertProperty<std::string>(std::string);
    template void Vector<double>::convertProperty<int>(std::string);
    template void Vector<double>::convertProperty<double>(std::string);
    template void Vector<double>::convertProperty<cl_uint>(std::string);
    template void Vector<double>::convertProperty<cl_uchar>(std::string);
}
