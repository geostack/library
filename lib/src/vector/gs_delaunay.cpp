/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <cmath>

#include "gs_delaunay.h"
#include "gs_robust.h"
#include "gs_geometry.h"

namespace Geostack {    

    // Forward declarations
    template <typename T>
    class Edge;

    template <typename T>
    class TriangleEdges;

    template <typename T>
    using EdgeRef = typename std::list<Edge<T> >::iterator;

    template <typename T>
    using TriangleEdgeRef = typename std::list<TriangleEdges<T> >::iterator;

    /**
    * %Edge class holding two %Coordinates
    */
    template <typename T>
    class Edge {
    
    public:

        // Edge coordinates
        std::size_t c0, c1;

        // Constructors
        Edge(const std::size_t &c0_, const std::size_t &c1_): 
            c0(c0_), c1(c1_), has_t0(false), has_t1(false) {}

        void set_t0(const TriangleEdgeRef<T> &t0_) {
            t0 = t0_;
            has_t0 = true;
        }

        void set_t1(const TriangleEdgeRef<T> &t1_) {
            t1 = t1_;
            has_t1 = true;
        }

        TriangleEdgeRef<T> &get_t0() {
            if (has_t0) {
                return t0;
            }
            throw std::runtime_error("Triangle 0 not set");
        }

        TriangleEdgeRef<T> &get_t1() {
            if (has_t1) {
                return t1;
            }
            throw std::runtime_error("Triangle 1 not set");
        }

        bool get_has_t0() {
            return has_t0;
        }

        bool get_has_t1() {
            return has_t1;
        }

        void clear_t0() {
            has_t0 = false;
        }

        void clear_t1() {
            has_t1 = false;
        }

    private:
    
        // Iterators and flags
        TriangleEdgeRef<T> t0, t1;
        bool has_t0, has_t1;
    };
    
    /**
    * %TriangleEdges class holding three %Edges
    */
    template <typename T>
    class TriangleEdges {
    
    public:

        // Triangle edges and flag for flipped edges
        bool flip[3];
        EdgeRef<T> e[3];
        typename std::multimap<uint64_t, TriangleEdgeRef<T> >::iterator hit;

        // Triangle polygon identification
        uint32_t id;
        
        // Constructor
        TriangleEdges(const EdgeRef<T> &e0_, const EdgeRef<T> &e1_, const EdgeRef<T> &e2_,
            bool flip0_ = false, bool flip1_ = false, bool flip2_ = false) {

            // Set edges
            e[0] = e0_;
            e[1] = e1_;
            e[2] = e2_;

            // Set edge flip flag
            flip[0] = flip0_;
            flip[1] = flip1_;
            flip[2] = flip2_;

            // Set id
            id = 0;
        }

        std::size_t get_c0() const { return flip[0] ? e[0]->c1 : e[0]->c0; }
        std::size_t get_c1() const { return flip[1] ? e[1]->c1 : e[1]->c0; }
        std::size_t get_c2() const { return flip[2] ? e[2]->c1 : e[2]->c0; }
    };

    // Symmetrical pairing function
    uint64_t pairing(uint64_t x, uint64_t y) {
        return x < y ? (y<<32|(x&0xFFFFFFFF)) : (x<<32|(y&0xFFFFFFFF));
    }

    /**
    * Update hash map by inserting triangle
    * @param triangleHash Hash map.
    * @param bounds %BoundingBox of domain.
    * @param triangleEdge %TriangleEdgeRef to insert.
    */
    template<typename T>
    void updateTriangleHash(const CoordinateList<T> &coords, std::multimap<uint64_t, TriangleEdgeRef<T> > &triangleHash, 
        BoundingBox<T> &bounds, TriangleEdgeRef<T> triangleEdge) {

        // Get centroid of triangle
        auto &tc0 = coords[triangleEdge->get_c0()];
        auto &tc1 = coords[triangleEdge->get_c1()];
        auto &tc2 = coords[triangleEdge->get_c2()];

        auto centroid = Coordinate<T>( {
            (tc0.p+tc1.p+tc2.p)/(T)3.0,
            (tc0.q+tc1.q+tc2.q)/(T)3.0
        } );

        // Create hash
        uint64_t hash = bounds.createZIndex2D(centroid);

        // Update triangles
        triangleEdge->hit = triangleHash.insert(std::make_pair(hash, triangleEdge));
    }

    /**
    * Find triangle containing coordinate
    * @param c %Coordinate to find.
    * @param coords List of all %Coordinates.
    * @param bounds %BoundingBox of domain.
    * @param triangleEdges List of %TriangleEdges.
    * @return iterator to %TriangleEdges.
    */
    template<typename T>
    TriangleEdgeRef<T> seek(Coordinate<T> &c, const CoordinateList<T> &coords, BoundingBox<T> &bounds,
        std::list<TriangleEdges<T> > &triangleEdges, std::multimap<uint64_t, TriangleEdgeRef<T> > &triangleHash) {

        // Search for approximate starting point
        TriangleEdgeRef<T> tSearch = triangleEdges.begin();
        uint64_t hash = bounds.createZIndex2D(c);        
        auto near = triangleHash.lower_bound(hash);
        if (near != triangleHash.end()) {
            tSearch = near->second;
        }

        // Search for starting triangle
        auto tLast = tSearch;
        do {
            tLast = tSearch;
            for (std::size_t i = 0; i < 3; i++) {
            
                Coordinate<T> c0, c1;
                EdgeRef<T> &e = tSearch->e[i];
                if (tSearch->flip[i]) {
                    c0 = coords[e->c0];
                    c1 = coords[e->c1];
                } else {
                    c0 = coords[e->c1];
                    c1 = coords[e->c0];
                }
                if (Geostack::Robust<T>::orient2d(c0, c1, { c.p, c.q } ) < 0.0) {
                    tSearch = e->get_t0() == tSearch ? e->get_t1() : e->get_t0();
                    break;
                }
            }
        } while(tLast != tSearch);
        return tSearch;
    }

    /**
    * Delaunay triangulation.
    * Performs triangulation of a set of %Coordinates using Bower-Watson method.
    * @param coords %Coordinates to triangulate.
    * @return list of %Triangles.
    */
    template<typename T>
    Vector<T> Delaunay<T>::triangulate(CoordinateList<T> coords, const std::vector<uint64_t> edgeIndexes, std::size_t geometryType) {
        
        // Create geometry lists
        std::list<Edge<T> > edges;
        std::list<std::pair<EdgeRef<T>, bool> > holeEdges;
        std::list<TriangleEdges<T> > triangleEdges;
        std::list<TriangleEdgeRef<T> > candidates;
        std::multimap<uint64_t, TriangleEdgeRef<T> > triangleHash;

        // Check size
        if (coords.size() < 3) {
            return Vector<T>();
        }

        // Find bounds
        T xmin = coords[0].p;
        T xmax = xmin;
        T ymin = coords[0].q;
        T ymax = ymin;
        for (auto const &c : coords) {
            xmin = std::min(xmin, c.p);
            xmax = std::max(xmax, c.p);
            ymin = std::min(ymin, c.q);
            ymax = std::max(ymax, c.q);
        }
        T delta = std::max(xmax-xmin, ymax-ymin);

        // Transform to work on delta positions only
        for (auto &c : coords) {
            c.p = (c.p-xmin)/delta;
            c.q = (c.q-ymin)/delta;
        }

        // Reserve first four coordinates for bounding box
        std::size_t reserveIndex = 0;
        if (coords.size() > 3) {
            coords.push_back(coords[0]);
            coords.push_back(coords[1]);
            coords.push_back(coords[2]);
            coords.push_back(coords[3]);
            reserveIndex = coords.size()-4;
        } else {
            coords.push_back(coords[0]);
            coords.push_back(coords[0]);
            coords.push_back(coords[1]);
            coords.push_back(coords[2]);
            reserveIndex = coords.size()-3;
        }

        // Create bounding box
        auto bc0 = Coordinate<T>{ -0.5, -0.5 };
        auto bc1 = Coordinate<T>{ -0.5,  1.5 };
        auto bc2 = Coordinate<T>{  1.5,  1.5 };
        auto bc3 = Coordinate<T>{  1.5, -0.5 };
        coords[0] = bc0;
        coords[1] = bc1;
        coords[2] = bc2;
        coords[3] = bc3;
        BoundingBox<T> bounds(bc0, bc2);

        // Snapping distance for inserted coordinates
        T epsSqr = 1.0E-9;

        // Create boundary edges
        auto be0 = Edge<T>(0, 1);
        auto be1 = Edge<T>(1, 2);
        auto be2 = Edge<T>(2, 0);
        auto be3 = Edge<T>(2, 3);
        auto be4 = Edge<T>(3, 0);
        
        edges.push_front(be4);
        auto ber4 = edges.begin();
        edges.push_front(be3);
        auto ber3 = edges.begin();
        edges.push_front(be2);
        auto ber2 = edges.begin();
        edges.push_front(be1);
        auto ber1 = edges.begin();
        edges.push_front(be0);
        auto ber0 = edges.begin();

        // Create boundary triangles
        triangleEdges.push_front( { ber0, ber1, ber2, false, false, false } );
        auto bt0 = triangleEdges.begin();
        updateTriangleHash(coords, triangleHash, bounds, triangleEdges.begin());

        triangleEdges.push_front( { ber2, ber3, ber4, true, false, false } );
        auto bt1 = triangleEdges.begin();
        updateTriangleHash(coords, triangleHash, bounds, triangleEdges.begin());

        ber0->set_t0(bt0);
        ber1->set_t0(bt0);
        ber2->set_t0(bt0);
        ber2->set_t1(bt1);
        ber3->set_t0(bt1);
        ber4->set_t0(bt1);

        // Update required edges
        if (edgeIndexes.size() > 0 && (edgeIndexes.size() & 0x01)) {
            throw std::runtime_error("Edge index list must contain a set of index pairs");
        }
        std::map<uint64_t, bool> requiredEdges;
        for (std::size_t i = 0; i < edgeIndexes.size(); i+=2) {
            auto ei0 = edgeIndexes[i];
            auto ei1 = edgeIndexes[i+1];
            if (ei0 < 4) {
                ei0 += reserveIndex;
            }
            if (ei1 < 4) {
                ei1 += reserveIndex;
            }
            if (ei0 != ei1) {
                requiredEdges[pairing(ei0, ei1)] = false;
            }
        }

        // Process all coordinates except four bounding box corners
        std::size_t ci = 4;        
        std::vector<uint64_t> addedEdges;
        do {
            while (ci < coords.size()) {

                // Clear hole edges
                holeEdges.clear();

                // Find starting triangle
                auto its = seek(coords[ci], coords, bounds, triangleEdges, triangleHash);

                // Check for close or duplicate coordinates
                bool skip = false;
                auto tsn = its->get_c0();
                if (Coordinate<T>::distanceSqr(coords[ci], coords[tsn]) < epsSqr) {
                    skip = true;
                } else {
                    tsn = its->get_c1();
                    if (Coordinate<T>::distanceSqr(coords[ci], coords[tsn]) < epsSqr) {
                        skip = true;
                    } else {
                        tsn = its->get_c2();
                        if (Coordinate<T>::distanceSqr(coords[ci], coords[tsn]) < epsSqr) {
                            skip = true;
                        }
                    }
                }
                if (skip) {

                    // Check and update edge list
                    auto rit = requiredEdges.lower_bound(ci<<32);
                    while (rit != requiredEdges.end()) {
                        std::size_t c0 = rit->first&0xFFFFFFFF;
                        std::size_t c1 = rit->first>>32;

                        // Update edge indexes for duplicate coordinates
                        if (c0 == ci) {
                            if (c1 != tsn) {
                                auto eit = requiredEdges.insert( { pairing(tsn, c1), rit->second } );

                                // Remove duplicate required edges
                                if (eit.second == false) {
                                    requiredEdges.erase(eit.first);
                                }
                            }
                            rit = requiredEdges.erase(rit);
                        } else if (c1 == ci) {
                            if (c0 != tsn) {
                                auto eit = requiredEdges.insert( { pairing(tsn, c0), rit->second } );

                                // Remove duplicate required edges
                                if (eit.second == false) {
                                    requiredEdges.erase(eit.first);
                                }
                            }
                            rit = requiredEdges.erase(rit);
                        } else {
                            ++rit;
                        }
                    }

                    // Ignore coordinate
                    ci++;
                    continue;
                }

                // Build hole
                candidates.push_back(its);
                while (candidates.size() > 0) {

                    // Get handle to triangle
                    auto itt = *candidates.begin();
                    auto &tri = *itt;

                    // Check if the point is inside the triangle circumcircle
                    auto incircle = Geostack::Robust<T>::incircle(
                        coords[tri.get_c0()], coords[tri.get_c1()], coords[tri.get_c2()], coords[ci]);
                    if (incircle <= 0.0) {
                
                        bool add_e[3] = { true, true, true };

                        auto ith = holeEdges.begin();
                        while (ith != holeEdges.end()) {

                            // Search for edge
                            bool found_e[3];
                            for (std::size_t i = 0; i < 3; i++) {
                                found_e[i] = (ith->first == tri.e[i]);
                            }

                            // Remove edge if found
                            if (found_e[0] || found_e[1] || found_e[2]) {
                                add_e[0] &= !found_e[0];
                                add_e[1] &= !found_e[1];
                                add_e[2] &= !found_e[2];
                                ith = holeEdges.erase(ith);
                            } else {
                                ++ith;
                            }
                        }                    

                        // Update edge lists
                        for (std::size_t i = 0; i < 3; i++) {

                            if (add_e[i]) {

                                // Update edge to remove triangle
                                if (tri.e[i]->get_has_t0() && tri.e[i]->get_t0() == itt) {
                                    tri.e[i]->clear_t0();

                                    // Add neighbour as candidate
                                    if (tri.e[i]->get_has_t1()) {
                                        auto neighbour_1 = tri.e[i]->get_t1();

                                        // Search list and add if not found
                                        auto itc = candidates.begin();
                                        while (itc != candidates.end() && *itc != neighbour_1) ++itc;
                                        if (itc == candidates.end()) {
                                            candidates.push_back(neighbour_1);
                                        }
                                    }
                                } else if (tri.e[i]->get_has_t1() && tri.e[i]->get_t1() == itt) {
                                    tri.e[i]->clear_t1();

                                    // Add neighbour as candidate
                                    if (tri.e[i]->get_has_t0()) {
                                        auto neighbour_0 = tri.e[i]->get_t0();

                                        // Search list and add if not found
                                        auto itc = candidates.begin();
                                        while (itc != candidates.end() && *itc != neighbour_0) ++itc;
                                        if (itc == candidates.end()) {
                                            candidates.push_back(neighbour_0);
                                        }
                                    }
                                }

                                // Add to hole edge list
                                holeEdges.push_front( { tri.e[i], tri.flip[i] } );

                            } else {

                                // Update required edges
                                auto rit = requiredEdges.find(pairing(tri.e[i]->c0, tri.e[i]->c1));
                                if (rit != requiredEdges.end()) {
                                    rit->second = false;
                                }

                                // Delete edge
                                edges.erase(tri.e[i]);
                            }
                        }

                        // Remove triangle
                        triangleHash.erase(itt->hit);
                        itt = triangleEdges.erase(itt);
                    }

                    // Remove candidate
                    candidates.pop_front();
                } 
            
                if (holeEdges.size() == 0) {
                
                    // Failed to build hole
                    throw std::runtime_error("Cannot build mesh");
                }

                // Update triangulation
                auto ith = holeEdges.begin();
                auto c_last = ith->second ? ith->first->c0 : ith->first->c1;
                auto ite_last = edges.begin();
                edges.push_front(Edge<T>(c_last, ci));
                auto ite_first = edges.begin();
                while (holeEdges.size() > 1) {

                    // Search for connecting edge
                    for (auto iths = holeEdges.begin(); iths != holeEdges.end(); ++iths) {

                        auto c_join = iths->second ? iths->first->c1 : iths->first->c0;
                        if (c_join == c_last) {

                            // Create new triangle
                            c_last = iths->second ? iths->first->c0 : iths->first->c1;
                            auto ite_previous = edges.begin();
                            edges.push_front(Edge<T>(c_last, ci));
                            triangleEdges.push_front(TriangleEdges<T>(iths->first, edges.begin(), ite_previous, iths->second, false, true));
                            updateTriangleHash(coords, triangleHash, bounds, triangleEdges.begin());

                            // Update edges with triangle
                            if (!iths->first->get_has_t0()) {
                                iths->first->set_t0(triangleEdges.begin());
                            } else {
                                iths->first->set_t1(triangleEdges.begin());
                            }
                            edges.begin()->set_t0(triangleEdges.begin());
                            ite_previous->set_t1(triangleEdges.begin());

                            // Delete edge from hole list
                            holeEdges.erase(iths);
                            break;
                        }
                    }
                }

                // Add final triangle
                ith = holeEdges.begin();
                triangleEdges.push_front(TriangleEdges<T>(ith->first, ite_first, edges.begin(), ith->second, false, true));
                updateTriangleHash(coords, triangleHash, bounds, triangleEdges.begin());

                // Update edges with triangle
                if (!ith->first->get_has_t0()) {
                    ith->first->set_t0(triangleEdges.begin());
                } else {
                    ith->first->set_t1(triangleEdges.begin());
                }
                ite_first->set_t0(triangleEdges.begin());
                edges.begin()->set_t1(triangleEdges.begin());

                // Check all newly added edges TODO unneeded?
                ite_last++;
                for (auto ite = edges.begin(); ite != ite_last; ++ite) {
                    auto rit = requiredEdges.find(pairing(ite->c0, ite->c1));
                    if (rit != requiredEdges.end()) {
                        rit->second = true;
                    }
                }

                // Increment coordinate counter
                ci++;
            }

            // If all coordinates have been processed, check for required edges
            addedEdges.clear();
            auto rit = requiredEdges.begin();
            while (rit != requiredEdges.end()) {

                // Check for required edges not present
                if (rit->second == false) {
                    
                    // Get coordinates
                    std::size_t c0 = rit->first&0xFFFFFFFF;
                    std::size_t c1 = rit->first>>32;
                    const auto &ec0 = coords[c0];
                    const auto &ec1 = coords[c1];

                    // Calculate edge midpoint using high-precision formula
                    Coordinate<T> mid( { (T)(ec0.p+(ec1.p/2-ec0.p/2)), (T)(ec0.q+(ec1.q/2-ec0.q/2)) } );
                    if (mid == ec0 || mid == ec1) {

                        // Midpoint cannot be resolved
                        throw std::runtime_error("Cannot resolve midpoint");
                    }
                    
                    // Find triangle containing edge centre using accurate method
                    auto &tri = *seek(mid, coords, bounds, triangleEdges, triangleHash);
                    auto tn0 = tri.get_c0();
                    auto tn1 = tri.get_c1();
                    auto tn2 = tri.get_c2();
                    const auto &tc0 = coords[tn0];
                    const auto &tc1 = coords[tn1];
                    const auto &tc2 = coords[tn2];

                    // Check any edges are required
                    if (rit->first == pairing(tn0, tn1) || rit->first == pairing(tn1, tn2) || rit->first == pairing(tn2, tn0)) {
                        rit->second = true;
                        ++rit;
                        continue;
                    }
                    
                    // Find intercept of edge and edge of enclosing triangle
                    T t0 = interceptValue2D<T>(ec0.p, ec0.q, ec1.p, ec1.q, tc0.p, tc0.q, tc1.p, tc1.q);
                    T t1 = interceptValue2D<T>(ec0.p, ec0.q, ec1.p, ec1.q, tc1.p, tc1.q, tc2.p, tc2.q);
                    T t2 = interceptValue2D<T>(ec0.p, ec0.q, ec1.p, ec1.q, tc2.p, tc2.q, tc0.p, tc0.q);

                    // Find closest intercept to midpoint
                    T t = 0.0;
                    int edgeIndex = -1;
                    if (t0 == t0 && fabs(t0-0.5) < fabs(t-0.5)) { t = t0; edgeIndex = 0; }
                    if (t1 == t1 && fabs(t1-0.5) < fabs(t-0.5)) { t = t1; edgeIndex = 1; }
                    if (t2 == t2 && fabs(t2-0.5) < fabs(t-0.5)) { t = t2; edgeIndex = 2; }
                    if (edgeIndex == -1) {

                        // For degenerate cases the intercept might slightly outside the triangle, use projected value instead
                        t0 = projectedValue2D<T>(ec0.p, ec0.q, ec1.p, ec1.q, tc0.p, tc0.q, tc1.p, tc1.q);
                        t1 = projectedValue2D<T>(ec0.p, ec0.q, ec1.p, ec1.q, tc1.p, tc1.q, tc2.p, tc2.q);
                        t2 = projectedValue2D<T>(ec0.p, ec0.q, ec1.p, ec1.q, tc2.p, tc2.q, tc0.p, tc0.q);

                        t = 0.0;
                        if (t0 == t0 && fabs(t0-0.5) < fabs(t-0.5)) { t = t0; edgeIndex = 0; }
                        if (t1 == t1 && fabs(t1-0.5) < fabs(t-0.5)) { t = t1; edgeIndex = 1; }
                        if (t2 == t2 && fabs(t2-0.5) < fabs(t-0.5)) { t = t2; edgeIndex = 2; }
                        
                        if (edgeIndex == -1) {
                            throw std::runtime_error("Cannot find edge intercept");
                        }
                    }
                    
                    // Add intersection coordinate and check for duplicates
                    auto nc = Coordinate<T>(ec0.p+t*(ec1.p-ec0.p), ec0.q+t*(ec1.q-ec0.q));

                    // Search triangles for existing coordinates
                    auto &tris = *seek(nc, coords, bounds, triangleEdges, triangleHash);
                    auto tsn0 = tris.get_c0();
                    auto tsn1 = tris.get_c1();
                    auto tsn2 = tris.get_c2();

                    std::size_t ni;
                    if (Coordinate<T>::distanceSqr(coords[tsn0], nc) < epsSqr) {
                        ni = tsn0;
                    } else if (Coordinate<T>::distanceSqr(coords[tsn1], nc) < epsSqr) {
                        ni = tsn1;
                    } else if (Coordinate<T>::distanceSqr(coords[tsn2], nc) < epsSqr) {
                        ni = tsn2;
                    } else {

                        // Search recently added coordinates
                        for (ni = ci; ni < coords.size(); ni++) {
                            if (Coordinate<T>::distanceSqr(coords[ni], nc) < epsSqr) {
                                break;
                            }
                        }
                        if (ni == coords.size()) {
                            coords.push_back(nc);
                        }
                    }

                    // Add edges
                    if (c0 == ni || c1 == ni) {

                        // Break at midpoint if insertion point is too close to existing coordinate
                        ni = coords.size();
                        coords.push_back(mid);
                    }
                    addedEdges.push_back(pairing(c0, ni));
                    addedEdges.push_back(pairing(c1, ni));

                    // Check if intersected edge is a required edge
                    if (tn0 != ni && tn1 != ni && tn2 != ni) {
                        if (edgeIndex == 0) {
                            auto eit = requiredEdges.find(pairing(tn0, tn1));
                            if (eit != requiredEdges.end()) {
                                addedEdges.push_back(pairing(tn0, ni));
                                addedEdges.push_back(pairing(tn1, ni));
                                requiredEdges.erase(eit);
                            }
                        } else if (edgeIndex == 1) {
                            auto eit = requiredEdges.find(pairing(tn1, tn2));
                            if (eit != requiredEdges.end()) {
                                addedEdges.push_back(pairing(tn1, ni));
                                addedEdges.push_back(pairing(tn2, ni));
                                requiredEdges.erase(eit);
                            }
                        } else if (edgeIndex == 2) {
                            auto eit = requiredEdges.find(pairing(tn2, tn0));
                            if (eit != requiredEdges.end()) {
                                addedEdges.push_back(pairing(tn2, ni));
                                addedEdges.push_back(pairing(tn0, ni));
                                requiredEdges.erase(eit);
                            }
                        }
                    }

                    // Remove old required edge
                    rit = requiredEdges.erase(rit);

                } else {
                    ++rit;
                }
            }

            // Add new edges
            for (auto addedEdge : addedEdges) {
                requiredEdges[addedEdge] = false;
            }

        } while (addedEdges.size() > 0 || ci < coords.size());

        // Return required geometry type
        Vector<T> v;

        if (geometryType & GeometryType::Point) {

            // Return points
            for (std::size_t i = 4; i < coords.size(); i++) {
                v.addPoint( { coords[i].p*delta+xmin, coords[i].q*delta+ymin } );
            }
        }

        if (geometryType & GeometryType::LineString) {

            if (edgeIndexes.size() > 0) {

                // Return required edges
                for (const auto &e : requiredEdges) {
                    std::size_t c0 = e.first&0xFFFFFFFF;
                    std::size_t c1 = e.first>>32;
                    const auto &ec0 = coords[c0];
                    const auto &ec1 = coords[c1];
                    v.addLineString( { 
                        { ec0.p*delta+xmin, ec0.q*delta+ymin }, 
                        { ec1.p*delta+xmin, ec1.q*delta+ymin }
                    } );
                }
            } else {

                // Return all edges
                for (const auto &e : edges) {
                    if (e.c0 > 3 && e.c1 > 3) {
                        v.addLineString( { 
                            { coords[e.c0].p*delta+xmin, coords[e.c0].q*delta+ymin },
                            { coords[e.c1].p*delta+xmin, coords[e.c1].q*delta+ymin }
                        } );
                    }
                }
            }
        }
        
        if (geometryType & GeometryType::Polygon) {
        
            if (edgeIndexes.size() > 0) {

                // Find starting triangle
                auto startTriangle = seek(bc0, coords, bounds, triangleEdges, triangleHash);
                candidates.push_back(startTriangle);

                // Partition polygons
                uint32_t id = 0;
                bool partition = true;
                while (partition) {                

                    // Increment polygon id
                    id++;

                    // Search for start triangle
                    for (auto itt = triangleEdges.begin(); candidates.size() == 0 && itt != triangleEdges.end(); ++itt) {
                        auto &tri = *itt;
                        if (tri.id == 0) {
                            for (std::size_t i = 0; i < 3; i++) {
                                if (tri.e[i]->get_has_t0() && tri.e[i]->get_t0() != itt) {
                                    auto neighbour_0 = tri.e[i]->get_t0();
                                    if (neighbour_0->id != 0) {
                                        candidates.push_back(itt);
                                    }
                                } else if (tri.e[i]->get_has_t1() && tri.e[i]->get_t1() != itt) {
                                    auto neighbour_1 = tri.e[i]->get_t1();
                                    if (neighbour_1->id != 0) {
                                        candidates.push_back(itt);
                                    }
                                }
                            }
                        }
                    }

                    // Set partition flag
                    partition = candidates.size() != 0;

                    // Paint polygons
                    while (candidates.size() > 0) {
            
                        // Get handle to triangle
                        auto itt = *candidates.begin();
                        auto &tri = *itt;
                        tri.id = id;

                        // Add neighbours as candidates
                        for (std::size_t i = 0; i < 3; i++) {

                            // Check edge
                            if (requiredEdges.find(pairing(tri.e[i]->c0, tri.e[i]->c1)) == requiredEdges.end()) {

                                if (tri.e[i]->get_has_t0() && tri.e[i]->get_t0() == itt) {

                                    // Add neighbour as candidate
                                    if (tri.e[i]->get_has_t1()) {
                                        auto neighbour_1 = tri.e[i]->get_t1();
                                
                                        // Add any neighbours not over a boundary edge
                                        if (neighbour_1->id != id) {
                                            neighbour_1->id = id;
                                            candidates.push_back(neighbour_1);
                                        }
                                    }
                                } else if (tri.e[i]->get_has_t1() && tri.e[i]->get_t1() == itt) {

                                    // Add neighbour as candidate
                                    if (tri.e[i]->get_has_t0()) {
                                        auto neighbour_0 = tri.e[i]->get_t0();

                                        // Add any neighbours not over a boundary edge
                                        if (neighbour_0->id != id) {
                                            neighbour_0->id = id;
                                            candidates.push_back(neighbour_0);
                                        } 
                                    }
                                }
                            }
                        }

                        // Remove candidate
                        candidates.pop_front();
                    }
                }

                // Clear all outer triangles
                auto itt = triangleEdges.begin();
                while (itt != triangleEdges.end()) {
                    auto &tri = *itt;
                    if (tri.id == 1) {                        
                        for (std::size_t i = 0; i < 3; i++) {
                            if (tri.e[i]->get_has_t0() && tri.e[i]->get_t0() == itt) {
                                tri.e[i]->clear_t0();
                            } else if (tri.e[i]->get_has_t1() && tri.e[i]->get_t1() == itt) {
                                tri.e[i]->clear_t1();
                            }
                        }
                        itt = triangleEdges.erase(itt);
                    } else {
                        ++itt;
                    }
                }

                // Add polygons
                for (uint32_t sid = 2; sid < id; sid++) {

                    // Clear hole edges
                    holeEdges.clear();
                    
                    // Search for polygon index
                    for (auto itt = triangleEdges.begin(); itt != triangleEdges.end(); ++itt) {

                        auto &tri = *itt;
                        if (tri.id == sid) {
                        
                            bool add_e[3] = { true, true, true };

                            auto ith = holeEdges.begin();
                            while (ith != holeEdges.end()) {

                                // Search for edge
                                bool found_e[3];
                                for (std::size_t i = 0; i < 3; i++) {
                                    found_e[i] = (ith->first == tri.e[i]);
                                }

                                // Remove edge if found
                                if (found_e[0] || found_e[1] || found_e[2]) {
                                    add_e[0] &= !found_e[0];
                                    add_e[1] &= !found_e[1];
                                    add_e[2] &= !found_e[2];
                                    ith = holeEdges.erase(ith);
                                } else {
                                    ++ith;
                                }
                            } 

                            // Update edge lists
                            for (std::size_t i = 0; i < 3; i++) {
                                if (add_e[i]) {
                                    holeEdges.push_front( { tri.e[i], tri.flip[i] } );
                                }
                            }
                        }
                    }
                            
                    // Build polygon
                    std::vector<CoordinateList<T> > polygonCoords;
                    while (holeEdges.size() > 1) {

                        // Get edge, ensuring first edge is an exterior boundary
                        auto ith = holeEdges.begin();
                        if (polygonCoords.size() == 0) {
                            while (ith != holeEdges.end()) {
                                if (ith->first->get_has_t0() && ith->first->get_t0()->id == sid) {
                                    if (!ith->first->get_has_t1() || ith->first->get_t1()->id < sid) {
                                        break;
                                    }
                                } else if (ith->first->get_has_t1() && ith->first->get_t1()->id == sid) {
                                    if (!ith->first->get_has_t0() || ith->first->get_t0()->id < sid) {
                                        break;
                                    }
                                }
                                ++ith;
                            }
                        }

                        // Add first ring coordinate
                        auto c_last = ith->second ? ith->first->c0 : ith->first->c1;
                        CoordinateList<T> ringCoords;
                        ringCoords.push_back( { coords[c_last].p*delta+xmin, coords[c_last].q*delta+ymin } );

                        // Construct ring
                        bool foundRing = true;
                        while (foundRing) {

                            // Search for connecting edge
                            foundRing = false;
                            for (auto iths = holeEdges.begin(); iths != holeEdges.end(); ++iths) {

                                auto c_join = iths->second ? iths->first->c1 : iths->first->c0;
                                if (c_join == c_last) {

                                    // Add coordinate
                                    c_last = iths->second ? iths->first->c0 : iths->first->c1;
                                    ringCoords.push_back( { coords[c_last].p*delta+xmin, coords[c_last].q*delta+ymin } );

                                    // Delete edge from hole list
                                    holeEdges.erase(iths);
                                    foundRing = true;
                                    break;
                                }
                            }
                        }

                        // Add to polygon list
                        polygonCoords.push_back(ringCoords);
                    }

                    // Add polygon
                    auto pid = v.addPolygon(polygonCoords);
                    v.setProperty(pid, "id", sid-1);
                }

            } else {

                // Return all polygons
                for (auto itt = triangleEdges.begin(); itt != triangleEdges.end(); ++itt) {

                    // Get triangle coordinates
                    const auto &tri = *itt;
                    std::size_t tn0 = tri.get_c0();
                    std::size_t tn1 = tri.get_c1();
                    std::size_t tn2 = tri.get_c2();
            
                    // Add polygon
                    if (tn0 > 3 && tn1 > 3 && tn2 > 3) {
                        v.addPolygon( { { 
                            { coords[tn0].p*delta+xmin, coords[tn0].q*delta+ymin },
                            { coords[tn1].p*delta+xmin, coords[tn1].q*delta+ymin },
                            { coords[tn2].p*delta+xmin, coords[tn2].q*delta+ymin }
                        } } );
                    }
                }
            }
        }
        return v;
    }

    // Float type definitions
    template Vector<float> Delaunay<float>::triangulate(CoordinateList<float>, const std::vector<uint64_t>, std::size_t);

    // Double type definitions
    template Vector<double> Delaunay<double>::triangulate(CoordinateList<double>, const std::vector<uint64_t>, std::size_t);
}
