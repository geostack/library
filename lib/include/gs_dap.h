/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_DAP_H
#define GEOSTACK_DAP_H

#include <string>
#include <list>

#include "httplib.h"

#include "gs_property.h"
#include "gs_raster.h"

namespace Geostack
{
    /**
    * Class to hold variable information
    */
    template <typename CTYPE>
    class DAPDimension {
    
    public:

        DAPDimension(): n(0), delta(0), origin(0) {}
        ~DAPDimension() {}

        uint32_t n;         //< Number of dimension entries
        CTYPE delta;        //< Dimension spacing
        CTYPE origin;       //< Dimension origin
        std::string name;   //< Dimension name
        std::string type;   //< Dimension OpenDAP type
    };
    
    /**
    * Class to hold grid information
    */
    template <typename CTYPE>
    class DAPGrid {
    
    public:

        DAPGrid() {}
        ~DAPGrid() {}

        std::string type;                       //< OpenDAP type
        std::vector<DAPDimension<CTYPE> > dims; //< Grid dimensions
    };

    /**
    * DAP handler class
    */
    template <typename RTYPE, typename CTYPE>
    class DAPHandler : public RasterFileHandler<RTYPE, CTYPE> {

    public:

        // Constructors
        DAPHandler(): pClient(nullptr) {}        
        ~DAPHandler() {}
        
        // Associate file with raster
        void read(std::string url, Raster<RTYPE, CTYPE> &r, std::string jsonConfig);

        // Write raster to file
        void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig);

    private:

        // Handle to client
        std::vector<std::string> paths;
        std::string gridName;
        std::string noData;
        httplib::Headers headers;
        std::unique_ptr<httplib::Client> pClient;

        // Grid data
        std::map<std::string, DAPGrid<CTYPE> > grids;
        std::vector<uint32_t> layers;

        // Handler to read tile, used in bind for RasterFileHandler
        void readDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r);
    };
}

#endif
