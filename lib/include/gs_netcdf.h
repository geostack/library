/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_NETCDF_H
#define GEOSTACK_NETCDF_H

#include <string>

#include "gs_property.h"
#include "gs_raster.h"

namespace Geostack
{
    /**
    * NetCDF data types.
    */
    namespace NetCDFData {
        enum Type {
            NC_BYTE      = 1,
            NC_CHAR      = 2,
            NC_SHORT     = 3,
            NC_INT       = 4,
            NC_FLOAT     = 5,
            NC_DOUBLE    = 6
        };
    }

    /**
    * NetCDF variable class.
    */
    template <typename CTYPE>
    class NetCDFVariable {

    public:
        NetCDFVariable(): isRecord(false), type(0), size(0), offset(0), delta(0.0), start(0.0) { }
        ~NetCDFVariable(){};
        
        bool isRecord;              ///< Flag for variable with unlimited dimension
        std::vector<uint32_t> dims; ///< List of dimension indexes
        PropertyMap attr;           ///< Property map of variable attributes
        uint32_t type;              ///< Variable data type
        uint32_t size;              ///< Variable element size for record variables or total size for non-record variables
        uint64_t offset;            ///< File offset for variable start data

        CTYPE delta; ///< Spacing for dimension variables
        CTYPE start; ///< Start value for dimension variables
    };

    template <typename RTYPE, typename CTYPE>
    class NetCDFHandler : public RasterFileHandler<RTYPE, CTYPE> {

    public:

        /**
        * %NetCDFHandler constructor.
        */
        NetCDFHandler(): RasterFileHandler<RTYPE, CTYPE>(), nx(0), ny(0), nz(1),
            oz(0.0), recordSize(0), xFlipped(false), yFlipped(false), zFlipped(false) { }
        
        ~NetCDFHandler(){};
        
        void read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Open file for reading to %Raster
        void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Write %Raster to file

    private:

        uint32_t nx; ///< Number of cells in x direction
        uint32_t ny; ///< Number of cells in y direction
        uint32_t nz; ///< Number of cells in z direction

        CTYPE oz; ///< Origin in z-direction

        uint64_t recordSize; ///< Size of record in bytes

        bool xFlipped; ///< NetCDF has mirrored x-coordinate
        bool yFlipped; ///< NetCDF has mirrored y-coordinate
        bool zFlipped; ///< NetCDF has mirrored z-coordinate
    
        template <typename VTYPE, typename DTYPE>
        void readData(std::fstream &fileStream, TileDimensions<CTYPE> &tdim, std::vector<RTYPE> &v, NetCDFVariable<CTYPE> &var); ///< Read %Raster block
        void readDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r);  ///< Read %Raster tile
        
        PropertyMap globalAttributes;
        std::map<std::string, NetCDFVariable<CTYPE> > vars;
        std::string varName;
        std::vector<uint32_t> layers;
    };
}

#endif
