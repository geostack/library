/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef GEOSTACK_VECTOR_H
#define GEOSTACK_VECTOR_H

#include <cstdint>
#include <memory>
#include <vector>
#include <list>
#include <map>

#include "gs_geometry.h"
#include "gs_raster.h"
#include "gs_property.h"
#include "gs_variables.h"

namespace Geostack
{
    template <typename CTYPE>
    class Coordinate;

    template <typename CTYPE>
    class GeometryBase;

    template <typename CTYPE>
    class VectorGeometry;

    template <typename CTYPE>
    class Point;

    template <typename CTYPE>
    class LineString;

    template <typename CTYPE>
    class Polygon;

    template <typename CTYPE>
    class Vector;

    template <typename CTYPE>
    class RTreeNode;

    template <typename CTYPE>
    class RTree;

    template <typename RTYPE, typename KTYPE>
    class Variables;

    template <typename CTYPE>
    struct ProjectionParameters;

    template <typename CTYPE>
    class RasterBase;

    template <typename CTYPE>
    using RasterBaseRef = std::reference_wrapper<RasterBase<CTYPE> >;

    // Aliases
    template <typename CTYPE>
    using GeometryBasePtr = std::shared_ptr<GeometryBase<CTYPE> >;

    template <typename CTYPE>
    using VectorGeometryPtr = std::shared_ptr<VectorGeometry<CTYPE> >;

    template <typename CTYPE>
    using PointPtr = std::shared_ptr<Point<CTYPE> >;

    template <typename CTYPE>
    using LineStringPtr = std::shared_ptr<LineString<CTYPE> >;

    template <typename CTYPE>
    using PolygonPtr = std::shared_ptr<Polygon<CTYPE> >;

    template <typename CTYPE>
    using RTreeNodePtr = std::shared_ptr<RTreeNode<CTYPE> >;

    template <typename CTYPE>
    using CoordinateList = std::vector<Coordinate<CTYPE> >;

    template <typename CTYPE>
    using VectorPtr = std::shared_ptr<Vector<CTYPE> >;

    /**
    * Vector script ordering type
    */
    namespace VectorOrdering {
        enum Type {
            Ordered   = 0, ///< Vector from script is ordered
            Unordered = 1, ///< Vector from script is unordered
        };
    }

    /**
    * Vector script layer handling
    */
    namespace VectorLayerHandling {
        enum Type {
            AllLayers  = 0,   ///< Vector script runs over all layers
            ByLayer = 1 << 1, ///< Vector script returns results by layer
        };
    }

    /**
    * Vector script indexing options
    */
    namespace VectorIndexingOptions {
        enum Type {
            All = 0,            ///< Vector polygon indexing uses interior and edge cells
            Interior  = 1 << 8, ///< Vector polygon indexing uses interior cells
            Edges     = 2 << 8, ///< Vector polygon indexing uses edge cells
        };
    }

    /**
    * Relation types
    */
    namespace RelationType {
        enum Type {
            None = 0,              ///< No type
            Neighbour = 1,         ///< Neighbours
        };
    }

    /**
    * %VectorGeometry class for two dimensional geometry objects.
    */
    template <typename CTYPE>
    class VectorGeometry : public GeometryBase<CTYPE> {
        public:

            friend class Vector<CTYPE>;

            // Constructors
            VectorGeometry():id(0) { }
            virtual ~VectorGeometry() { }

            // Associate vertex with geometry
            virtual void addVertex(cl_uint v_) { }

            // Update vector indexes
            virtual void updateVector(Vector<CTYPE> &v, cl_uint index) = 0;

            // Update bounds
            virtual void updateBounds(const Vector<CTYPE> &v) = 0;

            // Functional vertex test
            virtual bool hasVertexIndex(cl_uint idx) = 0;
            virtual cl_uint connectedVertices(VectorGeometryPtr<CTYPE> &g) = 0;

            // Clone vertex data
            virtual VectorGeometry<CTYPE> *clone() = 0;

            /**
            * Set unique identifier of Geometry.
            * @param identifier.
            */
            void setID(cl_uint id_) {
                id = id_;
            }

            /**
            * Get unique identifier of Geometry.
            * @return identifier.
            */
            cl_uint getID() const {
                return id;
            }

            // Geometry checks
            virtual bool within(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const = 0;
            virtual bool intercepts(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const = 0;
            virtual bool encloses(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const = 0;

        protected:

            // Shift vertex index
            virtual void shiftVertexIndex(int32_t shift) = 0;

            // Geometry identifier
            cl_uint id;
    };

    /**
    * %Point class for two dimensional points.
    * Contains a pointer to a single vertex
    */
    template <typename CTYPE>
    class Point : public VectorGeometry<CTYPE> {
        public:

            friend class Vector<CTYPE>;

            // Constructors
            Point() { }
            Point(Coordinate<CTYPE> c, cl_uint index):
              bc(c), pointVertex(index) { }
            virtual ~Point() { }

            // Get bounds and centroid
            BoundingBox<CTYPE> getBounds() const override;
            Coordinate<CTYPE> getCentroid() const override;

            // Update bounds
            void updateBounds(const Vector<CTYPE> &v) override;

            // Geometry checks
            bool hasVertexIndex(cl_uint idx) override;
            cl_uint connectedVertices(VectorGeometryPtr<CTYPE> &g) override;
            bool within(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;
            bool intercepts(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;
            bool encloses(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;

            /**
            * Get type of Vector
            * @return Point identifier.
            */
            bool isType(size_t typeMask) const override {
                return (bool)(typeMask & GeometryType::Point);
            }

            /**
            * Update %Vector %Point index
            */
            void updateVector(Vector<CTYPE> &v, cl_uint index) override {
                v.updatePointIndex(index);
            }

        private:

            // Associate vertex with point
            void addVertex(cl_uint) override;

            // Clone vertex data
            VectorGeometry<CTYPE> *clone() override;

            // Shift vertex index
            void shiftVertexIndex(int32_t shift) override {
                pointVertex += shift;
            }

            cl_uint pointVertex;  ///< Vertex index
            Coordinate<CTYPE> bc; ///< Coordinate, used for bounding box
    };

    /**
    * %LineString class for two dimensional line strings.
    * Contains a list of pointers to vertices definining a line
    */
    template <typename CTYPE>
    class LineString : public VectorGeometry<CTYPE> {
        public:

            friend class Vector<CTYPE>;

            // Constructors
            LineString() { }
            virtual ~LineString() { }

            /**
            * Get %LineString bounding box.
            * @return pair of coordinates spanning the bounding box of the line string.
            */
            BoundingBox<CTYPE> getBounds() const override {
                return bounds;
            }

            /**
            * Get %LineString centroid.
            * @return centroid of the bounding box of the line string.
            */
            Coordinate<CTYPE> getCentroid() const override {
                return bounds.centroid();
            }

            // Update bounds
            void updateBounds(const Vector<CTYPE> &v) override;

            // Geometry checks
            bool hasVertexIndex(cl_uint idx) override;
            cl_uint connectedVertices(VectorGeometryPtr<CTYPE> &g) override;
            bool within(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;
            bool intercepts(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;
            bool encloses(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;

            /**
            * Get type of Vector
            * @return LineString identifier.
            */
            bool isType(size_t typeMask) const override {
                return (bool)(typeMask & GeometryType::LineString);
            }

            /**
            * Update %Vector %LineString index
            */
            void updateVector(Vector<CTYPE> &v, cl_uint index) override {
                v.updateLineStringIndex(index);
            }

        private:

            // Associate vertex with line
            void addVertex(cl_uint) override;

            // Clone vertex data
            VectorGeometry<CTYPE> *clone() override;

            // Shift vertex index
            void shiftVertexIndex(int32_t shift) override {
                for (auto &i : lineVertices)
                    i += shift;
            }

            std::vector<cl_uint> lineVertices; //< List of vertex indexes
            BoundingBox<CTYPE> bounds;         //< Bounding box
    };

    /**
    * %Polygon class for two dimensional polygons.
    * Contains a list of pointers to vertices defining a polygon
    * and an list of vertex offsets for each sub-polygon. Each sub-polygon
    * represents a hole in the polygon.
    */
    template <typename CTYPE>
    class Polygon : public VectorGeometry<CTYPE> {
        public:

            friend class Vector<CTYPE>;

            // Constructors
            Polygon() { }
            virtual ~Polygon() { }

            /**
            * Get %Polygon sub-polygon vertex offset list.
            */
            const std::vector<cl_uint> &getVertexIndexes() const {
                return polygonVertices;
            }

            /**
            * Get %Polygon sub-polygon vertex offset list.
            */
            const std::vector<cl_uint> &getSubIndexes() const {
                return polygonSubIndexes;
            }

            /**
            * Get %Polygon sub-polygon %BoundingBox list.
            */
            const std::vector<BoundingBox<CTYPE> > &getSubBounds() const {
                return polygonBounds;
            }

            /**
            * Get %Polygon bounding box.
            * @return pair of coordinates spanning the bounding box of the polygon.
            */
            BoundingBox<CTYPE> getBounds() const override {
                return polygonBounds.front();
            }

            /**
            * Get %Polygon centroid.
            * @return centroid of the bounding box of the line string.
            */
            Coordinate<CTYPE> getCentroid() const override {
                return polygonBounds.front().centroid();
            }

            // Update bounds
            void updateBounds(const Vector<CTYPE> &v) override;

            // Geometry checks
            bool hasVertexIndex(cl_uint idx) override;
            cl_uint connectedVertices(VectorGeometryPtr<CTYPE> &g) override;
            bool within(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;
            bool intercepts(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;
            bool encloses(const Vector<CTYPE> &v, const BoundingBox<CTYPE> &b) const override;

            /**
            * Get type of Vector
            * @return Polygon identifier.
            */
            bool isType(size_t typeMask) const override {
                return (bool)(typeMask & GeometryType::Polygon);
            }

            /**
            * Update %Vector %Point index
            * @param vector %Vector to update.
            */
            void updateVector(Vector<CTYPE> &v, cl_uint index) override {
                v.updatePolygonIndex(index);
            }

        private:

            // Associate vertex with polygon
            void addVertex(cl_uint) override;

            // Clone vertex data
            VectorGeometry<CTYPE> *clone() override;

            // Add sub-polygon to polygon
            void addSubPolygon(cl_uint length);

            // Shift vertex index
            void shiftVertexIndex(int32_t shift) override {
                for (auto &i : polygonVertices)
                    i += shift;
            }

            std::vector<cl_uint> polygonVertices;           ///< List of vertex indexes
            std::vector<cl_uint> polygonSubIndexes;         ///< List of sub-polygon indexes
            std::vector<BoundingBox<CTYPE> > polygonBounds; ///< List of polygon bounding boxes
    };

    /**
    * %Vector class for two dimensional geospatial vector data.
    * This is the base class for vector data. This holds a handle
    * to an array of vertices, as well as %Point, %LineString and
    * %Polygon data.
    */
    template <typename CTYPE>
    class Vector {

        public:

            // Constructor
            Vector();
            Vector(std::string name);
            Vector(std::shared_ptr<VariablesVector<Coordinate<CTYPE> > > _pVertices,
                   std::shared_ptr<PropertyMap> _pProperties = nullptr,
                   std::shared_ptr<PropertyMap> _pGlobalProperties = nullptr);

            // Copy constructor
            Vector(const Vector &v);

            // Destructor
            virtual ~Vector();

            // Assignment operator
            Vector &operator=(const Vector &v);

            // Addition operator
            Vector &operator+=(const Vector &v);

            // Add geometry
            cl_uint clone(cl_uint);
            cl_uint addPoint(Coordinate<CTYPE> c_);
            cl_uint addLineString(CoordinateList<CTYPE> cs_);
            cl_uint addPolygon(std::vector<CoordinateList<CTYPE> > pcs_);

            /**
            * Update %Vector %Point index
            */
            void updatePointIndex(cl_uint index) {
                pointIndexes.push_back(index);
            }

            /**
            * Update %Vector %LineString index
            */
            void updateLineStringIndex(cl_uint index) {
                lineStringIndexes.push_back(index);
            }

            /**
            * Update %Vector %Polygon index
            */
            void updatePolygonIndex(cl_uint index) {
                polygonIndexes.push_back(index);
            }

            // Clear data
            void clear();

            // Rebuild RTree
            void buildTree();

            // Read from data source
            void read(std::string fileName, std::string jsonConfig = "");

            // Write data
            void write(std::string fileName, size_t writeType = 0x07,
                       bool enforceProjection = true,
                       bool writeNullProperties = true);

            /**
            * Get %Coordinate from %VariablesVector.
            */
            const Coordinate<CTYPE> &getCoordinate(const cl_uint index) const {
                return pVertices->getData()[index];
            }

            /**
            * Get %Coordinate vertex buffer.
            */
            const cl::Buffer &getVertexBuffer() {
                return pVertices->getBuffer();
            }

            /**
            * Get number of vertices in Vector.
            */
            std::size_t getVertexSize() {
                if (pVertices != nullptr)
                    return pVertices->size();
                return 0;
            }

            /**
            * Get %VectorGeometry list.
            */
            const VectorGeometryPtr<CTYPE> &getGeometry(const cl_uint index) const {
                std::vector<VectorGeometryPtr<CTYPE> > &geometry = *pGeometry;
                return geometry[index];
            }

            /**
            * Get %Vector property map.
            */
            PropertyMap &getProperties() const {
                if (pGeometry != nullptr)
                    return *pProperties;
                throw std::length_error("No properties in Vector");
            }

            /**
            * Get all geometry indexes from %Vector.
            * @return list of indexes
            */
            const std::vector<cl_uint> &getGeometryIndexes() const {
                return geometryIndexes;
            }

            /**
            * Get all %Point indexes from %Vector.
            * @return list of indexes
            */
            const std::vector<cl_uint> &getPointIndexes() const {
                return pointIndexes;
            }

            /**
            * Get all %LineString indexes from %Vector.
            * @return list of indexes
            */
            const std::vector<cl_uint> &getLineStringIndexes() const {
                return lineStringIndexes;
            }

            /**
            * Get all %Polygon indexes from %Vector.
            * @return list of indexes
            */
            const std::vector<cl_uint> &getPolygonIndexes() const {
                return polygonIndexes;
            }

            /**
            * Get base geometry count from %Vector.
            * @return geometry count
            */
            std::size_t getGeometryBaseCount() {
                if (pGeometry != nullptr) {
                    return pGeometry->size();
                }
                return 0;
            }

            /**
            * Get %Point count from %Vector.
            * @return %Point count
            */
            std::size_t getPointCount() {
                return pointIndexes.size();
            }

            /**
            * Get %LineString count from %Vector.
            * @return %LineString count
            */
            std::size_t getLineStringCount() {
                return lineStringIndexes.size();
            }

            /**
            * Get %Polygon count from %Vector.
            * @return %Polygon count
            */
            std::size_t getPolygonCount() {
                return polygonIndexes.size();
            }

            // Get geometry items from Vector by index
            Coordinate<CTYPE> getPointCoordinate(const cl_uint index) const;
            CoordinateList<CTYPE> getLineStringCoordinates(const cl_uint index) const;
            CoordinateList<CTYPE> getPolygonCoordinates(const cl_uint index) const;
            const std::vector<cl_uint> &getPolygonVertexIndexes(const cl_uint index) const;
            const std::vector<cl_uint> &getPolygonSubIndexes(const cl_uint index) const;
            const std::vector<BoundingBox<CTYPE> > &getPolygonSubBounds(const cl_uint index) const;

            // Property check
            bool hasProperty(std::string name) const;
            bool isPropertyNumeric(std::string name) const;

            // Add property
            void addProperty(std::string name);

            // Convert property
            template<typename PTYPE>
            void convertProperty(std::string name);

            // Remove property
            void removeProperty(std::string name);

            // Set global property
            template<typename PTYPE>
            void setGlobalProperty(std::string name, PTYPE v);

            // Set geometry property
            template<typename PTYPE>
            void setProperty(cl_uint index, std::string name, PTYPE v);

            // Set property vector
            template<typename PTYPE>
            void setPropertyVector(std::string name, PTYPE v);

            // Get global property
            template<typename PTYPE>
            PTYPE getGlobalProperty(std::string name) const;

            // Get geometry property
            template <typename PTYPE>
            PTYPE getProperty(cl_uint index, std::string name) const;

            // Get geometry property type
            PropertyType::Type getPropertyType(std::string name) const;

            // Get property vectors
            template <typename PTYPE>
            PTYPE &getPropertyVectorsRef(std::string name);

            // Get property vector
            template <typename PTYPE>
            PTYPE &getPropertyVectorRef(cl_uint index, std::string name);

            // Get property buffer
            bool hasPropertyBuffer(std::string name);
            cl::Buffer const &getPropertyBuffer(std::string name);

            /**
            * Set %Vector %ProjectionParameters.
            */
            void setProjectionParameters(ProjectionParameters<double> proj_) {
                proj = proj_;
            }

            /**
            * Get %Vector %ProjectionParameters.
            */
            ProjectionParameters<double> getProjectionParameters() const {
                return proj;
            }

            // Convert Vector
            Vector<CTYPE> convert(ProjectionParameters<double> to) const;
            Vector<CTYPE> convert(size_t geometryType) const;

            // Aliased conversion functions
            Vector<CTYPE> convertProjection(ProjectionParameters<double> to) const;
            Vector<CTYPE> convertGeometryType(size_t geometryType) const;

            // Find geometry within region
            Vector<CTYPE> region(BoundingBox<CTYPE> bounds,
                size_t geometryTypes = GeometryType::All);

            // Find nearest geometry
            Vector<CTYPE> nearest(BoundingBox<CTYPE> bounds,
                size_t geometryTypes = GeometryType::All);

            // Find geometry attached to coordinate
            std::vector<GeometryBasePtr<CTYPE> > attached(Coordinate<CTYPE> c,
                size_t geometryTypes = GeometryType::All);

            // Find geometry indices within region
            void regionGeometryIndices(BoundingBox<CTYPE> bounds,
                std::vector<GeometryBasePtr<CTYPE> > &geometryList,
                size_t geometryTypes = GeometryType::All);

            // Relation handling
            void buildRelations(size_t geometryType, size_t relationType = RelationType::Neighbour);
            std::size_t getRelationSize(cl_uint gid);
            cl_uint getRelationData(cl_uint gid, cl_uint idx);

            // De-duplication of vector vertices
            void deduplicateVertices();

            // Rasterisation of vector
            template <typename RTYPE>
            Raster<RTYPE, CTYPE> mapDistance(CTYPE resolution,
                std::string script = std::string(),
                size_t geometryTypes = GeometryType::All,
                BoundingBox<CTYPE> bounds = BoundingBox<CTYPE>());

            template <typename RTYPE>
            Raster<RTYPE, CTYPE> mapDistance(const RasterBase<CTYPE> &rasterBase,
                std::string script = std::string(),
                size_t geometryTypes = GeometryType::All);

            template <typename RTYPE>
            Raster<RTYPE, CTYPE> rasterise(CTYPE resolution,
                std::string script = std::string(),
                size_t parameters = GeometryType::All,
                BoundingBox<CTYPE> bounds = BoundingBox<CTYPE>());

            template <typename RTYPE>
            Raster<RTYPE, CTYPE> rasterise(const RasterBase<CTYPE> &rasterBase,
                std::string script = std::string(),
                size_t parameters = GeometryType::All);

            // Aliased mapDistance functions with no script
            template <typename RTYPE>
            Raster<RTYPE, CTYPE> mapDistance(CTYPE resolution,
                size_t geometryTypes = GeometryType::All,
                BoundingBox<CTYPE> bounds = BoundingBox<CTYPE>()) {
                return mapDistance<RTYPE>(resolution, std::string(), geometryTypes, bounds);
            }

            template <typename RTYPE>
            Raster<RTYPE, CTYPE> mapDistance(const RasterBase<CTYPE> &rasterBase,
                size_t geometryTypes = GeometryType::All) {
                return mapDistance<RTYPE>(rasterBase, std::string(), geometryTypes);
            }

            // Sample raster
            template <typename RTYPE>
            void pointSample(Raster<RTYPE, CTYPE> &r);

            /**
            * Get bounds of %Vector.
            * @return bounds of %Vector.
            */
            BoundingBox<CTYPE> getBounds() const {
                return tree.getBounds();
            }

            /**
            * Check for data in %Vector.
            * @return true if %Vector contains data.
            */
            bool hasData() const {
                return geometryIndexes.size() > 0;
            }

            // Run script on %Vector
            void runScript(std::string script);

        private:

            // Add geometry
            cl_uint add(VectorGeometryPtr<CTYPE> g);

            ProjectionParameters<double> proj; ///< %Vector projection
            RTree<CTYPE> tree;                 ///< %RTree for %Vector geometry

            Variables<cl_uint, cl_uint> relations; ///< Geometry relations
            std::vector<cl_uint> relationVector;   ///< Geometry relation data

            std::shared_ptr<VariablesVector<Coordinate<CTYPE> > > pVertices;     ///< List of all vertices
            std::shared_ptr<std::vector<VectorGeometryPtr<CTYPE> > > pGeometry;  ///< List of all geometry
            std::shared_ptr<PropertyMap> pProperties;                            ///< Map of all properties
            std::shared_ptr<PropertyMap> pGlobalProperties;                      ///< Map of all global properties

            std::vector<cl_uint> geometryIndexes;   ///< List of all geometry indexes
            std::vector<cl_uint> pointIndexes;      ///< List of all point indexes
            std::vector<cl_uint> lineStringIndexes; ///< List of all line string indexes
            std::vector<cl_uint> polygonIndexes;    ///< List of all polygon indexes
    };

    template <typename RTYPE, typename CTYPE>
    void runVectorScript(
        std::string script,
        Vector<CTYPE> &v,
        std::vector<RasterBaseRef<CTYPE> > rasterBaseRefs = { },
        Reduction::Type reductionType = Reduction::None,
        size_t parameters = 0);
}

#endif
