/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_TILE_H
#define GEOSTACK_TILE_H

#include <atomic>
#include <memory>
#include <vector>
#include <list>
#include <set>
#include <unordered_map>
#include <iterator>
#include <functional>
#include <fstream>

#ifdef USE_TILE_CACHING
#include <thread>
#endif

#include "gs_opencl.h"
#include "gs_solver.h"
#include "gs_geometry.h"
#include "gs_variables.h"
#include "gs_property.h"
#include "gs_projection.h"

namespace Geostack
{
    // Forward declarations
    class Solver;

    template <typename CTYPE>
    class BoundingBox;

    template <typename CTYPE>
    class Vector;

    template <typename RTYPE, typename CTYPE>
    class Tile;

    template <typename RTYPE, typename CTYPE>
    class Raster;
    
    // Definitions
    template <typename RTYPE, typename CTYPE>
    using TilePtr = std::shared_ptr<Tile<RTYPE, CTYPE> >;

    // Comparison operators
    template <typename RTYPE, typename CTYPE>
    bool tileDataEqual(Tile<RTYPE, CTYPE> &l, Tile<RTYPE, CTYPE> &r);

    using tileIndexSet = std::set<std::pair<uint32_t, uint32_t> >;
    
    /**
    * Tile fixed sizes
    */
    namespace TileMetrics {
    
        // Tile size power
        const uint32_t tileSizePower = 8;

        // Tile size
        const uint32_t tileSize = 1 << tileSizePower;

        // Tile size squared
        const uint32_t tileSizeSquared = tileSize*tileSize;

        // Tile mask
        const uint32_t tileSizeMask = tileSize-1;

        // Workgroup size power
        const uint32_t workgroupSizePower = 8;

        // Workgroup size
        const uint32_t workgroupSize = 1 << workgroupSizePower;

        // Reduction multiple
        const uint32_t reductionMultiple = tileSize/workgroupSize;
    }
    
    /**
    * %VectorRasterIndexes class for indexes of %Raster in %Vector
    * with corresponding area weighting.
    */    
    template <typename RTYPE>
    struct alignas(8) RasterIndex {
        uint32_t id;
        uint16_t i;
        uint16_t j;
        RTYPE w;
    };

    /**
    * %General dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) Dimensions {

        uint32_t nx; ///< Number of data cells in x dimension
        uint32_t ny; ///< Number of data cells in y dimension
        uint32_t nz; ///< Number of data cells in z dimension
        CTYPE hx;    ///< Spacing in x dimension
        CTYPE hy;    ///< Spacing in y dimension
        CTYPE hz;    ///< Spacing in z dimension
        CTYPE ox;    ///< Start coordinate in x dimension
        CTYPE oy;    ///< Start coordinate in y dimension
        CTYPE oz;    ///< Start coordinate in z dimension
        uint32_t mx; ///< Number of cells stored in memory for x dimension
        uint32_t my; ///< Number of cells stored in memory for y dimension
    };

    /**
    * %Tile dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) TileDimensions {
    
        Dimensions<CTYPE> d; ///< General dimensions
        CTYPE ex;            ///< End coordinate in x dimension
        CTYPE ey;            ///< End coordinate in y dimension
        CTYPE ez;            ///< End coordinate in z dimension
        uint32_t ti;         ///< Tile index in x dimension
        uint32_t tj;         ///< Tile index in y dimension
    };

    // Dimensions equality check.
    template <typename CTYPE>
    bool equalSpatialMetrics2D(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r);

    template <typename CTYPE>
    bool equalSpatialMetrics(const Dimensions<CTYPE> l, const Dimensions<CTYPE> r);

    /**
    * %Raster dimension structure
    */
    template <typename CTYPE>
    struct alignas(8) RasterDimensions {
    
        Dimensions<CTYPE> d; ///< General dimensions
        CTYPE ex;            ///< End coordinate in x dimension
        CTYPE ey;            ///< End coordinate in y dimension
        CTYPE ez;            ///< End coordinate in z dimension
        uint32_t tx;         ///< Number of tiles in x dimension
        uint32_t ty;         ///< Number of tiles in y dimension
    };
    
#ifdef USE_TILE_CACHING
    
    /**
    * %TileCacheItem class for %TileCacheManager data. 
    */
    template <typename RTYPE, typename CTYPE>
    class TileCacheItem {

    public:
    
        TileCacheItem(Tile<RTYPE, CTYPE> *pData_): 
            pData(pData_), dataConst(false) {}
        TileCacheItem(Tile<RTYPE, CTYPE> *pData_, bool dataConst_): 
            pData(pData_), dataConst(dataConst_) {}
        ~TileCacheItem() {}

        Tile<RTYPE, CTYPE> *pData; ///< Pointer to the tile
        bool dataConst;            ///< Whether the data in the tile can change
    };

    /**
    * %TileCacheManager class for %Tile data. 
    */
    template <typename RTYPE, typename CTYPE>
    class TileCacheManager {
    
    public:

        // Disable functions
        TileCacheManager(const TileCacheManager &) = delete;
        TileCacheManager(TileCacheManager &&) = delete;
        TileCacheManager& operator=(const TileCacheManager &) = delete;
        TileCacheManager& operator=(TileCacheManager &&) = delete;

        // Get singleton instance of %TileCacheManager.
        static TileCacheManager &instance();

        // %TileCacheManager functions
        void updateHost(Tile<RTYPE, CTYPE> *tile, bool isConst);
        void updateDevice(Tile<RTYPE, CTYPE> *tile);
        void removeHostEntry(Tile<RTYPE, CTYPE> *tile);
        void removeDeviceEntry(Tile<RTYPE, CTYPE> *tile);
        const std::string &getCacheDirectory() { return cacheDirectory; }
        void updateCacheHostAllocationBytes(int64_t delta) {
            cacheHostAllocationBytes += delta;
        }
        void updateCacheDeviceAllocationBytes(int64_t delta) {
            cacheDeviceAllocationBytes += delta;
        }
        std::size_t getHostAllocationBytes() const {
            return cacheHostAllocationBytes;
        }
        std::size_t getDeviceAllocationBytes() const {
            return cacheDeviceAllocationBytes;
        }

        std::map<Tile<RTYPE, CTYPE> *, std::vector<unsigned char> > compressedTiles;

    private:

        // Disable functions outside class
        TileCacheManager();
        ~TileCacheManager();
        
        // Maximum allocations for host and device
        std::size_t maxHostAllocationBytes;
        std::size_t maxDeviceAllocationBytes;
        std::size_t cacheHostAllocationBytes;
        std::size_t cacheDeviceAllocationBytes;

        // Cache variables
        std::list<TileCacheItem<RTYPE, CTYPE> > cacheDeviceList;
        std::list<TileCacheItem<RTYPE, CTYPE> > cacheHostList;
        typename std::list<TileCacheItem<RTYPE, CTYPE> >::iterator cacheHostCompressItem;
        std::unordered_map<Tile<RTYPE, CTYPE> *, typename std::list<TileCacheItem<RTYPE, CTYPE> >::iterator> cacheDeviceMap;
        std::unordered_map<Tile<RTYPE, CTYPE> *, typename std::list<TileCacheItem<RTYPE, CTYPE> >::iterator> cacheHostMap;

        // Temporary directory
        std::string cacheDirectory;
    };

#endif    

    // Data handlers
    template <typename RTYPE, typename CTYPE>
    using dataHandlerReadFunction = std::function<void(TileDimensions<CTYPE>, std::vector<RTYPE> &, Raster<RTYPE, CTYPE> &)>;

    template <typename RTYPE, typename CTYPE>
    using dataHandlerWriteFunction = std::function<void(TileDimensions<CTYPE>, std::vector<RTYPE> &, Raster<RTYPE, CTYPE> &)>;

    /**
    * %Tile class for one, two or three dimensional geospatial data.
    * This holds data for a square tile of a @Raster. 
    */
    template <typename RTYPE, typename CTYPE>
    class Tile : public GeometryBase<CTYPE> {

    public:

        // Constructor
        Tile();

        // Destructor
        ~Tile();
        
        // Disable Tile copy constructors.
        Tile(Tile const &) = delete;
        void operator=(Tile const &) = delete;

        friend bool tileDataEqual<RTYPE, CTYPE>(Tile<RTYPE, CTYPE> &, Tile<RTYPE, CTYPE> &);

        // Tile initialisation
        void createData();
        void clearData();
        void readData(Raster<RTYPE, CTYPE> &r, dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler = nullptr);
        bool init(uint32_t ti_, uint32_t tj_, RasterDimensions<CTYPE> rdim_);

        // Tile deletion
        void deleteData(bool updateCache = true);
        
        // Get bounds and centroid
        BoundingBox<CTYPE> getBounds() const override;
        Coordinate<CTYPE> getCentroid() const override;

        // Set tile orign
        void setOrigin_z(CTYPE oz_);

        /**
        * Get base dimensions.
        * @return base dimensions of %Tile.
        */
        Dimensions<CTYPE> getDimensions() { 
            return dim.d;
        }

        /**
        * Get data size dimensions.
        * @return data size in bytes of %Tile.
        */
        size_t getDataSize() const { 
            return dim.d.mx*dim.d.my*dim.d.nz;
        }

        /**
        * Get %Tile dimensions.
        * @return dimensions of %Tile.
        */
        TileDimensions<CTYPE> getTileDimensions() { 
            return dim;
        }
        
        /**
        * Get type of Geometry
        * @return Tile identifier.
        */
        bool isType(size_t typeMask) const override {
            return (bool)(typeMask & GeometryType::Tile);
        }

        // Buffer functions
        cl::Buffer &getDataBuffer();      ///< Get %Tile data buffer
        cl::Buffer &getReduceBuffer();    ///< Get %Tile reduction buffer
        cl::Buffer &getStatusBuffer();    ///< Get %Tile status buffer

        // Data handlers
        using tIterator = typename std::vector<RTYPE>::iterator;
        bool hasData();                                                       ///< Check %Tile data
        bool dataOnDevice();                                                  ///< Check if data is on the device
        void setAllCellValues(RTYPE val);                                     ///< Set all tile values
        void getData(tIterator iTo, size_t start, size_t end);                ///< Copy %Tile data to vector
        void setData(tIterator iFrom, tIterator iFromEnd, size_t start);      ///< Copy vector to %Tile data vector
        void extractData(std::vector<RTYPE> &data, Raster<RTYPE, CTYPE> &r);  ///< Read %Tile data to vector
        void setIndex(uint32_t ti_, uint32_t tj_);                            ///< Set %Tile index
        RTYPE &operator()(uint32_t i, uint32_t j = 0, uint32_t k = 0); ///< Get %Tile value

        RTYPE max();
        RTYPE min(); 

        // Reduction handlers
        RTYPE reduce(Reduction::Type type);                     ///< %Tile reduction
        std::vector<RTYPE> reduceByLayer(Reduction::Type type); ///< %Tile reduction by layer
        void resetReduction();                                  ///< Clear reduction buffer

        // Status handlers
        cl_uint getStatus();                       ///< Get %Tile status
        void setStatus(cl_uint newStatus);         ///< Set %Tile status
        cl_uint getResetStatus(cl_uint newStatus); ///< %Tile status with reset
        void clearDataBuffer();
        
#ifdef USE_TILE_CACHING

        // Cache handler
        bool isCached();
        void cacheWrite();
        void cacheRead();
        void cacheCompress();

#endif

        // Tile operations
        void mapVector(Vector<CTYPE> &v, std::size_t clHash, ProjectionParameters<double> rproj,
            size_t parameters = GeometryType::All,
            std::vector<std::string> fields = std::vector<std::string>(), bool useAll = false);

        void rasterise(Vector<CTYPE> &v, std::size_t clHash, std::vector<std::string> fields, ProjectionParameters<double> rproj,
            size_t parameters = GeometryType::All);

        void index(Vector<CTYPE> &v, std::vector<RasterIndex<CTYPE> > &rasterIndexes,
            std::size_t clHash, ProjectionParameters<double> rproj, cl_uchar parameters = 0x03);

    private:
        
        // Tile data
        volatile bool dataInitialised; ///< Tile data initialised
        TileDimensions<CTYPE> dim;     ///< Tile dimensions
        std::vector<RTYPE> dataVec;    ///< Data vector
        std::vector<RTYPE> reduceVec;  ///< Reduction vector
        cl_uint status;                ///< Status bits

#ifdef USE_TILE_CACHING

        friend class TileCacheManager<RTYPE, CTYPE>;

        /**
        * %TileCache class for writing %Tile data to a temporary file on disk. 
        */
        class TileCache {

        public:
            
            TileCache();
            ~TileCache(); 

            void readTileData(std::vector<RTYPE> &tileData);
            void writeTileData(std::vector<unsigned char> &compressedData);
            std::vector<unsigned char> compressTile(std::vector<RTYPE> &tileData);
            void uncompressTileData(std::vector<unsigned char> &compressedData, std::vector<RTYPE> &tileData);
            
            void setIsCompressed(bool isCompressed_) { isCompressed = isCompressed_; }
            void setIsWritten(bool isWritten_) { isWritten = isWritten_; }
            bool getIsCompressed() { return isCompressed; }
            bool getIsWritten() { return isWritten; }

        private:

            std::string cacheName;
            bool isCompressed;
            bool isWritten;
        };
        TileCache tileCache;
#endif

        // OpenCL data
        cl::Buffer dataBuffer;                          ///< Tile OpenCL data buffer
        mutable volatile bool dataBufferCreated;        ///< Flag indicating if data buffer has been created
        mutable volatile bool dataBufferOnDevice;       ///< Flag indicating if data buffer is currently on device
        void mapDataBuffer(cl::CommandQueue &);         ///< Map OpenCL data buffer to host
        void unmapDataBuffer(cl::CommandQueue &);       ///< Unmap OpenCL data buffer from host
        void ensureDataBufferOnHost();                  ///< Ensure the tile data is on the host

        cl::Buffer reduceBuffer;                        ///< Tile OpenCL reduction buffer
        mutable volatile bool reduceBufferCreated;      ///< Flag indicating if reduction buffer has been created
        mutable volatile bool reduceBufferOnDevice;     ///< Flag indicating if reduction buffer is currently on device
        void mapReduceBuffer(cl::CommandQueue &);       ///< Map OpenCL reduction buffer to host
        void unmapReduceBuffer(cl::CommandQueue &);     ///< Unmap OpenCL reduction buffer from host
        void ensureReduceBufferOnHost();                ///< Ensure the tile reduction data is on the host
        uint32_t reduceBufferSize;                      ///< Tile OpenCL reduction buffer size

        cl::Buffer statusBuffer;                        ///< Tile OpenCL status buffer
        mutable volatile bool statusBufferCreated;      ///< Flag indicating if status buffer has been created
        mutable volatile bool statusBufferOnDevice;     ///< Flag indicating if status buffer is currently on device
        void mapStatusBuffer(cl::CommandQueue &);       ///< Map OpenCL status buffer to host
        void unmapStatusBuffer(cl::CommandQueue &);     ///< Unmap OpenCL status buffer from host
        void ensureStatusBufferOnHost();                ///< Ensure the tile status data is on the host
    };
}

#endif
