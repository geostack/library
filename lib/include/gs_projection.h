/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef GEOSTACK_PROJ_H
#define GEOSTACK_PROJ_H

#include <string>

#include "json11.hpp"

#include "gs_solver.h"
#include "gs_variables.h"

// Link to external resources
extern const char R_PROJ4_datum_csv[];
extern const uint32_t R_PROJ4_datum_csv_size;

extern const char R_PROJ4_ellipsoid_csv[];
extern const uint32_t R_PROJ4_ellipsoid_csv_size;

// Projection based on GeoTIFF types
// See http://geotiff.maptools.org/spec/geotiff6.html#6.3.3.3
// Only a subset are supported
//
//Values:
//   CT_TransverseMercator =	1
//   CT_TransvMercator_Modified_Alaska = 2
//   CT_ObliqueMercator =	3
//   CT_ObliqueMercator_Laborde =	4
//   CT_ObliqueMercator_Rosenmund =	5
//   CT_ObliqueMercator_Spherical =	6
//   CT_Mercator =	7
//   CT_LambertConfConic_2SP =	8
//   CT_LambertConfConic_Helmert =	9
//   CT_LambertAzimEqualArea =	10
//   CT_AlbersEqualArea =	11
//   CT_AzimuthalEquidistant =	12
//   CT_EquidistantConic =	13
//   CT_Stereographic =	14
//   CT_PolarStereographic =	15
//   CT_ObliqueStereographic =	16
//   CT_Equirectangular =	17
//   CT_CassiniSoldner =	18
//   CT_Gnomonic =	19
//   CT_MillerCylindrical =	20
//   CT_Orthographic =	21
//   CT_Polyconic =	22
//   CT_Robinson =	23
//   CT_Sinusoidal =	24
//   CT_VanDerGrinten =	25
//   CT_NewZealandMapGrid =	26
//   CT_TransvMercator_SouthOriented=	27

namespace Geostack
{
    // Forward declarations
    template <typename T>
    class Coordinate;

    // Forward declarations
    template <typename T>
    class VertexCollection;

    /**
    * %Projection parameter structure
    */
    template <typename CTYPE>
    struct alignas(8) ProjectionParameters {

        // Member functions
        ProjectionParameters();

        template <typename DTYPE>
        ProjectionParameters(const ProjectionParameters<DTYPE> &r);

        ProjectionParameters<CTYPE> &operator=(ProjectionParameters const &r);

        std::string str();

        // Member variables
        uint8_t type;    // Projection type:
                         //    1. Projection coordinate system
                         //    2. Geographic latitude-longitude system
        uint8_t cttype;  // Coordinate transformation type:
                         //    1. Transverse Mercator
                         //    7. Mercator
                         //    8. Lambert conformal conic (2 point)
                         //   11. Albers equal area
                         //   24. Sinusoidal
        CTYPE a;         // Equatorial radius
        CTYPE f;         // Flattening
        CTYPE x0;        // Central meridian longitude
        CTYPE k0;        // Central meridian scale factor
        CTYPE fe;        // False easting
        CTYPE fn;        // False northing
        CTYPE phi_0;     // Origin latitude
        CTYPE phi_1;     // 1st standard latitude
        CTYPE phi_2;     // 2nd standard latitude
    };

    /**
    * %ProjectionTable class
    */
    class ProjectionTable {

    public:

        // Get singleton instance of %ProjectionTable.
        static ProjectionTable &instance();

        // Get datum
        bool getDatum(std::string datumName, std::string &ellipsoidName);

        // Get ellipsoid
        bool getEllipsoid(std::string ellipsoidName, double &equatorialRadius, double &inverseFlattening);

        // Get ellipsoid name
        std::string getEllipsoidName(double equatorialRadius, double inverseFlattening);

        // Get ellipsoid code
        uint32_t getEllipsoidCode(std::string ellipsoidName);

    private:

        void initialise();
        volatile bool initialised;

        // Projection tables
        std::map<std::string, std::string > datumTable;
        std::map<std::string, std::vector<double> > ellipsoidTable;
        std::map<std::string, uint32_t> ellipsoidCode;
    };

    /**
    * %ProjectionOperator class %Projection OpenCL.
    */
    class ProjectionOperator {

    public:

        // Get singleton instance of %ProjectionOperator.
        static ProjectionOperator &instance();

        // Get kernel
        cl::Kernel getKernel(std::string kernel);

    private:

        void initialise(Solver &);
        volatile bool initialised;
        std::size_t clProjectionHash;
    };

    /**
    * %Projection class for geospatial projections.
    */
    namespace Projection {

        // Parse PROJ4 string to and from projection object
        ProjectionParameters<double> parsePROJ4(const std::string PROJ4);
        std::string toPROJ4(const ProjectionParameters<double> proj);

        // Parse OGC WKT string into projection object
        ProjectionParameters<double> parseWKT(const std::string WKT);
        ProjectionParameters<double> parseWKT1(json11::Json::object jsonObj);
        ProjectionParameters<double> parseWKT2(json11::Json::object jsonObj);
        std::string toWKT(const ProjectionParameters<double> proj, int crs = 0);

        // search EPSG code in the epsg.json
        ProjectionParameters<double> fromEPSG(const std::string EPSG);

        // handle parsing of Proj4 or EPSG code
        ProjectionParameters<double> parseProjString(const std::string projString);

        // Project coordinates
        template <typename CTYPE>
        void convert(Coordinate<CTYPE> &c, ProjectionParameters<double> to, ProjectionParameters<double> from);
        template <typename CTYPE>
        void convert(std::vector<Coordinate<CTYPE> > &c, ProjectionParameters<double> to, ProjectionParameters<double> from);
        template <typename CTYPE>
        void convert(VariablesVector<Coordinate<CTYPE> > &v, ProjectionParameters<double> to, ProjectionParameters<double> from);

        // Check projection
        bool checkProjection(const ProjectionParameters<double> &proj);
    }

    // Comparison operators
    bool operator==(const ProjectionParameters<double> &l, const ProjectionParameters<double> &r);
    bool operator!=(const ProjectionParameters<double> &l, const ProjectionParameters<double> &r);

    // ProjectionParameters output stream
    std::ostream &operator<<(std::ostream &os, const ProjectionParameters<double> &p);
}

#endif