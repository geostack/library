/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef GEOSTACK_SOLVER_H
#define GEOSTACK_SOLVER_H

#include <map>
#include <mutex>
#include <vector>
#include <string>

#include "gs_opencl.h"

namespace Geostack
{
    /**
    * Verbosity level type
    */
    namespace Verbosity {
        enum Type {
            NotSet = 0,
            Debug = 10,
            Info = 20,
            Warning = 30,
            Error = 40,
            Critical = 50
        };
    }
    
    /**
    * Reduction type
    */
    namespace Reduction {
        enum Type {
            None       = 0,       ///< No reduction
            Maximum    = 1 << 8,  ///< Maximum over raster
            Minimum    = 2 << 8,  ///< Minimum over raster
            Sum        = 3 << 8,  ///< Sum over raster
            Count      = 4 << 8,  ///< Count of raster data values
            Mean       = 5 << 8,  ///< Mean over raster
            SumSquares = 6 << 8,  ///< Sum of squares of raster values
        };
    }
    
    /**
    * %RandomState structure
    */
    struct alignas(8) RandomState {
	    cl_uint a;
        cl_uint b;
        cl_uint c;
        cl_uint d;
    };

    // Null value handling
    template <typename RTYPE>
    static inline RTYPE getNullValue();

    template<> inline float getNullValue() {
        return std::numeric_limits<float>::quiet_NaN();
    }

    template<> inline double getNullValue() {
        return std::numeric_limits<double>::quiet_NaN();
    }

    template<> inline uint8_t getNullValue() {
        return 0; // No null value implemented for byte types
    }

    template<> inline uint32_t getNullValue() {
        return std::numeric_limits<uint32_t>::max();
    }

    template<> inline uint64_t getNullValue() {
        return std::numeric_limits<uint64_t>::max();
    }

    template<> inline int8_t getNullValue() {
        return 0; // No null value implemented for byte types
    }

    template<> inline int32_t getNullValue() {
        return std::numeric_limits<int32_t>::lowest();
    }

    template<> inline int64_t getNullValue() {
        return std::numeric_limits<int64_t>::lowest();
    }
    
    template<> inline std::string getNullValue() {
        return std::string("\x7F\x7F");
    }

    template <typename RTYPE>
    bool isValid(const RTYPE &a);

    template <typename RTYPE>
    bool isInvalid(const RTYPE &a);

    // Local comparison operators
    template <typename RTYPE>
    RTYPE max(RTYPE a, RTYPE b);

    template <typename RTYPE>
    RTYPE min(RTYPE a, RTYPE b);

    /**
    * %Geostack null hash class.
    * Contains only the null hash value for lookup.
    */
    class NullHash {

    public:
        NullHash();
        std::size_t value() const {
            return nullHash;
        }

    private:
        std::size_t nullHash;
    };
    
    /**
    * %AutoBuffer class.
    * Creates a read-only buffer which clears host memory after buffer is written.
    */
    template <typename T>
    class AutoBuffer {

    public:

        /**
        * %AutoBuffer constructor.
        */
        AutoBuffer(cl::Context &context, cl::CommandQueue &queue, const std::vector<T> &data_) {
        
            // Create buffer
            cl_int err = CL_SUCCESS;
            auto size = data_.size()*sizeof(T);
            buffer = cl::Buffer(context, CL_MEM_READ_ONLY, size, NULL, &err);
            if (err != CL_SUCCESS)
                throw std::runtime_error("Cannot initialise auto buffer");

            // Create data
            std::vector<T> *data = new std::vector<T>();
            *data = data_;

            // Write data buffer
            queue.enqueueWriteBuffer(buffer, CL_FALSE, 0, size, data->data(), NULL, &ev);

            // Set callback
            ev.setCallback(CL_COMPLETE, &AutoBuffer::callback, static_cast<void *>(data));
        }

        /**
        * %AutoBuffer destructor.
        */
        ~AutoBuffer() {

            // Wait for event
            ev.wait();
        }

        // Disable AutoBuffer copy constructors.
        AutoBuffer(AutoBuffer const &) = delete;
        void operator=(AutoBuffer const &) = delete;

        /**
        * Event callback.
        */
        static void callback(cl_event ev, cl_int status, void *ptr) {

            if (ptr) {

                // Get handle
                auto &data = *static_cast<std::vector<T> *>(ptr);

                // Clear data
                std::vector<T>().swap(data);

                // Delete pointer
                delete static_cast<std::vector<T> *>(ptr);
            }
        }

        /**
        * Get underlying buffer.
        */
        const cl::Buffer &getBuffer() const {
            return buffer;
        }

    private:

        cl::Event ev;        ///< OpenCL event triggered on buffer write
        cl::Buffer buffer;   ///< OpenCL buffer

    };

    /**
    * %Geostack solver base class.
    * Contains register and OpenCL handles for inter-processor use.
    */
    class Solver {  

    public:

        Solver();
        ~Solver();

        // Singleton instance of OpenCL solver.
        static Solver &getSolver();

        // Prevent copy and assignment
        Solver(Solver const&) = delete;
        void operator=(Solver const&) = delete;

        // Solver functions
        std::string getError() { return solverError; }
        bool openCLInitialised() { return initialised; }
        cl::Context& getContext();
        cl::CommandQueue& getQueue();
        std::size_t buildProgram(std::string clProgram);
        void registerProgram(std::size_t scriptHash, std::size_t programHash);
        std::size_t getProgram(std::size_t scriptHash);
        cl::Kernel& getKernel(std::size_t programHash, std::string kernelName);
        cl::size_type getKernelWorkgroupSize(std::size_t programHash, std::string kernelName);
        cl::size_type getKernelWorkgroupSize(cl::Kernel &);
        cl::size_type getKernelPreferredWorkgroupMultiple(cl::Kernel &);
        cl::size_type getMaxWorkgroupSize();
        cl_ulong getMaxAllocSizeBytes();
        cl_ulong getLocalMemorySizeBytes();
        bool getUseMapping() { return useMapping; }

        uint64_t getHostMemoryLimit();
        void setHostMemoryLimit(uint64_t hostMemoryLimit_);
        uint64_t getDeviceMemoryLimit();
        void setDeviceMemoryLimit(uint64_t deviceMemoryLimit_);

        // Output functions        
        void setVerbose(bool verbose_);
        void setVerboseLevel(uint8_t verbose_);
        uint8_t getVerboseLevel();

        // Process script for solver
        static std::string processScript(std::string script, bool addTerminator = true);

        static std::size_t getNullHash(); ///< Get null hash value
        
        template <typename RTYPE>
        static std::string getTypeString(); ///< Get type string

        template <typename RTYPE>
        static std::string getOpenCLTypeString(); ///< Get type string for OpenCL scripts
        
        // General functions
        template <typename TYPE>
        void fillBuffer(cl::Buffer &fill, TYPE v, cl_uint N);

        template <typename TYPE>
        void interleaveBuffer(cl::Buffer &in, cl::Buffer &out, TYPE noDataValue, uint32_t nx, uint32_t ny, uint32_t nz);

        // Timer functions
        void resetTimers();
        void switchTimers(std::string, std::string);
        void incrementTimers(std::string, std::string, double);
        void incrementTimers(std::string, std::string, cl::Event&);
        void incrementTimers(std::string, std::string, std::vector<cl::Event>&);
        void displayTimers();
        std::string currentTimer(std::string);

        void clearProgramMap();
        void clearKernels();
        bool initOpenCL();

    private:
                
        // Functions
        bool testOpenCL(cl::Context &, cl::CommandQueue &, int);
        std::string getBuildLog(std::vector<cl::Device> &platformDevices, cl::Program &);

        // Error holder
        std::string solverError;

        // Shared OpenCL items 
        int deviceCounter;
        static std::mutex solverMtx;
        volatile bool initialised;
        bool useMapping;

        // Null hash static reference
        static NullHash nullHash;
        
        int deviceID;
        int requestedPlatformID;
        std::vector<int> requestedDeviceIDs;

        uint64_t hostMemoryLimit; // Host memory limit in bytes
        uint64_t deviceMemoryLimit; // Device memory limit in bytes
        
        // OpenCL items
        cl::Context *pContext;
        cl::CommandQueue *pQueue;
        std::map<std::size_t, std::size_t> scriptMap; 
        std::map<std::size_t, cl::Program *> programMap; 
        std::map<std::pair<std::size_t, std::string>, cl::Kernel *> kernelMap; 
        std::map<std::pair<std::size_t, std::string>, cl::size_type> kernelWorkgroupSizeMap; 
        
        // Map from device to OpenCL items
        std::map<int, cl::size_type> maxWorkgroupSize;
        std::map<int, cl_ulong> maxAllocSizeBytes;
        std::map<int, cl_ulong> globalMemorySizeBytes;
        std::map<int, cl_ulong> localMemorySizeBytes;

        // Verbose flag
        uint8_t verbose;

        // Internal timing class
        class TimingDiff {

        private:

            // Start point
            std::chrono::steady_clock::time_point startTime;

            // Time interval in microseconds
            double accTime;

        public:
            // Constructors
            TimingDiff(): accTime(0) { }
            TimingDiff(const TimingDiff &t): 
                startTime(t.startTime), accTime(t.accTime) { }

            // Members
            void start() { 
                startTime = std::chrono::steady_clock::now();
            }
            void end() {
                accTime += std::chrono::duration<double, std::micro>(std::chrono::steady_clock::now()-startTime).count();
            }
            void increment(double t) {
                accTime += t;
            }
            double getAccTime() {
                return accTime;
            }
        };

        // Timing variables
        static std::mutex timingsMtx;
        std::map<std::string, std::map<std::string, TimingDiff> > timingsMap;
        std::map<std::string, std::string> timingsMapLastName;

    };


    /**
    * %KernelGenerator class
    * Holds parsed script and hash for script building
    */
    class KernelGenerator {
    public:

        /**
        * %KernelGenerator constructors
        */
        KernelGenerator():script(),
            programFlags(0) {
            programHash = Solver::getNullHash();
        }      
        KernelGenerator(const KernelGenerator &r): 
            script(r.script),
            fieldNames(r.fieldNames),
            programHash(r.programHash),
            programFlags(r.programFlags) {
        }
        KernelGenerator &operator=(const KernelGenerator &r) {
            if (this != &r) {
                script = r.script; 
                fieldNames = r.fieldNames;
                programHash = r.programHash;
                programFlags = r.programFlags;
            }
            return *this; 
        }
        
        std::string script;                  ///< Parsed script        
        std::vector<std::string> fieldNames; ///< Auxillary storage for field names
        std::size_t programHash;             ///< Compiled program hash
        uint64_t programFlags;               ///< Generated program flags
    };
    
    /**
    * %KernelCache class
    * Contains program hashes and corresponding generators
    */
    class KernelCache {  

    public:

        KernelCache() { };
        ~KernelCache() { };

        // Singleton instance of OpenCL solver.
        static KernelCache &getKernelCache();

        // Prevent copy and assignment
        KernelCache(KernelCache const&) = delete;
        void operator=(KernelCache const&) = delete;        

        // Member functions
        void setKernelGenerator(std::size_t, KernelGenerator);
        KernelGenerator getKernelGenerator(std::size_t scriptHash);

    private:
        std::map<std::size_t, KernelGenerator> generatorMap; 
    };
}

#endif
