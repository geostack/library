/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef GEOSTACK_VARIABLES_H
#define GEOSTACK_VARIABLES_H

#include <memory>
#include <vector>
#include <map>

#include "gs_opencl.h"
#include "gs_solver.h"

namespace Geostack
{
    // Forward declarations
    template <typename KTYPE>
    class VariablesBase;

    // Definitions
    template <typename KTYPE>
    using VariablesBasePtr = std::shared_ptr<VariablesBase<KTYPE> >;

    /**
    * %VariablesBase class.
    * Base class for %Variables.
    **/
    template <typename KTYPE>
    class VariablesBase {

    public:

        // Data handlers
        virtual cl::Buffer &getBuffer() = 0;
        virtual const std::map<KTYPE, std::size_t> &getIndexes() = 0;
        virtual std::size_t getSize(KTYPE) = 0;

        // Data check
        virtual bool hasData() = 0;

        // Get string for OpenCL scripts
        virtual std::string getOpenCLTypeString() = 0;

    protected:

        // Internal buffer
        cl::Buffer buffer;
    };

    /**
    * %Variables class for one dimensional named variables.
    */
    template <typename RTYPE, typename KTYPE>
    class Variables : public VariablesBase<KTYPE> {

    public:

        // Parent aliases
        using VariablesBase<KTYPE>::buffer;

        // Constructors
        Variables();
        Variables(const Variables &var);

        // Destructor
        ~Variables();
        
        // Assigment operator
        Variables &operator=(const Variables &var);

        // Set and get operations
        void clear();
        void set(KTYPE key, RTYPE value);
        void set(KTYPE key, RTYPE value, std::size_t index);
        RTYPE get(KTYPE key);
        RTYPE get(KTYPE key, std::size_t index);
        const std::map<KTYPE, std::size_t> &getIndexes();
        std::size_t getSize(KTYPE key);

        // Data handlers
        cl::Buffer &getBuffer();
        const std::vector<RTYPE> &getData();

        // Get string for OpenCL scripts
        std::string getOpenCLTypeString() {
            return Solver::getTypeString<RTYPE>();
        }

        // Run script over variables
        template <typename STYPE>
        friend void runScriptImpl(Variables<STYPE, std::string> &v, std::string script, std::string vin);
        void runScript(std::string script, std::string vin = std::string());

        /**
        * Check %Variables data.
        * @return true if map contains data, false otherwise
        */
        bool hasData() { return !dataMap.empty(); }

        bool hasVariable(KTYPE key);

    private:
        
        // Internal variables
        std::map<KTYPE, std::vector<RTYPE> > dataMap; ///< Data map
        std::map<KTYPE, std::size_t> dataIndexes;     ///< Indexes of data in vector
        std::vector<cl_uint> dataLengths;             ///< Lengths of data in vector
        std::vector<RTYPE> dataVec;                   ///< Data vector
        volatile bool bufferOnDevice;                 ///< Flag indicating if variable buffer is currently on device
        volatile bool dataChanged;                    ///< Flag indicating if data has been updated

        // Internal functions
        void refreshData();                       ///< Copy data from map to vector
        void mapBuffer(cl::CommandQueue &);       ///< Map buffer to host
        void unmapBuffer(cl::CommandQueue &);     ///< Unmap buffer from host
        void ensureDataOnHost();                  ///< Ensure the %Variables data is on the host
    };

    /**
    * %VariablesVector class for data arrays.
    */
    template <typename RTYPE>
    class VariablesVector {
    public:

        // Constructors
        VariablesVector();
        VariablesVector(VariablesVector const &r);
        VariablesVector &operator=(VariablesVector const &r);

        ~VariablesVector(){};

        /**
        * Get data from %VariablesVector.
        */
        std::vector<RTYPE> &getData() {
            ensureBufferOnHost();
            return dataVec;
        }

        /**
        * Get %VariablesVector buffer
        */
        const cl::Buffer &getBuffer();

        /**
        * Clear %VariablesVector data.
        */
        void clear() {
            ensureBufferOnHost();
            dataVec.clear();
        }

        /**
        * Get size of data vector.
        * @return size of data vector.
        */
        std::size_t size() const {
            return dataVec.size();
        }

        // Clone %VariablesVector
        VariablesVector<RTYPE> *clone();

    private:

        // Data vector
        mutable std::vector<RTYPE> dataVec;

        // OpenCL handlers
        cl::Buffer buffer;
        mutable volatile bool bufferOnDevice;          ///< Flag indicating if data buffer is currently on device
        mutable volatile std::size_t bufferSize;       ///< Current buffer size
        void mapBuffer(cl::CommandQueue &queue) const; ///< Map buffer to host
        void unmapBuffer(cl::CommandQueue &queue);     ///< Unmap buffer from host
        void ensureBufferOnHost() const;               ///< Ensure data is on the host
    };
}

#endif
