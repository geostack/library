/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef OPENCL_H
#define OPENCL_H

// Enable profiling
//#define CL_PROFILE_ENABLE

// Enable OpenCL exceptions
#define CL_HPP_ENABLE_EXCEPTIONS

// Ignore deprecation warnings
#define CL_USE_DEPRECATED_OPENCL_1_2_APIS

// Set OpenCL version to 1.2
#define CL_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_TARGET_OPENCL_VERSION 120

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#include "opencl.hpp"

// Standard OpenCL code block for REAL definition
#ifdef REAL_DOUBLE 
#define FLOAT_PRECISION "double"

#define CL_STD "-cl-std=CL1.2 -cl-mad-enable -D INOPENCL -w"

static const std::string clHeader = R"(
#pragma OPENCL EXTENSION cl_khr_fp64: enable

#define REAL_DOUBLE
#define REAL double
#define INT int
#define UINT uint
#define CHAR char
#define UCHAR uchar

#define REALVEC2 double2
#define REALVEC3 double3
#define REALVEC4 double4

#define noData_REAL NAN
#define noData_INT INT_MAX
#define noData_UINT UINT_MAX
#define noData_UCHAR 0 // Only used for out-of-range projection values

#define isValid_REAL(A) ((A) == (A))
#define isValid_INT(A) ((A) != noData_INT)
#define isValid_UINT(A) ((A) != noData_UINT)
#define isValid_UCHAR(A) ((A) == (A)) // Always true, coded to suppress warning
#define isInvalid_REAL(A) ((A) != (A))
#define isInvalid_INT(A) ((A) == noData_INT)
#define isInvalid_UINT(A) ((A) == noData_UINT)
#define isInvalid_UCHAR(A) ((A) != (A)) // Always false, coded to suppress warning

#define degrees_DEF(A) degrees((double)(A))
#define radians_DEF(A) radians((double)(A))
#define sqrt_DEF(A) sqrt((double)(A))
#define exp_DEF(A) exp((double)(A))
#define sin_DEF(A) sin((double)(A))
#define cos_DEF(A) cos((double)(A))
#define tan_DEF(A) tan((double)(A))
#define asin_DEF(A) asin((double)(A))
#define acos_DEF(A) acos((double)(A))
#define atan_DEF(A) atan((double)(A))
#define atan2_DEF(A, B) atan2((double)(A), (double)(B))
#define log_DEF(A) log((double)(A))
#define pow_DEF(A, B) pow((double)(A), (double)(B))
#define max_DEF(A, B) fmax((double)(A), (double)(B))
#define min_DEF(A, B) fmin((double)(A), (double)(B))
#define max_int_DEF(A, B) max((int)(A), (int)(B))
#define min_int_DEF(A, B) min((int)(A), (int)(B))
#define max_uint_DEF(A, B) max((uint)(A), (uint)(B))
#define min_uint_DEF(A, B) min((uint)(A), (uint)(B))
#define clamp_DEF(A, B, C) clamp((double)(A), (double)(B), (double)(C))
#define clamp_int_DEF(A, B, C) clamp((int)(A), (int)(B), (int)(C))
#define clamp_uint_DEF(A, B, C) clamp((uint)(A), (uint)(B), (uint)(C))
#define mad_DEF(A, B, C) mad((double)(A), (double)(B), (double)(C))
)";

#else

#define FLOAT_PRECISION "float"
#define CL_STD "-cl-std=CL1.2 -cl-mad-enable -cl-single-precision-constant -D INOPENCL -w"

static const std::string clHeader = R"(
#define REAL_FLOAT
#define REAL float
#define INT int
#define UINT uint
#define CHAR char
#define UCHAR uchar

// Alias double math definitions
#undef M_E
#undef M_LOG2E
#undef M_LOG10E
#undef M_LN2
#undef M_LN10
#undef M_PI
#undef M_PI_2
#undef M_PI_4
#undef M_1_PI
#undef M_2_PI
#undef M_2_SQRTPI
#undef M_SQRT2
#undef M_SQRT1_2

#define M_E         M_E_F
#define M_LOG2E     M_LOG2E_F
#define M_LOG10E    M_LOG10E_F
#define M_LN2       M_LN2_F
#define M_LN10      M_LN10_F
#define M_PI        M_PI_F
#define M_PI_2      M_PI_2_F
#define M_PI_4      M_PI_4_F
#define M_1_PI      M_1_PI_F
#define M_2_PI      M_2_PI_F
#define M_2_SQRTPI  M_2_SQRTPI_F
#define M_SQRT2     M_SQRT2_F
#define M_SQRT1_2   M_SQRT1_2_F

#define REALVEC2 float2
#define REALVEC3 float3
#define REALVEC4 float4

#define noData_REAL NAN
#define noData_INT INT_MAX
#define noData_UINT UINT_MAX
#define noData_UCHAR 0 // Only used for out-of-range projection values

#define isValid_REAL(A) ((A) == (A))
#define isValid_INT(A) ((A) != noData_INT)
#define isValid_UINT(A) ((A) != noData_UINT)
#define isValid_UCHAR(A) ((A) == (A)) // Always true, coded to suppress warning
#define isInvalid_REAL(A) ((A) != (A))
#define isInvalid_INT(A) ((A) == noData_INT)
#define isInvalid_UINT(A) ((A) == noData_UINT)
#define isInvalid_UCHAR(A) ((A) != (A)) // Always false, coded to suppress warning

#define degrees_DEF(A) degrees((float)(A))
#define radians_DEF(A) radians((float)(A))
#define sqrt_DEF(A) sqrt((float)(A))
#define exp_DEF(A) exp((float)(A))
#define sin_DEF(A) sin((float)(A))
#define cos_DEF(A) cos((float)(A))
#define tan_DEF(A) tan((float)(A))
#define asin_DEF(A) asin((float)(A))
#define acos_DEF(A) acos((float)(A))
#define atan_DEF(A) atan((float)(A))
#define atan2_DEF(A, B) atan2((float)(A), (float)(B))
#define log_DEF(A) log((float)(A))
#define pow_DEF(A, B) pow((float)(A), (float)(B))
#define max_DEF(A, B) fmax((float)(A), (float)(B))
#define min_DEF(A, B) fmin((float)(A), (float)(B))
#define max_int_DEF(A, B) max((int)(A), (int)(B))
#define min_int_DEF(A, B) min((int)(A), (int)(B))
#define max_uint_DEF(A, B) max((uint)(A), (uint)(B))
#define min_uint_DEF(A, B) min((uint)(A), (uint)(B))
#define clamp_DEF(A, B, C) clamp((float)(A), (float)(B), (float)(C))
#define clamp_int_DEF(A, B, C) clamp((int)(A), (int)(B), (int)(C))
#define clamp_uint_DEF(A, B, C) clamp((uint)(A), (uint)(B), (uint)(C))
#define mad_DEF(A, B, C) mad((float)(A), (float)(B), (float)(C))
)";

#endif

#endif
