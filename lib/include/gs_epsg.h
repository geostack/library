#ifndef GEOSTACK_EPSG_H
#define GEOSTACK_EPSG_H

#include <string>
#include "json11.hpp"

namespace Geostack
{
    extern const std::map<std::string, std::string> epsgDataset;
    
    std::string projParamsFromEPSG(std::string epsgCode);
}

#endif