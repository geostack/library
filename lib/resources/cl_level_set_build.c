/**
* Level set step build
*/

__kernel void build(
uint _sric,
uint _nric,
__global RasterIndex *_ri,
uint kn,
/*__ARGS__*/
) {

    const size_t _index = get_global_id(0);
    if (_index < _nric) {

        uint _i = (uint)(_ri+_sric+_index)->i;
        uint _j = (uint)(_ri+_sric+_index)->j;
        uint _k = (uint)(_ri+_sric+_index)->k;

        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/
/*__ADV_VARS__*/

        // Calculate derivative of level set for normed gradient
        REAL _dx = fmax(fmax(_distance-_distance_W, _distance-_distance_E)/_dim.hx, (REAL)0.0);
        REAL _dy = fmax(fmax(_distance-_distance_S, _distance-_distance_N)/_dim.hy, (REAL)0.0);
        REAL _mag = hypot(_dx, _dy);

        // Cell state is given by lower bit of classification buffer 
        uint _class_state = classbits&1;

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24;
    
        // Calculate upwind derivative of level set in wind direction
        if (advect_x > 0.0) {
            _dx = (_distance-_distance_W)/_dim.hx;
        } else if (advect_x < 0.0) {
            _dx = (_distance_E-_distance)/_dim.hx;
        } else {
            _dx = 0.5*(_distance_E-_distance_W)/_dim.hx;
        }
        
        if (advect_y > 0.0) {
            _dy = (_distance-_distance_S)/_dim.hy;
        } else if (advect_y < 0.0) {
            _dy = (_distance_N-_distance)/_dim.hy;
        } else {
            _dy = 0.5*(_distance_N-_distance_S)/_dim.hx;
        }
        
        REAL speed = 0.0; 

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

        // Set state
        classbits = (classbits&~1)|(_class_state&1);

        // Ensure speed is positive
        speed = fmax(speed, (REAL)0.0);

        // Boundary conditions
        if (!(classbits_W&1) && _distance > _distance_W)
            speed = 0.0;
        if (!(classbits_E&1) && _distance > _distance_E)
            speed = 0.0;
        if (!(classbits_S&1) && _distance > _distance_S)
            speed = 0.0;
        if (!(classbits_N&1) && _distance > _distance_N)
            speed = 0.0;

        // Set speed to zero if state is un-burnable
        if (_class_state == 0)
            speed = 0.0;

        // Update forcing
        _rate = -speed*_mag;

        // Time integrate with Euler step
        _distance_update = _distance+dt*_rate;

        // Store speed
        _speed = speed;

/*__POST__*/

    }

}

