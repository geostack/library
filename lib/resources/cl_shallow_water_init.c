/**
* Shallow water initialisation
*/
__kernel void init(
REAL ic2,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {

/*__VARS__*/

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

        // Set height nodata values to zero
        if (isInvalid_REAL(h)) h = 0.0;

        // Set unit discharge to zero
        uh = 0.0;
        vh = 0.0;

        // Calculate equilibrium values - velocity is zero
        // ref: eq. 2.6 Liu and Zhou (2014)
        //      Lattice Boltzmann approach to simulating a
        //      wetting-drying front in shallow flows 
        
        // Lattice stencil and indexes
        //
        // NW (8)  N (4)  NE (7)
        //       \   |   /
        //        \  |  /
        //         \ | /
        //          \|/
        // W (1) ----0---- E (3)
        //          /|\
        //         / | \
        //        /  |  \
        //       /   |   \
        // SW (5)  S (2)  SE (6)
        //
        
        // local variables
        const REAL g = 9.81;
        REAL gh2 = g*h*h;

        // added constant terms

        const REAL i3 = (REAL)(1.0/3.0);
        const REAL i6 = (REAL)(1.0/6.0);
        const REAL i8 = (REAL)(1.0/8.0);
        const REAL i12 = (REAL)(1.0/12.0);
        const REAL i24 = (REAL)(1.0/24.0);
        
        REAL u = 0.0;             // velocity vector component in x direction
        REAL v = 0.0;             // velocity vector component in y direction
        
        REAL c = 1.0 / sqrt(ic2);
        if (h > 0.0) {
            u = uh / h;
            v = vh / h;
        }

        // lattice centre
        _val3D_a(__fi, _i, _j, 0) = h - 5.0*i6*gh2*ic2 - 2.0*i3*h*ic2*u*u;
        // lattice edge centre locations
        _val3D_a(__fi, _i, _j, 1) = i6*gh2*ic2 - i3*h*ic2*c*u - i6*h*ic2*u*u;        // W
        _val3D_a(__fi, _i, _j, 2) = i6*gh2*ic2 - i6*h*ic2*u*u;                       // S
        _val3D_a(__fi, _i, _j, 3) = i6*gh2*ic2 + i3*h*ic2*c*u - i6*h*ic2*u*u;        // E
        _val3D_a(__fi, _i, _j, 4) = i6*gh2*ic2 - i6*h*ic2*u*u;                       // N
        // lattice diagonal locations
        _val3D_a(__fi, _i, _j, 5) = i24*gh2*ic2 - i12*h*ic2*c*u + i8*ic2*ic2*c*c*u*v - i24*h*ic2*u*u; //SW
        _val3D_a(__fi, _i, _j, 6) = i24*gh2*ic2 + i12*h*ic2*c*u - i8*ic2*ic2*c*c*u*v - i24*h*ic2*u*u; //SE
        _val3D_a(__fi, _i, _j, 7) = i24*gh2*ic2 + i12*h*ic2*c*u + i8*ic2*ic2*c*c*u*v - i24*h*ic2*u*u; //NE
        _val3D_a(__fi, _i, _j, 8) = i24*gh2*ic2 - i12*h*ic2*c*u - i8*ic2*ic2*c*c*u*v - i24*h*ic2*u*u; //NW

/*__POST__*/

    }

}

