/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
// Aligned access
#define _pval2D_a(R, _i, _j) (((R)+(_i)+(_j)*_dim.mx))
#define _val2D_a(R, _i, _j) (*((R)+(_i)+(_j)*_dim.mx))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

typedef struct RasterIndexStruct {

    // Coordinates
    uint id;
    ushort i;
    ushort j;
    REAL w;
    
} __attribute__ ((aligned (8))) RasterIndex;

// Function to test if bounding box b contains coordinate (x, y)
bool containsCoord2D(
    REAL bx_min, REAL by_min,
    REAL bx_max, REAL by_max,
    REAL x, REAL y) {
    return (x >= bx_min && x <= bx_max && y >= by_min && y <= by_max);
}

// Orientation test for three points
REAL orient2D(
    REAL x0, REAL y0, 
    REAL x1, REAL y1, 
    REAL x2, REAL y2) {
    return (x0-x2)*(y1-y2)-(y0-y2)*(x1-x2);
}

// Function to test if bounding box b intersects bounding box t
bool intersectsBoundingBox2D(
    REAL bx_min, REAL by_min,
    REAL bx_max, REAL by_max,
    REAL tx_min, REAL ty_min,
    REAL tx_max, REAL ty_max) {
    return (bx_min <= tx_max && bx_max >= tx_min && by_max >= ty_min && by_min <= ty_max);
}

// Function to test if two line segments (x0, y0), (x1, y1) and (x2, y2), (x3, y3) intercept
bool intercepts2D( 
    REAL x0, REAL y0, 
    REAL x1, REAL y1, 
    REAL x2, REAL y2, 
    REAL x3, REAL y3) {       
    
    // Degenerate cases
    REAL dx01 = x0-x1;
    REAL dx02 = x0-x2;
    REAL dx23 = x2-x3;
    if (dx01 == 0.0 && dx23 == 0.0) {
        if (dx02 != 0.0) return false;
        return max(y0, y1) >= min(y2, y3) && max(y2, y3) >= min(y0, y1);
    }
    REAL dy01 = y0-y1;
    REAL dy02 = y0-y2;
    REAL dy23 = y2-y3;
    if (dy01 == 0.0 && dy23 == 0.0) {
        if (dy02 != 0.0) return false;
        return max(x0, x1) >= min(x2, x3) && max(x2, x3) >= min(x0, x1);
    }
    
    // Check for intercept
    REAL d = dx01*dy23-dy01*dx23;
    REAL tn = dx02*dy23-dy02*dx23;
    if (tn*d >= 0.0 && fabs(tn) <= fabs(d)) {
        REAL un = dx02*dy01-dy02*dx01;
        if (un*d >= 0.0 && fabs(un) <= fabs(d)) {
            return true;
        }
    }
    return false;
}

/**
* Index points.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _id geometry index array.
* @param _ri Raster index structure array.
* @param _gi Global index counter.
* @param _n Number of coordinates.
*/
__kernel void indexPoints(
    __global TYPE *_u,
    const Dimensions _dim,
    __global Coordinate *_c,
    __global uint *_id,
    __global RasterIndex *_ri,
    __global uint *_gi,
    const uint _n/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);
    uint count = 0;

    // Check limits
    if (index < _n) {
    
        // Get coordinate
        Coordinate _pc = *(_c+index);
        
/*__PROJECT__
        // Convert to raster projection
        convert(&_pc, &_rproj, &_vproj);
__PROJECT__*/

        // Convert to raster coordinates
        _pc.p = (_pc.p-_dim.ox)/_dim.hx;
        _pc.q = (_pc.q-_dim.oy)/_dim.hy;

        // Check position
        RasterIndex ri;
        ri.id = *(_id+index);
        ri.i = 0;
        ri.j = 0;
        ri.w = 0.0;

        if (_pc.p >= 0.0 && _pc.q >= 0.0 && _pc.p < (REAL)_dim.nx && _pc.q < (REAL)_dim.ny) {

            // Get coordinates
            ri.i = (ushort)_pc.p;
            ri.j = (ushort)_pc.q;
            ri.w = 1.0;
        
            // Write to raster index
            *(_ri+atomic_inc(_gi)) = ri;
        }
    }
}

/**
* Index line strings.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _o Array of offsets denoting the start of each line string.
* @param _id geometry index array.
* @param _ri Raster index structure array.
* @param _gi Global index counter.
* @param _ln Number of line strings.
*/
__kernel void indexLineStrings(
    __global TYPE *_u,
    const Dimensions _dim,
    __global Coordinate *_c, 
    __global uint *_o,
    __global uint *_id,
    __global RasterIndex *_ri, 
    __global uint *_gi,
    const uint _ln/*__ARGS__*/
) {

    // Get line index
    const size_t index = get_global_id(0);
    uint count = 0;

    // Check limits
    if (index < _ln) {
    
        // Get offsets
        size_t soff = index > 0 ? _o[index-1]+1 : 0;
        size_t eoff = _o[index]+1; 
        uint id = _id[index];

        // Get coordinate
        Coordinate _lc = *(_c+soff);

/*__PROJECT__
        // Convert to raster projection
        convert(&_lc, &_rproj, &_vproj);
__PROJECT__*/

        // Convert to raster coordinates
        int sx, ex = (int)((_lc.p-_dim.ox)/_dim.hx);
        int sy, ey = (int)((_lc.q-_dim.oy)/_dim.hy);
    
        // Loop over lines
        for (size_t l = soff+1; l < eoff; l++) {
        
            // Get coordinates
            sx = ex;
            sy = ey;
            _lc = *(_c+l);            

/*__PROJECT__
            // Convert to raster projection
            convert(&_lc, &_rproj, &_vproj);
__PROJECT__*/

            // Convert to indexes
            ex = (int)((_lc.p-_dim.ox)/_dim.hx);
            ey = (int)((_lc.q-_dim.oy)/_dim.hy);

            // Calculate line points
            int dx = ex-sx;
            int dy = ey-sy;
            if (dx == 0 && dy == 0) {

                // Special case for single point
                if (sx >= 0 && sx < _dim.nx && sy >= 0 && sy < _dim.ny) {

                    // Write
                    RasterIndex ri;
                    ri.id = id;
                    ri.i = sx;
                    ri.j = sy;
                    ri.w = 1.0;
                    *(_ri+atomic_inc(_gi)) = ri;
                }

            } else {

                if (abs(dy) <= abs(dx) && ((sx > 0 && sx < _dim.nx) || (ex > 0 && ex < _dim.nx))) {

                    // Step in x
                    REAL m = (REAL)dy/(REAL)dx;
                    int inc = (dx > 0) ? 1 : -1;
                    for (int _i = sx ; _i != ex ; _i += inc) {
                        int _j = sy+(int)(m*(_i-sx));
                        if (_i >= 0 && _i < _dim.nx && _j >=0 && _j < _dim.ny) {
                        
                            // Write
                            RasterIndex ri;
                            ri.id = id;
                            ri.i = _i;
                            ri.j = _j;
                            ri.w = 1.0;
                            *(_ri+atomic_inc(_gi)) = ri;

                        }
                    }
                } else if (abs(dy) > abs(dx) && ((sy > 0 && sy < _dim.ny) || (ey > 0 && ey < _dim.ny))) {

                    // Step in y
                    REAL m = (REAL)dx/(REAL)dy;
                    int inc = (dy > 0) ? 1 : -1;
                    for (int _j = sy ; _j != ey ; _j += inc) {
                        int _i = sx+(int)(m*(_j-sy));
                        if (_i >= 0 && _i < _dim.nx && _j >=0 && _j < _dim.ny) {
                        
                            // Write
                            RasterIndex ri;
                            ri.id = id;
                            ri.i = _i;
                            ri.j = _j;
                            ri.w = 1.0;
                            *(_ri+atomic_inc(_gi)) = ri;
                        }
                    }
                }
            }
        }
    }
}

/**
* Index polygons.
* @param _u %Tile data.
* @param _dim %Dimensions structure.
* @param _c Array of coordinates.
* @param _o Array of offsets denoting the start of each polygon, must include terminator of 0.
* @param _b Array of polygon bounding boxes as coordinates.
* @param _id geometry index array.
* @param _ri Raster index structure array.
* @param _gi Global index counter.
* @param _n Total number of polygons
*/
__kernel void indexPolygons(
    __global TYPE *_u,
    const Dimensions _dim,
    __global REALVEC4 *_c,
    __global int *_o,
    __global REALVEC4 *_b,
    __global uint *_id,
    __global RasterIndex *_ri, 
    __global uint *_gi,
    const uchar _params,
    const uint _n/*__ARGS__*/
) {
      
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);

    // Intersection checks
    const uchar _interior = _params&0x01;
    const uchar _intersect = _params&0x02;

    // Check limits
    bool isValid = _n != 0 && _i < _dim.nx && _j < _dim.ny;
    if (isValid) {

        // Calculate cell bounding box
        REAL _bx_min = (REAL)_i*_dim.hx+_dim.ox;
        REAL _by_min = (REAL)_j*_dim.hy+_dim.oy;
        REAL _bx_max = _bx_min+_dim.hx;
        REAL _by_max = _by_min+_dim.hy;

/*__PROJECT__

        // Convert to vector projection
        Coordinate _b_min;
        _b_min.p = _bx_min;
        _b_min.q = _by_min;
        convert(&_b_min, &_vproj, &_rproj);
        
        Coordinate _b_max;
        _b_max.p = _bx_max;
        _b_max.q = _by_max;
        convert(&_b_max, &_vproj, &_rproj);

        _bx_min = min(_b_min.p, _b_max.p);
        _by_min = min(_b_min.q, _b_max.q);
        _bx_max = max(_b_min.p, _b_max.p);
        _by_max = max(_b_min.q, _b_max.q);

__PROJECT__*/

        // Sample point
        const REALVEC2 _p = (REALVEC2)(_bx_min, _by_min);

        // Internal variables
        bool _found = false; // Polygon interception found
        int _sign = 1;       // Sign of winding, 0 if outside polygon
        uint _k = 0;         // Coordinate offset
        uint _poly = 0;      // Total polygon count
        uint _top_poly = 0;  // Top-level polygon count

        // Vector coordinates
        REALVEC2 c0, c1;

        // Loop over polygons
        do {

            // Loop over rings
            do {

                // Process if cell is within polygon bounding box
                if (!_found && intersectsBoundingBox2D(_bx_min, _by_min, _bx_max, _by_max, 
                    _b[_poly*2].x, _b[_poly*2].y, _b[_poly*2+1].x, _b[_poly*2+1].y)) {

                    int _winding = 0;
                    uint _k_next = _k+abs(_o[_poly])-1;
                    while (_k < _k_next) {
            
                        // Get coordinates
                        c0 = _c[_k].xy;
                        c1 = _c[_k+1].xy;

                        if (orient2D(_bx_min, _by_min, _bx_min, _by_max, c0.x, c0.y) <= 0.0 &&
                            orient2D(_bx_min, _by_max, _bx_max, _by_max, c0.x, c0.y) <= 0.0 &&
                            orient2D(_bx_max, _by_max, _bx_max, _by_min, c0.x, c0.y) <= 0.0 &&
                            orient2D(_bx_max, _by_min, _bx_min, _by_min, c0.x, c0.y) <= 0.0) {

                            // Mark as found if any coordinate is within bounding box and intersection check is set
                            if (_intersect) _found = true;
                            _k = _k_next;
                            _winding = 0;
                            break;

                        } else if (
                            intercepts2D(_bx_min, _by_min, _bx_min, _by_max, c0.x, c0.y, c1.x, c1.y) || 
                            intercepts2D(_bx_min, _by_max, _bx_max, _by_max, c0.x, c0.y, c1.x, c1.y) || 
                            intercepts2D(_bx_max, _by_max, _bx_max, _by_min, c0.x, c0.y, c1.x, c1.y) ||
                            intercepts2D(_bx_max, _by_min, _bx_min, _by_min, c0.x, c0.y, c1.x, c1.y)) {

                            // Mark as found if any edge intercepts cell bounding box and intersection check is set
                            if (_intersect) _found = true;
                            _k = _k_next;
                            _winding = 0;
                            break;

                        } else if (_interior) {

                            // Check if bounding box point lies inside polygon
                            REALVEC2 l = c1-c0;
                            REALVEC2 v = _p-c0;

                            // Check _winding (http://geomalgorithms.com/a03-_inclusion.html)
                            if (c0.y <= _p.y) {
                                if (c1.y > _p.y && l.x*v.y > v.x*l.y)
                                    _winding++;
                            } else {
                                if (c1.y <= _p.y && l.x*v.y < v.x*l.y)
                                    _winding--;
                            }
                        }

                        // Increment coordinate
                        _k++;
                    }

                    // Increment coordinate
                    _k++;
            
                    // If the point is inside any polygon, flip the sign
                    if (_winding != 0) {
                        _sign = -_sign;
                    }

                } else {

                    // Skip polygon
                    _k+=abs(_o[_poly]);
                }

                // Increment polygon counter
                _poly++;

            } while(_o[_poly] < 0);

            // Increment top-level polygon counter
            _top_poly++;

            // Set index if any polygons are found
            if (_found || _sign < 0) {
                uint index = _top_poly-1;
                uint id = _id[index];

                // Write
                RasterIndex ri;
                ri.id = id;
                ri.i = _i;
                ri.j = _j;
                ri.w = _found ? 0.5 : 1.0;
                *(_ri+atomic_inc(_gi)) = ri;
            }

            // Reset flags
            _found = false;
            _sign = 1;

        } while( _poly < _n);
    }
}

