/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
// Aligned access
#define _val2D_a(R, i, j) (*((R)+(i)+(j)*_dim.mx))
#define _val3D_a(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim.my)*_dim.mx))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

/**
* Map distance from point to raster.
* @param _u %Tile data.
* @param _dim %Dimensions of _u.
* @param _cx Array of coordinates x positions.
* @param _cy Array of coordinates y positions.
* @param _p Array of offsets for properties.
* @param _n Number of coordinates.
*/
__kernel void map2DPointDistance(
    __global REAL *_u,
    const Dimensions _dim,
    __global REAL *_cx,
    __global REAL *_cy,
    __global uint *_p,
    const uint _n/*__ARGS__*/
) {

    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i >= _dim.nx || _j >= _dim.ny) {
        _val3D_a(_u, _i, _j, _k) = NAN;
        return;
    }
    
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)_i+0.5)*_dim.hx+_dim.ox, 
        ((REAL)_j+0.5)*_dim.hy+_dim.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, &_vproj, &_rproj);

__PROJECT__*/
        
    // Read initial value
    REAL output = NAN;
    output = _val3D_a(_u, _i, _j, _k);

/*__DENOM__
    REAL denom = 1.0;
__DENOM__*/
            
    // Loop over points
    for (uint index = 0; index < _n; index++) {

/*__VARS__*/

        // Calculate displacement vector
        const REALVEC2 _c = (REALVEC2)(_cx[index], _cy[index]);
        REALVEC2 d = p-_c;

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

        // TODO distance is a very rough approximation for GCS, use Haversine great circle formula
    }

/*__DENOM__
    output/=denom;
__DENOM__*/

    // Write to raster
    _val3D_a(_u, _i, _j, _k) = output;
}

/**
* Map distance from line strings to raster.
* @param _u %Tile data.
* @param _dim %Dimensions of _u.
* @param _cx Array of coordinates x positions.
* @param _cy Array of coordinates y positions.
* @param _p Array of offsets for properties.
* @param _o Array of offsets denoting the start of each line string.
* @param _n Length of offset array.
*/
__kernel void map2DLineStringDistance(
    __global REAL *_u,
    const Dimensions _dim,
    __global REAL *_cx,
    __global REAL *_cy,
    __global uint *_p, 
    __global uint *_o,
    const uint _n/*__ARGS__*/
) {
       
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i >= _dim.nx || _j >= _dim.ny) {
        _val3D_a(_u, _i, _j, _k) = NAN;
        return;
    }
    
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)_i+0.5)*_dim.hx+_dim.ox, 
        ((REAL)_j+0.5)*_dim.hy+_dim.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, &_vproj, &_rproj);

__PROJECT__*/

    // Read initial value
    REAL output = NAN;
    output = _val3D_a(_u, _i, _j, _k);
        
/*__DENOM__
    REAL denom = 1.0;
__DENOM__*/

    // Loop over lines
    uint _lastoff = 0; 
    for (uint index = 0; index < _n; _lastoff = _o[index]+1, index++) {

/*__VARS__*/

        for (uint _idx = _lastoff; _idx < _o[index]; _idx++) {

            // Get coordinates
            const REALVEC2 _c0 = (REALVEC2)(_cx[_idx], _cy[_idx]);
            const REALVEC2 _c1 = (REALVEC2)(_cx[_idx+1], _cy[_idx+1]);
            
            // Get segment
            REALVEC2 _l = _c1-_c0;
            REALVEC2 d = p-_c0;

            // Calculate Gram-Schmidt projection
            REAL _dlv = dot(_l, d);
            REAL _dll = dot(_l, _l);
            if (_dlv < 0.0 || _dlv > _dll) {

                // Projection onto line is outside line segment, use end points
        // ---------------------------------
        // User defined code
{
/*__CODE__*/
}
        // ---------------------------------
                d = p-_c1;
        // ---------------------------------
        // User defined code
{
/*__CODE__*/
}
        // ---------------------------------

                // TODO distance is a very rough approximation for GCS, user Haversine great circle formula

            } else {

                // Projection onto line is inside line segment, use distance to line
                d -= (_dlv/_dll)*_l;
        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------

                // TODO distance is a very rough approximation for GCS, user Haversine great circle formula
            }
        }
    }

/*__DENOM__
    output/=denom;
__DENOM__*/

    // Write to raster
    _val3D_a(_u, _i, _j, _k) = output;
}

/**
* Map distance from polygons to raster.
* @param _u %Tile data.
* @param _dim %Dimensions of _u.
* @param _cx Array of coordinates x positions.
* @param _cy Array of coordinates y positions.
* @param _p Array of offsets for properties.
* @param _o Array of offsets denoting the start of each polygon.
* @param _bx Array of polygon bounding box min and max x positions.
* @param _by Array of polygon bounding box min and max y positions.
* @param _n Length of offset array.
*/
__kernel void map2DPolygonDistance(
    __global REAL *_u,
    const Dimensions _dim,
    __global REAL *_cx,
    __global REAL *_cy,
    __global uint *_p, 
    __global uint *_o,
    __global REAL *_bx,
    __global REAL *_by,
    const uint _n/*__ARGS__*/
) {
      
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i >= _dim.nx || _j >= _dim.ny) {
        _val3D_a(_u, _i, _j, _k) = NAN;
        return;
    }
        
    // Cell centred position
    const REALVEC2 p = (REALVEC2)(
        ((REAL)_i+0.5)*_dim.hx+_dim.ox, 
        ((REAL)_j+0.5)*_dim.hy+_dim.oy);

/*__PROJECT__

    // Convert to vector projection
    convert(&p, &_vproj, &_rproj);

__PROJECT__*/

    // Read initial value
    REAL output = NAN;
    output = _val3D_a(_u, _i, _j, _k);

/*__DENOM__
    REAL denom = 1.0;
__DENOM__*/
        
    // Replace nodata with large value
    REAL _sign = 1.0;
    if (output != output) {
        output = MAXFLOAT;
    } else {
        _sign = output < 0.0 ? -1.0 : 1.0;
        output = output*output;
    }
        
    // Loop over polygons
    uint _lastoff = 0;  
    for (uint index = 0; index < _n; _lastoff = _o[index]+1, index++) {

/*__VARS__*/
        
        // Process if point is within polygon bounding box, 
        // or minimum distance to bounding box is less than current distance
        const REALVEC2 _b_min = (REALVEC2)(_bx[index*2], _by[index*2]);
        const REALVEC2 _b_max = (REALVEC2)(_bx[index*2+1], _by[index*2+1]);
        REALVEC2 _bc = p-clamp(p, _b_min, _b_max);
        if (dot(_bc, _bc) < output) {

            // Map polygon
            int _winding = 0;
            for (uint _idx = _lastoff; _idx < _o[index]; _idx++) {

                // Get coordinates
                const REALVEC2 _c0 = (REALVEC2)(_cx[_idx], _cy[_idx]);
                const REALVEC2 _c1 = (REALVEC2)(_cx[_idx+1], _cy[_idx+1]);
            
                // Get segment
                REALVEC2 _l = _c1-_c0;
                REALVEC2 _v = p-_c0;

                // Check winding (http://geomalgorithms.com/a03-_inclusion.html)
                if (_c0.y <= p.y) {
                    if (_c1.y > p.y && (_l.x*_v.y-_v.x*_l.y) > 0.0)
                        _winding++;
                } else {
                    if (_c1.y <= p.y && (_l.x*_v.y-_v.x*_l.y) < 0.0)
                        _winding--;
                }

                // Calculate Gram-Schmidt projection
                REAL _dlv = dot(_l, _v);
                REAL _dll = dot(_l, _l);
                if (_dlv < 0.0 || _dlv > _dll) {

                    // Projection onto line is outside line segment, use end points
                    REAL dval = dot(_v, _v);
                    if (dval < output) {
                        output = dval;
                    }
                    _v = p-_c1;
                    dval = dot(_v, _v);
                    if (dval < output) {
                        output = dval;
                    }

                    // TODO distance is a very rough approximation for GCS, use Haversine great circle formula

                } else {

                    // Projection onto line is inside line segment, use distance to line
                    REALVEC2 d = _v-(_dlv/_dll)*_l;
                    REAL dval = dot(d, d);
                    if (dval < output) {
                        output = dval;
                    }

                    // TODO distance is a very rough approximation for GCS, use Haversine great circle formula
                }
            }

            // If the point is inside any polygon, flip the sign
            if (_winding != 0)
                _sign = -_sign;
        }
    }

    if (output == MAXFLOAT) {
        output = NAN;
    } else {
        output = _sign*sqrt(output);

/*__DENOM__
        output/=denom;
__DENOM__*/

    }

    // Write to raster
    _val3D_a(_u, _i, _j, _k) = output;
}

