#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

/**
* Raster stippling count
* Counts number of points to create
*/
__kernel void stipple_count(
    __global uint *_ci,
    const uint _n,
/*__ARGS__*/
) {
      
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && _k < _dim.nz) {

        // Cell centred position
        REAL _x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        REAL _y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        REAL _z = get_global_size(2) == 1 ? 0.0 : ((REAL)_k+0.5)*_dim.hz+_dim.oz;
        
/*__VARS__*/
/*__VARS2__*/
        
        // Loop over number of points
        for (uint _count = 0; _count < _n; _count++) {

            // Copy variables
            const uint count = _count;
            REAL x = _x;
            REAL y = _y;
            REAL z = _z;
            REAL t = 0.0;

            // Set create flag
            bool create = false;

            // ---------------------------------
            // User defined code
/*__CODE__*/
            // ---------------------------------

            // Creation condition
            if (create) {
                atomic_inc(_ci);
            }
        }
        
/*__POST__*/

    }
}

/**
* Raster stippling
* Creates up to _n Points within each cell based on the create flag
*/
__kernel void stipple(
    __global Coordinate *_c,
    __global ulong *_cidx,
    __global uint *_ci,
    const uint _n,
    const uint _tnx,
    const uint _tny,
    const uint _tx,
    const uint _ty/*__ARGS2__*/
/*__ARGS__*/
) {
      
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);

    // Check limits
    if (_i < _dim.nx && _j < _dim.ny && _k < _dim.nz) {

        // Cell centred position
        REAL _x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        REAL _y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        REAL _z = get_global_size(2) == 1 ? 0.0 : ((REAL)_k+0.5)*_dim.hz+_dim.oz;
        
/*__VARS__*/
/*__VARS2__*/
        
        // Loop over number of points
        for (uint _count = 0; _count < _n; _count++) {

            // Copy variables
            const uint count = _count;
            REAL x = _x;
            REAL y = _y;
            REAL z = _z;
            REAL t = 0.0;

            // Set create flag
            bool create = false;

            // ---------------------------------
            // User defined code
/*__CODE__*/
            // ---------------------------------

            // Creation condition
            if (create) {

                // Get index
                uint _idx = atomic_inc(_ci);

                ulong _ti = _i+_tx*_dim.mx;
                ulong _tj = _j+_ty*_dim.my;
                _cidx[_idx] = (ulong)_count+(_ti+(_tj+(ulong)_k*_tny)*_tnx)*_n;

                _c[_idx].p = x;
                _c[_idx].q = y;
                _c[_idx].r = z;
                _c[_idx].s = t;

/*__POST2__*/

            }
        }
        
/*__POST__*/

    }
}