/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinates
    REAL p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

/*__RK_COEFF__*/

/**
* Hermite function
* @param t Hermite parameter
* @param p0 position at t = 0
* @param p1 position at t = 1
* @param d0 derivative at t = 0
* @param d1 derivative at t = 1
* @return Hermite function
*/
REALVEC3 Hermite(REAL t, REALVEC3 p0, REALVEC3 p1, REALVEC3 d0, REALVEC3 d1) {

    REAL t3 = t*t*t;
    REAL t2 = t*t;
    REAL coeff_1 = (2.0*t3-3.0*t2+1.0);
    REAL coeff_2 = (t3-2.0*t2+t);	
    REAL coeff_3 = (-2.0*t3+3.0*t2);
    REAL coeff_4 = (t3-t2);
    return (REALVEC3)(coeff_1, coeff_1, coeff_1) * p0 +
           (REALVEC3)(coeff_2, coeff_2, coeff_2) * d0 +
           (REALVEC3)(coeff_3, coeff_3, coeff_3) * p1 +
           (REALVEC3)(coeff_4, coeff_4, coeff_4) * d1;
}

/**
* Hermite derivative function
* @param t Hermite parameter
* @param p0 position at t = 0
* @param p1 position at t = 1
* @param d0 derivative at t = 0
* @param d1 derivative at t = 1
* @return derivative of Hermite function
*/
REALVEC3 dHermite(REAL t, REALVEC3 p0, REALVEC3 p1, REALVEC3 d0, REALVEC3 d1) {

    REAL t2 = t*t;
    REAL coeff_1 = 6.0*(t2-t);
    REAL coeff_2 = (3.0*t2-4.0*t+1.0);
    REAL coeff_3 = (3.0*t2-2.0*t);
    return (REALVEC3)(coeff_1, coeff_1, coeff_1) * (p0-p1) + 
           (REALVEC3)(coeff_2, coeff_2, coeff_2) * d0 + 
           (REALVEC3)(coeff_3, coeff_3, coeff_3) * d1;
}

/**
* Particle acceleration function
* @param position particle position vector
* @param velocity particle velocity vector
* @param advect particle advection vector
* @param time particle time
* @param radius particle radius
* @return instantaneous acceleration
*/
inline REALVEC3 acceleration_fn(REALVEC3 position, REALVEC3 velocity, REALVEC3 advect, REAL time, REAL radius/*__FN_ARGS__*/) {
    REALVEC3 acceleration = (REALVEC3)((REAL)0.0, (REAL)0.0, (REAL)0.0);
    
/*__FN_VARS__*/

    // ---------------------------------
    // User defined code
/*__ACC_CODE__*/
    // ---------------------------------

    return acceleration;
}

/**
* Particle velocity function
* @param position particle position vector
* @param advect particle advection vector
* @param time particle time
* @param radius particle radius
* @return instantaneous velocity
*/
inline REALVEC3 velocity_fn(REALVEC3 position, REALVEC3 advect, REAL time, REAL radius/*__FN_ARGS__*/) {
    REALVEC3 velocity = (REALVEC3)((REAL)0.0, (REAL)0.0, (REAL)0.0);
    
/*__FN_VARS__*/

    // ---------------------------------
    // User defined code
/*__VEL_CODE__*/
    // ---------------------------------

    return velocity;
}

/**
* Particle update operation using acceleration
* @param _cp particle position vector
* @param _vp particle position vector
* @param _n number of particles
* @param dt timestep
* @param _ci plane intersection count scalar
* @param _pi plane intersection index vector
*/
__kernel void updateAcceleration(
    __global Coordinate *_cp,
    __global Coordinate *_vp,
    const uint _n,
    const REAL dt,
    __global RandomState *_rs,
    __global uint *_pi,
    __global uint *_ci/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);

    // Check limits
    if (index < _n) {

        // Get random state
        RandomState _rsl = *(_rs+index); // Random state
        RandomState *_prsl = &_rsl;      // Random state pointer
    
        // Get variables
        Coordinate _c = *(_cp+index);
        Coordinate _v = *(_vp+index);

        REAL time = _c.s;
        REAL radius = _v.s;  
        REALVEC3 position = (REALVEC3)(_c.p, _c.q, _c.r);
        REALVEC3 velocity = (REALVEC3)(_v.p, _v.q, _v.r);    
        REALVEC3 advect = (REALVEC3)(0.0, 0.0, 0.0);  
        
/*__VARS__*/

/*__SAMPLE__
        REALVEC3 sample_centre = (REALVEC3)(__S_C_X__, __S_C_Y__, __S_C_Z__);
        REALVEC3 sample_normal = (REALVEC3)(__S_N_X__, __S_N_Y__, __S_N_Z__);
__SAMPLE__*/

        // ---------------------------------
        // User defined code
/*__INIT1__*/
        // ---------------------------------

        if (radius >= 0.0) {

            // Adaptive Runge-Kutta (assumes first-same-as-last and B is the same as the last row of A)
            int _nsteps = 0;
            REAL _adt = dt;
            REAL _error = 0.0;
            REALVEC3 _position_initial, _position_update;
            REALVEC3 _velocity_initial, _velocity_update;
            REALVEC3 _acceleration_initial;
            REALVEC3 _k1, _k2, _k3, _k4;
            REALVEC3 _l1, _l2, _l3, _l4;

            _position_initial = position;
            _velocity_initial = velocity;
            _acceleration_initial = acceleration_fn(position, velocity, advect, time, radius/*__FN_CALL_ARGS__*/);

            do {

                // Set initial variables
                _k4 = _acceleration_initial;
                _l4 = _velocity_initial;
                position = _position_initial;
                velocity = _velocity_initial;
                REAL _time = time;

                for (int c = 0; c < (1<<_nsteps); c++) {

                    _k1 = _k4;
                    _l1 = _l4;                
                    _velocity_update = velocity+_adt*_A21*_k1;
                    _position_update = position+_adt*_A21*_l1;

                    _k2 = acceleration_fn(_position_update, _velocity_update, advect, _time+_C2*_adt, radius/*__FN_CALL_ARGS__*/);
                    _l2 = _velocity_update;                    
                    _velocity_update = velocity+_adt*(_A31*_k1+_A32*_k2);
                    _position_update = position+_adt*(_A31*_l1+_A32*_l2);

                    _k3 = acceleration_fn(_position_update, _velocity_update, advect, _time+_C3*_adt, radius/*__FN_CALL_ARGS__*/);
                    _l3 = _velocity_update;                    
                    _velocity_update = velocity+_adt*(_A41*_k1+_A42*_k2+_A43*_k3);
                    _position_update = position+_adt*(_A41*_l1+_A42*_l2+_A43*_l3);

                    _k4 = acceleration_fn(_position_update, _velocity_update, advect, _time+_C4*_adt, radius/*__FN_CALL_ARGS__*/);
                    _l4 = _velocity_update;

                    // Calculate error
                    _error = length(_velocity_update-(velocity+_adt*(_B1*_k1+_B2*_k2+_B3*_k3+_B4*_k4)));
                    if (_error > __TOLERANCE__ && _nsteps < __MAX_STEPS__)
                        break;

                    position = _position_update;
                    velocity = _velocity_update;
                    _time += _adt;
                }

                // Update timestep
                _adt*=0.5;
                _nsteps++;

            } while(_error > __TOLERANCE__ && _nsteps <= __MAX_STEPS__);
            
            bool sample_plane_cross = false;
            REAL sample_dt = dt;
            REALVEC3 position_sample_plane = (REALVEC3)(noData_REAL, noData_REAL, noData_REAL);
/*__SAMPLE__
            // Calculate point-plane intersection
            if (dot(_position_initial-sample_centre, sample_normal)*
                dot(position-sample_centre, sample_normal) <= 0.0) {
            
                // Store index
                sample_plane_cross = true;
                uint _idx = atomic_inc(_ci);
                _pi[_idx] = index;
            
                // Fit cubic Bezier to curve
                REALVEC3 d0 = dt*_velocity_initial;
                REALVEC3 d1 = dt*velocity;

                // Enforce monotonicity
                REALVEC3 delta = position-_position_initial;
                REAL mag = ((d0.x*d0.x+d1.x*d1.x)/(delta.x*delta.x+1.0E-6))+
                           ((d0.y*d0.y+d1.y*d1.y)/(delta.y*delta.y+1.0E-6))+
                           ((d0.z*d0.z+d1.z*d1.z)/(delta.z*delta.z+1.0E-6));
                if (mag > 9.0) {
                    REAL tau = 3.0/sqrt(mag);
                    d0 *= tau;
                    d1 *= tau;
                }

                // Newton
                REAL t = 0.5;
                REAL t_last = t;
                int iters = 0;
                do {
                    position_sample_plane = Hermite(t, _position_initial, position, d0, d1);
                    t_last = t;
                    t -= dot(position_sample_plane-sample_centre, sample_normal)/
                        dot(dHermite(t, _position_initial, position, d0, d1), sample_normal);

                } while(fabs(t-t_last) > __TOLERANCE__ && iters++ < __MAX_STEPS__);
                sample_dt *= t;
            }
__SAMPLE__*/

            // ---------------------------------
            // User defined code
/*__POST1__*/
            // ---------------------------------

        }

/*__POST2__*/

        // Store particle parameters
        _c.p = position.x;
        _c.q = position.y;
        _c.r = position.z;
        _c.s = time;
        _v.p = velocity.x;
        _v.q = velocity.y;
        _v.r = velocity.z;
        _v.s = radius;

        *(_cp+index) = _c;
        *(_vp+index) = _v;

        // Store state
        *(_rs+index) = _rsl;
    }
}

/**
* Particle update operation using velocity
* @param _cp particle position vector
* @param _vp particle position vector
* @param _n number of particles
* @param dt timestep
* @param _ci plane intersection count scalar
* @param _pi plane intersection index vector
*/
__kernel void updateVelocity(
    __global Coordinate *_cp,
    __global Coordinate *_vp,
    const uint _n,
    const REAL dt,
    __global RandomState *_rs,
    __global uint *_pi,
    __global uint *_ci/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);

    // Check limits
    if (index < _n) {

        // Get random state
        RandomState _rsl = *(_rs+index); // Random state
        RandomState *_prsl = &_rsl;      // Random state pointer
    
        // Get variables
        Coordinate _c = *(_cp+index);
        Coordinate _v = *(_vp+index);

        REAL time = _c.s;
        REAL radius = _v.s;  
        REALVEC3 position = (REALVEC3)(_c.p, _c.q, _c.r);
        REALVEC3 velocity = (REALVEC3)(_v.p, _v.q, _v.r);    
        REALVEC3 advect = (REALVEC3)(0.0, 0.0, 0.0);  
        
/*__VARS__*/

/*__SAMPLE__
        REALVEC3 sample_centre = (REALVEC3)(__S_C_X__, __S_C_Y__, __S_C_Z__);
        REALVEC3 sample_normal = (REALVEC3)(__S_N_X__, __S_N_Y__, __S_N_Z__);
__SAMPLE__*/

        // ---------------------------------
        // User defined code
/*__INIT1__*/
        // ---------------------------------

        if (radius >= 0.0) {

            // Adaptive Runge-Kutta (assumes first-same-as-last and B is the same as the last row of A)
            int _nsteps = 0;
            REAL _adt = dt;
            REAL _error = 0.0;
            REALVEC3 _position_initial, _position_update;
            REALVEC3 _velocity_initial;
            REALVEC3 _k1, _k2, _k3, _k4;

            _position_initial = position;
            _velocity_initial = velocity_fn(position, advect, time, radius/*__FN_CALL_ARGS__*/);

            do {

                // Set initial variables
                _k4 = _velocity_initial;
                position = _position_initial;
                velocity = _velocity_initial;
                REAL _time = time;

                for (int c = 0; c < (1<<_nsteps); c++) {

                    _k1 = _k4;                
                    _position_update = position+_adt*_A21*_k1;

                    _k2 = velocity_fn(_position_update, advect, _time+_C2*_adt, radius/*__FN_CALL_ARGS__*/);
                    _position_update = position+_adt*(_A31*_k1+_A32*_k2);

                    _k3 = velocity_fn(_position_update, advect, _time+_C3*_adt, radius/*__FN_CALL_ARGS__*/);
                    _position_update = position+_adt*(_A41*_k1+_A42*_k2+_A43*_k3);

                    _k4 = velocity_fn(_position_update, advect, _time+_C4*_adt, radius/*__FN_CALL_ARGS__*/);

                    // Calculate error
                    _error = length(_position_update-(position+_adt*(_B1*_k1+_B2*_k2+_B3*_k3+_B4*_k4)));
                    if (_error > __TOLERANCE__ && _nsteps < __MAX_STEPS__)
                        break;

                    position = _position_update;
                    _time += _adt;
                }

                // Update timestep
                _adt*=0.5;
                _nsteps++;

            } while(_error > __TOLERANCE__ && _nsteps <= __MAX_STEPS__);
            
            bool sample_plane_cross = false;
            REAL sample_dt = dt;
            REALVEC3 position_sample_plane = (REALVEC3)(noData_REAL, noData_REAL, noData_REAL);
/*__SAMPLE__
            // Calculate point-plane intersection
            if (dot(_position_initial-sample_centre, sample_normal)*
                dot(position-sample_centre, sample_normal) <= 0.0) {
            
                // Store index
                sample_plane_cross = true;
                uint _idx = atomic_inc(_ci);
                _pi[_idx] = index;
            
                // Fit cubic Bezier to curve
                REALVEC3 d0 = dt*_velocity_initial;
                REALVEC3 d1 = dt*velocity;

                // Enforce monotonicity
                REALVEC3 delta = position-_position_initial;
                REAL mag = ((d0.x*d0.x+d1.x*d1.x)/(delta.x*delta.x+1.0E-6))+
                           ((d0.y*d0.y+d1.y*d1.y)/(delta.y*delta.y+1.0E-6))+
                           ((d0.z*d0.z+d1.z*d1.z)/(delta.z*delta.z+1.0E-6));
                if (mag > 9.0) {
                    REAL tau = 3.0/sqrt(mag);
                    d0 *= tau;
                    d1 *= tau;
                }

                // Newton
                REAL t = 0.5;
                REAL t_last = t;
                int iters = 0;
                do {
                    position_sample_plane = Hermite(t, _position_initial, position, d0, d1);
                    t_last = t;
                    t -= dot(position_sample_plane-sample_centre, sample_normal)/
                        dot(dHermite(t, _position_initial, position, d0, d1), sample_normal);

                } while(fabs(t-t_last) > __TOLERANCE__ && iters++ < __MAX_STEPS__);
                sample_dt *= t;
            }
__SAMPLE__*/

            // ---------------------------------
            // User defined code
/*__POST1__*/
            // ---------------------------------

        }

/*__POST2__*/

        // Store particle parameters
        _c.p = position.x;
        _c.q = position.y;
        _c.r = position.z;
        _c.s = time;
        _v.p = velocity.x;
        _v.q = velocity.y;
        _v.r = velocity.z;
        _v.s = radius;

        *(_cp+index) = _c;
        *(_vp+index) = _v;

        // Store state
        *(_rs+index) = _rsl;
    }
}

/**
* Particle update with no dynamics
* @param _cp particle position vector
* @param _vp particle position vector
* @param _n number of particles
* @param dt timestep
* @param _ci plane intersection count scalar
* @param _pi plane intersection index vector
*/
__kernel void updateNone(
    __global Coordinate *_cp,
    __global Coordinate *_vp,
    const uint _n,
    const REAL dt,
    __global RandomState *_rs,
    __global uint *_pi,
    __global uint *_ci/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);

    // Check limits
    if (index < _n) {

        // Get random state
        RandomState _rsl = *(_rs+index); // Random state
        RandomState *_prsl = &_rsl;      // Random state pointer
    
        // Get variables
        Coordinate _c = *(_cp+index);
        Coordinate _v = *(_vp+index);

        REAL time = _c.s;
        REAL radius = _v.s;  
        REALVEC3 position = (REALVEC3)(_c.p, _c.q, _c.r);
        REALVEC3 velocity = (REALVEC3)(_v.p, _v.q, _v.r);    
        REALVEC3 advect = (REALVEC3)(0.0, 0.0, 0.0);  
        
/*__VARS__*/

/*__SAMPLE__
        REALVEC3 sample_centre = (REALVEC3)(__S_C_X__, __S_C_Y__, __S_C_Z__);
        REALVEC3 sample_normal = (REALVEC3)(__S_N_X__, __S_N_Y__, __S_N_Z__);
__SAMPLE__*/

        // ---------------------------------
        // User defined code
/*__INIT1__*/
        // ---------------------------------

        if (radius >= 0.0) {          
            bool sample_plane_cross = false;
            REAL sample_dt = dt;
            REALVEC3 position_sample_plane = (REALVEC3)(noData_REAL, noData_REAL, noData_REAL);
/*__SAMPLE__
            // Calculate point-plane intersection
            if (dot(position-sample_centre, sample_normal) == 0.0) {
            
                // Store index
                sample_plane_cross = true;
                uint _idx = atomic_inc(_ci);
                _pi[_idx] = index;
                position_sample_plane = position;
            }
__SAMPLE__*/

            // ---------------------------------
            // User defined code
/*__POST1__*/
            // ---------------------------------

        }

/*__POST2__*/

        // Store particle parameters
        _c.p = position.x;
        _c.q = position.y;
        _c.r = position.z;
        _c.s = time;
        _v.p = velocity.x;
        _v.q = velocity.y;
        _v.r = velocity.z;
        _v.s = radius;

        *(_cp+index) = _c;
        *(_vp+index) = _v;

        // Store state
        *(_rs+index) = _rsl;
    }
}

inline REALVEC3 mu_fn(REALVEC3 position, REALVEC3 advect) {
    REALVEC3 mu = (REALVEC3)((REAL)0.0, (REAL)0.0, (REAL)0.0);

    // ---------------------------------
    // User defined code
/*__CODE2__*/
    // ---------------------------------

    return mu;
}

inline REALVEC3 sigma_fn(REALVEC3 position, REALVEC3 advect) {
    REALVEC3 sigma = (REALVEC3)((REAL)0.0, (REAL)0.0, (REAL)0.0);

    // ---------------------------------
    // User defined code
/*__CODE3__*/
    // ---------------------------------

    return sigma;
}

/**
* Particle stochastic update operation
*/
__kernel void updateStochastic(
    __global Coordinate *_cp,
    const uint _n,
    const REAL dt,
    __global RandomState *_rs/*__ARGS__*/
) {

    // Get coordinate index
    const size_t index = get_global_id(0);

    // Check limits
    if (index < _n) {
    
        // Get variables
        Coordinate _c = *(_cp+index);

        // Get random state
        RandomState _rsl = *(_rs+index); // Random state
        RandomState *_prsl = &_rsl;        // Random state pointer

        REAL time = _c.s;
        REALVEC3 position = (REALVEC3)(_c.p, _c.q, _c.r);  
        REALVEC3 advect = (REALVEC3)(0.0, 0.0, 0.0);  
        
/*__VARS__*/

    // ---------------------------------
    // User defined code
/*__INIT2__*/
    // ---------------------------------

        // Stochastic Euler
        REALVEC3 mu = mu_fn(position, advect);
        REALVEC3 sigma = sigma_fn(position, advect);
        REALVEC3 dW = (REALVEC3)(0.0, 0.0, 0.0); 

    // ---------------------------------
    // User defined code
/*__CODE4__*/
    // ---------------------------------

        position += mu*dt+sigma*dW;
        
/*__POST2__*/

        // Store particle parameters
        _c.p = position.x;
        _c.q = position.y;
        _c.r = position.z;
        _c.s = time;

        *(_cp+index) = _c;

        // Store state
        *(_rs+index) = _rsl;
    }
}

