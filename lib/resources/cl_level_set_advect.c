/**
* Level set step advect
*/

REAL advectiveMean_REAL(
    REAL v_default,
    REAL u,
    REAL u_N,
    REAL u_NE,
    REAL u_E,
    REAL u_SE,
    REAL u_S,
    REAL u_SW,
    REAL u_W,
    REAL u_NW) {

    // Calculate variable from neighbours
    REAL v = u;
    if (isInvalid_REAL(u)) {
        REAL n = 0.0;
        v = 0.0;
        if (isValid_REAL(u_N)) {
            v += u_N;
            n += 1.0;
        }
        if (isValid_REAL(u_NE)) {
            v += u_NE;
            n += 1.0;
        }
        if (isValid_REAL(u_E)) {
            v += u_E;
            n += 1.0;
        }
        if (isValid_REAL(u_SE)) {
            v += u_SE;
            n += 1.0;
        }
        if (isValid_REAL(u_S)) {
            v += u_S;
            n += 1.0;
        }
        if (isValid_REAL(u_SW)) {
            v += u_SW;
            n += 1.0;
        }
        if (isValid_REAL(u_W)) {
            v += u_W;
            n += 1.0;
        }
        if (isValid_REAL(u_NW)) {
            v += u_NW;
            n += 1.0;
        }
        if (n > 0.0) {
            v /= n;
        } else {                
            v = v_default;
        }
    }
    return v;
}

REAL advectiveMax_UINT(
    UINT v_default,
    UINT u,
    UINT u_N,
    UINT u_NE,
    UINT u_E,
    UINT u_SE,
    REAL u_S,
    UINT u_SW,
    UINT u_W,
    UINT u_NW) {

    // Calculate variable from neighbours
    UINT v = u;
    if (isInvalid_UINT(u)) {
        if (isValid_UINT(u_N)) {
            v = isValid_UINT(v) ? max_DEF(v, u_N) : u_N;
        }
        if (isValid_UINT(u_NE)) {
            v = isValid_UINT(v) ? max_DEF(v, u_NE) : u_NE;
        }
        if (isValid_UINT(u_E)) {
            v = isValid_UINT(v) ? max_DEF(v, u_E) : u_E;
        }
        if (isValid_UINT(u_SE)) {
            v = isValid_UINT(v) ? max_DEF(v, u_SE) : u_SE;
        }
        if (isValid_UINT(u_S)) {
            v = isValid_UINT(v) ? max_DEF(v, u_S) : u_S;
        }
        if (isValid_UINT(u_SW)) {
            v = isValid_UINT(v) ? max_DEF(v, u_SW) : u_SW;
        }
        if (isValid_UINT(u_W)) {
            v = isValid_UINT(v) ? max_DEF(v, u_W) : u_W;
        }
        if (isValid_UINT(u_NW)) {
            v = isValid_UINT(v) ? max_DEF(v, u_NW) : u_NW;
        }
        if (isInvalid_UINT(v)) {
            v = v_default;
        }
    }
    return v;
}

#define advectiveMean_REAL_DEF(v_default, u) advectiveMean_REAL(v_default, u, u##_N, u##_NE, u##_E, u##_SE, u##_S, u##_SW, u##_W, u##_NW)
#define advectiveMax_UINT_DEF(v_default, u) advectiveMax_UINT(v_default, u, u##_N, u##_NE, u##_E, u##_SE, u##_S, u##_SW, u##_W, u##_NW)

__kernel void advect(
uint _c,
__global uint *_ric,
__global RasterIndex *_ri,
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24;

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // --------------------------------- 

        // Set default rate to zero
        _rate = 0.0;
        
        // Time integrate with Euler step
        _distance_update = _distance;

        // Store speed
        _speed = 0.0;  
        
        // Store narrow band indexes
        if (fabs(_distance) < band_width) {        

            // Create narrow band index
            RasterIndex ri;
            ri.i = (ushort)_i;
            ri.j = (ushort)_j;
            ri.k = (ushort)_k;
/*__ADV_VARS__*/

            // Add to narrow band index
            atomic_inc(_ric+_c+1);
            *(_ri+atomic_inc(_ric)) = ri;
        }   

/*__POST__*/

    }
}

__kernel void advectInactive(
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    const size_t _k = get_global_id(2);
    
    // Sizes
    const size_t kn = get_global_size(2);
    
    // Check limits
    if (_i < _dim.nx && _j < _dim.ny) {
    
        // Cell centred position
        const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
        const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;
        const REAL z = ((REAL)_k+0.5)*_dim.hz+_dim.oz;

/*__VARS__*/

        // Cell classification is given by bits 1-24 bits of classification buffer 
        uint _class_lo = (classbits&0xFFFFFE)>>1;

        // Cell sub-classification is given by bits 24-32 of classification buffer 
        uint _class_sub_lo = classbits>>24; 

        // ---------------------------------
        // User defined code
/*__CODE__*/
        // ---------------------------------  

/*__POST__*/

    }
}

