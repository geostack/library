## Geostack

# Geospatial toolkit

Geostack is a toolkit for high performance geospatial processing, modelling and analysis.

Some highlights of Geostack include:
- Range of programmable geospatial operations based on OpenCL, including [map algebra](https://gitlab.com/geostack/library/-/wikis/Raster%20creation%20and%20manipulation%20(Python)), [distance mapping](https://gitlab.com/geostack/library/-/wikis/Distance%20maps%20(Python)) and [rasterisation](https://gitlab.com/geostack/library/-/wikis/Rasterisation%20(Python)).
- Data IO for common geospatial types such as [geotiff](https://gitlab.com/geostack/library/-/wikis/Reading%20and%20writing%20Raster%20layers%20(Python)) and shapefiles with no dependencies.
- Implicit handling geospatial alignment and projections, allowing easier coding of geospatial models.
- [Python bindings](https://gitlab.com/geostack/library/-/wikis/Rasters%20and%20numpy%20(Python)) for interoperability with GDAL/RasterIO/xarray/NetCDF.
- Built-in computational solvers including [level set](https://gitlab.com/geostack/library/-/wikis/Level%20set%20solver%20(Python)) and network flow models.

More information and build guides are on our [wiki](https://gitlab.com/geostack/library/-/wikis/home).

Geostack can be installed for Python [using conda](https://gitlab.com/geostack/library/-/wikis/Installing%20Geostack%20for%20Python%20using%20conda).

The C++ and Python bindings documentation is located [here](https://geostack.gitlab.io/library/). These are the low-level documentations of the geostack API. For examples, please checkout the wiki pages mentioned above.
