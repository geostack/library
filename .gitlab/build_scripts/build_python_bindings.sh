#!/usr/bin/env bash

set -x

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# MacOS does not have these tools installed on its image.
if [[ "$OSTYPE" == "darwin"* ]]; then
    brew install coreutils cmake ninja pocl libomp gcc@14
    chmod +x ${SCRIPT_DIR}/install_miniconda_macos.sh && ${SCRIPT_DIR}/install_miniconda_macos.sh
fi

source ${SCRIPT_DIR}/env.sh

rm -rf ${BINDINGS_BUILD_DIR} && mkdir -p ${BINDINGS_BUILD_DIR}

cmake -G Ninja -S ${SOURCE_DIR}/bindings -B ${BINDINGS_BUILD_DIR} \
      ${CMAKE_USE_GCC[@]} \
      -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
      -DGeostack_DIR=${INSTALL_PREFIX}/lib/cmake/Geostack/ \
      -DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
      -DPYTHON=${PYTHON} \
      -DCYTHON_BUILD_FLAG=true

pushd ${BINDINGS_BUILD_DIR}/python/src

# Build Python bindings
${PIP} install --verbose --editable . || exit 1

# Test Python bindings
pytest -sxv -k 'not 1d_setter and not 1d_getter' geostack --junitxml=${BUILD_DIR}/pytest_report.xml

# If build-wheel is requested:
if [ "$4" == "build-wheel" ]; then
    # Build wheel.
    ${PYTHON} -m build --wheel --no-isolation || exit 1

    # Fix/delocate the wheel.
    if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
        ${SOURCE_DIR}/scripts/fix_wheel.sh ./dist ${INSTALL_PREFIX}/wheelhouse
    elif [[ "$OSTYPE" == "darwin"* ]]; then
        WHEEL_FILE=$(basename $(ls -t dist/*.whl | head -n 1))
        ${PYTHON} -m delocate.cmd.delocate_wheel --wheel-dir=${INSTALL_PREFIX}/wheelhouse --lib-sdir=.libs dist/${WHEEL_FILE} || exit 1
    fi
fi

popd
