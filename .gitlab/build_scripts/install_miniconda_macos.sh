#!/usr/bin/env bash

# Do nothing if there is already miniconda
[[ -d ~/miniconda ]] && exit 0

# Prepare env with conda
mkdir -p ~/miniconda
curl https://repo.anaconda.com/miniconda/Miniconda3-latest-MacOSX-$(uname -m).sh -o ~/miniconda/miniconda.sh
bash ~/miniconda/miniconda.sh -b -u -p ~/miniconda
rm -rf ~/miniconda/miniconda.sh
eval "$(/$HOME/miniconda/bin/conda shell.bash hook)"
conda init
conda config --set auto_activate_base false
conda deactivate
conda create --name gs_build --yes
source ~/miniconda/bin/activate gs_build

set -x

[[ -z ${GS_WITH_PYTHON_VERSION} ]] && _PYTHON_VERSION=3.10 || _PYTHON_VERSION=${GS_WITH_PYTHON_VERSION}
conda install -y -n gs_build -c conda-forge -y ld64=609 python=${_PYTHON_VERSION} \
   pytest numpy cython build requests delocate patchelf
