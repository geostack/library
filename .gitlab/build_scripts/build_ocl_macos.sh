#!/usr/bin/env bash

set -o xtrace

mkdir -p ${BUILD_DIR}/ocl
pushd ${BUILD_DIR}/ocl

git clone --branch v2022.01.04 https://github.com/KhronosGroup/OpenCL-ICD-Loader
git clone --branch v2022.01.04 https://github.com/KhronosGroup/OpenCL-Headers

cmake -G Ninja -D CMAKE_INSTALL_PREFIX=./OpenCL-Headers/install \
      ${CMAKE_USE_GCC[@]} \
      -S ./OpenCL-Headers \
      -B ./OpenCL-Headers/build \
      -D CMAKE_INSTALL_PREFIX=${INSTALL_PREFIX}
cmake --build ./OpenCL-Headers/build --target install

cmake -G Ninja -D CMAKE_PREFIX_PATH=${PWD}/OpenCL-Headers/install \
      ${CMAKE_USE_GCC[@]} \
      -D OPENCL_ICD_LOADER_HEADERS_DIR=${INSTALL_PREFIX}/include \
      -D CMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
      -S ./OpenCL-ICD-Loader \
      -B ./OpenCL-ICD-Loader/build
cmake --build ./OpenCL-ICD-Loader/build --target install --config Release

echo "Geostack wheel includes Khronos Group OpenCL-ICD-Loader which is licensed as below" >> ${INSTALL_PREFIX}/LICENSE
cat ./OpenCL-ICD-Loader/LICENSE >> ${INSTALL_PREFIX}/LICENSE

popd
rm -rf ${BUILD_DIR}/ocl
