#!/usr/bin/env bash

set -x

# We are not using our prebuilt Docker image nor installing Miniconda to emulate a blank test environment
if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
    apt-get update
    apt-get install -y python3.10 python3-pip libpocl2
elif [[ "$OSTYPE" == "darwin"* ]]; then
    brew install python@3.10 pocl
fi
python3.10 -m pip install twine

SOURCE_DIR=$( realpath $1 )
INSTALL_PREFIX=$( realpath $2 )
REPOSITORY=$3

function do_upload_wheel() {
    local repository username password repo_url

    set +x

    repository=$1

    if [[ "${repository}" == "gitlab" ]]; then
        repo_url=https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi
        username=gitlab-ci-token
        password=${CI_JOB_TOKEN}
    elif [[ "${repository}" == "testpypi" ]]; then
        username=${TESTPYPI_USER}
        password=${TESTPYPI_TOKEN}
    elif [[ "${repository}" == "pypi" ]]; then
        username=${PYPI_USER}
        password=${PYPI_TOKEN}
    fi

    # Prepare the ~/.pypirc file with the proper credential needed by twine to upload the wheel file
    cat << EOF > ~/.pypirc
[distutils]
index-servers =
    ${repository}

[${repository}]
username = ${username}
password = ${password}
EOF

    if [ ! -z ${repo_url} ]; then
        echo "repository = ${repo_url}" >> ~/.pypirc
    fi

    set -x

    python3.10 ${SOURCE_DIR}/scripts/twine_upload.py \
               --wheel_directory ${INSTALL_PREFIX}/wheelhouse \
               --repository ${repository} || exit 1
}

# Upload wheel file to Gitlab repo.
do_upload_wheel gitlab || exit 1

# Test installing the wheel from Gitlab repo.
python3.10 -m pip install geostack-py \
           --index-url https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/pypi/simple || exit 1

# Prepare the environment to test the wheel
if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
    # Set up the pocl hack because we are running on Ubuntu 22.04 and LLVM reports the CPU as 'generic'
    bash ${SOURCE_DIR}/docker/docker_deps_scripts/pocl-hack.sh
    export LD_PRELOAD=/root/pocl-hack.so
elif [[ "$OSTYPE" == "darwin"* ]]; then
    export OCL_ICD_VENDORS=$(brew --prefix)/etc/OpenCL/vendors
fi
# Run the test
python3.10 $( find ${SOURCE_DIR} -name test_random_sort.py | head -n 1 ) || exit 1

# Upload to testpypi/pypi
do_upload_wheel ${REPOSITORY} || exit 1
