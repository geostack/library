#!/usr/bin/env bash

mkdir -p $1
mkdir -p $2
export SOURCE_DIR=$(realpath $1)
export BUILD_DIR=$(realpath $2)
export BUILD_TYPE=$3
export LIBRARY_BUILD_DIR=${BUILD_DIR}/build_library
export BINDINGS_BUILD_DIR=${BUILD_DIR}/py_build
export INSTALL_PREFIX=${BUILD_DIR}/install
export GEOSTACK_HOME=${INSTALL_PREFIX}/lib

if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
    export LD_LIBRARY_PATH=${GEOSTACK_HOME}
    export OCL_ICD_VENDORS=/etc/OpenCL/vendors
elif [[ "$OSTYPE" == "darwin"* ]]; then
    export DYLD_LIBRARY_PATH=${GEOSTACK_HOME}
    BREW_PREFIX=$(brew --prefix)
    # For Apple CLANG, OpenMP (libomp) is also installed by brew
    [[ -d ${BREW_PREFIX}/opt/libomp ]] && export OpenMP_ROOT=${BREW_PREFIX}/opt/libomp
    # Check if we have GCC, it's better than Apple Clang
    if [[ ( -f "${BREW_PREFIX}/bin/gcc-14" ) && ( -f "${BREW_PREFIX}/bin/g++-14" ) ]]; then
        export CC=${BREW_PREFIX}/bin/gcc-14
        export CXX=${BREW_PREFIX}/bin/g++-14
        export CMAKE_USE_GCC=(-DCMAKE_C_COMPILER=${CC} -DCMAKE_CXX_COMPILER=${CXX})
	# GCC already comes with OpenMP
	unset OpenMP_ROOT
    fi
    # On MacOS pocl installs ICD vendor in ${BREW_PREFIX}/etc/OpenCL/vendors
    [[ -d ${BREW_PREFIX}/etc/OpenCL/vendors ]] && export OCL_ICD_VENDORS=${BREW_PREFIX}/etc/OpenCL/vendors
else
    echo "Unsupported OS type ${OSTYPE}"
fi

if [ -d ~/miniconda/envs/gs_build ]; then
    source ~/miniconda/bin/activate gs_build
    export PY=$(python -c "import sys; print(f'python{sys.version_info.major}.{sys.version_info.minor}')")
    export PYTHON=`which python`
    export PIP=`which pip`
    export PYTHONPATH=${GEOSTACK_HOME}/${PY}/site-packages
fi

# Hack for pocl on Ubuntu 22.04 not knowing recent CPUs
if [ -f /root/pocl-hack.so ]; then
    export LD_PRELOAD=/root/pocl-hack.so
fi
