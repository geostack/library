#!/usr/bin/env bash

set -x

# MacOS runner on Gitlab is a preinstalled image and does not have these tools.
if [[ "$OSTYPE" == "darwin"* ]]; then
    brew install coreutils cmake ninja pocl libomp gcc@14
fi

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${SCRIPT_DIR}/env.sh

# Build ICD loader
# For Ubuntu we have prebuilt and tarred it into the Docker image.
# MacOS runner on Gitlab does not have it, so it needs to be built.
if [[ "${OSTYPE}" == "linux-gnu"* ]]; then
    mkdir -p ${INSTALL_PREFIX}
    pushd ${INSTALL_PREFIX}
    tar -xvf /root/gs_build.tar.xz
    popd
elif [[ "$OSTYPE" == "darwin"* ]]; then
    chmod +x ${SCRIPT_DIR}/build_ocl_macos.sh && ${SCRIPT_DIR}/build_ocl_macos.sh
fi

rm -rf ${LIBRARY_BUILD_DIR} && mkdir -p ${LIBRARY_BUILD_DIR}

cmake -G Ninja -S ${SOURCE_DIR} -B ${LIBRARY_BUILD_DIR} \
      ${CMAKE_USE_GCC[@]} \
      -DCMAKE_INSTALL_PREFIX=${INSTALL_PREFIX} \
      -DBUILD_SHARED=ON \
      -DUSE_TILE_CACHING=true \
      -DFORCE_COLORED_OUTPUT=true \
      -DCMAKE_BUILD_TYPE=${BUILD_TYPE} || exit 1
cmake --build ${LIBRARY_BUILD_DIR} --config ${BUILD_TYPE} --target install || exit 1
