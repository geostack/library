#!/usr/bin/env bash

set -x

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
source ${SCRIPT_DIR}/env.sh

# Generate Python doc
sphinx-build -b html ${SOURCE_DIR}/docs/python/source ${INSTALL_PREFIX}/docs/python

# Generate C++ doc
pushd ${SOURCE_DIR}
doxygen ${INSTALL_PREFIX}/docs/Doxygen.Geostack

# Copy root static index page
cp -f docs/index.html ${INSTALL_PREFIX}/docs/

# Publish docs
mkdir -p ${SOURCE_DIR}/public
cp -Rf ${INSTALL_PREFIX}/docs/* ${SOURCE_DIR}/public/
popd
