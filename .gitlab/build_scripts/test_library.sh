#!/usr/bin/env bash

set -x

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

# MacOS does not have the pocl driver and OpenMP installed on its image.
if [[ "$OSTYPE" == "darwin"* ]]; then
    brew install pocl libomp
fi

source ${SCRIPT_DIR}/env.sh

${INSTALL_PREFIX}/bin/GeostackTest -r junit > ${BUILD_DIR}/report.xml
