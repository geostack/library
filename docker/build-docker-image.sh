#!/bin/bash

cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null

docker-compose build
