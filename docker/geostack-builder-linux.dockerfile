# Base image
FROM ubuntu:22.04

# Author
LABEL description="Geostack Docker Builder image"

# Set up localization
ENV TZ Australia/Melbourne
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
WORKDIR /var/tmp/Geostack

# Install build tools
RUN apt-get update && \
    apt-get install -y vim wget cmake g++ git curl build-essential autoconf make ninja-build libtool ruby coreutils libpocl2 doxygen graphviz && \
    rm -rf /var/lib/apt/lists/*

# Install dependencies
ADD docker_deps_scripts docker_deps_scripts
# ICD Loader
RUN chmod +x docker_deps_scripts/build_icd_loader_linux.sh && \
    mkdir -p build/install && \
    docker_deps_scripts/build_icd_loader_linux.sh build/install > build/install/build_deps.log 2> build/install/build_deps.err
# Miniconda
RUN chmod +x docker_deps_scripts/install-miniconda.sh && \
    docker_deps_scripts/install-miniconda.sh
# POCL hack
RUN chmod +x docker_deps_scripts/pocl-hack.sh && \
    docker_deps_scripts/pocl-hack.sh

# Package the dependencies
RUN bash -xc "pushd build/install && tar cf /root/gs_build.tar.xz * && popd"
