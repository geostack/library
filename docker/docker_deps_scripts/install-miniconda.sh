#!/usr/bin/env bash

set -e -x

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O ./miniconda3.sh
bash ./miniconda3.sh -b -p $HOME/miniconda
eval "$(/$HOME/miniconda/bin/conda shell.bash hook)"
conda init
conda config --set auto_activate_base false
conda deactivate
conda create --name gs_build --yes

# install conda packages
conda install -y -n gs_build -c conda-forge -y python=3.10 numpy cython build pytest \
   libgcc-ng=12.3.0 sphinx sphinx_rtd_theme numpydoc nbsphinx \
   requests auditwheel patchelf
