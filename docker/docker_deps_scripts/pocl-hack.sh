#!/usr/bin/env bash

# Both Ubuntu 20.04 and 22.04 ship with with a pretty old version
# of LLVM. However, GitHub Actions runners currently seem to run
# on 3rd gen Ryzen CPUs. Those old versions of LLVM don't recognize
# them, so `llvm::sys::getHostCPUName()` returns "generic".

# This "generic" model is then passed by POCL as the target CPU
# for builds, but it doesn't appear to work, and fails with:
# > error: unknown target CPU 'generic'

# Newer versions of POCL include a way to override the CPU:
# https://github.com/pocl/pocl/commit/5cb5f67434f74565d327a84ad54703c222d45feb
# (This commit is also included in POCL >= 3.0 in Debian patches)
# However, the one that ships with Ubuntu 20.04 and 22.04 is also
# pretty old and doesn't include this override feature

# So, to hack around this, we monkey-patch `llvm::sys::getHostCPUName()`
# in a very hacky way to return "x86-64", which is a CPU model
# that works for LLVM builds.

# Source: https://github.com/joanbm/lib842/blob/a82d393678fea9e56fb610bc1acf4068f88ca7e4/.github/workflows/build-and-test.yml#L65

set -e -x

cat << EOF > pocl-hack.c
#include <stddef.h>
struct FakeStringRef {
    const char *data;
    size_t len;
};
struct FakeStringRef _ZN4llvm3sys14getHostCPUNameEv() {
    return (struct FakeStringRef){ .data = "x86-64", .len = 6 };
}
EOF

gcc pocl-hack.c -fPIC -shared -o /root/pocl-hack.so
