import re
import shlex
import subprocess
from pathlib import Path
from argparse import ArgumentParser
from typing import Union, Optional
import shutil


def upload_wheel(wheel: str, repository: Optional[str]) -> Union[bool, str]:
    """_summary_

    _extended_summary_

    Parameters
    ----------
    wheel : str
        _description_
    repository : Optional[str]
        _description_

    Returns
    -------
    Union[bool, str]
        _description_
    """
    if repository is not None:
        rc = subprocess.Popen(shlex.split(f"twine upload --verbose --repository {repository} {wheel}"),
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)
    else:
        rc = subprocess.Popen(shlex.split(f"twine upload {wheel}"),
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)
    stdout, stderr = rc.communicate()
    stdout = stdout.decode()
    stderr = stderr.decode()

    if re.search('error', stdout, re.IGNORECASE):
        if (re.search('400 Bad request', stdout, re.IGNORECASE) or
            re.search('400 Bad request', stderr, re.IGNORECASE)):
            return False, stdout, stderr
        else:
            return None, stdout, stderr
    else:
        return True, stdout, stderr


WHEEL_INFO_RE = re.compile(
    r"""^(?P<namever>(?P<name>.+?)-(?P<ver>\d.*?))(-(?P<build>\d.*?))?
     -(?P<pyver>[a-z].+?)-(?P<abi>.+?)-(?P<plat>.+?)(\.whl|\.dist-info)$""",
    re.VERBOSE,
).match

parser = ArgumentParser(description='script to load package using twine')
parser.add_argument('--wheel_directory', type=str, dest='wheel_directory',
                    required=True)
parser.add_argument('--repository', type=str, dest='repository',
                    choices=['testpypi', 'pypi', 'gitlab', None])

args = parser.parse_args()

# find built wheels
if Path(args.wheel_directory).exists() and Path(args.wheel_directory).is_dir():
    wheel_files = Path(args.wheel_directory).glob("*.whl")
else:
    raise NotADirectoryError(args.wheel_directory + "is not a valid path")

# upload wheel
for wheel in wheel_files:
    count = 1
    rc = False
    while rc is False and count < 100:
        match = WHEEL_INFO_RE(wheel.name)
        print(f"Attempting to upload {wheel.name}")
        rc, out, err = upload_wheel(wheel, args.repository)
        if rc is None:
            raise RuntimeError(out + "\n" + err)
        elif not rc:
            print(f"Upload attempt {count} failed: {err}")
            #update build number
            if match.group("build") is None:
                build_number = f"{1:02d}"
            else:
                build_number = int(match.group("build")) + 1
                build_number = f"{build_number:02d}"
            # create new name
            new_wheel_name = "{name}-{ver}-{build}-{pyver}-{abi}-{plat}.whl".format(
                name=match.group("name"),
                ver=match.group("ver"),
                build=build_number,
                pyver=match.group("pyver"),
                abi=match.group("abi"),
                plat=match.group("plat")
            )
            # copy to new name
            shutil.copy(wheel, Path(wheel.parent, new_wheel_name))
            # replace wheel name
            wheel = Path(wheel.parent, new_wheel_name)
            count += 1
        else:
            print(f"Upload successful: {out}")

    if rc is False:
        exit(1)