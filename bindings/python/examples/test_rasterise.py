import numpy as np
from geostack.vector import Vector

v = Vector()
polyPointsIn1 = []
polyPointsOut1 = []
for i in range (0, 10):
    if i&1 == 1:
        r = 1.0
    else:
        r = 0.5;
    polyPointsOut1.append( [ (r*np.sin(i*2.0*np.pi/10.0)), (-r*np.cos(i*2.0*np.pi/10.0)) ] )
    polyPointsIn1.append( [ (0.6*r*np.sin(i*2.0*np.pi/10.0)), (-0.6*r*np.cos(i*2.0*np.pi/10.0)) ] )

polyPointsIn2 = []
polyPointsOut2 = []
for i in range (0, 26):
    if i&1 == 1:
        r = 0.8
    else:
        r = 0.6;
        
    polyPointsOut2.append( [ (0.6+r*np.sin(0.1+(i*2.0*np.pi/26.0))), (-0.6+r*np.cos(0.1+(i*2.0*np.pi/26.0))) ] )
    polyPointsIn2.append( [ (0.6+0.8*r*np.sin(0.1+(i*2.0*np.pi/26.0))), (-0.6+0.8*r*np.cos(0.1+(i*2.0*np.pi/26.0))) ] )

id1 = v.addPolygon( [ polyPointsOut1, polyPointsIn1 ] )
id2 = v.addPolygon( [ polyPointsOut2, polyPointsIn2 ] )
v.setProperty(id1, "test", 1.0)
v.setProperty(id2, "test", 2.0)

v.to_geojson("poly.geojson", False)

# Rasterise
r = v.rasterise(0.02, "output = test;")
r.write("poly.tif")