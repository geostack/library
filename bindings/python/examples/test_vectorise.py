# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import sys
import numpy as np

from geostack.raster import Raster
from geostack.runner import runScript
from geostack.io import vectorToGeoJson

# Create raster layer
A = Raster(name = "A")

# Initialize Raster
A.init(nx = 200, ny = 200, hx = 1.0, hy = 1.0, ox = -100.0, oy = -100.0)

runScript("A = hypot(x, y)-25.0;", [A])

# Write Raster
A.write('./_out_vectorise_raster.tiff')

# Generate contours
isochroneVector = A.vectorise(
    np.arange(0, 75, 25), 75)

# Write contour file
with open('./_out_vectorise.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(isochroneVector, enforceProjection = False))

print("Done")

