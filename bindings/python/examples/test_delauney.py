# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import random
from geostack.vector import Vector
from geostack.io import vectorToGeoJson, vectorToShapefile
from geostack.definitions import GeometryType
from geostack.core import ProjectionParameters

# Create random point set
p = Vector()
for i in range(0, 100):
    p.addPoint( [ random.uniform(-1.0, 1.0), random.uniform(-1.0, 1.0) ] )

with open("_points.geojson", 'w') as fp:
    fp.write(vectorToGeoJson(p, enforceProjection = False))
    
# Create Delauney triangulation
d = p.convert(GeometryType.Polygon)

with open("_points_delauney.geojson", 'w') as fp:
    fp.write(vectorToGeoJson(d, enforceProjection = False))
    
# Create Delauney hull
h = d.convert(GeometryType.Polygon)

with open("_points_delauney_hull.geojson", 'w') as fp:
    fp.write(vectorToGeoJson(h, enforceProjection = False))