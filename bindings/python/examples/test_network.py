import numpy as np
import json

from geostack.vector import Vector
from geostack.io import geoJsonToVector
from geostack.solvers import NetworkFlowSolver

import plotly.graph_objects as go

# Create network
network = Vector()

network.addPoint( [1, 1] )
network.addPoint( [2, 0] )
network.addPoint( [2, 2] )

network.addLineString( [ [0, 0], [1, 1] ] )
network.addLineString( [ [1, 1], [2, 0] ] )
network.addLineString( [ [2, 0], [3, 1] ] )
network.addLineString( [ [1, 1], [2, 2] ] )
network.addLineString( [ [2, 2], [3, 2] ] )

# Set inflow
network.setProperty(0, "flow", 0.1)

# Create solver
networkConfig = {
    "constant": 100.0,
    "defaultLinkType": 1,
    "defaultDiameter": 1
}
networkFlowSolver = NetworkFlowSolver()
networkFlowSolver.init(network, json.dumps(networkConfig))

# Run solver
networkFlowSolver.run() 

# Get internal network from flow solver
network = networkFlowSolver.getNetwork()

node_x = []
node_y = []
node_head = []
node_flow = []
for i in network.getPointIndexes():
    pc = network.getPointCoordinate(i)

    # Create list of junctions
    node_x.append(pc[0])
    node_y.append(pc[1])

    # Get pressure head and flow
    node_head.append(network.getProperty(i, "head", float))
    node_flow.append(network.getProperty(i, "flow", float))
    
link_x = []
link_y = []
link_flow = []
for i in network.getLineStringIndexes(): 
    lc = network.getLineStringCoordinates(i)
    
    # Create terminated list of links
    link_x.append(lc[0][0])
    link_x.append(lc[1][0])
    link_x.append(None)
    link_y.append(lc[0][1])
    link_y.append(lc[1][1])
    link_y.append(None)

    # Get flow in link
    link_flow.append(network.getProperty(i, "flow", float))

# Create junction plot
cmin = np.min(node_head)
cmax = np.max(node_head)
nodes = go.Scatter(mode='markers', x=node_x, y=node_y, name='head',
    marker = {'colorscale': 'Magma', 'cmin': cmin, 'cmax': cmax, 'size': 15, 'color': node_head})

# Create link plot
links = go.Scatter(mode='lines', x=link_x, y=link_y, name='network', 
    line = {'width': 2, 'color': '#888'})

# Draw plot
layout = go.Layout(
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)'
)
fig = go.Figure(data=[links, nodes], layout=layout)
fig.show()



