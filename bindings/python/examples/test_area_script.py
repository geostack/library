# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import numpy as np

from geostack.raster import Raster
from geostack.runner import runScript, runAreaScript

# Create raster layer
A = Raster(name = "A")

# Initialize raster layer
A.init(nx = 10, ny = 10, hx = 1.0, hy = 1.0)

# Populate raster layer
runScript("A = x > 3 && x < 7 && y > 3 && y < 7 ? 1.0 : 0.0;", [A])

# Output data
np.set_printoptions(precision=2)
print("Input data:")
print(A.data)

# Run area script
output = runAreaScript('''
    if (isValid_REAL(A)) {
        if (isValid_REAL(output)) {
            output += A; 
            sum += 1.0;
        } else {
            output = A; 
            sum = 1.0;
        }
    }
    ''', A, 1);

# Output data
print("Average:")
print(output.data)

# Populate raster layer
runScript("A = x > 3 && x < 7 && y > 3 && y < 7 ? 1.0 : 0.0;", [A])

# Run area script
output = runAreaScript('''
    output = max(A, output); 
    ''', A, 1);

# Output data
print("Dilation:")
print(output.data)

# Populate raster layer
runScript("A = x > 3 && x < 7 && y > 3 && y < 7 ? 1.0 : 0.0;", [A])

# Run area script
output = runAreaScript('''
    if (isValid_REAL(A)) {
        REAL ssigma = 10.0;
        REAL csigma = 1.0/(2.0*ssigma);
        REAL G = exp(-csigma*dist_sqr)/(2.0*M_PI*ssigma);
        
        if (isValid_REAL(output)) {
            output += G*A; 
            sum += G;
        } else {
            output = G*A; 
            sum = G;
        }
    }
    ''', A, 1);

# Output data
print("Gaussian :")
print(output.data)