#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: cdivision=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libcpp.vector cimport vector
from cpython.ref cimport Py_INCREF
import numpy as np
cimport numpy as np
from ..core.cpp_tools cimport MatrixXd

np.import_array()

cpdef test(int nx, int ny):
    cdef MatrixXd m = MatrixXd.Random(nx, ny)
    print(m(0, 0))

cdef void set_matrix_element(MatrixXd& m, int row, int col, double elm):
    cdef double* d = &(m(row,col))
    d[0] = elm

cdef StdVectorSentinel _create_sentinel(vector_typed * vect_ptr):
    """
    ref: https://gist.github.com/jjerphan/6b90a9691e67444adc7a8a658747ebe1
    """
    if vector_typed is vector[float64_t]:
        return dbl_vector_as_array.create_for(vect_ptr)
    elif vector_typed is vector[float32_t]:
        return flt_vector_as_array.create_for(vect_ptr)
    elif vector_typed is vector[int32_t]:
        return i32_vector_as_array.create_for(vect_ptr)
    elif vector_typed is vector[int64_t]:
        return i64_vector_as_array.create_for(vect_ptr)
    elif vector_typed is vector[uint32_t]:
        return u32_vector_as_array.create_for(vect_ptr)
    elif vector_typed is vector[uint64_t]:
        return u64_vector_as_array.create_for(vect_ptr)
    else:  # intp_t
        return intp_vector_as_array.create_for(vect_ptr)

cdef class StdVectorSentinel:
    """Wraps a reference to a vector which will be deallocated with this object.

    When created, the StdVectorSentinel swaps the reference of its internal
    vectors with the provided one (vect_ptr), thus making the StdVectorSentinel
    manage the provided one's lifetime.

    ref: https://gist.github.com/jjerphan/6b90a9691e67444adc7a8a658747ebe1
    """

    cdef void* get_data(self):
        """Return pointer to data."""

    cdef int get_typenum(self):
        """Get typenum for PyArray_SimpleNewFromData."""

include "_cy_asarray.pxi"

cdef class intp_vector_as_array(StdVectorSentinel):

    cdef vector[intp_t] buf

    @staticmethod
    cdef StdVectorSentinel create_for(vector[intp_t] * vect_ptr):
        cdef intp_vector_as_array sentinel = intp_vector_as_array.__new__(
            intp_vector_as_array)
        sentinel.buf.swap(deref(vect_ptr))
        return sentinel

    cdef void* get_data(self):
        return self.buf.data()

    cdef int get_typenum(self):
        return np.NPY_INTP

cdef np.ndarray vector_to_nd_array(vector_typed * vect_ptr):
    """
    ref: https://gist.github.com/jjerphan/6b90a9691e67444adc7a8a658747ebe1
    """
    cdef:
        np.npy_intp size = deref(vect_ptr).size()
        StdVectorSentinel sentinel = _create_sentinel(vect_ptr)
        np.ndarray arr = np.PyArray_SimpleNewFromData(
            1, &size, sentinel.get_typenum(), sentinel.get_data())

    # Makes the numpy array responsible of the life-cycle of its buffer.
    # A reference to the StdVectorSentinel will be stolen by the call to
    # `PyArray_SetBaseObject` below, so we increase its reference counter.
    # See: https://docs.python.org/3/c-api/intro.html#reference-count-details
    Py_INCREF(sentinel)
    np.PyArray_SetBaseObject(arr, sentinel)
    return arr