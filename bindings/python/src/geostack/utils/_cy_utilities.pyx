#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: cdivision=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref, preincrement as inc
from libc.math cimport fabs, isnan, ceil, floor, fmin, fmax
from libcpp.vector cimport vector
from libcpp.algorithm cimport sort
from libcpp.limits cimport numeric_limits
from libcpp cimport bool
from libcpp.string cimport string
from libc.string cimport memset
import numpy as np
cimport numpy as np
cimport cython
from cython cimport floating

np.import_array()


ctypedef string (*proj4_getter) (void*, size_t)

cdef extern from "gs_utilities.h" namespace "Geostack":
    void callback_Proj4FromEPSG(void*, proj4_getter, size_t)

cdef inline string epsgCode_to_proj4(void* func_p, size_t epsgCode):
    return (<object> func_p)(epsgCode)

cpdef proj4_from_epsg(object inp_function, size_t epsgCode):
    callback_Proj4FromEPSG(<void*> inp_function, epsgCode_to_proj4, epsgCode)

# needs to be compiled with C++
cdef floating sort_cpp(floating[:] a, int idx, bool is_sorted) except *:
    # a must be c continuous (enforced with [::1])
    cdef int i, nx
    cdef vector[floating] temp
    nx = <int> a.shape[0]
    if not is_sorted:
        temp.reserve(nx)
        temp.assign(&a[0], &a[0]+nx)
        sort(temp.begin(), temp.end())
        return temp[idx]
    else:
        return a[idx]

cpdef floating c_percentile(floating[:] inp_data, floating p, bool is_sorted) except *:
    if p < 0 or p > 100:
        return np.nan
    else:
        return c_quantile(inp_data, p * 0.01, is_sorted)

cpdef floating c_quantile(floating[:] inp_data, floating q, bool is_sorted) except *:
    cdef floating out
    cdef int nz, i
    cdef float a, b, fuzz, nppm, j, h, right_elem, left_elem

    if q < 0 or q > 1:
        return np.nan

    a = 1.0/3.0
    b = 1.0/3.0
    nz = <int> inp_data.shape[0]
    fuzz = 4.0 * numeric_limits[floating].epsilon()
    nppm = a + q * (nz + 1 - a - b) - 1
    j = floor(nppm + fuzz)
    if fabs(nppm - j) <= fuzz:
        h = 0
    else:
        h = nppm - j
    left_elem = <int>fmax(0, fmin(<int> j + 1, nz-1))
    right_elem = <int>fmax(0, fmin(<int> j, nz-1))
    if h == 1:
        out = sort_cpp(inp_data, <int>right_elem, is_sorted)
    elif h == 0:
        out = sort_cpp(inp_data, <int>left_elem, is_sorted)
    else:
        out = (1 - h) * sort_cpp(inp_data, <int>left_elem, is_sorted) + h * sort_cpp(inp_data, <int>right_elem, is_sorted)
    return out