#!/bin/bash

# script to generate test files for validating time
# processing

set -e

function create_test_files() {
  local FREQUENCY
  FREQUENCY=$1

cat >test_${FREQUENCY}.cdl <<EOF
netcdf test_${FREQUENCY} {   // example of CDL notation
  dimensions:
	  lon = 2 ;
	  lat = 2 ;
    time = UNLIMITED ; //(2 currently)
  variables:
    float time(time) ;
      time:units = "${FREQUENCY} since 1971-01-01 00:00:00" ;
      time:calendar = "proleptic_gregorian";
    float lon(lon) ;
      lon:units = "degrees_east" ;
      lon:standard_name = "longitude" ;
    float lat(lat) ;
      lat:units = "degrees_north" ;
      lat:standard_name = "latitude" ;
	  float rh(time, lon, lat) ;
		  rh:units = "percent" ;
		  rh:long_name = "Relative humidity" ;
  // global attributes
	  :title = "Simple example, lacks some conventions" ;
  data:
   time = -1, 1;
   lon = 110.0, 160.0;
   lat = -45.0, -10.0;
   rh =
    2, 3, 5, 7, 8, 9, 10, 11;
}
EOF

  ncgen test_${FREQUENCY}.cdl -o test_${FREQUENCY}.nc
  rm test_${FREQUENCY}.cdl
}

export -f create_test_files

for freq in seconds minutes hours days weeks months years
do
  echo "Writing file for $freq"
  create_test_files $freq
done