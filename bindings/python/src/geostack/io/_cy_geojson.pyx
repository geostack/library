# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libc.stdio cimport printf
from libcpp.string cimport string as cpp_string
from libcpp cimport bool
import numpy as np
cimport numpy as np

np.import_array()

include "_cy_geojson.pxi"
