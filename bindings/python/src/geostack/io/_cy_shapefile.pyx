# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libc.stdio cimport printf
from libcpp.string cimport string
import numpy as np
cimport numpy as np

np.import_array()

cdef GeometryType map_geom_type(int geom_type):
    if geom_type == 0:
        return GeometryType.NoType
    elif geom_type == 1:
        return GeometryType.Point
    elif geom_type == 1 << 1:
        return GeometryType.LineString
    elif geom_type == 1 << 2:
        return GeometryType.Polygon
    elif geom_type == 0x07:
        return GeometryType.All
    elif geom_type == 1 << 3:
        return GeometryType.TileType

include "_cy_shapefile.pxi"