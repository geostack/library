# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libcpp.string cimport string
from libc.stdio cimport printf
cimport cython
import numpy as np
cimport numpy as np

np.import_array()

include "_cy_projection.pxi"

cdef class _ProjectionParameters_f:
    def __cinit__(self):
        self.thisptr = new ProjectionParameters[float]()

    @staticmethod
    def copy(_ProjectionParameters_f inp):
        out = _ProjectionParameters_f()
        out.c_copy(deref(inp.thisptr))
        return out

    cdef void c_copy(self, ProjectionParameters[float] inp):
        if type(self) is _ProjectionParameters_f:
            if self.thisptr is not NULL:
                del self.thisptr
            self.thisptr = new ProjectionParameters[float]()
            self.thisptr.type = inp.type
            self.thisptr.cttype = inp.cttype
            self.thisptr.a = inp.a
            self.thisptr.f = inp.f
            self.thisptr.x0 = inp.x0
            self.thisptr.k0 = inp.k0
            self.thisptr.fe = inp.fe
            self.thisptr.fn = inp.fn
            self.thisptr.phi_0 = inp.phi_0
            self.thisptr.phi_1 = inp.phi_1
            self.thisptr.phi_2 = inp.phi_2

    @property
    def type(self):
        return self.thisptr.type

    @property
    def cttype(self):
        return self.thisptr.cttype

    @property
    def a(self):
        return self.thisptr.a

    @property
    def f(self):
        return self.thisptr.f

    @property
    def x0(self):
        return self.thisptr.x0

    @property
    def k0(self):
        return self.thisptr.k0

    @property
    def fe(self):
        return self.thisptr.fe

    @property
    def fn(self):
        return self.thisptr.fn

    @property
    def phi_0(self):
        return self.thisptr.phi_0

    @property
    def phi_1(self):
        return self.thisptr.phi_1

    @property
    def phi_2(self):
        return self.thisptr.phi_2

    @staticmethod
    def from_dict(other):
        if not isinstance(other, dict):
            raise TypeError("input argument should be a dictionary")
        out = _ProjectionParameters_f()
        out.thisptr.type = <uint32_t> other['type']
        out.thisptr.cttype = <uint32_t> other['cttype']
        out.thisptr.a = <float> other['a']
        out.thisptr.f = <float> other['f']
        out.thisptr.x0 = <float> other['x0']
        out.thisptr.k0 = <float> other['k0']
        out.thisptr.fe = <float> other['fe']
        out.thisptr.fn = <float> other['fn']
        out.thisptr.phi_0 = <float> other['phi_0']
        out.thisptr.phi_1 = <float> other['phi_1']
        out.thisptr.phi_2 = <float> other['phi_2']
        return out

    def to_dict(self):
        out = {}
        for item in ["type", "cttype", "a", "f", "x0", "k0",
            "fe", "fn", "phi_0", "phi_1", "phi_2"]:
            out[item] = getattr(self, item)
        return out

    def __eq__(self, _ProjectionParameters_f other):
        cdef bool out = False
        if not isinstance(other, _ProjectionParameters_f):
            raise TypeError("Input argument should be an instance of ProjectionParameters_f")
        out = deref(self.thisptr) == deref(other.thisptr)
        return out

    def __ne__(self, _ProjectionParameters_f other):
        cdef bool out = False
        if not isinstance(other, _ProjectionParameters_f):
            raise TypeError("Input argument should be an instance of ProjectionParameters_f")
        out = deref(self.thisptr) != deref(other.thisptr)
        return out

    def __dealloc__(self):
        if self.thisptr != NULL:
            if type(self) is _ProjectionParameters_f:
                del self.thisptr

    def __del__(self):
        try:
            self.__dealloc__()
        except Exception:
            pass

    def __repr__(self):
        return self.__class__.__name__


cdef class _ProjectionParameters_d:
    def __cinit__(self):
        self.thisptr = new ProjectionParameters[double]()

    @staticmethod
    def copy(_ProjectionParameters_d inp):
        out = _ProjectionParameters_d()
        out.c_copy(deref(inp.thisptr))
        return out

    cdef void c_copy(self, ProjectionParameters[double] inp):
        if type(self) is _ProjectionParameters_d:
            if self.thisptr is not NULL:
                del self.thisptr
            self.thisptr = new ProjectionParameters[double]()
            self.thisptr.type = inp.type
            self.thisptr.cttype = inp.cttype
            self.thisptr.a = inp.a
            self.thisptr.f = inp.f
            self.thisptr.x0 = inp.x0
            self.thisptr.k0 = inp.k0
            self.thisptr.fe = inp.fe
            self.thisptr.fn = inp.fn
            self.thisptr.phi_0 = inp.phi_0
            self.thisptr.phi_1 = inp.phi_1
            self.thisptr.phi_2 = inp.phi_2

    @property
    def type(self):
        return self.thisptr.type

    @property
    def cttype(self):
        return self.thisptr.cttype

    @property
    def a(self):
        return self.thisptr.a

    @property
    def f(self):
        return self.thisptr.f

    @property
    def x0(self):
        return self.thisptr.x0

    @property
    def k0(self):
        return self.thisptr.k0

    @property
    def fe(self):
        return self.thisptr.fe

    @property
    def fn(self):
        return self.thisptr.fn

    @property
    def phi_0(self):
        return self.thisptr.phi_0

    @property
    def phi_1(self):
        return self.thisptr.phi_1

    @property
    def phi_2(self):
        return self.thisptr.phi_2

    @staticmethod
    def from_dict(other):
        if not isinstance(other, dict):
            raise TypeError("input argument should be a dictionary")
        out = _ProjectionParameters_d()
        out.thisptr.type = <uint32_t> other['type']
        out.thisptr.cttype = <uint32_t> other['cttype']
        out.thisptr.a = <double> other['a']
        out.thisptr.f = <double> other['f']
        out.thisptr.x0 = <double> other['x0']
        out.thisptr.k0 = <double> other['k0']
        out.thisptr.fe = <double> other['fe']
        out.thisptr.fn = <double> other['fn']
        out.thisptr.phi_0 = <double> other['phi_0']
        out.thisptr.phi_1 = <double> other['phi_1']
        out.thisptr.phi_2 = <double> other['phi_2']
        return out

    def to_dict(self):
        out = {}
        for item in ["type", "cttype", "a", "f", "x0", "k0",
            "fe", "fn", "phi_0", "phi_1", "phi_2"]:
            out[item] = getattr(self, item)
        return out

    def __eq__(self, _ProjectionParameters_d other):
        cdef bool out = False
        if not isinstance(other, _ProjectionParameters_d):
            raise TypeError("Input argument should be an instance of ProjectionParameters_d")
        out = deref(self.thisptr) == deref(other.thisptr)
        return out

    def __ne__(self, _ProjectionParameters_d other):
        cdef bool out = False
        if not isinstance(other, _ProjectionParameters_d):
            raise TypeError("Input argument should be an instance of ProjectionParameters_d")
        out = deref(self.thisptr) != deref(other.thisptr)
        return out

    def __dealloc__(self):
        if self.thisptr != NULL:
            if type(self) is _ProjectionParameters_d:
                del self.thisptr

    def __del__(self):
        try:
            self.__dealloc__()
        except Exception:
            pass

    def __repr__(self):
        return self.__class__.__name__

def cy_projParamsFromEPSG(EPSG):
    if isinstance(EPSG, str):
        return projParamsFromEPSG(<string>EPSG.encode('utf-8'))
    elif isinstance(EPSG, bytes):
        return projParamsFromEPSG(<string>EPSG)