# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libcpp.string cimport string
from libc.stdio cimport printf
cimport cython

include "_cy_operation.pxi"

cpdef _cy_json cy_readYaml(string configFile):
    cdef Json json_object
    cdef _cy_json out

    json_object = readYaml(configFile)
    out = _cy_json()
    out.c_copy(json_object.object_items())
    return out