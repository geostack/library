# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libc.stdint cimport uint8_t, uint64_t
from libcpp cimport bool
from libcpp.string cimport string

cdef class cySolver:

    def __cinit__(self):
        with nogil:
            self.thisptr = &Solver.getSolver()

    cpdef void setVerbose(self, bool verbose_) except *:
        with nogil:
            deref(self.thisptr).setVerbose(verbose_)

    cpdef void setVerboseLevel(self, uint8_t verbose_) except *:
        with nogil:
            deref(self.thisptr).setVerboseLevel(verbose_)

    cpdef uint8_t getVerboseLevel(self) except *:
        cdef uint8_t out
        with nogil:
            out = deref(self.thisptr).getVerboseLevel()
        return out

    cpdef void setHostMemoryLimit(self, uint64_t hostMemoryLimit_) except *:
        with nogil:
            deref(self.thisptr).setHostMemoryLimit(hostMemoryLimit_)

    cpdef uint64_t getHostMemoryLimit(self) except *:
        cdef uint64_t out
        with nogil:
            out = deref(self.thisptr).getHostMemoryLimit()
        return out

    cpdef void setDeviceMemoryLimit(self, uint64_t deviceMemoryLimit_) except *:
        with nogil:
            deref(self.thisptr).setDeviceMemoryLimit(deviceMemoryLimit_)

    cpdef uint64_t getDeviceMemoryLimit(self) except *:
        cdef uint64_t out
        with nogil:
            out = deref(self.thisptr).getDeviceMemoryLimit()
        return out

    cpdef string getError(self) except *:
        cdef string rc
        with nogil:
            rc = deref(self.thisptr).getError()
        return rc

    cpdef string processScript(self, string script) except *:
        cdef string rc
        with nogil:
            rc = deref(self.thisptr).processScript(script)
        return rc

    cpdef size_t getNullHash(self) except *:
        cdef size_t rc
        with nogil:
            rc = deref(self.thisptr).getNullHash()
        return rc

    cpdef bool openCLInitialised(self) except *:
        cdef bool rc
        with nogil:
            rc = deref(self.thisptr).openCLInitialised()
        return rc

    cpdef bool initOpenCL(self) except *:
        cdef bool rc
        with nogil:
            rc = deref(self.thisptr).initOpenCL()
        return rc

    def __dealloc__(self):
        pass

    def __repr__(self):
        return self.__class__.__name__

    def __del__(self):
        try:
            self.__dealloc__()
        except Exception:
            pass

include "_cy_solver.pxi"