# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref, preincrement as preinc
from libcpp.string cimport string
from libcpp.map cimport map as cpp_map
from libcpp.vector cimport vector as cpp_vector
from libcpp cimport bool, nullptr
from libc.stdio cimport (printf, FILE, fopen, fclose, fseek, ftell, SEEK_END,
    fread, rewind, fwrite, fflush)
from libc.stdlib cimport malloc, free
from libc.string cimport strlen, memset
import numpy as np
cimport numpy as np
from pickle import dump, load, HIGHEST_PROTOCOL

np.import_array()

cdef string file_reader_c(char* inp_file_name):

    cdef FILE *fid = NULL
    cdef long file_size
    cdef string buf
    cdef size_t rc
    fid = fopen(inp_file_name, "r")

    if fid is not NULL:
        with nogil:
            fseek(fid, 0, SEEK_END)
            file_size = ftell(fid)
            rewind(fid)
            buf.resize(file_size)
            rc = fread(&buf[0], file_size, 1, fid)
            fclose(fid)
    else:
        raise RuntimeError("Unable to open file to read")
    return buf

cdef class _cy_json:
    '''cython wrapper for json11
    '''

    def __cinit__(self):
        self.objptr.reset(new Json.json_object())
        self.thisptr.reset(new Json(deref(self.objptr)))

    cdef void c_copy(self, Json.json_object inp_obj):
        if self.objptr != nullptr:
            self.objptr.reset(nullptr)
        if self.thisptr != nullptr:
            self.thisptr.reset(nullptr)
        self.objptr.reset(new Json.json_object(inp_obj))
        self.thisptr.reset(new Json(deref(self.objptr)))

    cpdef string dump(self):
        cdef string out
        if self.thisptr != nullptr:
            out = deref(self.thisptr).dump()
        return out

    cpdef void dumps(self, string out_file_name):
        if not self.is_null():
            _to_file(self, out_file_name)

    cpdef void load(self, string inp_string):
        if self.thisptr != nullptr:
            self.thisptr.reset(nullptr)
        _out = _parse(<bytes> inp_string)
        if _out.is_object():
            self.c_copy(deref(_out.thisptr).object_items())

    cpdef void loads(self, string inp_file_name):
        if self.thisptr != nullptr:
            self.thisptr.reset(nullptr)
        _out = _from_file(<bytes> inp_file_name)
        if _out.is_object():
            self.c_copy(deref(_out.thisptr).object_items())

    cpdef void array_items(self):
        cdef Json.json_array out
        if self.thisptr != nullptr:
            out = deref(self.thisptr).array_items()

    cpdef void object_items(self):
        cdef Json.json_object out
        if self.thisptr != nullptr:
            out = deref(self.thisptr).object_items()

    cpdef void string_value(self):
        cdef string out
        if self.thisptr != nullptr:
            out = deref(self.thisptr).string_value()

    cpdef bool is_null(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).is_null()

    cpdef bool is_number(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).is_number()

    cpdef bool is_bool(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).is_bool()

    cpdef bool is_string(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).is_string()

    cpdef bool is_array(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).is_array()

    cpdef bool is_object(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).is_object()

    cpdef double number_value(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).number_value()

    cpdef int int_value(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).int_value()

    cpdef bool bool_value(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).bool_value()

    def keys(self):
        cdef cpp_map[string, Json].iterator end
        cdef cpp_map[string, Json].iterator it
        out_keys = []
        if self.objptr != nullptr:
            end = deref(self.objptr).end()
            it = deref(self.objptr).begin()
            while it != end:
                out_keys.append(deref(it).first)
                preinc(it)
        return out_keys

    def __getitem__(self, other):
        cdef Json out

        if self.thisptr != nullptr:
            if isinstance(other, int):
                out = deref(self.thisptr)[<size_t> other]
            elif isinstance(other, bytes):
                out = deref(self.thisptr)[<bytes> other]

            if out.is_bool():
                return out.bool_value()
            elif out.is_number():
                return out.number_value()
            elif out.is_string():
                return out.string_value()
            elif out.is_array():
                return _cy_json._to_list(out.array_items())
            elif out.is_object():
                return _cy_json._to_dict(out.object_items())

    @staticmethod
    cdef list _to_list(Json.json_array inp_arr):
        cdef vector[Json].iterator end
        cdef vector[Json].iterator it
        out_list = []

        end = inp_arr.end()
        it = inp_arr.begin()
        while it != end:
            if deref(it).is_number():
                out_list.append(deref(it).number_value())
            elif deref(it).is_string():
                out_list.append(deref(it).string_value())
            elif deref(it).is_bool():
                out_list.append(deref(it).bool_value())
            elif deref(it).is_array():
                out_list.append(_cy_json._to_list(deref(it).array_items()))
            elif deref(it).is_object():
                out_list.append(_cy_json._to_dict(deref(it).object_items()))
            preinc(it)
        return out_list

    @staticmethod
    cdef dict _to_dict(Json.json_object inp_obj):
        cdef cpp_map[string, Json].iterator end
        cdef cpp_map[string, Json].iterator it
        out_dict = {}

        end = inp_obj.end()
        it = inp_obj.begin()
        while it != end:
            if deref(it).second.is_string():
                out_dict[deref(it).first] = deref(it).second.string_value()
            elif deref(it).second.is_number():
                out_dict[deref(it).first] = deref(it).second.number_value()
            elif deref(it).second.is_bool():
                out_dict[deref(it).first] = deref(it).second.bool_value()
            elif deref(it).second.is_array():
                out_dict[deref(it).first] = _cy_json._to_list(deref(it).second.array_items())
            elif deref(it).second.is_object():
                out_dict[deref(it).first] = _cy_json._to_dict(deref(it).second.object_items())
            preinc(it)
        return out_dict

    @staticmethod
    cdef Json.json_array _from_list(list inp_list):
        cdef Json.json_array out
        cdef int i, nx
        nx = <int> len(inp_list)
        for i in range(nx):
            if isinstance(inp_list[i], str):
                out.push_back(Json(<bytes> inp_list[i].encode('UTF-8')))
            elif isinstance(inp_list[i], int):
                out.push_back(Json(<double> inp_list[i]))
            elif isinstance(inp_list[i], float):
                out.push_back(Json(<double> inp_list[i]))
            elif inp_list[i] is True or inp_list[i] is False:
                out.push_back(Json(<bool> inp_list[i]))
            elif isinstance(inp_list[i], list):
                out.push_back(Json(_cy_json._from_list(inp_list[i])))
            elif isinstance(inp_list[i], dict):
                out.push_back(Json(_cy_json._from_dict(inp_list[i])))
        return out

    @staticmethod
    cdef Json.json_object _from_dict(dict inp_dict):
        cdef Json.json_object out
        for item in inp_dict:
            if not isinstance(item, str):
                raise TypeError("dictionary key can only be of string type")
            if isinstance(inp_dict[item], int):
                out[<bytes> item.encode('UTF-8')] = Json(<double> inp_dict[item])
            elif isinstance(inp_dict[item], float):
                out[<bytes> item.encode('UTF-8')] = Json(<double> inp_dict[item])
            elif isinstance(inp_dict[item], str):
                out[<bytes> item.encode('UTF-8')] = Json(<bytes> inp_dict[item].encode('UTF-8'))
            elif inp_dict[item] is True or inp_dict[item] is False:
                out[<bytes> item.encode('UTF-8')] = Json(<bool> inp_dict[item])
            elif isinstance(inp_dict[item], list):
                out[<bytes> item.encode('UTF-8')] = Json(_cy_json._from_list(inp_dict[item]))
            elif isinstance(inp_dict[item], dict):
                out[<bytes> item.encode('UTF-8')] = Json(_cy_json._from_dict(inp_dict[item]))
        return out

    @staticmethod
    def from_dict(other):
        cdef Json.json_object _out_obj
        if isinstance(other, dict):
            _out_obj = _cy_json._from_dict(other)
            out = _cy_json()
            out.c_copy(_out_obj)
            return out
        else:
            raise TypeError("Input argument should be an instance of python dictionary")


    def to_dict(self):
        out = {}
        if self.objptr != nullptr:
            out = _cy_json._to_dict(deref(self.objptr))
        return out

    def __dealloc__(self):
        if self.objptr != nullptr:
            self.objptr.reset(nullptr)
        if self.thisptr != nullptr:
            self.thisptr.reset(nullptr)


cpdef _cy_json _from_file(bytes inp_file_name):
    '''read json file into a buffer and parse it through json11
    '''
    cdef Json _out
    cdef string err
    cdef string buf

    buf = file_reader_c(<bytes> inp_file_name)

    if buf.size() > 0:
        _out = Json.parse(buf, err, STANDARD)
        if err.size() > 0:
            printf("%s\n", <bytes> err)
            raise RuntimeError("Unable to read file to buffer")
        else:
            buf.clear()
    else:
        raise RuntimeError("Input buffer is null")

    if not _out.is_null():
        out = _cy_json()
        out.c_copy(_out.object_items())
        return out
    else:
        raise RuntimeError("json parsed object is null")


cpdef void _to_file(_cy_json inp, bytes out_file_name):
    '''write json object to a file
    '''
    cdef FILE *fid
    cdef string out_string
    fid = fopen(out_file_name, "w")
    if fid is not NULL:
        out_string = inp.dump()
        with nogil:
            fwrite(out_string.c_str(), out_string.length(), sizeof(char), fid)
            fflush(fid)
            fclose(fid)
        fid = NULL
    else:
        raise RuntimeError("Unable to open file to write")


cpdef _cy_json _parse(bytes input):
    '''parse input bytes into a json11 object
    '''
    cdef Json _out
    cdef string err
    _out = Json.parse(<bytes> input, err, STANDARD)
    printf("%s\n", <bytes> err)
    if _out.is_object():
        out = _cy_json()
        out.c_copy(_out.object_items())
        return out
    else:
        raise TypeError("output from json parse is not an object")
