# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from cython.operator cimport address as addr
from libcpp.string cimport string
from libc.stdio cimport printf
from libcpp.vector cimport vector
from libcpp.iterator cimport iterator
from libc.stdint cimport uint8_t, uint32_t
from libcpp cimport bool

include "_cy_runner.pxi"