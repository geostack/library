# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t, uint64_t
from libc.string cimport memcpy
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..vector._cy_vector cimport Vector
from ..raster._cy_raster cimport (_cyRaster_d, _cyRaster_f,
                                  _cyRaster_d_i, _cyRaster_f_i,
                                  RasterBase, Raster)
from ..vector._cy_vector cimport _Vector_d, _Vector_f
from ..raster._cy_raster cimport _RasterPtrList_d, _RasterPtrList_f
from ..core._cy_variables cimport (_Variables_f, _Variables_d,
                                   _Variables_i, VariablesBase, Variables)
from ..core._cy_property cimport PropertyMap

np.import_array()

ctypedef uint32_t cl_uint

cdef extern from "gs_particle.h" namespace "Geostack":
    cdef cppclass Particle[T]:
        Particle() except +
        bool init(string jsonConfig,
                  Vector[T] particles,
                  shared_ptr[Variables[T, string]] variables,
                  vector[shared_ptr[RasterBase[T]]] input_layers) except + nogil
        bool step() except + nogil
        void setTimeStep(T) except + nogil
        void addParticles(Vector[T] particles) except + nogil
        Vector[T] &getParticles() except + nogil
        cl_uint getSamplePlaneIndexCount() except + nogil
        vector[cl_uint] getSamplePlaneIndexes() except + nogil

include "_cy_particle.pxi"