# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..core._cy_variables cimport (Variables,
                                  _Variables_f,
                                  _Variables_d)
from ..series._cy_series cimport Series
from ..series._cy_series cimport Series_dbl_flt, Series_dbl_dbl
from ..series._cy_series cimport Series_int_flt, Series_int_dbl

np.import_array()

cdef extern from "gs_ode.h" namespace "Geostack":
    cdef cppclass ODE[T]:
        ODE() except +
        bool init(string jsonConfig,
                  shared_ptr[Variables[T, string]] variables) except +
        bool step() except +
        void setTime(T time_) except +
        void setTimeStep(T time_) except +
        bool isInitialised() except +
        T get(string var2_name, string var1_name, T var1, uint32_t index) except +
        Series[double, T]& getSeries(string, string, uint32_t) except +


include "_cy_ode.pxi"