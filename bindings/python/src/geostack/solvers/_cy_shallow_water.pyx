# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t, uint64_t
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..raster._cy_raster cimport (_cyRaster_d, _cyRaster_f,
                                  Raster, RasterBase,
                                  _cyRasterBase_f, _cyRasterBase_d)
from ..raster._cy_raster cimport _RasterPtrList_d, _RasterPtrList_f
from ..core._cy_property cimport PropertyMap

np.import_array()

cdef extern from "gs_shallow_water.h" namespace "Geostack":
    cdef enum ShallowWaterLayersType "Type":
        h
        b
        uh
        vh
        fi
        fe
        ft
        ShallowWaterLayers_END

cdef extern from "gs_shallow_water.h" namespace "Geostack":
    cdef cppclass ShallowWater[T]:
        ShallowWater() except +
        bool init(string jsonConfig,
                  vector[shared_ptr[RasterBase[T]]] input_layers) except + nogil
        bool step() except + nogil
        Raster[T, T]& getHeight() except +
        Raster[T, T]& getBase() except +
        Raster[T, T]& getUnitDischarge_x() except +
        Raster[T, T]& getUnitDischarge_y() except +
        Raster[T, T]& getU() except +
        Raster[T, T]& getV() except +

include "_cy_shallow_water.pxi"