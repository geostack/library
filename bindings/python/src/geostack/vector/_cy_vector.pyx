# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython cimport floating
from cython.operator cimport dereference as deref, preincrement as preinc
from libcpp cimport nullptr_t, nullptr, bool
from libcpp.memory cimport shared_ptr, make_shared
from libcpp.string cimport string
from libcpp.map cimport map as cpp_map
from libcpp.unordered_map cimport unordered_map
from libcpp.pair cimport pair as cpp_pair
from libcpp.vector cimport vector
from libcpp.algorithm cimport binary_search
from libc.math cimport ceil, fabs
from libc.stdio cimport printf
from libc.string cimport memcpy
from libc.stdint cimport uint32_t, uint16_t, uint64_t, uint8_t
import numpy as np
cimport numpy as np
from cpython cimport PyObject
from ..raster._cy_raster cimport Raster, GeometryType

np.import_array()

cdef inline bool is_equal(floating val1, floating val2):
    cdef bool out = False
    if fabs(val1 - val2) <= 1e-6:
        out = True
    return out

cdef class IndexList:
    def __cinit__(self):
        self.thisptr = new vector[cl_uint]()

    cdef void c_copy(self, _index_list other):
        deref(self.thisptr).clear()
        self.thisptr = new vector[cl_uint](other)

    @property
    def size(self):
        return deref(self.thisptr).size()

    @staticmethod
    cdef IndexList from_index_list(_index_list this):
        out = IndexList()
        out.c_copy(this)
        return out

    def as_array(self):
        cdef cl_uint[:] out
        out = np.zeros(shape=(self.size,), dtype=np.uint32)
        memcpy(&out[0], deref(self.thisptr).data(),
               self.size * sizeof(cl_uint))
        return out

    cpdef bool contains(self, size_t index) except *:
        cdef bool out = False
        out = binary_search(deref(self.thisptr).begin(),
                            deref(self.thisptr).end(), index)
        return out

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self):
        cdef int out
        if self.index < self.size:
            out = deref(self.thisptr)[self.index]
            self.index += 1
            return out
        else:
            self.index = 0
            raise StopIteration

    def __getitem__(self, size_t index):
        cdef int out
        if index >= 0 and index < self.size:
            out = deref(self.thisptr)[index]
            return out
        else:
            raise IndexError(f"{index} is out of range")

    def __len__(self):
        return self.size

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        deref(self.thisptr).clear()
        del self.thisptr

include "_cy_vector.pxi"