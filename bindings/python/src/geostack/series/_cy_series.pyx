# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#distutils: language=c++
#cython: boundscheck=False
#cython: wraparound=False
#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode
#cython: emit_linenums=True

from cython.operator cimport dereference as deref
from libcpp.string cimport string
from libcpp cimport nullptr
from libcpp.map cimport map as cpp_map
from libcpp.pair cimport pair as cpp_pair
from libcpp.vector cimport vector
from libc.stdio cimport printf
from libc.stdint cimport uint32_t, int64_t
from libc.string cimport memcpy
from numpy cimport float32_t, float64_t
import numpy as np
cimport numpy as np

np.import_array()

include "_cy_series.pxi"

cdef vector[cpp_pair[string, floating]] vec_of_pair(self, char[:, :] xval, floating[:] yval):
    raise NotImplementedError("")

cdef series_dbl get_series_item_dbl(double x, floating y):
    cdef series_dbl *out = new SeriesItem[double, floating]()
    out.x, out.y = x, y
    return deref(out)

cdef series_int get_series_item_int(int64_t x, floating y):
    cdef series_int *out = new SeriesItem[int64_t, floating]()
    out.x, out.y = x, y
    return deref(out)

cdef vector[series_dbl] vec_of_series_dbl(self, double[:] xval, floating[:] yval):
    cdef vector[series_dbl] out
    cdef int i, nx
    assert xval.shape == yval.shape, "Input x and y values should be of same length"
    nx = <int> xval.shape[0]
    for i in range(nx):
        out.push_back(get_series_item_dbl(xval[i], yval[i]))
    return out

cdef vector[series_int] vec_of_series_int(self, int64_t[:] xval, floating[:] yval):
    cdef vector[series_int] out
    cdef int i, nx
    assert xval.shape == yval.shape, "Input x and y values should be of same length"
    nx = <int> xval.shape[0]
    for i in range(nx):
        out.push_back(get_series_item_int(xval[i], yval[i]))
    return out