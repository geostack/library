{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "96b8b6eb-be07-46bf-be87-4d08bfcdf988",
   "metadata": {},
   "source": [
    "## 1. Introduction\n",
    "\n",
    "The aim of Geostack is to provide accelerated computation for geospatial tasks. Geostack shouldn't get in your way or replace any of your favourite tools, and should work interoperably wherever possible. Some examples of this will be described later. But if you're new to geospatial modelling, it should provide all the basic structure to do what you need.\n",
    "\n",
    "One of the challenges with geospatial modelling is the wide range of data formats and how to get these to work with one another. There are three main flavours:\n",
    "- _Raster data_, this is structured data defined over a set of cells each containing a value. Although this can come in many forms, a common format is a Cartesian grid where each grid cell contains some value. An example of this is elevation data, where the landscape is gridded into regular cells and the height about some datum (such as sea level) is stored in each.\n",
    "- _Vector data_, this is unstructured data containing shapes and associated values. Again, this can come in many forms but most commonly consists of points, lines and polygons. An example of this is a road network, which contains lines representing the roads and might have a road width stored for each line segment.\n",
    "- _Series data_, this is structured data containing a timestamp and associated values. An example of this are the readings from a weather station consisting of a time stamp and readings of precipitation and wind speed.\n",
    "\n",
    "These datasets also contain (or should contain) auxiliary information or metadata describing factors such as geospatial references to where they are located, how they are mapped, the validity of the data and the domain over which the data is defined. Some of the main difficulties in dealing with geospatial data is the handling of these different mappings in relation to each other, for example mixing data with geographic (ellipsoidal) coordinate systems with data in projected (flat) coordinate systems.\n",
    "\n",
    "Geostack attempts to abstract these data transformations (projection, resolution and validity) from the data itself. The idea being users can express the operations they'd like to perform on the data clearly and independently of any of the data transformations."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "af40650d-fa70-484c-8949-406c8b7fa3ef",
   "metadata": {},
   "source": [
    "### Reading and writing data\n",
    "\n",
    "Raster and vector data is stored in Geostack using _Raster_ and _Vector_ objects.\n",
    "\n",
    "#### Rasters\n",
    "An empty Raster object is created as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "5708b2af-20b4-403d-98d6-acc3841354fb",
   "metadata": {},
   "outputs": [],
   "source": [
    "from geostack.raster import Raster\n",
    "\n",
    "r = Raster(name = \"r\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b5a011f-08ca-4775-bcbc-8339a12036e5",
   "metadata": {},
   "source": [
    "where the _name_ parameter sets the internal name of the object for later manipulation operations. Then to read data into the Raster:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "c87b9d36-9cb6-48e8-a11f-b9a3a9e4f24e",
   "metadata": {},
   "outputs": [],
   "source": [
    "r.read(\"./data/01_example.tif\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1ae21a63-4d7d-4118-b64d-f8f748409531",
   "metadata": {},
   "source": [
    "Geostack natively supports GeoTiff, NetCDFv3, flt and asc data formats. For other data a handler called RasterFile can be used which uses the NetCDF, GDAL and Xarray Python libraries, which will be used in later examples.\n",
    "\n",
    "To write the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "31d9c945-7f41-4c29-89e2-f3f303e2bbb8",
   "metadata": {},
   "outputs": [],
   "source": [
    "#r.write(\"01_example_out.tif\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1d68673d-cd4a-422b-ba89-05588f24da55",
   "metadata": {},
   "source": [
    "Rasters don't have to be read from a data source, they can be initialised with cell numbers and spacings using an [_init_](https://gitlab.com/geostack/library/-/wikis/Raster%20creation%20and%20manipulation%20(Python)#rasters) function. For information on the Raster the getDimensions() function can be used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "7fe16e19-ad95-4a05-832d-e17ebd5af85d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "RasterDimensions:\n",
      "    ex    :  150.6361541748047\n",
      "    ey    :  -34.677215576171875\n",
      "    ez    :  6.0\n",
      "    tx    :  1\n",
      "    ty    :  1\n",
      "    nx    :  256\n",
      "    ny    :  256\n",
      "    nz    :  6\n",
      "    ox    :  150.26388549804688\n",
      "    oy    :  -34.80477523803711\n",
      "    oz    :  0.0\n",
      "    hx    :  0.0014541478594765067\n",
      "    hy    :  0.0004982842365279794\n",
      "    hz    :  1.0\n",
      "    mx    :  256\n",
      "    my    :  256\n"
     ]
    }
   ],
   "source": [
    "print(r.getDimensions())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2ec749e4-4f9e-4127-a746-c667bb6bb463",
   "metadata": {},
   "source": [
    "This shows the raster origin (ox, oy, oz), end point(ex, ey, ez), number of internal tiles and memory size (tx, ty and mx, my), spacing (hx, hy) and number of cells in each dimension (nx, ny, nz). This particular raster is 256 x 256 (x and y dimensions in Geostack) with 6 layers (z dimension in geostack).\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/cf0d4a63dfa053a1606e540e6596b8fe/Fig_raster_6_layer.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c742a22e-4d60-4737-954d-a0a8a86d9b8e",
   "metadata": {},
   "source": [
    "The values of the origin and spacing look like this Raster is in geographic coordinates, which can be shown using the getProjectionParameters() function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "edb69bc9-0b6b-4e08-9c94-f4f9fd4470ea",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "GEOGCS[\"Unknown\",DATUM[\"Unknown\",SPHEROID[\"WGS84\",6378137,298.257223563]],PRIMEM[\"Greenwich\",0],UNIT[\"Degree\",0.0174532925199433]]\n"
     ]
    }
   ],
   "source": [
    "print(r.getProjectionParameters().to_wkt())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9cbf8640-a76d-4261-9fac-488b452f14c0",
   "metadata": {},
   "source": [
    "#### Vectors\n",
    "A vector object can either be created empty or directly from a vector dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "71a603e8-fae5-447e-bb63-7f73731a73a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "from geostack.vector import Vector\n",
    "\n",
    "v = Vector.from_geojson(\"./data/01_example.geojson\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "65f545e1-1e83-4475-8dba-2ab04940c3eb",
   "metadata": {},
   "source": [
    "Geostack natively supports GeoJSON, Shapefile and WKT data formats, and Fiona, GeoPandas, PyShp and OGR using the Python libraries.\n",
    "\n",
    "To write the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "66f68caa-8829-489d-934d-0167c5005683",
   "metadata": {},
   "outputs": [],
   "source": [
    "from geostack.definitions import GeometryType\n",
    "\n",
    "#v.to_shapefile(\"01_example_out.shp\", GeometryType.Polygon)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c136a76-c12b-4cf1-a7e0-60bb9b7f3456",
   "metadata": {},
   "source": [
    "where an extra definition is required to write the shapefile, as only one geometry type per file is supported."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8fc55b8c-e188-4945-8a8c-0e89a234a1db",
   "metadata": {},
   "source": [
    "### Manipulating data\n",
    "\n",
    "#### Rasters\n",
    "\n",
    "Raster data cells can be read and set using the _getCellValue(i, j, k)_ and _setCellValue(v, i, j, k)_ functions, where _i_, _j_ and _k_ are the cell indexes in three dimensions and _v_ is the value to be set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "84be41a6-6a33-4469-8578-b900745d8fdd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "981.0\n",
      "9999.0\n"
     ]
    }
   ],
   "source": [
    "print(r.getCellValue(0, 0))\n",
    "r.setCellValue(9999.0, 0, 0)\n",
    "print(r.getCellValue(0, 0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5930ae14-36c5-440c-afcf-622a6939c3de",
   "metadata": {},
   "source": [
    "These functions are inefficient for changing large volumes of data. For more complex tasks the _data_ property of a Raster can be used. This returns the data as a 3D numpy array and is fully interoperable with numpy functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "6f1140c5-2fd1-4976-9eec-dac5d98e0492",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[[9999.   983.  1016.5 ... 1101.  1193.  1209. ]\n",
      "  [ 976.5 1007.5 1104.5 ... 1131.  1222.  1232. ]\n",
      "  [ 984.5  988.  1066.5 ... 1205.  1231.  1243. ]\n",
      "  ...\n",
      "  [ 982.5  975.   948.5 ... 1077.   932.   956. ]\n",
      "  [ 954.   984.   978.5 ...  968.   954.   961. ]\n",
      "  [ 962.5  960.5  988.5 ...  969.   955.   959. ]]\n",
      "\n",
      " [[ 751.   752.   787.5 ...  926.  1124.  1121. ]\n",
      "  [ 760.   767.   911.  ...  990.  1155.  1113. ]\n",
      "  [ 758.   756.   849.  ... 1103.  1171.  1165. ]\n",
      "  ...\n",
      "  [ 782.   779.   735.5 ... 1029.   719.   792. ]\n",
      "  [ 740.5  792.   749.  ...  787.   795.   779. ]\n",
      "  [ 758.5  751.   776.  ...  759.   751.   788. ]]\n",
      "\n",
      " [[ 794.5  753.5  770.  ...  698.   937.   975. ]\n",
      "  [ 765.   731.   944.5 ...  828.   974.   999. ]\n",
      "  [ 769.5  752.5  840.  ...  984.  1065.  1077. ]\n",
      "  ...\n",
      "  [ 767.5  759.   642.  ...  772.   468.   518. ]\n",
      "  [ 695.   747.   641.  ...  536.   505.   512. ]\n",
      "  [ 724.5  694.5  745.5 ...  535.   507.   516. ]]\n",
      "\n",
      " [[1142.   959.5  882.5 ... 2250.  2347.  2254. ]\n",
      "  [1033.5  804.  1076.5 ... 2275.  2426.  2240. ]\n",
      "  [1039.5  930.5  968.5 ... 2121.  2371.  2363. ]\n",
      "  ...\n",
      "  [1227.  1132.5 1203.  ... 3098.  1549.  2688. ]\n",
      "  [1080.  1183.5  709.5 ... 2384.  2336.  2306. ]\n",
      "  [1226.  1048.  1070.  ... 2084.  1894.  2342. ]]\n",
      "\n",
      " [[1255.  1057.  1005.  ... 2534.  2727.  2490. ]\n",
      "  [1193.5  936.5 1186.5 ... 2703.  2692.  2530. ]\n",
      "  [1139.5 1004.  1103.  ... 2400.  2653.  2544. ]\n",
      "  ...\n",
      "  [1347.  1270.5 1345.5 ... 3432.  2155.  3139. ]\n",
      "  [1282.  1319.   959.5 ... 2813.  2435.  2727. ]\n",
      "  [1305.  1278.  1154.5 ... 2458.  2267.  2421. ]]\n",
      "\n",
      " [[1171.5 1513.  2055.  ...  948.  1613.  1803. ]\n",
      "  [1223.5 1810.  2542.  ... 1489.  1784.  1896. ]\n",
      "  [1246.  1499.5 2313.  ... 1854.  1985.  1907. ]\n",
      "  ...\n",
      "  [1346.5 1046.5 1012.  ... 1077.   331.   499. ]\n",
      "  [1156.  1217.5 1674.5 ...  503.   388.   436. ]\n",
      "  [1073.  1170.5 1600.5 ...  373.   315.   346. ]]]\n"
     ]
    }
   ],
   "source": [
    "print(r.data)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "81a50031-6b96-4934-b4de-bae6d0b7899e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "9999.0\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "\n",
    "print(np.max(r.data))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7122c49a-d0d4-411e-9a54-52603a52f0bc",
   "metadata": {},
   "source": [
    "Note the data property is read-only, for changing data scripting operations can be used. These are covered in the next section."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64615f05-9395-4488-8385-131617b68d75",
   "metadata": {},
   "source": [
    "#### Vectors\n",
    "\n",
    "Fields in vector data can be updated using getProperty(id, name) and setProperty(id, name, v), where id is the geometry id, name is the property (or field) name and v is the value to be set. To get a list of all properties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "f82f8d62-14b6-4699-a575-55e82868accd",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'var1', 'var2'}\n"
     ]
    }
   ],
   "source": [
    "print(v.properties.getPropertyNames())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ce98ecc9-b778-42a1-be5e-393a1f23ab94",
   "metadata": {},
   "source": [
    "Then a property can be read and set on each geometry element using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "bc1cb592-c4f8-4600-bdd5-9d02a3414e9a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "id2\n",
      "2.0\n",
      "9999.0\n"
     ]
    }
   ],
   "source": [
    "print(v.getProperty(0, \"var1\"))\n",
    "print(v.getProperty(0, \"var2\"))\n",
    "v.setProperty(0, \"var2\", 9999.0)\n",
    "print(v.getProperty(0, \"var2\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "66efa5b8-0036-4003-8266-cf19cc1934cf",
   "metadata": {},
   "source": [
    "Where the geometry at index 0 was used in this example. A wide range of functions can be used for accessing various geometry elements, covered layer. For example, to show all 'var2' properties the following loop could be used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "d863b3f1-32f2-4c2e-91de-46a11db4c3b6",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "9999.0\n",
      "3.0\n",
      "1.0\n"
     ]
    }
   ],
   "source": [
    "for idx in v.getGeometryIndexes():\n",
    "    print(v.getProperty(idx, \"var2\"))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
