# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

cmake_minimum_required(VERSION 3.8)

# Get Python
if(NOT PYTHON)
    find_program(PYTHON "python")
endif()

# Building python bindings
option(CYTHON_BUILD_FLAG "Cythonize pyx files" ON)
if(NOT CYTHON_BUILD_FLAG)
    set(CYTHON_COMPILE_FLAG "false")
else()
    set(CYTHON_COMPILE_FLAG "true")
endif()

# Building bindings with debugging symbolds
option(CYTHON_DEBUG_FLAG "Cythonize pyx files" OFF)
if(NOT CYTHON_DEBUG_FLAG)
    set(CYTHON_DEBUG_FLAG "false")
else()
    set(CYTHON_DEBUG_FLAG "true")
endif()

# Copy source tree
file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/src DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# Append the content of ${CMAKE_INSTALL_PREFIX}/LICENSE to ${CMAKE_CURRENT_BINARY_DIR}/src/LICENSE
if(EXISTS "${CMAKE_INSTALL_PREFIX}/LICENSE")
    file(READ "${CMAKE_INSTALL_PREFIX}/LICENSE" LICENSE_CONTENT)
    file(APPEND "${CMAKE_CURRENT_BINARY_DIR}/src/LICENSE" "${LICENSE_CONTENT}")
endif()

# Write configuration files
set(SETUP_PY_IN "${CMAKE_CURRENT_BINARY_DIR}/src/setup.py.in")
set(SETUP_PY    "${CMAKE_CURRENT_BINARY_DIR}/src/setup.py")
configure_file(${SETUP_PY_IN} ${SETUP_PY})

